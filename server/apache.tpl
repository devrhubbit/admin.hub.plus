<VirtualHost *:80>
    ServerName ^#WS_NAME#^
    DocumentRoot ^#WS_ROOT#^
    Redirect permanent / https://^#WS_NAME#^/
</VirtualHost>

<VirtualHost *:443>
    ServerName ^#WS_NAME#^
    ServerAdmin info@rhubbit.com
    DocumentRoot ^#WS_ROOT#^
    SetEnv APPLICATION_ENV "production"

    SSLEngine on
    SSLCertificateKeyFile /etc/ssl/certs/HubPlus_private.key
    SSLCertificateFile /etc/ssl/certs/HubPlus_cert_file.crt
    SSLCertificateChainFile /etc/ssl/certs/HubPlus_cert_chain_file.crt

    CustomLog ^#WS_LOG#^/access.log combined
    ErrorLog ^#WS_LOG#^/error.log
    <Directory ^#WS_ROOT#^>
        DirectoryIndex index.php
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>

    Header always set Strict-Transport-Security "max-age=15768000; includeSubDomains; preload"
</VirtualHost>

<VirtualHost *:443>
    ServerName ^#CDN_NAME#^
    ServerAdmin info@rhubbit.com
    DocumentRoot ^#CDN_ROOT#^

    SSLEngine on
    SSLCertificateKeyFile /etc/ssl/certs/HubPlus_private.key
    SSLCertificateFile /etc/ssl/certs/HubPlus_cert_file.crt
    SSLCertificateChainFile /etc/ssl/certs/HubPlus_cert_chain_file.crt

    CustomLog ^#CDN_LOG#^/access.log combined
    ErrorLog ^#CDN_LOG#^/error.log

    <FilesMatch ".(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|css|rss|atom|js|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf|html|txt|htm)">
        Header set Access-Control-Allow-Origin "*"
    </FilesMatch>

    Header always set Strict-Transport-Security "max-age=15768000; includeSubDomains; preload"
</VirtualHost>