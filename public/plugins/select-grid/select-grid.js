/**
 * Created by mirco on 19/04/16.
 */
(function ( $ ) {
    var id = 0;
    var $selectGrid = null;
    var enableToggle = true;

    $.fn.select = function(key, value) {
        $('div#data-grid-' + id + ' span img').removeClass('selected');
        $('div#data-grid-' + id + ' span div').removeClass('selected');
        $('div#data-grid-' + id + ' span img').addClass('deselected');
        $('div#data-grid-' + id + ' span div').addClass('deselected');

        $('div#data-grid-' + id + ' span img[data-grid-val="'+key+'"]').removeClass('deselected').addClass('selected');
        $('div#data-grid-' + id + ' span div[data-grid-val="'+key+'"]').removeClass('deselected').addClass('selected');

        $('.data-grid-' + id).val("" + value).prop('selected', true).trigger('change');
        if($selectGrid != null) {
            $selectGrid.trigger("select-grid.selected", [key, value]);
        }
    },

    $.fn.selectGrid = function() {

        this.each(function () {
            id++;
            $selectGrid = $(this);

            var dataToggle = $selectGrid.attr("data-toggle");
            if (dataToggle != null) { enableToggle = dataToggle; }

            $(this).attr("data-grid", "data-grid-"+id);
            $(this).addClass("select-grid");
            $(this).addClass("data-grid-"+id);
            $(this).after('<div id="data-grid-'+id+'" class="row data-grid-row"></div>');

            var $dataGrid = $("#data-grid-"+id);

            var childId = 0;
            var selected_val = $selectGrid.val();
            var selected_txt = null;
            $(this).children().each(function () {

                childId++;

                var content = $(this).attr("data-content");
                if(content != null) {
                    var title = $(this).text();
                    var value = $(this).val();

                    var status_class = "deselected";
                    if(value == selected_val) {
                        status_class = "selected";
                        selected_txt = title;
                    }

                    $dataGrid.append('<div class="col-xs-3">' +
                        '<span class="thumbnail">' +
                        '<img id="data-grid-img-' + id + '-' + childId + '" data-grid-val="' + value + '" data-grid-txt="' + title + '" src="' + content + '" class="img-responsive '+status_class+' data-grid-item" />' +
                        '<div class="text-center title" data-grid-val="' + value + '">' + title + '</div>' +
                        '</span>' +
                        '</div>'
                    );

                    $('body').on('click', 'div.data-grid-row img#data-grid-img-' + id + '-' + childId, function () {
                        var dataGridId = $(this).parent().parent().parent().attr('id');
                        var optionVal  = $(this).attr("data-grid-val");
                        var optionTxt  = $(this).attr("data-grid-txt");

                        if($(this).hasClass('selected') && enableToggle) {
                            $('div#' + dataGridId + ' span img').removeClass('selected');
                            $('div#' + dataGridId + ' span div').removeClass('selected');

                            $('div#' + dataGridId + ' span img').addClass('deselected');
                            $('div#' + dataGridId + ' span div').addClass('deselected');

                            $('.' + dataGridId).val(null).prop('selected', true).trigger('change');
                            $selectGrid.trigger("select-grid.deselected", [optionVal, optionTxt]);
                        } else {
                            $('div#' + dataGridId + ' span img').removeClass('selected');
                            $('div#' + dataGridId + ' span div').removeClass('selected');

                            $('div#' + dataGridId + ' span img').addClass('deselected');
                            $('div#' + dataGridId + ' span div').addClass('deselected');

                            $(this).removeClass('deselected');
                            $(this).siblings().removeClass('deselected');

                            $(this).addClass('selected');
                            $(this).siblings().addClass('selected');

                            if($selectGrid != null) {
                                $('.' + dataGridId).val("" + optionVal).prop('selected', true).trigger('change');
                                $selectGrid.trigger("select-grid.selected", [optionVal, optionTxt]);
                            }
                        }
                    });
                }
            });
            if(selected_val == "") selected_val = null;

            $(document).trigger("select-grid.created", [selected_val, selected_txt]);
        });
    }
}( jQuery ));


