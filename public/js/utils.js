/* AT SESSION TIMEOUT SEND TO LOGIN */
function checkSessionTimeout(ajax_url, interval) {
    setTimeout(function () {
        $.get(ajax_url, function (data) {
            if (data.success) {
                if (!data.message.logged) {
                    window.location.replace(data.message.url);
                } else {
                    checkSessionTimeout(ajax_url, interval);
                }
            }
        });
    }, interval);
}

function changeStatusToDraft(sectionId) {
    var buttons = $(".section_" + sectionId);
    var item = $('.section-status li a[data-id=' + sectionId + ']');

    item.html("Move to <b>PUBLIC</b>");
    item.attr("data-value", 0);
    $("#section_" + sectionId).html("DRAFT");
    buttons.removeClass("btn-primary");
    buttons.addClass("btn-danger");
}

function changeStatusToPublic(sectionId) {
    var buttons = $(".section_" + sectionId);
    var item = $('.section-status li a[data-id=' + sectionId + ']');

    item.html("Move to <b>DRAFT</b>");
    item.attr("data-value", 1);
    $("#section_" + sectionId).html("PUBLIC");
    buttons.removeClass("btn-danger");
    buttons.addClass("btn-primary");
}

function ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

function initColorPickerDefault(data) {
    $('#global_bg').attr('value', data.color.global_bg);
    $('#cp-global_bg').colorpicker('setValue', data.color.global_bg);
    $('#accent_bg').attr('value', data.color.accent_bg);
    $('#cp-accent_bg').colorpicker('setValue', data.color.accent_bg);
    $('#primary_bg').attr('value', data.color.primary_bg);
    $('#cp-primary_bg').colorpicker('setValue', data.color.primary_bg);
    $('#secondary_bg').attr('value', data.color.secondary_bg);
    $('#cp-secondary_bg').colorpicker('setValue', data.color.secondary_bg);
    $('#light_txt').attr('value', data.color.light_txt);
    $('#cp-light_txt').colorpicker('setValue', data.color.light_txt);
    $('#dark_txt').attr('value', data.color.dark_txt);
    $('#cp-dark_txt').colorpicker('setValue', data.color.dark_txt);
    $('#primary_font').val(data.font.primary);
    $('#primary_font').selectpicker('render');
    $('#secondary_font').val(data.font.secondary);
    $('#secondary_font').selectpicker('render');
}

function setCustomTheme(custom) {
    custom.color.global_bg = $('#global_bg').attr('value');
    custom.color.global_bg = $('#cp-global_bg').colorpicker('getValue', custom.color.global_bg);
    custom.color.accent_bg = $('#accent_bg').attr('value');
    custom.color.accent_bg = $('#cp-accent_bg').colorpicker('getValue', custom.color.accent_bg);
    custom.color.primary_bg = $('#primary_bg').attr('value');
    custom.color.primary_bg = $('#cp-primary_bg').colorpicker('getValue', custom.color.primary_bg);
    custom.color.secondary_bg = $('#secondary_bg').attr('value');
    custom.color.secondary_bg = $('#cp-secondary_bg').colorpicker('getValue', custom.color.secondary_bg);
    custom.color.light_txt = $('#light_txt').attr('value');
    custom.color.light_txt = $('#cp-light_txt').colorpicker('getValue', custom.color.light_txt);
    custom.color.dark_txt = $('#dark_txt').attr('value');
    custom.color.dark_txt = $('#cp-dark_txt').colorpicker('getValue', custom.color.dark_txt);
    custom.font.primary = $('#primary_font').selectpicker('val');
    custom.font.secondary = $('#secondary_font').selectpicker('val');
}

function getLocaleText(lang, text, oldText) {
    var res = null;

    if (text !== null && typeof(text) === "object") {
        res = text[lang];
        if (res === undefined) {
            res = text[Object.keys(text)[0]];
        }
    } else {
        res = oldText;
    }

    return res;
}

function setMultiLanguageObject(lang, oldText, newLocaleText, userLanguages) {
    var res = null;
    if (oldText !== "") {
        try {
            var textObj = JSON.parse(oldText);
            textObj[lang] = newLocaleText.trim();
            res = textObj;
        } catch (e) {
            var newTextObj = {};
            for (var key in userLanguages) {
                if (!userLanguages.hasOwnProperty(key)) continue;

                newTextObj[userLanguages[key]] = newLocaleText.trim();
            }
            res = newTextObj;
        }
    }

    return res;
}

function initFormTab() {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var newTab = "#" + e.target.hash.substr(1) // newly activated tab
        var oldTab = "#" + e.relatedTarget.hash.substr(1) // previous active tab

        deactiveFormValidator(oldTab);
        initFormValidator(newTab);
    });

    $('a[data-toggle="tab"]').each(function () {
        var tabId = $(this).attr('href');
        var errorsCounter = $(tabId + ' .hp_tooltip').length;

        if (errorsCounter > 0) {
            $(this).html('<span class="label label-danger tab-badge" style="top: -2px; position: relative;">' + errorsCounter + '</span>&nbsp;' + $(this).html());
        }
    });
}

function initFormValidator(container) {

    //$('.error-tooltip-left').each(function() {
    //    $(this).after('<a href="#" class="error-tt-tag left" data-toggle="tooltip" data-placement="left" title="'+$(this).text()+'"></a>');
    //});
    //$('.error-tooltip-right').each(function() {
    //    $(this).after('<a href="#" class="error-tt-tag right" data-toggle="tooltip" data-placement="right" title="'+$(this).text()+'"></a>');
    //});

    $(container + ' .hp_tooltip').each(function () {
        var fieldName = $(this).attr("data-related");
        $(this).tooltip({
            container: 'body',
            trigger: "trigger",
            template: '<div class="error_' + fieldName + ' error_tooltip tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
        }).on('shown.bs.tooltip', function () {
            //console.log("SHOWN: "+fieldName);
            $('.error_' + fieldName).unbind('click').bind('click', function () {
                $(this).tooltip('hide');
            });
        }).on('hidden.bs.tooltip', function () {
            $(this).tooltip('destroy');
        }).tooltip('show');

        //$("#"+fieldName).unbind('change').bind('change', function() {
        //    $('.error_'+fieldName).trigger('click');
        //});
    });
}

function deactiveFormValidator(container) {
    $(container + ' .hp_tooltip').tooltip('destroy');
}

function initAvatarUploader(form_id, file_id, hidden_id, button_id, dropZoneTitle, imageBaseurl, uploadUrl) {
    var $form = $(form_id);
    var $input_file = $(file_id);
    var $input_hidden = $(hidden_id);
    var $input_button = $(button_id);
    var $initialPreview = [];

    $input_button.attr("file_selected", "no");

    if ($input_hidden.val() != null && $input_hidden.val() != "") {
        $initialPreview = ["<img src='" + imageBaseurl + $input_hidden.val() + "' class='file-preview-image' alt='Profile' title='Profile' style='width: 100%; height: auto;'>"];
    }

    $input_file.fileinput({
        uploadUrl: uploadUrl, // server upload action
        uploadAsync: true,
        overwriteInitial: true,
        showUpload: false, // hide upload button
        showRemove: false, // hide remove button
        showCaption: false,
        initialPreview: $initialPreview,
        browseClass: "btn btn-default btn-block btn-flat",
        dropZoneTitle: dropZoneTitle,
        layoutTemplates: {
            main2: '{preview} {remove} {browse}',
            actions: '',
            footer: '<div class="file-thumbnail-footer">\n' +
            '    <div class="file-caption-name" style="width:0; height: 0;"></div>\n' +
            '    {progress}\n' +
            '</div>',
        },
        previewTemplates: {
            image: '<div class="file-preview-frame" id="{previewId}" data-fileindex="{fileindex}" style="width: 100%; height: auto;">\n' +
            '   <img src="{data}" class="file-preview-image" title="{caption}" alt="{caption}">\n' +
            '   {footer}\n' +
            '</div>\n',
        },
        allowedFileExtensions: ["jpg", "jpeg", "png"]
    }).on("filebatchselected", function (event, files) {
        $input_button.attr("file_selected", "yes");
    }).on('filecleared', function (event) {
        $input_button.attr("file_selected", "no");
        $input_hidden.val("");
    }).on('fileuploaded', function (event, data, previewId, index) {
        var response = data.response;
        $input_hidden.val(response.file.name);
        $form.submit();
    });

    $input_button.bind('click', function () {
        if ($input_button.attr("file_selected") === "yes") {
            $input_file.fileinput("upload");
        } else {
            $form.submit();
        }
    });
}

function imageLoaded(file_id) {
    var $input_file = $(file_id);
    var $imgContainer = $('form#app .' + $input_file.attr('id') + ' .file-preview-frame');
    var uploaderHeight = $imgContainer.height();
    var uploaderWidth = $imgContainer.width();
    var $initialPreviewContainer = $('.initial-preview-container.' + $input_file.attr('id'));

    $initialPreviewContainer.height(uploaderHeight);
    $initialPreviewContainer.width(uploaderWidth);
}

function getActionsForSubmit() {
    var actions = [];

    var table = $('#content-actions').DataTable();

    table.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var id = $actionsTable.row(rowIdx).data()[1];
        var icon = $actionsTable.row(rowIdx).data()[5];
        var label = $actionsTable.row(rowIdx).data()[8];
        var area = $actionsTable.row(rowIdx).data()[10];
        var type = $actionsTable.row(rowIdx).data()[6];
        var params = $actionsTable.row(rowIdx).data()[7];

        var action = {
            "id": id,
            "icon": icon,
            "label": label,
            "type": type,
            "area": area,
            "params": params
        };

        actions.push(action);
    });

    $('#actions').val(JSON.stringify(actions));
}

function initAsyncUploader(file_id, hidden_id, dropZoneTitle, imageBaseurl, uploadUrl, button_id, button_callback, browsed_callback, maxFileSize) {
    var $input_file = $(file_id);
    var $input_hidden = $(hidden_id);
    var $input_button = $(button_id);
    var $initialPreview = [];

    $input_button.attr("file_selected", "no");

    if ($input_hidden.val() !== null && $input_hidden.val() !== "") {
        $initialPreview = [
            '<div class="initial-preview-container ' + $input_file.attr('id') + '" style="background: url(' + imageBaseurl + $input_hidden.val() + ') center center no-repeat, url(/img/placeholder/square-pattern.jpg) left top repeat;background-size: cover, 8px;overflow: hidden;width: 100%; height: auto;">' +
            '<img onload="imageLoaded(\'' + file_id + '\')" src="' + imageBaseurl + $input_hidden.val() + '" class="file-preview-image" alt="Profile" title="Profile" style="max-height: 100%;max-width: 100%;-ms-filter: \'progid:DXImageTransform.Microsoft.Alpha(Opacity=0)\';filter: alpha(opacity=0);opacity: 0;">' +
            '</div>'];
        // $initialPreview = [ imageBaseurl + $input_hidden.val() ];
    }

    $input_file.fileinput({
        uploadUrl: uploadUrl, // server upload action
        uploadAsync: true,
        overwriteInitial: true,
        showUpload: false, // hide upload button
        showRemove: false, // hide remove button
        showCaption: false,
        initialPreviewAsData: false,
        initialPreview: $initialPreview,
        browseClass: "btn btn-default btn-block btn-flat",
        dropZoneTitle: dropZoneTitle,
        maxFileSize: (maxFileSize === null || maxFileSize === undefined) ? 2048 : maxFileSize,
        layoutTemplates: {
            main2: '{preview} {remove} {browse}',
            actions: '',
            footer: '<div class="file-thumbnail-footer">\n' +
            '    <div class="file-caption-name" style="width:0; height: 0;"></div>\n' +
            '    {progress}\n' +
            '</div>',
        },
        previewTemplates: {
            image: '<div class="file-preview-frame" id="{previewId}" data-fileindex="{fileindex}" style="width: 100%; height: auto;">\n' +
            '   <img src="{data}" class="file-preview-image" title="{caption}" alt="{caption}">\n' +
            '   {footer}\n' +
            '</div>\n',
        },
        allowedFileExtensions: ["jpg", "jpeg", "png"]
    }).on("filebatchselected", function (event, files) {
        $input_button.attr("file_selected", "yes");
        if (browsed_callback != null) browsed_callback();
    }).on('filecleared', function (event) {
        $input_button.attr("file_selected", "no");
        $input_hidden.val("");
    }).on('fileuploaded', function (event, data, previewId, index) {
        var response = data.response.file;
        $input_hidden.val(response.name);
        button_callback();
    });

    $input_button.bind('click', function () {
        if ($input_button.attr("file_selected") === "yes") {
            $input_file.fileinput("upload");
        } else {
            button_callback();
        }
    });
}

function initAsyncUploaderId(file_id, hidden_id, hidden_name, dropZoneTitle, imageBaseurl, uploadUrl, button_id, button_callback, browsed_callback, maxFileSize) {
    var $input_file = $(file_id);
    var $input_hidden = $(hidden_id);
    var $input_hidden_name = $(hidden_name);
    var $input_button = $(button_id);
    var $initialPreview = [];

    $input_button.attr("file_selected", "no");

    if ($input_hidden_name.val() !== null && $input_hidden_name.val() !== "") {
        $initialPreview = [
            '<div class="initial-preview-container ' + $input_file.attr('id') + '" style="background: url(' + imageBaseurl + $input_hidden_name.val() + ') center center no-repeat, url(/img/placeholder/square-pattern.jpg) left top repeat;background-size: cover, 8px;overflow: hidden;width: 100%; height: auto">' +
            '<img onload="imageLoaded(\'' + file_id + '\')" src="' + imageBaseurl + $input_hidden_name.val() + '" class="file-preview-image" alt="Profile" title="Profile" style="max-height: 100%;max-width: 100%;-ms-filter: \'progid:DXImageTransform.Microsoft.Alpha(Opacity=0)\';filter: alpha(opacity=0);opacity: 0;">' +
            '</div>'];
    }

    var fileInputOptions = {
        uploadUrl: uploadUrl, // server upload action
        uploadAsync: true,
        overwriteInitial: true,
        showUpload: false, // hide upload button
        showRemove: false, // hide remove button
        showCaption: false,
        initialPreview: $initialPreview,
        browseClass: "btn btn-default btn-block btn-flat",
        maxFileSize: (maxFileSize === null || maxFileSize === undefined) ? 2048 : maxFileSize,
        layoutTemplates: {
            main2: '{preview} {remove} {browse}',
            actions: '',
            footer: '<div class="file-thumbnail-footer">\n' +
            '    <div class="file-caption-name" style="width:0; height: 0;"></div>\n' +
            '    {progress}\n' +
            '</div>',
        },
        allowedFileExtensions: ["jpg", "jpeg", "png"]
    };

    if (dropZoneTitle === "+") {
        dropZoneTitle = '<i class="fa fa-plus fa-3x" aria-hidden="true" style="margin-top: 5px; color: #aaa;"></i>';
        fileInputOptions.showBrowse = false;
        fileInputOptions.browseOnZoneClick = true;
        fileInputOptions.defaultPreviewContent = dropZoneTitle;
        fileInputOptions.previewTemplates = {
            image: '<div class="file-preview-frame" id="{previewId}" data-fileindex="{fileindex}" style="height: 50px;">\n' +
            '   <img src="{data}" class="file-preview-image" title="{caption}" alt="{caption}" style="width: 100%; height: 50px;">\n' +
            '   {footer}\n' +
            '</div>\n'
        };
    } else {
        fileInputOptions.dropZoneTitle = dropZoneTitle;
        fileInputOptions.previewTemplates = {
            image: '<div class="file-preview-frame" id="{previewId}" data-fileindex="{fileindex}" style="width: 100%; height: auto;">\n' +
            '   <img src="{data}" class="file-preview-image" title="{caption}" alt="{caption}">\n' +
            '   {footer}\n' +
            '</div>\n'
        };
    }

    $input_file.fileinput(fileInputOptions).on("filebatchselected", function (event, files) {
        $input_button.attr("file_selected", "yes");
        if (browsed_callback != null) browsed_callback();
    }).on('filecleared', function (event) {
        $input_button.attr("file_selected", "no");
        $input_hidden.val("");
        $input_hidden_name.val("");
    }).on('fileuploaded', function (event, data, previewId, index) {
        var response = data.response.file;
        $input_hidden.val(response.media_id);
        $input_hidden_name.val(response.name);

        button_callback();
    });

    $input_button.bind('click', function () {
        if ($input_button.attr("file_selected") === "yes") {
            $input_file.fileinput("upload");
        } else {
            button_callback();
        }
    });
}

function initAppExtraIconsUploader(file_id, hidden_id, dropZoneTitle, imageBaseurl, uploadUrl, button_id, button_callback, browsed_callback, maxFileSize) {
    var $input_file = $(file_id);
    var $input_hidden = $("#" + hidden_id);
    var $input_button = $(button_id);
    var $initialPreview = [];

    // $input_button.attr("file_selected", "no");

    if ($input_hidden.val() !== null && $input_hidden.val() !== "") {
        $initialPreview = ["<img src='" + imageBaseurl + $input_hidden.val() + "' class='file-preview-image' alt='Profile' title='Profile' style='background: url(" + imageBaseurl + $input_hidden.val() + ") center center no-repeat, url(/img/placeholder/square-pattern.jpg) left top repeat;background-size: cover, 8px;width: 100%; height: auto;'>"];
    }

    $input_file.fileinput({
        uploadUrl: uploadUrl, // server upload action
        uploadAsync: true,
        overwriteInitial: true,
        showUpload: false, // hide upload button
        showRemove: false, // hide remove button
        showCaption: false,
        showBrowse: false, // hide browse button
        initialPreview: $initialPreview,
        defaultPreviewContent: dropZoneTitle,
        browseOnZoneClick: true,
        maxFileSize: (maxFileSize === null || maxFileSize === undefined) ? 2048 : maxFileSize,
        layoutTemplates: {
            main2: '{preview} {remove} {browse}',
            actions: '',
            footer: '<div class="file-thumbnail-footer">\n' +
            '    <div class="file-caption-name" style="width:0; height: 0;"></div>\n' +
            '    {progress}\n' +
            '</div>',
        },
        previewTemplates: {
            image: '<div class="file-preview-frame" id="{previewId}" data-fileindex="{fileindex}" style="height: 50px;">\n' +
            '   <img src="{data}" class="file-preview-image" title="{caption}" alt="{caption}" style="width: 100%; height: 50px;">\n' +
            '   {footer}\n' +
            '</div>\n',
        },
        allowedFileExtensions: ["jpg", "jpeg", "png"]
    }).on("filebatchselected", function (event, files) {
        $input_button.attr("file_selected", "yes");
        if (browsed_callback != null) browsed_callback(hidden_id);
    }).on('filecleared', function (event) {
        $input_button.attr("file_selected", "no");
        $input_hidden.val("");
    }).on('fileuploaded', function (event, data, previewId, index) {
        var response = data.response;
        $input_hidden.val(response.file.name);
        button_callback(hidden_id);
    });

    $input_button.bind('click', function () {
        if ($input_button.attr("file_selected") === "yes") {
            $input_file.fileinput("upload");
        } else {
            button_callback(hidden_id);
        }
    });
}

function formatTwitterHashtag(tag) {
    //var tag = e.attrs.value;//.replace(/\s/g, '');
    tag = tag.replace(/\s/g, '');
    tag = tag.replace(/-/g, '');
    tag = tag.replace(/#/g, '');
    tag = tag.replace(/@/g, '');

    if (tag[0] !== "#") {
        tag = "#" + tag;
    }

    return tag;
}

function dateToUnixTimestamp(date) {
    return Math.round(new Date(date).getTime() / 1000)
}

function unixTimestampToString(timestamp) {
    var date = new Date(parseInt(timestamp) * 1000);
    var month = date.getMonth() + 1;
    var day = date.getDate();

    var hour = ("0" + date.getHours()).slice(-2);
    var min = ("0" + date.getMinutes()).slice(-2);

    if (month < 10) month = "0" + month;
    if (day < 10) day = "0" + day;

    return date.getFullYear() + "-" + month + "-" + day + " " + hour + ":" + min + ":00";
}

function initDatePicker($date_picker, date_range_box_id, date, auto_start, drops, with_time, callback) {
    var format = 'YYYY-MM-DD HH:mm';
    var timePicker = true;
    var timePicker24 = true;

    auto_start = (typeof auto_start !== 'undefined') ? auto_start : false;
    drops = (typeof drops !== 'undefined') ? drops : "down";
    with_time = (typeof with_time !== 'undefined') ? with_time : true;
    callback = (typeof callback !== 'undefined') ? callback : function (start, end, label) {
    };

    if (!with_time) {
        format = 'YYYY-MM-DD';
        timePicker = false;
        timePicker24 = false;
    }

    $date_picker.unbind('click');
    $date_picker.daterangepicker({
        parentEl: "#" + date_range_box_id,
        singleDatePicker: true,
        timePicker: timePicker,
        timePickerIncrement: 30,
        timePicker24Hour: timePicker24,
        timePickerSeconds: false,
        drops: drops,
        locale: {
            format: format
        },
        startDate: date,
        autoApply: true
    }, function (start, end, label) {
        callback(start, end, label);
    });

    if (auto_start) {
        $date_picker.data('daterangepicker').show();
    }
}

function initDateRangePicker(auto_start, date_range_id, date_range_box_id, start_id, end_id, start_date, end_date, drops) {

    var $dateRangeId = $('#' + date_range_id);
    var $startId = $("#" + start_id);
    var $endId = $("#" + end_id);

    drops = drops || "down";

    $dateRangeId.unbind('click');
    $dateRangeId.daterangepicker({
        parentEl: "#" + date_range_box_id,
        timePicker: true,
        timePickerIncrement: 30,
        timePicker24Hour: true,
        timePickerSeconds: false,
        drops: drops,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        },
        autoApply: true
    }, function (start, end, label) {
        $startId.val(unixTimestampToString("" + start.unix()));
        $endId.val(unixTimestampToString("" + end.unix()));
    });

    $dateRangeId.data('daterangepicker').setStartDate(start_date);
    $dateRangeId.data('daterangepicker').setEndDate(end_date);

    if (auto_start) {
        $startId.val(start_date);
        $endId.val(end_date);
        $dateRangeId.data('daterangepicker').show();
    }

}

function initNotificationModal(remote_url, submit_url, app_id) {
    $('.new-notification').click(function () {
        var title = "&nbsp;&nbsp;<span class='fa fa-bell'></span>&nbsp;&nbsp;NOTIFICATION";
        var footer = '<div class="row notification-btn">' +
            '<div class="col-lg-6 col-md-6 col-xs-6 pull-left">' +
            '<button type="button" name="dismiss" id="dismiss" class="btn btn-default pull-right" data-dismiss="modal" value="DISMISS">DISMISS</button>' +
            '</div>' +
            '<div class="col-lg-6 col-md-6 col-xs-6 pull-right">' +
            '<button type="button" name="send" id="send" class="btn btn-success pull-left" disabled="disabled" value="SEND">SEND</button>' +
            '</div>' +
            '</div>';

        showRemoteModal(title, footer, remote_url,
            {
                //"onAddCallback": null,
            },
            function () {
                $('.notification-btn #send').click(function () {

                    $.ajax({
                        url: submit_url,
                        type: 'post',
                        data: $("#notification").serialize(),
                        headers: {
                            "App-Id": app_id
                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data.success === true) {
                                $('#remoteDialogBox').modal('hide');
                            } else {
                                // Display error
                                $('#push-notification-error-box').show();
                                $('#push-notification-error-message').text(data.message);
                            }
                            // console.log(data);
                        }
                    });
                });
            },
            function () {
                console.log('hide');
            },
            ""
        );
    });
}

// http://locutus.io/php/url/base64_decode/
function base64_decode(encodedData) {
    if (typeof window !== 'undefined') {
        if (typeof window.atob !== 'undefined') {
            return decodeURIComponent(escape(window.atob(encodedData)));
        }
    } else {
        return new Buffer(encodedData, 'base64').toString('utf-8');
    }

    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var o1;
    var o2;
    var o3;
    var h1;
    var h2;
    var h3;
    var h4;
    var bits;
    var i = 0;
    var ac = 0;
    var dec = '';
    var tmpArr = [];

    if (!encodedData) {
        return encodedData;
    }

    encodedData += '';

    do {
        // unpack four hexets into three octets using index points in b64
        h1 = b64.indexOf(encodedData.charAt(i++));
        h2 = b64.indexOf(encodedData.charAt(i++));
        h3 = b64.indexOf(encodedData.charAt(i++));
        h4 = b64.indexOf(encodedData.charAt(i++));

        bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

        o1 = bits >> 16 & 0xff;
        o2 = bits >> 8 & 0xff;
        o3 = bits & 0xff;

        if (h3 === 64) {
            tmpArr[ac++] = String.fromCharCode(o1);
        } else if (h4 === 64) {
            tmpArr[ac++] = String.fromCharCode(o1, o2);
        } else {
            tmpArr[ac++] = String.fromCharCode(o1, o2, o3);
        }
    } while (i < encodedData.length);

    dec = tmpArr.join('');

    return decodeURIComponent(escape(dec.replace(/\0+$/, '')));
}

// http://locutus.io/php/url/base64_encode/
function base64_encode(stringToEncode) {
    if (typeof window !== 'undefined') {
        if (typeof window.btoa !== 'undefined') {
            return window.btoa(unescape(encodeURIComponent(stringToEncode)));
        }
    } else {
        return new Buffer(stringToEncode).toString('base64');
    }

    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var o1;
    var o2;
    var o3;
    var h1;
    var h2;
    var h3;
    var h4;
    var bits;
    var i = 0;
    var ac = 0;
    var enc = '';
    var tmpArr = [];

    if (!stringToEncode) {
        return stringToEncode;
    }

    stringToEncode = unescape(encodeURIComponent(stringToEncode));

    do {
        // pack three octets into four hexets
        o1 = stringToEncode.charCodeAt(i++);
        o2 = stringToEncode.charCodeAt(i++);
        o3 = stringToEncode.charCodeAt(i++);

        bits = o1 << 16 | o2 << 8 | o3;

        h1 = bits >> 18 & 0x3f;
        h2 = bits >> 12 & 0x3f;
        h3 = bits >> 6 & 0x3f;
        h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        tmpArr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < stringToEncode.length);

    enc = tmpArr.join('');

    var r = stringToEncode.length % 3;

    return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
}

function createToken(e, tagElem, errorElem) {
    var enabled = true;
    var tokens = tagElem.tokenfield('getTokens');

    if (e.attrs.value.length >= 4) {
        for (var i = 0; i < tokens.length; i++) {
            if (tokens[i].value === e.attrs.value) {
                enabled = false;
            }
        }
    } else {
        errorElem.html("Tag must be at leas 4 chars");
        enabled = false;
    }

    if (enabled) {
        errorElem.html("");

        if (e.attrs.id === undefined || e.attrs.id === null) {
            var tag = formatTwitterHashtag(e.attrs.value);
            if (tag !== e.attrs.value) {
                e.preventDefault();
                tagElem.tokenfield('createToken', tag);
            }
        }
    } else {
        e.preventDefault();
    }
}

function createExtraParamRow(counter, key, value) {
    return '<div class="row extra_param_row">' +
        '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 no-right-pad">' +
        '<div class="form-group has-feedback">' +
        '<input type="text" id="key-param_' + counter + '" class="form-control key-param" value="' + key + '" placeholder="' + key + '">' +
        '</div>' +
        '</div>' +
        '<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 no-lateral-pad">' +
        '<div class="form-group has-feedback">' +
        '<input type="text" id="value-param_' + counter + '" class="form-control value-param" value="' + value + '" placeholder="' + value + '">' +
        '</div>' +
        '</div>' +
        '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 no-left-pad">' +
        '<button type="button" name="remove-param" id="remove-param_' + counter + '" class="btn btn-danger btn-block btn-flat">' +
        '<i class="fa fa-minus" aria-hidden="true"></i>' +
        '</button>' +
        '</div>' +
        '</div>';
}

function initParamInputOptions(container_id, extra_params_hidden_id, counter) {
    $('#remove-param_' + counter).on('click', function () {
        $(this).closest(container_id + ' .extra_param_row').remove();
        extraParamsAssign(container_id, extra_params_hidden_id);
    });

    $('#key-param_' + counter).keyup(function () {
        extraParamsAssign(container_id, extra_params_hidden_id);
    });

    $('#value-param_' + counter).keyup(function () {
        extraParamsAssign(container_id, extra_params_hidden_id);
    });
}

function extraParamsAssign(container_id, extra_params_hidden_id) {
    var params = [];
    $(container_id + ' .extra_param_row').each(function () {
        var key = $(this).find('.key-param').val();
        var val = $(this).find('.value-param').val();
        params.push({key: key, value: val});
    });

    console.log(JSON.stringify(params));

    $(extra_params_hidden_id).val(JSON.stringify(params));
}

/*
var str = "TEST";
var str_encode = base64_encode(str);
var str_decode = base64_decode(str_encode);
console.log(str);
console.log(str_encode);
console.log(str_decode);
*/