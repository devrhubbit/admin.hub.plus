<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 13:02
 */

namespace RestHook\Exception;


use Rest\Exception\RestSecurityException;

class RestHookInvalidHashIdException extends RestSecurityException
{
    function __constructor() {
        $code = RestSecurityException::INVALID_HASH_ID;
        $msg  = RestSecurityException::MESSAGE_HASH_ID;

        parent::__construct($msg, $code);
    }

    public function getErrorMessage() {
        return RestSecurityException::MESSAGE_HASH_ID;
    }
}