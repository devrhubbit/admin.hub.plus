<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 28/07/2019
 * Time: 18:36
 */

namespace RestHook\Model;


use Application\Model\SectionModel;
use Database\HubPlus\AddressQuery;
use Database\HubPlus\UserAppQuery;
use Rest\Model\HpRestModel;

class FormRestModel extends HpRestModel
{

    private static $sqlFormRowById = "SELECT * FROM custom_form_%s WHERE id=:id;";
    private static $sqlFormList = "SELECT * FROM custom_form_%s LIMIT :offset, :limit";



    /**
     * @param $section_id
     * @param int $page
     * @param int $limit
     * @param null $filter
     * @param null $order
     *
     * TODO: implement ORDER BY and WHERE on query list
     */
    public function getRowList($section_id, $limit = 5, $offset = 0) {
        $response = null;

        if (!is_null($this->db)) {
            $stmt = $this->db->prepare(sprintf(self::$sqlFormList, $section_id));

            $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
            $stmt->bindParam(":offset", $offset, \PDO::PARAM_INT);

            if ($stmt->execute()) {
                $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

                if($rows) {
                    $data = array();
                    foreach ($rows as $row){
                        $data[] = $this->parseFormRow($section_id, $row);
                    }
                    $response = $this->getSimpleJsonResponse(200, $data);
                }
            }
        }

        return $response;
    }

    /**
     * @param $section_id
     * @param $row_id
     */
    public function getRowById($section_id, $row_id) {
        $response = null;

        if (!is_null($this->db)) {
            $stmt = $this->db->prepare(sprintf(self::$sqlFormRowById, $section_id));
            $stmt->bindParam(":id", $row_id, \PDO::PARAM_INT);

            if ($stmt->execute()) {
                $row = $stmt->fetchObject();

                if($row) {
                    $data = $this->parseFormRow($section_id, $row);
                    $response = $this->getJsonResponse(200, $data);
                }
            }
        }

        return $response;
    }

    /**
     * @param $section_id
     * @param $row
     * @return \stdClass
     *
     * TODO: manage cover and gallery field
     */
    private function parseFormRow($section_id, $row) {
        $data = new \stdClass();

        //GET FORM SECTION SETTINGS
        $sectionModel = new SectionModel($this->getController()->getServiceLocator());
        $formSettings = json_decode($sectionModel->getSectionByPk($section_id)->getParams());

        $fields = array();

        foreach ($formSettings->fieldsets as $fieldset) {
            $fields = array_merge($fields, $fieldset->fields);
        }

        $invalidTypes = array(SectionModel::HPFORM_INPUT_DESCR, SectionModel::HPFORM_IMAGE);
        $parsedFields = array();

        foreach ($fields as $field) {
            if(!in_array($field->type, $invalidTypes)) {
                $parsedFields[$field->name] = $field->type;
            }
        }

        //WRAP FORM DATA
        $data->id = $row->id;

        if(isset($row->user_id) && !is_null($row->user_id)) {
            $user = UserAppQuery::create()->findPk($row->user_id);
            if($user) {
                $data->username = $user->getEmail();
            }
        }

        foreach ($parsedFields as $fieldName => $type) {
            $value = $row->{base64_encode($fieldName)};

            if($type !== SectionModel::HPFORM_ADDRESS) {
                $data->{$fieldName} = $value;
            } else {
                if(!is_null($value)) {
                    $address = AddressQuery::create()->findPk($value);
                    $addressParts = array(
                        $address->getAddress(),
                        $address->getCity(),
                        $address->getState(),
                        $address->getCountry(),
                        $address->getZipcode(),
                    );

                    $data->{$fieldName} = implode(" ", $addressParts);
                }
            }
        }

        return $data;
    }

}