<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 20/07/2019
 * Time: 17:35
 */

namespace RestHook\Model;


use Database\HubPlus\PostQuery;
use Database\HubPlus\RemoteProvider;
use Database\HubPlus\RemoteProviderQuery;
use Database\HubPlus\SectionQuery;
use Database\HubPlus\Webhook;
use Database\HubPlus\WebhookQuery;
use Rest\Model\HpRestModel;
use RestHook\Exception\RestHookInvalidHashIdException;
use RestHook\Exception\RestHookUuidSecurityException;

class HookRestModel extends HpRestModel
{

    /* @var $provider RemoteProvider */
    private $provider = null;

    /* @var $webHook Webhook */
    private $webHook = null;

    /**
     * @return string
     * @throws RestHookUuidSecurityException
     */
    private function getProviderUuid()
    {
        $providerUuid = null;

        if ($this->controller->getRequest()->getHeader(self::PROVIDER_UUID)) {
            $providerUuid = $this->controller->getRequest()->getHeader(self::PROVIDER_UUID)->getFieldValue();
        } else {
            throw new RestHookUuidSecurityException();
        }

        return $providerUuid;
    }


    private function getHashId()
    {
        $hashId = null;

        if ($this->controller->getRequest()->getHeader(self::HASH_ID)) {
            $hashId = $this->controller->getRequest()->getHeader(self::HASH_ID)->getFieldValue();
        } else {
            throw new RestHookUuidSecurityException();
        }

        return $hashId;
    }

    /**
     * @param bool $withHashIdCheck
     * @return bool
     * @throws RestHookInvalidHashIdException
     * @throws RestHookUuidSecurityException
     */
    public function isValidHookRequest($withHashIdCheck = true) {
        $res = false;
        $uuid = $this->getProviderUuid();

        $this->provider = RemoteProviderQuery::create()->filterByUuid($uuid)->findOne();

        if (isset($this->provider)) {
            if($withHashIdCheck) {
                $hashId = $this->getHashId();
                $this->webHook = WebhookQuery::create()
                    ->filterByHashId($hashId)
                    ->filterByProviderId($this->provider->getId())
                    ->findOne();

                if (isset($this->webHook)) {
                    $res = true;
                } else {
                    throw new RestHookInvalidHashIdException();
                }
            } else {
                $res = true;
            }
        } else {
            throw new RestHookUuidSecurityException();
        }

        return $res;
    }


    /**
     * @param $hashId
     * @return \Database\HubPlus\Post|mixed|null
     * @throws RestHookInvalidHashIdException
     */
    private function getPostByHashId($hashId) {
        $result = null;

        list($prefix, $realHashId) = explode("-", $hashId);

        if($prefix == self::SCT_HASH_PREFIX) {
            $result = PostQuery::create()
                ->where('MD5(Post.Id) = ?', $realHashId)
                ->findOne();
        } else {
            throw new RestHookInvalidHashIdException();
        }

        return $result;
    }

    /**
     * @param $hashId
     * @return \Database\HubPlus\Section|mixed|null
     * @throws RestHookInvalidHashIdException
     */
    private function getSectionByHashId($hashId) {
        $result = null;

        list($prefix, $realHashId) = explode("-", $hashId);

        if($prefix == self::SCT_HASH_PREFIX) {
            $result = SectionQuery::create()
                ->where('MD5(Section.Id) = ?', $realHashId)
                ->findOne();
        } else {
            throw new RestHookInvalidHashIdException();
        }

        return $result;
    }

    public function me() {
        $response = null;

        if($this->isValidHookRequest(false)) {
            $data = new \stdClass();

            $data->name = $this->provider->getName();
            $data->email = $this->provider->getEmail();
            $data->site = $this->provider->getSite();
            $data->created_at = $this->provider->getCreatedAt();

            $response = $this->getJsonResponse(200, $data);
        }

        return $response;
    }

    /**
     * @param $type
     * @param $hashId
     * @param $bindTo
     * @param $targetUrl
     * @return null|string
     * @throws RestHookInvalidHashIdException
     */
    public function subscribe($type, $hashId, $bindTo, $targetUrl) {
        $response = null;

        if($this->isValidHookRequest(false)) {
            $data = new \stdClass();

            list($prefix, $realHashId) = explode("-", $hashId);
            $postId = null;
            $sectionId = null;

            $config = $this->controller->getServiceLocator()->get('config');
            $validTypes = $config['resthook_valid_type'];

            switch ($prefix) {
                case HpRestModel::CNT_HASH_PREFIX:
                    $postId = in_array($type, $validTypes[$prefix]) ? $this->getPostByHashId($hashId)->getId() : null;
                    break;
                case HpRestModel::SCT_HASH_PREFIX:
                    $sectionId = in_array($type, $validTypes[$prefix]) ? $this->getSectionByHashId($hashId)->getId() : null;
                    break;
                default:
                    throw new RestHookInvalidHashIdException();
            }

            if(is_null($postId) && is_null($sectionId)) {
                throw new RestHookInvalidHashIdException();
            }

            $webhook = WebhookQuery::create()
                ->filterByProviderId($this->provider->getId())
                ->filterByHashId($hashId)
                ->filterByBindTo($bindTo)
                ->filterByUrl($targetUrl)
                ->findOneOrCreate();

            $webhook->setProviderId($this->provider->getId())
                ->setHashId($hashId)
                ->setPostId($postId)
                ->setSectionId($sectionId)
                ->setBindTo($bindTo)
                ->setUrl($targetUrl)
                ->save();

            $data->id = $webhook->getId();
            $data->resource_id = (is_null($postId) ? 0 : $postId) + (is_null($sectionId) ? 0 : $sectionId);
            $data->type = $type;
            $data->bind = $bindTo;
            $data->url = $targetUrl;
            $data->provider = $this->provider->getName();
            $data->created_at =  date(DATE_ATOM, $webhook->getCreatedAt()->getTimestamp());

            $response = $this->getJsonResponse(200, $data);
        }

        return $response;
    }

    /**
     * @param $type
     * @param $subscriptionId
     * @return null|string
     */
    public function unsubscribe($type, $subscriptionId) {
        $response = null;

        if($this->isValidHookRequest(false)) {
            $data = new \stdClass();
            $webhook = WebhookQuery::create()->findPk($subscriptionId);

            if (!is_null($webhook) ) {
                $hashId = $webhook->getHashId();
                $bindTo = $webhook->getBindTo();
                $url = $webhook->getUrl();

                list($prefix, $realHashId) = explode("-", $hashId);

                $config = $this->controller->getServiceLocator()->get('config');
                $validTypes = $config['resthook_valid_type'];

                if (in_array($type, $validTypes[$prefix])) {
                    $webhook->delete();

                    $data->hash_id = $hashId;
                    $data->type = $type;
                    $data->bind = $bindTo;
                    $data->url = $url;

                    $response = $this->getJsonResponse(200, $data);
                } else {
                    $response = $this->getJsonResponse(412, null, null, "Invalid Subscription TYPE");
                }
            } else {
                $response = $this->getJsonResponse(412, null, null, "No subscription found with ID: ".$subscriptionId);
            }
        }

        return $response;
    }


    public function sampleRedirect($type) {
        $response = null; //$this->getJsonResponse(203);

        if($this->isValidHookRequest(false)) {
            switch ($type) {
                case 'form':
                    $section = $this->getSectionByHashId($this->getHashId());
                    $sectionId = $section->getId();
                    $response = $this->controller->redirect()->toRoute('form_hook/get',
                        array("fid" => $sectionId, "rid" => 0)
                    );
                    break;
            }
        }

        return $response;
    }

}