<?php

/**
 * Created by PhpStorm.
 * Form: mirco
 * Date: 27/07/2019
 * Time: 16:57
 */

namespace RestHook\Trigger\Form;


use RestHook\Trigger\Trigger;
use Zend\EventManager\Event;
use Zend\Http\Request;

class CreatedFormTrigger extends Trigger
{

    public function append(Event $e)
    {
        $payload = new \stdClass();
        $payload->id = $e->getParam('row_id');
        $payload->username = $e->getParam('username');

        $data = $e->getParam('data');
        foreach ($data as $field) {
            $value = null;
            if(is_string($field->value)) {
                $value = $field->value;
            } else {
                if(is_array($field->value)) {
                    $value = implode(" ", $field->value);
                } else {
                    if(is_object($field->value)) {
                        $value = implode(" ", ((array) $field->value));
                    }
                }
            }

            $payload->{$field->label} = $value;
        }

        $this->setSectionId($e->getParam('section_id'));
        $this->enqueue($e->getName(), $payload, Trigger::MODE_MERGE, Request::METHOD_POST, 3);
    }
}