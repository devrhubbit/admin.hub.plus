<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 27/07/2019
 * Time: 00:24
 */

namespace RestHook\Trigger;


use Database\HubPlus\WebhookQuery;
use Database\HubPlus\WebhookQueue;
use Database\HubPlus\WebhookQueueQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;
use Rest\Model\HpRestModel;
use RestHook\Exception\TriggerHookException;
use Zend\EventManager\Event;
use Zend\Http\Client;
use Zend\Http\Headers;
use Zend\Http\PhpEnvironment\Request;


class Trigger
{
    const MODE_MERGE = 0;
    const MODE_REPLACE = 1;
    const MODE_APPEND = 2;

    const STATUS_SYNC_TODO = 0;
    const STATUS_SYNC_ERROR = 100;
    const STATUS_SYNC_IN_PROGRESS = 200;
    const STATUS_SYNC_DONE = 300;

    private $postId = null;
    private $sectionId = null;

    public function append(Event $e) {}

    /**
     * @param $event - the event bundle name
     * @param $payload - data to sync
     * @param null $maxAttempts - max numbers of attemts in case of remote listener error (if NULL get data form webhook subscription settings)
     * @param null $toSyncAt - timestamp to define real sync time event (if NULL get data form webhook subscription settings)
     *
     * @throws TriggerHookException
     */
    protected function enqueue($event, $payload, $mode = self::MODE_APPEND, $method = Request::METHOD_POST, $maxAttempts = null, $toSyncAt = null) {
        try {
            $webhooks = WebhookQuery::create()
                ->filterByEnabled(1)
                ->filterByPostId($this->getPostId())
                ->filterBySectionId($this->getSectionId())
                ->filterByBindTo("%".$event."%", Criteria::LIKE)
                ->find();

            foreach ($webhooks as $webhook) {
                $bundle = $this->generateBundle($event);

                $webhookQueue = null;

                if($mode == self::MODE_APPEND) {
                    $webhookQueue = new WebhookQueue();
                } else {
                    $webhookQueue = WebhookQueueQuery::create()
                        ->filterByWebhookId($webhook->getId())
                        ->filterByBundle($bundle)
                        ->filterByStatus(self::STATUS_SYNC_IN_PROGRESS, Criteria::LESS_THAN)
                        ->where('WebhookQueue.failed_attempts < WebhookQueue.max_attempts')
                        ->orderByToSyncAt()
                        ->findOneOrCreate();
                }

                $payloads = null;

                switch ($mode) {
                    case self::MODE_APPEND: //Nothing to wrap, we are simply append a single payload to queue
                        $payloads = $payload;
                        break;
                    case self::MODE_MERGE: //Merge the old payloads stored to queue with the new one
                        if(
                            !is_null($webhookQueue) &&
                            !is_null($webhookQueue->getPayload()) &&
                            $webhookQueue->getPayload() !== ""
                        ) {
                            $payloads = json_decode($webhookQueue->getPayload());
                        }
                        $payloads []= $payload;
                        break;
                    case self::MODE_REPLACE: //Replace the old payloads stored to queue with the new one
                        $payloads = array();
                        $payloads []= $payload;
                        break;
                    default:
                        throw new TriggerHookException("Invale MODE param: ".$mode);
                }

                $webhookQueue
                    ->setWebhookId($webhook->getId())
                    ->setMethod($method)
                    ->setUrl($webhook->getUrl())
                    ->setStatus(self::STATUS_SYNC_TODO)
                    ->setBundle($bundle)
                    ->setFailedAttempts(0)
                    ->setMaxAttempts( is_null($maxAttempts) ? $webhook->getMaxAttempts() : $maxAttempts )
                    ->setPayload(json_encode($payloads))
                    ->setToSyncAt( is_numeric($toSyncAt) ? $toSyncAt : time() + $webhook->getSecondsDelay())
                    ->save();
            }
        } catch (PropelException $e) {
            $message = is_null($e->getPrevious()) ? $e->getMessage() : $e->getPrevious()->getMessage();
            throw new TriggerHookException("Failed to enqueue $event - ".$message);
        } catch (\Exception $e) {
            //print_r($e->getPrevious()->getMessage()); die();
            throw new TriggerHookException("Failed to enqueue $event - ".$e->getMessage());
        }

    }

    /**
     * @param int $limit
     * @param bool $verbose
     * @throws TriggerHookException
     */
    public function dequeue($limit = 5, $verbose = false) {
        try {
            $this->consoleLog("TRIGGER.DEQUEUE","Extracting data from queue...", $verbose);

            $webhooksQueue = WebhookQueueQuery::create()
                ->filterByStatus(self::STATUS_SYNC_IN_PROGRESS, Criteria::LESS_THAN)
                ->filterByToSyncAt('now', Criteria::LESS_EQUAL)
                ->where('WebhookQueue.failed_attempts < WebhookQueue.max_attempts')
                ->limit($limit)
                ->orderByToSyncAt()
                ->find();

            $ids = array();
            foreach ($webhooksQueue as $webhookQueue) {
                $ids[]=$webhookQueue->getId();
                $webhookQueue->setStatus(self::STATUS_SYNC_IN_PROGRESS);
            }

            WebhookQueueQuery::create()
                ->filterById($ids)
                ->update(array('Status' => self::STATUS_SYNC_IN_PROGRESS));

            $this->consoleLog("TRIGGER.DEQUEUE","Extracted ".count($ids)." webhooks from queue", $verbose);

            $progress = 0;
            $processedSuccess = 0;
            $processedError = 0;

            foreach ($webhooksQueue as $webhookQueue) {
                $progress++;
                $this->consoleLog("TRIGGER.DEQUEUE","> (".$progress."/".count($ids).") request in progress", $verbose);

                $method = $webhookQueue->getMethod();
                $url = $webhookQueue->getUrl();
                $payload = $webhookQueue->getPayload();

                $response = $this->send($method, $url, $payload);

                $log = "UNDEFINED RESULT";

                if($response->getStatusCode() < 400) {
                    $processedSuccess++;
                    $log = "SUCCESS";
                    $webhookQueue
                        ->setStatus(self::STATUS_SYNC_DONE)
                        ->setSyncedAt(time());
                } else {
                    $processedError++;
                    $log = "ERROR";
                    $webhookQueue
                        ->setStatus(self::STATUS_SYNC_ERROR)
                        ->setFailedAttempts( ($webhookQueue->getFailedAttempts()+1) );
                }

                $webhookQueue
                    ->setResponseStatus($response->getStatusCode())
                    ->setResponseLog($response->getBody())
                    ->save();

                $this->consoleLog("TRIGGER.DEQUEUE","> (".$progress."/".count($ids).") request completed with $log", $verbose);
            }

            $this->consoleLog("TRIGGER.DEQUEUE","Total webhooks processed: ".$progress." ($processedSuccess success / $processedError error)", $verbose);

        } catch (PropelException $e) {
            $message = is_null($e->getPrevious()) ? $e->getMessage() : $e->getPrevious()->getMessage();
            throw new TriggerHookException("Failed to dequeue - ".$message);
        } catch (\Exception $e) {
            //print_r($e->getPrevious()->getMessage()); die();
            throw new TriggerHookException("Failed to dequeue - ".$e->getMessage());
        }

    }

    /**
     * @param string $method
     * @param $url
     * @param $payload
     * @return \Zend\Http\Response
     */
    public function send($method = Request::METHOD_POST, $url, $payload) {
        $client = new Client();

        $client->setOptions(array(
            'maxredirects' => 10,
            'timeout'      => 10
        ));
        $client->setHeaders(array(
            'Content-Type' => 'application/json',
        ));
        $client->setMethod($method);
        $client->setUri($url);
        $client->setRawBody($payload);

        return $client->send();
    }

    /**
     * Log text message received in input showing it in console / page if $verbose is TRUE
     *
     * @param $tag - string added as log label
     * @param $message - string message to log
     * @param bool $verbose - flag to enable log if TRUE
     */
    public function consoleLog($tag, $message, $verbose = true) {
        if($verbose) {
            $bt = debug_backtrace();
            $caller = array_shift($bt);
            $phpFile = $caller['file'];
            $phpLine = $caller['line'];

            $now = \DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));

            $logMessage = $now->format("Y-m-d H:i:s.u") . " | " . $tag . " | " . $message . " | " . $phpFile." (".$phpLine.")\n";

            echo $logMessage;
        }
    }

    private function generateBundle($event) {
        $bundlePart = array();

        $bundlePart []= $event;
        if(!is_null($this->getPostId())) {
            $bundlePart []= strtolower(HpRestModel::CNT_HASH_PREFIX);
            $bundlePart []= $this->getPostId();
        }
        if(!is_null($this->getSectionId())) {
            $bundlePart []= strtolower(HpRestModel::SCT_HASH_PREFIX);
            $bundlePart []= $this->getSectionId();
        }

        return implode("/", $bundlePart);
    }

    /**
     * @return null
     */
    public function getSectionId()
    {
        return $this->sectionId;
    }

    /**
     * @param null $sectionId
     */
    public function setSectionId($sectionId)
    {
        $this->sectionId = (int) $sectionId;
    }

    /**
     * @return null
     */
    public function getPostId()
    {
        return $this->postId;
    }

    /**
     * @param null $postId
     */
    public function setPostId($postId)
    {
        $this->postId = (int) $postId;
    }
}