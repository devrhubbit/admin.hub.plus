<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 18/07/16
 * Time: 16:00
 */

namespace RestHook\Controller;

use Rest\Exception\RestAppIdSecurityException;

use RestHook\Exception\RestHookInvalidHashIdException;
use RestHook\Model\HookRestModel;
use RestHook\Exception\RestHookUuidSecurityException;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Router\Http\RouteMatch;
use Zend\Http\PhpEnvironment\Request;


class RestSubscriptionController extends AbstractActionController
{
    protected $request_content;

    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    /* @var $get_params \Zend\Stdlib\ParametersInterface */
    protected $get_params;

    /* @var $post_params \Zend\Stdlib\ParametersInterface */
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /**
     * @param MvcEvent $e
     * @return mixed
     */
    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = new ViewModel;
        $this->viewModel->setTemplate('rest/response');
        $this->viewModel->setTerminal(true);

        /** @var Request $request */
        $request = $this->getRequest();

        $this->get_params = $request->getQuery();
        $this->post_params = $request->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        if ($this->getRequest()->getContent() != null) {
            $this->request_content = json_decode($this->getRequest()->getContent());
        }

        return parent::onDispatch($e);
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function meAction()
    {
        try {
            $hm = new HookRestModel($this);
            $result = $hm->me();

            $response = $this->getResponse();
            $response->setStatusCode($hm->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch (RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid', array('error' => $e->getErrorMessage()));
        } catch (RestHookUuidSecurityException $e) {
            return $this->redirect()->toRoute('error_uuid', array('error' => $e->getErrorMessage()));
        }
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function subscribeAction()
    {
        try {
            $type = $this->route_params->getParam('type');

            $hashId = $this->request_content->hashId;
            $bindTo = $this->request_content->bindTo;
            $url = $this->request_content->hookUrl;

            $hm = new HookRestModel($this);
            $result = $hm->subscribe($type, $hashId, $bindTo, $url);

            $response = $this->getResponse();
            $response->setStatusCode($hm->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch (RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid', array('error' => $e->getErrorMessage()));
        } catch (RestHookUuidSecurityException $e) {
            return $this->redirect()->toRoute('error_uuid', array('error' => $e->getErrorMessage()));
        } catch (RestHookInvalidHashIdException $e) {
            return $this->redirect()->toRoute('error_hashid', array('error' => $e->getErrorMessage()));
        }
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function unsubscribeAction()
    {
        try {
            $type = $this->route_params->getParam('type');
            $subscriptionId = $this->route_params->getParam('id');

            $hm = new HookRestModel($this);
            $result = $hm->unsubscribe($type, $subscriptionId);

            $response = $this->getResponse();
            $response->setStatusCode($hm->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch (RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid', array('error' => $e->getErrorMessage()));
        } catch (RestHookUuidSecurityException $e) {
            return $this->redirect()->toRoute('error_uuid', array('error' => $e->getErrorMessage()));
        } catch (RestHookInvalidHashIdException $e) {
            return $this->redirect()->toRoute('error_hashid', array('error' => $e->getErrorMessage()));
        }
    }

    /**
     *
     * @return Response|ViewModel
     */
    public function sampleAction()
    {
        try {
            $type = $this->route_params->getParam('type');

            $hm = new HookRestModel($this);
            $response = $hm->sampleRedirect($type);
            if(!is_null($response)) {
                return $response;
            } else {
                $response = $this->getResponse();
                $response->setStatusCode(406);
                $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

                return $this->viewModel->setVariable('response', null);
            }

        } catch (RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid', array('error' => $e->getErrorMessage()));
        } catch (RestHookUuidSecurityException $e) {
            return $this->redirect()->toRoute('error_uuid', array('error' => $e->getErrorMessage()));
        } catch (RestHookInvalidHashIdException $e) {
            return $this->redirect()->toRoute('error_hashid', array('error' => $e->getErrorMessage()));
        }
    }

}