<?php

namespace RestHook\Controller;

use RestHook\Trigger\Trigger;
use Symfony\Component\Config\Definition\Exception\Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

class ConsoleController extends AbstractActionController
{
    private $config;

    /* @var $route_params RouteMatch */
    private $route_params = null;

    private $verboseMode = false;
    private $debugMode = false;

    public function onDispatch(MvcEvent $e)
    {
        $request = $this->getRequest();

        if (!$request instanceof \Zend\Console\Request) {
            die('ACCESS DANIED: You can only use this action from a console!');
        }

        $this->config = $this->getServiceLocator()->get('config');

        $this->route_params = $this->getEvent()->getRouteMatch();
        $this->verboseMode = ($this->route_params->getParam('verbose') || $this->route_params->getParam('v'));
        $this->debugMode = ($this->route_params->getParam('debug') || $this->route_params->getParam('d'));

        if ($this->debugMode) echo print_r($this->route_params->getParams(), true) . "\n";

        return parent::onDispatch($e);
    }

    public function testAction()
    {
        $msg = $this->route_params->getParam('msg');
        echo "hooktest: " . $msg . "\n";
    }

    public function hookSyncAction()
    {
        $limit = $this->route_params->getParam('limit', 5);
        $trigger = new Trigger();

        try {
            $trigger->dequeue($limit, $this->verboseMode);
            exit(0);
        } catch (Exception $e ) {
            $trigger->consoleLog("TRIGGER.ERROR", $e->getMessage(), $this->verboseMode);
            echo $e->getMessage();
            exit(1);
        }
    }

}
