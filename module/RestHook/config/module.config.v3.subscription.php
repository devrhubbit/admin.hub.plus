<?php

return array(
    'router' => array(
        'routes' => array(
            'hook_me' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/hook/me[/]',
                ),
                'child_routes' => [
                    'get' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get',
                            'defaults' => [
                                'controller' => 'RestHook\Controller\Subscription',
                                'action' => 'me',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post,put,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
            'hook_subscription' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/hook/subscribe/:type/[:id][/]',
                ),
                'child_routes' => [
                    'post' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post',
                            'defaults' => [
                                'controller' => 'RestHook\Controller\Subscription',
                                'action' => 'subscribe',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'delete',
                            'defaults' => [
                                'controller' => 'RestHook\Controller\Subscription',
                                'action' => 'unsubscribe',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,put,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
            'hook_sample' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/hook/sample/:type[/]',
                ),
                'child_routes' => [
                    'get' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get',
                            'defaults' => [
                                'controller' => 'RestHook\Controller\Subscription',
                                'action' => 'sample',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post,put,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
        ),
    ),
);