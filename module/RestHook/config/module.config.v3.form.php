<?php

return array(
    'router' => array(
        'routes' => array(
            'form_hook' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/hook/form/:fid[/row/:rid][/]',
                ),
                'child_routes' => [
                    'get' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get',
                            'defaults' => [
                                'controller' => 'RestHook\Controller\Form',
                                'action' => 'getData',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post,put,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
        ),
    ),
);