<?php

return array(
    'console' => array(
        'router' => array(
            'routes' => array(
                'htest' => array(
                    'options' => array(
                        // add [ and ] if optional ( ex : [<doname>] )
                        'route' => 'hp hooktest [--debug|-d] <msg>',
                        'defaults' => array(
                            'controller' => 'RestHook\Controller\Console',
                            'action' => 'test',
                        ),
                    ),
                ),
                'hook_sync' => array(
                    'options' => array(
                        'route' => 'hp hooksync [--debug|-d] [--verbose|-v] [--limit=]',
                        'defaults' => array(
                            'controller' => 'RestHook\Controller\Console',
                            'action' => 'hookSync',
                        ),
                    ),
                ),
            ),
        ),
    ),
);