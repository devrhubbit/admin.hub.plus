<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace RestHook;

use Common\Event\FormEventManager;
use RestHook\Trigger\Form\CreatedFormTrigger;
use Zend\Console\Adapter\AdapterInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\Event;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $this->catchTriggerEvent($eventManager);
    }

    private function catchTriggerEvent(EventManagerInterface $eventManager) {
        $sharedEvents = $eventManager->getSharedManager();

        $sharedEvents->attach('*',FormEventManager::ON_CREATE_ROW, function(Event $e) { (new CreatedFormTrigger())->append($e); });
    }

    public function getControllerConfig() {

    }

    public function getConfig()
    {
        //return include __DIR__ . '/config/module.config.php';
        $config = array();

        $configFiles = array(
            __DIR__ . '/config/module.config.php',                  // Module general config
            __DIR__ . '/config/module.config.v3.subscription.php',  // Subscription routes config
            __DIR__ . '/config/module.config.v3.form.php',          // Form routes config
            __DIR__ . '/config/module.config.v3.console.php',       // Console routes config
        );

        // Merge all module config options
        foreach ($configFiles as $configFile) {
            $config = \Zend\Stdlib\ArrayUtils::merge($config, include $configFile);
        }

        return $config;
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Returns an array or a string containing usage information for this module's Console commands.
     * The method is called with active Zend\Console\Adapter\AdapterInterface that can be used to directly access
     * Console and send output.
     *
     * If the result is a string it will be shown directly in the console window.
     * If the result is an array, its contents will be formatted to console window width. The array must
     * have the following format:
     *
     *     return array(
     *                'Usage information line that should be shown as-is',
     *                'Another line of usage info',
     *
     *                '--parameter'        =>   'A short description of that parameter',
     *                '-another-parameter' =>   'A short description of another parameter',
     *                ...
     *            )
     *
     * @param AdapterInterface $console
     * @return array|string|null
     */
    public function getConsoleUsage(AdapterInterface $console)
    {
        return array(
            'hp hooksync [--debug|-d] [--verbose|-v] [--limit=]' => "Command to sync RestHook queue with remote listener:",
            array('--debug|-d', "enable debug mode printing all params received in console"),
            array('--verbose|-v', "enable verbose mode printing send status log in console"),
            array('--limit', "number of messages extracted (default value is 5)"),
        );
    }
}
