<?php
namespace Common;

use Zend\Mvc\MvcEvent;

class Module
{
	protected $serviceLocator;

	public function onBootstrap(MvcEvent $e)
	{	
		$this->serviceManager 		= $e->getApplication()->getServiceManager();
	}
	
	public function getAutoloaderConfig()
    {
    	return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
    	return include __DIR__ . '/config/module.config.php';
    }
    
}
