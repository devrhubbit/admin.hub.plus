<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'view_manager' => array(
        'template_map' => array(
            'email/admin/registered'            => __DIR__ . '/../view/email/admin/registered.phtml',
            'email/admin/admin-registered'      => __DIR__ . '/../view/email/admin/registered-to-admin.phtml',
            'email/admin/password-recovery'     => __DIR__ . '/../view/email/admin/password-recovery.phtml',
            'email/admin/super-admin-created'   => __DIR__ . '/../view/email/admin/super-admin-created.phtml',
            'email/admin/simple'                => __DIR__ . '/../view/email/admin/simple.phtml',
            'email/app/registered'              => __DIR__ . '/../view/email/app/registered.phtml',
            'email/app/password-recovery'       => __DIR__ . '/../view/email/app/password-recovery.phtml',
            'email/app/forwarded-contact'       => __DIR__ . '/../view/email/app/forwarded-contact.phtml',
        ),
    ),
);
