<?php

namespace Common\Controller;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 17/11/16
 * Time: 11:22
 */
abstract class HpAbstractActionController extends AbstractActionController
{
    public function getRenderer() {
        return $this->getServiceLocator()->get('Zend\View\Renderer\RendererInterface');
    }

    public function getBaseurl() {
        $renderer   = $this->getRenderer();
        return $renderer->basePath();
    }

    public function getJsonResponse($success, $message = null) {
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        $response = new \StdClass();

        $response->success = $success;
        $response->message = $message;

        return json_encode($response);
    }

    public function getDTJsonResponse($data) {
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        return json_encode($data);
    }
}