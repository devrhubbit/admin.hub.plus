<?php

namespace Common\Helper;

use Application\Model\BackendUsersModel;
use Application\Model\SectionModel;
use Zend\View\Model\ViewModel;

class LayoutHelper
{
    const TITLE = "title";
    const SUBTITLE = "subtitle";
    const BREADCRUMBS = "breadcrumb";
    const MENU = "menu";
    const MENU_ENABLED = "menu_enabled";

    const MENU_HEADER = "header";
    const MENU_ITEM = "item";
    const MENU_LIST = "list";

    const USER_DATA = "user_data";
    const APP_DATA = "app_data";
    const APP_LIST_DATA = "app_list_data";

    const GMAP_KEY = "gmap_key";

    private $layout;

    function __construct(ViewModel $layout)
    {
        $this->layout = $layout;
    }

    public function setTitle($title)
    {
        $this->layout->setVariable(LayoutHelper::TITLE, $title);
    }

    public function setSubTitle($subtitle)
    {
        $this->layout->setVariable(LayoutHelper::SUBTITLE, $subtitle);
    }

    public function setBreadcrumbs($data = array())
    {
        $this->layout->setVariable(LayoutHelper::BREADCRUMBS, $data);
    }

    public function setMenu($data = array(), $enabled = true, $selected = "dashboard", $serviceLocator = null)
    {
        $aclHelper = new AclHelper($serviceLocator);
        $sectionModel = new SectionModel($serviceLocator);
        $config = $serviceLocator->get('config');

        /* add superpower menu */
        $userRoleId = $aclHelper->getUser()->role_id;
        if ($userRoleId === BackendUsersModel::SUPER_ADMIN_ROLE_ID) {
            $data[] = $config["menu"]["superpowers"];
        }
        /* .../add superpower menu */

        foreach ($data as &$node) {

            /* build dynamic content for Form sidebar */
            if ($serviceLocator != null) {
                if (isset($node['id']) && $node['id'] == "form") {
                    $node["target"] = $sectionModel->getCustomFormList($config);
                }
            }
            /* .../build dynamic content for Form sidebar */

            if (isset($node["target"])) {
                if ($node["type"] == "item") {
                    if (isset($node["id"]) && $node["id"] == $selected) {
                        $node["active"] = 1;
                    }
                } elseif ($node["type"] == "list" && $node["id"] == "form") {
                    $targets = &$node["target"];
                    foreach ($targets as &$target) {
                        if ($target["id"] == $selected) {
                            $node["active"] = 1;
                            $target["active"] = 1;
                        }
                    }
                } else {
                    $targets = &$node["target"];
                    foreach ($targets as &$target) {
                        if ($target["id"] == $selected) {
                            $target["active"] = 1;
                            $node["active"] = 1;
                        }
                    }
                }
            }
        }

        $this->layout->setVariable(LayoutHelper::MENU, $data);
        $this->layout->setVariable(LayoutHelper::MENU_ENABLED, $enabled);
    }

    public function setUser($user)
    {
        $this->layout->setVariable(LayoutHelper::USER_DATA, $user);
    }

    public function setApp($app)
    {
        $this->layout->setVariable(LayoutHelper::APP_DATA, $app);
    }

    public function setAppList($app_list)
    {
        $this->layout->setVariable(LayoutHelper::APP_LIST_DATA, $app_list);
    }

    public function setGMapKey($key)
    {
        $this->layout->setVariable(LayoutHelper::GMAP_KEY, $key);
    }
}