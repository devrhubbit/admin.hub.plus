<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 10/09/17
 * Time: 13:20
 */

namespace Common\Helper;

class MediaHelper
{
    const MEDIA_TYPE_TEXT = "TEXT";
    const MEDIA_TYPE_IMAGE = "IMAGE";
    const MEDIA_TYPE_ARCHIVE = "ARCHIVE";
    const MEDIA_TYPE_AUDIO = "AUDIO";
    const MEDIA_TYPE_VIDEO = "VIDEO";
    const MEDIA_TYPE_DOCUMENT = "DOCUMENT";
    const MEDIA_TYPE_UNKNOWN = "UNKNOWN";

    // text
    const TXT_MIME_TYPE = "text/plain";
    const HTML_MIME_TYPE = "text/html";
    const CSS_MIME_TYPE = "text/css";
    const CSV_MIME_TYPE = "text/csv";
    const JS_MIME_TYPE = "application/javascript";
    const JSON_MIME_TYPE = "application/json";
    const XML_MIME_TYPE = "application/xml";
    // images
    const PNG_MIME_TYPE = "image/png";
    const JPG_MIME_TYPE = "image/jpeg";
    const GIF_MIME_TYPE = "image/gif";
    const BMP_MIME_TYPE = "image/bmp";
    const ICO_MIME_TYPE = "image/vnd.microsoft.icon";
    const TIFF_MIME_TYPE = "image/tiff";
    const SVG_MIME_TYPE = "image/svg+xml";
    // archives
    const ZIP_MIME_TYPE = "application/zip";
    const RAR_MIME_TYPE = "application/x-rar-compressed";
    const MSI_MIME_TYPE = "application/x-msdownload";
    const CAB_MIME_TYPE = "application/vnd.ms-cab-compressed";
    // audio/video
    const WAV_MIME_TYPE = "audio/wav";
    const MPEG3_MIME_TYPE = "audio/mpeg3";
    const MP3_MIME_TYPE = "audio/mp3";
    const MPGA_MIME_TYPE = "audio/mpeg";
    const MIDI_MIME_TYPE = "audio/midi";
    const MOV_MIME_TYPE = "video/quicktime";
    const FLV_MIME_TYPE = "video/x-flv";
    const AVI_MIME_TYPE = "video/avi";
    const MSV_MIME_TYPE = "video/msvideo";
    const XMSV_MIME_TYPE = "video/x-msvideo";
    const MPEG_MIME_TYPE = "video/mpeg";
    const XMPG_MIME_TYPE = "video/x-motion-jpeg";
    const MP4_MIME_TYPE = "video/mp4";
    // adobe
    const PDF_MIME_TYPE = "application/pdf";
    const PSD_MIME_TYPE = "image/vnd.adobe.photoshop";
    const PS_MIME_TYPE = "application/postscript";
    const SWF_MIME_TYPE = "application/x-shockwave-flash";
    // ms office
    const DOC_MIME_TYPE = "application/msword";
    const RTF_MIME_TYPE = "application/rtf";
    const XLS_MIME_TYPE = "application/vnd.ms-excel";
    const PPT_MIME_TYPE = "application/vnd.ms-powerpoint";
    // open office
    const ODT_MIME_TYPE = "application/vnd.oasis.opendocument.text";
    const ODS_MIME_TYPE = "application/vnd.oasis.opendocument.spreadsheet";

    // URL
    const ATTACHMENT_BASE_URL = "/media/attachment/";


    public function getRandomFilename($mime_type)
    {
        $name = md5(uniqid());
        $ext = $this->getExtensionByType($mime_type);

        return ($name . "." . $ext);
    }

    public function getExtensionByType($mime_type)
    {
        $ext = "";

        switch ($mime_type) {
            // Text
            case self::TXT_MIME_TYPE:
                $ext = "txt";
                break;
            case self::HTML_MIME_TYPE:
                $ext = "html";
                break;
            case self::CSS_MIME_TYPE:
                $ext = "css";
                break;
            case self::CSV_MIME_TYPE:
                $ext = "csv";
                break;
            case self::JS_MIME_TYPE:
                $ext = "js";
                break;
            case self::JSON_MIME_TYPE:
                $ext = "json";
                break;
            case self::XML_MIME_TYPE:
                $ext = "xml";
                break;
            // Images
            case self::PNG_MIME_TYPE:
                $ext = "png";
                break;
            case self::JPG_MIME_TYPE:
                $ext = "jpg";
                break;
            case self::GIF_MIME_TYPE:
                $ext = "gif";
                break;
            case self::BMP_MIME_TYPE:
                $ext = "bmp";
                break;
            case self::ICO_MIME_TYPE:
                $ext = "ico";
                break;
            case self::TIFF_MIME_TYPE:
                $ext = "tif";
                break;
            case self::SVG_MIME_TYPE:
                $ext = "svg";
                break;
            //Archive
            case self::ZIP_MIME_TYPE:
                $ext = "zip";
                break;
            case self::RAR_MIME_TYPE:
                $ext = "rar";
                break;
            case self::MSI_MIME_TYPE:
                $ext = "msi";
                break;
            case self::CAB_MIME_TYPE:
                $ext = "cab";
                break;
            //Audio-Video
            case self::WAV_MIME_TYPE:
                $ext = "wav";
                break;
            case self::MP3_MIME_TYPE:
            case self::MPEG3_MIME_TYPE:
                $ext = "mp3";
                break;
            case self::MPGA_MIME_TYPE:
                $ext = "mpga";
                break;
            case self::MIDI_MIME_TYPE:
                $ext = "midi";
                break;
            case self::MOV_MIME_TYPE:
                $ext = "mov";
                break;
            case self::FLV_MIME_TYPE:
                $ext = "flv";
                break;
            case self::AVI_MIME_TYPE:
            case self::MSV_MIME_TYPE:
            case self::XMSV_MIME_TYPE:
                $ext = "avi";
                break;
            case self::MPEG_MIME_TYPE:
                $ext = "mpg";
                break;
            case self::XMPG_MIME_TYPE:
                $ext = "mjpg";
                break;
            case self::MP4_MIME_TYPE:
                $ext = "mp4";
                break;
            //Adobe
            case self::PDF_MIME_TYPE:
                $ext = "pdf";
                break;
            case self::PSD_MIME_TYPE:
                $ext = "psd";
                break;
            case self::PS_MIME_TYPE:
                $ext = "ps";
                break;
            case self::SWF_MIME_TYPE:
                $ext = "swf";
                break;
            //MS Office
            case self::DOC_MIME_TYPE:
                $ext = "doc";
                break;
            case self::RTF_MIME_TYPE:
                $ext = "rtf";
                break;
            case self::XLS_MIME_TYPE:
                $ext = "xls";
                break;
            case self::PPT_MIME_TYPE:
                $ext = "ppt";
                break;
            //Open Office
            case self::ODT_MIME_TYPE:
                $ext = "odt";
                break;
            case self::ODS_MIME_TYPE:
                $ext = "ods";
                break;
            default:
                $ext = "unsupported";
                break;
        }

        return $ext;
    }

    public function saveMedia($tmp_name, $path, $filename, $unique_in_folder = false)
    {
        if (!is_dir($path)) {
            mkdir($path, 0775, true);
        }

        $url = $path . $filename;

        if ($unique_in_folder) {
            array_map('unlink', glob($path . "*"));
        }

        if (move_uploaded_file($tmp_name, $url)) {
            return $url;
        } else {
            return false;
        }
    }

    public function getMediaType($mime_type)
    {
        switch ($mime_type) {
            case self::TXT_MIME_TYPE:
            case self::HTML_MIME_TYPE:
            case self::CSS_MIME_TYPE:
            case self::CSV_MIME_TYPE:
            case self::JS_MIME_TYPE:
            case self::JSON_MIME_TYPE:
            case self::XML_MIME_TYPE:
                $mediaType = self::MEDIA_TYPE_TEXT;
                break;

            case self::PNG_MIME_TYPE:
            case self::JPG_MIME_TYPE:
            case self::GIF_MIME_TYPE:
            case self::BMP_MIME_TYPE:
            case self::ICO_MIME_TYPE:
            case self::TIFF_MIME_TYPE:
            case self::SVG_MIME_TYPE:
                $mediaType = self::MEDIA_TYPE_IMAGE;
                break;

            case self::ZIP_MIME_TYPE:
            case self::RAR_MIME_TYPE:
            case self::MSI_MIME_TYPE:
            case self::CAB_MIME_TYPE:
                $mediaType = self::MEDIA_TYPE_ARCHIVE;
                break;

            case self::WAV_MIME_TYPE:
            case self::MP3_MIME_TYPE:
            case self::MPEG3_MIME_TYPE:
            case self::MPGA_MIME_TYPE:
            case self::MIDI_MIME_TYPE:
                $mediaType = self::MEDIA_TYPE_AUDIO;
                break;

            case self::MOV_MIME_TYPE:
            case self::FLV_MIME_TYPE:
            case self::AVI_MIME_TYPE:
            case self::MSV_MIME_TYPE:
            case self::XMSV_MIME_TYPE:
            case self::MP4_MIME_TYPE:
                $mediaType = self::MEDIA_TYPE_VIDEO;
                break;

            case self::PDF_MIME_TYPE:
            case self::PSD_MIME_TYPE:
            case self::PS_MIME_TYPE:
            case self::SWF_MIME_TYPE:
            case self::DOC_MIME_TYPE:
            case self::RTF_MIME_TYPE:
            case self::XLS_MIME_TYPE:
            case self::PPT_MIME_TYPE:
            case self::ODT_MIME_TYPE:
            case self::ODS_MIME_TYPE:
                $mediaType = self::MEDIA_TYPE_DOCUMENT;
                break;

            default:
                $mediaType = self::MEDIA_TYPE_UNKNOWN;
        }

        return $mediaType;
    }

    public static function getMediaUrl()
    {
        return sprintf(self::ATTACHMENT_BASE_URL);
    }
}