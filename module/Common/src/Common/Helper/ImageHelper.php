<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 09/05/16
 * Time: 18:37
 */

namespace Common\Helper;

use Imagine\Imagick\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;

class ImageHelper
{

    const JPG_MIME_TYPE = "image/jpeg";
    const GIF_MIME_TYPE = "image/gif";
    const PNG_MIME_TYPE = "image/png";

    const TMB_FOLDER = "thumb";
    const LOW_FOLDER = "low";
    const MDM_FOLDER = "medium";
    const HTH_FOLDER = "high";
    const UPL_FOLDER = "original";

    const TMB_SIZE = 150;
    const LOW_SIZE = 256;
    const MDM_SIZE = 512;
    const HTH_SIZE = 1024;

    const ICON_GLOBAL_SIZE = 1024;
    const SPLASH_HEIGHT_SIZE = 2208; // iPhone 7 resolution
    const SPLASH_WIDTH_SIZE = 1242; // iPhone 7 resolution

    const USER_AVATAR_BASEURL = "/media/user/%d/avatar/%s/";
    const USER_FORM_BASEURL = "/media/form/%d/%s/";
    const GALLERY_ITEMS_BASEURL = "/media/gallery/%d/items/%s/";
    const APP_ICON_BASEURL = "/media/app/icon/";
    const APP_SYSTEM_ICON_BASEURL = "/media/app/system/";
    const APP_LOGO_BASEURL = "/media/app/logo/";
    const APP_SPLASH_BASEURL = "/media/app/splash/";
    const ACTION_ICON_BASEURL = "/media/action/icon/%s/";
    const COVER_ICON_BASEURL = "/media/cover/icon/%s/";
    const CUSTOM_FORM_ICON_BASEURL = "/media/section/custom_form/";
    const PROVIDER_ICON_BASEURL = "/media/provider/icon/";
    const ARCHIVE_BASEURL = "/media/archive/%s/";
    const HOST_BASEURL = "/media/host/%d/%s/";
    const POI_PIN_BASEURL = "/media/poi/pin/";
    const SECTION_USER_ADD_ICON_BASEURL = "/media/section/user/add_icons/";
    const SECTION_USER_EDIT_ICON_BASEURL = "/media/section/user/edit_icons/";

    const TEMPLATE_ICON_BASEURL = "/media/template/";

    /* @var $image_service \Imagine\Imagick\Imagine */
    protected $image_service;

    private $baseurl = null;

    function __construct()
    {
        $this->image_service = new Imagine();
        $this->baseurl = "";
    }

    /**
     * @param $tmp_name
     * @param $path
     * @param $filename
     * @param $unique_in_folder
     *
     * @return string
     */
    public function saveImage($tmp_name, $path, $filename, $unique_in_folder = false)
    {

        if (!is_dir($this->baseurl . $path)) {
            mkdir($this->baseurl . $path, 0775, true);
        }

        $url = $path . $filename;
        $image = $this->image_service->open($tmp_name);

        if ($unique_in_folder) {
            array_map('unlink', glob($this->baseurl . $path . "*"));
        }

        $image->save($this->baseurl . $url);

        return $url;
    }

    /**
     * @param $size
     * @param $tmp_name
     * @param $path
     * @param $filename
     * @param $unique_in_folder
     *
     * @return string
     */
    public function saveImageCropped($size, $tmp_name, $path, $filename, $unique_in_folder = false)
    {

        if (!is_dir($this->baseurl . $path)) {
            mkdir($this->baseurl . $path, 0775, true);
        }

        $url = $path . $filename;

        $image = $this->image_service->open($tmp_name);

        $h = $image->getSize()->getHeight();
        $w = $image->getSize()->getWidth();

        $offset = null;
        $box = null;

        $tmb_box = new Box($size, $size);

        if ($h > $w) {
            $offset = new Point(0, (int)($h - $w) / 2);
            $box = new Box($w, $w);
        } else {
            $offset = new Point((int)($w - $h) / 2, 0);
            $box = new Box($h, $h);
        }

        if ($unique_in_folder) {
            array_map('unlink', glob($this->baseurl . $path . "*"));
        }

        $image->crop($offset, $box)
            ->resize($tmb_box)
            ->save($this->baseurl . $url);

        return $url;
    }

    /**
     * @param $width
     * @param $height
     * @param $tmp_name
     * @param $path
     * @param $filename
     * @param $unique_in_folder
     *
     * @return string
     */
    public function saveImageFullCropped($width, $height, $tmp_name, $path, $filename, $unique_in_folder = false)
    {
        if (!is_dir($this->baseurl . $path)) {
            mkdir($this->baseurl . $path, 0775, true);
        }

        $url = $path . $filename;

        $image = $this->image_service->open($tmp_name);

        $offset = null;
        $finalBox = new Box($width, $height);

        if ($unique_in_folder) {
            array_map('unlink', glob($this->baseurl . $path . "*"));
        }

        $image->thumbnail($finalBox)->save($this->baseurl . $url);

        return $url;
    }

    /**
     * @param $size
     * @param $tmp_name
     * @param $path
     * @param $filename
     * @param $unique_in_folder
     *
     * @return string
     */
    public function saveImageScaled($size, $tmp_name, $path, $filename, $unique_in_folder = false)
    {

        if (!is_dir($this->baseurl . $path)) {
            mkdir($this->baseurl . $path, 0775, true);
        }

        $url = $path . $filename;

        $image = $this->image_service->open($tmp_name);

        $h = $image->getSize()->getHeight();
        $w = $image->getSize()->getWidth();

        $box = null;

        if ($h > $w) {
            $box = new Box($size * $w / $h, $size);
        } else {
            $box = new Box($size, $size * $h / $w);
        }

        if ($unique_in_folder) {
            array_map('unlink', glob($this->baseurl . $path . "*"));
        }

        $image->resize($box)
            ->save($this->baseurl . $url);

        return $url;
    }

    /**
     * @param $size
     * @param $tmp_name
     * @param $path
     * @param $filename
     * @param $unique_in_folder
     *
     * @return string
     */
    public function saveImageResized($size, $tmp_name, $path, $filename, $unique_in_folder = false)
    {

        if (!is_dir($this->baseurl . $path)) {
            mkdir($this->baseurl . $path, 0775, true);
        }

        $url = $path . $filename;

        $image = $this->image_service->open($tmp_name);

        $box = new Box($size, $size);

        if ($unique_in_folder) {
            array_map('unlink', glob($this->baseurl . $path . "*"));
        }

        $image->resize($box)
            ->save($this->baseurl . $url);

        return $url;
    }

    /**
     * @param $width
     * @param $height
     * @param $tmp_name
     * @param $path
     * @param $filename
     * @param $unique_in_folder
     *
     * @return string
     */
    public function saveImageFullResized($width, $height, $tmp_name, $path, $filename, $unique_in_folder = false)
    {

        if (!is_dir($this->baseurl . $path)) {
            mkdir($this->baseurl . $path, 0775, true);
        }

        $url = $path . $filename;

        $image = $this->image_service->open($tmp_name);

        $box = new Box($width, $height);

        if ($unique_in_folder) {
            array_map('unlink', glob($this->baseurl . $path . "*"));
        }

        $image->resize($box)
            ->save($this->baseurl . $url);

        return $url;
    }

    public function compositeImage(array $imgSources, $backgroundColor = '#ffffff', \stdClass $imageSize = null)
    {
        $ImagickArray = array();
        if (count($imgSources) > 0) {
            foreach ($imgSources as $imgSource) {
                $image = file_get_contents($imgSource);
                $img = new \Imagick();
                $img->readImageBlob($image);

                $ImagickArray[] = $img;
            }

            $image = new \Imagick();
            $pixel = new \ImagickPixel($backgroundColor);

            if ($imageSize === null && !isset($imageSize->width) && !isset($imageSize->height)) {
                $width = $ImagickArray[0]->getImageWidth();
                $height = $ImagickArray[0]->getImageHeight();
            } else {
                $width = $imageSize->width;
                $height = $imageSize->height;
            }

            // Create image with background color
            $image->newImage($width, $height, $pixel);

            foreach ($ImagickArray as $Imagick) {
                $imagickWidth = $Imagick->getImageWidth();
                $imagickHeight = $Imagick->getImageHeight();
                if ($width >= $imagickWidth) {
                    $xOffset = ($width / 2) - ($Imagick->getImageWidth() / 2);
                } else {
                    $xOffset = -(($Imagick->getImageWidth() / 2) - ($width / 2));
                }

                if ($height >= $imagickHeight) {
                    $yOffset = ($height / 2) - ($Imagick->getImageHeight() / 2);
                } else {
                    $yOffset = -(($Imagick->getImageHeight() / 2) - ($height / 2));
                }

                $image->compositeImage($Imagick, \Imagick::COMPOSITE_DEFAULT, $xOffset, $yOffset);
            }

            $image->resetIterator();
            $combined = $image->appendImages(true);
            $combined->setResolution(72, 72);
            $combined->setImageFormat("jpg");

            return $combined;
        }

        return null;
    }

    public function getRandomFilename($mime_type)
    {
        $name = md5(uniqid());
        $ext = $this->getExtensionByType($mime_type);

        return ($name . "." . $ext);
    }

    public function getExtensionByType($mime_type)
    {
        $ext = "";

        switch ($mime_type) {
            case self::JPG_MIME_TYPE:
                $ext = "jpg";
                break;
            case self::GIF_MIME_TYPE:
                $ext = "gif";
                break;
            case self::PNG_MIME_TYPE:
                $ext = "png";
                break;
        }

        return $ext;
    }

    public static function getUserAvatarThumb($id)
    {
        return sprintf(self::USER_AVATAR_BASEURL, $id, self::TMB_FOLDER);
    }

    public static function getUserAvatarLow($id)
    {
        return sprintf(self::USER_AVATAR_BASEURL, $id, self::LOW_FOLDER);
    }

    public static function getUserAvatarMedium($id)
    {
        return sprintf(self::USER_AVATAR_BASEURL, $id, self::MDM_FOLDER);
    }

    public static function getUserAvatarHigh($id)
    {
        return sprintf(self::USER_AVATAR_BASEURL, $id, self::HTH_FOLDER);
    }

    public static function getFormThumbUrl($form_id)
    {
        return sprintf(self::USER_FORM_BASEURL, $form_id, self::TMB_FOLDER);
    }

    public static function getFormLowUrl($form_id)
    {
        return sprintf(self::USER_FORM_BASEURL, $form_id, self::LOW_FOLDER);
    }

    public static function getFormMediumUrl($form_id)
    {
        return sprintf(self::USER_FORM_BASEURL, $form_id, self::MDM_FOLDER);
    }

    public static function getFormHighUrl($form_id)
    {
        return sprintf(self::USER_FORM_BASEURL, $form_id, self::HTH_FOLDER);
    }

    public static function getAppIconUrl()
    {
        return sprintf(self::APP_ICON_BASEURL);
    }

    public static function getAppSystemIconUrl()
    {
        return sprintf(self::APP_SYSTEM_ICON_BASEURL);
    }

    public static function getAppLogoUrl()
    {
        return sprintf(self::APP_LOGO_BASEURL);
    }

    public static function getAppSplashUrl()
    {
        return sprintf(self::APP_SPLASH_BASEURL);
    }

    public static function getCoverIconThumbUrl()
    {
        $coverFormat = self::TMB_SIZE . "x" . self::TMB_SIZE;
        return sprintf(self::COVER_ICON_BASEURL, $coverFormat);
    }

    public static function getCoverIconFormatUrl($coverFormat)
    {
        return sprintf(self::COVER_ICON_BASEURL, $coverFormat);
    }

    public static function getActionIconThumbUrl()
    {
        $coverFormat = self::TMB_SIZE . "x" . self::TMB_SIZE;
        return sprintf(self::ACTION_ICON_BASEURL, $coverFormat);
    }

    public static function getActionIconUrl($coverFormat)
    {
        return sprintf(self::ACTION_ICON_BASEURL, $coverFormat);
    }

    public static function getCustomFormIconUrl()
    {
        return sprintf(self::CUSTOM_FORM_ICON_BASEURL);
    }

    public static function getProviderIconUrl()
    {
        return sprintf(self::PROVIDER_ICON_BASEURL);
    }

    public static function getTemplateIconUrl()
    {
        return self::TEMPLATE_ICON_BASEURL;
    }

    public static function getMediaThumbUrl()
    {
        return sprintf(self::ARCHIVE_BASEURL, self::TMB_FOLDER);
    }

    public static function getMediaLowUrl()
    {
        return sprintf(self::ARCHIVE_BASEURL, self::LOW_FOLDER);
    }

    public static function getMediaMediumUrl()
    {
        return sprintf(self::ARCHIVE_BASEURL, self::MDM_FOLDER);
    }

    public static function getMediaHighUrl()
    {
        return sprintf(self::ARCHIVE_BASEURL, self::HTH_FOLDER);
    }

    public static function getMediaUploadedUrl()
    {
        return sprintf(self::ARCHIVE_BASEURL, self::UPL_FOLDER);
    }

    public static function getHostThumbUrl($id)
    {
        return sprintf(self::HOST_BASEURL, $id, self::TMB_FOLDER);
    }

    public static function getHostHighUrl($id)
    {
        return sprintf(self::HOST_BASEURL, $id, self::HTH_FOLDER);
    }

    public static function getPoiPin()
    {
        return sprintf(self::POI_PIN_BASEURL);
    }

    public static function getUserSectionAddIcon()
    {
        return sprintf(self::SECTION_USER_ADD_ICON_BASEURL);
    }

    public static function getUserSectionEditIcon()
    {
        return sprintf(self::SECTION_USER_EDIT_ICON_BASEURL);
    }

    /*
        public static function getMediaOriginalUrl($id) {
            return sprintf(self::ARCHIVE_BASEURL, $id, self::MEDIA_ORIGINAL);
        }

        public static function getMediaResizedUrl($id) {
            return sprintf(self::ARCHIVE_BASEURL, $id, self::MEDIA_RESIZED);
        }*/
}