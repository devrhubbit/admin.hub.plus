<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 18/07/16
 * Time: 15:06
 */

namespace Common\Helper;


class StringHelper
{
    const STRING_SPACE = " ";

    /**
     * @param $string
     * @param $char
     * @param $step
     * @return string
     */
    public static function insertSeparator($string, $char, $step)
    {
        $res = $string;

        if ($step > 0) {
            $chunks = str_split($string, $step);
            $res = implode($char, $chunks);
        }

        return $res;
    }

    public static function getCanonical($str)
    {
        return strtolower($str);
    }

    /**
     * @param $text
     * @return string
     */
    public static function parseLikeString($text)
    {
        return "%" . str_replace(" ", "%", $text) . "%";
    }

    /**
     * @param $data
     * @return string
     */
    public static function parseLikeArray($data)
    {
        return "%" . implode("%", $data) . "%";
    }

    public static function parseMatchAgainstArray($data)
    {
        return '"' . implode('" "', $data) . '"';
    }

    public static function parseMatchAgainstString($data, $operator = "AND", $include = true)
    {
        if ($data !== null) {
            $words = explode(self::STRING_SPACE, trim($data));
            $wordsWithWildcards = array();
            foreach ($words as $word) {
                if ($word != "") {
                    $operator = strtoupper($operator);
                    if ($include) {
                        $word = "*$word*";
                    }
                    switch ($operator) {
                        case "AND":
                            $wordsWithWildcards[] = "+$word";
                            break;
                        case "OR":
                            $wordsWithWildcards[] = "$word";
                            break;
                        case "NOT":
                            $wordsWithWildcards[] = "-$word";
                            break;
                    }

                }
            }
            $data = implode(" ", $wordsWithWildcards);
        }
        return $data;
    }

    /**
     * Receives "STR1|STR2|STR3"
     * and returns "'STR1','STR2','STR3'"
     *
     * @param string $text
     * @return string
     */
    public static function parseSqlInString($text)
    {
        return "'" . str_replace("|", "','", $text) . "'";
    }

    /**
     * @param $text
     * @return string
     */
    public static function cleanStr($text)
    {
        return html_entity_decode(strip_tags($text));
    }

    /**
     * Return string inside $string beetween two different $start and $end one
     * (ie: [foo] & [/foo]), take a look at this post from Justin Cook
     * (http://www.justin-cook.com/wp/2006/03/31/php-parse-a-string-between-two-strings/)
     *
     * @param $string
     * @param $start
     * @param $end
     * @return string
     */
    public static function getStringBetween($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);

        return $length === 0 ||
            (substr($haystack, -$length) === $needle);
    }
}