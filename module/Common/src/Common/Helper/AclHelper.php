<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 14/04/16
 * Time: 14:34
 */

namespace Common\Helper;


use Application\Model\AppModel;
use Application\Model\BackendUsersModel;
use Zend\Session\Container;
use Zend\ServiceManager\ServiceLocatorInterface;

class AclHelper
{
    public $session = null;
    private $serviceLocator;

    function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->session = new Container('acl');
        $this->serviceLocator = $serviceLocator;

        if($this->session->isLogged == null) {
            $this->session->isLogged    = false;
            $this->session->userData    = null;
            $this->session->appData     = null;
            $this->session->appListData = null;
        }
    }

    /** ACL SUPPORT METHODS **/

    public function isLogged() {
        return $this->session->isLogged;
    }

    public function logout() {
        $this->session->isLogged    = false;
        $this->session->userData    = null;
        $this->session->appData     = null;
        $this->session->appListData = null;

        session_destroy();
    }

    public function getUser() {
        return $this->session->userData;
    }

    public function getApp() {
        return $this->session->appData;
    }

    public function setApp($app_data) {
        $this->session->appData = $app_data;
    }

    public function getAppList() {
        return $this->session->appListData;
    }

    public function updateAppList() {
        $appModel  = new AppModel($this->serviceLocator);
        $appListData = $appModel->getListApplicationByUser($this->session->userData->id);
        $this->session->appListData = $appListData;
    }

    public function getCategorySetting() {
        return json_decode($this->session->appData->category_setting);
    }

    public function setContentLang($locale, $systemLanguage)
    {
        if (array_key_exists($locale, $systemLanguage)) {
            $this->session->currentLang = $locale;
        }
    }

    public function getUserLanguages()
    {
        $res = "";

        $application = $this->getApp();
        if ($application !== null) {
            $userLanguages[] = $application->main_contents_language;
            $otherContentsLanguages = json_decode($application->other_contents_languages);

            if ($otherContentsLanguages) {
                foreach ($otherContentsLanguages as $language) {
                    $userLanguages[] = $language;
                }
            }

            $res = json_encode($userLanguages);
        }

        return $res;
    }

    /**
     * @param $email
     * @param $password
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function login($email, $password) {
        $res = array(
            'success' => false,
            'message' => "An error occurred",
        );

        $BackendUsersModel = new BackendUsersModel($this->serviceLocator);
        $appModel  = new AppModel($this->serviceLocator);

        $userData = $BackendUsersModel->authenticateUser($email, $password);

        if($userData != null) {
            $loginResponse = $BackendUsersModel->validateLogin($userData);

            if ($loginResponse->success === true) {
                $appData     = $appModel->getLastApplicationByUser($userData->id);
                $appListData = $appModel->getListApplicationByUser($userData->id);

                $this->session->isLogged    = true;
                $this->session->userData    = $userData;
                $this->session->appData     = $appData;
                $this->session->appListData = $appListData;
                $this->session->currentLang = isset($appData->main_contents_language) ? $appData->main_contents_language : null;

                $res['success'] = true;
                $res['message'] = "OK";
            } else {
                $res['message'] = $loginResponse->message;
            }
        } else {
            $this->session->isLogged    = false;
            $this->session->userData    = null;
            $this->session->appData     = null;
            $this->session->appListData = null;

            $res['message'] = "Wrong username or password!";
        }

        return $res;
    }

    public function getCurrentLanguage() {
        return $this->session->currentLang;
    }

    public function logAs($id) {
        $res = false;
        $BackendUsersModel = new BackendUsersModel($this->serviceLocator);
        $appModel  = new AppModel($this->serviceLocator);

        $customerData = $BackendUsersModel->authenticateUserById($id);

        if($customerData != null) {
            $appData     = $appModel->getLastApplicationByUser($customerData->id);
            $appListData = $appModel->getListApplicationByUser($customerData->id);

            $this->session->isLogged    = true;
            $this->session->userData    = $customerData;
            $this->session->appData     = $appData;
            $this->session->appListData = $appListData;
            $res = true;
        }

        return $res;
    }

}