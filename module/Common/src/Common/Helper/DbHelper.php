<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 21/04/16
 * Time: 16:29
 */

namespace Common\Helper;

use Zend\ServiceManager\ServiceLocatorInterface;


class DbHelper
{
    protected $serviceLocator;
    protected $aclHelper;

    private $app_dsn;
    private $app_user;
    private $app_pwd;

    private static $sqlDbApp = "SELECT a.*, ua.user_id
                                FROM `application` AS a JOIN `user_application` AS ua ON a.id = ua.app_id
                                WHERE a.`app_key` = :app_key AND a.`deleted_at` IS NULL AND (a.`expired_at` IS NULL OR a.`expired_at` < NOW());";

    public function __construct(ServiceLocatorInterface $serviceLocator, AclHelper $aclHelper = null)
    {
        $this->serviceLocator = $serviceLocator;
        $this->aclHelper = $aclHelper;
    }


    /**
     * @return \PDO
     */
    public function getDatabase($dsn, $usr, $pwd)
    {
        $db = new \PDO($dsn, $usr, $pwd);

        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        $db->setAttribute(\PDO::ATTR_STRINGIFY_FETCHES, false);

        return $db;
    }

    /**
     * @return \PDO
     */
    public function getCoreDatabase()
    {
        $config = $this->serviceLocator->get('config');

        $dsn = $config['database']['core']['dsn'];
        $usr = $config['database']['core']['usr'];
        $pwd = $config['database']['core']['pwd'];

        return $this->getDatabase($dsn, $usr, $pwd);
    }

    /**
     * @return \PDO
     */
    public function getAppDatabase()
    {
        if (!is_null($this->aclHelper->getApp()) &&
            property_exists($this->aclHelper->getApp(), 'id') &&
            $this->aclHelper->getApp()->id > 0
        ) {

            $dsn = $this->aclHelper->getApp()->dsn;
            $usr = $this->aclHelper->getApp()->db_user;
            $pwd = $this->aclHelper->getApp()->db_pwd;

            return $this->getDatabase($dsn, $usr, $pwd);
        } else {
            return null;
        }
    }

    /**
     * @return \PDO
     */
    public function getRestDatabase()
    {
        return $this->getDatabase($this->app_dsn, $this->app_user, $this->app_pwd);
    }

    public function getApplicationByKey($appKey)
    {
        try {
            $db = $this->getCoreDatabase();
            $stmt = $db->prepare(self::$sqlDbApp);

            $stmt->bindParam(":app_key", $appKey, \PDO::PARAM_STR);
            $stmt->execute();

            $res = $stmt->fetchObject();

            if ($res) {
                $this->app_dsn = $res->dsn;
                $this->app_user = $res->db_user;
                $this->app_pwd = $res->db_pwd;
            }

            return $res;

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        return false;
    }
}