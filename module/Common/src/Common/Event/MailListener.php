<?php

namespace Common\Event;
use AcMailer\Event\MailEvent;

/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 28/07/16
 * Time: 18:45
 */
class MailListener extends \AcMailer\Event\AbstractMailListener
{

    private $log_file = "";

    public function setLogFile($file) {
        $this->log_file = $file;
    }

    /**
     * Called before sending the email
     * @param MailEvent $e
     * @return mixed
     */
    public function onPreSend(MailEvent $e)
    {
        // TODO: Implement onPreSend() method.
        $this->log("onPreSend\n");
    }

    /**
     * Called after sending the email
     * @param MailEvent $e
     * @return mixed
     */
    public function onPostSend(MailEvent $e)
    {
        // TODO: Implement onPostSend() method.
        $this->log("onPostSend\n");
    }

    /**
     * Called if an error occurs while sending the email
     * @param MailEvent $e
     * @return mixed
     */
    public function onSendError(MailEvent $e)
    {
        // TODO: Implement onSendError() method.
        $this->log("onSendError: ".$e->getResult()->getMessage()."\n");
    }

    private function log($txt) {
        $file = fopen($this->log_file, "a") or die("Unable to open file!");
        $date = date("Y-m-d H:i:s");

        fwrite($file, $date . " - " . $txt);
        fclose($file);
    }
}