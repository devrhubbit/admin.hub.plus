<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 25/07/2019
 * Time: 23:36
 */

namespace Common\Event;


class FormEventManager extends HpEventManager
{

    const ON_CREATE_ROW = "plus.hub.formrow.created";
    const ON_UPDATE_ROW = "plus.hub.formrow.updated";
    const ON_DELETE_ROW = "plus.hub.formrow.deleted";

    public function triggerOnCreate($section_id, $row_id, $username, $data) {
        $params = array(
            'section_id' => $section_id,
            'row_id' => $row_id,
            'username' => $username,
            'data' => $data,
        );
        $this->getEventManager()->trigger(self::ON_CREATE_ROW, $this, $params);
        return $this;
    }

}