<?php

return array(
    'router' => array(
        'routes' => array(
            'push_notification' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/send-notification/:type[/]',
                ),
                'child_routes' => [
                    'post' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Notification',
                                'action' => 'sendNotification',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,put,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
            'message-read' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/notification/:type/:hashId[/]',
                ),
                'child_routes' => [
                    'put' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'put',
                            'defaults' => array(
                                'controller' => 'Rest\Controller\Tracker',
                                'action' => 'trackMessage',
                            ),
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,post,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
        ),
    ),
);