<?php

return array(
    'router' => array(
        'routes' => array(
            'error_method' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/error/method/[:error]',
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Error',
                        'action'     => 'invalidMethod',
                    ),
                ),
            ),
            'error_appid' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/error/appid/[:error]',
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Error',
                        'action'     => 'invalidAppId',
                    ),
                ),
            ),
            'error_uuid' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/error/uuid/[:error]',
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Error',
                        'action'     => 'invalidProviderUuid',
                    ),
                ),
            ),
            'error_hashid' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/error/hashid/[:error]',
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Error',
                        'action'     => 'invalidHashId',
                    ),
                ),
            ),
            'error_apikey' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/error/apikey/[:error]',
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Error',
                        'action'     => 'invalidApiKey',
                    ),
                ),
            ),
            'error_signature' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/error/signature/[:error]',
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Error',
                        'action'     => 'invalidSignature',
                    ),
                ),
            ),
            'error_params' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/error/notfound/[:error]',
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Error',
                        'action'     => 'invalidParams',
                    ),
                ),
            ),
        ),
    ),
);