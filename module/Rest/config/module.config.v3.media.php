<?php

return array(
    'router' => array(
        'routes' => array(
            'delete_media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/api/v3/media[/connector/:connector_id]/:id[/]',
                ),
                'child_routes' => [
                    'delete' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'delete',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Media',
                                'action' => 'deleteMedia',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,post,put,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
            'upload_media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/api/v3/media/upload/:context/:id/:type[/]',
                ),
                'child_routes' => [
                    'post' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Media',
                                'action' => 'uploadMedia',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'delete',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Media',
                                'action' => 'unlinkMedia',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,put,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
            'like_media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/api/v3/media[/connector/:connector_id]/:id/like[/]',
                ),
                'child_routes' => [
                    'post' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Media',
                                'action' => 'likeMedia',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'delete',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Media',
                                'action' => 'unlikeMedia',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,put,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
            'share_media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/api/v3/media[/connector/:connector_id]/:id/share[/]',
                ),
                'child_routes' => [
                    'post' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Media',
                                'action' => 'shareMedia',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,put,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
        ),
    ),
);