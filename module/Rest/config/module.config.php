<?php

return array(

    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Rest\Controller\Error' => 'Rest\Controller\RestErrorController',
            'Rest\Controller\Identity' => 'Rest\Controller\RestIdentityController',
            'Rest\Controller\Page' => 'Rest\Controller\RestPageController',
            'Rest\Controller\Media' => 'Rest\Controller\RestMediaController',
            'Rest\Controller\Notification' => 'Rest\Controller\RestNotificationController',
            'Rest\Controller\Tracker' => 'Rest\Controller\RestTrackerController',
            'Rest\Controller\Ajax' => 'Rest\Controller\RestAjaxController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'rest/response' => __DIR__ . '/../view/rest/response.phtml',
            'rest/app_store' => __DIR__ . '/../view/rest/app_store.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
);
