<?php

return array(
    'router' => array(
        'routes' => array(
            'form-select-filter' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/form/:id/select/:field[/]',
                ),
                'child_routes' => [
                    'get' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Ajax',
                                'action' => 'getSelectOptions',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post,put,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
        )
    )
);