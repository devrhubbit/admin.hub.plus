<?php

return array(
    'router' => array(
        'routes' => array(
            'app' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/app[/]',
                ),
                'child_routes' => [
                    'get' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Identity',
                                'action' => 'checkAppId',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post,put,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
            'device' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/device[/]',
                ),
                'child_routes' => [
                    'post' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Identity',
                                'action' => 'insertDevice',
                            ],
                        ],
                    ],
                    'put' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'put',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Identity',
                                'action' => 'updateDevice',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
            'remove-device' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/api/v3/remove-device/:token',
                    'defaults' => [
                        'controller' => 'Rest\Controller\Identity',
                        'action' => 'removeDevice',
                    ],
                ),
            ),
            'user_create' => array( //OLD REGISTRATION FORM
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/user[/register/:id][/]',
                ),
                'child_routes' => [
                    'get' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Identity',
                                'action' => 'detailProfile',
                            ],
                        ],
                    ],
                    'post' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Identity',
                                'action' => 'insertProfile',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'put,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
            'user_update' => array( //OLD REGISTRATION FORM
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/user/update[/]',
                ),
                'child_routes' => [
                    'post' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Identity',
                                'action' => 'updateProfile',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,put,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
            'user_auth' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/user/auth[/]',
                ),
                'child_routes' => [
                    'post' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Identity',
                                'action' => 'login',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'delete',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Identity',
                                'action' => 'logout',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,put,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
            'user_password_recovery' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/user/password-recovery[/]',
                ),
                'child_routes' => [
                    'post' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Identity',
                                'action' => 'passwordRecovery',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,put,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
            'user_form' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/api/v3/user/form/:id[/]',
                ),
                'child_routes' => [
                    'get' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Identity',
                                'action' => 'getUserData',
                            ],
                        ],
                    ],
                    'post' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Identity',
                                'action' => 'insertUserData',
                            ],
                        ],
                    ],
                    'put' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'put',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Identity',
                                'action' => 'updateUserData',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Error',
                                'action' => 'invalidMethod',
                            ],
                        ],
                    ],
                ],
            ),
        ),
    ),
);