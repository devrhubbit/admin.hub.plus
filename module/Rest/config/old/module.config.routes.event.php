<?php

return array(
    'router' => array(
        'routes' => array(
            'event_list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/event/list[/]',
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Event',
                        'action'     => 'list',
                    ),
                ),
            ),
            'event_detail' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/event/hash[/][:hash][/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Event',
                        'action'     => 'detail',
                    ),
                ),
            ),
            'event_share' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/event/hash/:hash/shared[/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                ),
                'child_routes' => [
                    'put' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'put',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Event',
                                'action' => 'shared',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,post,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Event',
                                'action' => 'invalidHeader',
                            ],
                        ],
                    ],
                ],
            ),
            'event_like' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/event/hash/:hash/liked[/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                ),
                'child_routes' => [
                    'put' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'put',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Event',
                                'action' => 'liked',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,post,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Event',
                                'action' => 'invalidHeader',
                            ],
                        ],
                    ],
                ],
            ),
            'event_reserve' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/event/hash/:hash/reserved[/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                ),
                'child_routes' => [
                    'put' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'post',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Event',
                                'action' => 'reserved',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,put,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Event',
                                'action' => 'invalidHeader',
                            ],
                        ],
                    ],
                ],
            ),
        ),
    ),
);