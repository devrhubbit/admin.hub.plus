<?php

return array(
    'router' => array(
        'routes' => array(
            'artist_list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/artist/list[/]',
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Artist',
                        'action'     => 'list',
                    ),
                ),
            ),
            'artist_detail' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/artist/hash[/][:hash][/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Artist',
                        'action'     => 'detail',
                    ),
                ),
            ),
            'artist_share' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/artist/hash/:hash/shared[/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                ),
                'child_routes' => [
                    'put' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'put',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Artist',
                                'action' => 'shared',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,post,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Artist',
                                'action' => 'invalidHeader',
                            ],
                        ],
                    ],
                ],
            ),
            'artist_like' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/artist/hash/:hash/liked[/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                ),
                'child_routes' => [
                    'put' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'put',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Artist',
                                'action' => 'liked',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,post,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Artist',
                                'action' => 'invalidHeader',
                            ],
                        ],
                    ],
                ],
            ),
        ),
    ),
);