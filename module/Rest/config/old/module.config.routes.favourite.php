<?php

return array(
    'router' => array(
        'routes' => array(
            'favourite_list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/favourite/list[/]',
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Favourite',
                        'action'     => 'list',
                    ),
                ),
            ),
        ),
    ),
);