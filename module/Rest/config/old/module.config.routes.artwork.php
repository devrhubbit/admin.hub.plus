<?php

return array(
    'router' => array(
        'routes' => array(
            'artwork_list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/artwork/list[/]',
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Artwork',
                        'action'     => 'list',
                    ),
                ),
            ),
            'artwork_detail' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/artwork/hash[/][:hash][/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Artwork',
                        'action'     => 'detail',
                    ),
                ),
            ),
            'artwork_share' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/artwork/hash/:hash/shared[/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                ),
                'child_routes' => [
                    'put' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'put',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Artwork',
                                'action' => 'shared',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,post,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Artwork',
                                'action' => 'invalidHeader',
                            ],
                        ],
                    ],
                ],
            ),
            'artwork_like' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/artwork/hash/:hash/liked[/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                ),
                'child_routes' => [
                    'put' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'put',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Artwork',
                                'action' => 'liked',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,post,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Artwork',
                                'action' => 'invalidHeader',
                            ],
                        ],
                    ],
                ],
            ),
        ),
    ),
);