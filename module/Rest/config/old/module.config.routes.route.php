<?php

return array(
    'router' => array(
        'routes' => array(
            'route_list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/route/list[/]',
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Route',
                        'action'     => 'list',
                    ),
                ),
            ),
            'route_detail' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/route/hash[/][:hash][/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Route',
                        'action'     => 'detail',
                    ),
                ),
            ),
            'route_detail_nodes' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/route/hash/:hash/nodes[/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Route',
                        'action'     => 'nodes',
                    ),
                ),
            ),
            'route_share' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/route/hash/:hash/shared[/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                ),
                'child_routes' => [
                    'put' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'put',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Route',
                                'action' => 'shared',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,post,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Route',
                                'action' => 'invalidHeader',
                            ],
                        ],
                    ],
                ],
            ),
            'route_like' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/route/hash/:hash/liked[/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                ),
                'child_routes' => [
                    'put' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'put',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Route',
                                'action' => 'liked',
                            ],
                        ],
                    ],
                    'invalid' => [
                        'type' => 'Zend\Mvc\Router\Http\Method',
                        'options' => [
                            'verb' => 'get,post,delete,patch,head,options',
                            'defaults' => [
                                'controller' => 'Rest\Controller\Route',
                                'action' => 'invalidHeader',
                            ],
                        ],
                    ],
                ],
            ),
        ),
    ),
);