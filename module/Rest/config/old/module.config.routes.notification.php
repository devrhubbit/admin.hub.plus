<?php

return array(
    'router' => array(
        'routes' => array(
            'notification_artist_detail' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/notification/artist/hash[/][:hash][/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Notification',
                        'action'     => 'artist',
                    ),
                ),
            ),
            'notification_artwork_detail' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/notification/artwork/hash[/][:hash][/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Notification',
                        'action'     => 'artwork',
                    ),
                ),
            ),
            'notification_event_detail' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/notification/event/hash[/][:hash][/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Notification',
                        'action'     => 'event',
                    ),
                ),
            ),
            'notification_route_detail' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/v2/notification/route/hash[/][:hash][/]',
                    'constraints' => array(
                        'hash'    => '[a-zA-Z0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Rest\Controller\Notification',
                        'action'     => 'route',
                    ),
                ),
            ),
        ),
    ),
);