<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Rest;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        //return include __DIR__ . '/config/module.config.php';
        $config = array();

        $configFiles = array(
            __DIR__ . '/config/module.config.php',                  // Module general config
            __DIR__ . '/config/module.config.v3.identity.php',      // Identity routes config
            __DIR__ . '/config/module.config.v3.error.php',         // Error routes config
            __DIR__ . '/config/module.config.v3.page.php',          // Page routes config
            __DIR__ . '/config/module.config.v3.media.php',         // Media routes config
            __DIR__ . '/config/module.config.v3.notification.php',  // Notification routes config
            __DIR__ . '/config/module.config.v3.ajax.php',          // Ajax routes config
        );

        // Merge all module config options
        foreach ($configFiles as $configFile) {
            $config = \Zend\Stdlib\ArrayUtils::merge($config, include $configFile);
        }

        return $config;
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
