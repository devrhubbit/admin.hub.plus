<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 26/05/17
 * Time: 17:48
 */

namespace Rest\Controller;

use Rest\Model\NotificationRestModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Router\Http\RouteMatch;
use Zend\View\Model\ViewModel;
use Zend\Http\PhpEnvironment\Request;
use Zend\Http\PhpEnvironment\Response;

use MessagesService\Service\Factory as MessagesFactory;

use Rest\Exception\RestApiKeySecurityException;
use Rest\Exception\RestAppIdSecurityException;

class RestNotificationController extends AbstractActionController
{
    protected $request_content;

    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /**
     * @param MvcEvent $e
     * @return mixed
     */
    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = new ViewModel;
        $this->viewModel->setTemplate('rest/response');
        $this->viewModel->setTerminal(true);

        /** @var Request $request */
        $request = $this->getRequest();

        $this->get_params = $request->getQuery();
        $this->post_params = $request->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        if ($this->getRequest()->getContent() != null) {
            $this->request_content = json_decode($this->getRequest()->getContent());
        }

        return parent::onDispatch($e);
    }

    public function sendNotificationAction()
    {
        $md5Uuid = (isset($this->get_params['token'])) ? $this->get_params['token'] : null;
        $notificationType = $this->route_params->getParam('type');

        /** @var Response $response */
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        $statusCode = 500;

        try {
            $notificationModel  = new NotificationRestModel($this);
            switch ($notificationType) {
                case MessagesFactory::MODE_PUSH:
                    $result = $notificationModel->sendPushNotification($this->request_content, $md5Uuid);
                    break;
                case MessagesFactory::MODE_MAIL:
                case MessagesFactory::MODE_SMS:
                default:
                    $result = null;
                    break;
            }
        } catch(RestAppIdSecurityException $e) {
            $res = array(
                'status'  => $statusCode,
                'success' => false,
                'message' => 'error_appid: ' . $e->getMessage(),
                'data' => null
            );
            $result = json_encode($res);
        } catch (RestApiKeySecurityException $e) {
            $res = array(
                'status'  => $statusCode,
                'success' => false,
                'message' => 'error_apikey: ' . $e->getMessage(),
                'data' => null
            );
            $result = json_encode($res);
        }

        if($notificationModel) {
            $response->setStatusCode($notificationModel->getDispatchedStatus());
        }

        return $this->viewModel->setVariable('response', $result);
    }
}