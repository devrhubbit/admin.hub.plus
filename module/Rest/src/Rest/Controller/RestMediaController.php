<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 18/07/16
 * Time: 16:00
 */

namespace Rest\Controller;

use Rest\Exception\RestApiKeySecurityException;
use Rest\Exception\RestAppIdSecurityException;

use Rest\Model\ExternalPageRestModel;
use Rest\Model\MediaRestModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

class RestMediaController extends AbstractActionController
{
    protected $request_content;

    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params \Zend\Mvc\Router\RouteMatch */
    protected $route_params;

    /**
     * @param MvcEvent $e
     * @return mixed
     */
    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = new ViewModel;
        $this->viewModel->setTemplate('rest/response');
        $this->viewModel->setTerminal(true);

        $this->get_params 	 = $this->getRequest()->getQuery();
        $this->post_params 	 = $this->getRequest()->getPost();
        $this->route_params  = $this->getEvent()->getRouteMatch();

        if ($this->getRequest()->getContent() != null) {
            $this->request_content = json_decode($this->getRequest()->getContent());
        }

        return parent::onDispatch($e);
    }

    public function likeMediaAction() {
        //die('likeMediaAction');
        try {
            $id   = $this->route_params->getParam('id');
            $connector_id = $this->route_params->getParam('connector_id');

            $result = null;

            if(is_null($connector_id)) {
                $model  = new MediaRestModel($this);
                $result = $model->like($id);
            } else {
                $model = new ExternalPageRestModel($this);
                $result = $model->likeRemoteContentById(ExternalPageRestModel::EXTERNAL_MEDIA, $connector_id, $id);
            }

            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            if($model) {
                $response->setStatusCode($model->getDispatchedStatus());
            }

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function unlikeMediaAction() {
        //die('unlikeMediaAction');
        try {
            $id   = $this->route_params->getParam('id');
            $connector_id = $this->route_params->getParam('connector_id');

            if(is_null($connector_id)) {
                $model  = new MediaRestModel($this);
                $result = $model->unlike($id);
            } else {
                $model = new ExternalPageRestModel($this);
                $result = $model->unlikeRemoteContentById(ExternalPageRestModel::EXTERNAL_MEDIA, $connector_id, $id);
            }

            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            if($model) {
                $response->setStatusCode($model->getDispatchedStatus());
            }

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function shareMediaAction() {
        //die('shareMediaAction');
        try {
            $id   = $this->route_params->getParam('id');
            $connector_id = $this->route_params->getParam('connector_id');

            if(is_null($connector_id)) {
                $model  = new MediaRestModel($this);
                $result = $model->share($id);
            } else {
                $model = new ExternalPageRestModel($this);
                $result = $model->shareRemoteContentById(ExternalPageRestModel::EXTERNAL_MEDIA, $connector_id, $id);
            }

            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            if($model) {
                $response->setStatusCode($model->getDispatchedStatus());
            }

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function uploadMediaAction() {
        try {
            $context = $this->route_params->getParam('context');
            $form_id = $this->route_params->getParam('id');
            $type    = $this->route_params->getParam('type');

            $media_type     = isset($_FILES['media']) ? $_FILES['media']['type']      : null;
            $media_tmp_name = isset($_FILES['media']) ? $_FILES['media']['tmp_name']  : null;
            $media_size     = isset($_FILES['media']) ? $_FILES['media']['size']      : null;
            $media_name     = isset($_FILES['media']) ? $_FILES['media']['name']      : null;

            $result = null;
            $model  = new MediaRestModel($this);

            switch($context) {
                case MediaRestModel::UPLOAD_CONTEXT_FORM:
                    $row_id = $this->get_params->id;
                    $field  = $this->get_params->field;
                    switch($type) {
                        case MediaRestModel::UPLOAD_TYPE_COVER:
                            $result = $model->uploadFormCover($form_id, $row_id, $field, $media_type, $media_tmp_name, $media_size, $media_name);
                            break;
                        case MediaRestModel::UPLOAD_TYPE_GALLERY:
                            $result = $model->uploadFormGalleryItem($form_id, $row_id, $field, $media_type, $media_tmp_name, $media_size, $media_name);
                            break;
                        default:
                            $result = $model->getJsonResponse(412);
                            break;
                    }
                    break;
                default:
                    $result = $model->getJsonResponse(412);
                    break;

            }

            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            if($model) {
                $response->setStatusCode($model->getDispatchedStatus());
            }

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function deleteMediaAction() {
        try {
            $id   = $this->route_params->getParam('id');
            $connector_id = $this->route_params->getParam('connector_id');

            if(is_null($connector_id)) {
                $model  = new MediaRestModel($this);
                $result = $model->delete($id);
            } else {
                $remoteMediaId = $this->get_params->media_id;
                $model = new ExternalPageRestModel($this);
                $result = $model->deleteRemoteContentById($connector_id, $remoteMediaId);
            }

            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            $response->setStatusCode($model->getDispatchedStatus());

            return $this->viewModel->setVariable('response', $result);
        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

}