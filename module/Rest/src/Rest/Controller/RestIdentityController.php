<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 18/07/16
 * Time: 16:00
 */

namespace Rest\Controller;

use Rest\Exception\RestApiKeySecurityException;
use Rest\Exception\RestAppIdSecurityException;
use Rest\Model\DeviceRestModel;
use Rest\Model\UserRestModel;
use Rest\Model\PageRestModel;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

class RestIdentityController extends AbstractActionController
{
    protected $request_content;

    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /* @var $dbHelper \Common\Helper\DbHelper */
    private $dbHelper = null;

    /**
     * @param MvcEvent $e
     * @return mixed
     */
    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = new ViewModel;
        $this->viewModel->setTemplate('rest/response');
        $this->viewModel->setTerminal(true);

        $this->get_params 	 = $this->getRequest()->getQuery();
        $this->post_params 	 = $this->getRequest()->getPost();
        $this->route_params  = $this->getEvent()->getRouteMatch();

        if ($this->getRequest()->getContent() != null) {
            $this->request_content = json_decode($this->getRequest()->getContent());
        }

        return parent::onDispatch($e);
    }

    /**
     * Thecnical support route, receives GET request to simply check if appId is valid or not
     * @return Response|ViewModel
     */
    public function checkAppIdAction() {
        $result = null;

        try {
            $dm = new DeviceRestModel($this);
            $result = $dm->checkAppId();
            $response = $this->getResponse();
            $response->setStatusCode($dm->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('error_appid');
        }

        return $this->viewModel->setVariable('response', $result);
    }


    /**
     * Receives POST request with JSON body like this:
     * {
     *      "enable_notification":  BOOLEAN,
     *      "device_token":         STRING,
     *      "device_os_version":    STRING,
     *      "device_type":          STRING,
     *      "device_os":            STRING
     * }
     *
     * respondes with status:
     * - SUCCESS => 201 / Created - and JSON data attribute like this: { "apyKey": STRING }
     *
     * @return ViewModel
     */
    public function insertDeviceAction() {
        try {
            $dm = new DeviceRestModel($this);
            $result = $dm->register(
                $this->request_content->device_type,
                $this->request_content->device_os,
                $this->request_content->device_os_version,
                $this->request_content->device_token,
                $this->request_content->enable_notification
            );

            $response = $this->getResponse();
            $response->setStatusCode($dm->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function loginAction() {
        try {
            $um   = new UserRestModel($this);

            $result = $um->login(
                $this->request_content->username,
                $this->request_content->password
            );

            $response = $this->getResponse();
            $response->setStatusCode($um->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function logoutAction() {
        try {
            $um   = new UserRestModel($this);

            $result = $um->logout();

            $response = $this->getResponse();
            $response->setStatusCode($um->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function detailProfileAction() {
        try {
            $um   = new UserRestModel($this);

            $result = $um->detail();

            $response = $this->getResponse();
            $response->setStatusCode($um->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    /**
     * Receives PUT request with JSON body like this:
     * {
     *      "enable_notification":  BOOLEAN,
     *      "old_token":            STRING,
     *      "new_token":            STRING,
     *      "device_os_version":    STRING,
     * }
     *
     * Respondes with status:
     * - SUCCESS => 202 / Accepted
     * - ERROR   => 412 / Precondition Failed (invalid "old_token" string recived)
     *
     * @return Response|ViewModel
     */
    public function updateDeviceAction() {
        try {
            $dm = new DeviceRestModel($this);

            $result = $dm->refresh(
                $this->request_content->old_token,
                $this->request_content->new_token,
                $this->request_content->device_os_version,
                $this->request_content->enable_notification
            );

            $response = $this->getResponse();
            $response->setStatusCode($dm->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function insertProfileAction() {
        try {
            $um   = new UserRestModel($this);

            $type       = isset($_FILES['avatar']) ? $_FILES['avatar']['type']      : null;
            $tmp_name   = isset($_FILES['avatar']) ? $_FILES['avatar']['tmp_name']  : null;

            $data = json_decode($this->post_params['data']);

            $section_id = $this->route_params->getParam('id');

            $result = $um->insert($data, $type, $tmp_name, $section_id);

            $response = $this->getResponse();
            $response->setStatusCode($um->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function updateProfileAction() {
        try {
            $um   = new UserRestModel($this);

            $type       = isset($_FILES['avatar']) ? $_FILES['avatar']['type']      : null;
            $tmp_name   = isset($_FILES['avatar']) ? $_FILES['avatar']['tmp_name']  : null;

            $data = json_decode($this->post_params['data']);

            $result = $um->update($data, $type, $tmp_name);

            $response = $this->getResponse();
            $response->setStatusCode($um->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function passwordRecoveryAction() {
        try {
            $um   = new UserRestModel($this);

            $result = $um->passwordRecovery($this->request_content->email);

            $response = $this->getResponse();
            $response->setStatusCode($um->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function insertUserDataAction() {
        try {
            $um   = new UserRestModel($this);

            $section_id = $this->route_params->getParam('id');
            $result = $um->insertUserData($section_id, $this->request_content);

            $response = $this->getResponse();
            $response->setStatusCode($um->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
//        catch (\Exception $e) {
//            error_log("TEST_HP - insertUserDataAction " . $e->getTraceAsString());
//        }

        return $this->viewModel->setVariable('response', $result);
    }

    public function updateUserDataAction() {
        try {
            $um   = new UserRestModel($this);

            $section_id = $this->route_params->getParam('id');
            $row_id = $this->get_params['row_id'];
            $result = $um->updateUserData($section_id, $this->request_content, $row_id);

            $response = $this->getResponse();
            $response->setStatusCode($um->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    /**
     * @return Response|ViewModel
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \Rest\Exception\RestSignatureSecurityException
     */
    public function getUserDataAction() {
        try {
            $pm   = new PageRestModel($this);
            $um   = new UserRestModel($this);

            $section_id = $this->route_params->getParam('id');
            $row_id = $this->get_params['row_id'];
            $section    = $pm->getSectionById($section_id);
            $result     = $um->getUserData($section, $row_id);

            $response = $this->getResponse();
            $response->setStatusCode($um->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch(RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch(RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function removeDeviceAction()
    {
        $dm = new DeviceRestModel($this, false);

        $type = (isset($this->get_params['type'])) ? $this->get_params['type'] : null;
        $hashId = (isset($this->get_params['hash_id'])) ? $this->get_params['hash_id'] : null;
        $status = (isset($this->get_params['status'])) ? $this->get_params['status'] : null;

        $result = null;
        if ($type == "push" && $status == "failed") {
            $result = $dm->removeDevice(
                $this->route_params->getParam('token')
            );
        }

        /** @var Response $response */
        $response = $this->getResponse();
        $response->setStatusCode($dm->getDispatchedStatus());
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        return $this->viewModel->setVariable('response', $result);
    }

}