<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 18/07/16
 * Time: 16:00
 */

namespace Rest\Controller;

use Rest\Model\HpRestModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

class RestErrorController extends AbstractActionController
{
    protected $viewModel;
    protected $appId;
    protected $get_params;
    protected $post_params;
    protected $route_params;
    protected $request_content;

    private $debug = false;

    private function isDebugMode() {
        return $this->debug;
    }

    /**
     * @param MvcEvent $e
     * @return mixed
     */
    public function onDispatch(MvcEvent $e)
    {
        $config = $this->getServiceLocator()->get('config');
        $this->debug = $config['debug'];

        $this->viewModel = new ViewModel;
        $this->viewModel->setTemplate('rest/response');
        $this->viewModel->setTerminal(true);

        $this->get_params 	 = $this->getRequest()->getQuery();
        $this->post_params 	 = $this->getRequest()->getPost();
        $this->route_params  = $this->getEvent()->getRouteMatch();

        if ($this->getRequest()->getContent() != null) {
            $this->request_content = json_decode($this->getRequest()->getContent());
        }

        return parent::onDispatch($e);
    }

    public function invalidMethodAction() {
        $rm = new HpRestModel($this, false);
        $result = $rm->getJsonResponse(405, null, null, ($this->isDebugMode() ? $this->route_params->getParam('error') : null));

        $response = $this->getResponse();
        $response->setStatusCode($rm->getDispatchedStatus());
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        return $this->viewModel->setVariable('response', $result);
    }

    public function invalidAppIdAction() {
        $rm = new HpRestModel($this, false);
        $result = $rm->getJsonResponse(403, null, null, ($this->isDebugMode() ? $this->route_params->getParam('error') : null));

        $response = $this->getResponse();
        $response->setStatusCode($rm->getDispatchedStatus());
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        return $this->viewModel->setVariable('response', $result);
    }

    public function invalidApiKeyAction() {
        $rm = new HpRestModel($this, false);
        $result = $rm->getJsonResponse(401, null, null, ($this->isDebugMode() ? $this->route_params->getParam('error') : null));

        $response = $this->getResponse();
        $response->setStatusCode($rm->getDispatchedStatus());
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        return $this->viewModel->setVariable('response', $result);
    }

    public function invalidProviderUuidAction() {
        $rm = new HpRestModel($this, false);
        $result = $rm->getJsonResponse(401, null, null, ($this->isDebugMode() ? $this->route_params->getParam('error') : null));

        $response = $this->getResponse();
        $response->setStatusCode($rm->getDispatchedStatus());
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        return $this->viewModel->setVariable('response', $result);
    }

    public function invalidHashIdAction() {
        $rm = new HpRestModel($this, false);
        $result = $rm->getJsonResponse(406, null, null, ($this->isDebugMode() ? $this->route_params->getParam('error') : null));

        $response = $this->getResponse();
        $response->setStatusCode($rm->getDispatchedStatus());
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        return $this->viewModel->setVariable('response', $result);
    }

    public function invalidSignatureAction() {
        $rm = new HpRestModel($this, false);
        $result = $rm->getJsonResponse(412, null, null, ($this->isDebugMode() ? $this->route_params->getParam('error') : null));

        $response = $this->getResponse();
        $response->setStatusCode($rm->getDispatchedStatus());
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        return $this->viewModel->setVariable('response', $result);
    }

    public function invalidParamsAction() {
        $rm = new HpRestModel($this, false);
        $result = $rm->getJsonResponse(400, null, null, ($this->isDebugMode() ? $this->route_params->getParam('error') : null));

        $response = $this->getResponse();
        $response->setStatusCode($rm->getDispatchedStatus());
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        return $this->viewModel->setVariable('response', $result);
    }

}