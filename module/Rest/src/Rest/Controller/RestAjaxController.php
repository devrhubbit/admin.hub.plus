<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 16/11/17
 * Time: 12:54
 */

namespace Rest\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\Http\RouteMatch;
use Zend\View\Model\ViewModel;

use Zend\Http\PhpEnvironment\Request;
use Zend\Http\PhpEnvironment\Response;

use Rest\Exception\RestApiKeySecurityException;
use Rest\Exception\RestAppIdSecurityException;
use Rest\Model\UserRestModel;

class RestAjaxController extends AbstractActionController
{

    protected $request_content;

    /* @var $viewModel ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /**
     * @param MvcEvent $e
     * @return mixed
     */
    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = new ViewModel();
        $this->viewModel->setTemplate('rest/response');
        $this->viewModel->setTerminal(true);

        /** @var Request $request */
        $request = $this->getRequest();

        $this->get_params = $request->getQuery();
        $this->post_params = $request->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        if ($this->getRequest()->getContent() != null) {
            $this->request_content = json_decode($this->getRequest()->getContent());
        }

        return parent::onDispatch($e);
    }

    /**
     * @return ViewModel
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getSelectOptionsAction()
    {
        $formId = $this->route_params->getParam('id');
        $fieldName = base64_decode($this->route_params->getParam('field'));
        $filter = $this->get_params['filter'];
        $row_id = $this->get_params['row_id'];

        /** @var Response $response */
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        $statusCode = 500;
        $result = null;

        try {
            $userRestModel  = new UserRestModel($this);
            $result = $userRestModel->getSelectOptions($formId, $fieldName, $filter, $row_id);
            $response->setStatusCode($userRestModel->getDispatchedStatus());
        } catch (RestAppIdSecurityException $e) {
            $res = array(
                'status'  => $statusCode,
                'success' => false,
                'message' => 'error_appid: ' . $e->getMessage(),
                'data' => null
            );
            $result = json_encode($res);
            $response->setStatusCode($statusCode);
        } catch (RestApiKeySecurityException $e) {
            $res = array(
                'status'  => $statusCode,
                'success' => false,
                'message' => 'error_apikey: ' . $e->getMessage(),
                'data' => null
            );
            $result = json_encode($res);
            $response->setStatusCode($statusCode);
        }

        return $this->viewModel->setVariable('response', $result);
    }
}