<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 15/02/17
 * Time: 16:57
 */

namespace Rest\Controller;

use Rest\Exception\RestApiKeySecurityException;
use Rest\Exception\RestAppIdSecurityException;
use Rest\Model\TrackerModel;
use Zend\Mvc\Controller\AbstractActionController,
    Zend\Mvc\Router\Http\RouteMatch,
    Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

class RestTrackerController extends AbstractActionController
{
    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /** @var TrackerModel tracker */
    protected $tracker;

    /**
     * @param MvcEvent $e
     * @return mixed
     */
    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = new ViewModel();
        $this->viewModel->setTemplate('rest/response');
        $this->viewModel->setTerminal(true);

        $this->route_params = $this->getEvent()->getRouteMatch();

        $this->tracker = new TrackerModel($this);

        return parent::onDispatch($e);
    }

    public function trackMessageAction()
    {
        $type = $this->route_params->getParam('type');
        $hashId = $this->route_params->getParam('hashId');

        try {
            $result = $this->tracker->track($type, $hashId);
            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            $response->setStatusCode($this->tracker->getDispatchedStatus());

            return $this->viewModel->setVariable('response', $result);
        } catch (RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch (RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }
}