<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 18/07/16
 * Time: 16:00
 */

namespace Rest\Controller;

use Database\HubPlus\ApplicationQuery;
use Rest\Exception\RestApiKeySecurityException;
use Rest\Exception\RestAppIdSecurityException;

use Rest\Exception\RestRemoteException;
use Rest\Exception\RestRemoteSectionFormatException;
use Rest\Exception\RestSignatureSecurityException;
use Rest\Model\ContentPageRestModel;
use Rest\Model\ExternalPageRestModel;
use Rest\Model\GenericPageRestModel;
use Rest\Model\PageRestModel;
use Rest\Model\EventPageRestModel;
use Rest\Model\FavouritePageRestModel;

use Rest\Model\PersonPageRestModel;
use Rest\Model\PoiPageRestModel;
use Rest\Model\SectionPageRestModel;
use Rest\Model\UserPageRestModel;
use Rest\Model\WebPageRestModel;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Router\Http\RouteMatch;
use Zend\Http\PhpEnvironment\Request;

use Zend\View\Model\JsonModel;

class RestPageController extends AbstractActionController
{
    const IOS_OS = "iOS";
    const ANDROID_OS = "Android";
    const UNKNOWN_OS = "Unknown";

    protected $request_content;

    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /**
     * @param MvcEvent $e
     * @return mixed
     */
    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = new ViewModel;
        $this->viewModel->setTemplate('rest/response');
        $this->viewModel->setTerminal(true);

        /** @var Request $request */
        $request = $this->getRequest();

        $this->get_params = $request->getQuery();
        $this->post_params = $request->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        if ($this->getRequest()->getContent() != null) {
            $this->request_content = json_decode($this->getRequest()->getContent());
        }

        return parent::onDispatch($e);
    }

    /**
     * Receives simple GET request without params.
     *
     * Respondes with status:
     * - SUCCESS => 200 / Ok - and JSON data attribute define in MAIN PAGE section of HUB+ docs
     *
     * @return Response|ViewModel
     */
    public function mainPageAction()
    {
        try {
            $pm = new PageRestModel($this);
            $result = $pm->main();

            $response = $this->getResponse();
            $response->setStatusCode($pm->getDispatchedStatus());
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

            return $this->viewModel->setVariable('response', $result);

        } catch (RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch (RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    /**
     * Receives GET request with :type param in url (es. /person/list/ , /event/list/, ...) to define related POST type
     * and query string params to filter dataset.
     *
     * Respondes with status:
     * - SUCCESS => 200 / Ok - and JSON data attribute define in HUB+ docs > LIST PAGE section
     *
     * @return Response|ViewModel
     */
    public function listPageAction()
    {
        try {
            $type = $this->route_params->getParam('type');

            $limit = (isset($this->get_params['limit'])) ? $this->get_params['limit'] : ContentPageRestModel::QUERY_LIMIT;
            $offset = (isset($this->get_params['offset'])) ? $this->get_params['offset'] : ContentPageRestModel::QUERY_OFFSET;
            $filter = (isset($this->get_params['filter'])) ? $this->get_params['filter'] : null;
            $tags = (isset($this->get_params['tags'])) ? $this->get_params['tags'] : null;
            $order = (isset($this->get_params['order'])) ? $this->get_params['order'] : 'ASC';

            $model = null;
            $result = null;

            switch ($type) {
                case PageRestModel::EXTERNAL_CONTENT://sortable DONE
                    $model = new ExternalPageRestModel($this);
                    $connector_id = $this->route_params->getParam('connector_id');
                    $result = $model->getList($connector_id, $this->get_params);
                    break;
                case PageRestModel::GENERIC_CONTENT://sortable DONE
                    $model = new GenericPageRestModel($this);
                    $result = $model->getList($limit, $offset, $filter, $tags, $order);
                    break;
                case PageRestModel::PERSON_CONTENT://sortable DONE
                    $model = new PersonPageRestModel($this);
                    $result = $model->getList($limit, $offset, $filter, $tags, $order);
                    break;
                case PageRestModel::EVENT_CONTENT://sortable DONE
                    $from = 0;
                    if (isset($this->get_params['from_current']) && $this->get_params['from_current'] === "1") {
                        $from = gmmktime('0', '0', '0', date("n"), date("j"), date("Y"));
                    } elseif (isset($this->get_params['from'])) {
                        $from = (int)$this->get_params['from'];
                    }

                    $to = strtotime('01-01-2037');
                    if (isset($this->get_params['to_current']) && $this->get_params['to_current'] === "1") {
                        $to = gmmktime('23', '59', '59', date("n"), date("j"), date("Y"));
                    } elseif (isset($this->get_params['to'])) {
                        $to = (int)$this->get_params['to'];
                    }

                    $model = new EventPageRestModel($this);
                    $result = $model->getList($limit, $offset, $filter, $tags, $from, $to, $order);
                    break;
                case PageRestModel::FAVOURITE_CONTENT:
                    $type = (isset($this->get_params['types'])) ? $this->get_params['types'] : null;

                    $model = new FavouritePageRestModel($this);
                    $result = $model->getList($limit, $offset, $filter, $tags, $type);
                    break;
                case PageRestModel::POI_CONTENT:
                    $model = new PoiPageRestModel($this);
                    $mode = $this->get_params['mode'];
                    $scan = (isset($this->get_params['scan'])) ? (boolean)$this->get_params['scan'] : false;

                    switch ($mode) {
                        case PoiPageRestModel::INDOOR_MODE:
                            $region_ids = (isset($this->get_params['regions'])) ? $this->get_params['regions'] : null;
                            $result = $model->getIndoorList($limit, $offset, $filter, $tags, $scan, $region_ids);
                            break;
                        case PoiPageRestModel::OUTDOOR_MODE:
                            $center = null;
                            $bbox = null;

                            $center = new \StdClass();
                            if (
                                array_key_exists('center_lat', $this->get_params) &&
                                array_key_exists('center_lng', $this->get_params)
                            ) {
                                $center->lat = $this->get_params['center_lat'];
                                $center->lng = $this->get_params['center_lng'];
                            } else {
                                return $this->redirect()->toRoute('error_params');
                            }

                            $bbox = new \StdClass();
                            $bbox->minLat = array_key_exists('min_lat', $this->get_params) ? $this->get_params['min_lat'] : null;
                            $bbox->minLng = array_key_exists('min_lng', $this->get_params) ? $this->get_params['min_lng'] : null;
                            $bbox->maxLat = array_key_exists('max_lat', $this->get_params) ? $this->get_params['max_lat'] : null;
                            $bbox->maxLng = array_key_exists('max_lng', $this->get_params) ? $this->get_params['max_lng'] : null;

                            $result = $model->getOutdoorList($limit, $offset, $filter, $tags, $scan, $center, $bbox);
                            break;
                        default:
                            return $this->redirect()->toRoute('error_params');
                            break;
                    }
                    break;
                case PageRestModel::WEB_CONTENT: //sortable DONE
                    $model = new WebPageRestModel($this);
                    $result = $model->getList($limit, $offset, $filter, $tags, $order);
                    break;
                case PageRestModel::SECTION_CONTENT: //sortable DONE
                    $id = (isset($this->get_params['id'])) ? $this->get_params['id'] : null;
                    if (is_null($id)) {
                        return $this->redirect()->toRoute('error_params', array('error' => "Missing 'id' param in query string"));
                    } else {
                        $model = new SectionPageRestModel($this);
                        $result = $model->getList($id, $limit, $offset, $filter, $tags, $order);
                    }
                    break;
                case PageRestModel::USER_CONTENT:
                    $id = (isset($this->get_params['id'])) ? $this->get_params['id'] : null;
                    if (is_null($id)) {
                        return $this->redirect()->toRoute('error_params', array('error' => "Missing 'id' param in query string"));
                    } else {
                        $model = new UserPageRestModel($this);
                        $result = $model->getList($id, $limit, $offset, $filter, $tags);
                    }
                    break;
                default:
                    return $this->redirect()->toRoute('error_params');
                    break;
            }

            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            if ($model) {
                $response->setStatusCode($model->getDispatchedStatus());
            }

            return $this->viewModel->setVariable('response', $result);

        } catch (RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid', array('error' => $e->getErrorMessage()));
        } catch (RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey', array('error' => $e->getErrorMessage()));
        } catch (RestRemoteSectionFormatException $e) {
            return $this->redirect()->toRoute('error_params', array('error' => $e->getErrorMessage()));
        } catch (RestRemoteException $e) {
            return $this->redirect()->toRoute('error_params', array('error' => $e->getErrorMessage()));
        } catch (RestSignatureSecurityException $e) {
            return $this->redirect()->toRoute('error_signature', array('error' => $e->getErrorMessage()));
        }
    }

    /**
     * Receives GET request with :type and :id param in url (es. /person/1/ , /event/25/, ...) to identify related POST.
     *
     * Responds with status:
     * - SUCCESS => 200 / Ok - and JSON data attribute define in HUB+ docs > DETAIL PAGE section
     *
     * @return Response|ViewModel
     */
    public function detailPageAction()
    {
        try {
            $id = $this->route_params->getParam('id');
            $type = $this->route_params->getParam('type');

            $model = null;
            $result = null;

            switch ($type) {
                case PageRestModel::EXTERNAL_CONTENT:
                    $model = new ExternalPageRestModel($this);
                    $connector_id = $this->route_params->getParam('connector_id');
                    $result = $model->getRemoteContentById(ExternalPageRestModel::EXTERNAL_CONTENT, $connector_id, $id, $this->get_params);
                    break;
                case PageRestModel::GENERIC_CONTENT:
                case PageRestModel::WEB_CONTENT:
                case PageRestModel::PERSON_CONTENT:
                case PageRestModel::EVENT_CONTENT:
                case PageRestModel::POI_CONTENT:
                    $model = new ContentPageRestModel($this);
                    $result = $model->getContentById($id);
                    break;
                case PageRestModel::USER_CONTENT:
                    $model = new ContentPageRestModel($this);
                    $result = $model->getUserContentById($id);
                    break;
                default:
                    return $this->redirect()->toRoute('error_params', array('error' => "Undefined content type used"));
                    break;
            }

            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            if ($model) {
                $response->setStatusCode($model->getDispatchedStatus());
            }

            return $this->viewModel->setVariable('response', $result);

        } catch (RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch (RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        } catch (RestRemoteSectionFormatException $e) {
            return $this->redirect()->toRoute('error_params', array('error' => $e->getErrorMessage()));
        } catch (RestRemoteException $e) {
            return $this->redirect()->toRoute('error_params', array('error' => $e->getErrorMessage()));
        } catch (RestSignatureSecurityException $e) {
            return $this->redirect()->toRoute('error_signature', array('error' => $e->getErrorMessage()));
        }
    }

    public function likePageAction()
    {
        try {
            $id = $this->route_params->getParam('id');
            $type = $this->route_params->getParam('type');

            $model = null;
            $result = null;

            switch ($type) {
                case PageRestModel::EXTERNAL_CONTENT:
                    $model = new ExternalPageRestModel($this);
                    $connector_id = $this->route_params->getParam('connector_id');
                    $result = $model->likeRemoteContentById(ExternalPageRestModel::EXTERNAL_CONTENT, $connector_id, $id);
                    break;
                case PageRestModel::GENERIC_CONTENT:
                case PageRestModel::PERSON_CONTENT:
                case PageRestModel::EVENT_CONTENT:
                case PageRestModel::POI_CONTENT:
                    $model = new ContentPageRestModel($this);
                    $result = $model->like($id);
                    break;
                case PageRestModel::USER_CONTENT:
                    $model = new ContentPageRestModel($this);
                    $result = $model->likeUserContent($id);
                    break;
                default:
                    return $this->redirect()->toRoute('error_params');
                    break;
            }

            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            if ($model) {
                $response->setStatusCode($model->getDispatchedStatus());
            }

            return $this->viewModel->setVariable('response', $result);

        } catch (RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch (RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function unlikePageAction()
    {
        try {
            $id = $this->route_params->getParam('id');
            $type = $this->route_params->getParam('type');

            $model = null;
            $result = null;

            switch ($type) {
                case PageRestModel::EXTERNAL_CONTENT:
                    $model = new ExternalPageRestModel($this);
                    $connector_id = $this->route_params->getParam('connector_id');
                    $result = $model->unlikeRemoteContentById(ExternalPageRestModel::EXTERNAL_CONTENT, $connector_id, $id);
                    break;
                case PageRestModel::GENERIC_CONTENT:
                case PageRestModel::PERSON_CONTENT:
                case PageRestModel::EVENT_CONTENT:
                case PageRestModel::POI_CONTENT:
                    $model = new ContentPageRestModel($this);
                    $result = $model->unlike($id);
                    break;
                case PageRestModel::USER_CONTENT:
                    $model = new ContentPageRestModel($this);
                    $result = $model->unlikeUserContent($id);
                    break;
                default:
                    return $this->redirect()->toRoute('error_params');
                    break;
            }

            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            if ($model) {
                $response->setStatusCode($model->getDispatchedStatus());
            }

            return $this->viewModel->setVariable('response', $result);

        } catch (RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch (RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function sharePageAction()
    {
        try {
            $id = $this->route_params->getParam('id');
            $type = $this->route_params->getParam('type');

            $model = null;
            $result = null;

            switch ($type) {
                case PageRestModel::EXTERNAL_CONTENT:
                    $model = new ExternalPageRestModel($this);
                    $connector_id = $this->route_params->getParam('connector_id');
                    $result = $model->shareRemoteContentById(ExternalPageRestModel::EXTERNAL_CONTENT, $connector_id, $id);
                    break;
                case PageRestModel::GENERIC_CONTENT:
                case PageRestModel::PERSON_CONTENT:
                case PageRestModel::EVENT_CONTENT:
                case PageRestModel::POI_CONTENT:
                    $model = new ContentPageRestModel($this);
                    $result = $model->share($id);
                    break;
                case PageRestModel::USER_CONTENT:
                    $model = new ContentPageRestModel($this);
                    $result = $model->shareUserContent($id);
                    break;
                default:
                    return $this->redirect()->toRoute('error_params');
                    break;
            }

            $response = $this->getResponse();
            $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
            if ($model) {
                $response->setStatusCode($model->getDispatchedStatus());
            }

            return $this->viewModel->setVariable('response', $result);

        } catch (RestAppIdSecurityException $e) {
            return $this->redirect()->toRoute('error_appid');
        } catch (RestApiKeySecurityException $e) {
            return $this->redirect()->toRoute('error_apikey');
        }
    }

    public function getSectionsListAction()
    {
        $md5Uuid = (isset($this->get_params['token'])) ? $this->get_params['token'] : null;

        /** @var Response $response */
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        $statusCode = 500;

        try {
            $model = new ExternalPageRestModel($this);
            $result = $model->getSectionsList($md5Uuid);
        } catch (RestAppIdSecurityException $e) {
            $res = array(
                'status'  => $statusCode,
                'success' => false,
                'message' => 'error_appid: ' . $e->getMessage(),
                'data' => null
            );
            $result = json_encode($res);
        } catch (RestApiKeySecurityException $e) {
            $res = array(
                'status'  => $statusCode,
                'success' => false,
                'message' => 'error_apikey: ' . $e->getMessage(),
                'data' => null
            );
            $result = json_encode($res);
        }

        if($model) {
            $statusCode = $model->getDispatchedStatus();
        }

        $response->setStatusCode($statusCode);

        return $this->viewModel->setVariable('response', $result);
    }

    public function appUrlAction()
    {
        $type = $this->route_params->getParam('type');
        $hashId = $this->route_params->getParam('hashId');

        try {
            $result = null;

            /** @var Response $response */
            $response = $this->getResponse();

            switch ($type) {
                case PageRestModel::SECTION_CONTENT:
                    $model = new SectionPageRestModel($this);
                    $result = $model->getSectionToSectionByHashId($hashId);
                    $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
                    $response->setStatusCode($model->getDispatchedStatus());
                    break;
                case PageRestModel::CONTENT_CONTENT:
                    $model = new ContentPageRestModel($this);
                    $result = $model->getContentByHashId($hashId);
                    $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');
                    $response->setStatusCode($model->getDispatchedStatus());
                    break;
            }

            return $this->viewModel->setVariable('response', $result);
        } catch (RestAppIdSecurityException $exception) {
            return $this->getAppStorePage();
        } catch (RestApiKeySecurityException $exception) {
            return $this->getAppStorePage();
        }
    }

    /**
     * @return ViewModel
     */
    private function getAppStorePage()
    {
        $config = $this->getServiceLocator()->get('config');

        /** @var Request $request */
        $request = $this->getRequest();
        $userAgent = $request->getHeaders()->get('useragent')->toString();

        $OS = self::UNKNOWN_OS;

        if (preg_match('/iPad|iPhone|iPod/i', $userAgent, $regs)) {
            $OS = self::IOS_OS;
        }

        if (preg_match('/android/i', $userAgent, $regs)) {
            $OS = self::ANDROID_OS;
        }

        $this->viewModel->setTemplate('rest/app_store');

        $application = ApplicationQuery::create()
            ->findOne();

        return $this->viewModel->setVariables(
            array(
                'cdnBaseUrl' => $config['cdn']['baseurl'],
                'appLogo' => $application->getAppLogo(),
                'operatingSystem' => $OS,
                'appStoreUrl' => $config['hub_plus_store_links']['app_store'],
                'googlePlayUrl' => $config['hub_plus_store_links']['play_store'],
            ));
    }

}