<?php

namespace Rest\Model;

use Common\Helper\StringHelper;
use Rest\Exception\RestApiKeySecurityException;

class WebPageRestModel extends ContentPageRestModel
{

    private static $sqlList = "SELECT SQL_CALC_FOUND_ROWS p.`id` AS pid, p.*, pl.*, m.`id` AS media_id,
                                  m.`type` AS media_type, m.`uri` AS media_uri, m.`title` AS media_title, m.`description` AS media_description,
                                  m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.size AS media_size,
                                  m.count_like AS media_count_like, m.count_share AS media_count_share, ml.liked_at AS media_liked_at,
                                  IF(JSON_VALID(p.`title`), JSON_EXTRACT(p.`title`, '$.%s'), CONCAT('\"', p.`title`, '\"')) AS title_ordered
                                FROM
                                  `post` AS p
                                  LEFT JOIN `media` AS m ON (p.`cover_id` = m.`id` AND m.`deleted_at` IS NULL)
                                  LEFT JOIN `post_log` AS pl ON ( p.`id` = pl.`post_id` AND pl.`user_id` = :user_id1 )
                                  LEFT JOIN `media_log` AS ml ON ( m.`id` = ml.`media_id` AND ml.`user_id` = :user_id2 )
                                WHERE
                                  p.`deleted_at` IS NULL AND p.`type` = :type
                                  AND `status` = 0 AND `search` LIKE :search
                                  %s
                                ORDER BY title_ordered %s LIMIT :offset, :limit";

    private static $getById = "SELECT p.*, pl.*, m.`id` AS media_id,
                                  m.`type` AS media_type, m.`uri` AS media_uri, m.`title` AS media_title, m.`description` AS media_description,
                                  m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.size AS media_size,
                                  m.count_like AS media_count_like, m.count_share AS media_count_share, ml.liked_at AS media_liked_at
                                FROM
                                  `post` AS p
                                  LEFT JOIN `media` AS m ON (p.`cover_id` = m.`id` AND m.`deleted_at` IS NULL)
                                  LEFT JOIN `post_log` AS pl ON ( p.`id` = pl.`post_id`)
                                  LEFT JOIN `media_log` AS ml ON ( m.`id` = ml.`media_id`)
                                WHERE p.`deleted_at` IS NULL AND p.`status` = 0 AND p.`id` = :id";


    public function getList($limit = 5, $offset = 0, $filter = "", $tags = null, $order = "ASC")
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                $device_lang = strtolower($this->getDeviceLang());
                $where = "";
                $widthTags = ($tags != null && count($tags) > 0);

                if ($widthTags) {
                    $where = " AND MATCH (`tags`) AGAINST (:tags  IN BOOLEAN MODE) ";
                }

                $section = $this->getRegistrationCustomFormSection();
                $labelsTag = $this->getRegistrationFormLabelsTag($section);
                if (count($labelsTag) > 0) {
                    $userRestModel = new UserRestModel($this->getController());
                    $userTags = $userRestModel->getFormTagsData($section->id, $user_id, $labelsTag);
                    if (count($userTags) > 0) {
                        $userTags = StringHelper::parseMatchAgainstArray($userTags);
                        $where .= " AND (MATCH (`tags_user`) AGAINST ('$userTags' IN BOOLEAN MODE) OR `tags_user` IS NULL) ";
                    } else {
                        $where .= " AND `tags_user` IS NULL ";
                    }
                }

                $stmt = $this->getDb()->prepare(sprintf(self::$sqlList, $device_lang, $where, $order));

                $type = strtoupper(self::WEB_CONTENT);
                $search = StringHelper::parseLikeString($filter);

                $stmt->bindParam(":user_id1", $user_id, \PDO::PARAM_INT);
                $stmt->bindParam(":user_id2", $user_id, \PDO::PARAM_INT);
                $stmt->bindParam(":type", $type, \PDO::PARAM_STR);
                $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
                $stmt->bindParam(":offset", $offset, \PDO::PARAM_INT);
                $stmt->bindParam(":search", $search, \PDO::PARAM_STR);

                if ($tags) {
                    $pTags = StringHelper::parseMatchAgainstArray(($tags) ? $this->parseTags($tags) : array());
                    $stmt->bindParam(":tags", $pTags, \PDO::PARAM_STR);
                }

                $stmt->execute();

                $contents = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\ContentSerializer', array("model" => $this, "mode" => self::MODE_MASTER));

                $page = new \stdClass();
                $header = new \stdClass();
                $body = new \stdClass();

                $header->demo = ((int)$this->getApp()->demo == 0) ? false : true;
                $header->white_label = ((int)$this->getApp()->white_label == 0) ? false : true;
                $header->rows = count($contents);
                $header->total_rows = (int)$this->getTotalRows();

                $body->sections = null;
                $body->contents = $contents;

                $page->type = self::TYPE_LIST;
                $page->header = $header;
                $page->body = $body;

                $response = $this->getJsonResponse(200, $page);
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function getItemById($id)
    {
        $result = null;

        $stmt = $this->getDb()->prepare(self::$getById);
        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $result = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\ContentSerializer', array("model" => $this, "mode" => self::MODE_MASTER));
        }

        return $result;
    }
}