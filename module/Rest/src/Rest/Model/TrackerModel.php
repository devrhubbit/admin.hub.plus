<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 15/02/17
 * Time: 17:00
 */

namespace Rest\Model;

use Database\HubPlus\PushNotificationDeviceQuery;
use Rest\Exception\RestApiKeySecurityException;
use MessagesService\Service\Factory as MessagesFactory;

class TrackerModel extends HpRestModel
{

    public function track($type, $hashId)
    {
        if($this->isValidRequest()) {
            $result = $this->getJsonResponse(400);
            switch ($type) {
                case MessagesFactory::MODE_MAIL:
//                $this->getJsonResponse(200);
//                $result = $this->getPixel();
                    break;
                case MessagesFactory::MODE_PUSH:
                    if($this->trackPush($hashId)) {
                        $result = $this->getJsonResponse(200);
                    } else {
                        $result = $this->getJsonResponse(202);
                    }
                    break;
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $result;
    }

    private function trackPush($hashId) {
        $result = false;

        $pushNotificationDevice = PushNotificationDeviceQuery::create()
            ->filterByReadAt(null)
            ->findOneByPushNotificationHashId($hashId);

        if(!is_null($pushNotificationDevice)) {
            $pushNotificationDevice
                ->setReadAt(time())
                ->save();

            $result = true;
        }

        return $result;
    }

    private function getPixel()
    {
        $pixel = null;

        ignore_user_abort(true);

        // turn off gzip compression
        if (function_exists('apache_setenv')) {
            apache_setenv('no-gzip', 1);
        }

        ini_set('zlib.output_compression', 0);

        // turn on output buffering if necessary
        if (ob_get_level() == 0) {
            ob_start();
        }

        // removing any content encoding like gzip etc.
        header('Content-encoding: none', true);

        //check to ses if request is a POST
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // the GIF should not be POSTed to, so do nothing...
            $pixel = ' ';
        } else {
            header('Content-disposition: filename="post-tracking"');

            // return 1x1 pixel transparent gif
            header("Content-type: image/gif");
            // needed to avoid cache time on browser side
            header("Content-Length: 42");
            header("Cache-Control: private, no-cache, no-cache=Set-Cookie, proxy-revalidate");
            header("Expires: Wed, 11 Jan 2000 12:59:00 GMT");

            /* $d = date("D, d M Y G:i:s", time());
              header("Expires:".$d." GMT"); */

            header("Last-Modified: Wed, 11 Jan 2006 12:59:00 GMT");
            header("Pragma: no-cache");

            $pixel = sprintf('%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%', 71, 73, 70, 56, 57, 97, 1, 0, 1, 0, 128, 255, 0, 192, 192, 192, 0, 0, 0, 33, 249, 4, 1, 0, 0, 0, 0, 44, 0, 0, 0, 0, 1, 0, 1, 0, 0, 2, 2, 68, 1, 0, 59);
        }

        // flush all output buffers. No reason to make the user wait for OWA.
        ob_flush();
        flush();
        ob_end_flush();

        return $pixel;
    }
}