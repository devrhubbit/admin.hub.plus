<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 20/07/16
 * Time: 12:14
 */

namespace Rest\Model;


use Application\Model\MediaModel;
use Application\Model\SectionModel;
use Common\Helper\ImageHelper;
use Database\HubPlus\Media;
use Database\HubPlus\MediaLogQuery;
use Database\HubPlus\MediaQuery;
use Database\HubPlus\SectionConnectorQuery;
use Database\HubPlus\UserAppQuery;
use Rest\Exception\RestApiKeySecurityException;
use Rest\Exception\RestRemoteException;
use Zend\Http\Client;

class MediaRestModel extends HpRestModel
{
    const LOG_VIEW = "VIEW";
    const LOG_LIKE = "LIKE";
    const LOG_UNLIKE = "UNLIKE";
    const LOG_SHARE = "SHARE";

    const UPLOAD_CONTEXT_ARCHIVE = "archive";
    const UPLOAD_CONTEXT_FORM = "form";

    const UPLOAD_TYPE_COVER = "cover";
    const UPLOAD_TYPE_COVER_ACTION = "action";
    const UPLOAD_TYPE_GALLERY = "gallery";

    private static $sqlExtraLog = "INSERT INTO `media_extra_log` (`id`, `media_id`, `device_id`, `type`, `timestamp`)
                                       VALUES (NULL, :media_id, :dev_id, :type, NOW());";

    private static $sqlAddMediaLog = "INSERT INTO `media_log`
                                         (`id`, `media_id`, `user_id`, `liked_at`, `shared_at`, `share_count`)
                                       VALUES
                                         (NULL, :media_id, :user_id, :liked_at, :shared_at, :share_count);";

    private static $sqlMediaLogShare = "UPDATE media_log SET share_count = share_count+1, shared_at = NOW() WHERE id = :id;";
    private static $sqlMediaLogLike = "UPDATE media_log SET liked_at = NOW() WHERE id = :id;";
    private static $sqlMediaLogUnlike = "UPDATE media_log SET liked_at = NULL  WHERE id = :id;";

    private static $sqlMediaLike = "UPDATE media SET count_like  = count_like+1  WHERE id = :media_id AND deleted_at IS NULL;";
    private static $sqlMediaUnlike = "UPDATE media SET count_like  = count_like-1  WHERE id = :media_id AND deleted_at IS NULL;";
    private static $sqlMediaShare = "UPDATE media SET count_share = count_share+1 WHERE id = :media_id AND deleted_at IS NULL;";

    private static $sqlGallery = "SELECT \"%s\" AS media_context, m.`id` AS media_id, m.`type` AS media_type, m.`uri` AS media_uri,
                                    m.`title` AS media_title, m.`description` AS media_description,
                                    m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.`size` AS media_size,
                                    m.`count_like` AS media_count_like, m.`count_share` AS media_count_share, ml.`liked_at` AS media_liked_at
                                  FROM
                                    `gallery` AS g JOIN `media` AS m ON g.`media_id` = m.`id`
                                    LEFT JOIN `media_log` AS ml ON ( m.`id` = ml.`media_id` AND ml.`user_id` = :user_id )
                                  WHERE
                                    m.`deleted_at` IS NULL AND g.`post_id` = :post_id
                                  ORDER BY g.`weight`;";

    private static $sqlMediaById = "SELECT \"%s\" AS media_context, %d AS media_form_id, \"%s\" AS cover_format, m.`id` AS media_id, m.`type` AS media_type, m.`uri` AS media_uri,
                                        m.`title` AS media_title, m.`description` AS media_description, m.`format` AS media_format,
                                        m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.`size` AS media_size,
                                        m.`count_like` AS media_count_like, m.`count_share` AS media_count_share, ml.`liked_at` AS media_liked_at
                                    FROM
                                        media AS m
                                        LEFT JOIN media_log AS ml ON ( m.`id` = ml.`media_id` AND ml.`user_id` = :user_id )
                                    WHERE m.`deleted_at` IS NULL AND m.`id`=:id";

    private static $sqlFormGallery = "SELECT \"%s\" AS media_context, m.`id` AS media_id, m.`type` AS media_type,
                                        m.`uri` AS media_uri, m.`title` AS media_title, m.`description` AS media_description,
                                        m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.`size` AS media_size,
                                        g.`form_id` AS media_form_id, g.`row_id` AS media_row_id,
                                        m.`count_like` AS media_count_like, m.`count_share` AS media_count_share,
                                        ml.`liked_at` AS media_liked_at
                                      FROM
                                        `gallery_form` AS g JOIN `media` AS m ON g.`media_id` = m.`id`
                                        LEFT JOIN `media_log` AS ml ON ( m.`id` = ml.`media_id` AND ml.`user_id` = :user_id )
                                      WHERE
                                        m.`deleted_at` IS NULL AND g.`form_id` = :form_id AND g.`row_id` = :row_id
                                      ORDER BY g.`weight`;";

    private static $sqlUploadFormCover = "UPDATE `custom_form_%s` SET `%s` = :media_id WHERE `id` = :id;";

    private static $sqlMediaDel = "UPDATE `media` SET deleted_at = NOW() WHERE deleted_at IS NULL AND `id` = :id ";

    private static $sqlMediaAdd = "INSERT INTO `media`
                                             (`id`, `device_id`, `type`, `uri`, `weight`, `title`, `description`,
                                              `extension`, `mime_type`, `size`, `created_at`, `updated_at`, `deleted_at`)
                                            SELECT
                                             NULL, :device_id, :type, :uri, COALESCE(MAX(`weight`) + 10, 10), :title, NULL, :ext, :mime, :size, NOW(), NOW(), NULL
                                            FROM `media`;";

    private static $sqlGalleryFormAdd = "INSERT INTO `gallery_form` (`media_id`, `form_id`, `row_id`, `field_name`, `weight`)
                                            VALUES (:media_id, :form_id, :row_id, :field, 0);";


    private function getMediaLog($media_id, $user_id)
    {
        $user_id = $user_id === 0 ? null : $user_id;

        $mediaLog = MediaLogQuery::create()
            ->filterByMediaId($media_id)
            ->filterByUserId($user_id)
            ->findOne();

        $logId = null;
        if ($mediaLog) {
            $logId = $mediaLog->getId();
        }

        return $logId;
    }

    private function addMediaLog($media_id, $user_id, $liked_at = 0, $shared_at = 0)
    {
        $stmt = $this->getDb()->prepare(self::$sqlAddMediaLog);

        $share_count = 0;
        $like_date = null;
        $share_date = null;

        if ($liked_at > 0) {
            $like_date = date('Y-m-d G:i:s', $liked_at);
        }

        if ($shared_at > 0) {
            $share_count++;
            $share_date = date('Y-m-d G:i:s', $shared_at);
        }

        $user_id = $user_id === 0 ? null : $user_id;

        $stmt->bindParam(":media_id", $media_id, \PDO::PARAM_INT);
        $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
        $stmt->bindParam(":liked_at", $like_date, \PDO::PARAM_STR);
        $stmt->bindParam(":shared_at", $share_date, \PDO::PARAM_STR);
        $stmt->bindParam(":share_count", $share_count, \PDO::PARAM_INT);

        $stmt->execute();
    }

    private function addExtraLog($media_id, $device_id, $type)
    {
        $stmt = $this->getDb()->prepare(self::$sqlExtraLog);

        $stmt->bindParam(":media_id", $media_id, \PDO::PARAM_INT);
        $stmt->bindParam(":dev_id", $device_id, \PDO::PARAM_INT);
        $stmt->bindParam(":type", $type, \PDO::PARAM_STR);

        $stmt->execute();
    }

    public function like($media_id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                if ($user_id != 0) {
                    $device_id = (int)$this->getDevice()->id;
                    $this->addExtraLog($media_id, $device_id, self::LOG_LIKE);
                    $step = "ADD EXTRA LOG";

                    $logId = $this->getMediaLog($media_id, $user_id);
                    $step = "GET MEDIA LOG";

                    if (!is_null($logId)) {
                        $stmt = $this->getDb()->prepare(self::$sqlMediaLogLike);
                        $stmt->bindParam(":id", $logId, \PDO::PARAM_INT);
                        $stmt->execute();

                        $step = "SELECT MEDIA LOG";
                    } else {
                        $this->addMediaLog($media_id, $user_id, time(), 0);
                        $step = "ADD MEDIA LOG";
                    }

                    $stmt = $this->getDb()->prepare(self::$sqlMediaLike);
                    $stmt->bindParam(":media_id", $media_id, \PDO::PARAM_INT);
                    $stmt->execute();
                    $step = "UPDATE MEDIA LOG";

                    $response = $this->getJsonResponse(202);
                } else {
                    $response = $this->getJsonResponse(412);
                }
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $step . " - " . $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function unlike($media_id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                if ($user_id != 0) {
                    $device_id = (int)$this->getDevice()->id;
                    $this->addExtraLog($media_id, $device_id, self::LOG_UNLIKE);

                    $logId = $this->getMediaLog($media_id, $user_id);

                    if (!is_null($logId)) {
                        $stmt = $this->getDb()->prepare(self::$sqlMediaLogUnlike);
                        $stmt->bindParam(":id", $logId, \PDO::PARAM_INT);
                        $stmt->execute();

                        $stmt2 = $this->getDb()->prepare(self::$sqlMediaUnlike);
                        $stmt2->bindParam(":media_id", $media_id, \PDO::PARAM_INT);
                        $stmt2->execute();
                    }

                    $response = $this->getJsonResponse(202);
                } else {
                    $response = $this->getJsonResponse(412);
                }
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function share($media_id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                $device_id = (int)$this->getDevice()->id;

                $this->addExtraLog($media_id, $device_id, self::LOG_SHARE);
                $logId = $this->getMediaLog($media_id, $user_id);

                if (!is_null($logId)) {
                    $stmt = $this->getDb()->prepare(self::$sqlMediaLogShare);
                    $stmt->bindParam(":id", $logId, \PDO::PARAM_INT);
                    $stmt->execute();
                } else {
                    $this->addMediaLog($media_id, $user_id, 0, time());
                }

                $stmt = $this->getDb()->prepare(self::$sqlMediaShare);
                $stmt->bindParam(":media_id", $media_id, \PDO::PARAM_INT);
                $stmt->execute();

                $response = $this->getJsonResponse(202);
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function getFormMediaById($form_id, $media_id)
    {
        $res = array();

        if ($this->isValidRequest()) {
            $user_id = $this->getDevice()->user_id;
            $stmt = $this->getDb()->prepare(sprintf(self::$sqlMediaById, self::UPLOAD_CONTEXT_FORM, $form_id, ImageHelper::TMB_SIZE . "x" . ImageHelper::TMB_SIZE));

            $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
            $stmt->bindParam(":id", $media_id, \PDO::PARAM_INT);

            if ($stmt->execute()) {
                $results = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\MediaSerializer', array("model" => $this));
                $res = $results[0];
            }
        }

        return $res;
    }

    public function getGalleryByContentId($id)
    {
        $res = array();

        if ($this->isValidRequest()) {
            $user_id = $this->getDevice()->user_id;
            $stmt = $this->getDb()->prepare(sprintf(self::$sqlGallery, self::UPLOAD_CONTEXT_ARCHIVE));

            $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
            $stmt->bindParam(":post_id", $id, \PDO::PARAM_INT);

            $stmt->execute();

            $res = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\MediaSerializer', array("model" => $this));
        }

        return $res;
    }

    public function getGalleryByFormId($form_id, $row_id)
    {
        $res = null;

        if ($this->isValidRequest()) {
            $user_id = $this->getDevice()->user_id;
            $stmt = $this->getDb()->prepare(sprintf(self::$sqlFormGallery, self::UPLOAD_CONTEXT_FORM));

            $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
            $stmt->bindParam(":form_id", $form_id, \PDO::PARAM_INT);
            $stmt->bindParam(":row_id", $row_id, \PDO::PARAM_INT);

            if ($stmt->execute()) {
                $res = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\MediaSerializer', array("model" => $this));
            }
        }

        return $res;
    }

    /**
     * @param $form_id
     * @param $row_id
     * @param $field_name
     * @param $media_type
     * @param $media_tmp_name
     * @param $media_size
     * @param $media_name
     * @return null|string
     * @throws RestApiKeySecurityException
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function uploadFormCover($form_id, $row_id, $field_name, $media_type, $media_tmp_name, $media_size, $media_name)
    {
        $response = null;
        try {
            if ($this->isValidRequest()) {

                $thumb_url = $this->cdnBasePath . ImageHelper::getFormThumbUrl($form_id);
                $low_url = $this->cdnBasePath . ImageHelper::getFormLowUrl($form_id);
                $medium_url = $this->cdnBasePath . ImageHelper::getFormMediumUrl($form_id);
                $high_url = $this->cdnBasePath . ImageHelper::getFormHighUrl($form_id);

                $media_id = $this->uploadMedia($media_type, $media_tmp_name, $media_size, $media_name, $thumb_url, $low_url, $medium_url, $high_url);

                if (is_numeric($form_id)) {
                    if ($media_id != null) {
                        $username = null;

                        if (!is_null($this->getDb())) {
                            $escape_form_id = (int)$form_id;
                            $escape_field_name = base64_encode($field_name);

                            $sql = sprintf(self::$sqlUploadFormCover, $escape_form_id, $escape_field_name);
                            $stmt = $this->getDb()->prepare($sql);

                            $stmt->bindParam(":id", $row_id, \PDO::PARAM_INT);
                            $stmt->bindParam(":media_id", $media_id, \PDO::PARAM_INT);

                            if ($stmt->execute()) {
                                $userModel = new UserRestModel($this->getController());
                                $CustomFormUser = $userModel->getCustomFormUser($form_id, $row_id);

                                if ($CustomFormUser !== null) {
                                    $CustomFormUserData = json_decode($CustomFormUser);
                                    if ($CustomFormUserData !== null) {
                                        $userModel->updateCustomFormUserCover($form_id, $row_id, $media_id);
                                    }
                                }

                                $sectionModel = new SectionModel($this->controller->getServiceLocator());
                                $registrationForm = $sectionModel->getRegistrationForm();

                                $responseData = null;
                                if ($registrationForm !== null) {
                                    $user_id = $userModel->getUserIdFromCustomForm($form_id, $row_id);

                                    $user = UserAppQuery::create()
                                        ->findPk($user_id);
                                    if ($user) {
                                        $username = $user->getUsernameCanonical();
                                    }

                                    if ($stmt->rowCount() > 0) {
                                        $form_id = $registrationForm->getId();
                                    }
                                }
                            }
                        }

                        $media = MediaQuery::create()
                            ->findPk($media_id);

                        $cdnMediaUrl = $this->getCdnBaseurl() . ImageHelper::getFormHighUrl($form_id) . $media->getUri();
                        try {
                            $responseData = $this->callRemoteConnector($form_id, $field_name, $cdnMediaUrl, $media, self::UPLOAD_TYPE_COVER, $username, "POST", $row_id);
                            $response = $this->getJsonResponse(201);
                        } catch (RestRemoteException $exception) {
                            $response = $this->getJsonResponse($exception->getRemoteStatusCode(), null, null, $exception->getMessage());
                        }
                    }
                } else {
                    $response = $this->getJsonResponse(417);
                }
            }
        } catch (\PDOException $ex) {
            if ($this->isDebug()) {
                $response = $this->getJsonResponse(500, $ex->getMessage());
            } else {
                $response = $this->getJsonResponse(500);
            }
        }

        return $response;
    }

    /**
     * @param $form_id
     * @param $row_id
     * @param $field_name
     * @param $media_type
     * @param $media_tmp_name
     * @param $media_size
     * @param $media_name
     * @return null|string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function uploadFormGalleryItem($form_id, $row_id, $field_name, $media_type, $media_tmp_name, $media_size, $media_name)
    {
        $response = null;

        try {
            if ($this->isValidRequest()) {
                $thumb_url = $this->cdnBasePath . ImageHelper::getFormThumbUrl($form_id);
                $low_url = $this->cdnBasePath . ImageHelper::getFormLowUrl($form_id);
                $medium_url = $this->cdnBasePath . ImageHelper::getFormMediumUrl($form_id);
                $high_url = $this->cdnBasePath . ImageHelper::getFormHighUrl($form_id);

                $media_id = $this->uploadMedia($media_type, $media_tmp_name, $media_size, $media_name, $thumb_url, $low_url, $medium_url, $high_url);

                if (is_numeric($form_id) && is_numeric($row_id)) {
                    if ($media_id != null) {
                        $username = null;

                        if (!is_null($this->getDb())) {
                            $escape_form_id = (int)$form_id;
                            $escape_row_id = (int)$row_id;
                            $escape_field_name = base64_encode($field_name);

                            $stmt = $this->getDb()->prepare(self::$sqlGalleryFormAdd);

                            $stmt->bindParam(":media_id", $media_id, \PDO::PARAM_INT);
                            $stmt->bindParam(":form_id", $escape_form_id, \PDO::PARAM_INT);
                            $stmt->bindParam(":row_id", $escape_row_id, \PDO::PARAM_INT);
                            $stmt->bindParam(":field", $escape_field_name, \PDO::PARAM_STR);

                            if ($stmt->execute()) {
                                $sectionModel = new SectionModel($this->controller->getServiceLocator());
                                $registrationForm = $sectionModel->getRegistrationForm();

                                $responseData = null;
                                if ($registrationForm !== null) {
                                    $userModel = new UserRestModel($this->getController());
                                    $user_id = $userModel->getUserIdFromCustomForm($form_id, $row_id);
                                    $username = null;
                                    $user = UserAppQuery::create()
                                        ->findPk($user_id);
                                    if ($user) {
                                        $username = $user->getUsernameCanonical();
                                    }
                                }
                            }
                        }

                        $media = MediaQuery::create()
                            ->findPk($media_id);

                        $cdnMediaUrl = $this->getCdnBaseurl() . ImageHelper::getFormHighUrl($form_id) . $media->getUri();
                        try {
                            $responseData = $this->callRemoteConnector($form_id, $field_name, $cdnMediaUrl, $media, self::UPLOAD_TYPE_GALLERY, $username, "POST", $row_id);
                            $response = $this->getJsonResponse(201);
                        } catch (RestRemoteException $exception) {
                            $response = $this->getJsonResponse($exception->getRemoteStatusCode(), null, null, $exception->getMessage());
                        }
                    }
                } else {
                    $response = $this->getJsonResponse(417);
                }
            }
        } catch (\PDOException $ex) {
            if ($this->isDebug()) {
                $response = $this->getJsonResponse(500, $ex->getTrace());
            } else {
                $response = $this->getJsonResponse(500);
            }
        }

        return $response;
    }

    private function uploadMedia($type, $tmp_name, $size, $name, $thumb_url = null, $low_url = null, $medium_url = null, $high_url = null, $cover_url = null)
    {
        $media_id = null;

        if (!is_null($type) && !is_null($tmp_name)) {
            $imgHelper = new ImageHelper();
            $filename = $imgHelper->getRandomFilename($type);
            $ext = $imgHelper->getExtensionByType($type);

            if ($thumb_url != null) $imgHelper->saveImageCropped(ImageHelper::TMB_SIZE, $tmp_name, $thumb_url, $filename, false);
            if ($low_url != null) $imgHelper->saveImageScaled(ImageHelper::HTH_SIZE, $tmp_name, $low_url, $filename, false);
            if ($medium_url != null) $imgHelper->saveImageScaled(ImageHelper::HTH_SIZE, $tmp_name, $medium_url, $filename, false);
            if ($high_url != null) $imgHelper->saveImageScaled(ImageHelper::HTH_SIZE, $tmp_name, $high_url, $filename, false);


            $media_id = $this->add("IMAGE", $name, $filename, $type, $ext, $size);//$this->add("IMAGE", $tmp_name, $filename);
            unlink($tmp_name);
        }

        return $media_id;
    }

    public function add($type, $name, $uri, $mime, $extension, $size)
    {
        $res = -1;

        if (!is_null($this->getDb())) {
            $stmt = $this->getDb()->prepare(self::$sqlMediaAdd);
            $cleanName = htmlspecialchars($name);

            $device_id = $this->getDevice()->id;

            $stmt->bindParam(":device_id", $device_id, \PDO::PARAM_INT);
            $stmt->bindParam(":type", $type, \PDO::PARAM_STR);
            $stmt->bindParam(":uri", $uri, \PDO::PARAM_STR);
            $stmt->bindParam(":title", $cleanName, \PDO::PARAM_STR);
            $stmt->bindParam(":ext", $extension, \PDO::PARAM_STR);
            $stmt->bindParam(":mime", $mime, \PDO::PARAM_STR);
            $stmt->bindParam(":size", $size, \PDO::PARAM_INT);

            $stmt->execute();
            $res = $this->getDb()->lastInsertId();
        }

        return $res;
    }

    public function delete($id)
    {
        $response = null;

        try {
            if ($this->isValidRequest()) {
                if (!is_null($this->getDb())) {
                    $stmt = $this->getDb()->prepare(self::$sqlMediaDel);
                    $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
                    if ($stmt->execute()) {
                        $response = $this->getJsonResponse(200);
                    } else {
                        $response = $this->getJsonResponse(400);
                    }
                } else {
                    $response = $this->getJsonResponse(417);
                }
            } else {
                $response = $this->getJsonResponse(406);
            }
        } catch (\PDOException $ex) {
            if ($this->isDebug()) {
                $response = $this->getJsonResponse(500, $ex->getTrace());
            } else {
                $response = $this->getJsonResponse(500);
            }
        }
        return $response;
    }

    /**
     * @param $section_id
     * @param $field_name
     * @param $mediaUrl
     * @param Media $media
     * @param $response
     * @param null $username
     * @param string $method
     * @param null $row_id
     * @return mixed|null
     * @throws RestRemoteException
     * @throws \Propel\Runtime\Exception\PropelException
     */
    private function callRemoteConnector($section_id, $field_name, $mediaUrl, Media $media, $response, $username = null, $method = "POST", $row_id = null)
    {
        $data = null;

        $sectionConnector = SectionConnectorQuery::create()
            ->findOneBySectionId($section_id);

        if ($sectionConnector) {
            $providerUUID = $sectionConnector->getRemoteProvider()->getUuid();
            $remoteUrl = $sectionConnector->getBaseurl();

            $headers = array(
                "Content-HMAC: " . base64_encode(hash_hmac("sha256", $remoteUrl . $media->getUri() . $media->getMimeType(), $providerUUID)),
                "Device-Lang: " . $this->getDeviceLang(),
                "demo: " . $this->getDemoFlag(),
                "cache-control: no-cache",
                "Hp-Response: " . $response,
                "Hp-Field: " . rawurlencode($field_name),
            );

            if ($username !== null) {
                $headers[] = "User-ID: " . base64_encode(hash_hmac("sha256", $username, $providerUUID));
            }

            if ($row_id !== null) {
                $headers[] = "Row-Id: " . $row_id;
            }

            $client = new Client();
            $client->setMethod($method);
            $client->setOptions(array(
                'maxredirects' => 5,
                'timeout' => 30
            ));
            $client->setUri($remoteUrl);
            $client->setFileUpload($mediaUrl, 'media', null, $media->getMimeType());
            $client->setHeaders($headers);

//            error_log("TEST_HP - Async Media Remote URL: " . $remoteUrl);
//            error_log("TEST_HP - Async Media  Headers: " . print_r($headers, 1));
//            error_log("TEST_HP - Async Media Body: " . rawurlencode($field_name));


            $response = $client->send();
            if ($response->isSuccess()) {
//                error_log("TEST_HP - Async Media Response: " . $response->getBody());
                $data = json_decode($response->getBody());
            } else {
                $remoteException = new RestRemoteException();
                $remoteException->setRemoteStatusCode($response->getStatusCode());
                throw $remoteException;
            }
        }

        return $data;
    }
}