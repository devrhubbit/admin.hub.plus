<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 26/07/16
 * Time: 12:50
 */

namespace Rest\Model;


use Application\Model\MediaModel;
use Application\Model\NotificationModel;
use Application\Model\SectionModel;
use Common\Event\FormEventManager;
use Common\Helper\SecurityHelper;
use Common\Helper\ImageHelper;
use Common\Helper\StringHelper;
use Database\HubPlus\DeviceQuery;
use Database\HubPlus\PushNotificationDeviceQuery;
use Database\HubPlus\PushNotificationQuery;
use Database\HubPlus\RemoteLogQuery;
use Database\HubPlus\RemoteProviderQuery;
use Database\HubPlus\SectionConnector;
use Database\HubPlus\SectionConnectorQuery;
use Database\HubPlus\SectionQuery;
use Database\HubPlus\UserAppQuery;
use Form\Notification\NotificationForm;
use Form\Section\CustomForm\CustomFieldForm;
use Form\Section\CustomForm\CustomFormSectionForm;
use MessagesService\Exception\MessagesServicePushException;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;
use Rest\Exception\RestApiKeySecurityException;
use MessagesService\Exception\MessagesServiceMailException;
use MessagesService\Model\MessagesModel,
    MessagesService\Model\MessageModel as Message,
    MessagesService\Service\Factory as MessagesFactory;
use Rest\Exception\RestRemoteException;
use Rest\Exception\RestSignatureSecurityException;
use Zend\Http\Client;

class UserRestModel extends HpRestModel
{
    const USER_CUSTOM_FORM = "form";
    const USER_LOGIN = "login";
    const USER_RECOVERY_PASSWORD = "password";

    protected $remoteFields = array();
    protected $fields = array();
    protected $keys = array();
    protected $values = array();
    protected $mailData = array();

    private static $enabled = 1;

    private static $sqlAddressInsert = "INSERT INTO `address` (`id`, `address`, `city`, `state`, `country`, `zipcode`)
                                        VALUES (NULL, :address, :city, :state, :country, :zipcode);";

    private static $sqlAddressUpdate = "UPDATE `address` SET `address` = :address, `city` = :city, `zipcode` = :zipcode WHERE `id`=:id;";

    private static $sqlDeviceLink = "UPDATE `device` SET `user_id` = :user_id WHERE `deleted_at` IS NULL AND `id` = :id";

    private static $sqlDeviceUnlink = "UPDATE `device` SET `user_id` = NULL WHERE `deleted_at` IS NULL AND `id` = :id";

    private static $sqlGetCustomFormIds = "SELECT `id` FROM `section` WHERE `type`='CUSTOM_FORM' AND `status` = 0 AND `deleted_at` IS NULL";
    private static $sqlResetCustomFormDeviceId = "UPDATE `custom_form_%s` SET `device_id` = NULL WHERE `device_id`=:id";

    private static $sqlUserById = "SELECT * FROM `user_app` LEFT JOIN `address` ON `user_app`.address_id = `address`.id
                                      WHERE `user_app`.`deleted_at` IS NULL AND `user_app`.`id` = :id;";

    private static $sqlUserInsert = "INSERT INTO `user_app` (
                                        `id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `privacy`, `terms`,
                                        `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`,
                                        `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`,
                                        `firstname`, `lastname`, `phone`, `updated_at`, `created_at`, `deleted_at`,
                                        `address_id`, `tags`, `section_id`)
                                     VALUES (
                                        NULL, :username, :username_c, :email, :email_c, :enabled, :privacy, :terms,
                                        :salt, :password, NULL, '0', '0', NULL, :confirm, NULL, :role, '0', NULL,
                                        :firstname, :lastname, :phone, NOW(), NOW(), NULL, :address_id, :tag, :section_id);";

    private static $sqlUserAvatar = "UPDATE `user_app` SET `imagePath` = :avatar WHERE `deleted_at` IS NULL AND id = :id;";

    private static $sqlCustomFormUser = "SELECT * FROM `custom_form_%d` WHERE `id` = :id";

    private static $sqlUpdateCustomFormUserCover = "UPDATE `custom_form_%d` SET `cover_id` = :cover_id WHERE `id` = :id";

    private function uploadAvatar($type, $tmp_name, $id)
    {
        $filename = null;

        if (!is_null($type) && !is_null($tmp_name)) {
            $imgHelper = new ImageHelper();
            $filename = $imgHelper->getRandomFilename($type);
            $app_user_id = (int)$this->getApp()->user_id;

            $tmb_url = $this->cdnBasePath . ImageHelper::getHostThumbUrl($app_user_id);
            $hgh_url = $this->cdnBasePath . ImageHelper::getHostHighUrl($app_user_id);

            $imgHelper->saveImageCropped(ImageHelper::TMB_SIZE, $tmp_name, $tmb_url, $filename, false);
            $imgHelper->saveImageScaled(ImageHelper::HTH_SIZE, $tmp_name, $hgh_url, $filename, false);

            unlink($tmp_name);
        }

        $stmt = $this->getDb()->prepare(self::$sqlUserAvatar);

        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
        $stmt->bindParam(":avatar", $filename, \PDO::PARAM_STR);

        $stmt->execute();
    }

    private function encodePassword($salt, $password)
    {
        return md5($salt . $password);
    }

    /**
     * @param $username
     * @param $password
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function login($username, $password)
    {
        $response = null;
        $responseData = null;

        if ($this->isValidRequest()) {

            $sectionModel = new SectionModel($this->controller->getServiceLocator());
            $registrationForm = $sectionModel->getRegistrationForm();

            $device_id = (int)$this->getDevice()->id;

            $device = DeviceQuery::create()
                ->findPk($device_id);

            if ($device) {
                $username_c = StringHelper::getCanonical($username);
                try {
                    if ($registrationForm !== null) {
                        // Remote connector call
                        $remoteConnectorFields = array(
                            'auth' => array(
                                'username' => $username_c,
                                'password' => $password,
                            ),
                            'fields' => array(),
                            'operation' => "READ",
                        );

                        $responseData = $this->callRemoteConnector($registrationForm->getId(), $remoteConnectorFields, self::USER_LOGIN, $username_c);
                    }

                    if ($responseData === null) {
                        $user = UserAppQuery::create()
                            ->filterByUsernameCanonical($username_c)
                            ->findOne();

                        if ($user) {
                            $encodePassword = $this->encodePassword($user->getSalt(), $password);
                            if ($encodePassword == $user->getPassword()) {
                                // Device link
                                $device
                                    ->setUserId($user->getId())
                                    ->save();
                                $response = $this->getJsonResponse(202);
                            } else {
                                $response = $this->getJsonResponse(412);
                            }
                        } else {
                            $response = $this->getJsonResponse(400);
                        }

                    } else {
                        $user = UserAppQuery::create()
                            ->filterByUsernameCanonical($username_c)
                            ->findOneOrCreate();

                        $user
                            ->setUsername($username)
                            ->setUsernameCanonical($username_c)
                            ->setEmail($username)
                            ->setEmailCanonical($username_c)
                            ->setEnabled(1)
                            ->setPrivacy(1)
                            ->setTerms(1)
                            ->setLocked(0)
                            ->setExpired(0)
                            ->setLastLogin(time())
                            ->setRoles("HOST")
                            ->setCredentialsExpired(0)
                            ->setSectionId($registrationForm->getId());

                        if (isset($password) && $password != null && $password != "") {
                            $salt = SecurityHelper::generateSalt(32);
                            $encPassword = $this->encodePassword($salt, $password);
                            $confirm = SecurityHelper::generateKey();

                            $user
                                ->setSalt($salt)
                                ->setPassword($encPassword)
                                ->setConfirmationToken($confirm);
                        }

                        $user->save();

                        // Device link
                        $device
                            ->setUserId($user->getId())
                            ->save();

                        $sql = sprintf(self::$sqlGetFormDataByUserId, $registrationForm->getId());
                        $stmt = $this->getDb()->prepare($sql);
                        $stmt->bindParam(":id", $user->getId(), \PDO::PARAM_INT);

                        if ($stmt->execute()) {
                            $userData = $stmt->fetch(\PDO::FETCH_OBJ);
                            $formParams = $this->getFormParams($registrationForm->getId());
                            $fieldsData = $this->parseFieldset($formParams->fieldsets);
                            $content = new \stdClass();

                            $content->registration = new \stdClass();
                            $content->registration->email = $user->getEmailCanonical();
                            $content->registration->password = $password;
                            $content->registration->privacy = 1;
                            $content->registration->terms = 1;
                            $content->data = $responseData->fields;

                            $this->parseContentData($content->data, $fieldsData);

                            if ($userData !== false) {
                                $this->updateCustomFormFields($device_id, $user->getId(), $registrationForm->getId(), $content);
                            } else {
                                $this->insertCustomFormFields($device_id, $user->getId(), $registrationForm->getId(), $content);
                            }
                        }

                        $response = $this->getJsonResponse(202, $responseData);
                    }
                } catch (PropelException $exception) {
                    $response = $this->getJsonResponse(500, $exception->getMessage());
                } catch (\PDOException $ex) {
                    if ($this->isDebug()) {
                        $response = $this->getJsonResponse(500, $ex->getMessage());
                    } else {
                        $response = $this->getJsonResponse(500);
                    }
                } catch (RestRemoteException $exception) {
                    $response = $this->getJsonResponse($exception->getRemoteStatusCode(), json_decode($responseData), null, $exception->getCustomMessage());
                } catch (RestSignatureSecurityException $exception) {
                    $response = $this->getJsonResponse(500, null, null, $exception->getMessage());
                }
            } else {
                $response = $this->getJsonResponse(417);
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    /**
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function logout()
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $device_id = (int)$this->getDevice()->id;

                $stmt = $this->getDb()->prepare(self::$sqlDeviceUnlink);
                $stmt->bindParam(":id", $device_id, \PDO::PARAM_INT);

                if ($stmt->execute()) {
                    $stmt2 = $this->getDb()->prepare(self::$sqlGetCustomFormIds);
                    if ($stmt2->execute()) {
                        $formIds = $stmt2->fetchAll(\PDO::FETCH_OBJ);
                        foreach ($formIds as $formId) {
                            $stmt3 = $this->getDb()->prepare(sprintf(self::$sqlResetCustomFormDeviceId, $formId->id));
                            $stmt3->bindParam(":id", $device_id, \PDO::PARAM_INT);
                            $stmt3->execute();
                        }
                    }

                    $response = $this->getJsonResponse(202);
                } else {
                    $response = $this->getJsonResponse(400);
                }
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    /**
     * @param $email
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function passwordRecovery($email)
    {
        $response = null;
        $responseData = null;

        if ($this->isValidRequest()) {
            $sectionModel = new SectionModel($this->controller->getServiceLocator());
            $registrationForm = $sectionModel->getRegistrationForm();

            $email_c = StringHelper::getCanonical($email);

            try {
                if ($registrationForm !== null) {
                    // Remote connector call
                    $remoteConnectorFields = array(
                        'auth' => array(
                            'username' => $email_c,
                        ),
                        'fields' => array(),
                        'operation' => "UPDATE",
                    );

                    $responseData = $this->callRemoteConnector($registrationForm->getId(), $remoteConnectorFields, self::USER_RECOVERY_PASSWORD, $email_c);
                }

                if ($responseData === null) {
                    $userApp = UserAppQuery::create()
                        ->filterByUsernameCanonical($email_c)
                        ->filterByEnabled(1)
                        ->findOne();

                    if ($userApp) {
                        $config = $this->controller->getServiceLocator()->get('config');
                        $salt = SecurityHelper::generateSalt(32);
                        $pwd = SecurityHelper::generateSalt(4);

                        $password = $this->encodePassword($salt, $pwd);

                        $userApp->setSalt($salt);
                        $userApp->setPassword($password);
                        $userApp->save();

                        $app_icon = null;
                        if ($this->getApp()->icon != "") {
                            $app_icon = $this->getCdnBaseurl() . \Common\Helper\ImageHelper::getAppIconUrl() . $this->getApp()->icon;
                        } else {
                            $app_icon = $this->getBaseurl() . "/img/placeholder/app-icon.png";
                        }

                        $message = new Message("...");
                        $message->setOption('jsonParams', "params from application config");
                        $message->setOption('from', $config['email_message']['sender.email']);
                        $message->setOption('senderLabel', $this->getApp()->title . " APP");
                        $message->setOption('to', $email);
                        $message->setOption('subject', strtoupper($this->getApp()->title) . ": new password");
                        $message->setOption('templateLayout', 'email/app/password-recovery');
                        $message->setOption('templateParams', json_encode(['app_icon' => $app_icon, 'app_name' => $this->getApp()->title, 'password' => $pwd,]));

                        $msgModel = new MessagesModel($this->controller->getServiceLocator(), MessagesFactory::MODE_MAIL);
                        $msgModel->pullMessage($message);

                        $response = $this->getJsonResponse(202);
                    } else {
                        $response = $this->getJsonResponse(412);
                    }
                } else {
                    // Al momento non fa niente
                    $response = $this->getJsonResponse(202, json_decode($responseData));
                }
            } catch (PropelException $exception) {
                $response = $this->getJsonResponse(500, $exception->getMessage());
            } catch (\PDOException $ex) {
                $response = $this->getJsonResponse(500, $ex->getMessage());
            } catch (RestRemoteException $exception) {
                $response = $this->getJsonResponse($exception->getRemoteStatusCode(), json_decode($responseData), null, $exception->getCustomMessage());
            } catch (RestSignatureSecurityException $exception) {
                $response = $this->getJsonResponse(500, null, null, $exception->getMessage());
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    /**
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function detail()
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;

                if ($user_id != 0) {
                    $stmt = $this->getDb()->prepare(self::$sqlUserById);
                    $stmt->bindParam(":id", $user_id, \PDO::PARAM_INT);

                    if ($stmt->execute()) {
                        $user = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\UserSerializer', array("model" => $this))[0];
                        $response = $this->getJsonResponse(202, $user);
                    } else {
                        $response = $this->getJsonResponse(400);
                    }
                } else {
                    $response = $this->getJsonResponse(406);
                }
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    //temporary work around to use stringed tag list ready for new realase
    private static $sqlGetUserTagIds = "SELECT id FROM tag WHERE deleted_at IS NULL AND `type` = :type AND label IN (%s);";

    public function getUserTagIds($tags)
    {
        $result = [];
        $type = "REGISTER";
        $parsedTags = '"' . implode('","', explode(",", $tags)) . '"';
        $stmt = $this->getDb()->prepare(sprintf(self::$sqlGetUserTagIds, $parsedTags));

        $stmt->bindParam(":type", $type, \PDO::PARAM_STR);

        if ($stmt->execute()) {
            $result = array_map(function ($var) {
                return (int)$var['id'];
            }, $stmt->fetchAll(\PDO::FETCH_ASSOC));
        }

        //$result = implode(",", $data);

        return $result;
    }

    private static $sqlGetFormParams = "SELECT `title`, `params` FROM `section`
                                                 WHERE `deleted_at` IS NULL AND `status`=0 AND `type` = 'CUSTOM_FORM' AND `id`=:id;";
    private static $sqlGetFormDataByUserId = "SELECT * FROM `custom_form_%s` WHERE user_id   = :id ORDER BY id DESC LIMIT 1;";
    private static $sqlGetFormDataByDeviceId = "SELECT * FROM `custom_form_%s` WHERE device_id = :id ORDER BY id DESC LIMIT 1;";
    private static $sqlGetFormDataByRowId = "SELECT * FROM `custom_form_%s` WHERE id = :id ORDER BY id DESC LIMIT 1;";
    private static $sqlGetAddress = "SELECT * FROM `address` WHERE id=:id";

    public function getFormTagsData($section_id, $user_id, $labelTags)
    {
        $result = array();

        $stmt = $this->getDb()->prepare(sprintf(self::$sqlGetFormDataByUserId, $section_id));
        $stmt->bindParam(":id", $user_id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $data = $stmt->fetchObject();
            if ($data !== false) {
                foreach ($labelTags as $labelTag) {
                    $labelTag = str_replace("`", "", $labelTag);
                    $values = explode(",", $data->{$labelTag});
                    foreach ($values as $value) {
                        $value = trim($value);
                        if ($value !== "") {
                            $result[] = $value;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $formId
     * @param $fieldName
     * @param $filter
     * @param null $row_id
     * @return null|string
     * @throws PropelException
     * @throws RestApiKeySecurityException
     * @throws RestSignatureSecurityException
     */
    public function getSelectOptions($formId, $fieldName, $filter, $row_id = null)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $selectField = $this->getSelectField($formId, $fieldName, $filter, $row_id);
                if ($selectField !== null) {
                    $response = $this->getJsonResponse(200, $selectField);
                } else {
                    $response = $this->getJsonResponse(500, $selectField, null, "$fieldName not found!");
                }
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, null, null, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    private function getAddressById($id)
    {
        $result = null;
        $stmt = $this->getDb()->prepare(self::$sqlGetAddress);
        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $result = $stmt->fetchObject();
        }

        return $result;
    }

    private function getUserById($user_id)
    {
        $result = null;

        $stmt = $this->getDb()->prepare(self::$sqlUserById);
        $stmt->bindParam(":id", $user_id, \PDO::PARAM_INT);


        if ($stmt->execute()) {
            $result = $stmt->fetchObject();
        }

        return $result;
    }

    /**
     * @param $section_id
     * @return bool|\Database\HubPlus\SectionConnector
     */
    private function getConnector($section_id)
    {
        $res = false;
        $sectionConnector = SectionConnectorQuery::create()
            ->filterBySectionId($section_id)
            ->findOne();

        if ($sectionConnector) {
            $res = $sectionConnector;
        }

        return $res;
    }

    /**
     * @param $section
     * @param $params
     * @param $device_id
     * @param null $user_id
     * @param null $row_id
     * @return mixed
     * @throws PropelException
     * @throws RestRemoteException
     * @throws RestSignatureSecurityException
     */
    private function getFormData($section, $params, $device_id, $user_id = null, $row_id = null)
    {
//        var_dump($params); die();

        $section_id = $section->id;

        // Multi Language manager
        $lang = strtolower($this->getDeviceLang());
        if ($section->type == CustomFormSectionForm::CUSTOM_FORM) {
            foreach ($params->fieldsets as $fieldSet) {
                $label = isset($fieldSet->label) ? json_encode($fieldSet->label) : $fieldSet->name;
                $fieldSet->label = $this->getLocaleText($lang, $label);
                foreach ($fieldSet->fields as $f) {
                    if (isset($f->name)) {
                        $f->label = $this->getLocaleText($lang, json_encode($f->label));
                        switch ($f->type) {
                            case SectionModel::HPFORM_TAG_GROUP:
                            case SectionModel::HPFORM_RADIOBOX:
                            case SectionModel::HPFORM_CHECKBOX:
                                $tags = array();
                                foreach ($f->value as $keyTag => $tagObj) {
                                    $tags[$keyTag] = (string)$this->getLocaleText($lang, json_encode($tagObj));
                                }
                                $f->value = $tags;
                                break;
                            case SectionModel::HPFORM_SELECT:
                                $tags = array();
                                $filter = null;
                                $placeholder = isset($f->value->placeholder) ? $f->value->placeholder->{$lang} : CustomFieldForm::HP_FORM_SELECT_DEFAULT_EMPTY_OPTION;
                                if ($f->value->origin_data === CustomFieldForm::HP_FORM_SELECT_TYPE_LOCALE &&
                                    $f->value->related_to === CustomFieldForm::HP_FORM_SELECT_RELATED_TO_NULL_LABEL) {
                                    $tags[""] = (string)$this->getLocaleText($lang, $placeholder);
                                    foreach ($f->value->option_values as $keyTag => $tagObj) {
                                        $tags[$keyTag] = (string)$this->getLocaleText($lang, json_encode($tagObj->label));
                                    }
                                    $tags = count($tags) <= 1 ? array() : $tags;
                                } elseif ($f->value->origin_data === CustomFieldForm::HP_FORM_SELECT_TYPE_REMOTE &&
                                    $f->value->related_to === CustomFieldForm::HP_FORM_SELECT_RELATED_TO_NULL_LABEL) {
                                    // Get Remote URL
                                    try {
                                        $tags = $this->getRemoteSelectOptions($section_id, "select", $f->name, $placeholder);
                                    } catch (RestRemoteException $exception) {
                                        $tags = array();
                                    }
                                }
                                $f->value->option_values = $tags;
                                break;
                            case SectionModel::HPFORM_IMAGE:
                                break;
                            default:
                                $f->value = ($f->value !== "") ? $this->getLocaleText($lang, json_encode($f->value)) : "";
                                break;
                        }
                    }
                    $this->remoteFields[] = array('label' => isset($f->name) ? $f->name : $f->label, 'value' => "", 'type' => $f->type, 'validator' => $f->validator);
                }
            }
        }

        $result = null;
        $data = false;
        $imageHelper = new ImageHelper();

        $connector = $this->getConnector($section_id);
        if ($connector !== false /* && !is_null($row_id) */) {
            $remoteConnectorFields = array(
                'auth' => array(),
                'fields' => $this->remoteFields,
                'operation' => "READ",
            );

            $username = null;
            if (!is_null($user_id)) {
                $user = UserAppQuery::create()
                    ->findPk($user_id);
                if ($user) {
                    $username = $user->getUsernameCanonical();
                }
            }
            $data = $this->callRemoteConnector($section_id, $remoteConnectorFields, self::USER_CUSTOM_FORM, $username, $row_id);
            foreach ($params->fieldsets as $fieldSet) {
                foreach ($fieldSet->fields as $f) {
                    $fieldNameToCheck = isset($f->name) ? $f->name : $f->label;
                    if (property_exists($data, $fieldNameToCheck)) {
                        $data->{base64_encode($fieldNameToCheck)} = $data->{$fieldNameToCheck};
                        unset($data->{$fieldNameToCheck});
                    }
                }
            }

            if (is_null($data)) {
                $data = false;
            }
        } else {
            if ($params->form_type === CustomFormSectionForm::USER_FORM) {
                $stmt = $this->getDb()->prepare(sprintf(self::$sqlGetFormDataByRowId, $section_id));
                $stmt->bindParam(":id", $row_id, \PDO::PARAM_INT);
            } else {
                if (is_null($user_id)) {
                    $stmt = $this->getDb()->prepare(sprintf(self::$sqlGetFormDataByDeviceId, $section_id));
                    $stmt->bindParam(":id", $device_id, \PDO::PARAM_INT);
                } else {
                    $stmt = $this->getDb()->prepare(sprintf(self::$sqlGetFormDataByUserId, $section_id));
                    $stmt->bindParam(":id", $user_id, \PDO::PARAM_INT);
                }
            }

            if ($stmt->execute()) {
                $data = $stmt->fetchObject();
            }
        }

        $row_id = isset($data->id) ? $data->id : 0;

        $params->resource_single = sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/upload/form/%d/cover/?id=%d&field=:field", $section_id, $row_id);
        $params->resource_gallery = sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/upload/form/%d/gallery/?id=%d&field=:field", $section_id, $row_id);

        unset($params->forward_to);
        unset($params->deleted_fields);

        if ($params->form_type == CustomFormSectionForm::REGISTRATION_FORM && $user_id != null) {
            $user = $this->getUserById($user_id);;
            $registration = new \stdClass();
            $registration->email = isset($user->email) ? $user->email : null;
            $registration->privacy = isset($user->privacy) ? (bool)((int)$user->privacy) : null;
            $registration->terms = isset($user->terms) ? (bool)((int)$user->terms) : null;

            $params->registration = $registration;
        }

        $completed = ($data !== false && $row_id > 0 && $params->form_type != CustomFormSectionForm::CONTACT_FORM);

       //if (!isset($f->hidden) || ($f->hidden === false)) {
        foreach ($params->fieldsets as $fieldset) {
            $cleanFields = array();
            foreach ($fieldset->fields as $f) {
                if (!isset($f->hidden) || ($f->hidden === false)) {
                    //var_dump($f);
                    array_push($cleanFields, $f);
                }
            }
            $fieldset->fields = $cleanFields;
        }
        foreach ($params->fieldsets as $fieldset) {
            foreach ($fieldset->fields as $f) {
                $encodedField = isset($f->name) ? base64_encode($f->name) : base64_encode($f->label);
                $f->placeholder = null;
                if (!isset($f->fb_type)) {
                    $f->fb_type = null;
                }

                unset($f->old_label);

                switch ($f->type) {
                    case SectionModel::HPFORM_ADDRESS:
                        if (is_string($f->value) && $f->value !== "") {
                            $f->placeholder = $f->value;
                        }

                        $f->value = null;
                        if ($data !== false) {
                            $f->value = is_numeric($data->{$encodedField}) ? $this->getAddressById($data->{$encodedField}) : $data->{$encodedField};
                        }
                        break;
                    case SectionModel::HPFORM_CHECKBOX:
                    case SectionModel::HPFORM_RADIOBOX:
                    case SectionModel::HPFORM_TAG_GROUP:
                        $value = $f->value;
                        $selected = ($data !== false && $data->{$encodedField} != "" && $params->form_type != CustomFormSectionForm::CONTACT_FORM) ? array_map('trim', explode(",", $data->{$encodedField})) : [];
                        if (!is_array($value)) {
                            $value = array_map('trim', explode(",", $value));
                        }
                        $f->value = [
                            'options' => $value,
                            'selected' => $selected,
                        ];
                        if (version_compare($this->getApiVersion(), HpRestModel::API_VERSION_120, '>=')) {
                            $options = array();
                            foreach ($value as $key => $option) {
                                $newOption = new \stdClass();
                                $newOption->key = is_numeric($key) ? $option : $key;
                                $newOption->value = $option;
                                $options[] = $newOption;
                            }
                            $f->value = [
                                'options' => $options,
                                'selected' => $selected,
                            ];
                        }
                        break;
                    case SectionModel::HPFORM_SELECT:
                        $value = $f->value->option_values;
                        $rowIdQueryString = "";
                        if ($row_id > 0) {
                            $rowIdQueryString = "?row_id=" . $row_id;
                        }
                        $relatedTo = $f->value->related_to !== CustomFieldForm::HP_FORM_SELECT_RELATED_TO_NULL_LABEL ? $f->value->related_to : null;
                        $url = $relatedTo !== null ? sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/form/%d/select/%s/" . $rowIdQueryString, $section_id, $encodedField) : null;
                        $selected = ($data !== false && $data->{$encodedField} != "" && $params->form_type != CustomFormSectionForm::CONTACT_FORM) ? array_map('trim', explode(",", $data->{$encodedField})) : [];

                        $options = array();
                        foreach ($value as $key => $option) {
                            $newOption = new \stdClass();
                            $newOption->key = $key;
                            $newOption->value = $option;
                            $options[] = $newOption;
                        }
                        $f->value = [
                            'options' => $options,
                            'related_to' => array(
                                'key' => $relatedTo,
                                'url' => $url,
                            ),
                            'selected' => $selected,
                        ];

                        if (count($selected) > 0 && $relatedTo !== null) {
                            foreach ($params->fieldsets as $fieldSet) {
                                foreach ($fieldSet->fields as $field) {
                                    $fieldName = isset($field->name) ? $field->name : $field->label;
                                    if ($fieldName === $relatedTo) {
                                        $encodedRelatedField = base64_encode($fieldName);
                                        $relatedSelect = ($data !== false && $data->{$encodedRelatedField} != "" && $params->form_type != CustomFormSectionForm::CONTACT_FORM) ? array_map('trim', explode(",", $data->{$encodedRelatedField})) : [];
                                        $selectField = $this->getSelectField($section_id, base64_decode($encodedField), implode("|", $relatedSelect), $row_id, $selected);

                                        $f->value = $selectField->value;
                                    }
                                }
                            }
                        }
                        break;
                    case SectionModel::HPFORM_IMAGE:
                        if ($f->value != null && $f->value != "") {
                            $f->placeholder = $this->getCdnBaseurl() . $imageHelper->getCustomFormIconUrl() . $f->value;
                            $f->value = null;
                        }
                        switch ($f->validator) {
                            case "SINGLE":
                                $f->value = null;
                                if ($data !== false && $data->{$encodedField} != null && $data->{$encodedField} != "" && $params->form_type != CustomFormSectionForm::CONTACT_FORM) {
                                    $mediaModel = new MediaRestModel($this->controller);
                                    if ($connector !== false && !is_null($row_id)) {
                                        $image = $this->wrapGallery($connector->getId(), $data->{$encodedField}, true);
                                        $f->value = is_array($image) ? $image[0] : null;
                                    } else {
                                        $f->value = $mediaModel->getFormMediaById($section_id, (int)$data->{$encodedField});
                                    }
                                }
                                break;
                            case "GALLERY":
                                if ($connector !== false && !is_null($row_id)) {
                                    if (!is_null($data->{$encodedField})) {
                                        $f->value = $this->wrapGallery($connector->getId(), $data->{$encodedField}, true);
                                    }
                                } else {
                                    $mediaModel = new MediaRestModel($this->controller);
                                    if ($params->form_type != CustomFormSectionForm::CONTACT_FORM) {
                                        $f->value = $mediaModel->getGalleryByFormId($section_id, $row_id);
                                    } else {
                                        $f->value = null;
                                    }
                                }
                                break;
                        }
                        break;
                    default:
                        if ($f->value != null && $f->value != "") {
                            $f->placeholder = $f->value;
                            $f->value = null;
                        }
                        if ($params->form_type != CustomFormSectionForm::CONTACT_FORM) {
                            $f->value = null;
                            if ($data !== false && isset($data->{$encodedField})) {
                                $f->value = $data->{$encodedField};
                            }
                        }
                }

            }
        }
        if (is_string($params->success_message)) {
            $successMessage = $params->success_message;
        } else {
            $successMessage = is_null($params->success_message) ? null : json_encode($params->success_message);
        }
        $params->success_message = $this->getSuccessMessageFormatted($this->getLocaleText($lang, $successMessage));
        $params->completed = $completed;

        return $params;
    }

    private function getSuccessMessageFormatted($message)
    {
        $res = "";

        if (!is_null($message) && $message !== "") {
            $style = "<style>#web-view-text-centered{ 
            width: 100%; 
            position: absolute; 
            top: 50%; 
            -webkit-transform: translate(0%, -50%);
            -moz-transform: translate(0%, -50%);    /* Older Gecko browser */
            -ms-transform: translate(0%, -50%);     /* IE9+ */
            -o-transform: translate(0%, -50%);
            transform: translate(0%, -50%);}</style>";

            $res = $style . "<div id='web-view-text-centered'>" . $message . "</div>";
        }

        return $res;
    }

    /**
     * @param $section_id
     * @param $content
     * @param null $row_id
     * @return string
     * @throws RestApiKeySecurityException
     */
    public function updateUserData($section_id, $content, $row_id = null)
    {
        if ($this->isValidRequest()) {
            try {
                $device_id = (int)$this->getDevice()->id;
                $user_id = $this->getDevice()->user_id;
                $formParams = $this->getFormParams($section_id);
                $fieldsData = $this->parseFieldset($formParams->fieldsets);

                $this->parseContentData($content->data, $fieldsData);

                switch ($formParams->form_type) {
                    case CustomFormSectionForm::REGISTRATION_FORM:
                        if ($user_id != null) {
                            $email = $content->registration->email;
                            $password = $content->registration->password;
                            $privacy = (int)$content->registration->privacy;
                            $terms = (int)$content->registration->terms;

                            if ($this->editUser($email, $email, $password)) {

                                $auth = array(
                                    'username' => $email,
                                    'password' => $password,
                                    'privacy' => $privacy,
                                    'terms' => $terms,
                                );

                                if ($this->updateCustomFormFields($device_id, $user_id, $section_id, $content)) {
                                    $result = null;
                                    $sql = "SELECT id FROM `custom_form_$section_id` WHERE `user_id` = :user_id ORDER by `id` DESC LIMIT 1;";
                                    $stmt2 = $this->getDb()->prepare($sql);

                                    $stmt2->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
                                    if ($stmt2->execute()) {
                                        $row = $stmt2->fetchObject();
                                        if ($row !== false) {
                                            $result = new \stdClass();
                                            $result->resource_single = sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/upload/form/%d/cover/?id=%d&field=:field", $section_id, $row->id);
                                            $result->resource_gallery = sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/upload/form/%d/gallery/?id=%d&field=:field", $section_id, $row->id);
                                        }
                                    }

                                    // Remote connector call
                                    $remoteConnectorFields = array(
                                        'auth' => $auth,
                                        'fields' => $this->remoteFields,
                                        'operation' => "UPDATE",
                                    );
                                    $responseData = $this->callRemoteConnector($section_id, $remoteConnectorFields, self::USER_CUSTOM_FORM, $email, $row_id);

                                    $response = $this->getJsonResponse(202, $result);
                                } else {
                                    $response = $this->getJsonResponse(400);
                                }
                            } else {
                                $response = $this->getJsonResponse(417);
                            }

                            $user_tags = $this->getUserTagsFromContentData();
                            $this->addPushNotification($device_id, $user_tags);
                        } else {
                            $response = $this->getJsonResponse(406);
                        }
                        break;
                    case CustomFormSectionForm::USER_FORM:
                        $result = null;
                        $email = null;

                        if ($user_id != null) {
                            $userApp = UserAppQuery::create()
                                ->findPk($user_id);
                            if ($userApp) {
                                $email = $userApp->getUsernameCanonical();
                            }
                        }

                        // Remote connector call
                        $remoteConnectorFields = array(
                            'auth' => array(),
                            'fields' => $this->remoteFields,
                            'operation' => "UPDATE",
                        );
                        $responseData = $this->callRemoteConnector($section_id, $remoteConnectorFields, self::USER_CUSTOM_FORM, $email, $row_id);

                        $result = new \stdClass();
                        if (is_null($responseData)) {
                            if ($this->updateCustomFormFields($device_id, $user_id, $section_id, $content, $row_id)) {
                                if (is_null($row_id)) {
                                    if ($user_id != null) {
                                        $sql = "SELECT id FROM `custom_form_$section_id` WHERE `user_id` = :user_id ORDER by `id` DESC LIMIT 1;";
                                        $stmt = $this->getDb()->prepare($sql);
                                        $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
                                    } else {
                                        $sql = "SELECT id FROM `custom_form_$section_id` WHERE `device_id` = :device_id ORDER by `id` DESC LIMIT 1;";
                                        $stmt = $this->getDb()->prepare($sql);
                                        $stmt->bindParam(":device_id", $device_id, \PDO::PARAM_INT);
                                    }

                                    if ($stmt->execute()) {
                                        $row = $stmt->fetchObject();
                                        $row_id = $row->id;
                                    }
                                }

                                $result->resource_single = sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/upload/form/%d/cover/?id=%d&field=:field", $section_id, $row_id);
                                $result->resource_gallery = sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/upload/form/%d/gallery/?id=%d&field=:field", $section_id, $row_id);

                                $response = $this->getJsonResponse(202, $result);
                            } else {
                                $response = $this->getJsonResponse(400);
                            }
                        } else {
                            if (isset($responseData->success) && $responseData->success === false) {
                                $response = $this->getJsonResponse($responseData->status, null, null, $responseData->message);
                            } else {
                                $result->resource_single = sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/upload/form/%d/cover/?id=%d&field=:field", $section_id, $row_id);
                                $result->resource_gallery = sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/upload/form/%d/gallery/?id=%d&field=:field", $section_id, $row_id);

                                $response = $this->getJsonResponse(202, $result);
                            }
                        }
                        break;
                    default:
                        $response = $this->getJsonResponse(412, null, null, "Form Type not valid.");
                        break;
                }
            } catch (\PDOException $ex) {
                if ($ex->getCode() == 23000) {
                    $response = $this->getJsonResponse(412);
                } else {
                    if ($this->isDebug()) {
                        $response = $this->getJsonResponse(500, $ex->getMessage());
                    } else {
                        $response = $this->getJsonResponse(500);
                    }
                }
            } catch (RestRemoteException $exception) {
                $response = $this->getJsonResponse($exception->getRemoteStatusCode(), null, null, $exception->getCustomMessage());
            } catch (RestSignatureSecurityException $exception) {
                $response = $this->getJsonResponse(500, null, null, $exception->getMessage());
            } catch (\Exception $exception) {
                $response = $this->getJsonResponse(500, null, null, $exception->getMessage());
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    /**
     * @param $section_id
     * @param $content
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function insertUserData($section_id, $content)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $device_id = $this->getDevice()->id;
                $user_id = $this->getDevice()->user_id;
                $formParams = $this->getFormParams($section_id);
                $fieldsData = $this->parseFieldset($formParams->fieldsets);

                $auth = null;
                if ($formParams->form_type == CustomFormSectionForm::REGISTRATION_FORM) {
                    $email = $content->registration->email;
                    $password = $content->registration->password;
                    $privacy = (int)$content->registration->privacy;
                    $terms = (int)$content->registration->terms;

                    $auth = array(
                        'username' => $email,
                        'password' => $password,
                        'privacy' => $privacy,
                        'terms' => $terms,
                    );

                    $user_id = $this->registerUser($section_id, $email, $email, $password, $privacy, $terms);
                }

                $username = null;
                $user = UserAppQuery::create()
                    ->findPk($user_id);
                if ($user) {
                    $username = $user->getUsernameCanonical();
                }

                $this->parseContentData($content->data, $fieldsData);

                // Remote connector call
                $remoteConnectorFields = array(
                    'auth' => $auth,
                    'fields' => $this->remoteFields,
                    'operation' => "CREATE",
                );

                $responseData = $this->callRemoteConnector($section_id, $remoteConnectorFields, self::USER_CUSTOM_FORM, $username);


                $result = new \StdClass();
                if (is_null($responseData)) {
                    if ($row_id = $this->insertCustomFormFields($device_id, $user_id, $section_id, $content)) {

                        $forward_to = $formParams->forward_to;
                        if ($formParams->form_type == CustomFormSectionForm::CONTACT_FORM && $forward_to != null && $forward_to != "") {
                            $config = $this->controller->getServiceLocator()->get('config');

                            $message = new Message("...");
                            $message->setOption('jsonParams', "params from application config");
                            $message->setOption('from', $config['email_message']['sender.email']);
                            $message->setOption('senderLabel', $this->getApp()->title . " APP");
                            $message->setOption('to', $forward_to);
                            $message->setOption('subject', strtoupper($this->getApp()->title) . " > contact form > " . $formParams->title);
                            $message->setOption('templateLayout', 'email/app/forwarded-contact');
                            $message->setOption('templateParams', json_encode(['data' => $this->mailData]));

                            if ($user) {
                                $message->setOption('replyTo', $username);
                                $message->setOption('replyToLabel', trim($user->getFirstname() . $user->getLastname()) !== "" ? $user->getFirstname() . " " . $user->getLastname() : null);
                            }

                            $msgModel = new MessagesModel($this->controller->getServiceLocator(), MessagesFactory::MODE_MAIL);
                            $msgModel->pullMessage($message);
                        }

                        $result->resource_single = sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/upload/form/%d/cover/?id=%d&field=:field", $section_id, $row_id);
                        $result->resource_gallery = sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/upload/form/%d/gallery/?id=%d&field=:field", $section_id, $row_id);

                        $this->insertFormDataCompleted($section_id, $row_id, $formParams->form_type, $content);
                        $response = $this->getJsonResponse(201, $result);
                    } else {
                        $response = $this->getJsonResponse(400);
                    }
                } else {
                    if (isset($responseData->success) && $responseData->success === false) {
                        $response = $this->getJsonResponse($responseData->status, null, null, $responseData->message);
                    } else {
                        $row_id = $responseData->id;

                        if ($formParams->form_type === CustomFormSectionForm::REGISTRATION_FORM) {
                            if ($row_id_tmp = $this->insertCustomFormFields($device_id, $user_id, $section_id, $content)) {
                                $row_id = $row_id_tmp;
                            }
                        }

                        $result->resource_single = sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/upload/form/%d/cover/?id=%d&field=:field", $section_id, $row_id);
                        $result->resource_gallery = sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/upload/form/%d/gallery/?id=%d&field=:field", $section_id, $row_id);

                        $this->insertFormDataCompleted($section_id, $row_id, $formParams->form_type, $content);
                        $response = $this->getJsonResponse(202, $result);
                    }
                }

                if ($formParams->form_type == CustomFormSectionForm::REGISTRATION_FORM) {
                    $user_tags = $this->getUserTagsFromContentData();
                    $this->addPushNotification($device_id, $user_tags);
                }
            } catch (\PDOException $ex) {
                if ($ex->getCode() == 23000) {
                    $response = $this->getJsonResponse(412, null, null, $ex->getMessage());
                } else {
                    if ($this->isDebug()) {
                        $response = $this->getJsonResponse(500, $ex->getTraceAsString());
                    } else {
                        $response = $this->getJsonResponse(500);
                    }
                }
            } catch (RestRemoteException $exception) {
                $response = $this->getJsonResponse($exception->getRemoteStatusCode(), null, null, $exception->getCustomMessage());
            } catch (RestSignatureSecurityException $exception) {
                $response = $this->getJsonResponse(500, null, null, $exception->getMessage());
            } catch (\Exception $exception) {
                $response = $this->getJsonResponse(500, null, null, $exception->getMessage());
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    private function insertFormDataCompleted($section_id, $row_id, $type, $content) {
        $formEventManager = new FormEventManager();
        $data = $content->data;

        switch ($type) {
            case CustomFormSectionForm::REGISTRATION_FORM:
                $username = isset($content->registration) ? $content->registration->email : null;
                $formEventManager->triggerOnCreate($section_id, $row_id, $username, $data);
                break;
            default:
                $username = null;
                $user_id = $this->getDevice()->user_id;
                $user = UserAppQuery::create()->findPk($user_id);
                if ($user) {
                    $username = $user->getUsernameCanonical();
                }
                $formEventManager->triggerOnCreate($section_id, $row_id, $username, $data);
                break;
        }
    }

    /**
     * @param $section
     * @param null $row_id
     * @return null|string
     * @throws PropelException
     * @throws RestApiKeySecurityException
     * @throws RestSignatureSecurityException
     */
    public function getUserData($section, $row_id = null)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $result = null;
                $device_id = $this->getDevice()->id;
                $user_id = $this->getDevice()->user_id;
                $result = $this->getFormData($section, json_decode($section->params), $device_id, $user_id, $row_id);
                $response = $this->getJsonResponse(200, $result);
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            } catch (RestRemoteException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    private function parseFieldset($fieldsets)
    {
        $fields = array();
        foreach ($fieldsets as $fieldset) {
            foreach ($fieldset->fields as $f) {
                if (isset($f->name)) {
                    $fields[$f->name] = $f;
                } else {
                    $fields[$f->label] = $f;
                }
            }
        }
        return $fields;
    }

    private function getFormParams($section_id)
    {
        $result = null;
        $stmt = $this->getDb()->prepare(self::$sqlGetFormParams);

        $stmt->bindParam(":id", $section_id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $data = $stmt->fetchObject();
            $result = json_decode($data->params);
            $result->title = $data->title;
        }

        return $result;
    }

    private function insertAddressData($data)
    {
        $result = -1;

        $stmt = $this->getDb()->prepare(self::$sqlAddressInsert);

        $stmt->bindParam(":address", $data->address, \PDO::PARAM_STR);
        $stmt->bindParam(":city", $data->city, \PDO::PARAM_STR);
        $stmt->bindParam(":zipcode", $data->zipcode, \PDO::PARAM_STR);
        $stmt->bindParam(":state", $data->state, \PDO::PARAM_STR);
        $stmt->bindParam(":country", $data->country, \PDO::PARAM_STR);

        if ($stmt->execute()) {
            $result = $this->getDb()->lastInsertId();
        }

        return $result;
    }

    /**
     * @param $section_id
     * @param $username
     * @param $email
     * @param $password
     * @param $privacy
     * @param $terms
     * @return int|null
     */
    private function registerUser($section_id, $username, $email, $password, $privacy, $terms)
    {
//        var_dump(func_get_args()); die();
        $user_id = null;

        $enabled = self::$enabled;
        $username_c = StringHelper::getCanonical($username);
        $email_c = StringHelper::getCanonical($email);
        $salt = SecurityHelper::generateSalt(32);
        $password = $this->encodePassword($salt, $password);
        $confirm = SecurityHelper::generateKey();
        $role = "HOST";

        $stmt2 = $this->getDb()->prepare(self::$sqlUserInsert);

        $fix = "";
        $address_id = null;

        $stmt2->bindParam(":username", $username, \PDO::PARAM_STR);
        $stmt2->bindParam(":username_c", $username_c, \PDO::PARAM_STR);
        $stmt2->bindParam(":email", $email, \PDO::PARAM_STR);
        $stmt2->bindParam(":email_c", $email_c, \PDO::PARAM_STR);
        $stmt2->bindParam(":enabled", $enabled, \PDO::PARAM_INT);
        $stmt2->bindParam(":privacy", $privacy, \PDO::PARAM_INT);
        $stmt2->bindParam(":terms", $terms, \PDO::PARAM_INT);
        $stmt2->bindParam(":salt", $salt, \PDO::PARAM_STR);
        $stmt2->bindParam(":password", $password, \PDO::PARAM_STR);
        $stmt2->bindParam(":confirm", $confirm, \PDO::PARAM_STR);
        $stmt2->bindParam(":role", $role, \PDO::PARAM_STR);
        $stmt2->bindParam(":section_id", $section_id, \PDO::PARAM_STR);
        $stmt2->bindParam(":firstname", $fix, \PDO::PARAM_STR);
        $stmt2->bindParam(":lastname", $fix, \PDO::PARAM_STR);
        $stmt2->bindParam(":phone", $fix, \PDO::PARAM_STR);
        $stmt2->bindParam(":tag", $fix, \PDO::PARAM_STR);
        $stmt2->bindParam(":address_id", $address_id, \PDO::PARAM_STR);

        if ($stmt2->execute()) {
            $user_id = (int)$this->getDb()->lastInsertId();
            $device_id = (int)$this->getDevice()->id;

            $stmt3 = $this->getDb()->prepare(self::$sqlDeviceLink);

            $stmt3->bindParam(":id", $device_id, \PDO::PARAM_INT);
            $stmt3->bindParam(":user_id", $user_id, \PDO::PARAM_INT);

            if ($stmt3->execute()) {
                $config = $this->controller->getServiceLocator()->get('config');
                $name = "Form";

                $app_icon = null;
                if ($this->getApp()->icon != "") {
                    $app_icon = $this->getCdnBaseurl() . \Common\Helper\ImageHelper::getAppIconUrl() . $this->getApp()->icon;
                } else {
                    $app_icon = $this->getBaseurl() . "/img/placeholder/app-icon.png";
                }

                $message = new Message("...");
                $message->setOption('jsonParams', "params from application config");
                $message->setOption('from', $config['email_message']['sender.email']);
                $message->setOption('senderLabel', $this->getApp()->title . " APP");
                $message->setOption('to', $email);
                $message->setOption('subject', strtoupper($this->getApp()->title) . " : registered");
                $message->setOption('templateLayout', 'email/app/registered');
                $message->setOption('templateParams', json_encode(['app_icon' => $app_icon, 'app_name' => $this->getApp()->title, 'email' => $email, 'name' => $name]));

                $msgModel = new MessagesModel($this->controller->getServiceLocator(), MessagesFactory::MODE_MAIL);
                $msgModel->pullMessage($message);
            }
        }
        return $user_id;
    }

    /**
     * @param $username
     * @param $email
     * @param $password
     * @return bool
     * @throws PropelException
     */
    private function editUser($username, $email, $password)
    {
        $user_id = null;
        $address_id = null;

        $user_id = $this->getDevice()->user_id;
        $username_c = StringHelper::getCanonical($username);
        $email_c = StringHelper::getCanonical($email);

        $userApp = UserAppQuery::create()
            ->findPk($user_id);

        if ($userApp) {
            $privacy = 1;
            $terms = 1;
            $fix = null;

            $userApp
                ->setUsername($username)
                ->setUsernameCanonical($username_c)
                ->setPrivacy($privacy)
                ->setTerms($terms)
                ->setEmail($email)
                ->setEmailCanonical($email_c)
                ->setFirstname($fix)
                ->setLastname($fix)
                ->setPhone($fix)
                ->setTags($fix)
                ->setAddressId($address_id);

            if (isset($password) && $password != null && $password != "") {
                $salt = SecurityHelper::generateSalt(32);
                $password = $this->encodePassword($salt, $password);
                $confirm = SecurityHelper::generateKey();

                $userApp
                    ->setSalt($salt)
                    ->setPassword($password)
                    ->setConfirmationToken($confirm);
            }

            $userApp->save();
            return true;
        }

        return false;
    }

    /**
     * DEPRECATED: used in RegistrationSection and non in CustomFormSection
     *
     * @param $data
     * @param $avatar_type
     * @param $avatar_tmp_name
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function insert($data, $avatar_type, $avatar_tmp_name, $section_id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = null;
                $address_id = null;

                if (isset($data->address)) {
                    $stmt1 = $this->getDb()->prepare(self::$sqlAddressInsert);

                    $stmt1->bindParam(":address", $data->address->address, \PDO::PARAM_STR);
                    $stmt1->bindParam(":city", $data->address->city, \PDO::PARAM_STR);
                    $stmt1->bindParam(":zipcode", $data->address->zipcode, \PDO::PARAM_STR);

                    if ($stmt1->execute()) {
                        $address_id = $this->getDb()->lastInsertId();
                    }
                }

                $enabled = self::$enabled;
                $username_c = StringHelper::getCanonical($data->username);
                $email_c = StringHelper::getCanonical($data->email);
                $salt = SecurityHelper::generateSalt(32);
                $password = $this->encodePassword($salt, $data->password);
                $confirm = SecurityHelper::generateKey();
                $role = "HOST";

                $stmt2 = $this->getDb()->prepare(self::$sqlUserInsert);

                $tags = $this->convertTagIdsToStrings($data->tag);

                $terms = 1;

                $stmt2->bindParam(":username", $data->username, \PDO::PARAM_STR);
                $stmt2->bindParam(":username_c", $username_c, \PDO::PARAM_STR);
                $stmt2->bindParam(":email", $data->email, \PDO::PARAM_STR);
                $stmt2->bindParam(":email_c", $email_c, \PDO::PARAM_STR);
                $stmt2->bindParam(":enabled", $enabled, \PDO::PARAM_INT);
                $stmt2->bindParam(":privacy", $data->privacy, \PDO::PARAM_INT);
                $stmt2->bindParam(":terms", $terms, \PDO::PARAM_INT);
                $stmt2->bindParam(":salt", $salt, \PDO::PARAM_STR);
                $stmt2->bindParam(":password", $password, \PDO::PARAM_STR);
                $stmt2->bindParam(":confirm", $confirm, \PDO::PARAM_STR);
                $stmt2->bindParam(":role", $role, \PDO::PARAM_STR);
                $stmt2->bindParam(":firstname", $data->firstname, \PDO::PARAM_STR);
                $stmt2->bindParam(":lastname", $data->lastname, \PDO::PARAM_STR);
                $stmt2->bindParam(":phone", $data->phone, \PDO::PARAM_STR);
                $stmt2->bindParam(":tag", $tags, \PDO::PARAM_STR);
                $stmt2->bindParam(":address_id", $address_id, \PDO::PARAM_STR);
                $stmt2->bindParam(":section_id", $section_id, \PDO::PARAM_STR);

                if ($stmt2->execute()) {
                    $user_id = $this->getDb()->lastInsertId();
                    $device_id = (int)$this->getDevice()->id;

                    $stmt3 = $this->getDb()->prepare(self::$sqlDeviceLink);

                    $stmt3->bindParam(":id", $device_id, \PDO::PARAM_INT);
                    $stmt3->bindParam(":user_id", $user_id, \PDO::PARAM_INT);

                    if ($stmt3->execute()) {
                        $config = $this->controller->getServiceLocator()->get('config');

                        $this->uploadAvatar($avatar_type, $avatar_tmp_name, $user_id);
                        $response = $this->getJsonResponse(201);

                        $name = "";

                        if (strlen($data->firstname . $data->lastname) > 0) {
                            $name = $data->firstname . " " . $data->lastname;
                        }

                        $app_icon = null;
                        if ($this->getApp()->icon != "") {
                            $app_icon = $this->getCdnBaseurl() . \Common\Helper\ImageHelper::getAppIconUrl() . $this->getApp()->icon;
                        } else {
                            $app_icon = $this->getBaseurl() . "/img/placeholder/app-icon.png";
                        }

                        $message = new Message("...");
                        $message->setOption('jsonParams', "params from application config");
                        $message->setOption('from', $config['email_message']['sender.email']);
                        $message->setOption('senderLabel', $this->getApp()->title . " APP");
                        $message->setOption('to', $data->email);
                        $message->setOption('subject', strtoupper($this->getApp()->title) . " : registered");
                        $message->setOption('templateLayout', 'email/app/registered');
                        $message->setOption('templateParams', json_encode(['app_icon' => $app_icon, 'app_name' => $this->getApp()->title, 'email' => $data->email, 'name' => $name]));

                        $msgModel = new MessagesModel($this->controller->getServiceLocator(), MessagesFactory::MODE_MAIL);
                        $msgModel->pullMessage($message);
                    } else {
                        $response = $this->getJsonResponse(400);
                    }
                }
            } catch (\PDOException $ex) {
//                echo $ex->getMessage();
                if ($ex->getCode() == 23000) {
                    $response = $this->getJsonResponse(412);
                } else {
                    if ($this->isDebug()) {
                        $response = $this->getJsonResponse(500, $ex->getMessage());
                    } else {
                        $response = $this->getJsonResponse(500);
                    }
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    /**
     * @param $data
     * @param $avatar_type
     * @param $avatar_tmp_name
     * @return null|string
     * @throws PropelException
     * @throws RestApiKeySecurityException
     */
    public function update($data, $avatar_type, $avatar_tmp_name)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = null;
                $address_id = $this->getDevice()->address_id;

                if (isset($data->address)) {
                    if (isset($address_id)) {
                        $stmt1 = $this->getDb()->prepare(self::$sqlAddressUpdate);

                        $stmt1->bindParam(":id", $address_id, \PDO::PARAM_INT);
                        $stmt1->bindParam(":address", $data->address->address, \PDO::PARAM_STR);
                        $stmt1->bindParam(":city", $data->address->city, \PDO::PARAM_STR);
                        $stmt1->bindParam(":zipcode", $data->address->zipcode, \PDO::PARAM_STR);

                        $stmt1->execute();
                    } else {
                        $stmt1 = $this->getDb()->prepare(self::$sqlAddressInsert);

                        $stmt1->bindParam(":address", $data->address->address, \PDO::PARAM_STR);
                        $stmt1->bindParam(":city", $data->address->city, \PDO::PARAM_STR);
                        $stmt1->bindParam(":zipcode", $data->address->zipcode, \PDO::PARAM_STR);

                        if ($stmt1->execute()) {
                            $address_id = $this->getDb()->lastInsertId();
                        }
                    }
                }

                $user_id = $this->getDevice()->user_id;
                $username_c = StringHelper::getCanonical($data->username);
                $email_c = StringHelper::getCanonical($data->email);

                $userApp = UserAppQuery::create()
                    ->findPk($user_id);
                if ($userApp) {
                    $tags = $this->convertTagIdsToStrings($data->tag);

                    $userApp
                        ->setUsername($data->username)
                        ->setUsernameCanonical($username_c)
                        ->setPrivacy($data->privacy)
                        ->setTerms($data->terms)
                        ->setEmail($data->email)
                        ->setEmailCanonical($email_c)
                        ->setFirstname($data->firstname)
                        ->setLastname($data->lastname)
                        ->setPhone($data->phone)
                        ->setTags($tags)
                        ->setAddressId($address_id);

                    if (isset($data->password) && $data->password != null && $data->password != "") {
                        $salt = SecurityHelper::generateSalt(32);
                        $password = $this->encodePassword($salt, $data->password);
                        $confirm = SecurityHelper::generateKey();

                        $userApp
                            ->setSalt($salt)
                            ->setPassword($password)
                            ->setConfirmationToken($confirm);
                    }

                    $userApp->save();

                    $this->uploadAvatar($avatar_type, $avatar_tmp_name, $user_id);
                    $response = $this->getJsonResponse(202);
                } else {
                    $response = $this->getJsonResponse(400);
                }
            } catch (\PDOException $ex) {
                if ($ex->getCode() == 23000) {
                    $response = $this->getJsonResponse(412);
                } else {
                    if ($this->isDebug()) {
                        $response = $this->getJsonResponse(500, $ex->getMessage());
                    } else {
                        $response = $this->getJsonResponse(500);
                    }
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function getUserIdFromCustomForm($formId, $id)
    {
        $userAppId = null;
        $sql = sprintf(self::$sqlCustomFormUser, $formId);
        $stmt = $this->getDb()->prepare($sql);

        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $CustomFormUser = $stmt->fetchObject();
            $userAppId = $CustomFormUser->user_id;
        }

        return $userAppId;
    }

    /**
     * @param $formId
     * @param $id
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function getCustomFormUser($formId, $id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $sql = sprintf(self::$sqlCustomFormUser, $formId);
                $stmt = $this->getDb()->prepare($sql);

                $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
                if ($stmt->execute()) {
                    $CustomFormUser = $stmt->fetchObject();
                    $response = $this->getJsonResponse(200, $CustomFormUser);
                } else {
                    $response = $this->getJsonResponse(400);
                }
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    /**
     * @param $formId
     * @param $id
     * @param $coverId
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function updateCustomFormUserCover($formId, $id, $coverId)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $sql = sprintf(self::$sqlUpdateCustomFormUserCover, $formId);
                $stmt = $this->getDb()->prepare($sql);

                $stmt->bindParam(":cover_id", $coverId, \PDO::PARAM_INT);
                $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
                if ($stmt->execute()) {
                    $CustomFormUser = $stmt->fetchObject();
                    $response = $this->getJsonResponse(200, $CustomFormUser);
                } else {
                    $response = $this->getJsonResponse(400);
                }
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    private function isValidSignature($public_key, $secret_key, $sign)
    {
        return (hash_hmac("sha256", $public_key, $secret_key) == $sign);
    }

    /**
     * @param $section_id
     * @param $remoteConnectorFields
     * @param $response
     * @param null $username
     * @param null $row_id
     * @return mixed|null
     * @throws PropelException
     * @throws RestRemoteException
     * @throws RestSignatureSecurityException
     */
    private function callRemoteConnector($section_id, $remoteConnectorFields, $response, $username = null, $row_id = null)
    {
        $data = null;

        $sectionConnector = SectionConnectorQuery::create()
            ->findOneBySectionId($section_id);

        if ($sectionConnector) {
            $providerUUID = $sectionConnector->getRemoteProvider()->getUuid();
            $remoteUrl = $sectionConnector->getBaseurl();

            $remoteConnectorFieldsJson = json_encode($remoteConnectorFields);

            $headers = array(
                "Content-HMAC: " . base64_encode(hash_hmac("sha256", $remoteConnectorFieldsJson, $providerUUID)),
                "Device-Lang: " . $this->getDeviceLang(),
                "demo: " . $this->getDemoFlag(),
                "cache-control: no-cache",
                "Hp-Response: " . $response,
                "Content-Type: application/json",
                "Content-Length: " . strlen($remoteConnectorFieldsJson),
            );

            if ($username !== null) {
                $headers[] = "Form-ID: " . base64_encode(hash_hmac("sha256", $username, $providerUUID));
            }

            if ($row_id !== null) {
                $headers[] = "Row-Id: " . $row_id;
            }

//            error_log("TEST_HP - Remote URL: " . $remoteUrl);
//            error_log("TEST_HP - Headers: " . print_r($headers, 1));
//            error_log("TEST_HP - Body: " . $remoteConnectorFieldsJson);

            $data = json_decode($this->connectorRequest($remoteUrl, $providerUUID, $headers, $remoteConnectorFieldsJson));

//            error_log("TEST_HP - Response: " . json_encode($data));
        }

        return $data;
    }

    /**
     * @param $url
     * @param null $secret_key
     * @param array $headers
     * @param string $fields
     * @return bool|null|string
     * @throws RestRemoteException
     * @throws RestSignatureSecurityException
     */
    private function connectorRequest($url, $secret_key = null, $headers = array(), $fields = "")
    {
        $data = null;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HEADER => 1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => $fields === "" ? array() : $fields,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $info = curl_getinfo($curl);

        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $responseHeaders = substr($response, 0, $header_size);
        $content = substr($response, $header_size);

        curl_close($curl);

//        error_log("TEST_HP - Response: " . $response);
//        var_dump($err);
//        var_dump($info);
//        var_dump($content);die();

        if ($err || $info['http_code'] >= 400) {
            //echo "cURL Error #:" . $err;
            $jsonContent = json_decode($content);
            $remoteException = new RestRemoteException();
            $remoteException->setRemoteStatusCode($info['http_code']);
            if ($jsonContent !== null) {
                $remoteException->setCustomMessage($jsonContent->message);
            }
            throw $remoteException;
        } else {
            $sign = null;
            $responseHeaders = explode("\n", $responseHeaders);
            foreach ($responseHeaders as $header) {
                if (stripos($header, 'Content-HMAC: ') !== false) {
                    $sign = base64_decode(trim(explode(":", $header)[1]));
                }
            }
            if (!$this->isValidSignature($content, $secret_key, $sign)) {
                throw new RestSignatureSecurityException();
            } else {
                $data = $content;
            }
        }

        return $data;
    }

    private function parseContentData($contentData, $fieldsData)
    {
        $this->remoteFields = array();
        $this->keys = array();
        $this->fields = array();
        $this->values = array();
        $this->mailData = array();

        $i = 1;
        foreach ($contentData as $row) {
            $typeInt = [SectionModel::HPFORM_ADDRESS];//, SectionModel::HPFORM_CHECKBOX, SectionModel::HPFORM_RADIOBOX];
            $field = base64_encode($row->label);
            $bind_id = ":bind_$i";
            $type = $fieldsData[$row->label]->type;
            $validator = $fieldsData[$row->label]->validator;
            $mode = in_array($type, $typeInt) ? \PDO::PARAM_INT : \PDO::PARAM_STR;

            $value = null;
            $valueRemote = null;
            switch ($type) {
                case SectionModel::HPFORM_ADDRESS:
                    $this->mailData[$row->label] = $row->value->address . " - " . $row->value->city . " - " . $row->value->zipcode;
                    $value = $this->insertAddressData($row->value);
                    $valueRemote = new \stdClass();
                    $valueRemote->address = $row->value->address;
                    $valueRemote->city = $row->value->city;
                    $valueRemote->zipcode = $row->value->zipcode;
                    $valueRemote->state = $row->value->state;
                    $valueRemote->country = $row->value->country;
                    break;
                default:
                    $value = $row->value;
                    $valueRemote = $row->value;
                    $this->mailData[$row->label] = $row->value;
                    break;

            }

            $this->fields[] = "`" . $field . "` = $bind_id";
            $this->remoteFields[] = array('label' => $row->label, 'value' => $valueRemote, 'type' => $type, 'validator' => $validator);
            $this->keys[] = $bind_id;
            $this->values[$bind_id] = array(
                'value' => $value,
                'mode' => $mode,
            );
            $i++;
        }
    }

    /**
     * @param $device_id
     * @param $user_id
     * @param $section_id
     * @param $content
     * @param $row_id
     * @return bool
     */
    private function updateCustomFormFields($device_id, $user_id, $section_id, $content, $row_id = null)
    {
        if (!is_null($row_id)) {
            $sql = "UPDATE `custom_form_$section_id` SET
                  `device_id` = :device_id, `row_data` = :row_data, `updated_at` = NOW() %s
                WHERE
                  `id` = :id
                ORDER by `id` DESC LIMIT 1;";
        } else {
            if ($user_id !== null) {
                $sql = "UPDATE `custom_form_$section_id` SET
                  `device_id` = :device_id, `row_data` = :row_data, `updated_at` = NOW() %s
                WHERE
                  `user_id` = :user_id
                ORDER by `id` DESC LIMIT 1;";
            } else {
                $sql = "UPDATE `custom_form_$section_id` SET
                  `device_id` = :device_id, `row_data` = :row_data, `updated_at` = NOW() %s
                WHERE
                  `device_id` = :device_id_2 AND `user_id` IS NULL
                ORDER by `id` DESC LIMIT 1;";
            }
        }

        if (count($this->fields) === 0) {
            $sql = sprintf($sql, "");
        } else {
            $sql = sprintf($sql, ", " . implode(", ", $this->fields));
        }

        $stmt = $this->getDb()->prepare($sql);

        if (!is_null($row_id)) {
            $stmt->bindParam(":id", $row_id, \PDO::PARAM_INT);
        } else {
            if ($user_id !== null) {
                $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
            } else {
                $stmt->bindParam(":device_id_2", $device_id, \PDO::PARAM_INT);
            }
        }

        $stmt->bindParam(":device_id", $device_id, \PDO::PARAM_INT);
        $stmt->bindParam(":row_data", json_encode($content), \PDO::PARAM_STR);

        foreach ($this->values as $k => $v) {
            $stmt->bindParam($k, $v['value'], $v['mode']);
        }

        return $stmt->execute();
    }

    /**
     * @param $device_id
     * @param $user_id
     * @param $section_id
     * @param $content
     * @return bool|int
     */
    private function insertCustomFormFields($device_id, $user_id, $section_id, $content)
    {
        $sql = "INSERT INTO `custom_form_$section_id`
                          (`id`, `device_id`, `user_id`, `row_data`, `created_at`, `updated_at` %s)
                        VALUES
                          (NULL, :device_id, :user_id, :row_data, NOW(), NOW() %s);";


        if (count($this->fields) === 0) {
            $sql = sprintf($sql, "", "");
        } else {
            $fields = array();
            foreach ($this->fields as $field) {
                $fields[] = "`" . StringHelper::getStringBetween($field, "`", "`") . "`";
            }
            $sql = sprintf($sql, ", " . implode(", ", $fields), ", " . implode(", ", $this->keys));
        }

        $stmt = $this->getDb()->prepare($sql);

        $stmt->bindParam(":device_id", $device_id, \PDO::PARAM_INT);
        $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
        $stmt->bindParam(":row_data", json_encode($content), \PDO::PARAM_STR);

        foreach ($this->values as $k => $v) {
            $stmt->bindParam($k, $v['value'], $v['mode']);
        }

        if ($stmt->execute()) {
            return (int) $this->getDb()->lastInsertId();
        }

        return false;
    }

    /**
     * @param $formId
     * @param $fieldName
     * @param $filter
     * @param $row_id
     * @param array $selectedValue
     * @return null
     * @throws PropelException
     * @throws RestSignatureSecurityException
     */
    private function getSelectField($formId, $fieldName, $filter, $row_id, $selectedValue = array())
    {
        $section = SectionQuery::create()
            ->findPk($formId);

        $sectionParams = json_decode($section->getParams());
        $selectField = null;
        foreach ($sectionParams->fieldsets as $fieldSet) {
            foreach ($fieldSet->fields as $field) {
                if (isset($field->name) && $field->name === $fieldName) {
                    $selectField = $field;
                }
            }
        }

        if ($selectField !== null) {
            $lang = strtolower($this->getDeviceLang());
            $filter = is_null($filter) ? array() : explode("|", $filter);
            $options = array();
            $selectField->label = $this->getLocaleText($lang, json_encode($selectField->label));
            unset($selectField->old_label);
            $selectField->placeholder = null;
            $selectFieldValue = $selectField->value;

            $encodedField = base64_encode($selectField->name);
            $relatedTo = $selectFieldValue->related_to !== CustomFieldForm::HP_FORM_SELECT_RELATED_TO_NULL_LABEL ? $selectFieldValue->related_to : null;
            $placeholder = isset($selectFieldValue->placeholder) ? $selectFieldValue->placeholder->{$lang} : CustomFieldForm::HP_FORM_SELECT_DEFAULT_EMPTY_OPTION;
            $rowIdQueryString = "";
            if (isset($row_id) && $row_id > 0) {
                $rowIdQueryString = "?row_id=" . $row_id;
            }
            $url = $relatedTo !== null ? sprintf($this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/form/%d/select/%s/" . $rowIdQueryString, $formId, $encodedField) : null;
            $selected = array();

            $newValue = array(
                'options' => $options,
                'related_to' => array(
                    'key' => $relatedTo,
                    'url' => $url,
                ),
                'selected' => $selected,
            );

            switch ($selectFieldValue->origin_data) {
                case CustomFieldForm::HP_FORM_SELECT_TYPE_LOCALE:
                    $options[""] = $placeholder;
                    foreach ($selectFieldValue->option_values as $optionKey => $optionValue) {
                        if (in_array($optionValue->filter, $filter)) {
                            $options[$optionKey] = $optionValue->label->{$lang};
                        }
                    }
                    $options = count($options) <= 1 ? array() : $options;
                    break;
                case CustomFieldForm::HP_FORM_SELECT_TYPE_REMOTE:
                    // Get options from Remote URL
                    try {
                        $options = $this->getRemoteSelectOptions($formId, "select", $selectField->name, $placeholder, implode("|", $filter), null, "GET", $row_id);
                    } catch (RestRemoteException $exception) {
                        $options = array();
                    }
                    break;
                default:
                    break;
            }

            $newValueOptions = array();
            foreach ($options as $key => $option) {
                $newOption = new \stdClass();
                $newOption->key = $key;
                $newOption->value = $option;
                $newValueOptions[] = $newOption;
            }

            $newValue['options'] = $newValueOptions;

            if (count($selectedValue) > 0) {
                $newValue['selected'] = $selectedValue;
            } //elseif (isset($row_id) && $row_id > 0) {
//                switch ($selectFieldValue->origin_data) {
//                    case CustomFieldForm::HP_FORM_SELECT_TYPE_LOCALE:
//                        $stmt = $this->getDb()->prepare(sprintf(self::$sqlGetFormDataByRowId, $formId));
//                        $stmt->bindParam(":id", $row_id, \PDO::PARAM_INT);
//                        if ($stmt->execute()) {
//                            $data = $stmt->fetchObject();
//                            $selected = ($data !== false && $data->{$encodedField} != "" && $sectionParams->form_type != CustomFormSectionForm::CONTACT_FORM) ? array_map('trim', explode(",", $data->{$encodedField})) : [];
//                            $newValue['selected'] = $selected;
//                        }
//                        break;
//                    case CustomFieldForm::HP_FORM_SELECT_TYPE_REMOTE:
//                        // TODO: make another call to obtain selected value?
//                        break;
//                }
//            }

            $selectField->value = $newValue;
        }

        return $selectField;
    }

    /**
     * @param $section_id
     * @param $hpResponse
     * @param $selectKey
     * @param string $placeholder
     * @param null $filter
     * @param null $username
     * @param string $method
     * @param null $row_id
     * @return array|null
     * @throws PropelException
     * @throws RestRemoteException
     * @throws RestSignatureSecurityException
     */
    private function getRemoteSelectOptions($section_id, $hpResponse, $selectKey, $placeholder = CustomFieldForm::HP_FORM_SELECT_DEFAULT_EMPTY_OPTION, $filter = null, $username = null, $method = "GET", $row_id = null)
    {
        $data = null;

        $sectionConnector = SectionConnectorQuery::create()
            ->findOneBySectionId($section_id);

        if ($sectionConnector) {
            $providerUUID = $sectionConnector->getRemoteProvider()->getUuid();
            $remoteUrl = $sectionConnector->getBaseurl() .
                (parse_url($sectionConnector->getBaseurl(), PHP_URL_QUERY) ? "&" : "?") . "key=" . base64_encode($selectKey);
            if (!is_null($filter) && $filter !== "") {
                $remoteUrl .= "&filter=" . base64_encode($filter);
            }
            if (!is_null($row_id) && (int)$row_id > 0) {
                $remoteUrl .= "&row_id=" . $row_id;
            }

            $headers = array(
                "Content-HMAC: " . base64_encode(hash_hmac("sha256", $remoteUrl, $providerUUID)),
                "Device-Lang: " . $this->getDeviceLang(),
                "demo: " . $this->getDemoFlag(),
                "cache-control: no-cache",
                "Hp-Response: " . $hpResponse,
            );

            if ($username !== null) {
                $headers[] = "Form-ID: " . base64_encode(hash_hmac("sha256", $username, $providerUUID));
            }

//            var_dump($remoteUrl);
//            var_dump($headers);

            $client = new Client();
            $client->setMethod($method);
            $client->setOptions(array(
                'maxredirects' => 5,
                'timeout' => 30
            ));
            $client->setUri($remoteUrl);
            $client->setHeaders($headers);
            $response = $client->send();
            if ($response->isSuccess()) {
                $sign = null;
                $responseHeaders = $response->getHeaders();
                foreach ($responseHeaders as $responseHeader) {
                    if ($responseHeader->getFieldName() === "Content-HMAC") {
                        $sign = base64_decode(trim($responseHeader->getFieldValue()));
                    }
                }

                if (!$this->isValidSignature($response->getBody(), $providerUUID, $sign)) {
                    throw new RestSignatureSecurityException();
                } else {
                    $options = (array)json_decode($response->getBody());
                    if (count($options) > 0) {
                        $options = array("" => $placeholder) + $options;
                    }
                    $data = $options;
                }
            } else {
                $remoteException = new RestRemoteException();
                $remoteException->setRemoteStatusCode($response->getStatusCode());
                throw $remoteException;
            }
        }

        return $data;
    }

    /**
     * @param $deviceId
     * @param array $tags
     * @return int|null
     * @throws PropelException
     */
    private function addPushNotification($deviceId, array $tags = array())
    {
        $response = null;
        $device = DeviceQuery::create()->findPk($deviceId);

        if ($device && $device->getEnableNotification()) {
            $notificationModel = new NotificationModel($this->getController());
            $notificationRestModel = new NotificationRestModel($this->controller);
            $msgModel = new MessagesModel($this->getController()->getServiceLocator(), MessagesFactory::MODE_PUSH);

            $pTags = "";
            if (count($tags) > 0) {
                $pTags = StringHelper::parseMatchAgainstArray($tags);
            }


            $pushNotifications = PushNotificationQuery::create()
                ->filterBySendDate(array('min' => date("Y-m-d H:i:s", time())))
                ->filterByReceiverFilter(null, Criteria::ISNULL)
                ->_if(count($tags) > 0)
                ->_or()
                ->where("MATCH (push_notification.receiver_filter) AGAINST ('$pTags' IN BOOLEAN MODE)")
                ->_endif()
                ->find();

            $trackingUrl = $this->getBaseurl() . $this->getController()->url()->fromRoute("message-read/put",
                    array(
                        "type" => MessagesFactory::MODE_PUSH,
                        "hashId" => MessagesModel::HASH_ID_PLACEHOLDER,
                    )
                );

            $application = $this->getApp();
            $pushNotificationParams = json_decode($application->push_notification);

            $hashIds = array();
            $tokenDeviceId = array();
            $notificationAdded = 0;
            foreach ($pushNotifications as $pushNotification) {
                $pushNotificationDeviceCount = PushNotificationDeviceQuery::create()
                    ->filterByNotificationId($pushNotification->getId())
                    ->filterByDeviceId($deviceId)
                    ->count();

                if ($pushNotificationDeviceCount === 0) {
                    $action = NotificationForm::OPEN_MAIN;
                    $sectionId = (int)$pushNotification->getSectionId();
                    $postId = (int)$pushNotification->getPostId();

                    if (is_null($sectionId) && is_null($postId)) {
                        $action = NotificationForm::OPEN_MAIN;
                    } elseif (isset($sectionId)) {
                        $action = NotificationForm::OPEN_SECTION;
                    } elseif (isset($postId)) {
                        $action = NotificationForm::OPEN_CONTENT;
                    }

                    $contentData = $notificationRestModel->getData($sectionId);
                    $contentOldData = $notificationRestModel->getData($sectionId, true);
                    if ($contentData !== null) {
                        // Pezza per ridurre la dimensione del JSON nei limiti dei 2KB di Apple
                        $contentData->tags = array();
                        $contentData->actions = array();
                    }

                    if ($contentData !== null) {
                        $contentData->tags = array();
                    }

                    $content = $contentOldData;
                    $apiVersion = $device->getApiVersion();
                    if (isset($apiVersion) &&
                        ucfirst(strtolower($apiVersion)) !== DeviceRestModel::API_VERSION_UNKNOWN &&
                        version_compare($apiVersion, "1.2.0", '>=')
                    ) {
                        $content = $contentData;
                        $action = ($action === NotificationForm::OPEN_SECTION) ? NotificationForm::OPEN_CONTENT : $action;
                    }

                    $message = new Message($pushNotification->getBody());
                    $message->setOption('subject', $pushNotification->getTitle());
                    $message->setOption('title', $pushNotification->getTitle());
                    $message->setOption('trackingUrl', $trackingUrl);
                    $message->setOption('sendDate', date("Y-m-d H:i:s", $pushNotification->getSendDate()->getTimestamp()));

                    $os = strtolower($device->getOs());

                    $pushParams = array();
                    switch ($os) {
                        case "ios":
                            $pushParams = array(
                                "certificate" => $pushNotificationParams->ios->certificate,
                                "passPhrase" => $pushNotificationParams->ios->pass_phrase,
//                                    "badge" => $pushNotificationCounter,
                            );
                            break;
                        case "android":
                            $pushParams = array(
                                "apiKey" => $pushNotificationParams->android->api_key,
                            );
                            break;
                    }

                    $systemIcon = json_decode($this->getApp()->system_icons);
                    $resourceIcon = null;
                    if ($systemIcon !== null) {
                        $resourceIcon = $systemIcon->app_in_app_notification_icon;
                    }

                    $pushParams["customData"] = array(
                        "type" => $sectionId > 0 ? $action : NotificationForm::OPEN_MAIN,
                        "title" => $pushNotification->getTitle() !== "" ? $pushNotification->getTitle() : null,
                        "resource_icon" => ($resourceIcon !== null && $resourceIcon !== "") ? $this->getCdnBaseurl() . ImageHelper::getAppSystemIconUrl() . $resourceIcon : null,
                        "data" => $content,
                    );

                    $pushParams["callback"] = $this->getRemoveDeviceUrl($device->getToken());

                    $message->setOption('jsonParams', json_encode($pushParams));
                    $message->setOption('to', $device->getToken());
                    $message->setOption('device', $os);

                    $msgModel->pullMessage($message);
                    $hash_id = $msgModel->getNotificationHash();
                    $hashIds[$device->getToken()] = $hash_id;
                    $tokenDeviceId[$device->getToken()] = $deviceId;
                    $notificationModel->setNotifyRead($pushNotification->getId(), $hashIds, $tokenDeviceId);
                    $notificationAdded++;
                }
            }

            $response = $notificationAdded;
        }

        return $response;
    }

    private function getUserTagsFromContentData()
    {
        $tags = array();

        foreach ($this->remoteFields as $field) {
            if ($field['type'] === SectionModel::HPFORM_TAG_GROUP) {
                $tags[] = $field['value'];
            }
        }

        return $tags;
    }
}