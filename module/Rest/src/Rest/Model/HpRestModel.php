<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 11:02
 */

namespace Rest\Model;


use Application\Model\MediaModel;
use Application\Model\SectionModel;
use Database\HubPlus\MediaQuery;
use Database\HubPlus\PushNotificationQuery;
use Form\Section\CustomForm\CustomFormSectionForm;
use Common\Helper\DbHelper;
use Common\Helper\StringHelper;
use Rest\Exception\RestApiKeySecurityException;
use Rest\Exception\RestAppIdSecurityException;
use Zend\Mvc\Controller\AbstractActionController;

class HpRestModel
{
    const APP_ID = "App-Id";
    const API_KEY = "Api-Key";
    const HASH_ID = "Hash-Id";
    const PROVIDER_UUID = "Provider-Uuid";
    const CONTENT_HMAC = "Content-HMAC";
    const DEVICE_LANG = "Device-Lang";
    const API_VERSION = "Api-Version";
    const DEMO = "Demo";
    const USER_ID = "User-ID";

    const API_VERSION_UNKNOWN = "Unknown";
    const API_VERSION_100 = "1.0.0";
    const API_VERSION_110 = "1.1.0";
    const API_VERSION_120 = "1.2.0";

    const CNT_HASH_PREFIX = "CNT";
    const SCT_HASH_PREFIX = "SCT";

    private static $sqlDevice = "SELECT `device`.*, `user_app`.`address_id` FROM `device` LEFT JOIN `user_app` ON `device`.user_id = `user_app`.id
                                       WHERE `device`.`deleted_at` IS NULL AND `device`.`api_key` = :api_key LIMIT 1";

    private static $sqlDeviceList1 = "SELECT device.* FROM device 
                                        JOIN user_app AS u ON device.`user_id` = u.id
                                        JOIN custom_form_%d AS cf ON device.`user_id` = cf.`user_id`
                                      WHERE u.`deleted_at` IS NULL AND device.`deleted_at` IS NULL AND device.`enable_notification` = 1 AND device.`language` = :lang %s;";

    private static $sqlDeviceList2 = "SELECT * FROM device
                                      WHERE device.`deleted_at` IS NULL AND device.`enable_notification` = 1 AND `language` = :lang;";

    private static $sqlTotalRows = "SELECT FOUND_ROWS() as total_rows;";

    private static $sqlTagIdsConvert = "SELECT GROUP_CONCAT(label) AS tags FROM tag WHERE id IN(%s)";

    private static $sqlGetRemoteProviderFromMd5Uuid = "SELECT * FROM `remote_provider` WHERE MD5(uuid) = :md5_uuid;";

    private static $sqlRegistrationCustomFormSection = "SELECT * FROM `section` WHERE `type` = :type AND `status` = 0 AND `deleted_at` IS NULL";

    private static $sqlCustomFormUser = "SELECT * FROM `custom_form_%d` WHERE `id` = :id";

    private static $sqlGetRemoteLog = "SELECT * FROM remote_log WHERE connector_id = :connector_id AND remote_id = :remote_id AND user_id = :user_id";

    protected $controller = null;

    protected $db = null;
    protected $app = null;
    protected $appId = null;
    protected $apiKey = null;
    protected $status_code = null;

    protected $dispatched_status = 200;

    protected $cdnBasePath;
    protected $cdnBaseUrl;

    protected $device = null;

    protected $sectionModel = null;
    protected $contentModel = null;

    private $debug = false;

    private $log_file = null;

    /**
     * @param AbstractActionController $controller
     * @param bool|true $safeMode : set TRUE if yoy want disable Aoo-Id check and DB connection (used only in RestErrorController)
     * @throws \Exception
     */
    function __construct(AbstractActionController $controller, $safeMode = true)
    {
        $config = $controller->getServiceLocator()->get('config');

        $this->controller = $controller;
        $this->status_code = $config['status_code'];
        $this->debug = $config['debug'];

        $this->cdnBasePath = $config['cdn']['basepath'];
        $this->cdnBaseUrl = $config['cdn']['baseurl'];

        $this->log_file = isset($config['log_files']['rest']) ? $config['log_files']['rest'] : null;

        $this->appId = $this->getHeaderAppId();

        if ($safeMode) {
            $dbHelper = new DbHelper($this->controller->getServiceLocator());
            $this->app = $dbHelper->getApplicationByKey($this->appId);

            if ($this->app === false) {
                throw new RestAppIdSecurityException();
            } else {
                $this->db = $dbHelper->getRestDatabase();
            }
        }
    }

    public function log($type, $txt) {
        if(!is_null($this->log_file)) {
            $file = fopen($this->log_file, "a") or die("Unable to open file!");
            $date = date("Y-m-d H:i:s");

            fwrite($file, $date . " - [" . $type . "] - " . $txt);
            fclose($file);
        }
    }

    /**
     * @param $appId
     * @throws RestAppIdSecurityException
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;

        $dbHelper = new DbHelper($this->controller->getServiceLocator());
        $this->app = $dbHelper->getApplicationByKey($this->appId);

        if ($this->app === false) {
            throw new RestAppIdSecurityException();
        } else {
            $this->db = $dbHelper->getRestDatabase();
        }
    }

    public function getAppId()
    {
        return $this->appId;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getBaseurl()
    {
        $renderer = $this->controller->getServiceLocator()->get('Zend\View\Renderer\RendererInterface');
        return $renderer->basePath();
    }

    public function getCdnBaseurl()
    {
        $config = $this->controller->getServiceLocator()->get('config');
        return $config['cdn']['baseurl'];
    }

    public function getCdnBasePath()
    {
        $config = $this->controller->getServiceLocator()->get('config');
        return $config['cdn']['basepath'];
    }

    /**
     * @return null|\PDO
     */
    public function getDb()
    {
        return $this->db;
    }

    public function getApp()
    {
        return $this->app;
    }

    /**
     * @return bool
     */
    public function isDebug()
    {
        return $this->debug;
    }

    public function getHeaderAppId()
    {
        if ($this->controller->getRequest()->getHeader(self::APP_ID)) {
            return $this->controller->getRequest()->getHeader(self::APP_ID)->getFieldValue();
        } else {
            return null;
        }
    }

    public function getApiKey()
    {
        if ($this->controller->getRequest()->getHeader(self::API_KEY)) {
            return $this->controller->getRequest()->getHeader(self::API_KEY)->getFieldValue();
        } else {
            throw new RestApiKeySecurityException();
        }
    }

    public function getDemoFlag()
    {
        $demo = false;
        if ($this->controller->getRequest()->getHeader(self::DEMO)) {
            $demo = $this->controller->getRequest()->getHeader(self::DEMO)->getFieldValue();
        }
        return $demo;
    }

    public function getDeviceLang()
    {
        $lang = "EN";
        if ($this->controller->getRequest()->getHeader(self::DEVICE_LANG)) {
            $lang = $this->controller->getRequest()->getHeader(self::DEVICE_LANG)->getFieldValue();
        }
        return $lang;
    }

    public function getLocaleText($lang, $text)
    {
        $res = null;
        if ($text !== "") {
            $multiLangObj = json_decode($text);
            if ($multiLangObj !== null && $multiLangObj !== false) {
                if (isset($multiLangObj->{$lang})) {
                    $res = $multiLangObj->{$lang};
                } else {
                    $app = $this->getApp();
                    $res = $multiLangObj->{$app->main_contents_language};
                }
            } else {
                $res = $text;
            }
        }
        return $res;
    }

    public function getUserId()
    {
        $userId = null;
        if ($this->controller->getRequest()->getHeader(self::USER_ID)) {
            $userId = $this->controller->getRequest()->getHeader(self::USER_ID)->getFieldValue();
        }
        return $userId;
    }

    public function getApiVersion()
    {
        $version = self::API_VERSION_UNKNOWN;
        if ($this->controller->getRequest()->getHeader(self::API_VERSION)) {
            $version = $this->controller->getRequest()->getHeader(self::API_VERSION)->getFieldValue();
        }
        return $version;
    }

    public function getHmac()
    {
        if ($this->controller->getRequest()->getHeader(self::CONTENT_HMAC)) {
            return $this->controller->getRequest()->getHeader(self::CONTENT_HMAC)->getFieldValue();
        } else {
            return null;
        }
    }

    /**
     * @return bool
     * @throws RestApiKeySecurityException
     */
    public function isValidRequest()
    {
        $res = false;

        if ($this->device == null) {
            $apiKey = $this->getApiKey();

            if ($this->app !== false && $apiKey != null) {
                try {
                    $stmt = $this->getDb()->prepare(self::$sqlDevice);
                    $stmt->bindParam(":api_key", $apiKey);
                    $stmt->execute();

                    $this->device = $stmt->fetchObject();

                    if ($this->device) {
                        $res = true;
                    }
                } catch (\PDOException $ex) {
                    $res = false;
                }
            }
        } else {
            $res = true;
        }

        return $res;
    }

    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @return int
     */
    public function getDispatchedStatus()
    {
        return $this->dispatched_status;
    }

    /**
     * @param $status
     * @param null $data
     * @param null $total_rows
     * @param null $message
     * @return string
     */
    public function getJsonResponse($status, $data = null, $total_rows = null, $message = null)
    {
        $success = false;
        if ($status < 400) $success = true;

        $response = array(
            'status' => (int)$status,
            'success' => $success,
            'message' => $this->status_code['' . $status] . (($message != null) ? " - " . $message : "")
        );

        if ($data != null || is_array($data)) {
            if (is_array($data)) $response["rows"] = (int)count($data);
            if (!is_null($total_rows)) $response["total_rows"] = (int)$total_rows;
            $response['data'] = $data;
        }

        $response = json_encode($response);
        if ($response === false) {
            switch (json_last_error()) {
                case JSON_ERROR_NONE:
                    $message = 'No errors';
                    break;
                case JSON_ERROR_DEPTH:
                    $message =  'Maximum stack depth exceeded';
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    $message =  'Underflow or the modes mismatch';
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    $message =  'Unexpected control character found';
                    break;
                case JSON_ERROR_SYNTAX:
                    $message =  'Syntax error, malformed JSON';
                    break;
                case JSON_ERROR_UTF8:
                    $message =  'Malformed UTF-8 characters, possibly incorrectly encoded';
                    break;
                default:
                    $message =  'Unknown error';
                    break;
            }
            $status = 500;
            $response = array(
                'status' => $status,
                'success' => false,
                'message' => $this->status_code['' . $status] . (($message != null) ? " - " . $message : "")
            );
            $response = json_encode($response);
        }

        $this->dispatched_status = $status;
        $this->controller->getResponse()->setStatusCode($status);

        return $response;
    }

    /**
     * @param $status
     * @param null $data
     * @param null $total_rows
     * @param null $message
     * @return string
     */
    public function getSimpleJsonResponse($status, $data = null)
    {
        $response = json_encode($data);
        if ($response === false) {
            switch (json_last_error()) {
                case JSON_ERROR_NONE:
                    $message = 'No errors';
                    break;
                case JSON_ERROR_DEPTH:
                    $message =  'Maximum stack depth exceeded';
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    $message =  'Underflow or the modes mismatch';
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    $message =  'Unexpected control character found';
                    break;
                case JSON_ERROR_SYNTAX:
                    $message =  'Syntax error, malformed JSON';
                    break;
                case JSON_ERROR_UTF8:
                    $message =  'Malformed UTF-8 characters, possibly incorrectly encoded';
                    break;
                default:
                    $message =  'Unknown error';
                    break;
            }
            $status = 500;
            $response = array(
                'status' => $status,
                'success' => false,
                'message' => $this->status_code['' . $status]
            );
            $response = json_encode($response);
        }

        $this->dispatched_status = $status;
        $this->controller->getResponse()->setStatusCode($status);

        return $response;
    }

    /**
     * @return int
     */
    public function getTotalRows()
    {
        $res = 0;
        try {
            $stmt = $this->db->prepare(self::$sqlTotalRows);
            $stmt->execute();
            $result = $stmt->fetchObject();

            if ($result) {
                $res = $result->total_rows;
            }
        } catch (\PDOException $ex) {
            $res = 0;
        }
        return $res;
    }

    public function convertTagIdsToStrings($tags)
    {
        $res = true;
        try {
            $tag_ids = "'" . implode("','", explode(",", $tags)) . "'";
            $stmt = $this->db->prepare(sprintf(self::$sqlTagIdsConvert, $tag_ids));

            if ($stmt->execute()) {
                $res = $stmt->fetchObject()->tags;
            }
        } catch (\PDOException $ex) {
            $res = false;
        }
        return $res;
    }

    public function getDeviceToNotify($filter, $lang)
    {
        $tags = array();
        $emails = array();
        $conditions = explode(",", $filter);
        foreach ($conditions as $condition) {
            if (strpos($condition, "#") !== false) $tags   [] = trim($condition);
            if (strpos($condition, "@") !== false) $emails [] = trim(StringHelper::getStringBetween($condition, "<", ">"));
        }
//        var_dump($filter);
//        var_dump($tags);
//        var_dump($emails);

        $data = array();
        if (!is_null($this->getDb())) {
            $where = array();
            $sql = self::$sqlDeviceList2;

            // Get published CUSTOM_FORM section with form_type = REGISTRATION_FORM
            $section = $this->getRegistrationCustomFormSection();
            if (!is_null($section)) {

                if (count($tags) > 0) {
                    $pTags = '"' . implode('", "', $tags) . '"';
                    $labelsTag = $this->getRegistrationFormLabelsTag($section);

                    if (count($labelsTag) > 0) {
                        foreach ($labelsTag as $labelTag) {
                            $where [] = "cf.$labelTag IN ($pTags)";
                        }
                    }
                }

                if (count($emails) > 0) {
                    $pEmails = "'" . implode("' , '", $emails) . "'";
                    $where [] = " u.`email` IN ($pEmails) ";
                }

                if (count($where) > 0) {
                    $whereSql = " AND (" . implode(" OR ", $where) . ")";
                    $sql = sprintf(self::$sqlDeviceList1, $section->id, $whereSql);
                }
            }

            //echo $sql; die();

            $stmt = $this->getDb()->prepare($sql);
            $stmt->bindParam(":lang", $lang, \PDO::PARAM_STR);
            if ($stmt->execute()) {
                $data = $stmt->fetchAll(\PDO::FETCH_OBJ);
            }
        }

        return $data;
    }

    public function getRegistrationCustomFormSection()
    {
        $res = null;
        if (!is_null($this->getDb())) {
            $type = CustomFormSectionForm::CUSTOM_FORM;

            $stmt = $this->getDb()->prepare(self::$sqlRegistrationCustomFormSection);
            $stmt->bindParam(":type", $type, \PDO::PARAM_STR);

            if ($stmt->execute()) {
                $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
                foreach ($rows as $row) {
                    $params = json_decode($row->params);
                    if ($params !== null && $params->form_type === CustomFormSectionForm::REGISTRATION_FORM) {
                        return $row;
                    }
                }
            }
        }

        return $res;
    }

    public function getFormDataById($formId, $id)
    {
        $userData = null;
        $sql = sprintf(self::$sqlCustomFormUser, $formId);
        $stmt = $this->getDb()->prepare($sql);

        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $userData = $stmt->fetchObject();
        }

        return $userData;
    }

    public function getRegistrationFormLabelsTag($customForm)
    {
        $labelsTag = array();
        if ($customForm !== null) {
            $customFormParams = json_decode($customForm->params);

            foreach ($customFormParams->fieldsets as $fields) {
                foreach ($fields->fields as $field) {
                    if ($field->type == SectionModel::HPFORM_TAG_GROUP ||
                        $field->type == SectionModel::HPFORM_CHECKBOX ||
                        $field->type == SectionModel::HPFORM_RADIOBOX) {
                        $encodedField = isset($field->name) ? base64_encode($field->name) : base64_encode($field->label);
                        $labelsTag[] = "`" . $encodedField . "`";
                    }
                }
            }
        }
        return $labelsTag;
    }

    public function isNotificationEnabled($deviceCount)
    {
        //var_dump($this->getApp());
        if ($this->getApp()->push_notification === null) {
            return false;
        } elseif (json_decode($this->getApp()->push_notification) === null) {
            return false;
        }

        $maxNotification = (int)$this->getApp()->max_notification;
        $sentNotification = (int)$this->getPushNotificationSent();
        //die($maxNotification." - ".$sentNotification);
        return ($maxNotification >= ($sentNotification + $deviceCount));
    }

    public function getPushNotificationSent()
    {
        $searchDate = date("Y-m");
        $endDate = date("Y-m-t");
        $pushNotifications = PushNotificationQuery::create()
            ->filterBySendDate(array("min" => $searchDate . "-01 00:00:00", "max" => $endDate . " 23:59:59"))
            ->find();

        $notificationSent = 0;
        foreach ($pushNotifications as $pushNotification) {
            $notificationSent += $pushNotification->getDeviceCount();
        }

        return $notificationSent;
    }

    public function getRemoteProviderFromMd5Uuid($md5Uuid)
    {
        $res = null;

        $stmt = $this->getDb()->prepare(self::$sqlGetRemoteProviderFromMd5Uuid);
        $stmt->bindParam(":md5_uuid", $md5Uuid, \PDO::PARAM_STR);

        if ($stmt->execute()) {
            $data = $stmt->fetchObject();
            if ($data !== false) {
                $res = $data;
            }
        }

        return $res;
    }

    public function getRemoveDeviceUrl($token)
    {
        $removeDeviceUrl = $this->getBaseurl() . $this->getController()->url()->fromRoute("remove-device",
                array(
                    "token" => md5($token),
                ));
        return $removeDeviceUrl;
    }

    public function addMediaFormat($media_id, $coverFormat)
    {
        $media = MediaQuery::create()
            ->findOneById($media_id);

        $formats = json_decode($media->getFormat());
        if ($formats === null) {
            $formats = array();
        }

        $formats[] = $coverFormat;

        $media->setFormat(json_encode($formats));
        $media->save();
    }

    public function wrapGallery($connector_id, $medias, $mediaCanBeDelete = false)
    {
        $gallery = array();
        $user_id = (int)$this->getDevice()->user_id;

        foreach ($medias as $media) {
            $remote_media_id = isset($media->remote_media_id) ? $media->remote_media_id : null;
            $remote_id = base64_encode($media->resource);
            $resource = $this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/media/connector/$connector_id/$remote_id";
            $logData = $this->getRemoteLogData($connector_id, $remote_id, $user_id);
            $providerIcon = null;
            $isValidMedia = false;

            switch ($media->type) {
                case MediaModel::MEDIA_TYPE_IMAGE:
                    $isValidMedia = true;
                    $urlExplode = explode(".", $media->resource);
                    $extension = end($urlExplode);
                    $media->mime_type = "image/" . $extension;
                    $media->extension = $extension;
                    $media->size = 0;
                    $media->resource_shareable = $media->resource;
                    break;
                default:
                    if (in_array($media->type, MediaModel::$remoteMedia)) {
                        $isValidMedia = true;
                        $config = $this->getController()->getServiceLocator()->get('config');
                        $extraIcon = $config['extra_app_icons'];

                        $providerIcon = $extraIcon[$media->type]['value'];
                        $media->resource_shareable = $media->resource;

                        $media->resource = $this->getBaseurl() . $this->getController()->url()->fromRoute("get-page-remote-media",
                                array(
                                    "type" => $media->type,
                                    "remote_url" => base64_encode($media->resource),
                                )
                            );

                        $media->type = MediaModel::MEDIA_TYPE_REMOTE;
                        $media->mime_type = null;
                        $media->extension = null;
                        $media->size = 0;
                    }
                    break;
            }

            $getParamMediaId = "";
            if ($remote_media_id !== null) {
                $getParamMediaId = "?media_id=" . $remote_media_id;
            }

            $media->context = "remote-gallery";
            $media->resource_like = $resource . "/like/";
            $media->resource_share = $resource . "/share/";
            $media->resource_delete = $mediaCanBeDelete ? $resource . "/" . $getParamMediaId : null;
            $media->like_count = ($logData) ? (int)$logData->like_count : 0;
            $media->liked_at = ($logData) ? $logData->liked_at : null;
            $media->provider_icon = $providerIcon;

            if ($isValidMedia) {
                $gallery [] = $media;
            }
        }

        return $gallery;
    }

    public function getRemoteLogData($connector_id, $remote_id, $user_id)
    {
        $stmt = $this->getDb()->prepare(self::$sqlGetRemoteLog);

        $stmt->bindParam(":connector_id", $connector_id, \PDO::PARAM_INT);
        $stmt->bindParam(":remote_id", $remote_id, \PDO::PARAM_STR);
        $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);

        $data = null;
        if ($stmt->execute()) {
            $data = $stmt->fetchObject();
        }

        return $data;
    }

}