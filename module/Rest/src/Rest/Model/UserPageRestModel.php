<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 19/10/17
 * Time: 17:16
 */

namespace Rest\Model;

use Application\Model\SectionModel;
use Common\Helper\StringHelper;
use Database\HubPlus\Base\CustomFormLogQuery;
use Rest\Exception\RestApiKeySecurityException;
use Rest\Model\Json\SectionToSectionSerializer;

class UserPageRestModel extends ContentPageRestModel
{
    const CONTENT_FILTER_OTHER_USER = "OTHER_USER";
    const CONTENT_FILTER_LOGGED_USER = "LOGGED_USER";
    const CONTENT_FILTER_ALL_USER = "ALL_USER";

    private static $sql = "SELECT SQL_CALC_FOUND_ROWS cf.`id` AS cf_row_id, cf.*, 
                                cfl.form_id, cfl.liked_at, cfl.shared_at, cfl.like_count, cfl.share_count, cfl.view_count
                           FROM `custom_form_%d` AS cf
                               LEFT JOIN `cf_log` AS cfl ON ( cf.`id` = cfl.`row_id` AND cfl.`user_id` = :user_id AND cfl.`form_id` = :form_id )
                           %s
                           ORDER BY cf.`created_at` DESC
                           LIMIT :start_row, :length;";

    /**
     * @param $id
     * @param int $limit
     * @param int $offset
     * @param null $filter
     * @param null $tags
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function getList($id, $limit = 5, $offset = 0, $filter = null, $tags = null)
    {
        $response = null;

        // TODO: filtro su tag - valutare se implementarlo.
        // 1) se nella tabella custom_form_x non c'è un campo di tipo TAG, non filtrare
        // 2) se nella tabella custom_form_x c'è un campo di tipo TAG, ma non è obbligatorio, non filtrare
        // 3) se nella tabella custom_form_x c'è un campo di tipo TAG ed è obbligatorio  filtrare per tag

        if ($this->isValidRequest()) {
            try {
                $sectionModel = new SectionModel($this->controller->getServiceLocator());
                $section = $sectionModel->getSectionByPk($id);
                if ($section->getType() === SectionToSectionSerializer::TYPE_USER) {
                    $sectionParams = json_decode($section->getParams());

                    if ($sectionParams) {
                        $userId = $this->getDevice()->user_id;
                        $customFormId = $sectionParams->custom_form_id;

                        $where = "";
                        if ($filter !== null && $filter !== "") {
                            $search = StringHelper::parseLikeString($filter);

                            $fields = $sectionParams->fields_map;
                            $searchFields = array();
                            foreach ($fields as $field) {
                                foreach ($field as $fieldName => $params) {
                                    $encodedFieldName = base64_encode($fieldName);
                                    switch ($params->map_type) {
                                        case "title":
                                            $searchFields[] = "`$encodedFieldName` LIKE \"$search\"";
                                            break;
                                        case "body_text":
                                            $searchFields[] = "`$encodedFieldName` LIKE \"$search\"";
                                            break;
                                    }
                                }
                            }

                            if (count($searchFields)) {
                                $where = "WHERE (" . implode(" OR ", $searchFields) . ")";
                            }
                        }

                        $contentFilter = "";
                        switch ($sectionParams->content_filter) {
                            case self::CONTENT_FILTER_LOGGED_USER:
                                if (isset($userId)) {
                                    $contentFilter = " cf.user_id = $userId ";
                                } else {
                                    $contentFilter = " cf.user_id = 0 ";
                                }
                                break;
                            case self::CONTENT_FILTER_OTHER_USER:
                                if (isset($userId)) {
                                    $contentFilter = " cf.user_id <> $userId ";
                                }
                                break;
                            default:
                                break;
                        }

                        if ($contentFilter !== "") {
                            if ($where === "") {
                                $where = "WHERE $contentFilter";
                            } else {
                                $where .= " AND $contentFilter";
                            }
                        }

                        $sql = sprintf(self::$sql, $customFormId, $where);

                        $stmt = $this->getDb()->prepare($sql);
                        $stmt->bindParam(":user_id", $userId, \PDO::PARAM_INT);
                        $stmt->bindParam(":form_id", $customFormId, \PDO::PARAM_INT);
                        $stmt->bindParam(":length", $limit, \PDO::PARAM_INT);
                        $stmt->bindParam(":start_row", $offset, \PDO::PARAM_INT);
                        $stmt->execute();

                        $contents = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\ContentUserSerializer', array(
                                "model" => $this,
                                "mode" => self::MODE_MASTER,
                                "section" => $section,
                                "customFormId" => $customFormId,
                                "userId" => $userId)
                        );

                        $page = new \stdClass();
                        $header = new \stdClass();
                        $body = new \stdClass();

                        $header->demo = ((int)$this->getApp()->demo == 0) ? false : true;
                        $header->white_label = ((int)$this->getApp()->white_label == 0) ? false : true;
                        $header->rows = count($contents);
                        $header->total_rows = ($header->rows > 0) ? (int)$this->getTotalRows() : 0;

                        $body->sections = null;
                        $body->contents = $contents;

                        $page->type = self::TYPE_LIST;
                        $page->header = $header;
                        $page->body = $body;

                        $response = $this->getJsonResponse(200, $page);
                    } else {
                        $response = $this->getJsonResponse(500, null, null, "Invalid section params");
                    }
                } else {
                    $response = $this->getJsonResponse(500, null, null, "Invalid id param");
                }
            } catch (\PDOException $exception) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, null, null, $exception->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function getContentShareCount($form_id, $row_id)
    {
        $res = 0;

        $cfLog = CustomFormLogQuery::create()
            ->select(array('share_count'))
            ->withColumn('SUM(cf_log.share_count)', 'share_count')
            ->filterByFormId($form_id)
            ->filterByRowId($row_id)
            ->groupByRowId()
            ->find();

        if ($cfLog && isset($cfLog->getData()[0])) {
            $res = (int)$cfLog->getData()[0];
        }

        return $res;
    }

    public function getContentViewCount($form_id, $row_id)
    {
        $res = 0;

        $cfLog = CustomFormLogQuery::create()
            ->select(array('view_count'))
            ->withColumn('SUM(cf_log.view_count)', 'view_count')
            ->filterByFormId($form_id)
            ->filterByRowId($row_id)
            ->groupByRowId()
            ->find();

        if ($cfLog && isset($cfLog->getData()[0])) {
            $res = (int)$cfLog->getData()[0];
        }

        return $res;
    }
}