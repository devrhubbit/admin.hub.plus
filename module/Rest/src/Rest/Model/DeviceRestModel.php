<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 11:21
 */

namespace Rest\Model;


use Common\Helper\SecurityHelper;
use Database\HubPlus\DeviceQuery;
use Rest\Exception\RestApiKeySecurityException;

class DeviceRestModel extends HpRestModel
{
    private static $ACTIVE   = "active";
    private static $INACTIVE = "inactive";

    private static $sqlDeviceCreate  = "INSERT INTO `device`
                                           (`id`, `status`, `type`,  `os`, `os_version`, `enable_notification`, `token`, `api_key`,
                                            `demo`, `language`, `api_version`,
                                            `created_at`, `updated_at`, `deleted_at`)
                                        VALUES
                                           (NULL, :status, :type, :os, :osv, :notify, :token, :api_key,
                                            :demo, :lang, :api_version,
                                            NOW(), NOW(), NULL);";

    private static $sqlDeviceUpdate  = "UPDATE `device` SET
                                           `os_version` = :osv, `enable_notification` = :notify, `token` = :new_token, `updated_at` = NOW(),
                                           `demo` = :demo, `language` = :lang, `api_version` = :api_version
                                        WHERE deleted_at IS NULL AND `token` = :old_token AND `api_key` = :api_key;";

    private static $sqlDeviceIpUpdate  = "UPDATE `device` SET `ip_address` = :ip_address,
                                          `demo` = :demo, `language` = :lang, `api_version` = :api_version, `updated_at` = NOW()
                                          WHERE `id` = :id;";


    public function checkAppId() {
        return $this->getJsonResponse(200);
    }

    public function register($type, $os, $osVersion, $token, $notify) {
        $response = null;

        // TODO: controllo su App-Id e sollevamento eccezione in caso di errore

        try {
            if($this->getDb() != null) {
                $apiKey = SecurityHelper::generateKey(65, 0);
                $language   = $this->getDeviceLang();
                $apiVersion = $this->getApiVersion();
                $demo       = $this->getDemoFlag();

                $stmt = $this->getDb()->prepare(self::$sqlDeviceCreate);

                $stmt->bindParam(":status",      self::$ACTIVE, \PDO::PARAM_STR);
                $stmt->bindParam(":type",        $type,         \PDO::PARAM_STR);
                $stmt->bindParam(":os",          $os,           \PDO::PARAM_STR);
                $stmt->bindParam(":osv",         $osVersion,    \PDO::PARAM_STR);
                $stmt->bindParam(":notify",      $notify,       \PDO::PARAM_STR);
                $stmt->bindParam(":token",       $token,        \PDO::PARAM_STR);
                $stmt->bindParam(":api_key",     $apiKey,       \PDO::PARAM_STR);
                $stmt->bindParam(":lang",        $language,     \PDO::PARAM_STR);
                $stmt->bindParam(":api_version", $apiVersion,   \PDO::PARAM_STR);
                $stmt->bindParam(":demo",        $demo,         \PDO::PARAM_INT);

                if($stmt->execute()) {
                    $result = new \stdClass();
                    $result->apiKey = $apiKey;

                    $response = $this->getJsonResponse(201, $result);
                } else {
                    $response = $this->getJsonResponse(400);
                }
            }
        } catch(\PDOException $ex) {
            if($this->isDebug()){
                $response = $this->getJsonResponse(500, $ex->getMessage());
            } else {
                $response = $this->getJsonResponse(500);
            }
        }

        return $response;
    }

    public function refresh($old_token, $new_token, $osVersion, $notify) {
        $response = null;

        if($this->isValidRequest()) {
            if($this->getDevice()->token == $old_token) {
                try {
                    $apiKey     = $this->getApiKey();
                    $language   = $this->getDeviceLang();
                    $apiVersion = $this->getApiVersion();
                    $demo       = $this->getDemoFlag();

                    $stmt = $this->getDb()->prepare(self::$sqlDeviceUpdate);

                    $stmt->bindParam(":osv",         $osVersion,  \PDO::PARAM_STR);
                    $stmt->bindParam(":notify",      $notify,     \PDO::PARAM_STR);
                    $stmt->bindParam(":api_key",     $apiKey,     \PDO::PARAM_STR);
                    $stmt->bindParam(":old_token",   $old_token,  \PDO::PARAM_STR);
                    $stmt->bindParam(":new_token",   $new_token,  \PDO::PARAM_STR);
                    $stmt->bindParam(":lang",        $language,   \PDO::PARAM_STR);
                    $stmt->bindParam(":api_version", $apiVersion, \PDO::PARAM_STR);
                    $stmt->bindParam(":demo",        $demo,       \PDO::PARAM_INT);

                    if($stmt->execute()) {
                        $response = $this->getJsonResponse(202);
                    } else {
                        $response = $this->getJsonResponse(400);
                    }
                } catch(\PDOException $ex) {
                    if($this->isDebug()){
                        $response = $this->getJsonResponse(500, $ex->getMessage());
                    } else {
                        $response = $this->getJsonResponse(500);
                    }
                }
            } else {
                $response = $this->getJsonResponse(412);
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function updateIpAddress($ipAddress)
    {
        $response = null;
        if($this->isValidRequest()) {
            $deviceId = $this->getDevice()->id;
            $language   = $this->getDeviceLang();
            $apiVersion = $this->getApiVersion();
            $demo       = $this->getDemoFlag();

            try {
                $stmt = $this->getDb()->prepare(self::$sqlDeviceIpUpdate);

                $stmt->bindParam(":ip_address",  $ipAddress,    \PDO::PARAM_STR);
                $stmt->bindParam(":id",          $deviceId,     \PDO::PARAM_STR);
                $stmt->bindParam(":lang",        $language,     \PDO::PARAM_STR);
                $stmt->bindParam(":api_version", $apiVersion,   \PDO::PARAM_STR);
                $stmt->bindParam(":demo",        $demo,         \PDO::PARAM_INT);

                if($stmt->execute()) {
                    $response = $this->getJsonResponse(200);
                } else {
                    $response = $this->getJsonResponse(400);
                }
            } catch(\PDOException $ex) {
                if($this->isDebug()){
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function removeDevice($md5Token)
    {
        $response = $this->getJsonResponse(404);

        $devices = DeviceQuery::create()
            ->where("MD5(device.token) = ?", $md5Token)
            ->find();

        foreach ($devices as $device) {
            $device->setUserId(null)
                ->delete();
            $response = $this->getJsonResponse(200);
        }

        return $response;
    }
}