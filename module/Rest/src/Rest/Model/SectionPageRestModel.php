<?php

namespace Rest\Model;

use Common\Helper\StringHelper;
use Rest\Exception\RestApiKeySecurityException;

class SectionPageRestModel extends ContentPageRestModel
{

    private static $sqlList = "SELECT SQL_CALC_FOUND_ROWS * FROM `section`
                                  WHERE `deleted_at` IS NULL AND `id` != :id AND `status` = 0 %s
                                ORDER BY `weight` %s LIMIT :offset, :limit";

    private static $sqlSectionById = "SELECT * FROM `section` WHERE MD5(`id`) = :id;";

    private static $sqlSectionDetail = "SELECT * FROM `section` WHERE deleted_at IS NULL AND id = :id";


    public function getList($id, $limit = 5, $offset = 0, $filter = null, $tags = null, $order = "ASC")
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                //$search = StringHelper::parseMatchAgainstString($filter, "AND", false);
                //$type = strtoupper(self::SECTION_CONTENT);

                $user_id = (int)$this->getDevice()->user_id;
                $where = "";
                $withTags = ($tags != null && count($tags) > 0);

                if ($withTags) {
                    $where = " AND MATCH (`parsed_tags`) AGAINST (:tags  IN BOOLEAN MODE) ";
                }

                $section = $this->getRegistrationCustomFormSection();
                $labelsTag = $this->getRegistrationFormLabelsTag($section);
                if (count($labelsTag) > 0) {
                    $userRestModel = new UserRestModel($this->getController());
                    $userTags = $userRestModel->getFormTagsData($section->id, $user_id, $labelsTag);
                    if (count($userTags) > 0) {
                        $userTags = StringHelper::parseMatchAgainstArray($userTags);
                        $where .= " AND (MATCH (`tags_user`) AGAINST ('$userTags' IN BOOLEAN MODE) OR `tags_user` IS NULL) ";
                    } else {
                        $where .= " AND `tags_user` IS NULL ";
                    }
                }

                if ($filter !== null) {
                    $search = StringHelper::parseLikeString($filter);
                    $where .= " AND (`title` LIKE '$search' OR `description` LIKE '$search') ";
                }

                $stmt = $this->getDb()->prepare(sprintf(self::$sqlList, $where, $order));

                $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
                $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
                $stmt->bindParam(":offset", $offset, \PDO::PARAM_INT);

                if ($withTags) {
                    $pTags = StringHelper::parseMatchAgainstArray(($tags) ? $this->parseTags($tags) : array());
                    $stmt->bindParam(":tags", $pTags, \PDO::PARAM_STR);
                }

                $stmt->execute();

                $sections = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\SectionToSectionSerializer', array("model" => $this, "mode" => self::MODE_MASTER));

                $page = new \stdClass();

                $header = new \stdClass();
                $header->demo = ((int)$this->getApp()->demo == 0) ? false : true;
                $header->white_label = ((int)$this->getApp()->white_label == 0) ? false : true;
                $header->rows = count($sections);
                $header->total_rows = (int)$this->getTotalRows();

                $body = new \stdClass();
                $body->sections = null;
                $body->contents = $sections;

                $page->type = self::TYPE_LIST;
                $page->header = $header;
                $page->body = $body;

                $response = $this->getJsonResponse(200, $page);
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function getSectionToSectionByHashId($hashId)
    {
        if ($this->isValidRequest()) {
            $stmt = $this->getDb()->prepare(self::$sqlSectionById);
            $stmt->bindParam(":id", $hashId, \PDO::PARAM_STR);
            $stmt->execute();

            $section = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\SectionToSectionSerializer', array("model" => $this, "mode" => self::MODE_MASTER));
            if (count($section) > 0) {
                $response = $this->getJsonResponse(200, $section[0]);
            } else {
                $response = $this->getJsonResponse(412, null, null, "Section not found");
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function getSerializedSectionById($id)
    {
        $result = null;

        if (!is_null($this->getDb())) {
            $stmt = $this->getDb()->prepare(self::$sqlSectionDetail);
            $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
            $stmt->execute();

            $sections = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\SectionSerializer', array("model" => $this));
            if (count($sections) > 0) {
                return $sections[0];
            }
        }

        return $result;
    }

    public function getSerializedSectionToSectionById($id)
    {
        $result = null;

        if (!is_null($this->getDb())) {
            $stmt = $this->getDb()->prepare(self::$sqlSectionDetail);
            $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
            $stmt->execute();

            $sections = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\SectionToSectionSerializer', array("model" => $this, "mode" => self::MODE_MASTER));
            if (count($sections) > 0) {
                return $sections[0];
            }
        }

        return $result;
    }
}