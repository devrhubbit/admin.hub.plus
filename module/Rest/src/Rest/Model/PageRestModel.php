<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 17:17
 */

namespace Rest\Model;


use Application\Model\MediaModel;
use Application\Model\SectionModel;
use Common\Helper\StringHelper;
use Database\HubPlus\CustomFormLogQuery;
use Database\HubPlus\SectionConnectorQuery;
use Rest\Exception\RestApiKeySecurityException;
use Common\Helper\ImageHelper;
use Rest\Model\Json\MediaSerializer;

class PageRestModel extends HpRestModel
{
    const TYPE_MAIN = "MAIN";
    const TYPE_LIST = "LIST";
    const TYPE_DETAIL = "DETAIL";

    const EVENT_CONTENT = "event";
    const PERSON_CONTENT = "person";
    const FAVOURITE_CONTENT = "favourite";
    const GENERIC_CONTENT = "generic";
    const EXTERNAL_CONTENT = "external";
    const POI_CONTENT = "poi";
    const WEB_CONTENT = "web";
    const SECTION_CONTENT = "section";
    const CONTENT_CONTENT = "content";
    const USER_CONTENT = "user";

    const SECTION_TYPE = "SECTION";

    const PLACEHOLDER_TYPE = "placeholder";

    private $controller_uri = array();

    private static $sqlSectionById = "SELECT s.*, log.`liked_at` 
                                        FROM `section` AS s 
                                            LEFT JOIN `cf_log` AS log ON (s.`id` = log.`form_id` AND log.`row_id` = :row_id AND log.`user_id` = :user_id)
                                        WHERE s.`deleted_at` IS NULL 
                                            AND s.`id` = :id";
    private static $sqlSectionList = "SELECT s.*, m.`id` AS media_id,
                                        m.type AS media_type, m.uri AS media_uri, m.title AS media_title, m.`description` AS media_description,
                                        m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.size AS media_size, m.format AS media_format,
                                        m.count_like AS media_count_like, m.count_share AS media_count_share
                                      FROM `section` AS s
                                      LEFT JOIN `media` AS m ON (s.`icon_id` = m.`id` AND m.`deleted_at` IS NULL)
                                      WHERE s.`deleted_at` IS NULL AND s.`status` = 0 %s
                                      ORDER BY s.`area`, s.`weight`;";
    private static $sqlTagLisByType = "SELECT `id` AS `key`, `label` AS `value`, `group_label` AS `group` FROM `tag` WHERE `deleted_at` IS NULL AND `type` = :type;";
    private static $sqlRegionList = "SELECT post_poi.* FROM post_poi INNER JOIN post ON (post.id = post_poi.post_id) WHERE post.status = :status;";
    private static $sqlCustomForm = "SELECT * FROM `custom_form_%d` WHERE `user_id` = :user_id;";

    private static $sqlCoverById = "SELECT \"%s\" AS media_context, \"%s\" AS cover_format, m.`id` AS media_id, m.`type` AS media_type, m.`uri` AS media_uri,
                                        m.`title` AS media_title, m.`description` AS media_description, m.`format` AS media_format,
                                        m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.`size` AS media_size,
                                        m.`count_like` AS media_count_like, m.`count_share` AS media_count_share
                                    FROM
                                        media AS m
                                    WHERE m.`deleted_at` IS NULL AND m.`id`=:id";

    private static $sqlGallery = "SELECT \"%s\" AS media_context, m.`id` AS media_id, m.`type` AS media_type, m.`uri` AS media_uri,
                                    m.`uri_thumb` AS media_uri_thumb, m.`title` AS media_title, m.`description` AS media_description,
                                    m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.`size` AS media_size,
                                    m.`count_like` AS media_count_like, m.`count_share` AS media_count_share, ml.liked_at AS media_liked_at
                                  FROM
                                    `gallery` AS g JOIN `media` AS m ON g.`media_id` = m.`id` JOIN `post` AS p ON g.`post_id` = p.`id`
                                    LEFT JOIN `media_log` AS ml ON ( m.`id` = ml.`media_id` AND ml.`user_id` = :user_id)
                                  WHERE
                                    m.`deleted_at` IS NULL AND g.`post_id` = :post_id AND g.`media_id` != p.`cover_id` %s
                                  ORDER BY g.`weight`;";


    /**
     * Responde with JSON data like this:
     * {
     *      "type"  : "MAIN",
     *      "header": {
     *          "layout": <LAYOUT_OBJECT/>,
     *      },
     *      "body"  : {
     *          "section": [ <SECTION_OBJECT/>, ... ],
     *      }
     * }
     *
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function main()
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $config = $this->getController()->getServiceLocator()->get('config');
                $deviceModel = new DeviceRestModel($this->getController());
                $deviceModel->updateIpAddress($_SERVER["REMOTE_ADDR"]);

                $sectionsConfig = $config['section_types'];
                $deviceApiVersion = $this->getApiVersion();
                $validSectionType = array();
                foreach ($sectionsConfig as $sectionType => $sectionConfig) {
                    if ($deviceApiVersion === self::API_VERSION_UNKNOWN) {
                        if (version_compare($sectionConfig['api_version'], "1.0.0", '<=')) {
                            $validSectionType[] = "'" . $sectionType . "'";
                        }
                    } else {
                        if (version_compare($sectionConfig['api_version'], $deviceApiVersion, '<=')) {
                            $validSectionType[] = "'" . $sectionType . "'";
                        }
                    }
                }

                $sectionFilter = "";
                if (count($validSectionType) > 0) {
                    $sectionFilter = " AND s.`type` IN (" . implode(",", $validSectionType) . ") ";
                }

                $user_id = (int)$this->getDevice()->user_id;
                $section = $this->getRegistrationCustomFormSection();
                $labelsTag = $this->getRegistrationFormLabelsTag($section);
                if (count($labelsTag) > 0) {
                    $userRestModel = new UserRestModel($this->getController());
                    $userTags = $userRestModel->getFormTagsData($section->id, $user_id, $labelsTag);
                    if (count($userTags) > 0) {
                        $userTags = StringHelper::parseMatchAgainstArray($userTags);
                        $sectionFilter .= " AND (MATCH (`tags_user`) AGAINST ('$userTags' IN BOOLEAN MODE) OR `tags_user` IS NULL) ";
                    } else {
                        $sectionFilter .= " AND `tags_user` IS NULL ";
                    }
                }

                $stmt = $this->getDb()->prepare(sprintf(self::$sqlSectionList, $sectionFilter));
                $stmt->execute();

                $sections = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\SectionSerializer', array("model" => $this));

                $page = new \stdClass();

                $geofencing = new \StdClass();
                $geofencing->regions = $this->getRegionList();

                $header = new \stdClass();
                $header->app_name = $this->getApp()->title;
                $header->demo = ((int)$this->getApp()->demo == 0) ? false : true;
                $header->white_label = ((int)$this->getApp()->white_label == 0) ? false : true;
                $header->layout = json_decode($this->getApp()->layout);
                $header->icons = $this->getSystemIconsUrl(json_decode($this->getApp()->system_icons));
                $header->splashscreen = array(
                    'bg_color' => $this->getApp()->background_color,
                    'bg_image' => $this->getCdnBaseurl() . ImageHelper::getAppSplashUrl() . $this->getApp()->splash_screen,
                    'app_icon' => $this->getCdnBaseurl() . ImageHelper::getAppLogoUrl() . $this->getApp()->app_logo,
                );
                $header->geofencing = $geofencing;

                $header->user = array(
                    'avatar' => $this->getUserAvatar(),
                    'avatar_placeholder' => $this->getUserAvatarPlaceholder(),
                );

                $header->toolbar = $this->getApp()->toolbar;

                $googleUA = array();
                if ($config['google-analytics']['ua-app'] !== null && $config['google-analytics']['ua-app'] !== "") {
                    $googleUA[] = $config['google-analytics']['ua-app'];
                }
                foreach (explode(",", $this->getApp()->google_analytics_ua) as $ua) {
                    $googleUA[] = $ua;
                }

                $header->google_ua = $googleUA;
                $header->facebook_app_id = $this->getApp()->facebook_token;

                $ws = new \stdClass();
                $ws->auth = $this->getBaseurl() . "/api/v3/user/auth/";
//                    $ws->user_detail        = $this->getBaseurl() . "/api/v3/user/";
//                    $ws->user_update        = $this->getBaseurl() . "/api/v3/user/update/";
                $ws->password_recovery = $this->getBaseurl() . "/api/v3/user/password-recovery/";

                $header->webservices = $ws;
                $header->controller_uri = $this->getControllerUriData($config, $this->getApp());

                $body = new \stdClass();
                $body->sections = $sections;
                $body->contents = null;

                $page->type = self::TYPE_MAIN;
                $page->header = $header;
                $page->body = $body;

                $response = $this->getJsonResponse(200, $page);
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function setControllerUri($key, $value)
    {
        $this->controller_uri[$key] = $value;
    }

    private function getSystemIconsUrl($icons)
    {
        $systemIcons = new \stdClass();

        $baseurl = $this->cdnBaseUrl;

        foreach ($icons as $k => $v) {
            if ($v != null && $v != "") {
                $systemIcons->$k = $baseurl . ImageHelper::getAppSystemIconUrl() . $v;
            } else {
                $systemIcons->$k = null;
            }
        }

        return $systemIcons;
    }

    public function getTagByPostType($type)
    {
        $stmt = $this->getDb()->prepare(self::$sqlTagLisByType);

        $stmt->bindParam(":type", $type, \PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getRegionList()
    {
        $published = 0;
        $stmt = $this->getDb()->prepare(self::$sqlRegionList);
        $stmt->bindParam(":status", $published, \PDO::PARAM_INT);

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\RegionSerializer', array("model" => $this));
    }

    public function getSectionById($section_id, $row_id = null, $user_id = null)
    {
        $result = null;
        $stmt = $this->getDb()->prepare(self::$sqlSectionById);

        $stmt->bindParam(":id", $section_id, \PDO::PARAM_INT);
        $stmt->bindParam(":row_id", $row_id, \PDO::PARAM_INT);
        $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $result = $stmt->fetchObject();
        };

        return $result;
    }

    public function getSectionConnectorBySectionId($sectionId)
    {
        $sectionConnector = SectionConnectorQuery::create()
            ->findOneBySectionId($sectionId);

        return $sectionConnector;
    }

    /**
     * @return null
     * @throws \Exception
     */
    private function getUserAvatar()
    {
        $avatar = null;
        $registrationForm = $this->getRegistrationCustomFormSection();
        if ($registrationForm !== null) {
            $sql = sprintf(self::$sqlCustomForm, $registrationForm->id);
            $stmt2 = $this->getDb()->prepare($sql);
            $stmt2->bindParam(":user_id", $this->device->user_id, \PDO::PARAM_INT);

            $coverId = null;
            if ($stmt2->execute()) {
                $customForm = $stmt2->fetch(\PDO::FETCH_OBJ);
                $coverId = $customForm->cover_id;
            }

            if ($coverId !== null) {
                // Get Avatar User
                $mediaModel = new MediaRestModel($this->getController());
                $media = $mediaModel->getFormMediaById($registrationForm->id, $coverId);
                if ($media != null) {
                    $cover = $media->jsonSerialize();
                    $avatar = $cover->resource;
                }
            }
        }
        return $avatar;
    }

    private function getUserAvatarPlaceholder()
    {
        $avatar = null;
        $registrationForm = $this->getRegistrationCustomFormSection();
        if ($registrationForm !== null) {

            // Get Avatar Placeholder
            $registrationFormParams = json_decode($registrationForm->params);
            foreach ($registrationFormParams->fieldsets as $fieldset) {
                foreach ($fieldset->fields as $field) {
                    if ($field->type === SectionModel::HPFORM_IMAGE && $field->is_avatar === true) {
                        if ($field->value != "") {
                            $avatar = $this->getCdnBaseurl() . ImageHelper::getCustomFormIconUrl() . $field->value;
                        } else {
                            $avatar = $this->getBaseurl() . "/img/placeholder/app-icon.png";
                        }
                    }
                }
            }
        }
        return $avatar;
    }

    private function getControllerUriData($config, $app)
    {
        $res = null;

        if ($app->controller_uri !== null && $app->controller_uri !== "") {
            $controllerUri = $config['controller_uri'];
            $controllerUriData = json_decode($app->controller_uri);
            $device = $this->device;
            if ($controllerUriData !== null) {
                $res = array();
                foreach ($controllerUriData as $os => $fields) {
                    if ($os !== $device->os) {
                        continue;
                    }
                    foreach ($fields as $fieldId => $value) {
                        $controllerName = $controllerUri[$os][$fieldId]['controller_name'];
                        $res[$controllerName] = $value;
                    }
                }
            }
        }

        foreach ($this->controller_uri as $key => $value) {
            $res[$key] = $value;
        }

        return $res;
    }

    public function getPlaceholder()
    {
        $res = null;
        $data = new \stdClass();

        $data->media_context = self::PLACEHOLDER_TYPE;
        $data->media_type = "IMAGE";
        $data->media_mime = "image/png";
        $data->media_extension = "png";
        $data->media_size = null;
        $data->media_title = "Placeholder";
        $data->media_description = "Placeholder";
        $data->media_count_like = 0;
        $data->media_liked_at = null;

        $jsonMediaSerializer = new MediaSerializer($this, $data);
        $res = $jsonMediaSerializer->jsonSerialize();

        return $res;
    }

    public function getCoverByIdAndFormat($media_id, $coverFormat, $mediaContext = MediaRestModel::UPLOAD_TYPE_COVER)
    {
        $res = array();

        $stmt = $this->getDb()->prepare(sprintf(self::$sqlCoverById, $mediaContext, $coverFormat));

        $stmt->bindParam(":id", $media_id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $results = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\MediaSerializer', array("model" => $this));
            $res = $results[0];
        }

        return $res;
    }

    public function getGalleryByContentId($id, $user_id)
    {
        $res = array();
        $imageFilter = "";

        if ($this->getApiVersion() === HpRestModel::API_VERSION_UNKNOWN) {
            // Fix for older app that managed only image
            $imageFilter = "AND m.`type` = '" . MediaModel::MEDIA_TYPE_IMAGE . "'";
        }

        $stmt = $this->getDb()->prepare(sprintf(self::$sqlGallery, MediaRestModel::UPLOAD_CONTEXT_ARCHIVE, $imageFilter));

        $stmt->bindParam(":post_id", $id, \PDO::PARAM_INT);
        $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $res = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\MediaSerializer', array("model" => $this));
        }

        return $res;
    }

    /**
     * @param $form_id
     * @param $row_id
     * @return int
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getContentLikeCount($form_id, $row_id)
    {
        $res = 0;

        $cfLog = CustomFormLogQuery::create()
            ->select(array('like_count'))
            ->withColumn('SUM(cf_log.like_count)', 'like_count')
            ->filterByFormId($form_id)
            ->filterByRowId($row_id)
            ->groupByRowId()
            ->find();

        if ($cfLog && isset($cfLog->getData()[0])) {
            $res = (int)$cfLog->getData()[0];
        }

        return $res;
    }

}