<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 20/07/16
 * Time: 12:14
 */

namespace Rest\Model;


use Common\Helper\StringHelper;
use Rest\Exception\RestApiKeySecurityException;

class FavouritePageRestModel extends ContentPageRestModel
{

    private static $sqlList = "SELECT SQL_CALC_FOUND_ROWS p.`id` AS pid, p.*, pl.*, m.`id` AS media_id,
                                  m.type AS media_type, m.uri AS media_uri, m.title AS media_title, m.`description` AS media_description,
                                  m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.size AS media_size,
                                  m.count_like AS media_count_like, m.count_share AS media_count_share, ml.liked_at AS media_liked_at,
                                  IF(JSON_VALID(p.`title`), JSON_EXTRACT(p.`title`, '$.%s'), CONCAT('\"', p.`title`, '\"')) AS title_ordered
                                FROM
                                  `post` AS p LEFT JOIN `media` AS m ON (p.`cover_id` = m.`id` AND m.`deleted_at` IS NULL)
                                  LEFT JOIN `post_log` AS pl ON (p.`id` = pl.`post_id` AND pl.`user_id` = :user_id1 )
                                  LEFT JOIN `media_log` AS ml ON (m.`id` = ml.`media_id` AND ml.`user_id` = :user_id2 )
                                WHERE
                                  p.`deleted_at` IS NULL
                                  AND pl.`liked_at` IS NOT NULL %s
                                  AND `status` = 0 AND `search` LIKE :search
                                ORDER BY title_ordered LIMIT :offset, :limit";

    public function getList($limit = 5, $offset = 0, $filter = "", $tags = null, $types = null)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $config = $this->getController()->getServiceLocator()->get('config');
                $device_lang = strtolower($this->getDeviceLang());
                $contentsConfig = $config['content_types'];
                $deviceApiVersion = $this->getApiVersion();

                $validContentType = array();
                if ($types !== null && is_string($types)) {
                    $validContentType = explode("|", $types);
                }

                if (count($validContentType) > 0) {
                    $idx = 0;
                    foreach ($contentsConfig as $contentType => $contentConfig) {
                        if (in_array($contentType, $validContentType)) {
                            if ($deviceApiVersion === self::API_VERSION_UNKNOWN) {
                                if (version_compare($contentConfig['api_version'], "1.0.0", '>')) {
                                    unset($validContentType[$idx]);
                                }
                            } else {
                                if (version_compare($contentConfig['api_version'], $deviceApiVersion, '>')) {
                                    unset($validContentType[$idx]);
                                }
                            }
                        } else {
                            unset($validContentType[$idx]);
                        }
                        $idx++;
                    }
                    $validContentType = array_values($validContentType);
                }

                $contentFilter = " AND 1=0 ";
                if (count($validContentType) > 0) {
                    $validContentType = implode("|", $validContentType);
                    $contentFilter = " AND p.`type` IN (" . StringHelper::parseSqlInString($validContentType) . ") ";
                }

                $sql = sprintf(self::$sqlList, $device_lang, $contentFilter);

                $stmt = $this->getDb()->prepare($sql);

                $type = strtoupper(self::FAVOURITE_CONTENT);
                $search = StringHelper::parseLikeString($filter);
                $pTags = StringHelper::parseLikeArray(($tags) ? $this->parseTags($tags) : array());
                $user_id = (int)$this->getDevice()->user_id;

                $stmt->bindParam(":user_id1", $user_id, \PDO::PARAM_INT);
                $stmt->bindParam(":user_id2", $user_id, \PDO::PARAM_INT);
//                $stmt->bindParam(":type",     $type,     \PDO::PARAM_STR);
                $stmt->bindParam(":limit", $limit, \PDO::PARAM_INT);
                $stmt->bindParam(":offset", $offset, \PDO::PARAM_INT);
                $stmt->bindParam(":search", $search, \PDO::PARAM_STR);

                $stmt->execute();

                $contents = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\ContentSerializer', array("model" => $this, "mode" => self::MODE_MASTER));

//                var_dump($contents); die();

                $page = new \stdClass();
                $header = new \stdClass();
                $body = new \stdClass();

                $header->demo = ((int)$this->getApp()->demo == 0) ? false : true;
                $header->white_label = ((int)$this->getApp()->white_label == 0) ? false : true;
                $header->rows = count($contents);
                $header->total_rows = (int)$this->getTotalRows();

                $body->sections = null;
                $body->contents = $contents;

                $page->type = self::TYPE_LIST;
                $page->header = $header;
                $page->body = $body;

                $response = $this->getJsonResponse(200, $page);
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }
}