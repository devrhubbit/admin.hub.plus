<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 20/07/16
 * Time: 12:18
 */

namespace Rest\Model;

use Common\Helper\StringHelper;
use Database\HubPlus\PostPoiQuery;
use Rest\Exception\RestApiKeySecurityException;

class PoiPageRestModel extends ContentPageRestModel
{

    const INDOOR_MODE = "INDOOR";
    const OUTDOOR_MODE = "OUTDOOR";

    const BEACON_REGION = "beacon_region";
    const CIRCULAR_REGION = "circular_region";

    private static $sqlBeacon = "SELECT SQL_CALC_FOUND_ROWS p.`id` AS pid, p.*,
                                      pa.id AS rid, pa.identifier, pa.uuid, pa.major, pa.minor, pa.distance, pa.type AS region_type, pa.pin,
                                      pl.*, m.`id` AS media_id,
                                      m.`type` AS media_type, m.`uri` AS media_uri, m.`title` AS media_title, m.`description` AS media_description,
                                      m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.size AS media_size,
                                      m.count_like AS media_count_like, m.count_share AS media_count_share, ml.liked_at AS media_liked_at %s,
                                      IF(JSON_VALID(p.`title`), JSON_EXTRACT(p.`title`, '$.%s'), CONCAT('\"', p.`title`, '\"')) AS title_ordered
                                  FROM
                                      `post` AS p JOIN `post_poi` AS pa ON p.`id` = pa.`post_id`
                                      LEFT JOIN `media` AS m ON (p.`cover_id` = m.`id` AND m.`deleted_at` IS NULL)
                                      LEFT JOIN `post_log` AS pl ON ( p.`id` = pl.`post_id` AND pl.`user_id` = :user_id1 )
                                      LEFT JOIN `media_log` AS ml ON ( m.`id` = ml.`media_id` AND ml.`user_id` = :user_id2 )
                                  WHERE
                                      p.`deleted_at` IS NULL AND pa.`deleted_at` IS NULL AND p.`type` = :p_type
                                      AND pa.`type` = :type %s
                                      AND `status` = 0 AND `search` LIKE :search
                                      %s
                                  ORDER BY %s title_ordered LIMIT :start_row, :length";


    private static $sqlGps = "SELECT SQL_CALC_FOUND_ROWS p.`id` AS pid, p.*, pa.id AS rid, pa.lat AS lat, pa.lng AS lng,
                                      pa.identifier, pa.lat, pa.lng, pa.radius, pa.type AS region_type, pa.pin,
                                      pl.*, m.`id` AS media_id,
                                      m.`type` AS media_type, m.`uri` AS media_uri, m.`title` AS media_title, m.`description` AS media_description,
                                      m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.size AS media_size,
                                      m.count_like AS media_count_like, m.count_share AS media_count_share, ml.liked_at AS media_liked_at,
                                      param.maxradius,
                                      (param.distance_unit
                                         * DEGREES(ACOS(COS(RADIANS(param.latpoint))
                                         * COS(RADIANS(pa.lat))
                                         * COS(RADIANS(param.longpoint) - RADIANS(pa.lng))
                                         + SIN(RADIANS(param.latpoint))
                                         * SIN(RADIANS(pa.lat))))) - (pa.radius/1000) AS w,
                                      IF(JSON_VALID(p.`title`), JSON_EXTRACT(p.`title`, '$.%s'), CONCAT('\"', p.`title`, '\"')) AS title_ordered
                                  FROM
                                      `post` AS p JOIN `post_poi` AS pa ON p.`id` = pa.`post_id`
                                      JOIN (
                                            SELECT  %f AS latpoint,  %f AS longpoint,
                                                    %f AS maxradius, %f AS distance_unit
                                      ) AS param ON 1=1
                                      LEFT JOIN `media` AS m ON (p.`cover_id` = m.`id` AND m.`deleted_at` IS NULL)
                                      LEFT JOIN `post_log` AS pl ON ( p.`id` = pl.`post_id` AND pl.`user_id` = :user_id1 )
                                      LEFT JOIN `media_log` AS ml ON ( m.`id` = ml.`media_id` AND ml.`user_id` = :user_id2 )
                                  WHERE
                                      p.`deleted_at` IS NULL AND pa.`deleted_at` IS NULL AND p.`type` = :p_type
                                      AND pa.`type` = :type
                                      AND pa.`lat`
                                         BETWEEN :min_lat
                                             AND :max_lat
                                      AND pa.`lng`
                                         BETWEEN :min_lng
                                             AND :max_lng
                                      AND `status` = 0 AND `search` LIKE :search
                                      %s
                                  HAVING w <= param.maxradius
                                  ORDER BY w, title_ordered LIMIT :start_row, :length";

    private static $getIndoorById = "SELECT p.*, p.id AS pid, pa.id AS rid, pa.identifier, pa.uuid, pa.major, pa.minor, pa.distance, pa.type AS region_type, pa.pin,
                                      pl.*, m.`id` AS media_id, m.`type` AS media_type, m.`uri` AS media_uri, m.`title` AS media_title,
                                      m.`description` AS media_description, m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.size AS media_size,
                                      m.count_like AS media_count_like, m.count_share AS media_count_share, ml.liked_at AS media_liked_at
                                  FROM
                                      `post` AS p JOIN `post_poi` AS pa ON p.`id` = pa.`post_id`
                                      LEFT JOIN `media` AS m ON (p.`cover_id` = m.`id` AND m.`deleted_at` IS NULL)
                                      LEFT JOIN `post_log` AS pl ON (p.`id` = pl.`post_id`)
                                      LEFT JOIN `media_log` AS ml ON (m.`id` = ml.`media_id`)
                                  WHERE p.`deleted_at` IS NULL AND pa.`deleted_at` IS NULL AND p.`status` = 0 AND p.`id` = :id";

    private static $getOutdoorById = "SELECT p.*, p.id AS pid, pa.id AS rid, pa.lat AS lat, pa.lng AS lng, pa.identifier, pa.lat, pa.lng, pa.radius, pa.pin,
                                      pa.type AS region_type, pl.*, m.`id` AS media_id, m.`type` AS media_type, m.`uri` AS media_uri,
                                      m.`title` AS media_title, m.`description` AS media_description, m.`mime_type` AS media_mime,
                                      m.`extension` AS media_extension, m.size AS media_size, m.count_like AS media_count_like,
                                      m.count_share AS media_count_share, ml.liked_at AS media_liked_at
                                  FROM
                                      `post` AS p JOIN `post_poi` AS pa ON p.`id` = pa.`post_id`
                                      LEFT JOIN `media` AS m ON (p.`cover_id` = m.`id` AND m.`deleted_at` IS NULL)
                                      LEFT JOIN `post_log` AS pl ON ( p.`id` = pl.`post_id`)
                                      LEFT JOIN `media_log` AS ml ON ( m.`id` = ml.`media_id`)
                                  WHERE p.`deleted_at` IS NULL AND pa.`deleted_at` IS NULL AND p.`status` = 0 AND p.`id` = :id";


    /**
     * This method return a list of POIContents related to specific indoor area list ordered by proximity priority
     * expressed by $regions_id ordered list.
     *
     * IF $scan is TRUE it return only the POIContent realated to $region_id list
     * ELSE return all the section POIContent setting low priority to not listed $region_id related post
     *
     * LINK UTILI http://www.w3resource.com/mysql/control-flow-functions/case-operator.php
     *
     * @param $limit
     * @param $offset
     * @param $filter
     * @param $tags
     * @param $scan
     * @param $region_ids
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function getIndoorList($limit, $offset, $filter, $tags, $scan, $region_ids)
    {
        $response = null;
        if ($this->isValidRequest()) {
            try {
                $contents = null;
                $regions = ($region_ids) ? explode("|", $region_ids) : [];

                if ($scan && count($regions) == 0) {
                    $contents = [];
                } else {
                    $user_id = (int)$this->getDevice()->user_id;
                    $device_lang = strtolower($this->getDeviceLang());
                    $where = "";
                    $widthTags = ($tags != null && count($tags) > 0);

                    if ($widthTags) {
                        $where = " AND MATCH (`tags`) AGAINST (:tags  IN BOOLEAN MODE) ";
                    }

                    $section = $this->getRegistrationCustomFormSection();
                    $labelsTag = $this->getRegistrationFormLabelsTag($section);
                    if (count($labelsTag) > 0) {
                        $userRestModel = new UserRestModel($this->getController());
                        $userTags = $userRestModel->getFormTagsData($section->id, $user_id, $labelsTag);
                        if (count($userTags) > 0) {
                            $userTags = StringHelper::parseMatchAgainstArray($userTags);
                            $where .= " AND (MATCH (`tags_user`) AGAINST ('$userTags' IN BOOLEAN MODE) OR `tags_user` IS NULL) ";
                        } else {
                            $where .= " AND `tags_user` IS NULL ";
                        }
                    }

                    $sqlExtra = array();

                    if (count($regions) > 0) {
                        $sqlW = ", CASE ";
                        foreach ($regions as $pos => $rid) {
                            $sqlW .= "WHEN `pa`.id = $rid THEN " . ($pos * 10) . " ";
                        }
                        $sqlW .= "ELSE " . count($regions) * 10;
                        $sqlW .= " END as w";
                        $sqlExtra['weight'] = $sqlW;
                        $sqlExtra['order_by'] = " w, ";
                    } else {
                        $sqlExtra['weight'] = "";
                        $sqlExtra['order_by'] = "";
                    }

                    if ($scan) {
                        $sqlExtra['in_region'] = " AND pa.`id` IN(" . implode(",", $regions) . ") ";
                    } else {
                        $sqlExtra['in_region'] = "";
                    }

                    $sql = sprintf(self::$sqlBeacon, $sqlExtra['weight'], $device_lang, $sqlExtra['in_region'], $where, $sqlExtra['order_by']);
                    $stmt = $this->getDb()->prepare($sql);

                    $p_type = strtoupper(self::POI_CONTENT);
                    $search = StringHelper::parseLikeString($filter);


                    $type = PoiPageRestModel::BEACON_REGION;

                    $stmt->bindParam(":type", $type, \PDO::PARAM_STR);
                    $stmt->bindParam(":user_id1", $user_id, \PDO::PARAM_INT);
                    $stmt->bindParam(":user_id2", $user_id, \PDO::PARAM_INT);
                    $stmt->bindParam(":p_type", $p_type, \PDO::PARAM_STR);
                    $stmt->bindParam(":length", $limit, \PDO::PARAM_INT);
                    $stmt->bindParam(":start_row", $offset, \PDO::PARAM_INT);
                    $stmt->bindParam(":search", $search, \PDO::PARAM_STR);
                    if ($widthTags) {
                        $pTags = StringHelper::parseMatchAgainstArray(($tags) ? $this->parseTags($tags) : array());
                        $stmt->bindParam(":tags", $pTags, \PDO::PARAM_STR);
                    }

                    $stmt->execute();

                    $contents = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\ContentSerializer', array("model" => $this, "mode" => self::MODE_MASTER));
                }

//                var_dump($contents); die();

                $page = new \stdClass();
                $header = new \stdClass();
                $body = new \stdClass();

                $header->demo = ((int)$this->getApp()->demo == 0) ? false : true;
                $header->white_label = ((int)$this->getApp()->white_label == 0) ? false : true;
                $header->rows = count($contents);
                $header->total_rows = ($header->rows > 0) ? (int)$this->getTotalRows() : 0;

                $body->sections = null;
                $body->contents = $contents;

                $page->type = "INDOOR_" . self::TYPE_LIST;
                $page->header = $header;
                $page->body = $body;

                $response = $this->getJsonResponse(200, $page);
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    /**
     * LINK UTILI: http://www.plumislandmedia.net/mysql/haversine-mysql-nearest-loc/
     *
     * @param $limit
     * @param $offset
     * @param $filter
     * @param $tags
     * @param $scan
     * @param $center
     * @param null $bbox
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function getOutdoorList($limit, $offset, $filter, $tags, $scan, $center, $bbox = null)
    {
        $response = null;
        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                $device_lang = strtolower($this->getDeviceLang());
                $where = "";
                $widthTags = ($tags != null && count($tags) > 0);

                if ($widthTags) {
                    $where = " AND MATCH (`tags`) AGAINST (:tags  IN BOOLEAN MODE) ";
                }

                $section = $this->getRegistrationCustomFormSection();
                $labelsTag = $this->getRegistrationFormLabelsTag($section);
                if (count($labelsTag) > 0) {
                    $userRestModel = new UserRestModel($this->getController());
                    $userTags = $userRestModel->getFormTagsData($section->id, $user_id, $labelsTag);
                    if (count($userTags) > 0) {
                        $userTags = StringHelper::parseMatchAgainstArray($userTags);
                        $where .= " AND (MATCH (`tags_user`) AGAINST ('$userTags' IN BOOLEAN MODE) OR `tags_user` IS NULL) ";
                    } else {
                        $where .= " AND `tags_user` IS NULL ";
                    }
                }

                $max_radius = 30000.0;
                //if($scan) { $max_radius = 0.5; }

//                AND pa.`lat`
//                    BETWEEN param.latpoint  - (param.maxradius / param.distance_unit)
//                    AND param.latpoint  + (param.maxradius / param.distance_unit)
//                AND pa.`lng`
//                    BETWEEN param.longpoint - (param.maxradius / (param.distance_unit * COS(RADIANS(param.latpoint))))
//                    AND param.longpoint + (param.maxradius / (param.distance_unit * COS(RADIANS(param.latpoint))))

                $distance_unit = 111.045; //to ge distance in km - use 69.0 to get distance in miles

//                if ($bbox != null) { //workaround to override offset and limit params in spatial query
                if (
                    !is_null($bbox->minLat) &&
                    !is_null($bbox->maxLat) &&
                    !is_null($bbox->minLng) &&
                    !is_null($bbox->maxLng)
                ) {
                    $offset = 0;
                    $limit = 999999;

                    $bboxDiagonalInKm = self::getDistanceBetweenTwoPoint($bbox->minLat, $bbox->maxLng, $bbox->maxLat, $bbox->minLng);
                    if($bboxDiagonalInKm > 60) {
                        $limit = 0;
                    }
                }
                if ($bbox->minLat == null) {
                    $bbox->minLat = $center->lat - ($max_radius / $distance_unit);
                }
                if ($bbox->maxLat == null) {
                    $bbox->maxLat = $center->lat + ($max_radius / $distance_unit);
                }
                if ($bbox->minLng == null) {
                    $bbox->minLng = $center->lng - ($max_radius / ($distance_unit * cos(deg2rad($center->lat))));
                }
                if ($bbox->maxLng == null) {
                    $bbox->maxLng = $center->lng + ($max_radius / ($distance_unit * cos(deg2rad($center->lat))));
                }

                $sql = sprintf(self::$sqlGps, $device_lang, $center->lat, $center->lng, $max_radius, $distance_unit, $where);
//                echo $sql;
                $stmt = $this->getDb()->prepare($sql);

                $p_type = strtoupper(self::POI_CONTENT);
                $search = StringHelper::parseLikeString($filter);

                $type = PoiPageRestModel::CIRCULAR_REGION;

                $stmt->bindParam(":type", $type, \PDO::PARAM_STR);
                $stmt->bindParam(":user_id1", $user_id, \PDO::PARAM_INT);
                $stmt->bindParam(":user_id2", $user_id, \PDO::PARAM_INT);
                $stmt->bindParam(":p_type", $p_type, \PDO::PARAM_STR);
                $stmt->bindParam(":length", $limit, \PDO::PARAM_INT);
                $stmt->bindParam(":start_row", $offset, \PDO::PARAM_INT);
                $stmt->bindParam(":search", $search, \PDO::PARAM_STR);
                $stmt->bindParam(":min_lat", $bbox->minLat, \PDO::PARAM_INT);
                $stmt->bindParam(":max_lat", $bbox->maxLat, \PDO::PARAM_INT);
                $stmt->bindParam(":min_lng", $bbox->minLng, \PDO::PARAM_INT);
                $stmt->bindParam(":max_lng", $bbox->maxLng, \PDO::PARAM_INT);
                if ($widthTags) {
                    $pTags = StringHelper::parseMatchAgainstArray(($tags) ? $this->parseTags($tags) : array());
                    $stmt->bindParam(":tags", $pTags, \PDO::PARAM_STR);
                }

                $stmt->execute();

                $contents = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\ContentSerializer', array("model" => $this, "mode" => self::MODE_MASTER));

                $page = new \stdClass();
                $header = new \stdClass();
                $body = new \stdClass();

                $header->demo = ((int)$this->getApp()->demo == 0) ? false : true;
                $header->white_label = ((int)$this->getApp()->white_label == 0) ? false : true;
                $header->rows = count($contents);
                $header->total_rows = ($header->rows > 0) ? (int)$this->getTotalRows() : 0;

                $body->sections = null;
                $body->contents = $contents;

                $page->type = "OUTDOOR_" . self::TYPE_LIST;
                $page->header = $header;
                $page->body = $body;

                $response = $this->getJsonResponse(200, $page);
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function getItemById($id)
    {
        $result = null;

        $sql = $this->getPoiModeQuery($id);

        $stmt = $this->getDb()->prepare($sql);
        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $result = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\ContentSerializer', array("model" => $this, "mode" => self::MODE_MASTER));
        }

        return $result;
    }

    private function getPoiModeQuery($id)
    {
        $result = null;

        $postPoi = PostPoiQuery::create()
            ->usePostQuery()
            ->filterById($id)
            ->endUse()
            ->findOne();

        if ($postPoi) {
            switch ($postPoi->getType()) {
                case self::BEACON_REGION:
                    $result = self::$getIndoorById;
                    break;
                case self::CIRCULAR_REGION:
                    $result = self::$getOutdoorById;
                    break;
            }
        }

        return $result;
    }

    const PI = 3.1415926;
    const DECIMAL_TO_RADIANT = 57.29578;
    const DISTANCE_IN_KM = 6370.0;
    const DISTANCE_IN_MILES = 3958.75;

    /**
     * Get distance between two pair of cords in Km or Miles
     *
     * @param $latPoint1
     * @param $lonPoint1
     * @param $latPoint2
     * @param $lonPoint2
     * @param bool $resultInMiles
     * @return float|int
     */
    public static function getDistanceBetweenTwoPoint($latPoint1, $lonPoint1, $latPoint2, $lonPoint2, $resultInMiles = false)
    {
        $distance = self::DISTANCE_IN_KM;
        if ($resultInMiles === true) {
            $distance = self::DISTANCE_IN_MILES;
        }

        return abs($distance * self::PI * sqrt(($latPoint2 - $latPoint1) * ($latPoint2 - $latPoint1) + cos($latPoint2 / self::DECIMAL_TO_RADIANT) * cos($latPoint1 / self::DECIMAL_TO_RADIANT) * ($lonPoint2 - $lonPoint1) * ($lonPoint2 - $lonPoint1)) / 180);
    }
}