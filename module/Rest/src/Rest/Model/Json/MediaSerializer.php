<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 20/07/16
 * Time: 15:07
 */

namespace Rest\Model\Json;


use Application\Model\MediaModel;
use Rest\Model\MediaRestModel;
use Rest\Model\HpRestModel;
use Common\Helper\ImageHelper;
use Rest\Model\PageRestModel;

class MediaSerializer extends \stdClass implements \JsonSerializable
{
    private $model = null;
    private $data = null;

    function __construct(HpRestModel $model, \stdClass $data = null)
    {
        $this->model = $model;
        if (!is_null($data)) {
            $this->data = $data;
        } else {
            $this->data = $this;
        }
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        $thumbBaseUrl = null;
        $resBaseUrl = null;
        $placeHolderImageUrl = $this->model->getBaseurl() . "/img/placeholder/app-icon.png";
        $likeUrl = null;
        $shareUrl = null;
        $delUrl = null;
        $media_context = null;
        $type = MediaModel::MEDIA_TYPE_UNKNOWN;
        $providerIcon = null;
        if (isset($this->data->media_context)) {
            $media_context = $this->data->media_context;
        }
        if (isset($this->data->media_type)) {
            $type = $this->data->media_type;
        }

        switch ($media_context) {
            case PageRestModel::PLACEHOLDER_TYPE:
                // Nothing to do
                break;
            case MediaRestModel::UPLOAD_CONTEXT_ARCHIVE:
                if (in_array($type, MediaModel::$remoteMedia)) {
                    $config = $this->model->getController()->getServiceLocator()->get('config');
                    $extraIcon = $config['extra_app_icons'];

                    $providerIcon = $extraIcon[$type]['value'];
                    $type = MediaModel::MEDIA_TYPE_REMOTE;
                    $thumbBaseUrl = $this->data->media_uri_thumb;
                    $resBaseUrl = $this->model->getBaseurl() . "/media/remote/get-page/" . $this->data->media_id;
                } else {
                    switch ($type) {
                        case MediaModel::MEDIA_TYPE_IMAGE:
                            $thumbBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getMediaThumbUrl();
                            $resBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getMediaHighUrl();
                            break;
                        default:
                            $thumbBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getMediaThumbUrl();
                            $resBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getMediaHighUrl();
                            break;
                    }
                }

                break;
            case MediaRestModel::UPLOAD_CONTEXT_FORM:
                $form_id = $this->data->media_form_id;
                $thumbBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getFormThumbUrl($form_id);
                $resBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getFormHighUrl($form_id);
                break;
            case MediaRestModel::UPLOAD_TYPE_COVER:
                $formatAvailable = json_decode($this->data->media_format);

                $thumbFormat = ImageHelper::TMB_SIZE . "x" . ImageHelper::TMB_SIZE;
                if ($formatAvailable !== null && in_array($thumbFormat, $formatAvailable)) {
                    $thumbBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getCoverIconFormatUrl($thumbFormat);
                }

                if ($formatAvailable !== null && in_array($this->data->cover_format, $formatAvailable)) {
                    $resBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getCoverIconFormatUrl($this->data->cover_format);
                } else {
                    $icon_format = $this->model->getCdnBasePath() . ImageHelper::getCoverIconFormatUrl($this->data->cover_format);
                    if (file_exists($icon_format . $this->data->media_uri)) {
                        $resBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getCoverIconFormatUrl($this->data->cover_format);
                    } else {
                        if (!file_exists($icon_format)) {
                            mkdir($icon_format, 0755, true);
                        }
                        // Generate new media from original
                        $icon_original = $this->model->getCdnBasePath() . ImageHelper::getCoverIconFormatUrl(ImageHelper::UPL_FOLDER);
                        if (file_exists($icon_original . $this->data->media_uri) && strpos($this->data->cover_format,"x") !== false) {
                            list($w, $h) = explode("x", $this->data->cover_format);
                            if (isset($w) && isset($h) && $w > 0 && $h > 0) {
                                $imageHelper = new ImageHelper();
                                $imageHelper->saveImageFullCropped($w, $h, $icon_original . $this->data->media_uri, $icon_format, $this->data->media_uri, false);
                                $resBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getCoverIconFormatUrl($this->data->cover_format);
                                $this->model->addMediaFormat($this->data->media_id, $this->data->cover_format);
                            }
                        }
                    }
                }
                break;
            case MediaRestModel::UPLOAD_TYPE_COVER_ACTION:
                $formatAvailable = json_decode($this->data->media_format);

                $thumbFormat = ImageHelper::TMB_SIZE . "x" . ImageHelper::TMB_SIZE;
                if ($formatAvailable !== null && in_array($thumbFormat, $formatAvailable)) {
                    $thumbBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getActionIconThumbUrl();
                }

                if ($formatAvailable !== null && in_array($this->data->cover_format, $formatAvailable)) {
                    $resBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getActionIconUrl($this->data->cover_format);
                } else {
                    $icon_format = $this->model->getCdnBasePath() . ImageHelper::getActionIconUrl($this->data->cover_format);
                    if (file_exists($icon_format . $this->data->media_uri)) {
                        $resBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getActionIconUrl($this->data->cover_format);
                    } else {
                        if (!file_exists($icon_format)) {
                            mkdir($icon_format, 0755, true);
                        }
                        // Generate new media from original
                        $icon_original = $this->model->getCdnBasePath() . ImageHelper::getActionIconUrl(ImageHelper::UPL_FOLDER);
                        if (file_exists($icon_original . $this->data->media_uri)) {
                            list($w, $h) = explode("x", $this->data->cover_format);
                            if (isset($w) && isset($h) && $w > 0 && $h > 0) {
                                $imageHelper = new ImageHelper();
                                $imageHelper->saveImageFullCropped($w, $h, $icon_original . $this->data->media_uri, $icon_format, $this->data->media_uri, false);
                                $resBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getActionIconUrl($this->data->cover_format);
                                $this->model->addMediaFormat($this->data->media_id, $this->data->cover_format);
                            }
                        }
                    }
                }
                break;
            default:
                $thumbBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getMediaThumbUrl();
                $resBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getMediaHighUrl();
        }

        if (property_exists($this->data, "media_id")) {
            $resource = $this->model->getBaseurl() . "/api/" . $this->model->getApp()->api_version . "/media/" . $this->data->media_id;
            $likeUrl = $resource . "/like/";
            $shareUrl = $resource . "/share/";
            $delUrl = $resource . "/";
        }

        $jsonObject = new \stdClass();

        $jsonObject->context = $media_context;
        $jsonObject->type = $type;
        $jsonObject->provider_icon = $providerIcon;
        $jsonObject->mime_type = $this->data->media_mime;
        $jsonObject->extension = $this->data->media_extension;
        $jsonObject->size = (int)$this->data->media_size;

        $jsonObject->title = $this->data->media_title;
        $jsonObject->subtitle = $this->data->media_description;

        switch ($type) {
            case MediaModel::MEDIA_TYPE_REMOTE:
                $jsonObject->thumb = $thumbBaseUrl !== null ? $thumbBaseUrl : $placeHolderImageUrl;
                $jsonObject->resource = $resBaseUrl !== null ? $resBaseUrl : $placeHolderImageUrl;
                $jsonObject->resource_shareable = $this->data->media_uri;
                break;
            default:
                $jsonObject->thumb = $thumbBaseUrl !== null ? $thumbBaseUrl . $this->data->media_uri : $placeHolderImageUrl;
                $jsonObject->resource = $resBaseUrl !== null ? $resBaseUrl . $this->data->media_uri : $placeHolderImageUrl;
                $jsonObject->resource_shareable = $jsonObject->resource;
                break;
        }

        $jsonObject->resource_like = $likeUrl;
        $jsonObject->resource_share = $shareUrl;
        $jsonObject->resource_delete = $delUrl;

        $jsonObject->like_count = (int)$this->data->media_count_like;
        $jsonObject->liked_at = isset($this->data->media_liked_at) ? $this->data->media_liked_at : null;

        return $jsonObject;
    }
}