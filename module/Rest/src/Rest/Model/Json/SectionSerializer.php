<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 18:38
 */

namespace Rest\Model\Json;


use Database\HubPlus\Section;
use Database\HubPlus\SectionQuery;
use Form\Action\ActionForm;
use Form\Section\Poi\PoiSectionForm;
use Rest\Model\ContentPageRestModel;
use Rest\Model\MediaRestModel;
use Rest\Model\PageRestModel;
use \Form\LayoutForm;
use Common\Helper\ImageHelper;
use Rest\Model\PoiPageRestModel;
use Rest\Model\UserPageRestModel;

class SectionSerializer extends \stdClass implements \JsonSerializable
{
    const TYPE_RSS = "RSS";
    const TYPE_PODCAST = "PODCAST";
    const TYPE_TWITTER = "TWITTER";
    const TYPE_REGISTER = "REGISTER";
    const TYPE_INFO = "INFO";
    const TYPE_FAVOURITE = "FAVOURITE";
    const TYPE_EVENT = "EVENT";
    const TYPE_PERSON = "PERSON";
    const TYPE_GENERIC = "GENERIC";
    const TYPE_POI = "POI";
    const TYPE_CUSTOM_FORM = "CUSTOM_FORM";
    const TYPE_WEB = "WEB";
    const TYPE_SECTION = "SECTION";
    const TYPE_LIST = "LIST";
    const TYPE_DEV = "DEV";
    const TYPE_QR_CODE = "QR_CODE";
    const TYPE_USER = "USER";

    const ADD_USER_FORM = "ADD_USER_FORM";
    const EDIT_USER_FORM = "EDIT_USER_FORM";

    const ADD_CONTENT_LABEL = "Add";

    private $model = null;
    /** @var $data \stdClass */
    private $data = null;

    function __construct(PageRestModel $model, $data = null)
    {
        $this->model = $model;
        if (is_null($data)) {
            $this->data = $this;
        } else {
            $this->data = $data;
        }

        if ($this->data->type === self::TYPE_DEV) {
            $device = $this->model->getDevice();
            $setting = json_decode($this->data->params);
            $this->model->setControllerUri($setting->controller_uri->uri, $setting->controller_uri->{$device->os});
        }
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        $wsBaseUrl = $this->model->getBaseurl() . "/api/" . $this->model->getApp()->api_version;
        $resBaseUrl = $wsBaseUrl . "/page";
        $mediaBaseUrl = $wsBaseUrl . "/media/upload/form/";
        $config = $this->model->getController()->getServiceLocator()->get('config');

        // http://osboxes.loc/admin.hub.plus/public/api/v3/media/upload/form/91/cover/?id=29&field=avatar

        $jsonObject = new \stdClass();

        $jsonObject->area = $this->data->area;
        $jsonObject->title = $this->model->getLocaleText(strtolower($this->model->getDeviceLang()), $this->data->title);

        $layout = null;
        if (isset($this->data->layout)) {
            $layout = json_decode($this->data->layout);
        }

        if ($layout === null) {
            $layout = json_decode($this->model->getApp()->layout);
        }

        if (is_bool($layout->filter)) {
            if ($layout->filter) $layout->filter = LayoutForm::SEARCH_BOTTOM_KEY;
            else $layout->filter = LayoutForm::SEARCH_DISABLED_KEY;
        }

        $coverFormat = "512x512";
        if (isset($layout->name) && $layout->name !== LayoutForm::NO_LAYOUT_NEEDED) {
            $coverFormat = $config['list_cell_types'][$layout->name]['cover_format'];
        }

        $jsonObject->icon = (isset($this->data->icon_id) && $this->data->icon_id > 0) ? $this->model->getCoverByIdAndFormat($this->data->icon_id, $coverFormat) : $this->model->getPlaceholder();
        // $jsonObject->icon_selected = (isset($this->data->icon_selected_id) && $this->data->icon_selected_id > 0) ? $this->model->getCoverByIdAndFormat($this->data->icon_selected_id, $coverFormat) : $this->model->getPlaceholder();

        $setting = null;
        if (isset($this->data->params)) {
            $setting = json_decode($this->data->params);
        }

        if ($setting === null || (is_array($setting) && count($setting) === 0)) {
            $setting = new \stdClass();
        }

        $jsonObject->type = $this->data->type;
        $jsonObject->actions = array();

        switch ($this->data->type) {
            case self::TYPE_RSS:
                $jsonObject->resource = $this->data->resource;
                break;
            case self::TYPE_PODCAST:
                $jsonObject->resource = $this->data->resource;
                $setting->paged = $layout->paged;
                $setting->autoplay = $layout->autoplay;
                $setting->streaming = $layout->streaming;
                break;
            case self::TYPE_TWITTER:
                $jsonObject->resource = null;
                break;
            case self::TYPE_REGISTER:
                $jsonObject->resource = $this->model->getBaseurl() . "/api/" . $this->model->getApp()->api_version . $this->data->resource;
                $jsonObject->tags = $this->parseTag(json_decode($this->tags), false);
                break;
            case self::TYPE_INFO:
                $setting->body = $this->model->getLocaleText(strtolower($this->model->getDeviceLang()), json_encode($setting->body));;
                $jsonObject->resource = null;
                break;
            case self::TYPE_FAVOURITE:
                $jsonObject->type = self::TYPE_LIST;
                $jsonObject->resource = $resBaseUrl . $this->data->resource;
                $jsonObject->tags = array();
                break;
            case self::TYPE_EVENT:
                $jsonObject->type = self::TYPE_LIST;
                $jsonObject->resource = $resBaseUrl . $this->data->resource;
                $jsonObject->tags = $this->parseTag($this->model->getTagByPostType($this->data->type));
                break;
            case self::TYPE_PERSON:
                $jsonObject->type = self::TYPE_LIST;
                $jsonObject->resource = $resBaseUrl . $this->data->resource;
                $jsonObject->tags = $this->parseTag($this->model->getTagByPostType($this->data->type));
                break;
            case self::TYPE_GENERIC:
                $jsonObject->type = self::TYPE_LIST;
                $jsonObject->resource = $resBaseUrl . $this->data->resource;
                $jsonObject->tags = $this->parseTag($this->model->getTagByPostType($this->data->type));
                break;
            case self::TYPE_POI:
                if ($setting->region->type === PoiPageRestModel::CIRCULAR_REGION) {
                    $setting->region->zoom = isset($setting->region->zoom) ? round(($setting->region->zoom * 100) / 22) : PoiSectionForm::ZOOM_MAP_DEFAULT;
                }
                $setting->region->type = strtoupper($setting->region->type);
                $jsonObject->type = $setting->mode . "_" . self::TYPE_LIST;
                $jsonObject->resource = $resBaseUrl . $this->data->resource;
                $jsonObject->tags = $this->parseTag($this->model->getTagByPostType($this->data->type));
                break;
            case self::TYPE_CUSTOM_FORM:
                $jsonObject->type = self::TYPE_CUSTOM_FORM;
                $jsonObject->resource = $wsBaseUrl . $this->data->resource . "/";
                $jsonObject->resource_cover = $mediaBaseUrl . $this->data->id . "/cover/";
                $jsonObject->resource_gallery = $mediaBaseUrl . $this->data->id . "/gallery/";

                $obj = new \stdClass();
                $obj->form_type = $setting->form_type;
                $setting = $obj; //$this->parseCustomFormParams($this->id, $setting);
                break;
            case self::TYPE_WEB:
                //$jsonObject->type = self::TYPE_LIST; //manca questo set, lo lascio commentato per una verifica di retrocompatibilità
                $jsonObject->resource = $resBaseUrl . $this->data->resource;
                $setting->share = $layout->share;
                $setting->like = $layout->like;
                $setting->filter = $layout->filter;
                break;
            case self::TYPE_SECTION:
                $jsonObject->type = self::TYPE_LIST;
                $jsonObject->resource = $resBaseUrl . $this->data->resource;
                $jsonObject->tags = $this->parseTag($this->model->getTagByPostType($this->data->type));
                break;
            case self::TYPE_DEV:
                $jsonObject->resource = $setting->controller_uri->uri;

                $obj = new \stdClass();
                $obj->extra_params = $setting->extra_params;
                $setting = $obj;
                break;
            case self::TYPE_USER:
                $jsonObject->type = self::TYPE_LIST;
                $jsonObject->resource = $resBaseUrl . $this->data->resource;
                $jsonObject->tags = $this->parseTag($this->model->getTagByPostType($this->data->type));

                $addAction = $this->getAddAction($setting);

                $jsonObject->actions = $addAction !== null ? array($addAction) : array();

                $setting = array();
                break;
        }

        $jsonObject->hash_id = $this->getHashId($this->data->id, $this->data->type);

        //if(count($setting) == 0) $setting = null;

        $jsonObject->layout = $layout;
        $jsonObject->setting = $setting;

        return $jsonObject;
    }

    public static function getHashId($id, $type)
    {
        return md5($id . "_" . $type);
    }

    private function getAddAction($params)
    {
        $section = SectionQuery::create()
            ->findPk($params->custom_form_id);

        $action = new \stdClass();

        if ($section) {
            $contentFilter = $params->content_filter;

            switch ($contentFilter) {
                case UserPageRestModel::CONTENT_FILTER_OTHER_USER:
                    $action = null;
                    break;
                case UserPageRestModel::CONTENT_FILTER_ALL_USER:
                case UserPageRestModel::CONTENT_FILTER_LOGGED_USER:
                    $config = $this->model->getController()->getServiceLocator()->get('config');
                    $data = $this->sectionDataToSerialize($section);

                    $jsonSerializer = new SectionToSectionSerializer($this->model, ContentPageRestModel::MODE_MASTER, $data);

                    $action->type = ActionForm::CONTENT_ACTION;
                    $action->label = is_null($params->add_icon->label) ? self::ADD_CONTENT_LABEL : $this->model->getLocaleText(strtolower($this->model->getDeviceLang()), json_encode($params->add_icon->label));
                    $action->area = $params->add_icon->position;
                    $action->params = new \stdClass();
                    $action->params->content = $jsonSerializer->jsonSerialize();

                    $layout = json_decode($section->getLayout());

                    $coverFormat = $config['list_cell_types'][$layout->name]['cover_format'];
                    $action->icon = (isset($params->add_icon->id) && $params->add_icon->id > 0) ? $this->model->getCoverByIdAndFormat($params->add_icon->id, $coverFormat, MediaRestModel::UPLOAD_TYPE_COVER_ACTION) : $this->model->getPlaceholder();
                    break;
                default:
                    $action = null;
                    break;
            }
        }

        return $action;
    }

    private function sectionDataToSerialize(Section $section)
    {
        $result = new \stdClass();

        $result->id = $section->getId();
        $result->type = $section->getType();
        $result->layout = $section->getLayout();
        $result->icon_id = $section->getIconId();
        $result->title = $section->getTitle();
        $result->description = $section->getDescription();
        $result->resource = $section->getResource();
        $result->params = $section->getParams();

        return $result;
    }

    private function parseTag($tags, $multiple = true)
    {
        $arr = [];
        foreach ($tags as $tag) {
            $data = new \stdClass();
            $data->key = (int)$tag->key;
            $data->value = str_replace("_", " ", $tag->value);

            $arr[$tag->group][] = $data;
        }

        $res = [];
        foreach ($arr as $label => $data) {
            $tag = new \stdClass();
            $tag->label = $label;
            $tag->multiple = $multiple;
            $tag->data = $data;

            $res [] = $tag;
        }

        return $res;
    }

//    Never used
//    private function parseCustomFormParams($section_id, $params) {
//        $user_id = (int) $this->model->getDevice()->user_id;
//        $userModel = new UserRestModel($this->model->getController());
//
//        return $userModel->getFormDatas($section_id, $user_id, $params);
//    }
}