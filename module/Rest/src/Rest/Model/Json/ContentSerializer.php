<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 20/07/16
 * Time: 15:07
 */

namespace Rest\Model\Json;

use Application\Model\MediaModel;
use Common\Helper\ImageHelper;
use Form\Action\ActionForm;
use Form\LayoutForm;
use Rest\Exception\RestAppIdSecurityException;
use Rest\Model\ContentPageRestModel;
use Rest\Model\MediaRestModel;
use Rest\Model\PageRestModel;
use Rest\Model\PoiPageRestModel;
use Rest\Model\WebPageRestModel;

class ContentSerializer extends \stdClass implements \JsonSerializable
{
    const EVENT_CONTENT = "EVENT";
    const PERSON_CONTENT = "PERSON";
    const GENERIC_CONTENT = "GENERIC";
    const POI_CONTENT = "POI";
    const POI_CONTENT_INDOOR = "POI_INDOOR";
    const POI_CONTENT_OUTDOOR = "POI_OUTDOOR";
    const WEB_CONTENT = "WEB";

    const CONTENT_ACTION = "CONTENT";
    const SECTION_ACTION = "SECTION";

    private $model = null;
    private $mode = null;
    /** @var $data \stdClass */
    private $data = null;

    function __construct(ContentPageRestModel $model, $mode, $data = null)
    {
        $this->model = $model;
        $this->mode = $mode;
        if (is_null($data)) {
            $this->data = $this;
        } else {
            $this->data = $data;
        }
    }

    /**
     * * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     *
     * @return mixed|\stdClass
     * @throws RestAppIdSecurityException
     */
    function jsonSerialize()
    {
        $resource = $this->model->getBaseurl() . "/api/" . $this->model->getApp()->api_version . "/page/" . strtolower($this->data->type) . "/detail/" . $this->data->pid;
        $likeUrl = $resource . "/like/";
        $shareUrl = $resource . "/share/";
        try {
            $mediaRestModel = new MediaRestModel($this->model->getController());
        } catch (RestAppIdSecurityException $exception) {
            $mediaRestModel = new MediaRestModel($this->model->getController(), false);
            $mediaRestModel->setAppId($this->model->getAppId());
        }

        $config = $this->model->getController()->getServiceLocator()->get('config');

        $jsonObject = new \stdClass();

        $jsonObject->mode = $this->mode;

        $jsonObject->type = $this->data->type;

//        $jsonObject->post_id   = $this->pid;
//        $jsonObject->region_id = $this->rid;
//        $jsonObject->user_id   = $this->model->getDevice()->user_id;

//        $jsonObject->region_type = $this->region_type;

        if (property_exists($this->data, "row_data")) {
            $rowData = json_decode($this->data->row_data);
            if ($rowData !== null) {
                foreach ($rowData as $row) {
                    if ($row->key === "region") {
                        switch ($row->value) {
                            case PoiPageRestModel::CIRCULAR_REGION:
                                $jsonObject->type = self::POI_CONTENT_OUTDOOR;
                                break;
                            case PoiPageRestModel::BEACON_REGION:
                                $jsonObject->type = self::POI_CONTENT_INDOOR;
                                break;
                        }
                    }
                }
            }
        }

        if (isset($this->data->region_type) && ($this->data->region_type == PoiPageRestModel::CIRCULAR_REGION || $this->data->region_type == PoiPageRestModel::BEACON_REGION)) {
            $jsonObject->region_id = (int)$this->data->rid;
            $jsonObject->pin_map = ($this->data->pin !== null && $this->data->pin !== "") ? $this->model->getCdnBaseurl() . ImageHelper::getPoiPin() . $this->data->pin : null;
        }

        if (isset($this->data->w) && $this->data->region_type == PoiPageRestModel::CIRCULAR_REGION) {
            $jsonObject->distance = (double)$this->data->w;
            $jsonObject->lat = (double)$this->data->lat;
            $jsonObject->lng = (double)$this->data->lng;
        }

        $jsonObject->resource_like = $likeUrl;
        $jsonObject->resource_share = $shareUrl;

        $layout = json_decode($this->data->layout);

        $coverFormat = $config['list_page_types'][$layout->name]['cover_format'];
        $jsonObject->cover = (isset($this->data->media_id) && $this->data->media_id > 0) ? $this->model->getCoverByIdAndFormat($this->data->media_id, $coverFormat) : null;

        $jsonObject->title = $this->model->getLocaleText(strtolower($this->model->getDeviceLang()), $this->data->title);
        $jsonObject->description = $this->model->getLocaleText(strtolower($this->model->getDeviceLang()), $this->data->description);
        switch (strtolower($jsonObject->type)) {
            case PageRestModel::WEB_CONTENT:
            case PageRestModel::GENERIC_CONTENT:
                $jsonObject->subtitle = $this->model->getLocaleText(strtolower($this->model->getDeviceLang()), $this->data->subtitle);
                $jsonObject->subtitle = $jsonObject->subtitle === null ? "" : $jsonObject->subtitle;
                break;
            default:
                $jsonObject->subtitle = $this->data->subtitle;
                break;
        }


        $jsonObject->like_count = (int)$this->data->count_like;
        $jsonObject->liked_at = $this->data->liked_at;

        if ($this->mode == ContentPageRestModel::MODE_MASTER) {
            $jsonObject = $this->contentListExtras($jsonObject, $this->data);

            $jsonObject->resource = $resource;
            $jsonObject->resource_shareable = $resource;
            $jsonObject->layout = $layout;

            if (is_bool($jsonObject->layout->filter)) {
                if ($jsonObject->layout->filter === true) $jsonObject->layout->filter = LayoutForm::SEARCH_BOTTOM_KEY;
                else $jsonObject->layout->filter = LayoutForm::SEARCH_DISABLED_KEY;
            }
        } else {
            $jsonObject = $this->contentDetailExtras($jsonObject, $this->data);

            $jsonObject->actions = $this->contentActions($this->data->pid);
            $jsonObject->gallery = $this->model->getGalleryByContentId($this->data->pid, $this->model->getDevice()->user_id);
            $jsonObject->body = $this->model->getLocaleText(strtolower($this->model->getDeviceLang()), $this->data->body);
            $jsonObject->row_data = json_decode($this->data->row_data);
        }

        return $jsonObject;
    }

    private function contentListExtras($jsonObject, $data)
    {
        switch ($data->type) {
            case self::EVENT_CONTENT:
                if (is_null($jsonObject->description) || $jsonObject->description === "") {
                    $jsonObject->description = $jsonObject->subtitle;
                }
                break;
            case self::WEB_CONTENT:
                $jsonObject->resource_link = $this->model->getLocaleText(strtolower($this->model->getDeviceLang()), $data->body);
                $jsonObject->resource_link_open_in_app = json_decode($data->layout)->open_in_app;
                break;
//            case self::PERSON_CONTENT:
//                break;
//            case self::POI_CONTENT:
//                break;
//            case self::GENERIC_CONTENT:
//                break;
        }

        return $jsonObject;
    }

    private function contentDetailExtras($jsonObject, $data)
    {
        switch ($data->type) {
            case self::EVENT_CONTENT:
                $jsonObject->start_at = $data->start_at;
                $jsonObject->end_at = $data->end_at;
                if (is_null($jsonObject->description) || $jsonObject->description === "") {
                    $jsonObject->description = $jsonObject->subtitle;
                }
                break;
            case self::WEB_CONTENT:
                $jsonObject->resource_link = $this->model->getLocaleText(strtolower($this->model->getDeviceLang()), $data->body);
                $jsonObject->resource_link_open_in_app = json_decode($data->layout)->open_in_app;
                $jsonObject->custom_css = $this->getCustomCode('custom_css', json_decode($data->row_data));
                $jsonObject->custom_js = $this->getCustomCode('custom_js', json_decode($data->row_data));
                break;
//            case self::PERSON_CONTENT:
//                break;
//            case self::POI_CONTENT:
//                break;
//            case self::GENERIC_CONTENT:
//                break;
        }

        return $jsonObject;
    }

    private function getCustomCode($custom, $data)
    {
        $res = null;

        foreach ($data as $k => $v) {
            if ($v->key == $custom) {
                $res = $v->value;
            }
        }

        return $res;
    }

    private function contentActions($contentId)
    {
        $result = array();

        $cntPgRestMdl = new ContentPageRestModel($this->model->getController());
        $actions = $cntPgRestMdl->getActionsById($contentId);

        foreach ($actions as $action) {
            if ($action->sstatus != 1 && $action->pstatus != 1) {
                $res = new \stdClass();

                $res->label = $this->model->getLocaleText(strtolower($this->model->getDeviceLang()), $action->label);
                $res->area = $action->area;
                $res = $this->getActionParamsAndIcon($res, $action);
                $res->type = $action->type;

                $result[] = $res;
            }
        }

        return $result;
    }

    private function getActionParamsAndIcon($result, $action)
    {
        $result->params = json_decode($action->params);

        switch ($action->type) {
            case ActionForm::CALENDAR_ACTION:
                $result->params->from = strtotime($result->params->from);
                $result->params->to = strtotime($result->params->to);
                break;
            case ActionForm::CONTENT_ACTION:
                $data = $this->contentDataToSerialize($action);

                $jsonSerializer = new ContentSerializer($this->model, \Rest\Model\ContentPageRestModel::MODE_MASTER, $data);
                $result->params->content = $jsonSerializer->jsonSerialize();
                break;
            case ActionForm::SECTION_ACTION:
                $action->type = ActionForm::CONTENT_ACTION;
                $data = $this->sectionDataToSerialize($action);

                $jsonSerializer = new SectionToSectionSerializer($this->model, \Rest\Model\ContentPageRestModel::MODE_MASTER, $data);
                unset($result->params->section);
                $result->params->content = $jsonSerializer->jsonSerialize();
                break;
            default:
                break;
        }

        $coverFormat = ImageHelper::ICON_GLOBAL_SIZE . "x" . ImageHelper::ICON_GLOBAL_SIZE;
        $result->icon = (isset($action->cover_id) && $action->cover_id > 0) ? $this->model->getCoverByIdAndFormat($action->cover_id, $coverFormat, MediaRestModel::UPLOAD_TYPE_COVER_ACTION) : $this->model->getPlaceholder();

        return $result;
    }

    private function contentDataToSerialize($action)
    {
        $result = new \stdClass();

        $result->pid = $action->pid;
        $result->type = $action->ptype;
        $result->title = $action->ptitle;
        $result->body = $action->pbody;
        $result->layout = $action->playout;
        $result->subtitle = $action->psubtitle;
        $result->description = $action->pdescription;
        $result->count_like = $action->pcount_like;
        $result->liked_at = $action->plliked_at;

        return $result;
    }

    private function sectionDataToSerialize($action)
    {
        $result = new \stdClass();

        $result->id = $action->sid;
        $result->area = $action->sarea;
        $result->type = $action->stype;
        $result->title = $action->stitle;
        $result->resource = $action->sresource;
        $result->params = $action->sparams;
        $result->layout = $action->slayout;
        $result->icon = $action->sicon;
        $result->icon_selected = $action->sicon_selected;

        return $result;
    }
}