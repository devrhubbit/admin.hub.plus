<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 15/06/17
 * Time: 12:46
 */

namespace Rest\Model\Json;

use Rest\Model\MediaRestModel;
use Rest\Model\PageRestModel;
use Form\LayoutForm;
use Common\Helper\ImageHelper;

class SectionToSectionSerializer extends \stdClass implements \JsonSerializable
{
    const TYPE_RSS = "RSS";
    const TYPE_PODCAST = "PODCAST";
    const TYPE_TWITTER = "TWITTER";
    const TYPE_REGISTER = "REGISTER";
    const TYPE_INFO = "INFO";
    const TYPE_FAVOURITE = "FAVOURITE";
    const TYPE_EVENT = "EVENT";
    const TYPE_PERSON = "PERSON";
    const TYPE_GENERIC = "GENERIC";
    const TYPE_POI = "POI";
    const TYPE_CUSTOM_FORM = "CUSTOM_FORM";
    const TYPE_WEB = "WEB";
    const TYPE_SECTION = "SECTION";
    const TYPE_LIST = "LIST";
    const TYPE_USER = "USER";
    const TYPE_USER_SECTION = "USER_SECTION";

    private $model = null;
    private $mode = null;
    /** @var $data \stdClass */
    private $data = null;
    private $userSection = false;
    private $userSectionParams = null;

    function __construct(PageRestModel $model, $mode, $data = null, $userSection = false, $userSectionParams = null)
    {
        $this->model = $model;
        $this->mode = $mode;
        if (is_null($data)) {
            $this->data = $this;
        } else {
            $this->data = $data;
        }
        $this->userSection = $userSection;

        $this->userSectionParams = $userSectionParams;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        $wsBaseUrl = $this->model->getBaseurl() . "/api/" . $this->model->getApp()->api_version;
        $mediaBaseUrl = $wsBaseUrl . "/media/upload/form/";
        $config = $this->model->getController()->getServiceLocator()->get('config');

        $jsonObject = new \stdClass();
        $jsonObject = $this->getResource($jsonObject, $this->data, $mediaBaseUrl);
        $jsonObject->resource = SectionSerializer::getHashId($this->data->id, $this->data->type);

        $layout = null;
        if (isset($this->data->layout)) {
            $layout = json_decode($this->data->layout);
        }

        $jsonObject->mode = $this->mode;
        $jsonObject->type = self::TYPE_SECTION;

        $coverFormat = $config['list_cell_types'][$layout->name]['cover_format'];
        $jsonObject->cover = (isset($this->data->icon_id) && $this->data->icon_id > 0) ? $this->model->getCoverByIdAndFormat($this->data->icon_id, $coverFormat) : $this->model->getPlaceholder();

        $jsonObject->title = $this->model->getLocaleText(strtolower($this->model->getDeviceLang()), $this->data->title);
        $jsonObject->subtitle = mb_substr($this->model->getLocaleText(strtolower($this->model->getDeviceLang()), $this->data->description), 0, 100);
        $jsonObject->description = $this->model->getLocaleText(strtolower($this->model->getDeviceLang()), $this->data->description);

        $jsonObject->like_count = (int)0;
        $jsonObject->liked_at = null;

        $jsonObject->layout = $layout;

        if ($this->userSection === true) {
            $jsonObject->type = self::TYPE_USER_SECTION;
            $jsonObject->row_id = isset($this->data->row_id) ? (int)$this->data->row_id : (int)0;
            $jsonObject->title = "";
            $jsonObject->subtitle = "";
            $descriptionArray = array();
            $body = array();

            $userData = $this->model->getFormDataById($this->data->id, $jsonObject->row_id);

            if ($this->userSectionParams !== null) {
                $resource = $wsBaseUrl . "/page/user/detail/" . $this->userSectionParams->section_id . "_" . $this->data->id . "_" . $jsonObject->row_id;
                $likeUrl = $resource . "/like/";
                $shareUrl = $resource . "/share/";

                $jsonObject->resource_like = $likeUrl;
                $jsonObject->resource_share = $shareUrl;

                $jsonObject->like_count = $this->model->getContentLikeCount($this->data->id, $jsonObject->row_id);
                $jsonObject->liked_at = isset($this->data->liked_at) ? $this->data->liked_at : null;

                $fields = $this->userSectionParams->fields_map;

                foreach ($fields as $field) {
                    foreach ($field as $fieldName => $params) {
                        $encodedFieldName = base64_encode($fieldName);
                        switch ($params->map_type) {
                            case ContentUserSerializer::MAP_TYPE_TITLE:
                                $jsonObject->title = $userData->{$encodedFieldName};
                                break;
                            case ContentUserSerializer::MAP_TYPE_SUBTITLE:
                                $jsonObject->subtitle = $userData->{$encodedFieldName};
                                $descriptionArray[] = $jsonObject->subtitle;
                                break;
                            case ContentUserSerializer::MAP_TYPE_BODY_TEXT:
                                $body[] = $userData->{$encodedFieldName};
                                break;
                            case ContentUserSerializer::MAP_TYPE_BODY_TABLE:
                                $textArray = explode(",", $userData->{$encodedFieldName});
                                $body[] = implode(", ", $textArray);
                                break;
                            case ContentUserSerializer::MAP_TYPE_COVER:
                                $MediaRestModel = new MediaRestModel($this->model->getController());
                                $jsonObject->cover = (isset($userData->{$encodedFieldName})) ? $MediaRestModel->getFormMediaById($this->data->id, $userData->{$encodedFieldName}) : null;

                                if (is_null($jsonObject->cover)) {
                                    $gallery = $MediaRestModel->getGalleryByFormId($this->data->id, $this->data->row_id);
                                    foreach ($gallery as $media) {
                                        $jsonObject->cover = $media;
                                        break;
                                    }
                                    if (is_null($jsonObject->cover)) {
                                        $jsonObject->cover = $this->model->getPlaceholder();
                                    }
                                }
                                break;
                            case ContentUserSerializer::MAP_TYPE_GALLERY:
                                break;
                        }
                    }
                }
            }

            if (count($descriptionArray) === 0) {
                $descriptionArray = array_map(
                    function ($v) {
                        return trim(strip_tags($v));
                    }, $body);
            }

            $jsonObject->description = mb_substr(implode("\n", $descriptionArray), 0, 100);
        }
        if (is_bool($jsonObject->layout->filter)) {
            if ($jsonObject->layout->filter === true) $jsonObject->layout->filter = LayoutForm::SEARCH_BOTTOM_KEY;
            else $jsonObject->layout->filter = LayoutForm::SEARCH_DISABLED_KEY;
        }

        return $jsonObject;
    }

    private function getResource($jsonObject, $data, $mediaBaseUrl)
    {
        switch ($data->type) {
            case self::TYPE_REGISTER;
                $jsonObject->tags = $this->parseTag(json_decode($this->tags), false);
                break;
            case self::TYPE_FAVOURITE;
                $jsonObject->tags = array();
                break;
            case self::TYPE_EVENT;
                $jsonObject->tags = $this->parseTag($this->model->getTagByPostType($data->type));
                break;
            case self::TYPE_PERSON;
                $jsonObject->tags = $this->parseTag($this->model->getTagByPostType($data->type));
                break;
            case self::TYPE_GENERIC;
                $jsonObject->tags = $this->parseTag($this->model->getTagByPostType($data->type));
                break;
            case self::TYPE_POI:
                $jsonObject->tags = $this->parseTag($this->model->getTagByPostType($data->type));
                break;
            case self::TYPE_CUSTOM_FORM:
                $jsonObject->resource_cover = $mediaBaseUrl . $data->id . "/cover/";
                $jsonObject->resource_gallery = $mediaBaseUrl . $data->id . "/gallery/";
                break;
            case self::TYPE_SECTION;
                $jsonObject->tags = $this->parseTag($this->model->getTagByPostType($data->type));
                break;
        }

        return $jsonObject;
    }

    private function parseTag($tags, $multiple = true)
    {
        $arr = [];
        foreach ($tags as $tag) {
            $data = new \stdClass();
            $data->key = (int)$tag->key;
            $data->value = str_replace("_", " ", $tag->value);

            $arr[$tag->group][] = $data;
        }

        $res = [];
        foreach ($arr as $label => $data) {
            $tag = new \stdClass();
            $tag->label = $label;
            $tag->multiple = $multiple;
            $tag->data = $data;

            $res [] = $tag;
        }

        return $res;
    }

    private function getSectionIcon($iconName)
    {
        $iconBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getCoverIconThumbUrl() . $iconName;

        $jsonObject = new \stdClass();

        $jsonObject->context = null;
        $jsonObject->type = null;
        $jsonObject->mime_type = null;
        $jsonObject->extension = null;
        $jsonObject->size = (int)0;

        $jsonObject->title = null;
        $jsonObject->subtitle = null;

        $jsonObject->thumb = $iconBaseUrl;
        $jsonObject->resource = $iconBaseUrl;

        $jsonObject->resource_like = null;
        $jsonObject->resource_share = null;
        $jsonObject->resource_delete = null;

        $jsonObject->like_count = (int)0;
        $jsonObject->liked_at = null;

        return $jsonObject;
    }
}