<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 20/07/16
 * Time: 15:07
 */

namespace Rest\Model\Json;

use Rest\Model\UserRestModel;
use Common\Helper\ImageHelper;


class UserSerializer extends \stdClass implements \JsonSerializable
{
    private $model = null;

    function __construct(UserRestModel $model)
    {
        $this->model = $model;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        $thumbBaseUrl = $this->model->getCdnBaseurl() . ImageHelper::getHostThumbUrl($this->model->getApp()->user_id);

        $address = new \stdClass();
        $address->address = $this->address;
        $address->city = $this->city;
        $address->state = $this->state;
        $address->country = $this->country;
        $address->zipcode = $this->zipcode;

        $jsonObject = new \stdClass();
        $jsonObject->avatar = (isset($this->imagePath)) ? $thumbBaseUrl . $this->imagePath : null;
        $jsonObject->email = $this->email;
        $jsonObject->username = $this->username;
        $jsonObject->firstname = $this->firstname;
        $jsonObject->lastname = $this->lastname;
        $jsonObject->phone = $this->phone;
        $jsonObject->privacy = (bool)$this->privacy;
        $jsonObject->terms = (bool)$this->terms;
        $jsonObject->tag = $this->model->getUserTagIds($this->tags);
        $jsonObject->address = $address;

        return $jsonObject;
    }
}