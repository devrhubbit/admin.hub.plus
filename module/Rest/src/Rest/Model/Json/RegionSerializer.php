<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 24/10/16
 * Time: 14:44
 */

namespace Rest\Model\Json;

use Common\Helper\ImageHelper;
use Rest\Model\HpRestModel;
use Rest\Model\PoiPageRestModel;

class RegionSerializer extends \stdClass implements \JsonSerializable
{

    const CIRCULAR_REGION = "circular_region";
    const BEACON_REGION = "beacon_region";

    private $model = null;

    function __construct(HpRestModel $model)
    {
        $this->model = $model;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        $jsonObject = new \stdClass();

        $poiPageRestModel = new PoiPageRestModel($this->model->getController());
        $content = $poiPageRestModel->getItemById($this->post_id);

        $jsonObject->id = $this->id;
        $jsonObject->type = strtoupper($this->type);
        $jsonObject->identifier = $this->identifier;
        $jsonObject->content = is_array($content) ? $content[0] : null;
        $jsonObject->notification = (bool)$this->poi_notification;
        $jsonObject->pin_map = ($this->pin !== null && $this->pin !== "") ? $this->model->getCdnBaseurl() . ImageHelper::getPoiPin() . $this->pin : null;

        switch ($this->type) {
            case RegionSerializer::CIRCULAR_REGION:
                $jsonObject->latitude = $this->lat;
                $jsonObject->longitude = $this->lng;
                $jsonObject->radius = $this->radius;
                break;
            case RegionSerializer::BEACON_REGION:
                $jsonObject->uuid = $this->uuid;
                $jsonObject->major = $this->major;
                $jsonObject->minor = $this->minor;
                $jsonObject->distance = $this->distance;
                break;
        }

        return $jsonObject;
    }
}