<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 19/10/17
 * Time: 18:11
 */

namespace Rest\Model\Json;

use Application\Model\SectionModel;
use Common\Helper\StringHelper;
use Database\HubPlus\AddressQuery;
use Database\HubPlus\Section;
use Form\LayoutForm;
use Rest\Model\ContentPageRestModel;
use Rest\Model\MediaRestModel;
use Rest\Model\PageRestModel;
use Rest\Model\UserPageRestModel;

class ContentUserSerializer extends \stdClass implements \JsonSerializable
{
    const GENERIC_CONTENT = "GENERIC";

    const MAP_TYPE_TITLE = "title";
    const MAP_TYPE_SUBTITLE = "subtitle";
    const MAP_TYPE_BODY_TEXT = "body_text";
    const MAP_TYPE_BODY_TABLE = "body_table";
    const MAP_TYPE_COVER = "cover";
    const MAP_TYPE_GALLERY = "gallery";

    private $model = null;
    private $mode = null;
    /** @var Section $section */
    private $section = null;
    private $customFormId = null;
    private $userId = null;
    /** @var $data \stdClass */
    private $data = null;

    function __construct(UserPageRestModel $model, $mode, $section, $customFormId, $userId, $data = null)
    {
        $this->model = $model;
        $this->mode = $mode;
        $this->section = $section;
        $this->customFormId = $customFormId;
        $this->userId = $userId;

        if (is_null($data)) {
            $this->data = $this;
        } else {
            $this->data = $data;
        }
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        $sectionParams = json_decode($this->section->getParams());
        $sectionLayout = json_decode($this->section->getLayout());
        $pm = new PageRestModel($this->model->getController());
        $customFormSection = $pm->getSectionById($this->customFormId, $this->data->cf_row_id, $this->userId);

        if (isset($this->userId) && $this->userId === $this->data->user_id) {
            // Return Custom Form
            $customFormSection->row_id = $this->data->cf_row_id;
            $sectionParams->section_id = $this->section->getId();

            $jsonSerializer = new SectionToSectionSerializer($this->model, $this->mode, $customFormSection, true, $sectionParams);
            $jsonObject = $jsonSerializer->jsonSerialize();

            $fields = array();
            $mediaRestModel = new MediaRestModel($this->model->getController());
            // Get custom form fields data
            if ($sectionParams !== null) {
                $fields = $sectionParams->fields_map;
            }
            foreach ($fields as $field) {
                foreach ($field as $fieldName => $params) {
                    $encodedFieldName = base64_encode($fieldName);

                    switch ($params->map_type) {
                        case self::MAP_TYPE_TITLE:
                            break;
                        case self::MAP_TYPE_SUBTITLE:
                            break;
                        case self::MAP_TYPE_BODY_TEXT:
                            break;
                        case self::MAP_TYPE_BODY_TABLE:
                            break;
                        case self::MAP_TYPE_COVER:
                            $jsonObject->cover = (isset($this->data->{$encodedFieldName})) ? $mediaRestModel->getFormMediaById($this->customFormId, $this->data->{$encodedFieldName}) : null;
                            break;
                        case self::MAP_TYPE_GALLERY:
                            break;
                    }
                }
            }

            if (is_null($jsonObject->cover)) {
                $gallery = $mediaRestModel->getGalleryByFormId($this->customFormId, $this->data->cf_row_id);
                foreach ($gallery as $media) {
                    $jsonObject->cover = $media;
                    break;
                }
                if (is_null($jsonObject->cover)) {
                    $jsonObject->cover = $this->model->getPlaceholder();
                }
            }
        }
        else {
            $jsonObject = new \stdClass();

            $jsonObject->mode = $this->mode;
            $jsonObject->type = self::GENERIC_CONTENT;

            $resource = $this->model->getBaseurl() . "/api/" . $this->model->getApp()->api_version . "/page/user/detail/" . $this->section->getId() . "_" . $this->customFormId . "_" . $this->data->cf_row_id;
            $likeUrl = $resource . "/like/";
            $shareUrl = $resource . "/share/";

            $jsonObject->resource_like = $likeUrl;
            $jsonObject->resource_share = $shareUrl;

            $layout = json_decode($this->section->getLayout());
            $layout->name = $sectionParams->detail_layout;

            $jsonObject->cover = null;

            $descriptionArray = array();
            $body = array();
            $fields = array();
            // Get custom form fields data
            if ($sectionParams !== null) {
                $fields = $sectionParams->fields_map;
            }

            $customFormFieldSets = array();
            $customFormFields = array();
            if (isset($customFormSection->params)) {
                $customFormParams = json_decode($customFormSection->params);
                if ($customFormParams !== null) {
                    $customFormFieldSets = $customFormParams->fieldsets;
                }
            }

            $secondaryBgColor = "#ffffff";
            if (isset($sectionLayout)) {
                $secondaryBgColor = $sectionLayout->theme->color->secondary_bg;
            }

            foreach ($customFormFieldSets as $fieldSet) {
                $customFormFields = array_merge($customFormFields, $fieldSet->fields);
            }

            $previousMapType = null;
            $title = array();
            $table = "";
            foreach ($fields as $field) {
                foreach ($field as $fieldName => $params) {
                    $encodedFieldName = base64_encode($fieldName);

                    if ($previousMapType === self::MAP_TYPE_BODY_TABLE && $params->map_type !== self::MAP_TYPE_BODY_TABLE) {
                        $table .= "</table>";
                    }

                    switch ($params->map_type) {
                        case self::MAP_TYPE_TITLE:
                            $fieldType = SectionModel::HPFORM_INPUT_TEXT;
                            foreach ($customFormFields as $customFormField) {
                                $fieldType = $customFormField->type;
                            }
                            switch ($fieldType) {
                                case SectionModel::HPFORM_ADDRESS:
                                    $title[] = $this->getAddressStringById($this->data->{$encodedFieldName});
                                    break;
                                default:
                                    $title[] = $this->data->{$encodedFieldName};
                                    break;
                            }
                            break;
                        case self::MAP_TYPE_SUBTITLE:
                            $fieldType = SectionModel::HPFORM_INPUT_TEXT;
                            foreach ($customFormFields as $customFormField) {
                                $fieldType = $customFormField->type;
                            }
                            switch ($fieldType) {
                                case SectionModel::HPFORM_ADDRESS:
                                    $jsonObject->subtitle = $this->getAddressStringById($this->data->{$encodedFieldName});
                                    break;
                                default:
                                    $jsonObject->subtitle = $this->data->{$encodedFieldName};
                                    break;
                            }
                            $descriptionArray[] = $jsonObject->subtitle;
                            break;
                        case self::MAP_TYPE_BODY_TEXT:
                            $fieldType = SectionModel::HPFORM_INPUT_TEXT;
                            foreach ($customFormFields as $customFormField) {
                                if (isset($customFormField->name) && $customFormField->name === $fieldName) {
                                    $fieldName = $customFormField->label->{strtolower($this->model->getDeviceLang())};
                                    $fieldType = $customFormField->type;
                                } elseif ($customFormField->label === $fieldName) {
                                    $fieldType = $customFormField->type;
                                }
                            }

                            switch ($fieldType) {
                                case SectionModel::HPFORM_INPUT_TEXT:
                                    $body[] = "<p><b>$fieldName</b>: " . $this->data->{$encodedFieldName} . "</p>";
                                    break;
                                case SectionModel::HPFORM_INPUT_DESCR:
                                case SectionModel::HPFORM_TEXTAREA:
                                    $body[] = "<p><b>$fieldName</b><br>" . $this->data->{$encodedFieldName} . "</p>";
                                    break;
                                case SectionModel::HPFORM_ADDRESS:
                                    $body[] = "<p><b>$fieldName</b><br>" . $this->getAddressStringById($this->data->{$encodedFieldName}) . "</p>";
                                    break;
                            }
                            $previousMapType = $params->map_type;
                            break;
                        case self::MAP_TYPE_BODY_TABLE:
                            foreach ($customFormFields as $customFormField) {
                                if (isset($customFormField->name) && $customFormField->name === $fieldName) {
                                    $fieldName = $customFormField->label->{strtolower($this->model->getDeviceLang())};
                                }
                            }

                            $textArray = explode(",", $this->data->{$encodedFieldName});

                            if ($previousMapType !== $params->map_type) {
                                $table .= "<table>";
                            }

                            $idx = 0;
                            foreach ($textArray as $value) {
                                if ($idx === 0) {
                                    $table .= "<tr>";
                                    $table .= "<td>" . $fieldName . "</td>";
                                    $table .= "<td>";
                                }

                                $table .= $value . "<br>";
                                $idx++;
                            }

                            $table .= "</td>";
                            $table .= "</tr>";

                            $previousMapType = $params->map_type;
                            break;
                        case self::MAP_TYPE_COVER:
                            $mediaRestModel = new MediaRestModel($this->model->getController());
                            $jsonObject->cover = (isset($this->data->{$encodedFieldName})) ? $mediaRestModel->getFormMediaById($this->customFormId, $this->data->{$encodedFieldName}) : null;
                            break;
                        case self::MAP_TYPE_GALLERY:
                            break;
                    }
                }
            }

            if (!StringHelper::endsWith($table, "</table>")) {
                $table .= "</table>";
            }

            $body[] = $table;

            if (count($descriptionArray) === 0) {
                $descriptionArray = array_map(
                    function ($v) {
                        return trim(strip_tags($v));
                    }, $body);
            }

            $body[] = "<style>tr:nth-child(even){background-color:" . $secondaryBgColor . ";opacity: 0.5;}table{width:100%;border-collapse:collapse;}th,td{border-bottom:1px solid " . $secondaryBgColor . ";}</style>";

            $jsonObject->title = implode(" ", $title);
            $jsonObject->description = mb_substr(implode("\n", $descriptionArray), 0, 100);

            $jsonObject->like_count = $this->model->getContentLikeCount($this->customFormId, $this->data->cf_row_id);
            $jsonObject->liked_at = isset($this->data->liked_at) ? $this->data->liked_at : null;

            $mediaRestModel = new MediaRestModel($this->model->getController());
            if ($this->mode == ContentPageRestModel::MODE_MASTER) {
                $jsonObject->resource = $resource;
                $jsonObject->resource_shareable = $resource;
                $jsonObject->layout = $layout;

                if (is_null($jsonObject->cover)) {
                    $gallery = $mediaRestModel->getGalleryByFormId($this->customFormId, $this->data->cf_row_id);
                    foreach ($gallery as $media) {
                        $jsonObject->cover = $media;
                        break;
                    }
                    if (is_null($jsonObject->cover)) {
                        $jsonObject->cover = $this->model->getPlaceholder();
                    }
                }

                if (is_bool($jsonObject->layout->filter)) {
                    if ($jsonObject->layout->filter === true) $jsonObject->layout->filter = LayoutForm::SEARCH_BOTTOM_KEY;
                    else $jsonObject->layout->filter = LayoutForm::SEARCH_DISABLED_KEY;
                }
            } else {
                $jsonObject->gallery = $mediaRestModel->getGalleryByFormId($this->customFormId, $this->data->cf_row_id);
                if (is_null($jsonObject->cover)) {
                    foreach ($jsonObject->gallery as $media) {
                        $jsonObject->cover = $media;
                        break;
                    }
                    if (is_null($jsonObject->cover)) {
                        $jsonObject->cover = $this->model->getPlaceholder();
                    }
                }
                $jsonObject->body = implode("<br>", $body);
                $row_data = json_decode($this->data->row_data);
                $contentData = array();
                if (isset($row_data->data)) {
                    foreach ($row_data->data as $data) {
                        $contentData[] = array(
                            "key" => $data->label,
                            "value" => $data->value
                        );
                    }
                }

                $jsonObject->row_data = $contentData;
            }
        }

        return $jsonObject;
    }

    /**
     * @param $jsonObject
     * @return mixed|null
     * @throws \Exception
     */
    private function getMasterCover($jsonObject) {
        $cover = null;

        $mediaRestModel = new MediaRestModel($this->model->getController());
        $gallery = $mediaRestModel->getGalleryByFormId($this->customFormId, $this->data->cf_row_id);
        foreach ($gallery as $media) {
            $cover = $media;
            break;
        }
        if (is_null($jsonObject->cover)) {
            $cover = $this->model->getPlaceholder();
        }

        return $cover;
    }

    private function getAddressStringById($addressId)
    {
        $addressString = "";

        $address = AddressQuery::create()->findPk($addressId);
        if ($address) {
            $street = $address->getAddress() !== null ? $address->getAddress() : "";
            $city = $address->getCity() !== null ? $address->getCity() : "";
            $country = $address->getCountry() !== null ? $address->getCountry() : "";
            $zipCode = $address->getZipcode() !== null ? $address->getZipcode() : "";

            $addressString = trim($street . " " . $city . " " . $country . " " . $zipCode);
        }

        return $addressString;
    }

}