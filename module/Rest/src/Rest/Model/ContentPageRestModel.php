<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 20/07/16
 * Time: 12:14
 */

namespace Rest\Model;


use Application\Model\SectionModel;
use Database\HubPlus\CustomFormExtraLog;
use Database\HubPlus\CustomFormLogQuery;
use Database\HubPlus\PostLogQuery;
use Propel\Runtime\Exception\PropelException;
use Rest\Exception\RestApiKeySecurityException;

class ContentPageRestModel extends PageRestModel
{
    const MODE_MASTER = "MASTER_ITEM";
    const MODE_DETAIL = "DETAIL_ITEM";

    const LOG_VIEW = "VIEW";
    const LOG_LIKE = "LIKE";
    const LOG_UNLIKE = "UNLIKE";
    const LOG_SHARE = "SHARE";
    const LOG_DELETE = "DELETE";

    const QUERY_LIMIT = 5;
    const QUERY_OFFSET = 0;

    private static $sqlTags = "SELECT `label` FROM `tag` WHERE `deleted_at` IS NULL AND id IN (%s);";

    private static $sqlDetail = "SELECT p.`id` AS pid, p.*, pe.*, pl.*, m.`id` AS media_id,
                                  m.type AS media_type, m.uri AS media_uri, m.title AS media_title, m.`description` AS media_description,
                                  m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.size AS media_size, m.format AS media_format,
                                  m.count_like AS media_count_like, m.count_share AS media_count_share, ml.liked_at AS media_liked_at
                                 FROM
                                  `post` AS p
                                  LEFT JOIN `post_event` AS pe ON p.`id` = pe.`post_id`
                                  LEFT JOIN `media` AS m ON (p.`cover_id` = m.`id` AND m.`deleted_at` IS NULL)
                                  LEFT JOIN `post_log` AS pl ON ( p.`id` = pl.`post_id` AND pl.`user_id` = :user_id1 )
                                  LEFT JOIN `media_log` AS ml ON ( m.`id` = ml.`media_id` AND ml.`user_id` = :user_id2 )
                                 WHERE
                                  p.`deleted_at` IS NULL
                                  AND `status` = 0 AND p.`id` = :id;";

    private static $sqlUserContentDetail = "SELECT cf.`id` AS cf_row_id, cf.*, 
                                                   cfl.form_id, cfl.liked_at, cfl.shared_at
                                            FROM `custom_form_%d` AS cf
                                              LEFT JOIN `cf_log` AS cfl ON ( cf.`id` = cfl.`row_id` AND cfl.`user_id` = :user_id AND cfl.`form_id` = :form_id )
                                            WHERE cf.`id` = :id;";

    private static $sqlExtraLog = "INSERT INTO `post_extra_log` (`id`, `post_id`, `device_id`, `type`, `timestamp`)
                                   VALUES (NULL, :post_id, :dev_id, :type, NOW());";

    private static $sqlAddPostLog = "INSERT INTO `post_log`
                                      (`id`, `post_id`, `user_id`, `liked_at`, `shared_at`,`share_count`,`view_count`)
                                     VALUES
                                      (NULL, :post_id, :user_id, :liked_at, :shared_at, :share_count, :view_count);";

    private static $sqlPostLogView = "UPDATE post_log SET view_count = view_count+1 WHERE id = :id;";
    private static $sqlPostLogShare = "UPDATE post_log SET share_count = share_count+1, shared_at = NOW() WHERE id = :id;";
    private static $sqlPostLogLike = "UPDATE post_log SET liked_at = NOW() WHERE id = :id;";
    private static $sqlPostLogUnlike = "UPDATE post_log SET liked_at = NULL WHERE id = :id;";

    private static $sqlPostLike = "UPDATE post SET count_like = count_like+1 WHERE id = :post_id AND deleted_at IS NULL;";
    private static $sqlPostUnlike = "UPDATE post SET count_like = count_like-1 WHERE id = :post_id AND deleted_at IS NULL;";
    private static $sqlPostShare = "UPDATE post SET count_share = count_share+1 WHERE id = :post_id AND deleted_at IS NULL;";

    private static $sqlActionsById = "SELECT DISTINCT SQL_CALC_FOUND_ROWS pa.*,
                                          s.id AS sid, s.area AS sarea, s.type AS stype, s.title AS stitle, s.icon_id AS sicon, s.icon_selected_id AS sicon_selected,
                                          s.resource AS sresource, s.params AS sparams, s.layout AS slayout, s.status AS sstatus,
                                          p.id AS pid, p.type AS ptype, p.status AS pstatus, p.title AS ptitle, p.subtitle AS psubtitle,
                                          p.description AS pdescription, p.body AS pbody, p.layout AS playout, p.count_like AS pcount_like,
                                          pl.liked_at AS plliked_at
                                        FROM `post_action` AS pa
                                          LEFT JOIN `section` AS s ON (pa.`section_id` = s.`id`)
                                          LEFT JOIN `post` AS p ON (pa.`content_id` = p.`id`)
                                          LEFT JOIN `post_log` AS pl ON (pa.`content_id` = pl.`post_id`)
                                        WHERE pa.`post_id` = :id
                                        ORDER BY pa.`weight`";

    private static $sqlPostById = "SELECT p.`id` AS pid, p.*, pe.*, pl.*, m.`id` AS media_id,
                                  m.type AS media_type, m.uri AS media_uri, m.title AS media_title, m.`description` AS media_description,
                                  m.`mime_type` AS media_mime, m.`extension` AS media_extension, m.size AS media_size, m.format AS media_format,
                                  m.count_like AS media_count_like, m.count_share AS media_count_share, ml.liked_at AS media_liked_at
                                 FROM
                                  `post` AS p
                                  LEFT JOIN `post_event` AS pe ON p.`id` = pe.`post_id`
                                  LEFT JOIN `media` AS m ON (p.`cover_id` = m.`id` AND m.`deleted_at` IS NULL)
                                  LEFT JOIN `post_log` AS pl ON ( p.`id` = pl.`post_id` AND pl.`user_id` = :user_id1 )
                                  LEFT JOIN `media_log` AS ml ON ( m.`id` = ml.`media_id` AND ml.`user_id` = :user_id2 )
                                 WHERE
                                  p.`deleted_at` IS NULL
                                  AND `status` = 0 AND MD5(p.`id`) = :id;";


    private function getPostLog($post_id, $user_id)
    {
        $user_id = $user_id === 0 ? null : $user_id;

        $postLog = PostLogQuery::create()
            ->filterByUserId($user_id)
            ->filterByPostId($post_id)
            ->findOne();

        $postLogId = null;
        if ($postLog) {
            $postLogId = $postLog->getId();
        }

        return $postLogId;
    }

    private function getCustomFormLog($form_id, $row_id, $user_id)
    {
        $cfLog = CustomFormLogQuery::create()
            ->filterByFormId($form_id)
            ->filterByRowId($row_id)
            ->filterByUserId($user_id)
            ->findOneOrCreate();

        return $cfLog;
    }

    private function addPostLog($post_id, $user_id, $liked_at = 0, $shared_at = 0, $view_count = 0)
    {
        $stmt = $this->getDb()->prepare(self::$sqlAddPostLog);

        $share_count = 0;
        $like_date = null;
        $share_date = null;

        if ($liked_at > 0) {
            $like_date = date('Y-m-d G:i:s', $liked_at);
        }

        if ($shared_at > 0) {
            $share_count++;
            $share_date = date('Y-m-d G:i:s', $shared_at);
        }

        $user_id = $user_id === 0 ? null : $user_id;

        $stmt->bindParam(":post_id", $post_id, \PDO::PARAM_INT);
        $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
        $stmt->bindParam(":liked_at", $like_date, \PDO::PARAM_STR);
        $stmt->bindParam(":shared_at", $share_date, \PDO::PARAM_STR);
        $stmt->bindParam(":share_count", $share_count, \PDO::PARAM_INT);
        $stmt->bindParam(":view_count", $view_count, \PDO::PARAM_INT);

        $stmt->execute();
    }

    private function addUserContentExtraLog($form_id, $row_id, $device_id, $type)
    {
        $cfExtraLog = new CustomFormExtraLog();
        $cfExtraLog
            ->setFormId($form_id)
            ->setRowId($row_id)
            ->setDeviceId($device_id)
            ->setType($type)
            ->setTimestamp(time())
            ->save();
    }

    private function addExtraLog($post_id, $device_id, $type)
    {
        $stmt = $this->getDb()->prepare(self::$sqlExtraLog);

        $stmt->bindParam(":post_id", $post_id, \PDO::PARAM_INT);
        $stmt->bindParam(":dev_id", $device_id, \PDO::PARAM_INT);
        $stmt->bindParam(":type", $type, \PDO::PARAM_STR);

        $stmt->execute();
    }

    private function updateUserContentViewCount($form_id, $row_id, $device_id, $user_id)
    {
        $this->addUserContentExtraLog($form_id, $row_id, $device_id, self::LOG_VIEW);

        $cfLog = $this->getCustomFormLog($form_id, $row_id, $user_id);

        $cfLog
            ->setFormId($form_id)
            ->setRowId($row_id)
            ->setUserId($user_id)
            ->setViewCount($cfLog->getViewCount() + 1);
        $cfLog->save();
    }

    private function updateViewCount($post_id, $device_id, $user_id)
    {
//        $user_id   = (int) $this->getDevice()->user_id;
//        $device_id = (int) $this->getDevice()->id;
        $this->addExtraLog($post_id, $device_id, self::LOG_VIEW);

        if ($user_id != 0) {
            $postLogId = $this->getPostLog($post_id, $user_id);

            if (!is_null($postLogId)) {
                $stmt = $this->getDb()->prepare(self::$sqlPostLogView);
                $stmt->bindParam(":id", $postLogId, \PDO::PARAM_INT);
                $stmt->execute();
            } else {
                $this->addPostLog($post_id, $user_id, 0, 0, 1);
            }
        }
    }

    public function likeUserContent($id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                if ($user_id != 0) {
                    list($section_id, $form_id, $row_id) = explode("_", $id);
                    $device_id = (int)$this->getDevice()->id;
                    $this->addUserContentExtraLog($form_id, $row_id, $device_id, self::LOG_LIKE);

                    $cfLog = $this->getCustomFormLog($form_id, $row_id, $user_id);
                    $cfLog
                        ->setFormId($form_id)
                        ->setRowId($row_id)
                        ->setUserId($user_id)
                        ->setLikeCount($cfLog->getLikeCount() + 1)
                        ->setLikedAt(time());
                    $cfLog->save();

                    $response = $this->getJsonResponse(202);
                } else {
                    $response = $this->getJsonResponse(412);
                }
            } catch (PropelException $exception) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $exception->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function like($post_id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                if ($user_id != 0) {
                    $device_id = (int)$this->getDevice()->id;
                    $this->addExtraLog($post_id, $device_id, self::LOG_LIKE);

                    $postLogId = $this->getPostLog($post_id, $user_id);

                    if (!is_null($postLogId)) {
                        $stmt = $this->getDb()->prepare(self::$sqlPostLogLike);
                        $stmt->bindParam(":id", $postLogId, \PDO::PARAM_INT);
                        $stmt->execute();
                    } else {
                        $this->addPostLog($post_id, $user_id, time(), 0, 0);
                    }

                    $stmt = $this->getDb()->prepare(self::$sqlPostLike);
                    $stmt->bindParam(":post_id", $post_id, \PDO::PARAM_INT);
                    $stmt->execute();

                    $response = $this->getJsonResponse(202);
                } else {
                    $response = $this->getJsonResponse(412);
                }
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function unlikeUserContent($id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                if ($user_id != 0) {
                    list($section_id, $form_id, $row_id) = explode("_", $id);
                    $device_id = (int)$this->getDevice()->id;
                    $this->addUserContentExtraLog($form_id, $row_id, $device_id, self::LOG_UNLIKE);

                    $cfLog = $this->getCustomFormLog($form_id, $row_id, $user_id);
                    $cfLog
                        ->setFormId($form_id)
                        ->setRowId($row_id)
                        ->setUserId($user_id)
                        ->setLikedAt(null)
                        ->setLikeCount($cfLog->getLikeCount() - 1)
                        ->save();

                    $response = $this->getJsonResponse(202);
                } else {
                    $response = $this->getJsonResponse(412);
                }
            } catch (PropelException $exception) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $exception->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function unlike($post_id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                if ($user_id != 0) {
                    $device_id = (int)$this->getDevice()->id;
                    $this->addExtraLog($post_id, $device_id, self::LOG_UNLIKE);

                    $postLogId = $this->getPostLog($post_id, $user_id);

                    if (!is_null($postLogId)) {
                        $stmt = $this->getDb()->prepare(self::$sqlPostLogUnlike);
                        $stmt->bindParam(":id", $postLogId, \PDO::PARAM_INT);
                        $stmt->execute();

                        $stmt2 = $this->getDb()->prepare(self::$sqlPostUnlike);
                        $stmt2->bindParam(":post_id", $post_id, \PDO::PARAM_INT);
                        $stmt2->execute();
                    }

                    $response = $this->getJsonResponse(202);
                } else {
                    $response = $this->getJsonResponse(412);
                }
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function shareUserContent($id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                list($section_id, $form_id, $row_id) = explode("_", $id);
                $user_id = (int)$this->getDevice()->user_id;
                $device_id = (int)$this->getDevice()->id;
                $this->addUserContentExtraLog($form_id, $row_id, $device_id, self::LOG_SHARE);

                $cfLog = $this->getCustomFormLog($form_id, $row_id, $user_id);
                $cfLog
                    ->setFormId($form_id)
                    ->setRowId($row_id)
                    ->setUserId($user_id)
                    ->setShareCount($cfLog->getShareCount() + 1)
                    ->setSharedAt(time())
                    ->save();

                $response = $this->getJsonResponse(202);
            } catch (PropelException $exception) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $exception->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function share($post_id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                $device_id = (int)$this->getDevice()->id;
                $this->addExtraLog($post_id, $device_id, self::LOG_SHARE);

                $postLogId = $this->getPostLog($post_id, $user_id);

                if (!is_null($postLogId)) {
                    $stmt = $this->getDb()->prepare(self::$sqlPostLogShare);
                    $stmt->bindParam(":id", $postLogId, \PDO::PARAM_INT);
                    $stmt->execute();
                } else {
                    $this->addPostLog($post_id, $user_id, 0, time(), 0);
                }

                $stmt = $this->getDb()->prepare(self::$sqlPostShare);
                $stmt->bindParam(":post_id", $post_id, \PDO::PARAM_INT);
                $stmt->execute();

                $response = $this->getJsonResponse(202);
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function parseTags($tags)
    {
        $t = str_replace("|", ",", $tags);
        $stmt = $this->getDb()->prepare(sprintf(self::$sqlTags, $t));
        $stmt->bindParam(":tags", $t, \PDO::PARAM_STMT);

        $result = array();

        if ($stmt->execute()) {
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            foreach ($rows as $row) {
                $result [] = $row->label;
            }
        }

        return $result;
    }

    public function getUserContentById($id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            list($section_id, $form_id, $row_id) = explode("_", $id);
            try {
                $userPageRestModel = new UserPageRestModel($this->controller);
                $sectionModel = new SectionModel($this->controller->getServiceLocator());
                $section = $sectionModel->getSectionByPk($section_id);
                $user_id = (int)$this->getDevice()->user_id;
                $dev_id = (int)$this->getDevice()->id;

                $stmt = $this->getDb()->prepare(sprintf(self::$sqlUserContentDetail, $form_id));
                $stmt->bindParam(":id", $row_id, \PDO::PARAM_INT);
                $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
                $stmt->bindParam(":form_id", $form_id, \PDO::PARAM_INT);
                $stmt->execute();

                $contents = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\ContentUserSerializer', array(
                        "model" => $userPageRestModel,
                        "mode" => self::MODE_DETAIL,
                        "section" => $section,
                        "customFormId" => $form_id,
                        "userId" => $user_id)
                );

                $page = new \stdClass();
                $header = new \stdClass();
                $body = new \stdClass();

                $header->demo = ((int)$this->getApp()->demo == 0) ? false : true;
                $header->white_label = ((int)$this->getApp()->white_label == 0) ? false : true;

                $body->sections = null;
                $body->contents = $contents[0];

                $page->type = self::TYPE_DETAIL;
                $page->header = $header;
                $page->body = $body;

                if (isset($user_id) && $user_id > 0) {
                    $this->updateUserContentViewCount($form_id, $row_id, $dev_id, $user_id);
                }

                $response = $this->getJsonResponse(200, $page);
            } catch (\PDOException $exception) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $exception->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function getContentById($id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $stmt = $this->getDb()->prepare(self::$sqlDetail);

                $user_id = (int)$this->getDevice()->user_id;
                $dev_id = (int)$this->getDevice()->id;

                $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
                $stmt->bindParam(":user_id1", $user_id, \PDO::PARAM_INT);
                $stmt->bindParam(":user_id2", $user_id, \PDO::PARAM_INT);

                $stmt->execute();

                $contents = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\ContentSerializer', array("model" => $this, "mode" => self::MODE_DETAIL));

                $page = new \stdClass();
                $header = new \stdClass();
                $body = new \stdClass();

                $header->demo = ((int)$this->getApp()->demo == 0) ? false : true;
                $header->white_label = ((int)$this->getApp()->white_label == 0) ? false : true;

                $body->sections = null;
                $body->contents = $contents[0];

                $page->type = self::TYPE_DETAIL;
                $page->header = $header;
                $page->body = $body;

                $this->updateViewCount($id, $dev_id, $user_id);

                $response = $this->getJsonResponse(200, $page);
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    public function getSerializedContentById($id, $user_id)
    {
        $result = null;

        if (!is_null($this->getDb())) {
            $stmt = $this->getDb()->prepare(self::$sqlDetail);

            $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
            $stmt->bindParam(":user_id1", $user_id, \PDO::PARAM_INT);
            $stmt->bindParam(":user_id2", $user_id, \PDO::PARAM_INT);

            $stmt->execute();

            $content = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\ContentSerializer', array("model" => $this, "mode" => self::MODE_MASTER));

            if (count($content) > 0) {
                return $content[0];
            }
        }

        return $result;
    }

    public function getActionsById($id)
    {
        $result = null;

        $stmt = $this->getDb()->prepare(self::$sqlActionsById);
        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $result = $stmt->fetchAll(\PDO::FETCH_OBJ);
        }

        return $result;
    }

    public function getContentByHashId($hashId)
    {
        if ($this->isValidRequest()) {
            $user_id = isset($this->getDevice()->user_id) ? $this->getDevice()->user_id : null;

            $stmt = $this->getDb()->prepare(self::$sqlPostById);
            $stmt->bindParam(":id", $hashId, \PDO::PARAM_STR);
            $stmt->bindParam(":user_id1", $user_id, \PDO::PARAM_INT);
            $stmt->bindParam(":user_id2", $user_id, \PDO::PARAM_INT);
            $stmt->execute();

            $content = $stmt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\Json\ContentSerializer', array("model" => $this, "mode" => self::MODE_MASTER));
            if (count($content) > 0) {
                $response = $this->getJsonResponse(200, $content[0]);
            } else {
                $response = $this->getJsonResponse(412, null, null, "Content not found");
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

}