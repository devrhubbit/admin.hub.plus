<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 20/07/16
 * Time: 12:18
 */

namespace Rest\Model;

use Database\HubPlus\RemoteLogQuery;
use Database\HubPlus\SectionQuery;
use Database\HubPlus\SectionRelatedTo;
use Database\HubPlus\SectionRelatedToQuery;
use Database\HubPlus\UserAppQuery;
use Propel\Runtime\Exception\PropelException;
use Rest\Model\Json\ContentSerializer;
use Rest\Model\Json\ContentUserSerializer;
use Rest\Model\Json\SectionSerializer;
use Rest\Model\Json\SectionToSectionSerializer;
use Zend\Http\Client;
use Zend\Stdlib\Parameters as GetParams;
use Rest\Exception\RestApiKeySecurityException;
use Rest\Exception\RestRemoteSectionFormatException;
use Rest\Exception\RestRemoteException;
use Rest\Exception\RestSignatureSecurityException;

class ExternalPageRestModel extends ContentPageRestModel
{

    const EXTERNAL_CONTENT = "POST";
    const EXTERNAL_MEDIA = "MEDIA";

    const TYPE_IMAGE = "IMAGE";
    const TYPE_YOUTUBE = "YOUTUBE";

    const REMOTE_CACHE = false;

    private static $sqlConnector = "SELECT sc.*, rp.*, s.type AS section_type, s.layout AS layout, s.resource AS section_resource
                                      FROM `section_connector` AS sc
                                      JOIN `remote_provider` AS rp ON sc.provider_id = rp.id
                                      JOIN `section` AS s ON sc.section_id = s.id
                                    WHERE
                                      s.deleted_at  IS NULL AND
                                      sc.deleted_at IS NULL AND
                                      rp.deleted_at IS NULL AND
                                      sc.id = :id;";

    private static $sqlExtraLog = "INSERT INTO `remote_extra_log` (`id`, `connector_id`, `remote_id`, `device_id`, `type`, `content_type`, `timestamp`)
                                        VALUES (NULL, :connector_id, :remote_id, :dev_id, :type, :content_type, NOW());";
    private static $sqlAddRemoteLog = "INSERT INTO `remote_log`
                                          (`id`, `connector_id`, `remote_id`, `user_id`, `content_type`, `liked_at`, `shared_at`, `share_count`, `view_count`, `like_count`)
                                        VALUES
                                          (NULL, :connector_id, :remote_id, :user_id, :content_type, :liked_at, :shared_at, :share_count, :view_count, :like_count);";

    private static $sqlRemoteLogView = "UPDATE remote_log SET view_count = view_count+1 WHERE id = :id;";
    private static $sqlRemoteLogShare = "UPDATE remote_log SET share_count = share_count+1, shared_at = NOW() WHERE id = :id;";
    private static $sqlRemoteLogLike = "UPDATE remote_log SET like_count = like_count+1, liked_at = NOW() WHERE id = :id;";
    private static $sqlRemoteLogUnlike = "UPDATE remote_log SET like_count = like_count-1, liked_at = NULL  WHERE id = :id;";

    private static $sqlGetSectionsConnectorByProviderId = "SELECT * FROM `section_connector` WHERE provider_id = :provider_id;";


    /**
     * @param $type
     * @param $connector_id
     * @param $remote_id
     * @return null|string
     * @throws RestApiKeySecurityException
     * @throws RestSignatureSecurityException
     */
    public function likeRemoteContentById($type, $connector_id, $remote_id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                if ($user_id != 0) {
                    $connector = $this->getConnector($connector_id);
                    $providerUUID = $connector->uuid;
                    $remoteUrl = $connector->baseurl;

                    $postFields = json_encode(array(
                        'remote_id' => $remote_id,
                        'favourite' => true
                    ));

                    $headers = array(
                        "Content-HMAC: " . base64_encode(hash_hmac("sha256", $postFields, $providerUUID)),
                        "Device-Lang: " . $this->getDeviceLang(),
                        "demo: " . $this->getDemoFlag(),
                        "Cache-Control: no-cache",
                        "Hp-Response: favourite-" . strtolower($type),
                        "Content-Type: application/json",
                        "Content-Length: " . strlen($postFields),
                    );

                    $user = UserAppQuery::create()
                        ->findPk($user_id);

                    if ($user) {
                        $headers[] = "User-ID: " . base64_encode(hash_hmac("sha256", $user->getUsernameCanonical(), $providerUUID));
                    }

                    $client = new Client();
                    $client->setMethod('POST');
                    $client->setOptions(array(
                        'maxredirects' => 5,
                        'timeout' => 30
                    ));
                    $client->setRawBody($postFields);
                    $client->setUri($remoteUrl);
                    $client->setHeaders($headers);
                    $response = $client->send();
                    $body = json_decode($response->getBody());

                    if ($response->isSuccess()) {
                        if (!is_null($body)) {
                            $sign = null;
                            $responseHeaders = $response->getHeaders();
                            foreach ($responseHeaders as $header) {
                                if ($header->getFieldName() === 'Content-HMAC') {
                                    $sign = base64_decode(trim($header->getFieldValue()));
                                }
                            }

                            if (!$this->isValidSignature($response->getBody(), $providerUUID, $sign)) {
                                throw new RestSignatureSecurityException();
                            } else {
                                $device_id = (int)$this->getDevice()->id;
                                $this->addRemoteExtraLog($type, $connector_id, $remote_id, $device_id, self::LOG_LIKE);

                                $remoteLogId = $this->getRemoteLog($connector_id, $remote_id, $user_id);

                                if (!is_null($remoteLogId)) {
                                    $stmt = $this->getDb()->prepare(self::$sqlRemoteLogLike);
                                    $stmt->bindParam(":id", $remoteLogId, \PDO::PARAM_INT);
                                    $stmt->execute();
                                } else {
                                    $this->addRemoteLog($type, $connector_id, $remote_id, $user_id, time(), 0, 0);
                                }

                                $response = $this->getJsonResponse(202);
                            }
                        }
                    }
                } else {
                    $response = $this->getJsonResponse(412);
                }
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    //NB: come caso limite potrebbe capitare che se un utente ha fatto uno share di un content e poi il sistema gli permette
    //    di fare UNLIKE senza che abbia prima fatto LIKE il contatore dei like vada a negativo
    /**
     * @param $type
     * @param $connector_id
     * @param $remote_id
     * @return null|string
     * @throws RestApiKeySecurityException
     * @throws RestSignatureSecurityException
     */
    public function unlikeRemoteContentById($type, $connector_id, $remote_id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                if ($user_id != 0) {
                    $connector = $this->getConnector($connector_id);
                    $providerUUID = $connector->uuid;
                    $remoteUrl = $connector->baseurl;

                    $postFields = json_encode(array(
                        'remote_id' => $remote_id,
                        'favourite' => false
                    ));

                    $headers = array(
                        "Content-HMAC: " . base64_encode(hash_hmac("sha256", $postFields, $providerUUID)),
                        "Device-Lang: " . $this->getDeviceLang(),
                        "demo: " . $this->getDemoFlag(),
                        "Cache-Control: no-cache",
                        "Hp-Response: favourite-" . strtolower($type),
                        "Content-Type: application/json",
                        "Content-Length: " . strlen($postFields),
                    );

                    $user = UserAppQuery::create()
                        ->findPk($user_id);

                    if ($user) {
                        $headers[] = "User-ID: " . base64_encode(hash_hmac("sha256", $user->getUsernameCanonical(), $providerUUID));
                    }

                    $client = new Client();
                    $client->setMethod('POST');
                    $client->setOptions(array(
                        'maxredirects' => 5,
                        'timeout' => 30
                    ));
                    $client->setRawBody($postFields);
                    $client->setUri($remoteUrl);
                    $client->setHeaders($headers);
                    $response = $client->send();
                    $body = json_decode($response->getBody());

                    if ($response->isSuccess()) {
                        if (!is_null($body)) {
                            $sign = null;
                            $responseHeaders = $response->getHeaders();
                            foreach ($responseHeaders as $header) {
                                if ($header->getFieldName() === 'Content-HMAC') {
                                    $sign = base64_decode(trim($header->getFieldValue()));
                                }
                            }

                            if (!$this->isValidSignature($response->getBody(), $providerUUID, $sign)) {
                                throw new RestSignatureSecurityException();
                            } else {
                                $device_id = (int)$this->getDevice()->id;

                                $remoteLogId = $this->getRemoteLog($connector_id, $remote_id, $user_id);

                                if (!is_null($remoteLogId)) {
                                    $this->addRemoteExtraLog($type, $connector_id, $remote_id, $device_id, self::LOG_UNLIKE);

                                    $stmt = $this->getDb()->prepare(self::$sqlRemoteLogUnlike);
                                    $stmt->bindParam(":id", $remoteLogId, \PDO::PARAM_INT);
                                    $stmt->execute();

                                    $response = $this->getJsonResponse(202);
                                } else {
                                    $response = $this->getJsonResponse(406);
                                }
                            }
                        }
                    }
                } else {
                    $response = $this->getJsonResponse(412);
                }
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    /**
     * @param $type
     * @param $connector_id
     * @param $remote_id
     * @return null|string
     * @throws RestApiKeySecurityException
     */
    public function shareRemoteContentById($type, $connector_id, $remote_id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            try {
                $user_id = (int)$this->getDevice()->user_id;
                $device_id = (int)$this->getDevice()->id;

                $this->addRemoteExtraLog($type, $connector_id, $remote_id, $device_id, self::LOG_SHARE);
                $remoteLogId = $this->getRemoteLog($connector_id, $remote_id, $user_id);

                if (!is_null($remoteLogId)) {
                    $stmt = $this->getDb()->prepare(self::$sqlRemoteLogShare);
                    $stmt->bindParam(":id", $remoteLogId, \PDO::PARAM_INT);
                    $stmt->execute();
                } else {
                    $this->addRemoteLog($type, $connector_id, $remote_id, $user_id, 0, time(), 0);
                }

                $response = $this->getJsonResponse(202);
            } catch (\PDOException $ex) {
                if ($this->isDebug()) {
                    $response = $this->getJsonResponse(500, $ex->getMessage());
                } else {
                    $response = $this->getJsonResponse(500);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    /**
     * @param $connector_id
     * @param $remote_id
     * @return null|string|\Zend\Http\Response
     * @throws RestApiKeySecurityException
     * @throws RestSignatureSecurityException
     */
    public function deleteRemoteContentById($connector_id, $remote_id)
    {
        $response = null;

        if ($this->isValidRequest()) {
            $connector = $this->getConnector($connector_id);
            $providerUUID = $connector->uuid;
            $remoteUrl = $connector->baseurl .
                (parse_url($connector->baseurl, PHP_URL_QUERY) ? "&" : "?") . "media_id=" . $remote_id;

            $headers = array(
                "Content-HMAC: " . base64_encode(hash_hmac("sha256", $remoteUrl, $providerUUID)),
                "Device-Lang: " . $this->getDeviceLang(),
                "demo: " . $this->getDemoFlag(),
                "Cache-Control: no-cache",
                "Hp-Response: media",
            );

            $client = new Client();
            $client->setMethod('DELETE');
            $client->setOptions(array(
                'maxredirects' => 5,
                'timeout' => 30
            ));
            $client->setUri($remoteUrl);
            $client->setHeaders($headers);
            $response = $client->send();
            $body = json_decode($response->getBody());

            if ($response->isSuccess()) {
                if (!is_null($body)) {
                    $sign = null;
                    $responseHeaders = $response->getHeaders();
                    foreach ($responseHeaders as $header) {
                        if ($header->getFieldName() === 'Content-HMAC') {
                            $sign = base64_decode(trim($header->getFieldValue()));
                        }
                    }

                    if (!$this->isValidSignature($response->getBody(), $providerUUID, $sign)) {
                        throw new RestSignatureSecurityException();
                    } else {
                        $response = $this->getJsonResponse(200, $body);
                    }
                }
            } else {
                if (isset($body->success) && $body->success === false) {
                    $response = $this->getJsonResponse(isset($body->status) ? $body->status : 500, null, null, isset($body->message) ? $body->message : null);
                }
            }
        } else {
            throw new RestApiKeySecurityException();
        }

        return $response;
    }

    /**
     * @param $type
     * @param $connector_id
     * @param $remote_id
     * @param GetParams $getParams
     * @return null|string
     * @throws RestApiKeySecurityException
     * @throws RestRemoteSectionFormatException
     */
    public function getRemoteContentById($type, $connector_id, $remote_id, GetParams $getParams)
    {
        $response = null;
        if ($connector_id != null) {
            if ($this->isValidRequest()) {
                $user_id = (int)$this->getDevice()->user_id;

                $connector = $this->getConnector($connector_id);
                $providerUUID = $connector->uuid;
                $detailUrl = $resource = $this->getBaseurl() . "/api/" . $this->getApp()->api_version . "/page/external/connector/$connector_id/detail/";
                $remoteUrl = $connector->baseurl .
                    (parse_url($connector->baseurl, PHP_URL_QUERY) ? "&" : "?") . "id=" . $remote_id;

                if ($getParams->toString() !== "") {
                    $remoteUrl .= "&" . $getParams->toString();
                }

                $headers = array();
                if (!self::REMOTE_CACHE) {
                    $remoteUrl .= "&cache=" . time();
                    $headers[] = "Cache-Control: no-cache";
                }

                $headers[] = "Content-HMAC: " . base64_encode(hash_hmac("sha256", $remoteUrl, $providerUUID));
                $headers[] = "Device-Lang: " . $this->getDeviceLang();
                $headers[] = "demo: " . $this->getDemoFlag();
                $headers[] = "Hp-Response: detail";

                $user = UserAppQuery::create()
                    ->findPk($user_id);

                if ($user) {
                    $headers[] = "User-ID: " . base64_encode(hash_hmac("sha256", $user->getUsernameCanonical(), $providerUUID));
                }

                try {
                    $remoteData = json_decode($this->connectorRequest($remoteUrl, $providerUUID, $headers));

                    $logData = $this->getRemoteLogData($connector_id, $remote_id, $user_id);
                    $placeholderImage = $this->getBaseurl() . "/img/placeholder/app-icon.png";

                    $page = new \stdClass();
                    $header = new \stdClass();
                    $body = new \stdClass();
                    $content = new \stdClass();

                    $header->demo = ((int)$this->getApp()->demo == 0) ? false : true;
                    $header->white_label = ((int)$this->getApp()->white_label == 0) ? false : true;

                    $content->mode = ContentPageRestModel::MODE_DETAIL;
                    $content->type = $connector->section_type;
                    $content->resource_like = $detailUrl . $remote_id . "/like/";
                    $content->resource_share = $detailUrl . $remote_id . "/share/";
                    $content->cover = array(
                        "context" => "placeholder",
                        "type" => "IMAGE",
                        "mime_type" => "image/png",
                        "extension" => "png",
                        "size" => 0,
                        "title" => "cover",
                        "subtitle" => "cover",
                        "thumb" => $remoteData->cover !== "" ? $remoteData->cover : $placeholderImage,
                        "resource" => $remoteData->cover !== "" ? $remoteData->cover : $placeholderImage,
                        "resource_shareable" => $remoteData->cover !== "" ? $remoteData->cover : $placeholderImage,
                        "resource_like" => null,
                        "resource_share" => null,
                        "resource_delete" => null,
                        "like_count" => 0,
                        "liked_at" => null
                    );
                    $content->title = $remoteData->title !== null ? $remoteData->title : "";
                    $content->subtitle = $remoteData->subtitle !== null ? $remoteData->subtitle : "";
                    $content->description = $remoteData->description !== null ? $remoteData->description : "";
                    $content->like_count = ($logData) ? (int)$logData->like_count : 0;
                    $content->liked_at = ($logData) ? $logData->liked_at : null;
                    $content->gallery = $this->wrapGallery($connector_id, $remoteData->gallery);
                    $content->actions = array();
                    $content->body = $remoteData->body !== null ? $remoteData->body : "";
                    $content->row_data = $remoteData->data;

                    $mode = strtoupper($getParams->get("mode"));
                    if ($mode !== null && $connector->section_type . "_" . $mode === ContentSerializer::POI_CONTENT_OUTDOOR) {
                        $content->type = $connector->section_type . "_" . $mode;
                    }
                    if ($content->type === SectionToSectionSerializer::TYPE_USER || $content->type === SectionToSectionSerializer::TYPE_FAVOURITE) {
                        $content->type = ContentUserSerializer::GENERIC_CONTENT;
                    }

                    $body->sections = null;
                    $body->contents = $content;

                    $page->type = self::TYPE_DETAIL;
                    $page->header = $header;
                    $page->body = $body;

                    $this->updateRemoteViewCount($type, $connector_id, $remote_id);

                    $response = $this->getJsonResponse(200, $page);
                } catch (RestRemoteException $exception) {
                    $response = $this->getJsonResponse($exception->getRemoteStatusCode(), $exception->getMessage());
                } catch (RestSignatureSecurityException $exception) {
                    $response = $this->getJsonResponse(500, $exception->getMessage());
                }
            } else {
                throw new RestApiKeySecurityException();
            }
        } else {
            throw new RestRemoteSectionFormatException();
        }

        return $response;
    }

    /**
     * @param $connector_id
     * @param GetParams $getParams
     * @return null|string
     * @throws RestApiKeySecurityException
     * @throws RestRemoteSectionFormatException
     */
    public function getList($connector_id, GetParams $getParams)
    {
        $response = null;

        if ($connector_id != null) {
            if ($this->isValidRequest()) {
                $user_id = (int)$this->getDevice()->user_id;

                $connector = $this->getConnector($connector_id);
                $providerUUID = $connector->uuid;
                $remoteSectionId = $connector->remote_section_id;

                $wsBaseUrl = $this->getBaseurl() . "/api/" . $this->getApp()->api_version;

                $detailUrl = $wsBaseUrl . "/page/external/connector/$connector_id/detail/";
                $mediaBaseUrl = $wsBaseUrl . "/media/upload/form/";

                $remoteUrl = $connector->baseurl;

                if ($getParams->toString() !== "") {
                    $remoteUrl = $remoteUrl . (parse_url($remoteUrl, PHP_URL_QUERY) ? "&" : "?") . $getParams->toString();
                }

                $headers = array();
                if (!self::REMOTE_CACHE) {
                    $remoteUrl = $remoteUrl . (parse_url($remoteUrl, PHP_URL_QUERY) ? "&" : "?") . "cache=" . time();
                    $headers[] = "Cache-Control: no-cache";
                }

                $headers[] = "Content-HMAC: " . base64_encode(hash_hmac("sha256", $remoteUrl, $providerUUID));
                $headers[] = "Device-Lang: " . $this->getDeviceLang();
                $headers[] = "demo: " . $this->getDemoFlag();
                $headers[] = "Hp-Response: list";

                $user = UserAppQuery::create()
                    ->findPk($user_id);

                $hubPlusUserId = "";
                if ($user) {
                    $hubPlusUserId = base64_encode(hash_hmac("sha256", $user->getUsernameCanonical(), $providerUUID));
                    $headers[] = "User-ID: " . $hubPlusUserId;
                }

                if ($connector->section_type === SectionToSectionSerializer::TYPE_USER) {
                    $sectionId = $connector->section_id;
                    $section = SectionQuery::create()->findPk($sectionId);
                    $sectionParams = json_decode($section->getParams());
                    if (isset($sectionParams->content_filter)) {
                        $headers[] = "User-Filter: " . $sectionParams->content_filter;
                    }
                }

                $layout = json_decode($connector->layout);

                try {
                    $remoteData = json_decode($this->connectorRequest($remoteUrl, $providerUUID, $headers));

                    $placeholderImage = $this->getBaseurl() . "/img/placeholder/app-icon.png";
                    $contents = array();

                    $mode = strtoupper($getParams->get("mode"));

                    foreach ($remoteData->contents as $content) {
                        $cnt = new \stdClass();
                        $remote_id = $content->content_id;
                        $logData = $this->getRemoteLogData($connector_id, $remote_id, $user_id);
                        $layout->name = $content->layout;

                        $cnt->mode = ContentPageRestModel::MODE_MASTER;
                        $cnt->type = $connector->section_type;
                        if ($cnt->type === SectionToSectionSerializer::TYPE_USER || $cnt->type === SectionToSectionSerializer::TYPE_FAVOURITE) {
                            $cnt->type = ContentUserSerializer::GENERIC_CONTENT;
                        }

                        if (isset($content->author_id) && $content->author_id === $hubPlusUserId) {
                            $sectionId = $connector->section_id;
                            $relatedSectionTo = SectionRelatedToQuery::create()
                                ->findOneByFromSectionId($sectionId);

                            $relatedSection = SectionQuery::create()->findPk($relatedSectionTo->getToSectionId());

                            $cnt->type = SectionToSectionSerializer::TYPE_USER_SECTION;
                            $cnt->resource = SectionSerializer::getHashId($relatedSection->getId(), $relatedSection->getType());
                            $cnt->resource_cover = $mediaBaseUrl . $relatedSection->getId() . "/cover/";
                            $cnt->resource_gallery = $mediaBaseUrl . $relatedSection->getId() . "/gallery/";
                            $cnt->row_id = $remote_id;
                        } else {
                            if ($remoteSectionId === null) {
                                $cnt->resource = $detailUrl . $remote_id;
                            } else {
                                $childSection = SectionQuery::create()
                                    ->findPk($remoteSectionId);
                                $cnt->resource = SectionSerializer::getHashId($remoteSectionId, $childSection->getType());
                            }
                        }

                        $cnt->resource_like = $detailUrl . $remote_id . "/like/";
                        $cnt->resource_share = $detailUrl . $remote_id . "/share/";

                        $cnt->cover = array(
                            "context" => "placeholder",
                            "type" => "IMAGE",
                            "mime_type" => "image/png",
                            "extension" => "png",
                            "size" => 0,
                            "title" => "cover",
                            "subtitle" => "cover",
                            "thumb" => $content->cover !== "" ? $content->cover : $placeholderImage,
                            "resource" => $content->cover !== "" ? $content->cover : $placeholderImage,
                            "resource_shareable" => $content->cover !== "" ? $content->cover : $placeholderImage,
                            "resource_like" => null,
                            "resource_share" => null,
                            "resource_delete" => null,
                            "like_count" => 0,
                            "liked_at" => null
                        );
                        $cnt->title = $content->title !== null ? $content->title : "";
                        $cnt->subtitle = $content->subtitle !== null ? $content->subtitle : "";
                        $cnt->description = $content->description !== null ? $content->description : "";
                        $cnt->like_count = ($logData) ? (int)$logData->like_count : 0;
                        $cnt->liked_at = ($logData) ? $logData->liked_at : null;
                        $cnt->layout = $layout;

                        $cnt->distance = $content->distance;
                        if ($mode !== null && $connector->section_type . "_" . $mode === ContentSerializer::POI_CONTENT_OUTDOOR) {
                            $cnt->type = $connector->section_type . "_" . $mode;
                            $cnt->resource = $detailUrl . $remote_id . "?mode=$mode";
                            $cnt->lat = isset($content->lat) ? $content->lat : null;
                            $cnt->lng = isset($content->lng) ? $content->lng : null;
                        }

                        $contents [] = $cnt;
                    }

                    $page = new \stdClass();
                    $header = new \stdClass();
                    $body = new \stdClass();

                    $header->demo = ((int)$this->getApp()->demo == 0) ? false : true;
                    $header->white_label = ((int)$this->getApp()->white_label == 0) ? false : true;
                    $header->rows = $remoteData->rows;
                    $header->total_rows = $remoteData->total_rows;

                    $body->sections = null;
                    $body->contents = $contents; //$remoteList->contents;

                    $page->type = self::TYPE_LIST;
                    $page->header = $header;
                    $page->body = $body;

                    $response = $this->getJsonResponse(200, $page);
                } catch (RestRemoteException $exception) {
                    $response = $this->getJsonResponse($exception->getRemoteStatusCode(), $exception->getMessage());
                } catch (RestSignatureSecurityException $exception) {
                    $response = $this->getJsonResponse(500, $exception->getMessage());
                }
            } else {
                throw new RestApiKeySecurityException();
            }
        } else {
            throw new RestRemoteSectionFormatException();
        }

        return $response;
    }

    public function getSectionsList($md5Uuid)
    {
        $response = null;
        $hmac = $this->getHmac();

        if ($hmac !== null && $md5Uuid !== null) {
            $remoteProvider = $this->getRemoteProviderFromMd5Uuid($md5Uuid);

            $requestKey = base64_decode($hmac);
            $checkKey = hash_hmac("sha256", $md5Uuid, $remoteProvider->uuid);

            if ($requestKey === $checkKey) {
                $sectionsId = $this->getSectionsIdByProviderId($remoteProvider->id);

                $sectionModel = new SectionPageRestModel($this->getController());
                $sections = array();
                foreach ($sectionsId as $sectionId) {
                    $section = $sectionModel->getSectionById($sectionId);

                    $sections[] = array(
                        'section_id' => $section->id,
                        'label' => $section->title,
                    );
                }

                $response = $this->getJsonResponse(200, $sections);
            } else {
                $response = $this->getJsonResponse(401);
            }
        } else {
            $response = $this->getJsonResponse(412);
        }
        return $response;
    }

    private function getSectionsIdByProviderId($providerId)
    {
        $res = null;

        $stmt = $this->getDb()->prepare(self::$sqlGetSectionsConnectorByProviderId);
        $stmt->bindParam(":provider_id", $providerId, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            $res = array();
            foreach ($rows as $row) {
                $res[] = $row->section_id;
            }
        }

        return $res;
    }

    private function updateRemoteViewCount($type, $connector_id, $remote_id)
    {
        $user_id = (int)$this->getDevice()->user_id;
        $device_id = (int)$this->getDevice()->id;
        $this->addRemoteExtraLog($type, $connector_id, $remote_id, $device_id, self::LOG_VIEW);

        if ($user_id != 0) {
            $remoteLogId = $this->getRemoteLog($connector_id, $remote_id, $user_id);

            if (!is_null($remoteLogId)) {
                $stmt = $this->getDb()->prepare(self::$sqlRemoteLogView);
                $stmt->bindParam(":id", $remoteLogId, \PDO::PARAM_INT);
                $stmt->execute();
            } else {
                $this->addRemoteLog($type, $connector_id, $remote_id, $user_id, 0, 0, 1);
            }
        }
    }

    private function addRemoteExtraLog($content_type, $connector_id, $remote_id, $device_id, $type)
    {
        $stmt = $this->getDb()->prepare(self::$sqlExtraLog);

        $stmt->bindParam(":content_type", $content_type, \PDO::PARAM_STR);
        $stmt->bindParam(":connector_id", $connector_id, \PDO::PARAM_INT);
        $stmt->bindParam(":remote_id", $remote_id, \PDO::PARAM_STR);
        $stmt->bindParam(":dev_id", $device_id, \PDO::PARAM_INT);
        $stmt->bindParam(":type", $type, \PDO::PARAM_STR);

        $stmt->execute();
    }

    private function getRemoteLog($connector_id, $remote_id, $user_id)
    {
        $user_id = $user_id === 0 ? null : $user_id;

        $remoteLog = RemoteLogQuery::create()
            ->filterByConnectorId($connector_id)
            ->filterByRemoteId($remote_id)
            ->filterByUserId($user_id)
            ->findOne();

        $remoteLogId = null;
        if ($remoteLog) {
            $remoteLogId = $remoteLog->getId();
        }

        return $remoteLogId;
    }

    private function addRemoteLog($content_type, $connector_id, $remote_id, $user_id, $liked_at = 0, $shared_at = 0, $view_count = 0)
    {
        $stmt = $this->getDb()->prepare(self::$sqlAddRemoteLog);

        $like_count = 0;
        $share_count = 0;
        $like_date = null;
        $share_date = null;

        if ($liked_at > 0) {
            $like_count++;
            $like_date = date('Y-m-d G:i:s', $liked_at);
        }

        if ($shared_at > 0) {
            $share_count++;
            $share_date = date('Y-m-d G:i:s', $shared_at);
        }

        $user_id = $user_id === 0 ? null : $user_id;

        $stmt->bindParam(":content_type", $content_type, \PDO::PARAM_STR);
        $stmt->bindParam(":connector_id", $connector_id, \PDO::PARAM_INT);
        $stmt->bindParam(":remote_id", $remote_id, \PDO::PARAM_INT);
        $stmt->bindParam(":user_id", $user_id, \PDO::PARAM_INT);
        $stmt->bindParam(":liked_at", $like_date, \PDO::PARAM_STR);
        $stmt->bindParam(":shared_at", $share_date, \PDO::PARAM_STR);
        $stmt->bindParam(":like_count", $like_count, \PDO::PARAM_INT);
        $stmt->bindParam(":share_count", $share_count, \PDO::PARAM_INT);
        $stmt->bindParam(":view_count", $view_count, \PDO::PARAM_INT);

        $stmt->execute();
    }

    private function getConnector($connector_id)
    {
        $stmt = $this->getDb()->prepare(self::$sqlConnector);

        $stmt->bindParam(":id", $connector_id, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchObject();
    }

    private function isValidSignature($public_key, $secret_key, $sign)
    {
        return (hash_hmac("sha256", $public_key, $secret_key) == $sign);
    }

    /**
     * @param $url
     * @param null $secret_key
     * @param array $headers
     * @return bool|string
     * @throws RestRemoteException
     * @throws RestSignatureSecurityException
     */
    private function connectorRequest($url, $secret_key = null, $headers = array())
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HEADER => 1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $info = curl_getinfo($curl);

        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $responseHeaders = substr($response, 0, $header_size);
        $content = substr($response, $header_size);

        curl_close($curl);

        if ($err || $info['http_code'] >= 400) {
            //echo "cURL Error #:" . $err;
            $remoteException = new RestRemoteException();
            $remoteException->setRemoteStatusCode($info['http_code']);
            throw $remoteException;
        } else {
            $sign = null;
            $responseHeaders = explode("\n", $responseHeaders);
            foreach ($responseHeaders as $header) {
                if (stripos($header, 'Content-HMAC: ') !== false) {
                    $sign = base64_decode(trim(explode(":", $header)[1]));
                }
            }
            if (!$this->isValidSignature($content, $secret_key, $sign)) {
                throw new RestSignatureSecurityException();
            } else {
                $data = $content;
            }
        }

        return $data;
    }
}