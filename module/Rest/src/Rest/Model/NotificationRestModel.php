<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 20/07/16
 * Time: 12:14
 */

namespace Rest\Model;

use Application\Model\NotificationModel;
use MessagesService\Model\MessagesModel,
    MessagesService\Model\MessageModel as Message,
    MessagesService\Service\Factory as MessagesFactory,
    MessagesService\Exception\MessagesServicePushException;
use Form\Notification\NotificationForm;
use Rest\Exception\RestAppIdSecurityException;
use Common\Helper\ImageHelper;

class NotificationRestModel extends HpRestModel
{
    /**
     * @param $data
     * @param null $md5Uuid
     * @param null $lang
     * @return null|string
     * @throws RestAppIdSecurityException
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function sendPushNotification($data, $md5Uuid = null, $lang = null)
    {
        $response = null;

        if ($this->getHeaderAppId() !== null) {
            $hmac = $this->getHmac();
            if ($hmac !== null) {
                $remoteProvider = $this->getRemoteProviderFromMd5Uuid($md5Uuid);

                if ($remoteProvider !== null) {
                    $requestKey = base64_decode($hmac);
                    $checkKey = hash_hmac("sha256", json_encode($data), $remoteProvider->uuid);

//                    var_dump(base64_encode($checkKey));

                    if ($requestKey === $checkKey) {
                        $application = $this->getApp();
                        $lang = isset($lang) ? $lang : $application->main_contents_language;

                        $deviceList = $this->getDeviceToNotify($data->to, $lang);
                        $deviceCount = count($deviceList);
                        $errorCount = array();
                        $sendDate = date("Y-m-d H:i:s", $data->send_date);

                        if ($deviceCount > 0) {
                            $notificationBadget = $this->isNotificationEnabled($deviceCount);
                            if ($notificationBadget) {
                                $notificationModel = new NotificationModel($this->getController());


                                $pushNotificationParams = json_decode($application->push_notification);

                                $notificationId = $notificationModel->addNotifyToDB($data->body, $sendDate, $deviceCount, $lang, $data->title, $data->to, $data->section_id);

                                $trackingUrl = $this->getBaseurl() . $this->getController()->url()->fromRoute("message-read/put",
                                        array(
                                            "type" => MessagesFactory::MODE_PUSH,
                                            "hashId" => MessagesModel::HASH_ID_PLACEHOLDER,
                                        )
                                    );

                                $messages = array();
                                $tokenDeviceId = array();

                                $contentData = $this->getData($data->section_id);
                                $contentOldData = $this->getData($data->section_id, true);
                                if ($contentData !== null) {
                                    // Pezza per ridurre la dimensione del JSON nei limiti dei 2KB di Apple
                                    $contentData->tags = array();
                                    $contentData->actions = array();
                                }

                                if ($contentData !== null) {
                                    $contentData->tags = array();
                                }

                                foreach ($deviceList as $device) {
                                    $content = $contentOldData;
                                    $action = NotificationForm::OPEN_SECTION;

                                    if (isset($device->api_version) &&
                                        ucfirst(strtolower($device->api_version)) !== DeviceRestModel::API_VERSION_UNKNOWN &&
                                        version_compare($device->api_version, "1.2.0", '>=')
                                    ) {
                                        $content = $contentData;
                                        $action = NotificationForm::OPEN_CONTENT;
                                    }

                                    $message = new Message($data->body);
                                    $message->setOption('subject', $data->title);
                                    $message->setOption('title', $data->title);
                                    $message->setOption('trackingUrl', $trackingUrl);
                                    $message->setOption('sendDate', $sendDate);

                                    $os = strtolower($device->os);

                                    $pushParams = array();
                                    switch ($os) {
                                        case "ios":
                                            $pushParams = array(
                                                "certificate" => $pushNotificationParams->ios->certificate,
                                                "passPhrase" => $pushNotificationParams->ios->pass_phrase,
//                                    "badge" => $pushNotificationCounter,
                                            );
                                            break;
                                        case "android":
                                            $pushParams = array(
                                                "apiKey" => $pushNotificationParams->android->api_key,
                                            );
                                            break;
                                    }

                                    $systemIcon = json_decode($this->getApp()->system_icons);
                                    $resourceIcon = null;
                                    if ($systemIcon !== null) {
                                        $resourceIcon = $systemIcon->app_in_app_notification_icon;
                                    }

                                    $pushParams["customData"] = array(
                                        "type" => $data->section_id > 0 ? $action : NotificationForm::OPEN_MAIN,
                                        "title" => $data->title !== "" ? $data->title : null,
                                        "resource_icon" => ($resourceIcon !== null && $resourceIcon !== "") ? $this->getCdnBaseurl() . ImageHelper::getAppSystemIconUrl() . $resourceIcon : null,
                                        "data" => $content,
                                    );

                                    $pushParams["callback"] = $this->getRemoveDeviceUrl($device->token);

                                    $message->setOption('jsonParams', json_encode($pushParams));
                                    $message->setOption('to', $device->token);
                                    $message->setOption('device', $os);

                                    $messages[] = $message;
                                    $tokenDeviceId[$device->token] = $device->id;
                                }

                                $msgModel = new MessagesModel($this->getController()->getServiceLocator(), MessagesFactory::MODE_PUSH);
                                try {
                                    $hashIds = $msgModel->pullMessages($messages);

                                    $notificationModel->setNotifyRead($notificationId, $hashIds, $tokenDeviceId);

                                    $notificationResponse = array(
                                        'device_count_total' => $deviceCount,
                                        'device_count_error' => $deviceCount - count($hashIds),
                                        'send_date' => $sendDate,
                                    );

                                    $msg = count($hashIds) . "/$deviceCount users of your app will be notified at " . $sendDate . ".";
                                    $response = $this->getJsonResponse(200, $notificationResponse, null, $msg);
                                } catch (MessagesServicePushException $exception) {
                                    $response = $this->getJsonResponse(500, null, null, $exception->getMessage());
                                }
                            } else {
                                $response = $this->getJsonResponse(402, null, null, "You do not have enough credit to send this notifications!");
                            }
                        } else {
                            $response = $this->getJsonResponse(200, null, null, "No users to notify.");
                        }


                    } else {
                        $response = $this->getJsonResponse(401, null, null, "HMAC error");
                    }
                } else {
                    $response = $this->getJsonResponse(401, null, null, "No remote provider found");
                }
            } else {
                $response = $this->getJsonResponse(401, null, null, "Missing HMAC");
            }
        } else {
            throw new RestAppIdSecurityException();
        }
        return $response;
    }

    public function getData($section_id, $old = false)
    {
        $res = null;

        if ($section_id > 0) {
            $sectionModel = new SectionPageRestModel($this->getController());
            if ($old === true) {
                $res = $sectionModel->getSerializedSectionById($section_id);
            } else {
                $res = $sectionModel->getSerializedSectionToSectionById($section_id);
            }
        }

        return $res;
    }
}