<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 13:02
 */

namespace Rest\Exception;


class RestRemoteException extends RestFormatException
{
    protected $remoteStatusCode;
    protected $customMessage = "";

    public function __constructor()
    {
        $code = RestFormatException::INVALID_REMOTE_URL;
        $msg = RestFormatException::MESSAGE_REMOTE_URL;

        parent::__construct($msg, $code);
    }

    public function getErrorMessage()
    {
        return RestFormatException::MESSAGE_REMOTE_URL;
    }

    public function setCustomMessage($msg)
    {
        $this->customMessage = $msg;
    }

    public function getCustomMessage()
    {
        return $this->customMessage !== "" ? $this->customMessage : $this->getMessage();
    }

    public function setRemoteStatusCode($statusCode)
    {
        $this->remoteStatusCode = $statusCode;
    }

    public function getRemoteStatusCode()
    {
        return $this->remoteStatusCode;
    }

}