<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 13:02
 */

namespace Rest\Exception;


class RestApiKeySecurityException extends RestSecurityException
{
    function __constructor() {
        $code = RestSecurityException::INVALID_API_KEY;
        $msg  = RestSecurityException::MESSAGE_API_KEY;

        parent::__construct($msg, $code);
    }

    public function getErrorMessage() {
        return RestSecurityException::MESSAGE_API_KEY;
    }
}