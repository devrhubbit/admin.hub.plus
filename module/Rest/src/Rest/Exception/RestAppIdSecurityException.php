<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 13:02
 */

namespace Rest\Exception;


class RestAppIdSecurityException extends RestSecurityException
{
    function __constructor() {
        $code = RestSecurityException::INVALID_APP_ID;
        $msg  = RestSecurityException::MESSAGE_APP_ID;

        parent::__construct($msg, $code);
    }

    public function getErrorMessage() {
        return RestSecurityException::MESSAGE_APP_ID;
    }
}