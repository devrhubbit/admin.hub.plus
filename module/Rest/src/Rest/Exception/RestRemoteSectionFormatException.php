<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 13:02
 */

namespace Rest\Exception;


class RestRemoteSectionFormatException extends RestFormatException
{
    public function __constructor() {
        $code = RestFormatException::INVALID_CONNECTOR_ID;
        $msg  = RestFormatException::MESSAGE_CONNECTOR_ID;

        parent::__construct($msg, $code);
    }

    public function getErrorMessage() {
        return RestFormatException::MESSAGE_CONNECTOR_ID;
    }
}