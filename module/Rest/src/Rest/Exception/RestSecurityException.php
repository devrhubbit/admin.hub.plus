<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 13:02
 */

namespace Rest\Exception;


class RestSecurityException extends \Exception
{
    const INVALID_APP_ID  = 1000;
    const INVALID_API_KEY = 1001;
    const INVALID_PROVIDER_UUID = 1002;
    const INVALID_HASH_ID = 1003;


    const INVALID_SIGN    = 100;

    const MESSAGE_APP_ID  = "Invalid or null App-Id header param";
    const MESSAGE_API_KEY = "Invalid or null Api-Key header param";
    const MESSAGE_PROVIDER_UUID = "Invalid or null Provider-Uuid param";
    const MESSAGE_HASH_ID = "Invalid Hash ID param";
    const MESSAGE_SIGN    = "Invalid signature";
}