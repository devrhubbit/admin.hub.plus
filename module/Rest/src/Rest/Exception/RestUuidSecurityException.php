<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 13:02
 */

namespace Rest\Exception;


class RestUuidSecurityException extends RestSecurityException
{
    function __constructor() {
        $code = RestSecurityException::INVALID_PROVIDER_UUID;
        $msg  = RestSecurityException::MESSAGE_PROVIDER_UUID;

        parent::__construct($msg, $code);
    }

    public function getErrorMessage() {
        return RestSecurityException::MESSAGE_PROVIDER_UUID;
    }
}