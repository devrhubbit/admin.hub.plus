<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 13:02
 */

namespace Rest\Exception;


class RestFormatException extends \Exception implements IException
{
    const INVALID_CONNECTOR_ID = 2000;
    const INVALID_REMOTE_URL   = 3001;

    const MESSAGE_CONNECTOR_ID = "Invalid or null connector id in query string";
    const MESSAGE_REMOTE_URL   = "The remote connector's url is invalid or can't be access";

//    function __constructor($message = null, $code = 0) {
//        echo "XXXX";
//        $code = RestFormatException::INVALID_CONNECTOR_ID;
//        $msg  = RestFormatException::MESSAGE_CONNECTOR_ID;
//
//        parent::__construct($msg, $code);
//    }
}