<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 19/07/16
 * Time: 13:02
 */

namespace Rest\Exception;


class RestSignatureSecurityException extends RestSecurityException
{
    function __constructor() {
        $code = RestSecurityException::INVALID_SIGN;
        $msg  = RestSecurityException::MESSAGE_SIGN;

        parent::__construct($msg, $code);
    }

    public function getErrorMessage() {
        return RestSecurityException::MESSAGE_SIGN;
    }
}