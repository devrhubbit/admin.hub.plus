<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Application;
use Zend\Mvc\Router\RouteMatch;
use Zend\Session\Config\SessionConfig;
use Zend\Session\SessionManager;
use Zend\Session\Container;
use Zend\Stdlib\ArrayUtils;
use Zend\Config\Factory as ZendConfigFactory;
use Common\Helper\AclHelper;
use Zend\ModuleManager\ModuleManager;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $this->initSession(array(
            'remember_me_seconds' => 180,
            'use_cookies' => true,
            'cookie_httponly' => true,
        ));

        /** @var \Zend\ServiceManager\ServiceManager $serviceManager */
        $serviceManager = $e->getApplication()->getServiceManager();
        /* @var $viewModel \Zend\View\Model\ViewModel */
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();

        $config = $serviceManager->get('config');
        $viewModel->cdnBaseurl = $config['cdn']['baseurl'];
        $viewModel->googleAnalytics = $config['google-analytics'];
        $viewModel->appStoreLink = $config['hub_plus_store_links']['app_store'];
        $viewModel->playStoreLink = $config['hub_plus_store_links']['play_store'];

        if (!$config['debug']) {
            //handle the dispatch error (exception)
            $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'handleDispatchError'), 100);

            //handle the view render error (exception)
            $eventManager->attach(MvcEvent::EVENT_RENDER_ERROR, array($this, 'handleDispatchError'), 100);
        }

        if (isset($config['update_mode']) && $config['update_mode'] === true) {
            $aclHelper = new AclHelper($serviceManager);
            $aclHelper->logout();

            $eventManager->attach(MvcEvent::EVENT_ROUTE, function (MvcEvent $event) {
                /** @var \Zend\Http\PhpEnvironment\Response $response */
                $response = $event->getResponse();
                // set content & status
                $response->setStatusCode(503);
                $response->setContent(file_get_contents(APPLICATION_ROOT . '/public/maintenance.html'));
                return $response;
            }, 1000);
        }
    }

    public function init(ModuleManager $moduleManager)
    {
        $moduleName = $moduleManager->getEvent()->getModuleName();
        if ($moduleName == 'Application') {
            $events = $moduleManager->getEventManager();
            $sharedEvents = $events->getSharedManager();
            $sharedEvents->attach(array(__NAMESPACE__, 'Application'), 'dispatch', array($this, 'initAuth'), 100);
        }
    }

    public function initAuth(MvcEvent $e)
    {
        $app = $e->getApplication();
        $sm = $app->getServiceManager();

        $routerMatch = $e->getRouteMatch();
        $controller = $routerMatch->getParam('controller');

        $config = $sm->get('config');
        $controllerWithNoAuth = $config['controller-with-no-auth'];

        $aclHelper = new AclHelper($sm);
        if (!in_array($controller, $controllerWithNoAuth) && !$aclHelper->isLogged()) {
            $controller = $e->getTarget();
            return $controller->redirect()->toRoute('home');
        }

        return null;
    }

    public function getConfig()
    {
        $config = array();

        $configFiles = array(
            __DIR__ . '/config/module.config.php',                       // Module main config
            __DIR__ . '/config/module.config.action.php',                // Module action route config
            __DIR__ . '/config/module.config.admin.php',                 // Module admin main route config
            __DIR__ . '/config/module.config.admin.analytics.php',       // Module admin analytics route config
            __DIR__ . '/config/module.config.admin.analytics.ajax.php',  // Module admin analytics ajax route config
            __DIR__ . '/config/module.config.admin.content.php',         // Module admin content route config
            __DIR__ . '/config/module.config.admin.form.php',            // Module admin user route config
            __DIR__ . '/config/module.config.admin.notification.php',    // Module admin notification route config
            __DIR__ . '/config/module.config.admin.section.php',         // Module admin section route config
            __DIR__ . '/config/module.config.admin.user.php',            // Module admin user route config
            __DIR__ . '/config/module.config.ajax.php',                  // Module ajax route config
            __DIR__ . '/config/module.config.index.php',                 // Module index route config
            __DIR__ . '/config/module.config.media.php',                 // Module media route config
            __DIR__ . '/config/module.config.provider.ajax.php',         // Module provider route config
            __DIR__ . '/config/module.config.superadmin.php',            // Module superadmin route config
            __DIR__ . '/config/module.config.superadmin.ajax.php',       // Module superadmin ajax requests route config
            __DIR__ . '/config/module.config.remote.php',                // Module remote route config
        );

        // Merge all module config options
        foreach ($configFiles as $configFile) {
            $config = ArrayUtils::merge($config, include $configFile);
        }

        $config_json = ZendConfigFactory::fromFile(__DIR__ . '/config/module.config.menu.json', false);
        $config = ArrayUtils::merge($config, $config_json);

        return $config;
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getViewHelperConfig()
    {
        return include __DIR__ . '/config/viewhelper.config.php';
    }

    public function initSession($config)
    {
        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config);
        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();
        Container::setDefaultManager($sessionManager);
    }

    public function handleDispatchError(MvcEvent $e)
    {
        //die("ERROR: ".$e->getError());
        $action = null;
        $error = $e->getError();

        switch ($error) {
            case Application::ERROR_CONTROLLER_NOT_FOUND:
                $action = "controllerNotFound";
                break;
            case Application::ERROR_CONTROLLER_INVALID:
                $action = "controllerInvalid";
                break;
            case Application::ERROR_CONTROLLER_CANNOT_DISPATCH:
                $action = "controllerCannotDispatch";
                break;
            case Application::ERROR_ROUTER_NO_MATCH:
                $action = "routerNoMatch";
                break;
            case Application::ERROR_EXCEPTION:
                //$exception = $e->getParam('exception');
                //echo($exception);
                $action = "exception";
                break;
            default:
                $action = "generic";
        }

        // Here is where the "re-dispatch" happens, you can change the controller and action to your needs
        $em = $e->getApplication()->getEventManager();
        $routerMatch = new RouteMatch(array('controller' => 'Application\Controller\Error', 'action' => $action));

        $errorEvent = clone $e;
        $errorEvent->setRouteMatch($routerMatch);
        $e->stopPropagation(true);
        $em->trigger('dispatch', $errorEvent);
    }
}
