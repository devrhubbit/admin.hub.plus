<?php
namespace Application\Controller;

use Application\Model\NotificationModel;
use Common\Controller\HpAbstractActionController;
use Common\Helper\AclHelper;
use Common\Helper\LayoutHelper;

use Propel\Runtime\Exception\PropelException;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

class NotificationController extends HpAbstractActionController
{

    /* @var $viewModel \Zend\View\Model\ViewModel */
	protected $viewModel;

	protected $get_params;
	protected $post_params;

    /* @var $route_params RouteMatch */
	protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    /* @var $layoutHelper \Common\Helper\LayoutHelper */
    private $layoutHelper = null;

    private $actionWhiteList = ['notification-create'];

    private function inActionsWhiteList() {
        return in_array($this->params('action'), $this->actionWhiteList);
    }

	public function onDispatch(MvcEvent $e)
	{
        $this->get_params 	= $this->getRequest()->getQuery();
        $this->post_params 	= $this->getRequest()->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();
        $this->aclHelper    = new AclHelper($this->getServiceLocator());
        $this->layoutHelper = new LayoutHelper($this->layout());
        $this->viewModel    = new ViewModel();

        if(!$this->aclHelper->isLogged() && !$this->inActionsWhiteList()) {
            return $this->redirect()->toRoute('home');
        } else {
            $this->layoutHelper->setGMapKey($this->getServiceLocator()->get('config')['google-maps']['key']);
            $this->layoutHelper->setUser($this->aclHelper->getUser());
            $this->layoutHelper->setApp($this->aclHelper->getApp());
            $this->layoutHelper->setAppList($this->aclHelper->getAppList());
        }

        return parent::onDispatch($e);
	}

    public function notificationCreateAction() {
        $this->layout('layout/ajax');

        if(!$this->aclHelper->isLogged()) {
            die("ERROR: Session expired, try to <a href='".$this->getBaseurl()."'>LOG IN</a> again.");
        }

        $config = $this->getServiceLocator()->get('config');

        $form = $this->getServiceLocator()->get('Form\Notification\Factory');

        $application = $this->aclHelper->getApp();
        $systemLanguage = $config['system_languages'];
        $userLanguages[$application->main_contents_language] = $systemLanguage[$application->main_contents_language];
        $otherContentsLanguages = json_decode($application->other_contents_languages);

        if ($otherContentsLanguages) {
            foreach ($otherContentsLanguages as $language) {
                $userLanguages[$language] = $systemLanguage[$language];
            }
        }

        return $this->viewModel
            ->setVariable('userLanguages', $userLanguages)
            ->setVariable('form', $form);
    }

    public function notificationAppendAction() {
        $this->layout('layout/ajax');
        $this->viewModel->setTemplate('ajax/response');

        if(!$this->aclHelper->isLogged()) {
            die("ERROR: Session expired, try to <a href='".$this->getBaseurl()."'>LOG IN</a> again.");
        }

        $notificationModel = new NotificationModel($this);
        try {
            $notificationLang = $this->post_params['notification_lang'];
            $res = $notificationModel->appendNotification($this->post_params, $notificationLang);
            $content = $this->getJsonResponse($res->success, $res->message);
        } catch (PropelException $exception) {
            $content = $this->getJsonResponse(false, $exception->getMessage());
        }

        return $this->viewModel
                    ->setVariable('content', $content);
    }

    public function notificationListAction() {
        $config = $this->getServiceLocator()->get('config');

        $this->layoutHelper->setTitle("NOTIFICATIONS");
        $this->layoutHelper->setSubtitle("Your notification history");

        $breadcrumbs = array("Dashboard" => "/", "Notifications" => "/admin/notification/list");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "notifications", $this->getServiceLocator());

        return $this->viewModel;
    }

    public function ajaxNotificationListAction()
    {
        $this->layout('layout/ajax');
        $this->viewModel->setTemplate('ajax/response');

        if(!$this->aclHelper->isLogged()) {
            die("ERROR: Session expired, try to <a href='".$this->getBaseurl()."'>LOG IN</a> again.");
        }

        $notificationModel = new NotificationModel($this);

        $draw = (int)$this->get_params['draw'];
        $start = $this->get_params['start'];
        $length = $this->get_params['length'];
        $cols = $this->get_params['columns'];
        $order = $this->get_params['order'];
        $search = $this->get_params['search'];

        $data = $notificationModel->getList($draw, $start, $length, $cols, $order, $search);

        return $this->viewModel
            ->setVariable('content', $this->getDTJsonResponse($data));
    }

}
