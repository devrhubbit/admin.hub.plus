<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 14/04/16
 * Time: 11:36
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;

use \Application\Model\IndexModel;

class ErrorController extends AbstractActionController
{

    protected $viewModel;
    protected $route_params;

    public function onDispatch(MvcEvent $e)
    {
        $this->layout('layout/error');

        $this->viewModel = new ViewModel;
        $this->viewModel->setTemplate('application/error/error');

        $this->route_params = $this->getEvent()->getRouteMatch();

        return parent::onDispatch($e);
    }

    //http://stackoverflow.com/questions/12108921/how-to-render-a-different-view-in-controller-action-of-zf2
    public function genericAction() {
        return $this->viewModel->setVariable('message', "ERROR ACTION generic");
    }

    public function exceptionAction() {
        return $this->viewModel->setVariable('message', "ERROR ACTION exception");
    }

    public function routerNoMatchAction() {
        return $this->viewModel->setVariable('message', "ERROR ACTION routerNoMatch");
    }

    public function controllerNotFoundAction() {
        return $this->viewModel->setVariable('message', "ERROR ACTION controllerNotFound");
    }

    public function controllerInvalidAction() {
        return $this->viewModel->setVariable('message', "ERROR ACTION controllerInvalid");
    }

    public function controllerCannotDispatchAction() {
        return $this->viewModel->setVariable('message', "ERROR ACTION controllerCannotDispatch");
    }
}