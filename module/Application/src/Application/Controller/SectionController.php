<?php

namespace Application\Controller;

use Application\Model\MediaModel;
use Common\Helper\AclHelper;
use Common\Helper\LayoutHelper;

use Application\Model\SectionModel;
use Form\Section\Favourite\FavouriteSectionForm;
use Rest\Model\HpRestModel;
use Rest\Model\PageRestModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

use Form\Section\SectionForm,
    Form\Section\CustomForm\CustomFieldForm,
    Form\Section\CustomForm\CustomFieldFilter;

class SectionController extends AbstractActionController
{

    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    /* @var $layoutHelper \Common\Helper\LayoutHelper */
    private $layoutHelper = null;

    private $baseUrl = "";

    public function onDispatch(MvcEvent $e)
    {
        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->get_params = $this->getRequest()->getQuery();
        $this->post_params = $this->getRequest()->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();
        $this->layoutHelper = new LayoutHelper($this->layout());
        $this->viewModel = new ViewModel();

        if (!$this->aclHelper->isLogged()) {
            return $this->redirect()->toRoute('home');
        } else {
            $this->layoutHelper->setGMapKey($this->getServiceLocator()->get('config')['google-maps']['key']);
            $this->layoutHelper->setUser($this->aclHelper->getUser());
            $this->layoutHelper->setApp($this->aclHelper->getApp());
            $this->layoutHelper->setAppList($this->aclHelper->getAppList());
        }

        $uri = $this->getRequest()->getUri();
        $scheme = $uri->getScheme();
        $host = $uri->getHost();
        $this->baseUrl = sprintf('%s://%s', $scheme, $host);

        return parent::onDispatch($e);
    }

    public function sectionListAction()
    {
        $config = $this->getServiceLocator()->get('config');

        $this->layoutHelper->setTitle("SECTION");
        $this->layoutHelper->setSubtitle("Manage your application areas");

        $breadcrumbs = array("Dashboard" => "/", "Sections" => "/admin/section");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "section", $this->getServiceLocator());

        $sectionModel = new SectionModel($this->getServiceLocator());
        $mediaModel = new MediaModel($this->getServiceLocator());

        $area = $this->route_params->getParam('area');

        return $this->viewModel
            ->setVariable('area', strtoupper($area))
            ->setVariable('user', $this->aclHelper->getUser())
            ->setVariable('mediaModel', $mediaModel)
            ->setVariable('section_types', $sectionModel->getSectionTypes())
            ->setVariable('header_sections', $sectionModel->getSections(\Form\Section\SectionForm::AREA_HEADER))
            ->setVariable('body_sections', $sectionModel->getSections(\Form\Section\SectionForm::AREA_BODY))
            ->setVariable('drawer_sections', $sectionModel->getSections(\Form\Section\SectionForm::AREA_MENU))
            ->setVariable('menu_sections', $sectionModel->getSections(\Form\Section\SectionForm::AREA_FOOTER))
            ->setVariable('hidden_sections', $sectionModel->getSections(\Form\Section\SectionForm::AREA_HIDDEN));
    }

    public function sectionCreateAction()
    {
        $config = $this->getServiceLocator()->get('config');
        $sectionType = $this->route_params->getParam('type');

        $baseLayout = "";
        $layoutList = $config['list_cell_types'];
        foreach ($layoutList as $key => $value) {
            if ($baseLayout == "") {
                if (in_array(strtoupper($sectionType), $value['types'])) {
                    $baseLayout = $key;
                }
            }
        }

        $layout = json_decode($this->aclHelper->getApp()->layout);
        $customLayoutTheme = $layout->theme;
        $customTheme = json_encode($customLayoutTheme);

        $sectionModel = new SectionModel($this->getServiceLocator());
        $mediaModel = new MediaModel($this->getServiceLocator());

        if (!$sectionModel->isValidType($sectionType)) {
            return $this->redirect()->toRoute('home');
        }

        $sectionParams = $sectionModel->getByType($sectionType);
        $sectionTypes = $sectionModel->getSectionTypes($sectionType);

        SectionModel::setSelectedType($sectionType);

        $this->layoutHelper->setTitle("SECTION");
        $this->layoutHelper->setSubtitle("Create new application area");

        $breadcrumbs = array("Dashboard" => "/", "Sections" => "/admin/section", strtolower($sectionParams["label"]) => "");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "section", $this->getServiceLocator());

        /** @var FavouriteSectionForm $form */
        $form = $this->getServiceLocator()->get('Form\Section\Factory');

        $data = $sectionModel->flatLayoutJson($this->aclHelper->getApp()->layout, $baseLayout, $this->aclHelper->getApp()->template_id);

        $application = $this->aclHelper->getApp();
        $systemLanguage = $config['system_languages'];
        $userLanguages[$application->main_contents_language] = $systemLanguage[$application->main_contents_language];
        $data['content_lang'] = $application->main_contents_language;


        $form->setData($data);
//        var_dump($data, $form->getInputFilter());

        if ($form->isValidPostRequest($this->request)) {
            $res = $sectionModel->createSection($form->getData());
//            var_dump($res);
            if ($res['result']) {
                $this->redirect()->toRoute('sections', array('area' => strtolower($res['area'])));
            } else {
                $this->viewModel->setVariable('error_msg', "INTERNAL SERVER ERROR!");
            }
        } else {
//            var_dump($this->request->isPost(), $form->getMessages());
            //die('xxxx');
        }

        if (strtoupper($sectionType) == SectionForm::CUSTOM_FORM) {
            $formField = new CustomFieldForm($this->serviceLocator);
            $formFieldFilter = new CustomFieldFilter();
            $formField->setInputFilter($formFieldFilter->getInputFilter());
            $this->viewModel->setVariable('form_field', $formField);
        }

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('section_types', $sectionTypes)
            ->setVariable('section_params', $sectionParams)
            ->setVariable('custom_theme', $customTheme)
            ->setVariable('currentLang', $application->main_contents_language)
            ->setVariable('userLanguages', $userLanguages)
            ->setVariable('aclHelper', $this->aclHelper)
            ->setVariable('mapList', $config['custom_form_fields_map'])
            ->setVariable('form', $form);
    }

    public function sectionUpdateAction()
    {
        $config = $this->getServiceLocator()->get('config');
        $sectionType = $this->route_params->getParam('type');
        $sectionId = $this->route_params->getParam('id');

        $sectionModel = new SectionModel($this->getServiceLocator());
        $mediaModel = new MediaModel($this->getServiceLocator());

        if (!$sectionModel->isValidType($sectionType)) {
            return $this->redirect()->toRoute('home');
        }
        $sectionData = $sectionModel->getSectionById($sectionId);
        $sectionParams = $sectionModel->getByType($sectionType);
        $sectionTypes = $sectionModel->getSectionTypes($sectionType);

        $sectionLayout = array(
            'color' => array(
                'global_bg' => $sectionData['global_bg'],
                'accent_bg' => $sectionData['accent_bg'],
                'primary_bg' => $sectionData['primary_bg'],
                'secondary_bg' => $sectionData['secondary_bg'],
                'light_txt' => $sectionData['light_txt'],
                'dark_txt' => $sectionData['dark_txt'],
            ),
            'font' => array(
                'primary' => $sectionData['primary_font'],
                'primary_size' => $sectionData['primary_font_size'],
                'secondary' => $sectionData['secondary_font'],
                'secondary_size' => $sectionData['secondary_font_size'],
            )
        );

        $customTheme = json_encode($sectionLayout);

        $this->layoutHelper->setTitle("SECTION");
        $this->layoutHelper->setSubtitle("Update your application area");

        $breadcrumbs = array("Dashboard" => "/", "Sections" => "/admin/section", $sectionData['title'] => "");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "section", $this->getServiceLocator());

        SectionModel::setSelectedType($sectionType);
        /** @var $form \Form\Section\SectionForm */
        $form = $this->getServiceLocator()->get('Form\Section\Factory');

        $form->setData($sectionData);

        if ($form->isValidPostRequest($this->request)) {
            $formData = $form->getData();
            $res = $sectionModel->saveSection($sectionId, $formData);
            if ($res['result']) {
                $sectionData = $sectionModel->getSectionById($sectionId);
                $form->setData($sectionData);
                if ($formData['reload_content'] === "0") {
                    $this->redirect()->toRoute('sections', array('area' => strtolower($res['area'])));
                }
            } else {
                $this->viewModel->setVariable('error_msg', "INTERNAL SERVER ERROR!");
            }
        }

        if (strtoupper($sectionType) == SectionForm::CUSTOM_FORM) {
            $formField = new CustomFieldForm($this->serviceLocator);
            $formFieldFilter = new CustomFieldFilter();
            $formField->setInputFilter($formFieldFilter->getInputFilter());
            $this->viewModel->setVariable('form_field', $formField);
        }

        $hashId = md5($sectionId);

        $appUrl = $this->baseUrl . $this->url()->fromRoute("app_url/get",
                array(
                    "type" => strtolower(PageRestModel::SECTION_CONTENT),
                    "hashId" => $hashId,
                )
            );

        $application = $this->aclHelper->getApp();
        $systemLanguage = $config['system_languages'];
        $userLanguages[$application->main_contents_language] = $systemLanguage[$application->main_contents_language];
        $otherContentsLanguages = json_decode($application->other_contents_languages);

        if ($otherContentsLanguages) {
            foreach ($otherContentsLanguages as $language) {
                $userLanguages[$language] = $systemLanguage[$language];
            }
        }

        $session = new Container('acl');

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('section_types', $sectionTypes)
            ->setVariable('section_params', $sectionParams)
            ->setVariable('section_id', $sectionId)
            ->setVariable('custom_theme', $customTheme)
            ->setVariable('appUrl', $appUrl)
            ->setVariable('hashId', HpRestModel::SCT_HASH_PREFIX."-".$hashId)
            ->setVariable('currentLang', $session->currentLang)
            ->setVariable('userLanguages', $userLanguages)
            ->setVariable('aclHelper', $this->aclHelper)
            ->setVariable('mapList', $config['custom_form_fields_map'])
            ->setVariable('form', $form);
    }

}
