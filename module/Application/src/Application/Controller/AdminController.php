<?php

namespace Application\Controller;

use Application\Model\AnalyticsModel;
use Application\Model\ContentModel;
use Application\Model\BackendUsersModel;
use Application\Model\SectionModel;
use Application\Model\TemplateModel;
use Common\Helper\AclHelper;
use Common\Helper\LayoutHelper;

use Application\Model\AppModel;
use Application\Model\UserModel;
use Application\Model\MediaModel;
use Common\Helper\SecurityHelper;
use Form\App\AppForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Common\Helper\ImageHelper;
use Zend\Http\PhpEnvironment\Request;

class AdminController extends AbstractActionController
{

    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    /** @var Request $request */
    protected $request = null;

    protected $get_params;
    protected $post_params;
    protected $file_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    /* @var $layoutHelper \Common\Helper\LayoutHelper */
    private $layoutHelper = null;

    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = new ViewModel();

        /** @var ViewModel $viewModelForHelper */
        $viewModelForHelper = $this->layout();

        $this->request = $this->getRequest();
        $this->get_params = $this->request->getQuery();
        $this->post_params = $this->request->getPost();
        $this->file_params = $this->request->getFiles();
        $this->route_params = $this->getEvent()->getRouteMatch();

        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->layoutHelper = new LayoutHelper($viewModelForHelper);
        $this->layoutHelper->setGMapKey($this->getServiceLocator()->get('config')['google-maps']['key']);

        if (!$this->aclHelper->isLogged()) {
            $this->redirect()->toRoute('home');
        } else {
            $this->layoutHelper->setUser($this->aclHelper->getUser());
            $this->layoutHelper->setApp($this->aclHelper->getApp());
            $this->layoutHelper->setAppList($this->aclHelper->getAppList());
        }

        return parent::onDispatch($e);
    }

    public function adminAction()
    {
        $app = $this->aclHelper->getApp();

        if (is_null($app) ||
            !property_exists($this->aclHelper->getApp(), 'id') ||
            $this->aclHelper->getApp()->id <= 0) {
            if ($this->aclHelper->getUser()->role_id <= BackendUsersModel::ADMIN_ROLE_ID) {
                return $this->redirect()->toRoute('register-app');
            } else {
                return $this->redirect()->toRoute('home');
            }
        } else {
            $analitycsModel = new AnalyticsModel($this->getServiceLocator());
            $deviceData = $analitycsModel->getSimpleDeviceData();
            $postViewData = $analitycsModel->getSimplePostViewData();
            $postLikeData = $analitycsModel->getSimplePostLikeData();
            $postShareData = $analitycsModel->getSimplePostShareData();

            $userModel = new UserModel($this->getServiceLocator());
            $lastUsers = $userModel->getLastUsers(0, 8);

            $contentModel = new ContentModel($this->getServiceLocator());
            $lastContents = $contentModel->getLastContents(0, 4);

            $sectionModel = new SectionModel($this->getServiceLocator());
            $lastSections = $sectionModel->getLastSections(0, 4);

            $mediaModel = new MediaModel($this->getServiceLocator());

            $config = $this->getServiceLocator()->get('config');

            $this->layoutHelper->setTitle("ADMIN");
            $this->layoutHelper->setSubtitle("main page");

            $breadcrumbs = array("Dashboard" => "/");
            $this->layoutHelper->setBreadcrumbs($breadcrumbs);

            $menu = $config["menu"]["admin"];
            $this->layoutHelper->setMenu($menu, true, $selected = "dashboard", $this->getServiceLocator());

            return $this->viewModel
                ->setVariable('user', $this->aclHelper->getUser())
                ->setVariable('device', $deviceData)
                ->setVariable('post_view', $postViewData)
                ->setVariable('post_like', $postLikeData)
                ->setVariable('post_share', $postShareData)
                ->setVariable('users', $lastUsers)
                ->setVariable('contents', $lastContents)
                ->setVariable('mediaModel', $mediaModel)
                ->setVariable('sections', $lastSections);
        }

    }

    public function profileAction()
    {
        $config = $this->getServiceLocator()->get('config');

        $this->layoutHelper->setTitle("PROFILE");
        $this->layoutHelper->setSubtitle("Update your data");

        $breadcrumbs = array("Dashboard" => "#", "Profile" => "/admin/profile");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, ($this->aclHelper->getApp() != null), $selected = "dashboard", $this->getServiceLocator());

        $form = $this->getServiceLocator()->get('Form\Profile\Factory');

        $userData = $this->aclHelper->getUser();
        $form->setData(array(
            'first-name' => $userData->firstname,
            'last-name' => $userData->lastname,
            'email' => $userData->email,
            'avatar' => $userData->imagePath != "" ? $userData->imagePath : null,
        ));

        if ($form->isValidPostRequest($this->request)) {
            if ($this->post_params['password'] != $this->post_params['confirm-password']) {
                $this->viewModel->setVariable('error_msg', "Password and confirm password fields are different!");
            } else {
                $id = $userData->id;
                $email = $this->post_params['email'];
                $password = $this->post_params['password'];
                $firstname = $this->post_params['first-name'];
                $lastname = $this->post_params['last-name'];
                $avatar = $this->post_params['avatar'];

                $BackendUsersModel = new BackendUsersModel($this->getServiceLocator());

                if ($BackendUsersModel->updateUser($id, $email, $password, $firstname, $lastname, $avatar)) {
                    $this->aclHelper->getUser()->firstname = $firstname;
                    $this->aclHelper->getUser()->lastname = $lastname;
                    $this->aclHelper->getUser()->email = $email;
                    $this->aclHelper->getUser()->imagePath = $avatar;
                }
            }
        }

        return $this->viewModel
            ->setVariable('form', $form);
    }

    /**
     * @return ViewModel
     * @throws \Exception
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function registerAppAction()
    {
        $config = $this->getServiceLocator()->get('config');
        $appId = $this->route_params->getParam('id');

        $this->layoutHelper->setTitle("APPLICATION");
        $this->layoutHelper->setSubtitle("Create a new application");
        $boxTitle = "NEW APPLICATION</b> creation walkthrough<b>";
        $breadcrumbs = array("Dashboard" => "/", "New Application" => "#");

        $menu = $config["menu"]["admin"];

        $action = AppForm::CREATE_APP;
        if (!is_null($appId)) {
            $action = AppForm::UPDATE_APP;
        }

        /** @var \Form\App\AppForm $form */
        $form = $this->getServiceLocator()->get('Form\App\Factory');
        $form->setData(array(
            'template_id' => 1,
            'filter' => true,
            'share' => true,
            'like' => true,
        ));

        $form->setAction($action);

        $appModel = new AppModel($this->getServiceLocator());
        $templateModel = new TemplateModel($this->getServiceLocator());

        $extraAppIcons = $config['extra_app_icons'];
        $form->setExtraAppIcons($extraAppIcons);

        if (APPLICATION_ENV === "production" && $config['section_types']['DEV']['only_dev'] === true) {
            $controllerUriFields = null;
        } else {
            $controllerUriFields = $config['controller_uri'];
        }
        $form->setControllerUriFields($controllerUriFields);

        $customLayoutTheme = $templateModel->getPaletteTheme(1);

        $enable_sidebar = false;

        $app_data_title = null;
        $app_data_logo = null;
        $icon_path = $appModel->getBaseurl() . "/img/placeholder/app-icon.png"; //avatar placeholder

        if (!is_null($appId)) {
            $enable_sidebar = true;
            $form->setEditMode(true);

            $app = $appModel->getApplicationByAppId($appId);
            $layout = json_decode($app->layout);
            $customLayoutTheme = $layout->theme;

            $this->aclHelper->setApp($app);
            $this->layoutHelper->setApp($this->aclHelper->getApp());

            $this->layoutHelper->setSubtitle("Modify your application");
            $boxTitle = strtoupper($app->title) . " APPLICATION";
            $breadcrumbs = array("Dashboard" => "/", $app->title . " Application" => "#");

            $app_data_title = $app->title;
            $app_data_logo = $app->icon;

            $this->updateFormData($app, $config, $appModel, $appId, $form);

            $this->viewModel->setVariable('app_id', $appId);
        }

        $this->layoutHelper->setBreadcrumbs($breadcrumbs);
        $this->layoutHelper->setMenu($menu, $enable_sidebar, $selected = "dashboard", $this->getServiceLocator());

        if ($this->request->isPost()) {
            $GoogleFileJson = $this->file_params['upload-google-notification-json'];
            $data = array_merge_recursive(
                $this->request->getPost()->toArray(),
                array('upload-google-notification-json' => $GoogleFileJson) // Trick for upload file validator
            );

            $form->setData($data);
            if ($form->isValid()) {
                $certFile = isset($this->file_params['upload-apple-certificate']) && $this->file_params['upload-apple-certificate']['error'] === UPLOAD_ERR_OK ? $this->file_params['upload-apple-certificate'] : null;
                $googleJsonFile = isset($this->file_params['upload-google-notification-json']) && $this->file_params['upload-google-notification-json']['error'] !== 4 ? $this->file_params['upload-google-notification-json'] : null;

                if (is_null($appId)) {
                    $msg = $appModel->createApp($this->post_params, $extraAppIcons, $controllerUriFields, $certFile, $googleJsonFile);
                    if (is_null($msg)) {
                        $this->redirect()->toRoute('admin');
                    } else {
                        $this->viewModel->setVariable('error_msg', $msg);
                    }
                } else {
                    $appModel->updateApp($appId, $this->post_params, $extraAppIcons, $controllerUriFields, $certFile, $googleJsonFile, $config);

                    $app = $appModel->getApplicationByAppId($appId);
                    $layout = json_decode($app->layout);
                    $customLayoutTheme = $layout->theme;
                    $this->updateFormData($app, $config, $appModel, $appId, $form);

                    $app_data_title = $app->title;
                    $app_data_logo = $app->icon;
                }

                // Put language in session
                $systemLanguage = $config['system_languages'];
                $this->aclHelper->setContentLang(strtolower($data['main-language']), $systemLanguage);
            }
        }

        $customTheme = json_encode($customLayoutTheme);

        if (!is_null($app_data_logo) && $app_data_logo != "") {
            $cdnBaseUrl = $appModel->getCdnBaseurl();
            $icon_path = $cdnBaseUrl . ImageHelper::getAppIconUrl() . $app_data_logo;
        }

        $mediaModel = new MediaModel($this->getServiceLocator());

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('extraAppIcons', $extraAppIcons)
            ->setVariable('controllerUriFields', $controllerUriFields)
            ->setVariable('app_data_title', $app_data_title)
            ->setVariable('app_data_logo', $icon_path)
            ->setVariable('box_title', $boxTitle)
            ->setVariable('action', $action)
            ->setVariable('custom_theme', $customTheme)
            ->setVariable('bundle_root', $config['bundle_root'])
            ->setVariable('form', $form);
    }

    public function archiveAction()
    {
        $config = $this->getServiceLocator()->get('config');

        $this->layoutHelper->setTitle("GALLERY");
        $this->layoutHelper->setSubtitle("Manage your archive");

        $breadcrumbs = array("Dashboard" => "#", "Gallery" => "/media/archive");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "gallery", $this->getServiceLocator());

        $mediaModel = new MediaModel($this->getServiceLocator());
        $media = $mediaModel->getMedias();

//        echo "<pre>";
//        var_dump($media->initialPreviewThumbTags);
//        echo "</pre>";
//        die();

        $addRemoteMediaForm = $this->getServiceLocator()->get('Form\Media\Factory');

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('addRemoteMediaForm', $addRemoteMediaForm)
            ->setVariable('initialPreview', json_encode($media->initialPreview))
            ->setVariable('initialPreviewConfig', json_encode($media->initialPreviewConfig))
            ->setVariable('initialPreviewThumbTags', json_encode($media->initialPreviewThumbTags));
    }

    public function providerAction()
    {
        $config = $this->getServiceLocator()->get('config');

        $this->layoutHelper->setTitle("PROVIDER");
        $this->layoutHelper->setSubtitle("Manage your connectors");

        $breadcrumbs = array("Dashboard" => "#", "Provider" => "/admin/provider");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "provider", $this->getServiceLocator());

        $form = $this->getServiceLocator()->get('Form\Provider\Factory');

        return $this->viewModel
            ->setVariable('form', $form);
    }

    public function templatesAction()
    {
        $config = $this->getServiceLocator()->get('config');

        $templateModel = new TemplateModel($this->serviceLocator);

        $this->layoutHelper->setTitle("TEMPLATES");
        $this->layoutHelper->setSubtitle("Manage your templates");

        $breadcrumbs = array("Dashboard" => "/", "Templates" => "/admin/templates");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "templates", $this->getServiceLocator());

        $form = $this->getServiceLocator()->get('Form\Template\Factory');

        if ($this->request->isPost()) {
            if ($form->isValidPostRequest($this->request)) {
                $templateId = $this->post_params['template-id'];
                if (empty($templateId)) {
                    $res = $templateModel->createTemplate($this->post_params);
                } else {
                    $res = $templateModel->updateTemplate($templateId, $this->post_params);
                }
                if ($res) $this->redirect()->toRoute('templates');
            } else {
                $this->viewModel
                    ->setVariable('error_msg', "Template save error, insert title.");
            }
        }

        return $this->viewModel
            ->setVariable('form', $form);
    }

    /**
     * @param $app
     * @param $config
     * @param AppModel $appModel
     * @param $appId
     * @param AppForm $form
     * @throws \Exception
     */
    private function updateFormData($app, $config, AppModel $appModel, $appId, AppForm $form)
    {
        $layout = json_decode($app->layout);
        $customLayoutTheme = $layout->theme;
        $pushNotificationParams = json_decode($app->push_notification);

        $appData = array(
            'title' => $app->title,
            'category' => $app->category_id,
            'description' => $app->description,
            'bundle' => str_replace($config['bundle_root'], "", $app->bundle),
            'cell-type' => $layout->name,
            'template_id' => $app->template_id,
            'old_layout' => json_encode($customLayoutTheme),
            'global_bg' => $customLayoutTheme->color->global_bg,
            'accent_bg' => $customLayoutTheme->color->accent_bg,
            'primary_bg' => $customLayoutTheme->color->primary_bg,
            'secondary_bg' => $customLayoutTheme->color->secondary_bg,
            'light_txt' => $customLayoutTheme->color->light_txt,
            'dark_txt' => $customLayoutTheme->color->dark_txt,
            'primary_font' => $customLayoutTheme->font->primary,
            'primary_font_size' => $customLayoutTheme->font->primary_size,
            'secondary_font' => $customLayoutTheme->font->secondary,
            'secondary_font_size' => $customLayoutTheme->font->secondary_size,
            'icon' => $app->icon,
            'splash' => $app->splash_screen,
            'app-logo' => $app->app_logo,
            'splash-bg' => $app->background_color,
            'toolbar' => $app->toolbar,
            'auth' => $layout->auth->mode,
            'filter' => $layout->filter,
            'share' => $layout->share,
            'like' => $layout->like,
            'app-published' => $app->published,
            'google-api-key' => $pushNotificationParams !== null ? $pushNotificationParams->android->api_key : null,
            'upload-google-notification-json' => $pushNotificationParams !== null ? $pushNotificationParams->android->json_file : null,
            'pass-phrase-apple-certificate' => $pushNotificationParams !== null ? $pushNotificationParams->ios->pass_phrase : null,
            'upload-apple-certificate' => $pushNotificationParams !== null ? $pushNotificationParams->ios->certificate : null,
            'facebook-token' => $app->facebook_token,
            'google-analytics-ua' => $app->google_analytics_ua,
            'store-apple-username' => $app->apple_id,
            'store-apple-password' => SecurityHelper::decrypt($app->apple_id_password, $appModel->getEncryptKey($appId), true),
            'store-apple-app-url' => $app->apple_store_app_link,
            'store-google-username' => $app->play_store_id,
            'store-google-password' => SecurityHelper::decrypt($app->play_store_id_password, $appModel->getEncryptKey($appId), true),
            'store-google-app-url' => $app->play_store_app_link,
            'main-language' => $app->main_contents_language,
            'other-language' => json_decode($app->other_contents_languages),
        );

        if ($app->system_icons != "") {
            $extraIcons = json_decode($app->system_icons);
            foreach ($extraIcons as $k => $v) {
                $appData[$k] = $v;
            }
        }

        if ($app->controller_uri !== null && $app->controller_uri !== "") {
            $controllerUriFields = json_decode($app->controller_uri);
            foreach ($controllerUriFields as $os => $fields) {
                foreach ($fields as $k => $v) {
                    $appData[$k] = $v;
                }
            }
        }

        $form->setData($appData);
    }
}
