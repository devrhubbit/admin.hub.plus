<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 07/09/17
 * Time: 15:59
 */

namespace Application\Controller;

use Application\Model\MediaModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

class RemoteController extends AbstractActionController
{
    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    /** @var \Zend\Http\PhpEnvironment\Request $request */
    protected $request;

    /** @var  $post_params \Zend\Stdlib\Parameters */
    protected $get_params;

    /** @var  $post_params \Zend\Stdlib\Parameters */
    protected $post_params;

    /* @var $route_params \Zend\Mvc\Router\RouteMatch */
    protected $route_params;

    protected $request_content;

    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = new ViewModel();
        $this->viewModel->setTemplate('remote/remote-media-page');

        $this->request = $this->getRequest();

        $this->get_params = $this->request->getQuery();
        $this->post_params = $this->request->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        $this->layout('layout/remote');

        if ($this->getRequest()->getContent() != null) {
            $this->request_content = json_decode($this->getRequest()->getContent());
        }

        return parent::onDispatch($e);
    }

    public function getRemoteMediaPageAction()
    {
        $mediaId = $this->route_params->getParam('media_id');
        $model  = new MediaModel($this->getServiceLocator());

        $snippet = $model->getRemoteMediaPage($mediaId);

        return $this->viewModel->setVariable('snippet', $snippet);
    }

    public function getPageRemoteMediaPageAction()
    {
        $mediaType = $this->route_params->getParam('type');
        $remoteUrl = base64_decode($this->route_params->getParam('remote_url'));
        $model  = new MediaModel($this->getServiceLocator());

        $snippet = $model->getRemoteIframePage($mediaType, $remoteUrl);

        return $this->viewModel->setVariable('snippet', $snippet);
    }
}