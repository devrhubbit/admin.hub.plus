<?php

namespace Application\Controller;

use Application\Model\AppModel;
use Common\Helper\AclHelper;
use Common\Helper\LayoutHelper;

use Propel\Runtime\Exception\PropelException;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

use Zend\Http\PhpEnvironment\Request;
use \Application\Model\BackendUsersModel;

class IndexController extends AbstractActionController
{
    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel = null;

    /** @var Request $request */
    protected $request = null;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    /* @var $layoutHelper \Common\Helper\LayoutHelper */
    private $layoutHelper = null;

    /* @var $BackendUsersModel \Application\Model\BackendUsersModel; */
    private $BackendUsersModel = null;

    private $get_params = null;
    private $post_params = null;

    /* @var $route_params RouteMatch */
    private $route_params = null;

    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = new ViewModel;
        /** @var ViewModel $viewModelForHelper */
        $viewModelForHelper = $this->layout();

        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->layoutHelper = new LayoutHelper($viewModelForHelper);

        $this->BackendUsersModel = new BackendUsersModel($this->getServiceLocator());

        $this->request = $this->getRequest();

        $this->get_params = $this->request->getQuery();
        $this->post_params = $this->request->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        if ($this->aclHelper->isLogged()) {
            $this->redirect()->toRoute('admin');
        }

        $this->layout('layout/starter');
        $form = $this->getServiceLocator()->get('Form\Login\Factory');

        $config = $this->getServiceLocator()->get('config');
        $backendName = $config['backend_name'];

        $renderer = $this->getServiceLocator()->get('Zend\View\Renderer\PhpRenderer');
        $renderer->headTitle($backendName);

        if ($this->route_params->getParam('token') !== null) {
            $token = explode(":", base64_decode($this->route_params->getParam('token')));

            $email = $token[0];
            $password = $token[1];

            $loginResponse = $this->aclHelper->login($email, $password);

            if (!$this->aclHelper->isLogged()) {
                $this->viewModel->setVariable('error_msg', $loginResponse['message']);
            } else {
                $this->redirect()->toRoute('admin');
            }
        }

        if ($form->isValidPostRequest($this->request)) {
            $email = $this->post_params['email'];
            $password = $this->post_params['password'];

            $loginResponse = $this->aclHelper->login($email, $password);

            if ($this->aclHelper->isLogged()) {
                $this->redirect()->toRoute('admin');
            } else {
                $this->viewModel->setVariable('error_msg', $loginResponse['message']);
            }
        }

        return $this->viewModel
            ->setVariable('form', $form);
    }

    public function logoutAction()
    {
        $this->aclHelper->logout();
        $this->redirect()->toRoute('home');
    }

    public function passwordRecoveryAction()
    {
        $this->layout('layout/starter');
        $form = $this->getServiceLocator()->get('Form\PasswordRecovery\Factory');

        $config = $this->getServiceLocator()->get('config');
        $backendName = $config['backend_name'];

        $renderer = $this->getServiceLocator()->get('Zend\View\Renderer\PhpRenderer');
        $renderer->headTitle($backendName);

        if ($form->isValidPostRequest($this->request)) {
//            $this->generateNewPassword($this->request->getPost());
//            $this->redirect()->toRoute('password-recovered');
            $user = $this->BackendUsersModel->isValidEmail($this->post_params['email']);
            if (!is_null($user) && isset($user->id) && isset($user->email)) {
                $message = $this->BackendUsersModel->newPasswordAndEmail($user->id, $user->email);
                $this->redirect()->toRoute('password-recovered', array('message' => $message));
            } else {
                $this->viewModel->setVariable('error_msg', "This email address doesn't exists!");
            }
        }

        return $this->viewModel
            ->setVariable('form', $form);

    }

    public function passwordRecoveredAction()
    {
        $this->layout('layout/starter');

        $config = $this->getServiceLocator()->get('config');
        $backendName = $config['backend_name'];

        $renderer = $this->getServiceLocator()->get('Zend\View\Renderer\PhpRenderer');
        $renderer->headTitle($backendName);

        return $this->viewModel
            ->setVariable('message', $this->route_params->getParam('message'));

    }

    public function registerUserAction()
    {
        $this->layout('layout/starter');
        $form = $this->getServiceLocator()->get('Form\Registration\Factory');

        $config = $this->getServiceLocator()->get('config');
        $backendName = $config['backend_name'];

        $renderer = $this->getServiceLocator()->get('Zend\View\Renderer\PhpRenderer');
        $renderer->headTitle($backendName);

        if ($form->isValidPostRequest($this->request)) {
            $email = $this->post_params['email'];
            $password = $this->post_params['password'];
            $privacy = $this->post_params['privacy'];
            $firstName = $this->post_params['first-name'];
            $lastName = $this->post_params['last-name'];

            $appModel = new AppModel($this->getServiceLocator());
            $appID = $appModel->getApplicationId();

            $roleId = BackendUsersModel::USER;
            if ($appID === null) {
                $roleId = BackendUsersModel::ADMIN_ROLE_ID;
            }

            $userId = $this->BackendUsersModel->registerUser($email, $password, $privacy, $firstName, $lastName, $roleId);
            $this->BackendUsersModel->sendRegistrationEmail($email, null, $firstName, $lastName);

            if ($privacy != "no" && $userId !== null) {
                if ($appID !== null) {
                    $this->BackendUsersModel->lockUnlockUser($userId, 1);
                    try {
                        $appModel->AddUserAppRel($appID, $userId);

                        $this->BackendUsersModel->sendRegistrationEmailToAdmins($email, $firstName, $lastName);
                        $this->redirect()->toRoute('register-user-thank-you');
                    } catch (PropelException $exception) {
                        $this->viewModel
                            ->setVariable('error_msg', $exception->getMessage());
                    }
                } else {
                    $this->aclHelper->login($email, $password);
                    $this->redirect()->toRoute('register-app');
                }
            } else {
                $this->viewModel
                    ->setVariable('error_msg', "Registration failed, please try again");
            }
        }

        return $this->viewModel
            ->setVariable('form', $form)
            ->setVariable('backend_name', $backendName);
    }

    public function registerUserThankYouAction()
    {
        $this->layout('layout/starter');

        $config = $this->getServiceLocator()->get('config');
        $backendName = $config['backend_name'];

        $renderer = $this->getServiceLocator()->get('Zend\View\Renderer\PhpRenderer');
        $renderer->headTitle($backendName);

        return $this->viewModel
            ->setVariable('title', 'Thank You')
            ->setVariable('message', '<br>Thank you for registering to ' . $backendName . '.<br>You will be able to login after you receive the activation email.');
    }

}