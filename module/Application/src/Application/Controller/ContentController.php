<?php

namespace Application\Controller;

use Application\Model\ActionModel;
use Application\Model\NotificationModel;
use Common\Helper\AclHelper;
use Common\Helper\LayoutHelper;

use Application\Model\ContentModel;
use Application\Model\MediaModel;

use Rest\Model\HpRestModel;
use Rest\Model\PageRestModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

class ContentController extends AbstractActionController
{

    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    /* @var $layoutHelper \Common\Helper\LayoutHelper */
    private $layoutHelper = null;

    private $baseUrl = "";

    public function onDispatch(MvcEvent $e)
    {
        $this->get_params = $this->getRequest()->getQuery();
        $this->post_params = $this->getRequest()->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();
        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->layoutHelper = new LayoutHelper($this->layout());
        $this->viewModel = new ViewModel();

        if (!$this->aclHelper->isLogged()) {
            return $this->redirect()->toRoute('home');
        } else {
            $this->layoutHelper->setGMapKey($this->getServiceLocator()->get('config')['google-maps']['key']);
            $this->layoutHelper->setUser($this->aclHelper->getUser());
            $this->layoutHelper->setApp($this->aclHelper->getApp());
            $this->layoutHelper->setAppList($this->aclHelper->getAppList());
        }

        $uri = $this->getRequest()->getUri();
        $scheme = $uri->getScheme();
        $host = $uri->getHost();
        $this->baseUrl = sprintf('%s://%s', $scheme, $host);

        return parent::onDispatch($e);
    }

    public function contentListAction()
    {
        $config = $this->getServiceLocator()->get('config');

        $this->layoutHelper->setTitle("CONTENT");
        $this->layoutHelper->setSubtitle("Manage your contents");

        $breadcrumbs = array("Dashboard" => "/", "Contents" => "/admin/content");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "content", $this->getServiceLocator());

        $contentModel = new ContentModel($this->getServiceLocator());

        return $this->viewModel
            ->setVariable('user', $this->aclHelper->getUser())
            ->setVariable('content_types', $contentModel->getContentTypes());
    }

    public function contentCreateAction()
    {
        $config = $this->getServiceLocator()->get('config');
        $contentType = $this->route_params->getParam('type');

        $baseLayout = "";
        $layoutList = $config['list_page_types'];
        foreach ($layoutList as $key => $value) {
            if ($baseLayout == "") {
                if (in_array(strtoupper($contentType), $value['types'])) {
                    $baseLayout = $key;
                }
            }
        }

        $layout = json_decode($this->aclHelper->getApp()->layout);
        $customLayoutTheme = $layout->theme;
        $customTheme = json_encode($customLayoutTheme);

        $contentModel = new ContentModel($this->getServiceLocator());

        if (!$contentModel->isValidType($contentType)) {
            return $this->redirect()->toRoute('home');
        }

        $contentParams = $contentModel->getByType($contentType);
        $contentTypes = $contentModel->getContentTypes($contentType);

        ContentModel::setSelectedType($contentType);

        $this->layoutHelper->setTitle("CONTENT");
        $this->layoutHelper->setSubtitle("Create new content");

        $breadcrumbs = array("Dashboard" => "/", "Contents" => "/admin/content", strtolower($contentParams["label"]) => "");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "content", $this->getServiceLocator());

        $form = $this->getServiceLocator()->get('Form\Content\Factory');

        $data = $contentModel->flatLayoutJson($this->aclHelper->getApp()->layout, $baseLayout, $this->aclHelper->getApp()->template_id);

        $application = $this->aclHelper->getApp();
        $systemLanguage = $config['system_languages'];
        $userLanguages[$application->main_contents_language] = $systemLanguage[$application->main_contents_language];
        $data['content_lang'] = $application->main_contents_language;

        $form->setData($data);

        $mediaModel = new MediaModel($this->getServiceLocator());

        if ($form->isValidPostRequest($this->request)) {
            if ($contentModel->createContent($form->getData())) {
                $mediaModel->setMediaRel($contentModel->getLastContentId(), $form->getData()['gallery_ids']);
                $this->redirect()->toRoute('contents');
            } else {
                $this->viewModel->setVariable('error_msg', "INTERNAL SERVER ERROR!");
            }
        } else {
            $media = $mediaModel->getMediasByIds($this->post_params['gallery_ids']);
            $this->viewModel
                ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
                ->setVariable('initialPreview', json_encode($media->initialPreview))
                ->setVariable('initialPreviewConfig', json_encode($media->initialPreviewConfig))
                ->setVariable('initialPreviewThumbTags', json_encode($media->initialPreviewThumbTags));
        }

        $addRemoteMediaForm = $this->getServiceLocator()->get('Form\Media\Factory');

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('content_types', $contentTypes)
            ->setVariable('content_params', $contentParams)
            ->setVariable('custom_theme', $customTheme)
            ->setVariable('addRemoteMediaForm', $addRemoteMediaForm)
            ->setVariable('currentLang', $application->main_contents_language)
            ->setVariable('userLanguages', $userLanguages)
            ->setVariable('form', $form);
    }

    public function contentUpdateAction()
    {
        $session = new Container('acl');
        $config = $this->getServiceLocator()->get('config');
        $contentType = $this->route_params->getParam('type');
        $contentId = $this->route_params->getParam('id');

        $contentModel = new ContentModel($this->getServiceLocator());
        $actionModel = new ActionModel($this->getServiceLocator());

        if (!$contentModel->isValidType($contentType)) {
            return $this->redirect()->toRoute('home');
        }

        $contentData = $contentModel->getContentById($contentType, $contentId);
        $contentParams = $contentModel->getByType($contentType);
        $contentTypes = $contentModel->getContentTypes($contentType);
        $contentActions = $actionModel->getContentActionList($contentId, $session->currentLang);

        $sectionLayout = array(
            'color' => array(
                'global_bg' => $contentData['global_bg'],
                'accent_bg' => $contentData['accent_bg'],
                'primary_bg' => $contentData['primary_bg'],
                'secondary_bg' => $contentData['secondary_bg'],
                'light_txt' => $contentData['light_txt'],
                'dark_txt' => $contentData['dark_txt'],
            ),
            'font' => array(
                'primary' => $contentData['primary_font'],
                'primary_size' => $contentData['primary_font_size'],
                'secondary' => $contentData['secondary_font'],
                'secondary_size' => $contentData['secondary_font_size'],
            )
        );

        $customTheme = json_encode($sectionLayout);

        $mediaModel = new MediaModel($this->getServiceLocator());
        $media = $mediaModel->getMediasByPostId($contentId);
        $contentData['gallery_ids'] = implode(",", $media->galleryIds);

        ContentModel::setSelectedType($contentType);

        $this->layoutHelper->setTitle("CONTENT");
        $this->layoutHelper->setSubtitle("Update your content");

        $breadcrumbs = array("Dashboard" => "/", "Contents" => "/admin/content", $contentData['title'] => "");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "content", $this->getServiceLocator());

        $form = $this->getServiceLocator()->get('Form\Content\Factory');

        $form->setData($contentData);

        if ($form->isValidPostRequest($this->request)) {
            $formData = $form->getData();

            $app = $this->aclHelper->getApp();
            $notificationModel = new NotificationModel($this, false);
            $notificationModel->setAppId($app->app_key);

            if ($contentModel->saveContent($contentId, $formData, $notificationModel)) {
                $mediaModel->setMediaRel($contentId, $form->getData()['gallery_ids']);
                $contentData = $contentModel->getContentById($contentType, $contentId);
                $form->setData($contentData);
                if ($formData['reload_content'] === "0") {
                    $this->redirect()->toRoute('contents');
                }
            } else {
                $this->viewModel->setVariable('error_msg', "INTERNAL SERVER ERROR!");
            }
        }

        $addRemoteMediaForm = $this->getServiceLocator()->get('Form\Media\Factory');

        $hashId = md5($contentId);

        $appUrl = $this->baseUrl . $this->url()->fromRoute("app_url/get",
                array(
                    "type" => strtolower(PageRestModel::CONTENT_CONTENT),
                    "hashId" => $hashId,
                )
            );

        $application = $this->aclHelper->getApp();
        $systemLanguage = $config['system_languages'];
        $userLanguages[$application->main_contents_language] = $systemLanguage[$application->main_contents_language];
        $otherContentsLanguages = json_decode($application->other_contents_languages);

        if ($otherContentsLanguages) {
            foreach ($otherContentsLanguages as $language) {
                $userLanguages[$language] = $systemLanguage[$language];
            }
        }

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('initialPreview', json_encode($media->initialPreview))
            ->setVariable('initialPreviewConfig', json_encode($media->initialPreviewConfig))
            ->setVariable('initialPreviewThumbTags', json_encode($media->initialPreviewThumbTags))
            ->setVariable('content_actions', $contentActions)
            ->setVariable('content_types', $contentTypes)
            ->setVariable('content_params', $contentParams)
            ->setVariable('custom_theme', $customTheme)
            ->setVariable('addRemoteMediaForm', $addRemoteMediaForm)
            ->setVariable('appUrl', $appUrl)
            ->setVariable('hashId', HpRestModel::CNT_HASH_PREFIX."-".$hashId)
            ->setVariable('currentLang', $session->currentLang)
            ->setVariable('userLanguages', $userLanguages)
            ->setVariable('form', $form);
    }

}
