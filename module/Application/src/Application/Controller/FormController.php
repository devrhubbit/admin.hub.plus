<?php
namespace Application\Controller;

use Application\Model\UserModel;
use Common\Helper\AclHelper;
use Common\Helper\LayoutHelper;

use Application\Model\ContentModel;
use Application\Model\MediaModel;
use Application\Model\SectionModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

class FormController extends AbstractActionController {

    /* @var $viewModel \Zend\View\Model\ViewModel */
	protected $viewModel;

	protected $get_params;
	protected $post_params;

    /* @var $route_params RouteMatch */
	protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    /* @var $layoutHelper \Common\Helper\LayoutHelper */
    private $layoutHelper = null;

	public function onDispatch(MvcEvent $e)
	{
        $this->get_params 	= $this->getRequest()->getQuery();
        $this->post_params 	= $this->getRequest()->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();
        $this->aclHelper    = new AclHelper($this->getServiceLocator());
        $this->layoutHelper = new LayoutHelper($this->layout());
        $this->viewModel    = new ViewModel();

        if(!$this->aclHelper->isLogged()) {
            return $this->redirect()->toRoute('home');
        } else {
            $this->layoutHelper->setGMapKey($this->getServiceLocator()->get('config')['google-maps']['key']);
            $this->layoutHelper->setUser($this->aclHelper->getUser());
            $this->layoutHelper->setApp($this->aclHelper->getApp());
            $this->layoutHelper->setAppList($this->aclHelper->getAppList());
        }

        return parent::onDispatch($e);
	}

    public function formListAction() {

        $config = $this->getServiceLocator()->get('config');

        $this->layoutHelper->setTitle("FORM");
        $this->layoutHelper->setSubtitle("Manage this form's users");

        $breadcrumbs = array("Dashboard" => "/", "Form" => "/admin/form");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $sectionId = $this->route_params->getParam('id');

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "section_".$sectionId, $this->getServiceLocator());

        $sectionModel = new SectionModel($this->getServiceLocator());
        $customFormData = $sectionModel->getCustomFormDataDetails($sectionId);

        return $this->viewModel
                    ->setVariable('sectionId', $sectionId)
                    ->setVariable('customFormTitle', $customFormData['title'])
                    ->setVariable('customFormDataDetails', $customFormData['details'])
                    ->setVariable('form', $this->aclHelper->getUser());
    }

    /**
     * TODO: manage GALLERY FIELD, ADDRESS FIELD and RELATED SELECT rules
     *
     * @return ViewModel
     */
    public function formRowEditAction() {
        $config = $this->getServiceLocator()->get('config');

        $this->layoutHelper->setTitle("FORM DATA");
        $this->layoutHelper->setSubtitle("Edit form's user data");

        $sectionId = $this->route_params->getParam('id');
        $rowId = $this->route_params->getParam('row_id');

        $breadcrumbs = array("Dashboard" => "/", "Form" => "/admin/form/$sectionId", "User data" => "/");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);


        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "section_".$sectionId, $this->getServiceLocator());

        /** @var \Form\CustomForm\Factory $formFactory */
        $formFactory = $this->getServiceLocator()->get('Form\CustomForm\Factory');
        $sectionModel = new SectionModel($this->getServiceLocator());
        $formParams = $sectionModel->getFormFields($sectionId, $rowId);

        $formFactory->setCreationOptions($formParams['fieldset']);

        $form = $formFactory->getForm();
        $form->setData($formParams['data']);

        if ($form->isValidPostRequest($this->request)) {
            $res = $sectionModel->setFormUserDataById($sectionId, $rowId, $form->getData());
            if ($res) {
                $this->viewModel->setVariable('success_msg', "Form data updated!");
            } else {
                $this->viewModel->setVariable('error_msg', "INTERNAL SERVER ERROR!");
            }
        } else {
            if ($this->request->isPost()) {
                $this->viewModel->setVariable('error_msg', "Invalid FORM data, errors below!");
            }
        }

        return $this->viewModel
            ->setVariable('sectionId', $sectionId)
            ->setVariable('rowId', $rowId)
            ->setVariable('form', $form);
    }
}