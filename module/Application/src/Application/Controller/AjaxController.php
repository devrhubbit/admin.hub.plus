<?php

namespace Application\Controller;

use Application\Model\ActionModel;
use Application\Model\NotificationModel;
use Common\Controller\HpAbstractActionController;
use Common\Helper\AclHelper;

use Application\Model\AppModel;
use Application\Model\ContentModel;
use Application\Model\SectionModel;
use Application\Model\TagModel;
use Application\Model\TemplateModel;
use Application\Model\UserModel;

use Common\Helper\ImageHelper;
use MessagesService\Exception\MessagesServicePushException;
use Propel\Runtime\Exception\PropelException;
use Rest\Exception\RestAppIdSecurityException;
use Zend\Http\Response;
use Zend\Http\Request;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Http\Headers;
use Zend\Http\Response\Stream;
use Zend\Mvc\Router\RouteMatch;

class AjaxController extends HpAbstractActionController
{
    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    //Array of action that can be executed without being logged
    private $whiteListAction = array(
        'checkSessionTimeout',
    );

    public function onDispatch(MvcEvent $e)
    {
        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->viewModel = new ViewModel();
        $this->viewModel->setTemplate('ajax/response');

        /** @var Request $request */
        $request = $this->getRequest();

        $this->get_params = $request->getQuery();
        $this->post_params = $request->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        $this->layout('layout/ajax');

        $action = $this->params('action');

        if (!$this->aclHelper->isLogged() && !in_array($action, $this->whiteListAction)) {
            die("FORBIDDEN ACCESS");
        }

        return parent::onDispatch($e);
    }

    public function setLanguageAction()
    {
        $locale = $this->route_params->getParam('locale');
        $config = $this->getServiceLocator()->get('config');
        $systemLanguage = $config['system_languages'];

        $this->aclHelper->setContentLang($locale, $systemLanguage);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse(true));
    }

    public function templateCarouselAction()
    {
        $this->viewModel->setTemplate('ajax/template-carousel');

        $templateModel = new TemplateModel($this->getServiceLocator());

        $template = $templateModel->getTemplate($this->route_params->getParam('id'));
        $views = json_decode($template->json_views);

        return $this->viewModel
            ->setVariable('root', AppModel::$TPL_ROOT)
            ->setVariable('views', $views);
    }

    public function getSectionLayoutsAlloverAction()
    {
        $old_layout = json_decode($this->get_params['old_layout']);
        $new_layout = json_decode($this->get_params['new_layout']);
        $template_id = $this->get_params['template_id'];

        $sectionModel = new SectionModel($this->getServiceLocator());
        $sections = $sectionModel->getSectionLayoutsAllover($template_id, $old_layout, $new_layout);

        $result = json_encode($sections);

        return $this->viewModel
            ->setVariable('content', $result);
    }

    public function setSectionLayoutsAlloverAction()
    {
        $new_layout = json_decode($this->get_params['new_layout']);
        $sections_id = $this->get_params['sections_id'];
        $template_id = $this->get_params['template_id'];

        $sectionModel = new SectionModel($this->getServiceLocator());
        $res = $sectionModel->setSectionLayoutsAllover($template_id, $new_layout, $sections_id);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res, 'Sections layout cascade updates'));
    }

    public function getContentLayoutsAlloverAction()
    {
        $old_layout = json_decode($this->get_params['old_layout']);
        $new_layout = json_decode($this->get_params['new_layout']);
        $template_id = $this->get_params['template_id'];

        $contentModel = new ContentModel($this->getServiceLocator());
        $contents = $contentModel->getContentLayoutsAllover($template_id, $old_layout, $new_layout);

        $result = json_encode($contents);

        return $this->viewModel
            ->setVariable('content', $result);
    }

    public function setContentLayoutsAlloverAction()
    {
        $new_layout = json_decode($this->get_params['new_layout']);
        $contents_id = $this->get_params['contents_id'];
        $template_id = $this->get_params['template_id'];

        $contentModel = new ContentModel($this->getServiceLocator());
        $res = $contentModel->setContentLayoutsAllover($template_id, $new_layout, $contents_id);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res, 'Contents layout cascade updates'));
    }

    public function deleteAppAction()
    {
        $appModel = new AppModel($this->getServiceLocator());

        try {
            $res = $appModel->deleteApp($this->route_params->getParam('id'));
            $message = null;
        } catch (PropelException $exception) {
            $res = false;
            $message = $exception->getMessage();
        }

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res, $message));
    }

    public function tagsContentListAction()
    {
        /** @var Response $response */
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        $cntModel = new TagModel($this->serviceLocator);
        $tags = $cntModel->getTags($this->get_params['term'], $this->route_params->getParam('type'));

        return $this->viewModel
            ->setVariable('content', json_encode($tags));
    }

    public function tagsUserListAction()
    {
        /** @var Response $response */
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        $cntModel = new TagModel($this->serviceLocator);
        $tags = $cntModel->getUserTag($this->get_params['term']);

        return $this->viewModel
            ->setVariable('content', json_encode($tags));
    }

    public function sectionsWeightAction()
    {
        $sctModel = new SectionModel($this->getServiceLocator());

        $rows = $this->post_params["data"];
        foreach ($rows as $row) {
            $sctModel->setSectionWeight($row["section"], $row["weight"]);
        }

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse(true));
    }

    public function sectionsCustomFormGroupAction()
    {
        $sctModel = new SectionModel($this->getServiceLocator());

        $group = $this->route_params->getParam('group');

        $res = $sctModel->createCustomFormGroup($group);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res));
    }

    public function sectionsCustomFormActualGroupListAction()
    {
        $sctModel = new SectionModel($this->getServiceLocator());

        $existingFieldsets = json_decode($this->get_params["existingFieldsets"]);

        $res = $sctModel->getActualSectionGroupList($existingFieldsets);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res));
    }

    public function sectionsCustomFormValidatorsAction()
    {
        $sctModel = new SectionModel($this->getServiceLocator());

        $type = $this->route_params->getParam('type');

        $res = $sctModel->getCustomFormValidatorsByType($type);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res));
    }

    public function sectionStatusAction()
    {
        $sctModel = new SectionModel($this->getServiceLocator());

        $isUniqueRegistration = null;
        $id = $this->route_params->getParam('id');
        $override = $this->post_params["override"];
        $status = $this->post_params["status"];
        $oldRegistrationId = $this->post_params["oldRegistrationId"];

        $app = $this->aclHelper->getApp();
        $notificationModel = new NotificationModel($this, false);
        try {
            $notificationModel->setAppId($app->app_key);
            $res = $sctModel->chooseActionStatus($id, $override, $status, $oldRegistrationId, $notificationModel);

            $this->viewModel
                ->setVariable('content', $this->getJsonResponse($res['result'], $res['unique']));
        } catch (RestAppIdSecurityException $exception) {
            $res = false;
            $this->viewModel
                ->setVariable('content', $this->getJsonResponse($res, $exception->getMessage()));
        } catch (MessagesServicePushException $exception) {
            $res = false;
            $this->viewModel
                ->setVariable('content', $this->getJsonResponse($res, $exception->getMessage()));
        }

        return $this->viewModel;
    }

    /**
     * TODO: manage delete error messagge if user tries to delete limited section (eg. we need almost one HEADER, ...)
     * @return mixed
     */
    public function sectionDeleteAction()
    {
        $sctModel = new SectionModel($this->getServiceLocator());

        $id = $this->route_params->getParam('id');

        $res = $sctModel->deleteSectionSwitch($id);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res));
    }

    public function userListAction()
    {
        $userModel = new UserModel($this->getServiceLocator());

        $formId = $this->route_params->getParam('id');
        $draw = (int)$this->get_params['draw'];
        $start = $this->get_params['start'];
        $length = $this->get_params['length'];
        $cols = $this->get_params['columns'];
        $order = $this->get_params['order'];
        $search = $this->get_params['search'];

        $data = $userModel->getUsers($formId, $draw, $start, $length, $cols, $order, $search);

        return $this->viewModel
            ->setVariable('content', $this->getDTJsonResponse($data));
    }

    public function formUsersListAction()
    {
        $sectionModel = new SectionModel($this->getServiceLocator());

        $formId = $this->route_params->getParam('id');
        $draw = (int)$this->get_params['draw'];
        $start = $this->get_params['start'];
        $length = $this->get_params['length'];
        $cols = $this->get_params['columns'];
        $order = $this->get_params['order'];
        $search = $this->get_params['search'];

        $data = $sectionModel->getFormUsersData($formId, $draw, $start, $length, $cols, $order, $search);

        return $this->viewModel
            ->setVariable('content', $this->getDTJsonResponse($data));
    }

    public function contentListAction()
    {
        $cntModel = new ContentModel($this->getServiceLocator());

        $draw = (int)$this->get_params['draw'];
        $start = $this->get_params['start'];
        $length = $this->get_params['length'];
        $cols = $this->get_params['columns'];
        $order = $this->get_params['order'];
        $search = $this->get_params['search'];

        $data = $cntModel->getContents($draw, $start, $length, $cols, $order, $search);

        return $this->viewModel
            ->setVariable('content', $this->getDTJsonResponse($data));
    }

    public function contentStatusAction()
    {
        $sctModel = new ContentModel($this->getServiceLocator());

        $id = $this->route_params->getParam('id');
        $status = (int)$this->post_params["status"];

        $app = $this->aclHelper->getApp();
        $notificationModel = new NotificationModel($this, false);
        try {
            $notificationModel->setAppId($app->app_key);
        } catch (RestAppIdSecurityException $exception) {
            $res = false;
            $this->viewModel->setVariable('content', $this->getJsonResponse($res, $exception->getMessage()));
        }

        try {
            $res = $sctModel->setContentStatus($id, $status, $notificationModel);
            $this->viewModel->setVariable('content', $this->getJsonResponse($res));
        } catch (PropelException $exception) {
            $res = false;
            $this->viewModel->setVariable('content', $this->getJsonResponse($res, $exception->getMessage()));
        } catch (MessagesServicePushException $exception) {
            $res = false;
            $this->viewModel->setVariable('content', $this->getJsonResponse($res, $exception->getMessage()));
        }

        return $this->viewModel;
    }

    /**
     * TODO: manage delete error messagge if user tries to delete limited section (eg. we need almost one HEADER, ...)
     * @return mixed
     */
    public function contentDeleteAction()
    {
        $sctModel = new ContentModel($this->getServiceLocator());

        $id = $this->route_params->getParam('id');
        $type = $this->route_params->getParam('type');

        $res = $sctModel->deleteContent($id, $type);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res));
    }

    public function checkSectionFieldContentAction()
    {
        $sectionModel = new SectionModel($this->getServiceLocator());

        $fieldLabel = base64_encode($this->get_params['fieldLabel']);
        $customFormId = $this->get_params['customFormId'];

        $res = $sectionModel->checkSectionFieldContent($fieldLabel, $customFormId);

        return $this->viewModel
            ->setVariable('content', $this->getDTJsonResponse($res));
//                    ->setVariable('content', $this->getJsonResponse($res));
    }

    public function paletteListAction()
    {
        $templateModel = new TemplateModel($this->getServiceLocator());

        $draw = (int)$this->get_params['draw'];
        $start = $this->get_params['start'];
        $length = $this->get_params['length'];
        $search = $this->get_params['search'];
        $shownCols = 3;

        $data = $templateModel->getPaletteList($draw, $start, $length, $search, $shownCols);

        return $this->viewModel
            ->setVariable('content', $this->getDTJsonResponse($data));
    }

    public function paletteThemeAction()
    {
        $templateModel = new TemplateModel($this->getServiceLocator());

        $id = $this->route_params->getParam('id');

        $data = $templateModel->getPaletteTheme($id);

        return $this->viewModel
            ->setVariable('content', $this->getDTJsonResponse($data));
    }

    public function checkSessionTimeoutAction()
    {
        $message = new \stdClass();
        $message->logged = $this->aclHelper->isLogged();
        $message->url = $this->getBaseurl();

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse(true, $message));
    }

    public function getCompositeImageAction()
    {
        $bgColor = $this->post_params['bg_color'];
        $imgResources = $this->post_params['resources'];
        $compositeImgSize = $this->post_params['img_size'];

        $imageHelper = new ImageHelper();
        $imageSize = null;
        if (is_array($compositeImgSize) &&
            array_key_exists('width', $compositeImgSize) &&
            array_key_exists('height', $compositeImgSize)
        ) {
            $imageSize = new \stdClass();
            $imageSize->width = $compositeImgSize['width'];
            $imageSize->height = $compositeImgSize['height'];
        }

        /** @var \Imagick $res */
        $res = $imageHelper->compositeImage($imgResources, $bgColor, $imageSize);

        $response = new Response();
        $response->setContent($res->getImageBlob());
        $response->setStatusCode(200);
        $headers = new Headers();
        $headers->addHeaders(array(
//            'Content-Description' => 'File Transfer',
//            'Content-Disposition' => 'attachment; filename="splash.jpg"',
            'Content-Type' => 'image/jpeg',
//            'Content-Transfer-Encoding' => 'binary',
            'Content-Length' => $res->getImageLength(),
            'Expires' => '@0', // @0, because zf2 parses date as string to \DateTime() object
            'Cache-Control' => 'public',
            'Pragma' => 'public'
        ));
        $response->setHeaders($headers);

        return $response;
    }

    public function getFileFromPathAction()
    {
        $path = $this->post_params['path'];

        $response = new Stream();
        $response->setStream(fopen($path, 'r'));
        $response->setStatusCode(200);
        $response->setStreamName(basename($path));
        $headers = new Headers();
        $headers->addHeaders(array(
            'Content-Disposition' => 'attachment; filename="' . basename($path) .'"',
            'Content-Type' => 'application/octet-stream',
            'Content-Length' => filesize($path),
            'Expires' => '@0', // @0, because zf2 parses date as string to \DateTime() object
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public'
        ));
        $response->setHeaders($headers);
        return $response;
    }

    public function actionsReorderAction()
    {
        $actionModel = new ActionModel($this->getServiceLocator());

        $data = $this->post_params['data'];

        $res = $actionModel->actionReorder($data);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res));
    }

    public function getSectionParamsAction()
    {
        $sectionModel = new SectionModel($this->getServiceLocator());
        $sectionId = $this->route_params->getParam('sectionId');

        $params = $sectionModel->getSectionParamsById($sectionId);

        return $this->viewModel
            ->setVariable('content', $this->getDTJsonResponse($params));
    }

    public function templatesListAction()
    {
        $templateModel = new TemplateModel($this->serviceLocator);

        $draw = (int)$this->get_params['draw'];
        $start = $this->get_params['start'];
        $length = $this->get_params['length'];
        $cols = $this->get_params['columns'];
        $order = $this->get_params['order'];
        $search = $this->get_params['search'];

        $data = $templateModel->getTemplates($draw, $start, $length, $cols, $order, $search);

        return $this->viewModel
            ->setVariable('content', $this->getDTJsonResponse($data));
    }

    public function templateDetailsAction()
    {
        $templateModel = new TemplateModel($this->serviceLocator);

        $templateId = $this->get_params['templateId'];

        $data = $templateModel->getTemplate($templateId);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($data));
    }

    public function templateDeleteAction()
    {
        $templateModel = new TemplateModel($this->serviceLocator);

        $templateId = $this->get_params['templateId'];

        $res = $templateModel->deleteTemplate($templateId);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res));
    }
}