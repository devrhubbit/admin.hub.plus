<?php

namespace Application\Controller;

use Application\Model\AppModel;
use Application\Model\BackendUsersModel;
use Application\Model\TemplateModel;
use Common\Helper\AclHelper;
use Common\Helper\ImageHelper;
use Common\Helper\LayoutHelper;

use Rest\Model\HpRestModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

class SuperadminController extends AbstractActionController
{
    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    private $cdnBasePath;
    private $cdnBaseUrl;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    /* @var $layoutHelper \Common\Helper\LayoutHelper */
    private $layoutHelper = null;

    private $config;

    public function onDispatch(MvcEvent $e)
    {
        $this->config = $this->getServiceLocator()->get('config');

        $this->viewModel = new ViewModel();
        $this->get_params = $this->getRequest()->getQuery();
        $this->post_params = $this->getRequest()->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        $config = $this->getServiceLocator()->get('config');
        $this->cdnBasePath = $config['cdn']['basepath'];
        $this->cdnBaseUrl = $config['cdn']['baseurl'];

        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->layoutHelper = new LayoutHelper($this->layout());
        $this->layoutHelper->setGMapKey($this->getServiceLocator()->get('config')['google-maps']['key']);

        if (!$this->aclHelper->isLogged() || $this->aclHelper->getUser()->role_id !== BackendUsersModel::SUPER_ADMIN_ROLE_ID) {
            $this->redirect()->toRoute('home');
        } else {
            $this->layoutHelper->setUser($this->aclHelper->getUser());
            $this->layoutHelper->setApp($this->aclHelper->getApp());
            $this->layoutHelper->setAppList($this->aclHelper->getAppList());
        }

        return parent::onDispatch($e);
    }

    public function backendUsersAction()
    {
        $this->layoutHelper->setTitle("BACKEND USERS");
        $this->layoutHelper->setSubtitle("Manage your users");

        $breadcrumbs = array("Dashboard" => "/", "Backend Users" => "/superadmin/backend-users");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $this->config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "backend_users", $this->getServiceLocator());

        /* @var \Form\Registration\RegistrationForm $registrationForm */
        $registrationForm = $this->getServiceLocator()->get('Form\Registration\Factory');

        if ($this->request->isPost()) {
            if ($registrationForm->isValidPostRequest($this->request)) {
                $email = $this->post_params['email'];
                $password = $this->post_params['password'];
                $firstName = $this->post_params['first-name'];
                $lastName = $this->post_params['last-name'];

                $BackendUsersModel = new BackendUsersModel($this->getServiceLocator());

                $userId = $BackendUsersModel->registerUser($email, $password, "yes", $firstName, $lastName);

                if ($userId === null) {
                    $this->viewModel
                        ->setVariable('error_msg', "Registration failed, please try again");
                } else {
                    $appModel = new AppModel($this->getServiceLocator());
                    $appID = $appModel->getApplicationId();
                    $appModel->AddUserAppRel($appID, $userId);
                    $BackendUsersModel->sendRegistrationEmail($email, $password, $firstName, $lastName);
                }
            } else {
                $this->viewModel
                    ->setVariable('error_msg', "Registration failed, insert required fields (min password length 8 chars)");
            }
        }


        return $this->viewModel
            ->setVariable('user', $this->aclHelper->getUser())
            ->setVariable('registrationForm', $registrationForm);
    }

    public function customerLogAsAction()
    {
        $aclHelper = new AclHelper($this->serviceLocator);

        $customerId = $this->route_params->getParam('id');
        if ($customerId != null) {
            $res = $aclHelper->logAs($customerId);
            if ($res) $this->redirect()->toRoute('admin');
        }
    }

    public function appSettingsAction()
    {
        $this->layoutHelper->setTitle("APPLICATION SETTINGS");
        $this->layoutHelper->setSubtitle("Manage application");

        $breadcrumbs = array("Dashboard" => "/", "Templates" => "/superadmin/app-settings");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $this->config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "app-settings", $this->getServiceLocator());

        /* @var \Form\SuperAdminSettings\SettingsForm $settingsForm */
        $settingsForm = $this->getServiceLocator()->get('Form\SuperAdminSettings\Factory');

        $appModel = new AppModel($this->getServiceLocator());
        $contentsNumberBySuperAdmin = $appModel->getContentsNumberInsertedBySuperAdmin();
        $appId = $this->aclHelper->getApp()->id;

        $restModel = new HpRestModel($this, false);

        if (!is_null($appId)) {
            $app = $appModel->getApplicationByAppId($appId);
            $appData = array(
                'max-push-notification' => $app->max_notification,
                'sent-push-notification' => $restModel->getPushNotificationSent(),
                'max-advice-hours' => $app->max_advice_hour,
                'advice-hours-done' => $app->advice_hour_done,
                'max-insert-contents' => $app->max_content_insert,
            );

            $settingsForm->setData($appData);

            $this->viewModel->setVariable('app_id', $appId);
        }

        if ($settingsForm->isValidPostRequest($this->request)) {

            if (is_null($appId)) {
                $this->viewModel->setVariable('error_msg', 'No App ID in session. Try to login again.');
            } else {
                $appModel->updateSuperAdminSettings($appId, $this->post_params);
            }


//            $this->viewModel->setVariable('error_msg', $msg);
        }

        return $this->viewModel
            ->setVariable('form', $settingsForm)
            ->setVariable('contents_number', $contentsNumberBySuperAdmin);
    }
}