<?php

namespace Application\Controller;

use Application\Model\AnalyticsModel;
use Common\Helper\AclHelper;
use Common\Helper\LayoutHelper;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Router\RouteMatch;

class AnalyticsDataController extends AbstractActionController {

    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    /* @var $layoutHelper \Common\Helper\LayoutHelper */
    private $layoutHelper = null;

    public function onDispatch(MvcEvent $e)
    {
        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->viewModel = new ViewModel();
        $this->viewModel->setTemplate('ajax/response');

        $this->get_params 	= $this->getRequest()->getQuery();
        $this->post_params 	= $this->getRequest()->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        $this->layout('layout/ajax');

        if(!$this->aclHelper->isLogged()) {
            die("FORBIDDEN ACCESS");
        }

        return parent::onDispatch($e);
    }

    private function getJsonResponse($success, $message = null) {
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        $response = new \StdClass();

        $response->success = $success;
        $response->message = $message;

        return json_encode($response);
    }

    private function getDTJsonResponse($data) {
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        return json_encode($data);
    }

    public function deviceDataAction() {
        $from = $this->get_params['from'];
        $to   = $this->get_params['to'];

        $os   = json_decode($this->get_params['os']);
        $type = json_decode($this->get_params['type']);

        $analyticsModel = new AnalyticsModel($this->getServiceLocator());
        $data = $analyticsModel->getDeviceData($os, $type, $from, $to);

        return $this->viewModel
                    ->setVariable('content', $this->getDTJsonResponse($data));
    }

    public function postsDataAction() {
        $from = $this->get_params['from'];
        $to   = $this->get_params['to'];

        $posts = get_object_vars(json_decode($this->get_params['posts']));

        $analyticsModel = new AnalyticsModel($this->getServiceLocator());
        $data = $analyticsModel->getPostsData($posts, $from, $to);

        return $this->viewModel
                    ->setVariable('content', $this->getDTJsonResponse($data));
    }



}