<?php

namespace Application\Controller;

use Application\Model\BackendUsersModel;
use Application\Model\TemplateModel;
use Common\Controller\HpAbstractActionController;
use Common\Helper\AclHelper;

use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Http\PhpEnvironment\Response;

class SuperadminDataController extends HpAbstractActionController
{

    protected $viewModel;
    protected $get_params;
    protected $post_params;
    protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    public function onDispatch(MvcEvent $e)
    {

        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->viewModel = new ViewModel();
        $this->viewModel->setTemplate('ajax/response');

        $this->get_params = $this->getRequest()->getQuery();
        $this->post_params = $this->getRequest()->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        $this->layout('layout/ajax');

        /** @var Response $response */
        $response = $this->getResponse();

        $notValidRequest = false;
        $notValidMessage = "";
        if (!$this->aclHelper->isLogged()) {
            $notValidRequest = true;
            $notValidMessage = "Unauthorized";
            $response->setStatusCode(401);
        } elseif ($this->aclHelper->getUser()->role_id !== BackendUsersModel::SUPER_ADMIN_ROLE_ID) {
            $notValidRequest = true;
            $notValidMessage = "Forbidden";
            $response->setStatusCode(403);
        }

        if ($notValidRequest) {
            parent::onDispatch($e);
            return $this->viewModel
                ->setVariable('content', $this->getJsonResponse(false, $notValidMessage));
        }

        return parent::onDispatch($e);
    }

    public function backendUsersListAction()
    {
        $BackendUsersModel = new BackendUsersModel($this->serviceLocator);

        $draw = (int)$this->get_params['draw'];
        $start = $this->get_params['start'];
        $length = $this->get_params['length'];
        $cols = $this->get_params['columns'];
        $order = $this->get_params['order'];
        $search = $this->get_params['search'];

        $data = $BackendUsersModel->getBackendUsers($draw, $start, $length, $cols, $order, $search);

        return $this->viewModel
            ->setVariable('content', $this->getDTJsonResponse($data));
    }

    public function backendUserChangeRoleAction()
    {
        $BackendUsersModel = new BackendUsersModel($this->serviceLocator);

        $customerId = $this->get_params['customerId'];
        $newRole = $this->get_params['newRole'];

        $res = $BackendUsersModel->changeBackendUserRole($customerId, $newRole);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res));
    }

    public function deleteBackendUserAction()
    {
        $BackendUsersModel = new BackendUsersModel($this->getServiceLocator());
        $backendUserId = $this->route_params->getParam('id');
        $res = $BackendUsersModel->deleteUser($backendUserId);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res['success'], $res['message']));
    }

    public function lockUnlockBackendUserAction()
    {
        $BackendUsersModel = new BackendUsersModel($this->getServiceLocator());
        $backendUserId = $this->route_params->getParam('id');
        $backendUserLockValue = $this->route_params->getParam('lock');
        $res = $BackendUsersModel->lockUnlockUser($backendUserId, $backendUserLockValue);

        return $this->viewModel
            ->setVariable('content', $this->getJsonResponse($res['success'], $res['message']));
    }
}