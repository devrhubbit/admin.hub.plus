<?php

namespace Application\Controller;

use Application\Model\SectionModel;
use Application\Model\UserModel;
use Common\Helper\AclHelper;
use Common\Helper\LayoutHelper;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;


class UserController extends AbstractActionController
{

    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    /* @var $layoutHelper \Common\Helper\LayoutHelper */
    private $layoutHelper = null;

    public function onDispatch(MvcEvent $e)
    {
        $this->get_params = $this->getRequest()->getQuery();
        $this->post_params = $this->getRequest()->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();
        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->layoutHelper = new LayoutHelper($this->layout());
        $this->viewModel = new ViewModel();

        if (!$this->aclHelper->isLogged()) {
            return $this->redirect()->toRoute('home');
        } else {
            $this->layoutHelper->setGMapKey($this->getServiceLocator()->get('config')['google-maps']['key']);
            $this->layoutHelper->setUser($this->aclHelper->getUser());
            $this->layoutHelper->setApp($this->aclHelper->getApp());
            $this->layoutHelper->setAppList($this->aclHelper->getAppList());
        }

        return parent::onDispatch($e);
    }

    public function usersListAction()
    {
        $config = $this->getServiceLocator()->get('config');

        $this->layoutHelper->setTitle("Users");
        $this->layoutHelper->setSubtitle("Application Users List");

        $breadcrumbs = array("Dashboard" => "/", "Users" => "/admin/user");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "dashboard", $this->getServiceLocator());

        $sectionModel = new SectionModel($this->serviceLocator);
        $section = $sectionModel->getRegistrationForm();
        $customFormData = null;
        $sectionId = null;
        if ($section !== null) {
            $sectionId = $section->getId();
            $customFormData = $sectionModel->getCustomFormDataDetails($sectionId);
        }

        return $this->viewModel
            ->setVariable('customFormData', $customFormData)
            ->setVariable('formId', $sectionId);

    }

    public function restoreTagsAction()
    {
        $this->layout('layout/ajax');

        $userModel = new UserModel($this->serviceLocator);
        $userModel->restoreTags();

        return $this->viewModel->setTemplate('ajax/response');
    }
}
