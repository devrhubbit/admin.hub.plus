<?php

namespace Application\Controller;

use Common\Helper\AclHelper;
use Common\Helper\LayoutHelper;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

use Zend\Http\Request;

class AnalyticsController extends AbstractActionController {

    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    /* @var $layoutHelper \Common\Helper\LayoutHelper */
    private $layoutHelper = null;

    public function onDispatch(MvcEvent $e) {

        $this->viewModel = new ViewModel();

        /** @var ViewModel $viewModelForHelper */
        $viewModelForHelper = $this->layout();

        /** @var Request $request */
        $request = $this->getRequest();

        $this->get_params 	= $request->getQuery();
        $this->post_params 	= $request->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->layoutHelper = new LayoutHelper($viewModelForHelper);
        $this->layoutHelper->setGMapKey($this->getServiceLocator()->get('config')['google-maps']['key']);

        if(!$this->aclHelper->isLogged()) {
            $this->redirect()->toRoute('home');
        } else {
            $this->layoutHelper->setUser($this->aclHelper->getUser());
            $this->layoutHelper->setApp($this->aclHelper->getApp());
            $this->layoutHelper->setAppList($this->aclHelper->getAppList());
        }

        return parent::onDispatch($e);
    }

    public function analyticsListAction() {
        $config = $this->getServiceLocator()->get('config');

        $this->layoutHelper->setTitle("ANALYTICS");
        $this->layoutHelper->setSubtitle("View your stats");

        $breadcrumbs = array("Dashboard" => "#", "Analytics" => "/admin/analytics");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "analytics", $this->getServiceLocator());
    }

    public function deviceDataDetailsAction() {
        $config = $this->getServiceLocator()->get('config');

        $this->layoutHelper->setTitle("ANALYTICS");
        $this->layoutHelper->setSubtitle("Device data details");

        $breadcrumbs = array("Dashboard" => "#", "Analytics" => "/admin/analytics", "Device Data" => "/ajax/analytics/device/data/details");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "analytics", $this->getServiceLocator());
    }

    public function postsDataDetailsAction() {
        $config = $this->getServiceLocator()->get('config');

        $this->layoutHelper->setTitle("ANALYTICS");
        $this->layoutHelper->setSubtitle("Content data details");

        $breadcrumbs = array("Dashboard" => "#", "Analytics" => "/admin/analytics", "Posts Data" => "/ajax/analytics/posts/data/details");
        $this->layoutHelper->setBreadcrumbs($breadcrumbs);

        $menu = $config["menu"]["admin"];
        $this->layoutHelper->setMenu($menu, true, "analytics", $this->getServiceLocator());
    }
}