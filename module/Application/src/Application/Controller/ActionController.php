<?php

namespace Application\Controller;

use Application\Model\ActionModel;
use Application\Model\ContentModel;
use Application\Model\MediaModel;
use Application\Model\SectionModel;
use Common\Helper\AclHelper;
use Form\Action\ActionForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Http\Response;
use Zend\Http\Request;

class ActionController extends AbstractActionController
{
    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    public function onDispatch(MvcEvent $e)
    {
        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->viewModel = new ViewModel();

        /** @var Request $request */
        $request = $this->getRequest();

        $this->get_params = $request->getQuery();
        $this->post_params = $request->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        $this->layout('layout/ajax');

        if (!$this->aclHelper->isLogged()) {
            die("FORBIDDEN ACCESS");
        }

        return parent::onDispatch($e);
    }

    private function getJsonResponse($success, $message = null)
    {
        /** @var Response $response */
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        $response = new \StdClass();

        $response->success = $success;
        $response->message = $message;

        return json_encode($response);
    }

    public function attachmentModalAction()
    {
        $actionData = $this->post_params['jsonExtra'];
        $actionForm = $this->getActionForm(ActionForm::ATTACHMENT_ACTION);
        $this->viewModel->setTemplate('action/response');

        $mediaId = "";

        if ($actionData != "" && $actionData != null) {
            $decodedParams = json_decode($actionData['params']);
            $action = array();
            $action = $this->setCommonFieldsForm($actionData, $action);
            $action['action_attachment_media_id'] = $mediaId = $decodedParams->media_id;
            $action['action_attachment_media_url'] = $decodedParams->url;
            $action['action_attachment_media_mime_type'] = $decodedParams->mime_type;

            $actionForm->setData($action);
        }

        $mediaModel = new MediaModel($this->getServiceLocator());
        $media = $mediaModel->getAttachmentById($mediaId);

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('initialPreview', json_encode($media->initialPreview))
            ->setVariable('initialPreviewConfig', json_encode($media->initialPreviewConfig))
            ->setVariable('initialPreviewThumbTags', json_encode($media->initialPreviewThumbTags))
            ->setVariable('actionForm', $actionForm);
    }

    public function attachmentValidatorAction()
    {
        /* @var $actionForm \Form\Action\ActionForm */
        $actionForm = $this->getActionForm(ActionForm::ATTACHMENT_ACTION);
        $validation = $this->validateActionData($actionForm);

        return $validation;
    }

    public function calendarModalAction()
    {
        $actionData = $this->post_params['jsonExtra'];
        $actionForm = $this->getActionForm(ActionForm::CALENDAR_ACTION);
        $this->viewModel->setTemplate('action/response');

        $mediaModel = new MediaModel($this->getServiceLocator());

        if ($actionData != "" && $actionData != null) {
            $decodedParams = json_decode($actionData['params']);
            $action = array();
            $action = $this->setCommonFieldsForm($actionData, $action);
            $action['action_start_date'] = $decodedParams->from;
            $action['action_end_date'] = $decodedParams->to;

            $actionForm->setData($action);
        }

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('actionForm', $actionForm);
    }

    public function calendarValidatorAction()
    {
        /* @var $actionForm \Form\Action\ActionForm */
        $actionForm = $this->getActionForm(ActionForm::CALENDAR_ACTION);
        $validation = $this->validateActionData($actionForm);

        return $validation;
    }

    public function contentModalAction()
    {
        $actionData = $this->post_params['jsonExtra'];
        $actionForm = $this->getActionForm(ActionForm::CONTENT_ACTION);
        $this->viewModel->setTemplate('action/response');

        $mediaModel = new MediaModel($this->getServiceLocator());

        $contentModel = new ContentModel($this->getServiceLocator());
        $contentList = $contentModel->publishedContentsList();
        $actionForm->setContentList($contentList);

        if ($actionData != "" && $actionData != null) {
            $decodedParams = json_decode($actionData['params']);
            $action = array();
            $action = $this->setCommonFieldsForm($actionData, $action);
            $action['action_content_list'] = $decodedParams->content;

            $actionForm->setData($action);
        }

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('actionForm', $actionForm);
    }

    public function contentValidatorAction()
    {
        /* @var $actionForm \Form\Action\ActionForm */
        $actionForm = $this->getActionForm(ActionForm::CONTENT_ACTION);
        $validation = $this->validateActionData($actionForm);

        return $validation;
    }

    public function emailModalAction()
    {
        $actionData = $this->post_params['jsonExtra'];
        $actionForm = $this->getActionForm(ActionForm::EMAIL_ACTION);
        $this->viewModel->setTemplate('action/response');

        $mediaModel = new MediaModel($this->getServiceLocator());

        $action = array();
        if ($actionData != "" && $actionData != null) {
            $decodedParams = json_decode($actionData['params']);
            $action = $this->setCommonFieldsForm($actionData, $action);
            $action['action_email'] = $decodedParams->address;
            $action['action_email_subject'] = $decodedParams->subject;
            $action['action_email_placeholder'] = $decodedParams->placeholder;
        } else {
            // get logged user
            $user = $this->aclHelper->getUser();
            $action['action_email'] = $user->email;
        }

        $actionForm->setData($action);

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('actionForm', $actionForm);
    }

    public function emailValidatorAction()
    {
        /* @var $actionForm \Form\Action\ActionForm */
        $actionForm = $this->getActionForm(ActionForm::EMAIL_ACTION);
        $validation = $this->validateActionData($actionForm);

        return $validation;
    }

    public function linkModalAction()
    {
        $actionData = $this->post_params['jsonExtra'];
        $actionForm = $this->getActionForm(ActionForm::LINK_ACTION);
        $this->viewModel->setTemplate('action/response');

        $mediaModel = new MediaModel($this->getServiceLocator());

        if ($actionData != "" && $actionData != null) {
            $decodedParams = json_decode($actionData['params']);
            $action = array();
            $action = $this->setCommonFieldsForm($actionData, $action);
            $action['action_url'] = $decodedParams->url;
            $action['action_open_in_app'] = $decodedParams->open_in_app;
            $action['action_custom_css'] = $decodedParams->custom_css;
            $action['action_custom_js'] = $decodedParams->custom_js;

            $actionForm->setData($action);
        }

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('actionForm', $actionForm);
    }

    public function linkValidatorAction()
    {
        /* @var $actionForm \Form\Action\ActionForm */
        $actionForm = $this->getActionForm(ActionForm::LINK_ACTION);
        $validation = $this->validateActionData($actionForm);

        return $validation;
    }

    public function mapModalAction()
    {
        $actionData = $this->post_params['jsonExtra'];
        $actionForm = $this->getActionForm(ActionForm::MAP_ACTION);
        $this->viewModel->setTemplate('action/response');

        $mediaModel = new MediaModel($this->getServiceLocator());

        if ($actionData != "" && $actionData != null) {
            $decodedParams = json_decode($actionData['params']);
            $action = array();
            $action = $this->setCommonFieldsForm($actionData, $action);
            $action['action_address'] = $decodedParams->address;
            $action['action_street'] = $decodedParams->street;
            $action['action_city'] = $decodedParams->city;
            $action['action_state'] = $decodedParams->state;
            $action['action_zip'] = $decodedParams->zipcode;
            $action['action_country'] = $decodedParams->country;
            $action['action_latitude'] = $decodedParams->latitude;
            $action['action_longitude'] = $decodedParams->longitude;

            $actionForm->setData($action);
        }

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('actionForm', $actionForm);
    }

    public function mapValidatorAction()
    {
        /* @var $actionForm \Form\Action\ActionForm */
        $actionForm = $this->getActionForm(ActionForm::MAP_ACTION);
        $validation = $this->validateActionData($actionForm);

        return $validation;
    }

    public function phoneModalAction()
    {
        $actionData = $this->post_params['jsonExtra'];
        $actionForm = $this->getActionForm(ActionForm::PHONE_ACTION);
        $this->viewModel->setTemplate('action/response');

        $mediaModel = new MediaModel($this->getServiceLocator());

        if ($actionData != "" && $actionData != null) {
            $decodedParams = json_decode($actionData['params']);
            $action = array();
            $action = $this->setCommonFieldsForm($actionData, $action);
            $action['action_prefix'] = $decodedParams->prefix;
            $action['action_number'] = $decodedParams->number;
            $action['action_operation'] = $decodedParams->mode;
            $action['action_phone_placeholder'] = $decodedParams->placeholder;

            $actionForm->setData($action);
        }

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('actionForm', $actionForm);
    }

    public function phoneValidatorAction()
    {
        /* @var $actionForm \Form\Action\ActionForm */
        $actionForm = $this->getActionForm(ActionForm::PHONE_ACTION);
        $validation = $this->validateActionData($actionForm);

        return $validation;
    }

    public function sectionModalAction()
    {
        $actionData = $this->post_params['jsonExtra'];
        $actionForm = $this->getActionForm(ActionForm::SECTION_ACTION);
        $this->viewModel->setTemplate('action/response');

        $mediaModel = new MediaModel($this->getServiceLocator());

        $sectionModel = new SectionModel($this->getServiceLocator());
        $sectionList = $sectionModel->publishedSectionsList();
        $actionForm->setSectionList($sectionList);

        if ($actionData != "" && $actionData != null) {
            $decodedParams = json_decode($actionData['params']);
            $action = array();
            $action = $this->setCommonFieldsForm($actionData, $action);
            $action['action_section_list'] = $decodedParams->section;

            $actionForm->setData($action);
        }

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('actionForm', $actionForm);
    }

    public function sectionValidatorAction()
    {
        /* @var $actionForm \Form\Action\ActionForm */
        $actionForm = $this->getActionForm(ActionForm::SECTION_ACTION);
        $validation = $this->validateActionData($actionForm);

        return $validation;
    }

    private function getActionForm($action)
    {
        ActionModel::setSelectedType($action);
        $actionForm = $this->getServiceLocator()->get('Form\Action\Factory');

        return $actionForm;
    }

    private function validateActionData(ActionForm $form)
    {
        $success = true;
        $messages = null;

        if (!$form->isValidPostRequest($this->request)) {
            $success = false;
            $messages = $form->getMessages();
        }

        $this->viewModel->setTemplate('action/validate');

        return $this->viewModel->setVariable('content', $this->getJsonResponse($success, $messages));
    }

    /**
     * @param $actionData
     * @param $action
     * @return mixed
     */
    private function setCommonFieldsForm($actionData, $action)
    {
        $action['action_label'] = $actionData['label'];
        $action['action_area'] = $actionData['area'];
        $action['action_icon'] = $actionData['icon_uri'];
        $action['action_icon_id'] = $actionData['icon_id'];

        return $action;
    }
}