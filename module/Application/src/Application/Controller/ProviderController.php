<?php

namespace Application\Controller;

use Application\Model\ProviderModel;
use Common\Helper\AclHelper;
use Common\Helper\LayoutHelper;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

class ProviderController extends AbstractActionController {

    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    protected $get_params;
    protected $post_params;

    /* @var $route_params RouteMatch */
    protected $route_params;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    /* @var $layoutHelper \Common\Helper\LayoutHelper */
    private $layoutHelper = null;

    public function onDispatch(MvcEvent $e)
    {
        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->viewModel = new ViewModel();
        $this->viewModel->setTemplate('ajax/response');

        $this->get_params 	= $this->getRequest()->getQuery();
        $this->post_params 	= $this->getRequest()->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        $this->layout('layout/ajax');

        if(!$this->aclHelper->isLogged()) {
            die("FORBIDDEN ACCESS");
        }

        return parent::onDispatch($e);
    }

    private function getJsonResponse($success, $message = null) {
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        $response = new \StdClass();

        $response->success = $success;
        $response->message = $message;

        return json_encode($response);
    }

    private function getDTJsonResponse($data) {
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        return json_encode($data);
    }

    public function getProviderAction() {
        $providerModel = new ProviderModel($this->getServiceLocator());

        $id = $this->route_params->getParam('id');
        $data = $providerModel->getProvider($id);

        return $this->viewModel
                    ->setVariable('content', $this->getDTJsonResponse($data));
    }

    public function providerListAction() {
        $providerModel = new ProviderModel($this->getServiceLocator());

        $draw   = (int) $this->get_params['draw'];
        $start  = $this->get_params['start'];
        $length = $this->get_params['length'];
        $cols   = $this->get_params['columns'];
        $order  = $this->get_params['order'];
        $search = $this->get_params['search'];

        $data = $providerModel->getProviderList($draw, $start, $length, $cols, $order, $search);

        return $this->viewModel
                    ->setVariable('content', $this->getDTJsonResponse($data));
    }

    public function createProviderAction() {
        $providerModel = new ProviderModel($this->getServiceLocator());

        $name  = $this->get_params['name'];
        $site  = $this->get_params['site'];
        $email = $this->get_params['email'];

        $res = $providerModel->createProvider($name, $site, $email);

        return $this->viewModel
                    ->setVariable('content', $this->getDTJsonResponse($res));
    }

    public function updateProviderAction() {
        $providerModel = new ProviderModel($this->getServiceLocator());

        $id    = $this->route_params->getParam('id');
        $name  = $this->get_params['name'];
        $site  = $this->get_params['site'];
        $email = $this->get_params['email'];

        $res = $providerModel->updateProvider($id, $name, $site, $email);

        return $this->viewModel
                    ->setVariable('content', $this->getDTJsonResponse($res));
    }

//    public function createProviderAction() {
//        $providerModel = new ProviderModel($this->getServiceLocator());
//
//        $name  = $this->get_params['name'];
//        $site  = $this->get_params['site'];
//        $email = $this->get_params['email'];
//
//
//        $form = $this->getServiceLocator()->get('Form\Provider\Factory');
//
//        if ($form->isValidPostRequest($this->request)) {
//            var_dump('create si'); die();
//            $res = $providerModel->createProvider($name, $site, $email);
//        } else {
//            var_dump('create no'); die();
//            $res = false;
//        }
//
//        return $this->viewModel
//                    ->setVariable('content', $this->getDTJsonResponse($res));
//    }
//
//    public function updateProviderAction() {
//        $providerModel = new ProviderModel($this->getServiceLocator());
//
//        $id    = $this->route_params->getParam('id');
//        $name  = $this->get_params['name'];
//        $site  = $this->get_params['site'];
//        $email = $this->get_params['email'];
//
//
//
//        $form = $this->getServiceLocator()->get('Form\Provider\Factory');
//
//        if ($form->isValidPostRequest($this->request)) {
//            var_dump('update si'); die();
//            $res = $providerModel->updateProvider($id, $name, $site, $email);
//        } else {
//            var_dump('update no'); die();
//            $res = false;
//        }
//
//        return $this->viewModel
//                    ->setVariable('content', $this->getDTJsonResponse($res));
//    }

    public function deleteProviderAction() {
        $providerModel = new ProviderModel($this->getServiceLocator());

        $id = $this->route_params->getParam('id');

        $res = $providerModel->deleteProvider($id);

        return $this->viewModel
                    ->setVariable('content', $this->getJsonResponse($res));
    }

}