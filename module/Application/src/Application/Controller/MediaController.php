<?php

namespace Application\Controller;

use Common\Helper\AclHelper;
use Common\Helper\ImageHelper;
use Common\Helper\MediaHelper;

use Application\Model\BackendUsersModel;
use Application\Model\MediaModel;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

class MediaController extends AbstractActionController
{
    /* @var $viewModel \Zend\View\Model\ViewModel */
    protected $viewModel;

    /* @var $aclHelper \Common\Helper\AclHelper */
    private $aclHelper = null;

    /* @var $imgHelper \Common\Helper\ImageHelper */
    private $imgHelper = null;

    /** @var $mediaHelper \Common\Helper\MediaHelper */
    private $mediaHelper = null;

    private $cdnBasePath;
    private $cdnBaseUrl;

    /** @var \Zend\Http\PhpEnvironment\Request $request */
    protected $request;

    /** @var  $post_params \Zend\Stdlib\Parameters */
    protected $get_params;

    /** @var  $post_params \Zend\Stdlib\Parameters */
    protected $post_params;

    /* @var $route_params \Zend\Mvc\Router\RouteMatch */
    protected $route_params;

    public function onDispatch(MvcEvent $e)
    {
        $this->aclHelper = new AclHelper($this->getServiceLocator());
        $this->imgHelper = new ImageHelper();
        $this->mediaHelper = new MediaHelper();
        $this->viewModel = new ViewModel();

        $this->viewModel->setTemplate('attachment/response');

        $this->request = $this->getRequest();

        $this->get_params = $this->request->getQuery();
        $this->post_params = $this->request->getPost();
        $this->route_params = $this->getEvent()->getRouteMatch();

        $this->layout('layout/attachment');

        $config = $this->getServiceLocator()->get('config');
        $this->cdnBasePath = $config['cdn']['basepath'];
        $this->cdnBaseUrl = $config['cdn']['baseurl'];

        return parent::onDispatch($e);
    }

    private function getJsonResponse($success, $message = null)
    {
        /** @var \Zend\Http\PhpEnvironment\Response $response */
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/json');

        $res = new \StdClass();
        if (!$success) {
            $response->setStatusCode(500);
            $res->error = $message;
        } else {
            $response->setStatusCode(200);
        }


        $res->success = $success;
        $res->message = $message;

        return json_encode($res);
    }

    public function modalArchiveAction()
    {
        $this->viewModel->setTemplate('attachment/archive');
        $mediaIdExcluded = $this->get_params['ids'];

        $mediaModel = new MediaModel($this->getServiceLocator());
        $media = $mediaModel->getMedias($mediaIdExcluded);

        return $this->viewModel
            ->setVariable('maxFileSize', $mediaModel->returnMaxSize())
            ->setVariable('initialPreview', json_encode($media->initialPreview))
            ->setVariable('initialPreviewConfig', json_encode($media->initialPreviewConfig))
            ->setVariable('initialPreviewThumbTags', json_encode($media->initialPreviewThumbTags));
    }

    public function infoArchiveAction()
    {
        $id = $this->post_params['id'];
        $title = $this->post_params['title'];
        $description = $this->post_params['description'];

        $mediaModel = new MediaModel($this->getServiceLocator());

        $response = null;
        if ($mediaModel->setInfo($id, $title, $description)) {
            $response = $this->getJsonResponse(true);
        } else {
            $response = $this->getJsonResponse(false, "ERROR: Something is wrong!");
        }

        return $this->viewModel->setVariable('response', $response);
    }

    public function deleteArchiveAction()
    {
        $id = $this->post_params['key'];

        $mediaModel = new MediaModel($this->getServiceLocator());

        $response = null;
        if ($mediaModel->delete($id)) {
            $response = $this->getJsonResponse(true);
        } else {

            $response = $this->getJsonResponse(false, "ERROR: You can not remove this media because it is used by a content!");
        }

        return $this->viewModel->setVariable('response', $response);
    }

    public function unlinkPostAction()
    {
        $response = $this->getJsonResponse(true);

        return $this->viewModel->setVariable('response', $response);
    }

    public function addAttachmentMediaAction()
    {
        $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
        $baseUrl = $this->cdnBaseUrl . MediaHelper::getMediaUrl();
        $basePathUrl = $this->cdnBasePath . MediaHelper::getMediaUrl();
        $deleteUlr = $renderer->basePath("/media/archive/delete/");

        $type = $_FILES['action_upload_file']['type'];
        $tmp_name = $_FILES['action_upload_file']['tmp_name'];
        $size = $_FILES["action_upload_file"]["size"];
        $name = $_FILES["action_upload_file"]["name"];

        $filename = $this->mediaHelper->getRandomFilename($type);
        $ext = $this->mediaHelper->getExtensionByType($type);

        $saveMediaResponse = $this->mediaHelper->saveMedia($tmp_name, $basePathUrl, $filename, false);
        $mediaModel = new MediaModel($this->serviceLocator);

        $mediaId = null;
        $file = array();
        $initialPreview = array();
        $maxFileSize = $mediaModel->returnMaxSize();
        $initialPreviewConfig = array();
        $initialPreviewThumbTags = array();

        if ($saveMediaResponse !== false) {
            $mediaModel = new MediaModel($this->serviceLocator);
            $mediaId = $mediaModel->add($this->mediaHelper->getMediaType($type), $name, $filename, $type, $ext, $size, array(), 0);

            $file = array(
                'name' => $filename,
                'file_path' => $saveMediaResponse,
                'url' => $baseUrl . $filename,
                'media_id' => $mediaId,
                'mime_type' => $type,
            );
            $initialPreview = array($baseUrl . $filename);
            $initialPreviewConfig = array(
                array(
                    'caption' => $name,
                    'size' => $size,
                    'width' => "120px",
                    'showDrag' => false,
                    'showZoom' => false,
                    'url' => $deleteUlr,
                    'key' => $mediaId
                )
            );
            $initialPreviewThumbTags = array(
                array(
                    '{add}' => '<button type="button" class="kv-file-remove btn btn-xs btn-success" id="add_media_' . $mediaId . '" title="ADD file" onClick="addMedia(' . $mediaId . ')" ><i class="glyphicon glyphicon-plus"></i> ADD</button>',
                    '{edit}' => '<button type="button" class="kv-file-remove btn btn-xs btn-default" id="edit_media_' . $mediaId . '" data-size="' . $size . '" data-url="' . $saveMediaResponse . '" data-title="' . $name . '" data-description="" onClick="editMedia(' . $mediaId . ')" title="Edit info" ><i class="glyphicon glyphicon-pencil text-defualt"></i></button>',
                )
            );
        }

        $response = json_encode(
            array(
                'file' => $file,
                'initialPreview' => $initialPreview,
                'maxFileSize' => $maxFileSize,
                'initialPreviewConfig' => $initialPreviewConfig,
                'initialPreviewThumbTags' => $initialPreviewThumbTags,
            )
        );

        return $this->viewModel->setVariable('response', $response);

    }

    public function addArchiveAction()
    {
        $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
        $baseUrl = $this->cdnBaseUrl . ImageHelper::getMediaThumbUrl();
        $deleteUlr = $renderer->basePath("/media/archive/delete/");

        $type = $_FILES['upload-archive']['type'];
        $tmp_name = $_FILES['upload-archive']['tmp_name'];
        $size = $_FILES["upload-archive"]["size"];
        $name = $_FILES["upload-archive"]["name"];

        $filename = $this->imgHelper->getRandomFilename($type);
        $ext = $this->imgHelper->getExtensionByType($type);

        $cover_url = $this->cdnBasePath . ImageHelper::getCoverIconThumbUrl();
        $tmb_url = $this->cdnBasePath . ImageHelper::getMediaThumbUrl();
        $low_url = $this->cdnBasePath . ImageHelper::getMediaLowUrl();
        $mdm_url = $this->cdnBasePath . ImageHelper::getMediaMediumUrl();
        $hgh_url = $this->cdnBasePath . ImageHelper::getMediaHighUrl();
        $upl_url = $this->cdnBasePath . ImageHelper::getMediaUploadedUrl();

        $mediaModel = new MediaModel($this->serviceLocator);
        $mediaId = $mediaModel->add("IMAGE", $name, $filename, $type, $ext, $size);
        $highUrl = $this->cdnBaseUrl . ImageHelper::getMediaHighUrl() . $filename;

        $response = json_encode(
            array(
                'file' => array(
                    'name' => $filename,
                    'cover' => $this->imgHelper->saveImageCropped(ImageHelper::TMB_SIZE, $tmp_name, $cover_url, $filename, false),
                    'thumb' => $this->imgHelper->saveImageCropped(ImageHelper::TMB_SIZE, $tmp_name, $tmb_url, $filename, false),
                    'low' => $this->imgHelper->saveImageScaled(ImageHelper::LOW_SIZE, $tmp_name, $low_url, $filename, false),
                    'medium' => $this->imgHelper->saveImageScaled(ImageHelper::MDM_SIZE, $tmp_name, $mdm_url, $filename, false),
                    'high' => $this->imgHelper->saveImageScaled(ImageHelper::HTH_SIZE, $tmp_name, $hgh_url, $filename, false),
                    'upl' => $this->imgHelper->saveImage($tmp_name, $upl_url, $filename, false),
                ),
                'initialPreview' => array(
                    ($baseUrl . $filename)
                ),
                'maxFileSize' => $mediaModel->returnMaxSize(),
                'initialPreviewConfig' => array(
                    array(
                        'caption' => $name,
                        'size' => $size,
                        'width' => "120px",
                        'showDrag' => false,
                        'showZoom' => false,
                        'url' => $deleteUlr,
                        'key' => $mediaId
                    )
                ),
                'initialPreviewThumbTags' => array(
                    array(
                        '{add}' => '<button type="button" class="kv-file-remove btn btn-xs btn-success" id="add_media_' . $mediaId . '" title="ADD file" onClick="addMedia(' . $mediaId . ')" ><i class="glyphicon glyphicon-plus"></i> ADD</button>',
                        '{edit}' => '<button type="button" class="kv-file-remove btn btn-xs btn-default" id="edit_media_' . $mediaId . '" data-type="image" data-size="' . $size . '" data-filename="' . $filename . '" data-url="' . ($baseUrl . $filename) . '" data-highurl="' . $highUrl . '" data-title="' . $name . '" data-description="" onClick="editMedia(' . $mediaId . ')" title=\"Edit info\" ><i class="glyphicon glyphicon-pencil text-defualt"></i></button>',
                    )
                )
            )
        );

        unlink($tmp_name);

        return $this->viewModel->setVariable('response', $response);
    }

    public function avatarAction()
    {
        $type = $_FILES['upload-avatar']['type'];
        $tmp_name = $_FILES['upload-avatar']['tmp_name'];
        $id = $this->aclHelper->getUser()->id;
        $filename = $this->imgHelper->getRandomFilename($type);

        $tmb_url = $this->cdnBasePath . ImageHelper::getUserAvatarThumb($id);
        $low_url = $this->cdnBasePath . ImageHelper::getUserAvatarLow($id);
        $mdm_url = $this->cdnBasePath . ImageHelper::getUserAvatarMedium($id);
        $hgh_url = $this->cdnBasePath . ImageHelper::getUserAvatarHigh($id);

        $BackendUsersModel = new BackendUsersModel($this->getServiceLocator());
        $BackendUsersModel->updateAvatar($id, $filename);

        $response = json_encode(
            array(
                'file' => array(
                    'name' => $filename,
                    'thumb' => $this->imgHelper->saveImageCropped(ImageHelper::TMB_SIZE, $tmp_name, $tmb_url, $filename, true),
                    'low' => $this->imgHelper->saveImageScaled(ImageHelper::LOW_SIZE, $tmp_name, $low_url, $filename, true),
                    'medium' => $this->imgHelper->saveImageScaled(ImageHelper::MDM_SIZE, $tmp_name, $mdm_url, $filename, true),
                    'high' => $this->imgHelper->saveImageScaled(ImageHelper::HTH_SIZE, $tmp_name, $hgh_url, $filename, true),
                ),
            )
        );

        unlink($tmp_name);

        return $this->viewModel->setVariable('response', $response);
    }

    public function appIconAction()
    {
        $type = $_FILES['upload-icon']['type'];
        $tmp_name = $_FILES['upload-icon']['tmp_name'];
        $filename = $this->imgHelper->getRandomFilename($type);

        $icon_url = $this->cdnBasePath . ImageHelper::getAppIconUrl();
        $response = json_encode(
            array(
                'file' => array(
                    'name' => $filename,
                    'icon' => $this->imgHelper->saveImageResized(ImageHelper::ICON_GLOBAL_SIZE, $tmp_name, $icon_url, $filename, false),
                )
            )
        );

        unlink($tmp_name);

        return $this->viewModel->setVariable('response', $response);
    }

    public function appSystemIconAction()
    {
        $systemType = $this->route_params->getParam('system_type');

        $type = $_FILES['upload-' . $systemType]['type'];
        $tmp_name = $_FILES['upload-' . $systemType]['tmp_name'];
        $filename = $this->imgHelper->getRandomFilename($type);

        $icon_url = $this->cdnBasePath . ImageHelper::getAppSystemIconUrl();
        $response = json_encode(
            array(
                'file' => array(
                    'name' => $filename,
                    'icon' => $this->imgHelper->saveImageResized(ImageHelper::ICON_GLOBAL_SIZE, $tmp_name, $icon_url, $filename, false),
                )
            )
        );

        unlink($tmp_name);

        return $this->viewModel->setVariable('response', $response);
    }

    public function appLogoAction()
    {
        $type = $_FILES['upload-app-logo']['type'];
        $tmp_name = $_FILES['upload-app-logo']['tmp_name'];
        $filename = $this->imgHelper->getRandomFilename($type);

        $appLogoUrl = $this->cdnBasePath . ImageHelper::getAppLogoUrl();

        $response = json_encode(
            array(
                'file' => array(
                    'name' => $filename,
                    'logo' => $this->imgHelper->saveImageScaled(ImageHelper::ICON_GLOBAL_SIZE, $tmp_name, $appLogoUrl, $filename, false),
                )
            )
        );

        unlink($tmp_name);

        return $this->viewModel->setVariable('response', $response);
    }

    public function pinMapAction()
    {
        $mediaModel = new MediaModel($this->serviceLocator);

        $type = $_FILES['upload_pin_map']['type'];
        $tmp_name = $_FILES['upload_pin_map']['tmp_name'];
        $name = $_FILES["upload_pin_map"]["name"];
        $size = $_FILES["upload_pin_map"]["size"];
        $filename = $this->imgHelper->getRandomFilename($type);
        $ext = $this->imgHelper->getExtensionByType($type);

        $pinMapUrl = $this->cdnBasePath . ImageHelper::getPoiPin();

        $pinMapSize = ImageHelper::ICON_GLOBAL_SIZE;
        $pinMapSizeFormat = $pinMapSize . "x" . $pinMapSize;
        $pinMapId = $mediaModel->add("IMAGE", $name, $filename, $type, $ext, $size, array($pinMapSizeFormat), 0);

        $response = json_encode(
            array(
                'file' => array(
                    'media_id' => $pinMapId,
                    'name' => $filename,
                    'pin' => $this->imgHelper->saveImageScaled(ImageHelper::ICON_GLOBAL_SIZE, $tmp_name, $pinMapUrl, $filename, false),
                )
            )
        );

        unlink($tmp_name);

        return $this->viewModel->setVariable('response', $response);
    }

    public function userSectionAddIconAction()
    {
        $mediaModel = new MediaModel($this->serviceLocator);
        $file = $_FILES['upload_user_section_add_icon'];

        $response = $this->addImageScaled($mediaModel, $file);

        return $this->viewModel->setVariable('response', $response);
    }

    public function appSplashAction()
    {
        $type = $_FILES['upload-splash']['type'];
        $tmp_name = $_FILES['upload-splash']['tmp_name'];
        $filename = $this->imgHelper->getRandomFilename($type);

        $splash_url = $this->cdnBasePath . ImageHelper::getAppSplashUrl();
        $response = json_encode(
            array(
                'file' => array(
                    'name' => $filename,
                    'splash' => $this->imgHelper->saveImageScaled(ImageHelper::SPLASH_HEIGHT_SIZE, $tmp_name, $splash_url, $filename, false),
                )
            )
        );

        unlink($tmp_name);

        return $this->viewModel->setVariable('response', $response);
    }

    public function sectionIconAction()
    {
        $mediaModel = new MediaModel($this->serviceLocator);

        $type = $_FILES['upload-icon']['type'];
        $tmp_name = $_FILES['upload-icon']['tmp_name'];
        $name = $_FILES["upload-icon"]["name"];
        $size = $_FILES["upload-icon"]["size"];

        $filename = $this->imgHelper->getRandomFilename($type);
        $ext = $this->imgHelper->getExtensionByType($type);

        $iconThumbSize = ImageHelper::TMB_SIZE;
        $iconThumbSizeFormat = $iconThumbSize . "x" . $iconThumbSize;
        $icon_thumb = $this->cdnBasePath . ImageHelper::getCoverIconFormatUrl($iconThumbSizeFormat);
        $icon_original = $this->cdnBasePath . ImageHelper::getCoverIconFormatUrl(ImageHelper::UPL_FOLDER);

        $sectionIconId = $mediaModel->add("IMAGE", $name, $filename, $type, $ext, $size, array($iconThumbSizeFormat), 0);

        $response = json_encode(
            array(
                'file' => array(
                    'media_id' => $sectionIconId,
                    'name' => $filename,
                    'original' => $this->imgHelper->saveImage($tmp_name, $icon_original, $filename, false),
                    'thumb' => $this->imgHelper->saveImageResized($iconThumbSize, $tmp_name, $icon_thumb, $filename, false),
                )
            )
        );

        unlink($tmp_name);

        return $this->viewModel->setVariable('response', $response);
    }

    public function customFormIconAction()
    {
        $type = $_FILES['custom_form_field_upload-icon']['type'];
        $tmp_name = $_FILES['custom_form_field_upload-icon']['tmp_name'];
        $filename = $this->imgHelper->getRandomFilename($type);

        $icon_url = $this->cdnBasePath . ImageHelper::getCustomFormIconUrl();
        $response = json_encode(
            array(
                'file' => array(
                    'name' => $filename,
                    'icon' => $this->imgHelper->saveImageResized(ImageHelper::ICON_GLOBAL_SIZE, $tmp_name, $icon_url, $filename, false),
                )
            )
        );

        unlink($tmp_name);

        return $this->viewModel->setVariable('response', $response);
    }

    public function providerIconAction()
    {
        $type = $_FILES['upload-icon']['type'];
        $tmp_name = $_FILES['upload-icon']['tmp_name'];
        $filename = $this->imgHelper->getRandomFilename($type);

        $icon_url = $this->cdnBasePath . ImageHelper::getProviderIconUrl();
        $response = json_encode(
            array(
                'file' => array(
                    'name' => $filename,
                    'icon' => $this->imgHelper->saveImageResized(ImageHelper::ICON_GLOBAL_SIZE, $tmp_name, $icon_url, $filename, false),
                )
            )
        );

        unlink($tmp_name);

        return $this->viewModel->setVariable('response', $response);
    }

    public function templateIconAction()
    {
        $type = $_FILES['upload-icon']['type'];
        $tmp_name = $_FILES['upload-icon']['tmp_name'];
        $filename = $this->imgHelper->getRandomFilename($type);

        $icon_url = $this->cdnBasePath . ImageHelper::getTemplateIconUrl();
        $response = json_encode(
            array(
                'file' => array(
                    'name' => $filename,
                    'icon' => $this->imgHelper->saveImageResized(ImageHelper::ICON_GLOBAL_SIZE, $tmp_name, $icon_url, $filename, false),
                )
            )
        );

        unlink($tmp_name);

        return $this->viewModel->setVariable('response', $response);
    }

    public function actionIconAction()
    {
        $mediaModel = new MediaModel($this->serviceLocator);

        $type = $_FILES['action_upload_icon']['type'];
        $tmp_name = $_FILES['action_upload_icon']['tmp_name'];
        $name = $_FILES["action_upload_icon"]["name"];
        $size = $_FILES["action_upload_icon"]["size"];

        $filename = $this->imgHelper->getRandomFilename($type);
        $ext = $this->imgHelper->getExtensionByType($type);

        $iconThumbSize = ImageHelper::TMB_SIZE;
        $iconThumbSizeFormat = $iconThumbSize . "x" . $iconThumbSize;
        $iconSize = ImageHelper::ICON_GLOBAL_SIZE;
        $iconSizeFormat = $iconSize . "x" . $iconSize;
        $icon_thumb = $this->cdnBasePath . ImageHelper::getActionIconUrl($iconThumbSizeFormat);
        $icon_original = $this->cdnBasePath . ImageHelper::getActionIconUrl(ImageHelper::UPL_FOLDER);
        $iconActionUrl = $this->cdnBasePath . ImageHelper::getActionIconUrl($iconSizeFormat);

        $actionIconId = $mediaModel->add("IMAGE", $name, $filename, $type, $ext, $size, array($iconThumbSizeFormat, $iconSizeFormat), 0);

        $response = json_encode(
            array(
                'file' => array(
                    'media_id' => $actionIconId,
                    'name' => $filename,
                    'original' => $this->imgHelper->saveImage($tmp_name, $icon_original, $filename, false),
                    'thumb' => $this->imgHelper->saveImageResized($iconThumbSize, $tmp_name, $icon_thumb, $filename, false),
                    'pin' => $this->imgHelper->saveImageScaled($iconSize, $tmp_name, $iconActionUrl, $filename, false),
                )
            )
        );

        unlink($tmp_name);

        return $this->viewModel->setVariable('response', $response);
    }

    public function checkRemoteMediaAction()
    {
        /** @var \Form\Media\AddRemoteMediaForm $addRemoteMediaForm */
        $addRemoteMediaForm = $this->getServiceLocator()->get('Form\Media\Factory');

        if ($addRemoteMediaForm->isValidPostRequest($this->request)) {
            $remoteMedia = $this->post_params->remote_media_link;
            $mediaModel = new MediaModel($this->serviceLocator);
            $mediaArray = $mediaModel->getRemoteMedia($remoteMedia);

            if (is_array($mediaArray)) {
                $response = json_encode($mediaArray);
                $response = $this->getJsonResponse(true, $response);
            } else {
                $response = $this->getJsonResponse(false, 'Invalid media');
            }
        } else {
            $response = "";
            foreach ($addRemoteMediaForm->getMessages() as $field => $messages) {
                foreach ($messages as $message) {
                    $response .= '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>&nbsp;' . $addRemoteMediaForm->get($field)->getLabel() . ': ' . $message . '<br>';
                }

            }
            $response = $this->getJsonResponse(false, $response);
        }

        return $this->viewModel->setVariable('response', $response);
    }

    public function uploadRemoteMediaAction()
    {
        $mediaModel = new MediaModel($this->serviceLocator);
        $mediaId = $mediaModel->addRemoteMedia($this->post_params);

        if ($mediaId) {
            $response = $this->getJsonResponse(true, $mediaId);
        } else {
            $response = $this->getJsonResponse(false, 'Media not imported.');
        }

        return $this->viewModel->setVariable('response', $response);
    }

    /**
     * @param MediaModel $mediaModel
     * @param $file
     * @return string
     */
    private function addImageScaled(MediaModel $mediaModel, $file)
    {
        $type = $file['type'];
        $tmp_name = $file['tmp_name'];
        $name = $file["name"];
        $size = $file["size"];

        $filename = $this->imgHelper->getRandomFilename($type);
        $ext = $this->imgHelper->getExtensionByType($type);

        $iconThumbSize = ImageHelper::TMB_SIZE;
        $iconThumbSizeFormat = $iconThumbSize . "x" . $iconThumbSize;
        $iconSize = ImageHelper::ICON_GLOBAL_SIZE;
        $iconSizeFormat = $iconSize . "x" . $iconSize;
        $icon_thumb = $this->cdnBasePath . ImageHelper::getActionIconUrl($iconThumbSizeFormat);
        $icon_original = $this->cdnBasePath . ImageHelper::getActionIconUrl(ImageHelper::UPL_FOLDER);
        $iconActionUrl = $this->cdnBasePath . ImageHelper::getActionIconUrl($iconSizeFormat);

        $imageId = $mediaModel->add("IMAGE", $name, $filename, $type, $ext, $size, array($iconThumbSizeFormat, $iconSizeFormat), 0);

        $response = json_encode(
            array(
                'file' => array(
                    'media_id' => $imageId,
                    'name' => $filename,
                    'original' => $this->imgHelper->saveImage($tmp_name, $icon_original, $filename, false),
                    'thumb' => $this->imgHelper->saveImageResized($iconThumbSize, $tmp_name, $icon_thumb, $filename, false),
                    'pin' => $this->imgHelper->saveImageScaled(ImageHelper::ICON_GLOBAL_SIZE, $tmp_name, $iconActionUrl, $filename, false),
                )
            )
        );

        unlink($tmp_name);
        return $response;
    }
}

