<?php
namespace Application\Model;

use Common\Helper\ImageHelper;

class TemplateModel extends HpModel
{

    public static $TPL_ROOT         = "/img/template";

    private static $getPaletteList = "SELECT SQL_CALC_FOUND_ROWS `template`.* FROM `template`
                                      WHERE `template`.`deleted_at` IS NULL AND `title` LIKE :name
                                      ORDER BY `title` = 'custom' DESC, `created_at` DESC
                                      LIMIT :start , :length";

    private static $sqlFoundRows = "SELECT FOUND_ROWS() as row_number";

    private static $getPaletteTheme = "SELECT `layout` FROM `template` WHERE id = :id AND deleted_at IS NULL";

//    private static $sqlTemplates    = "SELECT * FROM `template` WHERE deleted_at IS NULL ORDER BY title";
    private static $sqlTemplate     = "SELECT * FROM `template` WHERE id = :id AND deleted_at IS NULL";

    private static $sqlGetTemplates = "SELECT SQL_CALC_FOUND_ROWS *
                                        FROM `template` WHERE `deleted_at` IS NULL AND title LIKE :title
                                        %s
                                        LIMIT :start, :length;";

    private static $sqlCreateTemplate = "INSERT INTO `template`
                                           (`id`, `title`, `icon`, `layout`, `enabled`, `created_at`, `updated_at`)
                                          VALUES
                                           (NULL, :title, :icon, :layout, 1, NOW(), NOW());";

    private static $sqlUpdateTemplate = "UPDATE `template` SET `title` = :title, `icon` = :icon, `layout` = :layout, updated_at = NOW() WHERE `id` = :id";

    private static $sqlDeleteTemplate = "UPDATE `template` SET deleted_at = NOW(), updated_at = NOW() WHERE `id` = :id";


//    public function getTemplates() {
//        $stmt = $this->db->prepare(self::$sqlTemplates);
//
//        $stmt->execute();
//        $templates = $stmt->fetchAll(\PDO::FETCH_OBJ);
//
//        //if we don't set data-content attributes then related select-grid box will not appear
//        $res = array(array('value' => null, 'label' => '-'));
//
//        $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
//
//        foreach($templates as $t) {
//            $res []= array(
//                'value'=>$t->id,
//                'label'=>$t->title,
//                'attributes' => array(
//                    'data-content' => $renderer->basePath(self::$TPL_ROOT . $t->icon) ,
//                )
//            );
//        }
//
//        return $res;
//    }

    public function getTemplate($id) {
        $stmt = $this->db->prepare(self::$sqlTemplate);

        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchObject();
    }

    public function getPaletteList($draw, $start, $length, $search, $shownCols) {
        $res = array('draw' => $draw, 'data' => array());

        $stmt = $this->db->prepare(self::$getPaletteList);

        $filter = "%";
        $searched = trim($search['value']);
        if($searched != "") $filter = "%".$searched."%";

        $stmt->bindParam(":start",  $start,  \PDO::PARAM_INT);
        $stmt->bindParam(":length", $length, \PDO::PARAM_INT);
        $stmt->bindParam(":name",   $filter, \PDO::PARAM_STR);

        $stmt->execute();

        $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

        $data = array();
        foreach($rows as $row) {
            if(count($data) <= $shownCols) {
                $data[] = $row;
            }
            if(count($data) == $shownCols) {
                $res['data'][] = $data;
                $data = array();
            }
        }
        if(count($data) != 0) {
            $counter = count($data);
            for ($i = $counter; $i < $shownCols; $i++) {
                $data[] = "";
            }
            $res['data'][] = $data;
        }

        $totalRows = $this->getFoundRows();

        $res['recordsFiltered'] = $totalRows;
        $res['recordsTotal'] = $totalRows;

        return $res;
    }

    public function getFoundRows() {
        $rows = 0;

        if(!is_null($this->db)) {
            $stmt = $this->db->prepare(self::$sqlFoundRows);
            $stmt->execute();

            $rows = $stmt->fetchObject()->row_number;
        }

        return $rows;
    }

    public function getPaletteTheme($id) {
        $theme = array();

        $stmt = $this->db->prepare(self::$getPaletteTheme);

        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $palettes = $stmt->fetchAll(\PDO::FETCH_OBJ);
            foreach ($palettes as $palette) {
                $layout = json_decode($palette->layout);
                $theme = $layout->theme;
            }
        }

        return $theme;
    }

    public function getTemplates($draw, $start, $length, $cols, $order, $search) {
        $res = array('draw' => $draw, 'data' => array() );

        if(!is_null($this->db)) {
            $orderBy = "ORDER BY ";

            if ($order[0]['column'] == 0) {
                $orderBy .= "`updated_at` desc";
            } else {
                foreach($order as $ord) {
                    $id = $ord['column'];
                    $dir = $ord['dir'];
                    $field = $cols[$id]['name'];

                    $orderBy .= "`".$field."` ".$dir;
                }
            }

            $sql = sprintf(self::$sqlGetTemplates, $orderBy);

            $stmt = $this->db->prepare($sql);

            $filter = "%";
            if($search['value'] != "") $filter = "%".$search['value']."%";

            $stmt->bindParam(":start",  $start,  \PDO::PARAM_INT);
            $stmt->bindParam(":length", $length, \PDO::PARAM_INT);
            $stmt->bindParam(":title",  $filter, \PDO::PARAM_STR);

            $stmt->execute();

            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

            foreach($rows as $row) {
                $delete = '<button type="button" class="delete_icon btn btn-default" template-name="'.$row->title.'" style="margin-bottom: 0;"><i class="fa fa-trash-o fa-2x" aria-hidden="true"></i></button>';
                if ($row->id == 1) $delete = '';

                $layout = json_decode($row->layout);

                $res['data'][]= array(
                    'DT_RowAttr' => array(
                        'data-template-id' => $row->id,
                    ),
                    $row->id,
                    $row->icon,
                    $row->title,
                    $layout->like,
                    $layout->share,
                    $layout->filter,
                    $layout->page_size,
                    $layout->auth->mode,
                    $layout->theme->font,
                    $layout->theme->color,
                    $delete
                );
            }

            $totalRows = $this->getFoundRows();

            $res['recordsFiltered'] = $totalRows;
            $res['recordsTotal'] = $totalRows;
        }

        return $res;
    }

    public function createTemplate($data) {
        $res = false;

        $title = $data['title'];
        $icon = $data['icon'];
        $layout = json_encode(array(
            'name'   => strtoupper($data['title']).'_LAYOUT',
            'like'   => boolval($data['like']),
            'share'  => boolval($data['share']),
            'filter' => boolval($data['filter']),
            'sort'   => boolval($data['sort']),
            'page_size' => intval($data['page_size']),
            'auth' => array(
                'mode'  => $data['mode'],
                'login' => $data['login'],
            ),
            'theme' => array(
                'color' => array(
                    'global_bg'    => $data['global_bg'],
                    'primary_bg'   => $data['primary_bg'],
                    'secondary_bg' => $data['secondary_bg'],
                    'accent_bg'    => $data['accent_bg'],
                    'light_txt'    => $data['light_txt'],
                    'dark_txt'     => $data['dark_txt'],
                ),
                'font' => array(
                    'primary'   => $data['primary_font'],
                    'secondary' => $data['secondary_font'],
                ),
            )
        ));

        $stmt = $this->db->prepare(self::$sqlCreateTemplate);
        $stmt->bindParam(":title",  $title,  \PDO::PARAM_STR);
        $stmt->bindParam(":icon",   $icon,   \PDO::PARAM_STR);
        $stmt->bindParam(":layout", $layout, \PDO::PARAM_STR);

        if($stmt->execute()) {
            $res = true;
        }

        return $res;
    }

    public function updateTemplate($id, $data) {
        $res = false;

        $title = $data['title'];

        $icon = null;
        if ($data['icon'] != "") $icon = $data['icon'];

        $layout = json_encode(array(
            'name'   => strtoupper($data['title']).'_LAYOUT',
            'like'   => boolval($data['like']),
            'share'  => boolval($data['share']),
            'filter' => boolval($data['filter']),
            'sort'   => boolval($data['sort']),
            'page_size' => intval($data['page_size']),
            'auth' => array(
                'mode'  => $data['mode'],
                'login' => $data['login'],
            ),
            'theme' => array(
                'color' => array(
                    'global_bg'    => $data['global_bg'],
                    'primary_bg'   => $data['primary_bg'],
                    'secondary_bg' => $data['secondary_bg'],
                    'accent_bg'    => $data['accent_bg'],
                    'light_txt'    => $data['light_txt'],
                    'dark_txt'     => $data['dark_txt'],
                ),
                'font' => array(
                    'primary'   => $data['primary_font'],
                    'secondary' => $data['secondary_font'],
                ),
            )
        ));

        $stmt = $this->db->prepare(self::$sqlUpdateTemplate);
        $stmt->bindParam(":id",     $id,     \PDO::PARAM_INT);
        $stmt->bindParam(":title",  $title,  \PDO::PARAM_STR);
        $stmt->bindParam(":icon",   $icon,   \PDO::PARAM_STR);
        $stmt->bindParam(":layout", $layout, \PDO::PARAM_STR);

        if($stmt->execute()) {
            $res = true;
        }

        return $res;
    }

    public function deleteTemplate($id) {
        $res = false;

        $stmt = $this->db->prepare(self::$sqlDeleteTemplate);
        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        if($stmt->execute()) {
            $res = true;
        }

        return $res;
    }

}