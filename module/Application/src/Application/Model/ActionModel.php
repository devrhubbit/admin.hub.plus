<?php

namespace Application\Model;

use Common\Helper\ImageHelper;
use Common\Helper\SecurityHelper;
use Database\HubPlus\MediaQuery;
use Database\HubPlus\PostAction;
use Database\HubPlus\PostActionQuery;
use Form\Action\ActionForm;
use Propel\Runtime\Exception\PropelException;

class ActionModel extends HpModel
{

    private static $sqlActionList = "SELECT SQL_CALC_FOUND_ROWS pa.*, s.status AS section_status, s.icon_id AS section_icon, p.status AS post_status
                                        FROM `post_action` AS pa
                                          LEFT JOIN `section` AS s ON (pa.`section_id` = s.`id`)
                                          LEFT JOIN `post` AS p ON (pa.`content_id` = p.`id`)
                                        WHERE pa.`post_id` = :id
                                        ORDER BY pa.`weight`;";

    private static $selectedType = null;

    public static function setSelectedType($type)
    {
        self::$selectedType = strtoupper($type);
    }

    public static function getSelectedType()
    {
        return self::$selectedType;
    }

    public function getContentActionList($id, $lang)
    {
        $result = array();

        $stmt = $this->dbApp->prepare(self::$sqlActionList);
        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            foreach ($rows as $row) {
                $row->active = true;

                $result[] = $this->modifyRowData($row, $lang);
            }
        }

        return $result;
    }

    private function modifyRowData($row, $lang)
    {
        $row->label = $this->getLocaleText($lang, $row->label);

        switch ($row->type) {
            case ActionForm::CONTENT_ACTION:
                if ($row->post_status == 1) {
                    $row->active = false;
                }
                break;
            case ActionForm::SECTION_ACTION:
                if ($row->section_status == 1) {
                    $row->active = false;
                }
                break;
        }

        $row->icon = null;
        if ($row->cover_id !== null) {
            $media = MediaQuery::create()
                ->findOneById($row->cover_id);
            $row->icon = $media->getUri();
        }

        return $row;
    }

    public function generateActions($contentId, $actions, $lang)
    {
        if ($actions !== "" && $actions !== null) {
            $weight = 0;
            foreach ($actions as $action) {
                $weight = $weight + 10;
                $action->weight = $weight;
                $action = $this->refactorActionData($action);
                if ($action->id != "" && $action->id != null) {
                    $this->updateAction($action, $lang);
                } else {
                    $this->createAction($contentId, $action, $lang);
                }
            }
        }

        return true;
    }

    private function refactorActionData($action)
    {
        $params = json_decode($action->params);

        if ($params !== null) {
            switch ($action->type) {
                case ActionForm::CONTENT_ACTION:
                    $action->content_id = (int)$params->content;
                    $action->section_id = null;
                    break;
                case ActionForm::SECTION_ACTION:
                    $action->section_id = (int)$params->section;
                    $action->content_id = null;
                    break;
                default:
                    $action->section_id = null;
                    $action->content_id = null;
            }
        } else {
            $action->section_id = null;
            $action->content_id = null;
        }

        if ($action->icon === "") {
            $action->icon = null;
        }

        return $action;
    }

    private function createAction($contentId, $action, $lang)
    {
//        var_dump($action);
        $postAction = new PostAction();
        $postAction
            ->setPostId($contentId)
            ->setArea($action->area)
            ->setType($action->type)
            ->setLabel($this->setMultiLanguageObject($lang, $action->label, $action->label))
            ->setCoverId($action->icon)
            ->setParams($action->params)
            ->setSectionId($action->section_id)
            ->setContentId($action->content_id)
            ->setWeight($action->weight)
            ->save();
    }

    private function updateAction($action, $lang)
    {
        $postAction = PostActionQuery::create()
            ->findOneById($action->id);

        $postAction
            ->setArea($action->area)
            ->setType($action->type)
            ->setLabel($this->setMultiLanguageObject($lang, $postAction->getLabel(), $action->label))
            ->setCoverId($action->icon)
            ->setParams($action->params)
            ->setSectionId($action->section_id)
            ->setContentId($action->content_id)
            ->setWeight($action->weight)
            ->save();
    }

    public function deletedActions($deletedActions)
    {
        $res = true;

        if ($deletedActions !== "" && $deletedActions !== null) {
            $actions = explode(',', $deletedActions);

            try {
                $postsAction = PostActionQuery::create()
                    ->filterById($actions)
                    ->find();

                foreach ($postsAction as $postAction) {
                    $postAction->delete();
                }
                $res = true;
            } catch (PropelException $exception) {
                $res = false;
            }
        }

        return $res;
    }

    public function deleteActionAfterSectionDelete($sectionId)
    {
        $postsAction = PostActionQuery::create()
            ->filterBySectionId($sectionId)
            ->find();

        foreach ($postsAction as $postAction) {
            $postAction->delete();
        }

        return true;
    }

    public function deleteActionAfterContentDelete($contentId)
    {
        $postsAction = PostActionQuery::create()
            ->filterByContentId($contentId)
            ->_or()
            ->filterByPostId($contentId)
            ->find();

        foreach ($postsAction as $postAction) {
            $postAction->delete();
        }
    }

    public function actionReorder($data)
    {
        $res = false;

        foreach ($data as $d) {
            try {
                $action = PostActionQuery::create()
                    ->findOneById($d['actionId']);

                $action
                    ->setWeight($d['newWeight'])
                    ->save();

                $res = true;
            } catch (PropelException $ex) {
                $res = false;
            }
        }

        return $res;
    }
}