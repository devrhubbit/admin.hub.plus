<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 08/07/16
 * Time: 19:31
 */

namespace Application\Model;

use Common\Helper\ImageHelper;
use Common\Helper\MediaHelper;
use Database\HubPlus\Gallery;
use Database\HubPlus\GalleryQuery;
use Form\Media\RemoteMediaValidator;
use Database\HubPlus\Media;
use Database\HubPlus\MediaQuery;
use Propel\Runtime\Exception\PropelException;
use Zend\Http\Client;

class MediaModel extends HpModel
{
    const MEDIA_TYPE_UNKNOWN = 'UNKNOWN';
    const MEDIA_TYPE_IMAGE = 'IMAGE';

    const MEDIA_TYPE_REMOTE = 'REMOTE';
    // Remote Media Video
    const MEDIA_TYPE_YOUTUBE = 'YOUTUBE';
    const MEDIA_TYPE_VIMEO = 'VIMEO';
    const MEDIA_TYPE_DAILYMOTION = 'DAILYMOTION';
    const MEDIA_TYPE_FACEBOOK_VIDEO = 'FACEBOOK_VIDEO';
    // Remote Media 3D Nav Player
    const MEDIA_TYPE_MATTERPORT = 'MATTERPORT';
    // Remote Media Content
    const MEDIA_TYPE_ISSUU = 'ISSUU';
    const MEDIA_TYPE_SLIDE_SHARE = 'SLIDE_SHARE';

    public static $remoteMedia = array(
        self::MEDIA_TYPE_YOUTUBE,
        self::MEDIA_TYPE_VIMEO,
        self::MEDIA_TYPE_MATTERPORT,
        self::MEDIA_TYPE_DAILYMOTION,
        self::MEDIA_TYPE_ISSUU,
        self::MEDIA_TYPE_FACEBOOK_VIDEO,
        self::MEDIA_TYPE_SLIDE_SHARE,
    );

    private static $sqlMediaList = "SELECT * 
                                    FROM `media`
                                    WHERE `media`.id NOT IN (%s) AND `media`.is_public = 1 AND `media`.id NOT IN 
                                        (SELECT `section`.icon_id FROM `section` JOIN `media` ON (`media`.id = `section`.icon_id )) 
                                    AND device_id IS NULL AND `media`.deleted_at IS NULL;";
    private static $sqlMediaByIds = "SELECT * FROM `media` WHERE id IN (%s) AND deleted_at IS NULL";
    private static $sqlMediaByPost = "SELECT * FROM `media` JOIN `gallery` ON `media`.id = `gallery`.media_id
                                      WHERE `gallery`.post_id = :post_id AND deleted_at IS NULL AND `media`.device_id IS NULL
                                      ORDER BY `gallery`.weight;";

    private static $sqlMediaSelect = "SELECT * FROM media WHERE `id` = :id AND deleted_at IS NULL;";

    private static $sqlFormUserMediaGallery = "SELECT * FROM `media` JOIN `gallery_form` ON `media`.id = `gallery_form`.media_id
                                                WHERE deleted_at IS NULL AND `gallery_form`.form_id = :form_id AND `gallery_form`.row_id = :row_id AND `gallery_form`.field_name = :field_name";

    private static $sqlCheckFormGalleryContent = "SELECT * FROM `gallery_form` WHERE form_id = :form_id AND field_name = :field_name";

    public function getMedias($idToExclude = "0")
    {
        if (!is_null($this->dbApp)) {
            if ($idToExclude == "") $idToExclude = "0";
            $sql = sprintf(self::$sqlMediaList, $idToExclude);

            $stmt = $this->dbApp->prepare($sql);
            $stmt->execute();

            $media = $stmt->fetchAll(\PDO::FETCH_OBJ);

            return $this->parseMediaList($media);
        }
    }

    public function getMediaById($id)
    {
        $res = null;
        if (!is_null($this->dbApp)) {
            $stmt = $this->dbApp->prepare(self::$sqlMediaSelect);

            $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
            $stmt->execute();

            $res = $stmt->fetchObject();
        }
        return $res;
    }

    public function getMediasByPostId($postId)
    {
        if (!is_null($this->dbApp)) {
            $stmt = $this->dbApp->prepare(self::$sqlMediaByPost);

            $stmt->bindParam(":post_id", $postId, \PDO::PARAM_INT);
            $stmt->execute();

            $media = $stmt->fetchAll(\PDO::FETCH_OBJ);

            return $this->parseMediaList($media, false);
        }
    }

    public function getMediasByIds($mediaIds)
    {
        if (!is_null($this->dbApp)) {
            if ($mediaIds == "") $mediaIds = "0";
            $sql = sprintf(self::$sqlMediaByIds, $mediaIds);

            $stmt = $this->dbApp->prepare($sql);
            $stmt->execute();

            $media = $stmt->fetchAll(\PDO::FETCH_OBJ);

            return $this->parseMediaList($media, false);
        }
    }

    public function getAttachmentById($mediaId)
    {
        $media = MediaQuery::create()
            ->findPk($mediaId);

        return $this->parseAttachment($media);
    }

    /**
     * @param Media $media
     * @return \stdClass
     */
    private function parseAttachment($media)
    {
        $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
        $deleteUlr = $renderer->basePath("/media/attachment/delete/");

        $initialPreview = array();
        $initialPreviewConfig = array();
        $initialPreviewThumbTags = array();

        $media_data = new \stdClass();

        if ($media) {
            $cfg = new \stdClass();
            $frameAttr = new \stdClass();

            $frameAttr->mediaId = $media->getId();
            $frameAttr->media_type = strtolower($media->getType());

            $cfg->caption = $media->getTitle();
            $cfg->size = $media->getSize();
            $cfg->width = "120px";
            $cfg->showZoom = false;
            $cfg->key = $media->getId();
            $cfg->frameAttr = $frameAttr;

            $cfg->showDrag = false;
            $cfg->url = $deleteUlr;

            $initialPreview[] = $this->getAttachmentPreviewUrl($media);
            $initialPreviewConfig[] = $cfg;
        }

        $media_data->initialPreview = $initialPreview;
        $media_data->initialPreviewConfig = $initialPreviewConfig;
        $media_data->initialPreviewThumbTags = $initialPreviewThumbTags;

        return $media_data;
    }

    private function parseMediaList($media, $isArchive = true)
    {
        $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
        $cdnBaseurl = $this->serviceLocator->get('config')['cdn']['baseurl'];
        $coverBaseUrl = $cdnBaseurl . ImageHelper::getCoverIconThumbUrl();
        $highBaseUrl = $cdnBaseurl . ImageHelper::getMediaHighUrl();
        $deleteUlr = $renderer->basePath("/media/archive/delete/");

        $galleryIds = array();
        $initialPreview = array();
        $initialPreviewConfig = array();
        $initialPreviewThumbTags = array();

        $media_data = new \stdClass();

        foreach ($media as $m) {
            $cfg = new \stdClass();

            $frameAttr = new \stdClass();
            $frameAttr->mediaId = $m->id;
            $frameAttr->media_type = strtolower($m->type);

            $cfg->caption = $m->title;
            $cfg->size = $m->size;
            $cfg->width = "120px";
            $cfg->showZoom = false;
            $cfg->key = $m->id;
            $cfg->frameAttr = $frameAttr;

            if ($isArchive) {
                $cfg->showDrag = false;
                $cfg->url = $deleteUlr;

                $tag = array(
                    '{add}' => '<button type=\"button\" class=\"kv-file-remove btn btn-xs btn-success add-btn\" id=\"add_media_' . $m->id . '\" onClick=\"addMedia(' . $m->id . ')\" title=\"ADD media\"><i class=\"glyphicon glyphicon-plus\"></i> ADD</button>',
                    '{edit}' => '<button type=\"button\" class=\"kv-file-remove btn btn-xs btn-default edit-btn\" id=\"edit_media_' . $m->id . '\" data-size=\"' . $m->size . '\" data-type=\"' . strtolower($m->type) . '\" data-highurl=\"' . $this->getPreviewUrl($m, $highBaseUrl) . '\" data-url=\"' . $this->getPreviewUrl($m, $coverBaseUrl) . '\" data-title=\"' . $m->title . '\" data-description=\"' . $m->description . '\" onClick=\"editMedia(' . $m->id . ')\" title=\"Edit info\" data-media-id=\"' . $m->id . '\"><i class=\"glyphicon glyphicon-pencil text-defualt\"></i></button>',
                );

                $initialPreviewThumbTags[] = $tag;
            } else {
                $cfg->previewUrl = $this->getPreviewUrl($m, $coverBaseUrl);
                $cfg->showDrag = true;
            }

            $galleryIds [] = $m->id;
            $initialPreview[] = $this->getPreviewUrl($m, $coverBaseUrl);
            $initialPreviewConfig[] = $cfg;
        }

        $media_data->galleryIds = $galleryIds;
        $media_data->initialPreview = $initialPreview;
        $media_data->initialPreviewConfig = $initialPreviewConfig;
        $media_data->initialPreviewThumbTags = $initialPreviewThumbTags;

//        var_dump($media_data);

        return $media_data;
    }

    /**
     * @param Media $media
     * @return null|string
     */
    private function getAttachmentPreviewUrl($media)
    {
        $res = null;

        switch ($media->getType()) {
            case MediaHelper::MEDIA_TYPE_TEXT:
            case MediaHelper::MEDIA_TYPE_IMAGE :
            case MediaHelper::MEDIA_TYPE_ARCHIVE:
            case MediaHelper::MEDIA_TYPE_AUDIO:
            case MediaHelper::MEDIA_TYPE_VIDEO:
            case MediaHelper::MEDIA_TYPE_DOCUMENT:
                $res = $this->serviceLocator->get('config')['cdn']['baseurl'] . MediaHelper::getMediaUrl() . $media->getUri();
                break;
        }

        return $res;
    }

    private function getPreviewUrl($media, $baseUrl)
    {
        $res = null;

        if (in_array($media->type, self::$remoteMedia)) {
            // Remote Media
            $res = $media->uri_thumb;
        } else {
            switch ($media->type) {
                case self::MEDIA_TYPE_IMAGE:
                    $res = $baseUrl . $media->uri;
                    break;
                default:
                    $res = $media->uri;
                    break;
            }
        }


        return $res;
    }

    public function setMediaRel($postId, $galleryIds)
    {
        $mediasGallery = GalleryQuery::create()
            ->filterByPostId($postId)
            ->find();

        foreach ($mediasGallery as $mediaGallery) {
            $mediaGallery->delete();
        }

        if ($galleryIds !== "") {
            $w = 0;
            $step = 10;
            $mediaIds = explode(",", $galleryIds);
            foreach ($mediaIds as $mediaId) {
                if (!is_null($mediaId)) {
                    $gallery = new Gallery();
                    $gallery
                        ->setPostId($postId)
                        ->setMediaId($mediaId)
                        ->setWeight($w)
                        ->save();

                    $w += $step;
                }
            }
        }
    }

    public function add($type, $name, $uri, $mime = null, $extension = null, $size = 0, $format = array(), $isPublic = 1, $description = null, $uriThumb = null, $extraParams = null)
    {
        $res = -1;

        if (!is_null($this->dbApp)) {
            $media = MediaQuery::create()
                ->select(array('m'))
                ->withColumn('MAX(Media.Weight)', 'm')
                ->find();
            $maxWeightData = $media->getData();
            $maxWeight = $maxWeightData[0] === null ? 0 : $maxWeightData[0];

            $description = $description !== null ? htmlspecialchars(preg_replace("/\r|\n/", "", $description), ENT_QUOTES) : null;

            $media = new Media();
            $media
                ->setType($type)
                ->setIsPublic($isPublic)
                ->setUri($uri)
                ->setUriThumb($uriThumb)
                ->setExtraParams($extraParams)
                ->setTitle(htmlspecialchars($name, ENT_QUOTES))
                ->setExtension($extension)
                ->setMimeType($mime)
                ->setSize($size)
                ->setDescription($description)
                ->setWeight($maxWeight + 10)
                ->setFormat(json_encode($format));
            $media->save();
            $res = $media->getId();
        }

        return $res;
    }

    public function delete($id)
    {
        $res = false;

        if (!is_null($this->dbApp)) {
            try {
                $media = MediaQuery::create()
                    ->findOneById($id);

                $media->delete();
                $res = true;
            } catch (PropelException $exception) {
                $res = false;
            }
        }

        return $res;
    }

    public function setInfo($id, $title, $description)
    {
        try {
            $media = MediaQuery::create()
                ->findPk($id);

            $media
                ->setTitle(htmlspecialchars($title, ENT_QUOTES))
                ->setDescription(htmlspecialchars(preg_replace("/\r|\n/", "", $description), ENT_QUOTES))
                ->save();
            $res = true;
        } catch (PropelException $exception) {
            $res = false;
        }

        return $res;
    }

    public function getFormUserMediaGallery($formId, $rowId, $colName)
    {
        $res = null;

        $stmt = $this->dbApp->prepare(self::$sqlFormUserMediaGallery);

        $stmt->bindParam(":form_id", $formId, \PDO::PARAM_INT);
        $stmt->bindParam(":row_id", $rowId, \PDO::PARAM_INT);
        $stmt->bindParam(":field_name", $colName, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $res = $stmt->fetchAll(\PDO::FETCH_OBJ);
        }

        return $res;
    }

    public function checkSectionFieldGalleryContent($fieldLabel, $customFormId)
    {
        $res = false;

        $stmt = $this->dbApp->prepare(self::$sqlCheckFormGalleryContent);

        $stmt->bindParam(":form_id", $customFormId, \PDO::PARAM_INT);
        $stmt->bindParam(":field_name", $fieldLabel, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            if ($rows) {
                $res = true;
            }
        }

        return $res;
    }

    public function getRemoteMedia($remoteMedia)
    {
        if (strpos($remoteMedia, "http") === 0) {
            // Media is URL
            $remoteMediaUri = $remoteMedia;
        } else {
            // Media is EMBEDDED CODE
            preg_match('/src="([^"]+)"/', $remoteMedia, $match);
            $remoteMediaUri = $match[1];
        }

        $validLink = RemoteMediaValidator::getValidLink();
        $remoteMediaType = self::MEDIA_TYPE_UNKNOWN;
        $remoteMediaId = null;
        $remoteMediaThumb = "";
        $duration = 0;
        $title = null;
        $description = null;

        $remoteMediaExtraParams = new \stdClass();

        if (strpos($remoteMediaUri, $validLink[0]) !== false || strpos($remoteMediaUri, $validLink[1]) !== false || strpos($remoteMediaUri, $validLink[2]) !== false) {
            $remoteMediaType = self::MEDIA_TYPE_YOUTUBE;
            $youtubeObject = $this->getYoutubeObject($remoteMediaUri);
            if ($youtubeObject !== null) {
                $remoteMediaId = $youtubeObject->id;
                $remoteMediaThumb = $youtubeObject->thumbnail_url;
                $title = $youtubeObject->title;
            }
        }

        if (strpos($remoteMediaUri, $validLink[3]) !== false || strpos($remoteMediaUri, $validLink[4]) !== false) {
            $remoteMediaType = self::MEDIA_TYPE_VIMEO;
            $vimeoObject = $this->getVimeoObject($remoteMediaUri);
            if ($vimeoObject !== null) {
                $remoteMediaId = $vimeoObject->video_id;
                $remoteMediaThumb = $vimeoObject->thumbnail_url;
                $duration = $vimeoObject->duration;
                $title = $vimeoObject->title;
                $description = $vimeoObject->description;
            }
        }

        if (strpos($remoteMediaUri, $validLink[5]) !== false) {
            $remoteMediaType = self::MEDIA_TYPE_MATTERPORT;
            $matterPortObject = $this->getMatterPortObject($remoteMediaUri);
            $remoteMediaId = $matterPortObject->id;
            $title = $matterPortObject->id;
            $remoteMediaThumb = $matterPortObject->thumb;
        }

        if (strpos($remoteMediaUri, $validLink[6]) !== false || strpos($remoteMediaUri, $validLink[7]) !== false) {
            $remoteMediaType = self::MEDIA_TYPE_DAILYMOTION;
            $dailyMotionObject = $this->getDailyMotionObject($remoteMediaUri);
            $remoteMediaId = $dailyMotionObject->id;
            $title = $dailyMotionObject->title;
            $remoteMediaThumb = $dailyMotionObject->thumb;
        }

        if (strpos($remoteMediaUri, $validLink[8]) !== false) {
            $remoteMediaType = self::MEDIA_TYPE_ISSUU;
            $issuuObject = $this->getIssuuObject($remoteMediaUri);
            $remoteMediaId = $issuuObject->id;
            $title = $issuuObject->title;
            $description = $issuuObject->description;
            $remoteMediaThumb = $issuuObject->thumbnail_url;
        }

        if (strpos($remoteMediaUri, $validLink[9]) !== false) {
            $remoteMediaType = self::MEDIA_TYPE_FACEBOOK_VIDEO;
            parse_str(parse_url($remoteMediaUri, PHP_URL_QUERY), $my_array_of_vars);
            if (isset($my_array_of_vars['href'])) {
                $remoteMediaUri = $my_array_of_vars['href'];
            }
            $facebookVideoObject = $this->getFacebookVideoObject($remoteMediaUri);
            $remoteMediaId = $facebookVideoObject->id;
            $remoteMediaThumb = $facebookVideoObject->thumb;
        }

        if (strpos($remoteMediaUri, $validLink[10]) !== false) {
            $remoteMediaType = self::MEDIA_TYPE_SLIDE_SHARE;
            $slideShareObject = $this->getSlideShareObject($remoteMediaUri);
            $remoteMediaId = $slideShareObject->slideshow_id;
            $title = $slideShareObject->title;
            $remoteMediaThumb = $slideShareObject->thumbnail_url;
            $remoteMediaExtraParams->embed_key = $slideShareObject->embedKey;
        }

        if (strpos($remoteMediaUri, $validLink[11]) !== false || strpos($remoteMediaUri, $validLink[12]) !== false) {
            $remoteMediaType = self::MEDIA_TYPE_MATTERPORT;
            $matterPortObject = $this->getMatterPortObjectFromDimensione3($remoteMediaUri);
            $remoteMediaId = $matterPortObject->id;
            $title = $matterPortObject->id;
            $remoteMediaThumb = $matterPortObject->thumb;
        }


        $remoteMediaExtraParams->id = $remoteMediaId;
        $remoteMediaExtraParams->duration = $duration;

        return array(
            'remoteMediaType' => $remoteMediaType,
            'remoteMediaUri' => $remoteMediaUri,
            'remoteMediaId' => $remoteMediaId,
            'remoteMediaTitle' => $title,
            'remoteMediaDescription' => $description,
            'remoteMediaThumb' => $remoteMediaThumb,
            'remoteMediaExtraParams' => json_encode($remoteMediaExtraParams),
        );
    }

    public function addRemoteMedia($params)
    {
        $mediaId = $this->add($params->remoteMediaType, $params->remoteMediaTitle, $params->remoteMediaUri, null, null, 0, array(), 1, $params->remoteMediaDescription, $params->remoteMediaThumb, $params->remoteMediaExtraParams);

        if ($mediaId) {
            $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
            $deleteUlr = $renderer->basePath("/media/archive/delete/");

            return array(
                'initialPreview' => array(
                    $params->remoteMediaThumb,
                ),
                'initialPreviewConfig' => array(
                    array(
                        'caption' => $params->remoteMediaTitle !== null ? $params->remoteMediaTitle : $params->remoteMediaId,
                        'frameAttr' => array(
                            'mediaId' => $mediaId,
                            'media_type' => strtolower($params->remoteMediaType),
                        ),
                        'size' => 0,
                        'width' => "120px",
                        'showDrag' => false,
                        'showZoom' => false,
                        'url' => $deleteUlr,
                        'key' => $mediaId
                    )
                ),
                'initialPreviewThumbTags' => array(
                    array(
                        '{add}' => '<button type="button" class="btn_add kv-file-remove btn btn-xs btn-success" id="add_media_' . $mediaId . '" title="ADD file" onClick="addMedia(' . $mediaId . ')" ><i class="glyphicon glyphicon-plus"></i> ADD</button>',
                        '{edit}' => '<button type="button" class="kv-file-remove btn btn-xs btn-default" id="edit_media_' . $mediaId . '" data-size="0" data-type="' . strtolower($params->remoteMediaType) . '" data-highurl="'.$params->remoteMediaUri.'" data-url="' . $params->remoteMediaThumb . '" data-title="' . $params->remoteMediaId . '" data-description="" onClick="editMedia(' . $mediaId . ')" title="Edit info"><i class="glyphicon glyphicon-pencil text-defualt"></i></button>',
//                        '{get}' => '',
                    )
                )
            );
        }

        return false;
    }

    public function getRemoteMediaPage($mediaId)
    {
        $embeddedCode = "<div><p style='color: white;'>No media</p></div>";

        $media = MediaQuery::create()
            ->findPk($mediaId);

        if ($media) {
            $extraParams = json_decode($media->getExtraParams());
            switch ($media->getType()) {
                case self::MEDIA_TYPE_YOUTUBE:
                    $embeddedCode = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/' . $extraParams->id . '?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
                    break;
                case self::MEDIA_TYPE_VIMEO:
                    $embeddedCode = '<iframe width="100%" height="100%" src="https://player.vimeo.com/video/' . $extraParams->id . '"  frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
                    break;
                case self::MEDIA_TYPE_MATTERPORT:
                    $embeddedCode = '<iframe width ="100%" height="100%" src="https://my.matterport.com/show/?m=' . $extraParams->id . '?brand=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
                    break;
                case self::MEDIA_TYPE_DAILYMOTION:
                    $embeddedCode = '<iframe width="100%" height="100%" src="https://www.dailymotion.com/embed/video/' . $extraParams->id . '" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
                    break;
                case self::MEDIA_TYPE_ISSUU:
                    $embeddedCode = '<div data-url="' . $media->getUri() . '"style="width:100%;height:100%;" class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>';
                    break;
                case self::MEDIA_TYPE_FACEBOOK_VIDEO:
                    $embeddedCode = '<iframe width="100%" height="100%" src="https://www.facebook.com/plugins/video.php?href=' . $media->getUri() . '&show_text=0" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowfullscreen="allowfullscreen"></iframe>';
                    break;
                case self::MEDIA_TYPE_SLIDE_SHARE:
                    $embeddedCode = '<iframe width="100%" height="100%" src="https://www.slideshare.net/slideshow/embed_code/key/' . $extraParams->embed_key . '" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>';
                    break;
            }
        }

        return $embeddedCode;
    }

    public function getRemoteIframePage($mediaType, $remoteUrl)
    {
        $embeddedCode = "<div><p style='color: white;'>No media</p></div>";

        switch ($mediaType) {
            case self::MEDIA_TYPE_YOUTUBE:
                $youTubeObj = $this->getYoutubeObject($remoteUrl);
                $embeddedCode = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/' . $youTubeObj->id . '?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
                break;
            case self::MEDIA_TYPE_VIMEO:
                $vimeoObject = $this->getVimeoObject($remoteUrl);
                $embeddedCode = '<iframe width="100%" height="100%" src="https://player.vimeo.com/video/' . $vimeoObject->video_id . '"  frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
                break;
            case self::MEDIA_TYPE_MATTERPORT:
                $matterPortObject = $this->getMatterPortObject($remoteUrl);
                $embeddedCode = '<iframe width="100%" height="100%" src="https://my.matterport.com/show/?m=' . $matterPortObject->id . '?brand=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
                break;
            case self::MEDIA_TYPE_DAILYMOTION:
                $dailyMotionObject = $this->getDailyMotionObject($remoteUrl);
                $embeddedCode = '<iframe width="100%" height="100%" src="https://www.dailymotion.com/embed/video/' . $dailyMotionObject->id . '" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
                break;
            case self::MEDIA_TYPE_ISSUU:
                $embeddedCode = '<div data-url="' . $remoteUrl . '"style="width:100%;height:100%;" class="issuuembed"></div><script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>';
                break;
            case self::MEDIA_TYPE_FACEBOOK_VIDEO:
                $embeddedCode = '<iframe width="100%" height="100%" src="https://www.facebook.com/plugins/video.php?href=' . $remoteUrl . '&show_text=0" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowfullscreen="allowfullscreen"></iframe>';
                break;
            case self::MEDIA_TYPE_SLIDE_SHARE:
                $slideShareObject = $this->getSlideShareObject($remoteUrl);
                $embeddedCode = '<iframe width="100%" height="100%" src="https://www.slideshare.net/slideshow/embed_code/key/' . $slideShareObject->embedKey . '" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>';
                break;
        }

        return $embeddedCode;
    }

    private function getYoutubeObject($url)
    {
        $youtubeObject = null;

        if (preg_match("/(?<=(?:v|i)=)[a-zA-Z0-9-]+(?=&)|(?<=(?:v|i)\/)[^&\n]+|(?<=embed\/)[^\"&\n]+|(?<=(?:v|i)=)[^&\n]+|(?<=youtu.be\/)[^&\n]+/", $url, $matches)) {
            $videoId = $matches[0];
            $youtubeObject = json_decode(file_get_contents("http://www.youtube.com/oembed?url=https://youtu.be/$videoId&format=json"));
            $youtubeObject->id = $videoId;
        }

        return $youtubeObject;
    }

    private function getVimeoObject($url)
    {
        $vimeoObject = json_decode(file_get_contents("https://vimeo.com/api/oembed.json?url=$url"));

        return $vimeoObject;
    }

    private function getMatterPortObject($url)
    {
        $matterPortObject = new \stdClass();
        parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);

        $matterPortObject->id = $my_array_of_vars['m'];
        $matterPortObject->thumb = "https://my.matterport.com/api/v1/player/models/" . $matterPortObject->id . "/thumb";

        return $matterPortObject;
    }

    private function getMatterPortObjectFromDimensione3($url)
    {
        $config = array(
            'adapter' => 'Zend\Http\Client\Adapter\Curl',
        );
        $client = new Client($url, $config);
        $client->send();

        /** @var \Zend\Http\Client\Adapter\Curl $adapter */
        $adapter = $client->getAdapter();
        $handle = $adapter->getHandle();
        $effectiveUrl = curl_getinfo($handle, CURLINFO_EFFECTIVE_URL);

        return $this->getMatterPortObject($effectiveUrl);
    }

    private function getDailyMotionObject($url)
    {
        $videoId = strtok(basename($url), '_');;
        $dailyMotionObject = json_decode(file_get_contents("https://api.dailymotion.com/video/$videoId"));
        $dailyMotionObject->thumb = "https://www.dailymotion.com/thumbnail/video/$videoId";

        return $dailyMotionObject;
    }

    private function getIssuuObject($url)
    {
        $issuuObject = json_decode(file_get_contents("http://issuu.com/oembed?url=$url&format=json"));
        if ($issuuObject !== null && preg_match("/\/(?P<documentId>[a-z0-9-]+)\//", $issuuObject->thumbnail_url, $matches)) {
            $issuuObject->id = $matches['documentId'];
        }

        return $issuuObject;
    }

    private function getFacebookVideoObject($url)
    {
        $options = array(
            'http' => array(
                'method' => "GET",
                'header' => "Accept-language: en\r\n" .
                    "Cookie: foo=bar\r\n" .  // check function.stream-context-create on php.net
                    "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n" // i.e. An iPad
            )
        );

        $context = stream_context_create($options);
        $facebookVideoObject = json_decode(file_get_contents("https://www.facebook.com/plugins/video/oembed.json/?url=$url", false, $context));
        if ($facebookVideoObject !== null && preg_match("~(/videos/(?:t\.\d+/)?|id=|v=)(?P<videoId>\d+)~i", $url, $matches)) {
            $videoId = $matches['videoId'];
            $facebookVideoObject->id = $videoId;
            $facebookVideoObject->thumb = "https://graph.facebook.com/$videoId/picture";
        }

        return $facebookVideoObject;
    }

    private function getSlideShareObject($url)
    {
        $slideShareObject = json_decode(file_get_contents("http://www.slideshare.net/api/oembed/2?url=$url&format=json"));
        if ($slideShareObject !== null) {
            // Media is EMBEDDED CODE
            preg_match('/src="([^"]+)"/', $slideShareObject->html, $match);
            $embedKey = explode('key/', $match[1]);
            $slideShareObject->embedKey = $embedKey;
        }
        return $slideShareObject;
    }
}