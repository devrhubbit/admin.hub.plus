<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 22/04/16
 * Time: 15:53
 */

namespace Application\Model;

use Common\Helper\SecurityHelper;
use Database\HubPlus\Application,
    Database\HubPlus\ApplicationQuery,
    Database\HubPlus\UserApplication,
    Database\HubPlus\UserApplicationQuery;
use Database\HubPlus\CategoryQuery;
use Database\HubPlus\PackQuery;
use Database\HubPlus\PostQuery;
use Database\HubPlus\SectionQuery;
use Database\HubPlus\UserBackendQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class AppModel extends HpModel
{
    public static $TPL_ROOT = "/img/template";
    public static $DBNAME_ROOT = "hp_";
    public static $SERVER_NAME = "127.0.0.1";
    public static $API_VERSION = "v3";
    public static $START_PACK = "hub.plus.pack/free";
    public static $WHITE_LABEL = 1;
    public static $DEMO = 1;

    const APPLE_CERTIFICATE_TYPE = "pem";
    const GOOGLE_JSON_TYPE = "json";


    private static $sqlLastAppByUser = "SELECT application.*, template.title AS tpl_title, category.name AS category_name, category.setting AS category_setting
                                        FROM (`user_backend` JOIN `user_application` ON `user_backend`.id = `user_application`.user_id)
                                        JOIN `application` ON `user_application`.app_id = `application`.id
                                        JOIN template ON application.template_id = template.id
                                        JOIN category ON application.category_id = category.id
                                        WHERE `user_backend`.id = :id AND application.deleted_at IS NULL AND template.deleted_at IS NULL
                                        ORDER BY `user_application`.connected_at DESC LIMIT 1";

    private static $sqlListAppByUser = "SELECT application.*, template.title AS tpl_title
                                        FROM (`user_backend` JOIN `user_application` ON `user_backend`.id = `user_application`.user_id)
                                        JOIN `application` ON `user_application`.app_id = `application`.id
                                        JOIN template ON application.template_id = template.id
                                        WHERE `user_backend`.id = :id AND application.deleted_at IS NULL AND template.deleted_at IS NULL
                                        ORDER BY `user_application`.connected_at DESC";

    private static $sqlGetApp = "SELECT application.* FROM application WHERE `application`.deleted_at IS NULL;";

    private static $sqlGetAppById = "SELECT application.*, template.title AS tpl_title, category.name AS category_name, category.setting AS category_setting
                                     FROM application
                                     JOIN template ON application.template_id = template.id
                                     JOIN category ON application.category_id = category.id
                                     WHERE application.id=:id AND application.deleted_at IS NULL AND template.deleted_at IS NULL";


    /**
     * @param $user_id
     * @return mixed|\stdClass
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getLastApplicationByUser($user_id)
    {
        $config = $this->serviceLocator->get('config');
        $stmt = $this->db->prepare(self::$sqlLastAppByUser);

        $stmt->bindParam(":id", $user_id, \PDO::PARAM_INT);
        $stmt->execute();

        $res = $stmt->fetchObject();
        if (!$res) {
            $res = new \stdClass();
        }

        $res->version = $config['version'];
        $res->backend_name = $config['backend_name'];
        $res->api_url = $this->getRestBaseUrl();

        $this->updateLastUserAppRel($this->db->lastInsertId());

        return $res;
    }

    public function getListApplicationByUser($user_id)
    {
        $stmt = $this->db->prepare(self::$sqlListAppByUser);

        $stmt->bindParam(":id", $user_id, \PDO::PARAM_INT);
        $stmt->execute();

        $res = $stmt->fetchAll(\PDO::FETCH_OBJ);

        return $res;
    }

    public function getApplicationId()
    {
        $res = null;
        $stmt = $this->db->prepare(self::$sqlGetApp);
        if ($stmt->execute()) {
            $app = $stmt->fetchObject();
            if ($app !== false) {
                $res = $app->id;
            }
        }

        return $res;
    }

    /**
     * @param $app_id
     * @return mixed
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getApplicationByAppId($app_id)
    {
        $config = $this->serviceLocator->get('config');
        $stmt = $this->db->prepare(self::$sqlGetAppById);

        $stmt->bindParam(":id", $app_id, \PDO::PARAM_INT);
        $stmt->execute();

        $res = $stmt->fetchObject();
        $res->version = $config['version'];
        $res->backend_name = $config['backend_name'];
        $res->api_url = $this->getRestBaseUrl();

        $this->updateLastUserAppRel($app_id);

        return $res;
    }

    public function getContentsNumberInsertedBySuperAdmin()
    {
        $superAdmins = UserBackendQuery::create()
            ->filterByRoleId(BackendUsersModel::SUPER_ADMIN_ROLE_ID)
            ->find();

        $superAdminIds = array();
        foreach ($superAdmins as $superAdmin) {
            $superAdminIds[] = $superAdmin->getId();
        }

        $section = SectionQuery::create()
            ->filterByAuthorId($superAdminIds)
            ->find();

        $content = PostQuery::create()
            ->filterByAuthorId($superAdminIds)
            ->find();

        return $section->count() + $content->count();
    }

    /**
     * @param $app_id
     * @throws \Propel\Runtime\Exception\PropelException
     */
    private function updateLastUserAppRel($app_id)
    {
        $usersApplication = UserApplicationQuery::create()
            ->filterByAppId($app_id)
            ->find();

        foreach ($usersApplication as $userApplication) {
            $userApplication->setConnectedAt(time());
            $userApplication->save();
        }
    }

    /**
     * @param $app_id
     * @param $postParams
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function updateSuperAdminSettings($app_id, $postParams)
    {
        $application = ApplicationQuery::create()
            ->findOneById($app_id);

        $application
            ->setMaxNotification($postParams['max-push-notification'])
            ->setMaxAdviceHour($postParams['max-advice-hours'])
            ->setAdviceHourDone($postParams['advice-hours-done'])
            ->setMaxContentInsert($postParams['max-insert-contents']);

        $application->save();
    }

    public function getCategories()
    {
        $categories = CategoryQuery::create()
            ->filterByVisible(1)
            ->orderByWeight()
            ->find();

        $res = array();
        foreach ($categories as $category) {
            $res [] = array(
                'value' => $category->getId(),
                'label' => $category->getName()
            );
        }

        return $res;
    }

    public function getPackId($pack_bundle)
    {
        $pack = PackQuery::create()
            ->filterByBundle($pack_bundle)
            ->findOne();

        if ($pack) {
            return $pack->getId();
        }

        return null;
    }

    private function getDsnAttribute($name, $dsn)
    {
        if (preg_match('/' . $name . '=([^;]*)/', $dsn, $match)) {
            return $match[1];
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @param $extraAppIcons
     * @param $controllerUriFields
     * @param $certFile
     * @param $googleJsonFile
     * @return mixed|null|string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function createApp($data, $extraAppIcons, $controllerUriFields, $certFile, $googleJsonFile)
    {
        $config = $this->serviceLocator->get('config');

        $slug = SecurityHelper::createSlug($data['title']);
        $complete_bundle = $config['bundle_root'] . $data['bundle']; //str_replace("-", "", $slug);
        $app_key = SecurityHelper::generateKey();

        $host = self::$SERVER_NAME;

        $usr = $config['database']['core']['usr'];
        $pwd = $config['database']['core']['pwd'];
        $dsn = $config['database']['core']['dsn'];
        $dbName = $this->getDsnAttribute("dbname", $dsn);
        $title = $data['title'];
        $description = $data['description'];
        $category = $data['category'];
        $template_id = $data['template_id'];
        $icon = $data['icon'];
        $systemIcons = $this->getSystemIcons($extraAppIcons, $data);
        $splash = $data['splash'];
        $background_color = $data['splash-bg'];
        $app_logo = $data['app-logo'];
        $facebookToken = $data['facebook-token'];
        $googleAnalyticsUA = $data['google-analytics-ua'];
        $toolbar = $data['toolbar'];
        $appleID = $data['store-apple-username'] !== "" ? $data['store-apple-username'] : null;
        $appleIdPassword = $data['store-apple-password'] !== "" ? SecurityHelper::encrypt($data['store-apple-password'], $complete_bundle . SecurityHelper::KEY . $app_key, true) : null;
        $appleStoreAppLink = $data['store-apple-app-url'] !== "" ? $data['store-apple-app-url'] : null;
        $playStoreID = $data['store-google-username'] !== "" ? $data['store-google-username'] : null;
        $playStoreIdPassword = $data['store-google-password'] !== "" ? SecurityHelper::encrypt($data['store-google-password'], $complete_bundle . SecurityHelper::KEY . $app_key, true) : null;
        $playStoreAppLink = $data['store-google-app-url'] !== "" ? $data['store-google-app-url'] : null;
        $controllerUriData = $this->getControllerUriData($controllerUriFields, $data);
        $mainContentLang = $data['main-language'];
        $otherContentLang = json_encode($data['other-language']);

        $layoutJson = $this->layoutJson($data);

        $pack_id = $this->getPackId(self::$START_PACK);

        $msg = null;

        try {
            $user_id = $this->aclHelper->getUser()->id;
            if ($user_id != null && $user_id != "") {

                $application = new Application();
                $application
                    ->setTitle($title)
                    ->setSlug($slug)
                    ->setBundle($complete_bundle)
                    ->setAppKey($app_key)
                    ->setDsn($dsn)
                    ->setDbHost($host)
                    ->setDbName($dbName)
                    ->setDbUser($usr)
                    ->setDbPwd($pwd)
                    ->setPackId($pack_id)
                    ->setDescription($description)
                    ->setCategoryId($category)
                    ->setTemplateId($template_id)
                    ->setApiVersion(self::$API_VERSION)
                    ->setWhiteLabel(self::$WHITE_LABEL)
                    ->setDemo(self::$DEMO)
                    ->setIcon($icon)
                    ->setSystemIcons($systemIcons)
                    ->setSplashScreen($splash)
                    ->setBackgroundColor($background_color)
                    ->setAppLogo($app_logo)
                    ->setLayout($layoutJson)
                    ->setFacebookToken($facebookToken)
                    ->setGoogleAnalyticsUa($googleAnalyticsUA)
                    ->setToolbar($toolbar)
                    ->setAppleId($appleID)
                    ->setAppleIdPassword($appleIdPassword)
                    ->setAppleStoreAppLink($appleStoreAppLink)
                    ->setPlayStoreId($playStoreID)
                    ->setPlayStoreIdPassword($playStoreIdPassword)
                    ->setPlayStoreAppLink($playStoreAppLink)
                    ->setControllerUri($controllerUriData)
                    ->setPublished(0)
                    ->setMainContentsLanguage($mainContentLang)
                    ->setOtherContentsLanguages($otherContentLang)
                    ->setMaxNotification(1000)
                    ->save();

                $app_id = $application->getId();

                $this->AddUserAppRel($app_id, $this->aclHelper->getUser()->id);
                $this->aclHelper->setApp($this->getApplicationByAppId($app_id));

                $this->aclHelper->updateAppList();

                $certPath['path'] = "";
                if ($certFile !== null) {
                    $certPath = $this->uploadPushNotificationFile($certFile, $app_id, $config);
                    if ($certPath['success'] === false) {
                        $msg = $certPath['message'];
                        $certPath['path'] = "";
                    }
                }

                $jsonPath['path'] = "";
                if ($googleJsonFile !== null) {
                    $jsonPath = $this->uploadPushNotificationFile($googleJsonFile, $app_id, $config);
                    if ($jsonPath['success'] === false) {
                        $googleErrorMsg = $jsonPath['message'];
                        if ($msg !== null) {
                            $msg .= "<br>" . $googleErrorMsg;
                        } else {
                            $msg = $googleErrorMsg;
                        }
                        $jsonPath['path'] = "";
                    }
                }

                $pushParams = array();
                $pushParams['ios'] = array('pass_phrase' => $data['pass-phrase-apple-certificate'], 'certificate' => $certPath['path']);
                $pushParams['android'] = array('json_file' => $jsonPath['path'], 'api_key' => $data['google-api-key']);

                $application->setPushNotification(json_encode($pushParams));
                $application->save();

                $this->addSuperAdminAppRel($app_id);
                $this->addUserAppRelBeforeAppCreate($app_id);
            } else {
                $msg = "Your session has expired, please <a href='" . $this->getBaseurl() . "'>Sign in again</a>.";
            }
        } catch (\PDOException $e) {
            if ($e->getCode() == 23000) { //Unique constraints violation (on slug field)
                $msg = "The selected APP bundle identifier already exists";
            } else {
                $msg = "Critical error, new application can't be registered!" . $e->getMessage();
            }
        }

        return $msg;
    }

    private function getSystemIcons($extraAppIcons, $data)
    {
        $systemIcons = array();
        foreach ($extraAppIcons as $appIcon) {
            $systemIcons[$appIcon['value']] = $data[$appIcon['value']];
        }

        return json_encode($systemIcons);
    }

    private function getControllerUriData($controllerUriFields, $data)
    {
        $controllerUriData = array();
        if ($controllerUriFields !== null) {
            foreach ($controllerUriFields as $os => $fields) {
                foreach ($fields as $fieldId => $field) {
                    $controllerUriData[$os][$fieldId] = $data[$fieldId] === "" ? null : $data[$fieldId];
                }

            }
        }

        return json_encode($controllerUriData);
    }

    /**
     * @param $appId
     * @throws \Propel\Runtime\Exception\PropelException
     */
    private function addSuperAdminAppRel($appId)
    {
        $superAdmins = UserBackendQuery::create()
            ->filterByRoleId(BackendUsersModel::SUPER_ADMIN_ROLE_ID)
            ->find();

        foreach ($superAdmins as $superAdmin) {
            $userApp = UserApplicationQuery::create()
                ->filterByAppId($appId)
                ->filterByUserId($superAdmin->getId())
                ->find();

            if ($userApp->count() === 0) {
                $superAdminApp = new UserApplication();
                $superAdminApp
                    ->setAppId($appId)
                    ->setUserId($superAdmin->getId());
                $superAdminApp->save();
            }
        }
    }

    /**
     * @param $appId
     * @throws \Propel\Runtime\Exception\PropelException
     */
    private function addUserAppRelBeforeAppCreate($appId)
    {
        $usersApplication = UserApplicationQuery::create()->find();

        $usersId = array();
        foreach ($usersApplication as $userApplication) {
            $usersId[] = $userApplication->getUserId();
        }

        $usersBackend = UserBackendQuery::create()
            ->filterByRoleId(BackendUsersModel::SUPER_ADMIN_ROLE_ID, Criteria::GREATER_THAN)
            ->filterById($usersId, Criteria::NOT_IN)
            ->find();

        foreach ($usersBackend as $userBackend) {
            $userApplication = new UserApplication();
            $userApplication
                ->setUserId($userBackend->getId())
                ->setAppId($appId)
                ->save();
        }
    }

    /**
     * @param $app_id
     * @param $data
     * @param $extraAppIcons
     * @param $controllerUriFields
     * @param $certFile
     * @param $googleJsonFile
     * @param $config
     * @throws \Exception
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function updateApp($app_id, $data, $extraAppIcons, $controllerUriFields, $certFile, $googleJsonFile, $config)
    {
        $layoutJson = $this->layoutJson($data);

        $slug = SecurityHelper::createSlug($data['title']);

        $title = $data['title'];
        $category = $data['category'];
        $mainContentLang = $data['main-language'];
        $otherContentLang = json_encode($data['other-language']);
        $description = $data['description'];
        $template_id = $data['template_id'];
        $icon = $data['icon'];
        $systemIcons = $this->getSystemIcons($extraAppIcons, $data);
        $splash = $data['splash'];
        $background_color = $data['splash-bg'];
        $app_logo = $data['app-logo'];
        $facebookToken = $data['facebook-token'];
        $googleAnalyticsUA = $data['google-analytics-ua'];
        $toolbar = $data['toolbar'];
        $published = $data['app-published'];
        $controllerUriData = $this->getControllerUriData($controllerUriFields, $data);

        $certPathMsg = null;
        $jsonPathMsg = null;

        $certPath = $this->uploadPushNotificationFile($certFile, $app_id, $config);
        if ($certPath['success'] === false) {
            $app = $this->getApplicationByAppId($app_id);
            $pushNotificationParams = json_decode($app->push_notification);
            if ($certFile === null && $pushNotificationParams !== null) {
                $certPath['path'] = $pushNotificationParams->ios->certificate;
            } else {
                $certPath['path'] = "";
//                $certPathMsg = $certPath['message'];
            }
        }

        $jsonPath = $this->uploadPushNotificationFile($googleJsonFile, $app_id, $config);
        if ($jsonPath['success'] === false) {
            $app = $this->getApplicationByAppId($app_id);
            $pushNotificationParams = json_decode($app->push_notification);
            if ($googleJsonFile === null && $pushNotificationParams !== null) {
                $jsonPath['path'] = $pushNotificationParams->android->json_file;
            } else {
                $jsonPath['path'] = "";
//                $jsonPathMsg = $jsonPath['message'];
            }

        }

        $pushParams = array();
        $pushParams['ios'] = array('certificate' => $certPath['path'], 'pass_phrase' => $data['pass-phrase-apple-certificate']);
        $pushParams['android'] = array('json_file' => $jsonPath['path'], 'api_key' => $data['google-api-key']);

        $application = ApplicationQuery::create()
            ->findOneById($app_id);

        $appleID = $data['store-apple-username'] !== "" ? $data['store-apple-username'] : null;
        $appleIdPassword = null;
        $appleStoreAppLink = $data['store-apple-app-url'] !== "" ? $data['store-apple-app-url'] : null;
        $playStoreID = $data['store-google-username'] !== "" ? $data['store-google-username'] : null;
        $playStoreIdPassword = null;
        $playStoreAppLink = $data['store-google-app-url'] !== "" ? $data['store-google-app-url'] : null;

        $appleIdPasswordDecoded = null;
        if ($application->getAppleIdPassword() !== null) {
            $appleIdPassword = $application->getAppleIdPassword();
            $appleIdPasswordDecoded = SecurityHelper::decrypt($appleIdPassword, $this->getEncryptKey($app_id), true);
        }

        if ($data['store-apple-password'] !== "" && $appleIdPasswordDecoded !== $data['store-apple-password']) {
            $appleIdPassword = SecurityHelper::encrypt($data['store-apple-password'], $this->getEncryptKey($app_id), true);
        }

        $playStoreIdPasswordDecoded = null;
        if ($application->getPlayStoreIdPassword() !== null) {
            $playStoreIdPassword = $application->getPlayStoreIdPassword();
            $playStoreIdPasswordDecoded = SecurityHelper::decrypt($playStoreIdPassword, $this->getEncryptKey($app_id), true);
        }

        if ($data['store-google-password'] !== "" && $playStoreIdPasswordDecoded !== $data['store-google-password']) {
            $playStoreIdPassword = SecurityHelper::encrypt($data['store-google-password'], $this->getEncryptKey($app_id), true);
        }

        $application
            ->setTitle($title)
            ->setSlug($slug)
            ->setCategoryId($category)
            ->setDescription($description)
            ->setTemplateId($template_id)
            ->setIcon($icon)
            ->setSystemIcons($systemIcons)
            ->setSplashScreen($splash)
            ->setBackgroundColor($background_color)
            ->setAppLogo($app_logo)
            ->setLayout($layoutJson)
            ->setPushNotification(json_encode($pushParams))
            ->setFacebookToken($facebookToken)
            ->setGoogleAnalyticsUa($googleAnalyticsUA)
            ->setToolbar($toolbar)
            ->setAppleId($appleID)
            ->setAppleIdPassword($appleIdPassword)
            ->setAppleStoreAppLink($appleStoreAppLink)
            ->setPlayStoreId($playStoreID)
            ->setPlayStoreIdPassword($playStoreIdPassword)
            ->setPlayStoreAppLink($playStoreAppLink)
            ->setControllerUri($controllerUriData)
            ->setPublished($published)
            ->setMainContentsLanguage($mainContentLang)
            ->setOtherContentsLanguages($otherContentLang)
            ->save();

        $this->aclHelper->setApp($this->getApplicationByAppId($app_id));
        $this->aclHelper->updateAppList();
    }

    public function getEncryptKey($app_id)
    {
        $application = ApplicationQuery::create()
            ->findOneById($app_id);

        $key = $application->getBundle() . SecurityHelper::KEY . $application->getAppKey();
        return $key;
    }

    public function getLanguages()
    {
        $config = $this->serviceLocator->get('config');

        $languages = $config['system_languages'];
        $res = array();
        foreach ($languages as $value => $language) {
            $res[] = array(
                'value' => $value,
                'label' => $language['label'],
                'attributes' => array(
                    'data-content' => '<span class="' . $language['icon'] . '" style="margin-right: 2px"></span>' . $language['label'],
                ),
            );
        }

        return $res;
    }

    public function layoutJson($data)
    {
        $layout = new \stdClass();
        $layout->name = $data['cell-type'];

        $layout->page_size = 10;
        $layout->filter = $data['filter'];
        $layout->like = boolval($data['like']);
        $layout->share = boolval($data['share']);
        $layout->sort = boolval(0);

        $layout->auth = new \stdClass();
        $layout->auth->mode = $data['auth'];
        $layout->auth->login = "SIMPLE";

        $layout->theme = new \stdClass();
        $layout->theme->color = new \stdClass();
        $layout->theme->font = new \stdClass();

        $layout->theme->color->global_bg = $data['global_bg'];
        $layout->theme->color->accent_bg = $data['accent_bg'];
        $layout->theme->color->primary_bg = $data['primary_bg'];
        $layout->theme->color->secondary_bg = $data['secondary_bg'];
        $layout->theme->color->light_txt = $data['light_txt'];
        $layout->theme->color->dark_txt = $data['dark_txt'];

        $layout->theme->font->primary = $data['primary_font'];
        $layout->theme->font->primary_size = $data['primary_font_size'];
        $layout->theme->font->secondary = $data['secondary_font'];
        $layout->theme->font->secondary_size = $data['secondary_font_size'];

        $new_layout = json_encode($layout);

        return $new_layout;
    }

    /**
     * @param $app_id
     * @return bool
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function deleteApp($app_id)
    {
        $res = false;
        if ($this->aclHelper->isLogged() && $this->aclHelper->getApp()->id != $app_id) {

            $application = ApplicationQuery::create()
                ->findOneById($app_id);
            $application->delete();

            $this->aclHelper->updateAppList();

            $res = true;
        }
        return $res;
    }

    /**
     * @param $filename
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setAppIcon($filename)
    {
        $application = ApplicationQuery::create()
            ->findOneById($this->aclHelper->getApp()->id);

        $application->setIcon($filename);
        $application->save();
    }

    /**
     * @param $filename
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setAppSplash($filename)
    {
        $application = ApplicationQuery::create()
            ->findOneById($this->aclHelper->getApp()->id);

        $application->setSplashScreen($filename);
        $application->save();
    }

    private function uploadPushNotificationFile($cert, $app_id, $config)
    {
        $success = false;
        $resultPath = "";
        $message = "";

        if ($cert !== null) {
            $cert_type = $cert['type'];
            $cert_tmp_name = $cert['tmp_name'];
//            $cert_size = $cert['size'];
            $cert_name = $cert['name'];

            $instanceURL = $config["cdn"]["baseurl"];
            $find = "://";
            $instanceName = str_replace("-cdn", "", substr($instanceURL, strpos($instanceURL, $find) + strlen($find)));

            $target_dir = $config["push-notification-file-path"] . "/" . $instanceName . "/" . $app_id;
            $target_file = $target_dir . "/" . basename($cert_name);
            $fileType = pathinfo($target_file, PATHINFO_EXTENSION);

            // Check if file is valid or not
            if (($fileType === self::APPLE_CERTIFICATE_TYPE && $cert_type === "application/x-x509-ca-cert") ||
                ($fileType === self::GOOGLE_JSON_TYPE && $cert_type === "application/json")
            ) {
                if (!is_dir($target_dir)) {
                    mkdir($target_dir, 0755, true);
                }

                $target_file_uid = $target_dir . "/" . $cert_name;

                if (move_uploaded_file($cert_tmp_name, $target_file_uid)) {
                    $resultPath = $target_file_uid;
                    $success = true;

                    if ($fileType === self::GOOGLE_JSON_TYPE && $cert_type === "application/json") {
                        $success = false;
                        // Clean google service JSON file
                        $googleServiceFile = file_get_contents($resultPath);
                        $googleServiceJson = json_decode($googleServiceFile);
                        if ($googleServiceJson !== null) {
                            $app = ApplicationQuery::create()->findPk($app_id);
                            $clients = $googleServiceJson->client;

                            $idx = 0;
                            foreach ($clients as $client) {
                                if ($app->getBundle() !== $client->client_info->android_client_info->package_name) {
                                    unset($clients[$idx]);
                                }
                                $idx++;
                            }

                            if (count($clients) === 1 && $clients[0]->client_info->android_client_info->package_name === $app->getBundle()) {
                                $googleServiceJson->client = $clients;
                                file_put_contents($resultPath, json_encode($googleServiceJson));
                                $success = true;
                            } else {
                                $message = "Google notification JSON must have '" . $app->getBundle() . "' package name.";
                            }
                        }
                    }
                } else {
                    $message = "Unable to copy " . $cert_name . " file.";
                }
            }
        }

        return array(
            "success" => $success,
            "path" => $resultPath,
            "message" => $message,
        );
    }

    /**
     * @param $app_id
     * @param $user_id
     * @return mixed
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function AddUserAppRel($app_id, $user_id)
    {
        $userApplication = new UserApplication();
        $userApplication->setAppId($app_id)
            ->setUserId($user_id);
        $userApplication->save();

        return $app_id;
    }

}