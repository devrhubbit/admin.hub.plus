<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 28/06/16
 * Time: 11:01
 */

namespace Application\Model;


use Database\HubPlus\Tag;
use Database\HubPlus\TagQuery;
use Propel\Runtime\Exception\PropelException;

class TagModel extends HpModel
{
    const UNDEFINED = "undefined";

    private $lastInsertTagId = 0;

    private static $sqlTagsAutocomplete = "SELECT `id`, `label` AS `value`, `label` FROM `tag` WHERE `label` LIKE :term %s AND `deleted_at` IS NULL ORDER BY label;";
    private static $sqlUserTagsAutocomplete = "SELECT * FROM
                                                  (SELECT CONCAT('tag_', id) AS id, label AS value FROM tag WHERE `deleted_at` IS NULL AND type IN ('CUSTOM_FORM','CUSTOM_FORM_FIELD')
                                                   UNION
                                                   SELECT CONCAT('user_', id) AS id, CONCAT('<' , email , '>') AS value
                                                   FROM user_app WHERE `deleted_at` IS NULL AND locked = 0 AND expired = 0) AS t
                                               WHERE value LIKE :term
                                               ORDER BY value;";

    public function getTags($term, $type)
    {
        if (!is_null($this->dbApp)) {
            $section_type = "SECTION";

            $typeFilter = " AND (`type` = :type OR `type` = :type_section) ";
            if ($type == $section_type) $typeFilter = "";


            $stmt = $this->dbApp->prepare(sprintf(self::$sqlTagsAutocomplete, $typeFilter));

            $t = $term . "%";
            if ($t[0] != "#") $t = "#" . $t;

            $stmt->bindParam(":term", $t, \PDO::PARAM_STR);
            if ($type != "SECTION") {
                $stmt->bindParam(":type", $type, \PDO::PARAM_STR);
                $stmt->bindParam(":type_section", $section_type, \PDO::PARAM_STR);
            }

            $stmt->execute();

            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
    }

    public function getUserTag($term)
    {
        if (!is_null($this->dbApp)) {
            $stmt = $this->dbApp->prepare(self::$sqlUserTagsAutocomplete);

            $t = "%" . $term . "%";
            $stmt->bindParam(":term", $t, \PDO::PARAM_STR);
            $stmt->execute();

            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
    }

    public function parseTagList($tags, $type, $group = TagModel::UNDEFINED)
    {
        $res = array();

        foreach ($tags as $tag) {
            if ($tag != "") {
                $tag = trim($tag);

                $tagId = $this->getTagId($tag, $type);
                if ($tagId == 0) {
                    if ($this->insertTag($tag, $type, null, $group)) {
                        $res [] = array(
                            "key" => $this->lastInsertTagId,
                            "value" => $tag,
                            "group" => $group,
                        );
                    } else {
                        return false;
                    }
                } else {
                    $res [] = array(
                        "key" => $tagId,
                        "value" => $tag,
                        "group" => $group,
                    );
                }
            }
        }

        return $res;
    }

    public function parseTagListObject($tagsObject, $type, $group = TagModel::UNDEFINED)
    {
        if (is_string($tagsObject)) {
            return $this->parseTagList(array_map('trim', explode(",", $tagsObject)), $type, $group);
        }

        foreach ($tagsObject as $keyTag => $tagObj) {
            $keyTagId = $this->getTagId($keyTag, $type);
            if ($keyTagId == 0) {
                if ($this->insertTag($keyTag, $type, null, $group)) {
                    $keyTagId = $this->lastInsertTagId;
                }
            }
            foreach ($tagObj as $tag) {
                $tagId = $this->getTagId($tag, $type);
                if ($tagId == 0) {
                    $this->insertTag($tag, $type, $keyTagId, $group);
                }
            }
        }
    }

    private function getTagId($label, $type)
    {
        $tag = TagQuery::create()
            ->filterByLabel($label)
            ->filterByType($type)
            ->findOne();

        if ($tag) {
            return $tag->getId();
        } else {
            return 0;
        }
    }

    private function insertTag($label, $type, $relatedTagId = null, $group = TagModel::UNDEFINED)
    {
        $res = true;

        try {
            $tag = new Tag();
            $tag
                ->setRelatedTag($relatedTagId)
                ->setLabel($label)
                ->setType($type)
                ->setGroupLabel($group)
                ->save();

            $this->lastInsertTagId = $tag->getId();
        } catch (PropelException $exception) {
            $res = false;
        }

        return $res;
    }
}