<?php

namespace Application\Model;

use Common\Helper\SecurityHelper;
use Common\Helper\StringHelper;
use Database\HubPlus\RemoteProviderQuery;
use Propel\Runtime\Exception\PropelException;

class ProviderModel extends HpModel {

    private static $sqlGetProviderList = "SELECT SQL_CALC_FOUND_ROWS `remote_provider`.*, GROUP_CONCAT(`section_connector`.`section_id`) as section_id
                                          FROM `remote_provider` LEFT JOIN `section_connector` ON (`remote_provider`.id = `section_connector`.`provider_id`)
                                          WHERE (name LIKE :name OR site LIKE :site OR email LIKE :email)
                                          GROUP BY `remote_provider`.id
                                            %s
                                          LIMIT :start, :length";

    private static $sqlGetProviders   = "SELECT * FROM `remote_provider` ORDER BY name";

    private static $sqlGetProvider    = "SELECT * FROM `remote_provider` WHERE `id` = :id";

    private static $sqlCreateProvider  = "INSERT INTO `remote_provider` (`id`, `uuid`, `email`, `email_canonical`, `name`, `slug`, `site`, `created_at`, `updated_at`)
                                          VALUES (NULL, :uuid, :email, :email_c, :name, :slug, :site, NOW(), NOW())";

    private static $sqlUpdateProvider  = "UPDATE `remote_provider` SET
                                          `email` = :email, `email_canonical` = :email_c, `name` = :name, `slug` = :slug, `site` = :site, updated_at = NOW()
                                          WHERE `id` = :id";



    public function getProviderList($draw, $start, $length, $cols, $order, $search) {
        $res = array('draw' => $draw, 'data' => array());

        if(!is_null($this->dbApp)) {
            $orderBy = "ORDER BY ";

            foreach($order as $ord) {
                $id = $ord['column'];
                $dir = $ord['dir'];
                $field = $cols[$id]['name'];

                $orderBy .= "`remote_provider`.`".$field."` ".$dir;
            }

            $sql = sprintf(self::$sqlGetProviderList, $orderBy);

            $stmt = $this->dbApp->prepare($sql);

            $filter = "%";
            if($search['value'] != "") $filter = "%".$search['value']."%";

            $stmt->bindParam(":start",  $start,  \PDO::PARAM_INT);
            $stmt->bindParam(":length", $length, \PDO::PARAM_INT);
            $stmt->bindParam(":name",   $filter, \PDO::PARAM_STR);
            $stmt->bindParam(":site",   $filter, \PDO::PARAM_STR);
            $stmt->bindParam(":email",  $filter, \PDO::PARAM_STR);

            $stmt->execute();

            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

            foreach($rows as $row) {
                ($row->section_id) ? $disabled = " disabled" : $disabled = "";
                $res['data'][]= array(
                    'DT_RowAttr' => array(
                        'data-content-id' => $row->id,
                    ),
                    $row->id,
                    $row->name,
                    $row->site,
                    $row->email,
                    '<button type="button" class="copy_clipboard_icon btn btn-default" data-value="'.$row->uuid.'"><i class="fa fa-code fa-2x" aria-hidden="true"></i></button>',
                    '<button type="button" class="delete_icon btn btn-default'.$disabled.'" provider-title="'.$row->name.'" provider-id="'.$row->id.'" section-id="'.$row->section_id.'"><i class="fa fa-trash-o fa-2x" aria-hidden="true"></i></button>',
                );
            }

            $totalRows = $this->getFoundRows();

            $res['recordsFiltered'] = $totalRows;
            $res['recordsTotal']    = $totalRows;
        }

        return $res;
    }

    public function getProvider($id) {
        $stmt = $this->dbApp->prepare(self::$sqlGetProvider);
        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        $stmt->execute();
        $res = $stmt->fetchAll(\PDO::FETCH_OBJ);

        return $res;
    }

    public function getProviders() {
        $stmt = $this->dbApp->prepare(self::$sqlGetProviders);

        $stmt->execute();
        $providers = $stmt->fetchAll(\PDO::FETCH_OBJ);

        $res = array();
        foreach($providers as $p) {
            $res []= array( 'value'=>$p->id, 'label'=>$p->name );
        }

        return $res;
    }

    public function createProvider($name, $site, $email) {
        $stmt = $this->dbApp->prepare(self::$sqlCreateProvider);

        $uuid = SecurityHelper::generateKey();
        $slug = SecurityHelper::createSlug($name);
        $email_c = StringHelper::getCanonical($email);

        $stmt->bindParam(":uuid",        $uuid,      \PDO::PARAM_STR);
        $stmt->bindParam(":email",       $email,     \PDO::PARAM_STR);
        $stmt->bindParam(":email_c",     $email_c,   \PDO::PARAM_STR);
        $stmt->bindParam(":name",        $name,      \PDO::PARAM_STR);
        $stmt->bindParam(":slug",        $slug,      \PDO::PARAM_STR);
        $stmt->bindParam(":site",        $site,      \PDO::PARAM_STR);

        return $stmt->execute();
    }

    public function updateProvider($id, $name, $site, $email) {
        $stmt = $this->dbApp->prepare(self::$sqlUpdateProvider);

        $slug = SecurityHelper::createSlug($name);
        $email_c = StringHelper::getCanonical($email);

        $stmt->bindParam(":id",          $id,        \PDO::PARAM_INT);
        $stmt->bindParam(":email",       $email,     \PDO::PARAM_STR);
        $stmt->bindParam(":email_c",     $email_c,   \PDO::PARAM_STR);
        $stmt->bindParam(":name",        $name,      \PDO::PARAM_STR);
        $stmt->bindParam(":slug",        $slug,      \PDO::PARAM_STR);
        $stmt->bindParam(":site",        $site,      \PDO::PARAM_STR);

        return $stmt->execute();
    }

    public function deleteProvider($id) {
        try {
            $provider = RemoteProviderQuery::create()
                ->findPk($id);

            if ($provider) {
                $provider->delete();
            }
            $res = true;
        } catch (PropelException $exception) {
            $res = false;
        }

        return $res;
    }

}