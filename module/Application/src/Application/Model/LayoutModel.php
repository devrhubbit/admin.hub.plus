<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 05/06/16
 * Time: 20:29
 */

namespace Application\Model;

/**
 * Class LayoutModel
 *
 *  {
 *   "name"      : ENUM(LEFT_LAYOUT, RIGHT_LAYOUT, CARD_LAYOUT...),
 *    "like"     : BOOLEAN,
 *    "share"    : BOOLEAN,
 *    "filter"   : BOOLEAN,
 *    "sort"     : BOOLEAN,
 *    "page_size": INT,
 *    "auth": {
 *        "mode":  ENUM(MANDATORY|OPTIONAL|POPUP),
 *        "login": ENUM(AUTO|SIMPLE|ADVANCED), //The only used value is ever SIMPLE, AUTO / ADVANCED values will be reserved for future release
 *    },
 *        "theme": {
 *        "color": {
 *           "global_bg"   : EX_COLOR,
 *            "primary_bg"  : EX_COLOR,
 *            "secondary_bg": EX_COLOR,
 *            "accent_bg"   : EX_COLOR,
 *            "light_txt"   : EX_COLOR,
 *            "dark_txt"    : EX_COLOR
 *         },
 *         "font": null | {
 *            "primary"  : null | STRING,
 *            "secondary": null | STRING,
 *         }
 *     },
 *   }
 *
 * @package Application\Model
 */

class LayoutModel {

    public static $MAIN_TAB_LAYOUT = "MAIN_TAB_LAYOUT";

    private $layout = null;

    public function __construct($name) {
        $this->layout = new \stdClass();

        $this->layout->name      = $name;
        $this->layout->like      = false;
        $this->layout->share     = false;
        $this->layout->filter    = false;
        $this->layout->sort      = false;
        $this->layout->page_size = 20;
//        $this->layout->login     = null;
//        $this->layout->register  = false;

        $theme = new \stdClass();
        $theme->color = null;
        $theme->font  = null;

        $this->layout->theme = $theme;
    }

    public function getJson() {
        return json_encode($this->layout);
    }

    public function setJson($json) {
        $this->layout = json_decode($json);
    }

    public function setColors($colors) {
        $this->layout->theme->color = $colors;
    }

    public function setFont($primary, $secondary) {
        $font = new \stdClass();
        $font->primary = $primary;
        $font->secondary = $secondary;

        $this->layout->theme->font = $font;
    }
}