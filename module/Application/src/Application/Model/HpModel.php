<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 29/04/16
 * Time: 17:07
 */

namespace Application\Model;


use Database\HubPlus\MediaQuery;
use Form\LayoutForm;
use Form\Section\CustomForm\CustomFormSectionForm;
use Form\Section\SectionForm;
use Zend\ServiceManager\ServiceLocatorInterface;
use Common\Helper\DbHelper;
use Common\Helper\AclHelper;

class HpModel
{
    protected $serviceLocator;
    protected $dbHelper;
    public $aclHelper;
    protected $db;

    private static $sqlFoundRows = "SELECT FOUND_ROWS() as row_number;";

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->aclHelper      = new AclHelper($this->serviceLocator);
        $this->dbHelper       = new DbHelper($this->serviceLocator, $this->aclHelper);
        $this->db             = $this->dbHelper->getCoreDatabase();
        $this->dbApp          = $this->dbHelper->getAppDatabase();
    }

    public function getLocaleText($lang, $text)
    {
        $res = null;
        if ($text !== "") {
            $multiLangObj = json_decode($text);
            if ($multiLangObj) {
                if (isset($multiLangObj->{$lang})) {
                    $res = $multiLangObj->{$lang};
                } else {
                    $app = $this->aclHelper->getApp();
                    $res = $multiLangObj->{$app->main_contents_language};
                }
            } else {
                $res = $text;
            }
        }

        return $res;
    }

    public function setMultiLanguageObject($lang, $oldText, $newLocaleText, $toJson = true)
    {
        $res = null;
        if ($oldText !== "") {
            $textObj = json_decode($oldText);
            if ($textObj) {
                $textObj->{$lang} = $newLocaleText;
                $res = $textObj;
            } else {
                $userLanguages = $this->getUserLanguages();

                $newTextObj = new \stdClass();
                foreach ($userLanguages as $key) {
                    $newTextObj->{$key} = $newLocaleText;
                }
                $res = $newTextObj;
            }

            if ($toJson === true) {
                $res = json_encode($res);
            }
        }

        return $res;
    }

    public function getFoundRows() {
        $rows = 0;

        if(!is_null($this->dbApp)) {
            $stmt = $this->dbApp->prepare(self::$sqlFoundRows);
            $stmt->execute();

            $rows = $stmt->fetchObject()->row_number;
        }

        return $rows;
    }

    public function getBaseurl() {
        $renderer   = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
        return $renderer->basePath();
    }

    public function getCdnBaseurl() {
        $config = $this->serviceLocator->get('config');
        return $config['cdn']['baseurl'];
    }

    public function getCdnBasePath() {
        $config = $this->serviceLocator->get('config');
        return $config['cdn']['basepath'];
    }

    public function flatLayoutJson($layout, $cell = null, $template_id = null, $type = null) {
        $json = json_decode($layout);
        $data = array();

        if (isset($cell)) {
            $data['cell-type'] = $cell;
        } else {
            $data['cell-type']  = isset($json->name) ? $json->name : "LAYOUT";
        }

        $data['page_size'] = isset($json->page_size)  ? $json->page_size  : 10;
        $data['filter']    = isset($json->filter)     ? $json->filter     : LayoutForm::SEARCH_DISABLED_KEY;
        $data['like']      = isset($json->like)       ? $json->like       : false;
        $data['share']     = isset($json->share)      ? $json->share      : false;
        $data['sort']      = isset($json->sort)       ? $json->sort       : false;

        $data['auth'] = isset($json->auth->mode) ? $json->auth->mode : LayoutForm::AUTH_MODE_OPTIONAL;

        $data['global_bg']    = isset($json->theme->color->global_bg)    ? $json->theme->color->global_bg    : '#ffffff';
        $data['primary_bg']   = isset($json->theme->color->primary_bg)   ? $json->theme->color->primary_bg   : '#ffffff';
        $data['secondary_bg'] = isset($json->theme->color->secondary_bg) ? $json->theme->color->secondary_bg : '#ffffff';
        $data['accent_bg']    = isset($json->theme->color->accent_bg)    ? $json->theme->color->accent_bg    : '#ffffff';
        $data['light_txt']    = isset($json->theme->color->light_txt)    ? $json->theme->color->light_txt    : '#ffffff';
        $data['dark_txt']     = isset($json->theme->color->dark_txt)     ? $json->theme->color->dark_txt     : '#ffffff';

        $data['primary_font']        = isset($json->theme->font->primary)        ? $json->theme->font->primary        : '#ffffff';
        $data['primary_font_size']   = isset($json->theme->font->primary_size)   ? $json->theme->font->primary_size   : 12;
        $data['secondary_font']      = isset($json->theme->font->secondary)      ? $json->theme->font->secondary      : '#ffffff';
        $data['secondary_font_size'] = isset($json->theme->font->secondary_size) ? $json->theme->font->secondary_size : 12;

        switch ($type) {
            case strtolower(LayoutForm::EVENT):
                break;
            case SectionForm::RSS:
                $data['paged']       = isset($json->paged) ? $json->paged : false;
                $data['open_in_app'] = isset($json->open_in_app) ? $json->open_in_app : true;
                break;
            case SectionForm::PODCAST:
                $data['paged']     = isset($json->paged)     ? $json->paged     : false;
                $data['autoplay']  = isset($json->autoplay)  ? $json->autoplay  : false;
                $data['streaming'] = isset($json->streaming) ? $json->streaming : false;
                break;
            case strtolower(SectionForm::WEB):
                $data['open_in_app'] = isset($json->open_in_app) ? $json->open_in_app : true;
                break;
        }

        $data['template_id'] = $template_id;

        return $data;
    }

    public function parseLayout($data) {
        $authMode = array_key_exists('auth', $data) ? $data['auth'] : LayoutForm::AUTH_MODE_OPTIONAL;
        if (array_key_exists('form_type', $data) && $data['form_type'] === CustomFormSectionForm::REGISTRATION_FORM) {
            $authMode = LayoutForm::AUTH_MODE_OPTIONAL;
        }

        $layout = array(
            "name"            => array_key_exists('cell-type', $data) ? $data['cell-type'] : LayoutForm::NO_LAYOUT_NEEDED,
            "page_size"       => array_key_exists('page_size', $data) ? (int) $data['page_size'] : 10,
            "filter"          => array_key_exists('filter', $data) ? $data['filter'] : LayoutForm::SEARCH_DISABLED_VALUE,
            "like"            => array_key_exists('like', $data) ? (boolean) $data['like'] : false,
            "share"           => array_key_exists('share', $data) ? (boolean) $data['share'] : false,
            "sort"            => false, //array_key_exists('sort', $data) ? $data['sort'] : false,
            "auth" => array(
                "mode"  => $authMode,
                "login" => "SIMPLE",
            ),
            "theme" => array(
                "color" => array(
                    "global_bg"     => $data['global_bg'],
                    "primary_bg"    => $data['primary_bg'],
                    "secondary_bg"  => $data['secondary_bg'],
                    "accent_bg"     => $data['accent_bg'],
                    "light_txt"     => $data['light_txt'],
                    "dark_txt"      => $data['dark_txt'],
                ),
                "font" => array(
                    "primary"        => $data['primary_font'],
                    "primary_size"   => $data['primary_font_size'],
                    "secondary"      => $data['secondary_font'],
                    "secondary_size" => $data['secondary_font_size'],
                )
            )
        );

        return $layout;
    }

    public function getTypeLabel($type, $div) {
        $config = $this->serviceLocator->get('config');

        $label = "";
        foreach($config[$div] as $key => $values) {
            if ($key == $type) {
                $label = $values['label'];
            }
        }

        return $label;
    }

    public function parseTag($data) {
        $tagModel = new TagModel($this->serviceLocator);

        $tags = explode(",", $data["tags"]);
        $type = $data["type"];

        return $tagModel->parseTagList($tags, $type);
    }

    public function flatTagsJson($tags) {
        $jsonTags = json_decode($tags);
        $res = array();

        foreach($jsonTags as $tag) {
            $res []= $tag->value;
        }

        return implode(",", $res);
    }

    public function flatGroupedTagsJson($tags) {
        $jsonTags = json_decode($tags);

        $res = array();
        $list = array();

        $lastGroupName = "";
        $tagObj = new \stdClass();;

        foreach($jsonTags as $tag) {
            if($lastGroupName != $tag->group) {
                $list = array();
                $lastGroupName = $tag->group;
                $tagObj = new \stdClass();

                $tagObj->label = $tag->group;
                $res []= $tagObj;
            }

            $list []= $tag->value;
            $tagObj->tags = implode(",",$list);
        }

        return count($res)>0 ? json_encode($res) : "";
    }

    public function checkLayoutEquality($this_layout, $old_layout) {

        $match_global_bg    = ($old_layout->color->global_bg == $this_layout->theme->color->global_bg);
        $match_primary_bg   = ($old_layout->color->primary_bg == $this_layout->theme->color->primary_bg);
        $match_secondary_bg = ($old_layout->color->secondary_bg == $this_layout->theme->color->secondary_bg);
        $match_accent_bg    = ($old_layout->color->accent_bg == $this_layout->theme->color->accent_bg);
        $match_light_txt    = ($old_layout->color->light_txt == $this_layout->theme->color->light_txt);
        $match_dark_txt     = ($old_layout->color->dark_txt == $this_layout->theme->color->dark_txt);

        $match_pri_font = ($old_layout->font->primary == $this_layout->theme->font->primary);
        $match_sec_font = ($old_layout->font->secondary == $this_layout->theme->font->secondary);

        if ($match_global_bg && $match_primary_bg && $match_secondary_bg && $match_accent_bg && $match_light_txt && $match_dark_txt && $match_pri_font && $match_sec_font) {
            return true;
        } else {
            return false;
        }
    }

    public function getRestBaseUrl()
    {
        return $this->getBaseurl() . "/api/v3";
    }

    public function addMediaFormat($media_id, $coverFormat)
    {
        $media = MediaQuery::create()
            ->findOneById($media_id);

        $formats = json_decode($media->getFormat());
        if ($formats === null) {
            $formats = array();
        }

        $formats[] = $coverFormat;

        $media->setFormat(json_encode($formats));
        $media->save();
    }

    public function returnMaxSize()
    {
        $maxFileSizeValues = array(
            $this->return_bytes(ini_get('post_max_size')),
            $this->return_bytes(ini_get('upload_max_filesize')),
        );

        return min($maxFileSizeValues);
    }

    public function return_bytes($val) {
        $val = trim($val);
        $last = strtolower($val[strlen($val)-1]);
        switch($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 'g':
                $val *= 1024;
                break;
            case 'm':
                $val *= 1024;
                break;
            case 'k':
                $val *= 1024;
                break;
        }

        return $val;
    }

    /**
     * @return array
     */
    public function getUserLanguages()
    {
        $application = $this->aclHelper->getApp();
        $userLanguages = array($application->main_contents_language);
        $otherContentsLanguages = json_decode($application->other_contents_languages);

        if ($otherContentsLanguages) {
            foreach ($otherContentsLanguages as $language) {
                $userLanguages[] = $language;
            }
        }
        return $userLanguages;
    }
}