<?php

namespace Application\Model;

use Database\HubPlus\Section;
use Database\HubPlus\SectionQuery;
use Form\Section\CustomForm\CustomFormSectionForm;

class UserModel extends HpModel
{

    private static $sqlUserList = "SELECT SQL_CALC_FOUND_ROWS `user_app`.id AS user_id, `user_app`.*, `user_app`.created_at AS `registered at`, `address`.*, NULL AS `last modified`
                                      FROM `user_app` LEFT JOIN `address` ON `user_app`.address_id = `address`.id
                                      WHERE
                                        `deleted_at` IS NULL AND
                                        (address LIKE :addr OR city LIKE :ct OR username LIKE :username)
                                      %s LIMIT :start, :length;";

    private static $sqlUserList2 = "SELECT SQL_CALC_FOUND_ROWS `user_app`.*, `user_app`.created_at AS `registered at`, `address`.*, cf.*, cf.updated_at AS `last modified`
                                      FROM `user_app` LEFT JOIN `address` ON `user_app`.address_id = `address`.id
                                      LEFT JOIN `custom_form_%s` AS cf ON `user_app`.`id` = cf.`user_id`
                                      WHERE
                                        `deleted_at` IS NULL AND
                                        (address LIKE :addr OR city LIKE :ct OR username LIKE :username)
                                      %s LIMIT :start, :length;";


    public function getLastUsers($start, $length)
    {
        $rows = null;
        $registrationFormParams = null;
        $registrationFormId = null;

        if (!is_null($this->dbApp)) {
            $sectionModel = new SectionModel($this->serviceLocator);
            $registrationForm = $sectionModel->getRegistrationForm();


            if ($registrationForm !== null) {
                $registrationFormId = $registrationForm->getId();
                $registrationFormParams = json_decode($registrationForm->getParams());
            }

            $sql = $this->getUserQuery($registrationForm);

            $stmt = $this->dbApp->prepare($sql);

            $filter = "%";

            $stmt->bindParam(":start", $start, \PDO::PARAM_INT);
            $stmt->bindParam(":length", $length, \PDO::PARAM_INT);
            $stmt->bindParam(":addr", $filter, \PDO::PARAM_STR);
            $stmt->bindParam(":ct", $filter, \PDO::PARAM_STR);
            $stmt->bindParam(":username", $filter, \PDO::PARAM_STR);

            $stmt->execute();

            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
        }

        return array('users' => $rows, 'formId' => $registrationFormId, 'params' => $registrationFormParams);
    }

    public function getUsers($formId, $draw, $start, $length, $cols, $order, $search)
    {
        $res = array('draw' => $draw, 'data' => array());

        if (!is_null($this->dbApp)) {
            $sectionModel = new SectionModel($this->serviceLocator);
            $registrationForm = $sectionModel->getRegistrationForm();
            $registrationFormParams = null;

            if ($registrationForm !== null) {
                $registrationFormId = $registrationForm->getId();
                $registrationFormParams = json_decode($registrationForm->getParams());
            }

            $sql = $this->getUserQuery($registrationForm, $order, $cols);

            $stmt = $this->dbApp->prepare($sql);

            $filter = "%";
            if ($search['value'] != "") $filter = "%" . $search['value'] . "%";

            $stmt->bindParam(":start", $start, \PDO::PARAM_INT);
            $stmt->bindParam(":length", $length, \PDO::PARAM_INT);
            $stmt->bindParam(":addr", $filter, \PDO::PARAM_STR);
            $stmt->bindParam(":ct", $filter, \PDO::PARAM_STR);
            $stmt->bindParam(":username", $filter, \PDO::PARAM_STR);

            $stmt->execute();

            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

            $rowsData = array();
            foreach ($rows as $row) {
                $rowData = array(
                    'DT_RowAttr' => array(
                        'data-content-id' => $row->user_id,
                    ),
                );

                $lastModify = null;
                if ($registrationForm !== null) {
                    $lastModify = $row->updated_at;
                }

                $registeredAtLabel = "registered at";

                $rowData[] = $row->user_id;
                $rowData[] = $row->address;
                $rowData[] = $row->city;
                $rowData[] = $lastModify;
                $rowData[] = $row->username;
                $rowData[] = $row->$registeredAtLabel;

                if ($registrationFormParams !== null) {
                    foreach ($registrationFormParams->fieldsets as $fieldSets) {
                        $sectionFields = $fieldSets->fields;
                        foreach ($sectionFields as $fields) {
                            $field = base64_encode($fields->label);
                            $col['name'] = $field;
                            $row->form_user_id = $row->user_id;
                            $rowData [] = $sectionModel->manipulateRowData($registrationFormId, $registrationFormParams->fieldsets, $col, $row);
                        }
                    }

                }

                $rowsData [] = $rowData;
            }

            $res['data'] = $rowsData;

            $totalRows = $this->getFoundRows();

            $res['recordsFiltered'] = $totalRows;
            $res['recordsTotal'] = $totalRows;
        }

        return $res;
    }

    public function getUserTags()
    {
        $res = array();
        $section = $this->getRegistrationCustomFormSection();
        $userTags = $this->getRegistrationFormUserTags($section);

        foreach ($userTags as $optGroupLabel => $options) {
            $res[$optGroupLabel] = $options;
        }

//        var_dump($res);die();

        return $res;
    }

    /**
     * @return Section|mixed|null
     */
    public function getRegistrationCustomFormSection()
    {
        $res = null;
        $type = CustomFormSectionForm::CUSTOM_FORM;

        $sections = SectionQuery::create()
            ->filterByType($type)
            ->filterByStatus(0)
            ->find();

        foreach ($sections as $section) {
            $params = json_decode($section->getParams());
            if ($params !== null && $params->form_type === CustomFormSectionForm::REGISTRATION_FORM) {
                return $section;
            }
        }

        return $res;
    }

    public function getRegistrationFormUserTags(Section $customForm = null)
    {
        $userTags = array();
        if ($customForm !== null) {
            $lang = $this->aclHelper->session->currentLang;
            $customFormParams = json_decode($customForm->getParams());

            foreach ($customFormParams->fieldsets as $fields) {
                foreach ($fields->fields as $field) {
                    if ($field->type == SectionModel::HPFORM_TAG_GROUP ||
                        $field->type == SectionModel::HPFORM_CHECKBOX ||
                        $field->type == SectionModel::HPFORM_RADIOBOX) {

                        $optGroupLabel = isset($field->name) ? $field->label->{$lang} : $field->label;
                        $options = array();
                        if (isset($field->name)) {
                            foreach ($field->value as $keyTag => $tagObj) {
                                $label = (string)$this->getLocaleText($lang, json_encode($tagObj));
                                $options[] = array(
                                    'value' => $keyTag,
                                    'label' => $label,
                                    'attributes' => array(
                                        'data-content' => '<span class="tokenfield"><span class="token" style="margin-top: 0;"><span class="token-label" style="padding-right: 4px;">' . $label . '</span></span></span>',
                                    ),
                                );
                            }
                        } else {
                            $values = explode(",", $field->value);
                            foreach ($values as $value) {
                                $value = trim($value);
                                $options[$value] = $value;
                            }
                        }

                        $userTags[$optGroupLabel] = array(
                            'label' => $optGroupLabel,
                            'options' => $options,
                        );
                    }
                }
            }
        }

        return $userTags;
    }

    private function getUserQuery(Section $registrationForm = null, $order = null, $cols = null)
    {
        $orderBy = "ORDER BY `registered at` DESC ";

        if ($order !== null) {
            $orderBy = "ORDER BY ";

            foreach ($order as $ord) {
                $id = $ord['column'];
                $dir = $ord['dir'];
                $field = $cols[$id]['name'];

                $orderBy .= "`" . $field . "` " . $dir;
            }
        }

        if ($registrationForm !== null) {
            $registrationFormId = $registrationForm->getId();
            $sql = sprintf(self::$sqlUserList2, $registrationFormId, $orderBy);
        } else {
            $sql = sprintf(self::$sqlUserList, $orderBy);
        }

        return $sql;
    }

    // vWORK AROUND to update old user tags stored on db in form o 1,2,3.. to standard form #tag1,#tag1,...
    // call on time to update old app DB
    public function restoreTags()
    {
        if ($this->aclHelper->isLogged()) {
            $stmt = $this->dbApp->prepare("SELECT id, tags FROM user WHERE tags IS NOT NULL;");
            if ($stmt->execute()) {
                $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
                foreach ($rows as $row) {
                    $user_id = $row->id;
                    $tag_ids = "'" . implode("','", explode(",", $row->tags)) . "'";
                    $stmt2 = $this->dbApp->prepare("SELECT GROUP_CONCAT(label) as t FROM tag WHERE id IN($tag_ids);");
                    if ($stmt2->execute()) {
                        $res = $stmt2->fetchObject();
                        $tags = $res->t;

                        $stmt3 = $this->dbApp->prepare("UPDATE user SET tags = '$tags' WHERE id=$user_id");
                        if ($stmt3->execute()) {
                            echo $user_id . " >> " . $tag_ids . " | " . $tags . " [SUCCESS] - " . "UPDATE user SET tags = '$tags' WHERE id=$user_id";
//                            echo $user_id." >> ".$tag_ids. " | ".$tags ." [SUCCESS]";
                        } else {
                            echo $user_id . " >> " . $tag_ids . " | " . $tags . " [ERROR]";
                        }
                    }
                    echo "<br/>";
                }
                echo "<br/>";
            }
            die('RESTORED');
        } else {
            die('ACCESS DANIED');
        }
    }
}
