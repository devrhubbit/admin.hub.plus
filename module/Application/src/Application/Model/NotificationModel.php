<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 18/11/16
 * Time: 18:13
 */

namespace Application\Model;

use Common\Helper\AclHelper;
use Database\HubPlus\MediaLogQuery;
use Database\HubPlus\PostLogQuery;
use Database\HubPlus\PostQuery;
use Database\HubPlus\PushNotificationDeviceQuery;
use Database\HubPlus\PushNotificationQuery;
use Form\Notification\NotificationForm;
use MessagesService\Model\MessagesModel,
    MessagesService\Model\MessageModel as Message,
    MessagesService\Service\Factory as MessagesFactory,
    MessagesService\Exception\MessagesServicePushException;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Exception\PropelException;
use Rest\Exception\RestAppIdSecurityException;
use Rest\Model\DeviceRestModel;
use Rest\Model\HpRestModel,
    Rest\Model\SectionPageRestModel,
    Rest\Model\ContentPageRestModel;
use Database\HubPlus\PushNotification,
    Database\HubPlus\PushNotificationDevice;
use Common\Helper\ImageHelper;
use Rest\Model\PageRestModel;

class NotificationModel extends HpRestModel
{
    /**
     * @param $data
     * @param null $lang
     * @return \stdClass
     * @throws PropelException
     */
    //Remember to convert send_time from client timezone to server timezone
    public function appendNotification($data, $lang = null)
    {
        $application = $this->getApp();
        $lang = isset($lang) ? $lang : $application->main_contents_language;

        $res = new \stdClass();

        $deviceList = $this->getDeviceToNotify($data['to'], $lang);
        $deviceCount = count($deviceList);

        $res->deviceCount = $deviceCount;
        if ($deviceCount > 0) {
            $notificationBudget = $this->isNotificationEnabled($deviceCount);
            if ($notificationBudget) {
                $pushNotificationParams = json_decode($application->push_notification);

                list($day, $month, $year) = explode("-", $data['send_date']);
                $time = $data['send_time'];
                $sendDate = date("Y-m-d H:i:s", strtotime("$year-$month-$day $time"));

                $postLogs = array();
                $mediaLogs = array();
                switch ($data['action']) {
                    case NotificationForm::OPEN_SECTION:
                        $section_id = isset($data['section']) ? $data['section'] : null;
                        $post_id = null;
                        break;
                    case NotificationForm::OPEN_CONTENT:
                        $section_id = null;
                        $post_id = isset($data['content']) ? $data['content'] : null;
                        $postLogs = PostLogQuery::create()
                            ->filterByPostId($post_id)
                            ->find();
                        $post = PostQuery::create()
                            ->findOneById($post_id);
                        $mediaLogs = MediaLogQuery::create()
                            ->filterByMediaId($post->getCoverId())
                            ->find();
                        break;
                    default:
                        $section_id = null;
                        $post_id = null;
                        break;
                }

                $title = $this->getLocaleText($lang, $data['title']);
                $body = $this->getLocaleText($lang, $data['message']);
                $to = $data['to'] !== "" ? $data['to'] : null;

                $notificationId = $this->addNotifyToDB($body, $sendDate, $deviceCount, $lang, $title, $to, $section_id, $post_id);

                $messages = array();
                $tokenDeviceId = array();
                $contentData = $this->getData($data);
                $contentOldData = $this->getData($data, true);
                if ($contentOldData !== null) {
                    // Pezza per ridurre la dimensione del JSON nei limiti dei 2KB di Apple
                    $contentOldData->tags = array();
                    $contentOldData->actions = array();
                }

                if ($contentData !== null) {
                    $contentData->tags = array();
                }

                $userPostLogs = array();
                foreach ($postLogs as $postLog) {
                    $userPostLogs[$postLog->getUserId()] = array(
                        'liked_at' => $postLog->getLikedAt() !== null ? $postLog->getLikedAt()->getTimestamp() : null,
                        'shared_at' => $postLog->getSharedAt() !== null ? $postLog->getSharedAt()->getTimestamp() : null,
                        'share_count' => $postLog->getShareCount(),
                        'view_count' => $postLog->getViewCount(),
                    );
                }

                $userMediaLog = array();
                foreach ($mediaLogs as $mediaLog) {
                    $userMediaLog[$mediaLog->getUserId()] = array(
                        'liked_at' => $mediaLog->getLikedAt() !== null ? $mediaLog->getLikedAt()->getTimestamp() : null,
                        'shared_at' => $mediaLog->getSharedAt() !== null ? $mediaLog->getSharedAt()->getTimestamp() : null,
                        'share_count' => $mediaLog->getShareCount(),
                    );
                }

                foreach ($deviceList as $device) {
                    $content = $contentOldData;
                    $action = $data['action'];

                    if (isset($device->api_version) &&
                        ucfirst(strtolower($device->api_version)) !== DeviceRestModel::API_VERSION_UNKNOWN &&
                        version_compare($device->api_version, "1.2.0", '>=')
                    ) {
                        $content = $contentData;
                        $action = $data['action'] === NotificationForm::OPEN_SECTION ? NotificationForm::OPEN_CONTENT : $data['action'];
                    }

                    if ($data['action'] === NotificationForm::OPEN_CONTENT) {
                        if (isset($userPostLogs[$device->user_id])) {
                            $content->liked_at = $userPostLogs[$device->user_id]['liked_at'];
                            $content->like_count = count($userPostLogs);
                        }

                        if (isset($userMediaLog[$device->user_id])) {
                            $content->cover->liked_at = $userMediaLog[$device->user_id]['liked_at'];
                            $content->cover->like_count = count($userMediaLog);
                        }
                    }

                    $message = new Message($body);
                    $os = strtolower($device->os);

                    $pushParams = array();
                    switch ($os) {
                        case "ios":
                            $pushParams = array(
                                "certificate" => $pushNotificationParams->ios->certificate,
                                "passPhrase" => $pushNotificationParams->ios->pass_phrase,
//                                    "badge" => $pushNotificationCounter,
                            );
                            break;
                        case "android":
                            $pushParams = array(
                                "apiKey" => $pushNotificationParams->android->api_key,
                            );
                            break;
                    }


                    $systemIcon = json_decode($this->getApp()->system_icons);
                    $resourceIcon = null;
                    if ($systemIcon !== null) {
                        $resourceIcon = $systemIcon->app_in_app_notification_icon;
                    }

                    $pushParams["customData"] = array(
                        "type" => $action,
                        "title" => $title !== "" ? $title : null,
                        "resource_icon" => ($resourceIcon !== null && $resourceIcon !== "") ? $this->getCdnBaseurl() . ImageHelper::getAppSystemIconUrl() . $resourceIcon : null,
                        "data" => $content,
                    );

                    $pushParams["callback"] = $this->getRemoveDeviceUrl($device->token);

                    $trackingUrl = $this->getBaseurl() . $this->getController()->url()->fromRoute("message-read/put",
                            array(
                                "type" => MessagesFactory::MODE_PUSH,
                                "hashId" => MessagesModel::HASH_ID_PLACEHOLDER,
                            )
                        );

                    $message->setOption('jsonParams', json_encode($pushParams));
                    $message->setOption('to', $device->token);
                    $message->setOption('subject', $title);
                    $message->setOption('device', $os);
                    $message->setOption('title', $title);
                    $message->setOption('sendDate', $sendDate);
                    $message->setOption('trackingUrl', $trackingUrl);
                    if ($application->published) {
                        $message->setOption('mode', 'prod');
                    }

                    $messages[] = $message;
                    $tokenDeviceId[$device->token] = $device->id;
                }

                try {
                    $msgModel = new MessagesModel($this->getController()->getServiceLocator(), MessagesFactory::MODE_PUSH);
                    $hashIds = $msgModel->pullMessages($messages);

                    $res->success = true;
                    $res->notification_id = $notificationId;
                    $res->message = count($hashIds) . "/$deviceCount users of your app will be notified at " . $sendDate . ".";

                    try {
                        $this->setNotifyRead($notificationId, $hashIds, $tokenDeviceId);
                    } catch (PropelException $exception) {
                        $res->success = false;
                        $res->message = "Insert in push_notification_device table ERROR: " . $exception->getMessage();
                    }
                } catch (MessagesServicePushException $exception) {
                    $res->success = false;
                    $res->message = "PUSH NOTIFICATION ERROR: " . $exception->getMessage();
                }
            } else {
                $res->success = false;
                $res->message = "You do not have enough credit to send this notifications!";

                $backendUserModel = new BackendUsersModel($this->controller->getServiceLocator());
                $backendUserModel->sendNotificationLimitEmailToAdmins();
            }
        } else {
            $res->success = false;
            $res->message = "There are no device with selected language (" . $lang . ")";
            if ($data['to'] !== "") {
                $res->message .= " and target (" . $data['to'] . "), please try again!";
            }
        }

        //var_dump($this->aclHelper->getApp());
        //var_dump($data);
        //die();
        return $res;
    }

    /**
     * @param $type
     * @param $content_id
     * @return PushNotification[]|null|ObjectCollection
     */
    public function getPushNotifications($type, $content_id)
    {
        switch ($type) {
            case PageRestModel::CONTENT_CONTENT:
                $pushNotifications = PushNotificationQuery::create()
                    ->filterByPostId($content_id)
                    ->filterBySendDate(array('min' => date("Y-m-d H:i:s", time())))
                    ->find();
                break;
            case PageRestModel::SECTION_CONTENT:
                $pushNotifications = PushNotificationQuery::create()
                    ->filterBySectionId($content_id)
                    ->filterBySendDate(array('min' => date("Y-m-d H:i:s", time())))
                    ->find();
                break;
            default:
                $pushNotifications = null;
                break;
        }

        return $pushNotifications;
    }

    /**
     * @param $pushNotificationId
     * @param $data
     * @return bool|string
     * @throws MessagesServicePushException
     * @throws PropelException
     */
    public function updateNotification($pushNotificationId, $data)
    {
        $res = false;

        $pushNotification = PushNotificationQuery::create()->findPk($pushNotificationId);

        list($day, $month, $year) = explode("-", $data['send_date']);

        $sendDate = "$year-$month-$day " . $data['send_time'];

        $pushNotification
            ->setTitle($data['title'])
            ->setBody($data['message'])
            ->setSendDate($sendDate)
            ->save();

        $pushNotificationDevices = PushNotificationDeviceQuery::create()
            ->filterByNotificationId($pushNotificationId)
            ->find();

        $hashIds = array();
        foreach ($pushNotificationDevices as $pushNotificationDevice) {
            $hashIds[] = $pushNotificationDevice->getPushNotificationHashId();
        }

        if (count($hashIds) > 0) {
            $msgModel = new MessagesModel($this->getController()->getServiceLocator(), MessagesFactory::MODE_PUSH);
            $res = $msgModel->updatePushMessages($hashIds, $data['message'], $data['title'], $sendDate);
        }

        return $res;
    }

    /**
     * @param $pushNotificationId
     * @return bool|string
     * @throws MessagesServicePushException
     * @throws PropelException
     */
    public function removeNotification($pushNotificationId)
    {
        $res = false;

        $pushNotification = PushNotificationQuery::create()->findPk($pushNotificationId);

        $pushNotificationDevices = PushNotificationDeviceQuery::create()
            ->filterByNotificationId($pushNotificationId)
            ->find();

        $hashIds = array();
        foreach ($pushNotificationDevices as $pushNotificationDevice) {
            $hashIds[] = $pushNotificationDevice->getPushNotificationHashId();
        }

        $pushNotificationDevices->delete();
        $pushNotification->delete();

        if (count($hashIds) > 0) {
            $msgModel = new MessagesModel($this->getController()->getServiceLocator(), MessagesFactory::MODE_PUSH);
            $res = $msgModel->removePushMessagesFromQueue($hashIds);
        }

        return $res;
    }

    private function getData($data, $old = false)
    {
        $res = null;

        switch ($data['action']) {
            case NotificationForm::OPEN_SECTION:
                if (isset($data['section'])) {
                    $sectionModel = new SectionPageRestModel($this->getController());
                    if ($old === true) {
                        $res = $sectionModel->getSerializedSectionById($data['section']);
                    } else {
                        $res = $sectionModel->getSerializedSectionToSectionById($data['section']);
                    }
                }
                break;
            case NotificationForm::OPEN_CONTENT:
                if (isset($data['content'])) {
                    try {
                        $contentModel = new ContentPageRestModel($this->getController());
                    } catch (RestAppIdSecurityException $exception) {
                        // Workaround for event post notification
                        $contentModel = new ContentPageRestModel($this->getController(), false);
                        $contentModel->setAppId($this->getAppId());
                    }

                    $res = $contentModel->getSerializedContentById($data['content'], null);
                }
                break;
            default:
                $res = null;
                break;
        }

        return json_decode(json_encode($res));
    }

    /**
     * @param $message
     * @param $sendDate
     * @param $deviceCount
     * @param $lang
     * @param null $title
     * @param null $to
     * @param null $section_id
     * @param null $post_id
     * @return int|null
     * @throws PropelException
     */
    public function addNotifyToDB($message, $sendDate, $deviceCount, $lang, $title = null, $to = null, $section_id = null, $post_id = null)
    {
        $notificationId = null;
        if (!is_null($this->getDb())) {

            $section = $section_id == 0 ? null : $section_id;

            $pushNotification = new PushNotification();
            $pushNotification
                ->setLang($lang)
                ->setTitle($title)
                ->setBody($message)
                ->setSectionId($section)
                ->setPostId($post_id)
                ->setDeviceCount($deviceCount)
                ->setSendDate($sendDate)
                ->setReceiverFilter($to);

            $pushNotification->save();

            $notificationId = $pushNotification->getId();
        }
        return $notificationId;
    }

    /**
     * @param $notificationId
     * @param $hashIds
     * @param $tokenDeviceId
     *
     * @return bool
     */
    public function setNotifyRead($notificationId, $hashIds, $tokenDeviceId)
    {
        $collection = new ObjectCollection();
        foreach ($hashIds as $deviceToken => $hashId) {
            $pushNotificationDevice = new PushNotificationDevice();
            $deviceId = $tokenDeviceId[$deviceToken];

            $pushNotificationDevice
                ->setNotificationId($notificationId)
                ->setDeviceId($deviceId)
                ->setPushNotificationHashId($hashId);

            $collection->append($pushNotificationDevice);
        }

        $collection->setModel('Database\HubPlus\PushNotificationDevice');
        $collection->save();
        $res = true;

        return $res;
    }


    /**
     * @param PushNotification $pushNotification
     * @return array|null
     */
    private function getRelatedLink(PushNotification $pushNotification) {
        $data = null;
        $aclHelper = new AclHelper($this->controller->getServiceLocator());
        $lang = $aclHelper->getCurrentLanguage();

        if( !is_null( $pushNotification->getPost() ) ) {
            $post = $pushNotification->getPost();
            $data = array(
                'title' => $this->getLocaleText($lang, $post->getTitle()),
                'url' => $this->getBaseurl() . strtolower("/admin/content/" . $post->getType() . "/" . $post->getId())
            );

        } elseif ( !is_null( $pushNotification->getSection() ) ) {
            $section = $pushNotification->getSection();
            $data = array(
                'title' => $this->getLocaleText($lang, $pushNotification->getSection()->getTitle()),
                'url' => $this->getBaseurl() . strtolower("/admin/section/" . $section->getType() . "/" . $section->getId() . "/update")
            );
        }

        return $data;
    }

    /**
     * @param $draw
     * @param $start
     * @param $max_per_page
     * @param $cols
     * @param $order
     * @param $search
     * @return array
     */
    public function getList($draw, $start, $max_per_page, $cols, $order, $search)
    {
        $page = ($start / $max_per_page) + 1;
        $filter = ($search['value'] != "") ? "%".$search['value']."%" : null;

        $con = \Propel\Runtime\Propel::getConnection();
        $con->useDebug(true);

        $orderByCriteria = new Criteria();
        foreach ($order as $ord) {
            $id = $ord['column'];
            $order = strtoupper($ord['dir']);
            $field = $cols[$id]['name'];

            switch ($order) {
                case Criteria::ASC:
                    $orderByCriteria->addAscendingOrderByColumn($field);
                    break;
                case Criteria::DESC:
                    $orderByCriteria->addDescendingOrderByColumn($field);
                    break;
            }

        }

        $pushNotificationsQuery = PushNotificationQuery::create()
            ->withColumn('IF(read_message_count > 0, read_message_count/device_count, "-")', 'conversion_rate')
            ->mergeWith($orderByCriteria);

        if(isset($filter)) {
            $pushNotificationsQuery
                ->filterByTitle($filter, Criteria::LIKE)
                ->_or()
                ->filterByReceiverFilter($filter, Criteria::LIKE)
                ->_or()
                    ->usePostQuery()
                        ->filterByTitle($filter, Criteria::LIKE)
                    ->endUse()
                ->_or()
                    ->useSectionQuery()
                        ->filterByTitle($filter, Criteria::LIKE)
                    ->endUse();
        }

        $pushNotifications = $pushNotificationsQuery->paginate($page, $max_per_page);

        $data = array();
        foreach ($pushNotifications as $pushNotification) {
            $data []= array(
                'DT_RowAttr' => array(
                    'data-content-id' => $pushNotification->getId(),
                    'data-content-type' => 'notification',
                ),
                $pushNotification->getId(),
                $pushNotification->getSendDate('Y-m-d H:i:s'),
                $pushNotification->getTitle(),
                $pushNotification->getBody(),
                ($pushNotification->getReceiverFilter() != null) ? htmlentities($pushNotification->getReceiverFilter()) : "-",
                $this->getRelatedLink($pushNotification),
                $pushNotification->getDeviceCount(),
                $pushNotification->getReadMessageCount(),
                //($pushNotification->getConversionRate() != null) ? number_format($pushNotification->getConversionRate(), 3) : "-",
                $pushNotification->getVirtualColumn('conversion_rate')
            );
        }

        $res = array(
            'draw' => $draw,
            'data' => $data,
            'recordsFiltered' => $pushNotifications->getNbResults(),
            'recordsTotal' => $pushNotifications->getNbResults(),
            'sql' => $con->getLastExecutedQuery()
        );

        return $res;
    }
}