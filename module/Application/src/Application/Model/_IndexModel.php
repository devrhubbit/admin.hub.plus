<?php

namespace Application\Model;

class IndexModel {
	
	protected $db;
	
	function __construct(\Application\Controller\IndexController $controller) {
		$config = $controller->getServiceLocator()->get('config');
		
		$dsn = $config['database']['dsn'];
		$usr = $config['database']['usr'];
		$pwd = $config['database']['pwd'];
		
		
		$this->db = new \PDO($dsn,$usr,$pwd);
		$this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$this->db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
	}
	
	public function getLeaderboard() {
		
		$sqld 	= 	"SELECT SUM(votes.`value`) as pt, regions.`identifier`,COUNT(votes.`value`) as num_vote, SUM(votes.`value`) / COUNT(votes.`value`) as mean_vote
				   	FROM votes RIGHT JOIN regions ON votes.region_id=regions.id 
				   	WHERE regions.`type` = 'BEACON' GROUP BY regions.`id` ORDER BY mean_vote DESC;";
		
		$sqlt	= 	"SELECT `timestamp` FROM votes ORDER BY `timestamp` DESC LIMIT 1;";
		
		$stmtd 	= 	$this->db->query($sqld);
		$stmtt 	= 	$this->db->query($sqlt);
		
		$res = new \stdClass();
		$last_vote = $stmtt->fetchAll(\PDO::FETCH_CLASS);
		$res->last_vote = $last_vote[0]->timestamp;
		$res->data = $stmtd->fetchAll(\PDO::FETCH_CLASS);
		
		return $res;
	}
		
	public function getRates($offset, $limit, $search = null, $sort = null, $order = null) 
	{
		if($sort  == null || !in_array($sort,array("region", "device_id", "timestamp", "value")) ) $sort  = "timestamp";
		if($order == null || $order != "asc") $order = "desc";
		if($search == null) $search = "%%";
		else $search = "%$search%";
		
		$sqld 	= 	"SELECT SQL_CALC_FOUND_ROWS regions.id, identifier as region, device_id, `timestamp`, `value` 
					FROM `votes` JOIN regions ON `votes`.region_id=regions.id 
					WHERE device_id LIKE :search_d OR identifier LIKE :search_r ORDER BY $sort $order LIMIT :offset, :limit;";
		
		$sqlc	= 	"SELECT FOUND_ROWS() as total;";
		
		$stmtd 	= 	$this->db->prepare($sqld);
		$stmtd->bindParam(':search_d', 	$search, 	\PDO::PARAM_STR);
		$stmtd->bindParam(':search_r', 	$search, 	\PDO::PARAM_STR);
		$stmtd->bindParam(':offset',	$offset,	\PDO::PARAM_INT);
		$stmtd->bindParam(':limit',  	$limit, 	\PDO::PARAM_INT);
	 	$stmtd->execute();
		
		$rows	= 	$stmtd->fetchAll(\PDO::FETCH_CLASS);
		
		$stmtc 	= 	$this->db->query($sqlc);
		$total	= 	$stmtc->fetchAll(\PDO::FETCH_CLASS);
		
		return json_encode(array('total'=>$total[0]->total,'rows'=>$rows));
	}
		
	public function getTracks($offset, $limit, $search = null, $sort = null, $order = null) 
	{
		if($sort  == null || !in_array($sort,array("region", "device_id", "timestamp", "event", "type")) ) $sort  = "timestamp";
		if($order == null || $order != "asc") $order = "desc";
		if($search == null) $search = "%%";
		else $search = "%$search%";
		
		$sqld 	= 	"SELECT SQL_CALC_FOUND_ROWS regions.id, identifier as region, device_id, `timestamp`, `event`, `type`, lat, lon 
					FROM `tracks` JOIN regions ON `tracks`.region_id=regions.id 
					WHERE device_id LIKE :search_d OR identifier LIKE :search_r ORDER BY $sort $order LIMIT :offset, :limit;";
		
		$sqlc	= 	"SELECT FOUND_ROWS() as total;";
		
		$stmtd 	= 	$this->db->prepare($sqld);
		$stmtd->bindValue(':search_d', 	$search, 	\PDO::PARAM_STR);
		$stmtd->bindValue(':search_r', 	$search, 	\PDO::PARAM_STR);
		$stmtd->bindValue(':offset',	$offset,	\PDO::PARAM_INT);
		$stmtd->bindValue(':limit',  	$limit, 	\PDO::PARAM_INT);
	 	$stmtd->execute();
		
		$rows	= 	$stmtd->fetchAll(\PDO::FETCH_CLASS);
		
		$stmtc 	= 	$this->db->query($sqlc);
		$total	= 	$stmtc->fetchAll(\PDO::FETCH_CLASS);
		
		foreach ($rows as $row) {
			$row->where = "<a href='http://maps.google.com/maps?z=19&q=$row->lat,$row->lon' target='_blank'>$row->event @ pos</a>";
		}
		
		return json_encode(array('total'=>$total[0]->total,'rows'=>$rows));
	}
		
	public function getUser($username, $password) {
		$res = null;
		$sql = "SELECT * FROM users WHERE username = :u AND password = :p LIMIT 1;";	
		
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':u', $username, 		\PDO::PARAM_STR);
		$stmt->bindParam(':p', md5($password), 	\PDO::PARAM_STR);
		$stmt->execute();
		
		$rows = $stmt->fetchAll(\PDO::FETCH_CLASS);
		if(count($rows) > 0) {
			$res = new \stdClass();
			$res->username 	= $rows[0]->username;
			$res->role 		= $rows[0]->role;
			$res->lastlogin = $rows[0]->last_login;
		}
		
		return $res;
	}
	
	public function getStats($offset, $limit, $search = null, $sort = null, $order = null) {
		if($sort  == null || !in_array($sort,array("num_event", "device_id", "event")) ) $sort  = "device_id";
		if($order == null || $order != "asc") $order = "desc";
		if($search == null) $search = "%%";
		else $search = "%$search%";
		
		$sqld 	=  "SELECT SQL_CALC_FOUND_ROWS `device_id`, `event`, count('event') as num_event FROM tracks 
					WHERE device_id LIKE :search_d OR event LIKE :search_e 
					GROUP BY `device_id`, `event` ORDER BY $sort $order LIMIT :offset, :limit ;";
		
		$sqlc	=  "SELECT FOUND_ROWS() as total;";
		
		$stmtd 	= 	$this->db->prepare($sqld);
		$stmtd->bindParam(':search_d', 	$search, 	\PDO::PARAM_STR);
		$stmtd->bindParam(':search_e', 	$search, 	\PDO::PARAM_STR);
		$stmtd->bindParam(':offset',	$offset,	\PDO::PARAM_INT);
		$stmtd->bindParam(':limit',  	$limit, 	\PDO::PARAM_INT);
		$stmtd->execute();
		
		$rows	= 	$stmtd->fetchAll(\PDO::FETCH_CLASS);
		
		$stmtc 	= 	$this->db->query($sqlc);
		$total	= 	$stmtc->fetchAll(\PDO::FETCH_CLASS);
		
		return json_encode(array('total'=>$total[0]->total,'rows'=>$rows));
	}
}
	
	

?>