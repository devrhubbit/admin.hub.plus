<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 17/06/16
 * Time: 16:01
 */

namespace Application\Model;

use Common\Helper\ImageHelper;
use Database\HubPlus\GalleryQuery;
use Database\HubPlus\MediaQuery;
use Database\HubPlus\Post;
use Database\HubPlus\PostActionQuery;
use Database\HubPlus\PostConnector;
use Database\HubPlus\PostConnectorQuery;
use Database\HubPlus\PostPoi;
use Database\HubPlus\PostPoiQuery;
use Database\HubPlus\PostQuery;
use Database\HubPlus\PushNotification;
use Database\HubPlus\PushNotificationQuery;
use Form\Content\ContentForm;
use Common\Helper\SecurityHelper;
use Form\LayoutForm;
use Form\Notification\NotificationForm;
use MessagesService\Exception\MessagesServicePushException;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;
use Rest\Exception\RestRemoteException;
use Rest\Exception\RestSignatureSecurityException;

class ContentModel extends HpModel
{
    const CONTENT_STATUS_PUBLISHED = 0;
    const CONTENT_STATUS_DRAFT = 1;

    private static $sqlAppContentsList = "SELECT * FROM `post` WHERE deleted_at IS NULL ORDER BY title";
    private static $sqlGetContentLayout = "SELECT `layout` FROM `post` WHERE id = :id";
    private static $sqlSetContentLayout = "UPDATE `post` SET `layout` = :layout, `template_id` = :temp_id, updated_at = NOW() WHERE `id` = :id";
    private static $sqlGetPublishedContents = "SELECT `id`, `title` FROM `post` WHERE deleted_at IS NULL AND status = 0 ORDER BY title";

    private static $sqlEventCreate = "INSERT INTO `post_event`
                                          (`id`, `post_id`, `start_at`, `end_at`, `created_at`, `updated_at`, `deleted_at` )
                                       VALUES
                                          (NULL, :post_id, :start_at, :end_at, NOW(), NOW(), NULL)";

    private static $sqlPersonCreate = "INSERT INTO `post_person`
                                          (`id`, `post_id`, `firstname`, `lastname`, `birthday`, `death`,
                                           `created_at`, `updated_at`, `deleted_at` )
                                       VALUES
                                          (NULL, :post_id, :firstname, :lastname, :birthday, :death, NOW(), NOW(), NULL)";

    private static $sqlContentDetail = "SELECT * FROM `post` WHERE deleted_at IS NULL AND id = :id";
    private static $sqlContentList = "SELECT SQL_CALC_FOUND_ROWS * FROM `post` WHERE `deleted_at` IS NULL AND (title LIKE :title OR description LIKE :descr OR tags LIKE :tags) %s LIMIT :start, :length";
    private static $sqlContentSimpleList = "SELECT id, type, title FROM `post` WHERE `deleted_at` IS NULL AND `status` = 0 ORDER BY type, title;";

    private static $sqlContentDelete = "UPDATE `post` SET deleted_at = NOW(), updated_at = NOW() WHERE `id` = :id";
    private static $sqlPoiContentDelete = "UPDATE `post_poi` SET deleted_at = NOW(), updated_at = NOW() WHERE `post_id` = :id";
    private static $sqlPersonContentDelete = "UPDATE `post_person` SET deleted_at = NOW(), updated_at = NOW() WHERE `post_id` = :id";
    private static $sqlEventContentDelete = "UPDATE `post_event` SET deleted_at = NOW(), updated_at = NOW() WHERE `post_id` = :id";

    private static $sqlPersonUpdate = "UPDATE `post_person` SET
                                          `firstname` = :firstname, `lastname` = :lastname,
                                          `birthday` = :birthday, `death` = :death,
                                          `updated_at` = NOW()
                                        WHERE deleted_at IS NULL AND `post_id` = :post_id;";

    private static $sqlEventUpdate = "UPDATE `post_event` SET
                                          `start_at` = :start_at, `end_at` = :end_at, `updated_at` = NOW()
                                        WHERE deleted_at IS NULL AND `post_id` = :post_id;";


    private $lastContentId = 0;
    private static $selectedType = null;

    public static function setSelectedType($type)
    {
        self::$selectedType = strtoupper($type);
    }

    public static function getSelectedType()
    {
        return self::$selectedType;
    }

    public function getLastContentId()
    {
        return $this->lastContentId;
    }

    public function getContentTypes($selected_type = null)
    {
        $config = $this->serviceLocator->get('config');
        $setting = $this->aclHelper->getCategorySetting();

        foreach ($setting->content_types as $type) {
            $config['content_types'][$type]["enabled"] = true;
        }

        if ($selected_type != null) {
            $config['content_types'][strtoupper($selected_type)]["active"] = true;
        }

        return $config['content_types'];
    }

    public function isValidType($type)
    {
        $config = $this->serviceLocator->get('config');
        return array_key_exists(strtoupper($type), $config['content_types']);
    }

    public function getByType($type)
    {
        $config = $this->serviceLocator->get('config');
        return $config['content_types'][strtoupper($type)];
    }

    public function getContentById($type, $id)
    {
        $data = array();
        if (!is_null($this->dbApp)) {
            $stmt = $this->dbApp->prepare(self::$sqlContentDetail);

            $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
            $stmt->execute();

            $content = $stmt->fetchObject();

            $data = $this->flatLayoutJson($content->layout, null, $content->template_id, $type);
            $row_data = json_decode($content->row_data);

            $eP = array();
            foreach ($row_data as $rd) {
                if (isset($rd->extraParam)) {
                    $eP [] = array("key" => $rd->key, "value" => $rd->value);
                } else {
                    $data[$rd->key] = $rd->value;
                }
            }

            // Multi language fields
            $lang = $this->aclHelper->session->currentLang;
            $title = $this->getLocaleText($lang, $content->title);
            $description = $this->getLocaleText($lang, $content->description);
            $body = $this->getLocaleText($lang, $content->body);
            $data["subtitle"] = $content->subtitle;
            switch ($content->type) {
                case ContentForm::GENERIC:
                case ContentForm::WEB:
                    $data["subtitle"] = $this->getLocaleText($lang, $content->subtitle);
                    break;
                case ContentForm::EVENT:
                    $data["content_push_notification_enable_hidden"] = (isset($data["content_push_notification_enable"]) && $data["content_push_notification_enable"] !== "") ? "" . (int)$data["content_push_notification_enable"] : "0";

                    if ($content->status === self::CONTENT_STATUS_PUBLISHED && isset($data['content_push_notification_id']) && is_array($data['content_push_notification_id']) && $data["content_push_notification_enable"]) {
                        $pushNotifications = PushNotificationQuery::create()
                            ->filterById($data['content_push_notification_id'])
                            ->find();

                        $data['content_push_notification_id'] = json_encode($data['content_push_notification_id']);

                        foreach ($pushNotifications as $pushNotification) {
                            $data["content_push_notification_send_date"] = $pushNotification->getSendDate()->format("Y-m-d");
                            $data["content_push_notification_send_time"] = $pushNotification->getSendDate()->format("H:i:s");
                        }
                    }
                    break;
            }

            $data["content_status"] = $content->status;
            $data["content_lang"] = $lang;
            $data["title"] = $title;
            $data["description"] = $description;
            $data["body"] = $body;

            $data["reload_content"] = 0;

            $data['extra-params'] = json_encode($eP);
            $data["type"] = $content->type;
            $data["cover_id"] = $content->cover_id;
            $data["tags"] = $content->tags;
            $data["user_tags"] = json_decode($content->tags_user);
            $data["resource"] = $this->getResource($content->type, $content->id);

            $postConnector = $this->getContentConnector($id);
            if ($postConnector) {
                $data['connector'] = $postConnector->getType();
                $data['provider'] = $postConnector->getProviderId();
                $data['connector_base_url'] = $postConnector->getBaseurl();
                $data['remote_post_id'] = $postConnector->getRemotePostId();
            } else {
                $data['connector'] = LayoutForm::CONNECTOR_TYPE_HUB;
            }
        }

        return $data;
    }

    public function getLastContents($start, $length)
    {
        $rows = null;

        if (!is_null($this->dbApp)) {
            $orderBy = " ORDER BY  `updated_at` DESC ";
            $sql = sprintf(self::$sqlContentList, $orderBy);

            $stmt = $this->dbApp->prepare($sql);

            $filter = "%";

            $stmt->bindParam(":start", $start, \PDO::PARAM_INT);
            $stmt->bindParam(":length", $length, \PDO::PARAM_INT);
            $stmt->bindParam(":title", $filter, \PDO::PARAM_STR);
            $stmt->bindParam(":descr", $filter, \PDO::PARAM_STR);
            $stmt->bindParam(":tags", $filter, \PDO::PARAM_STR);

            $stmt->execute();

            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            $application = $this->aclHelper->getApp();
            foreach ($rows as $row) {
                $mediaUri = "";
                $mediaModel = new MediaModel($this->serviceLocator);
                if (!is_null($row->cover_id) && $row->cover_id > 0) {
                    $media = $mediaModel->getMediaById($row->cover_id);
                    if (isset($media->uri)) $mediaUri = $media->uri;
                }

                // Multi language fields
                $row->title = $this->getLocaleText($application->main_contents_language, $row->title);
                $row->description = $this->getLocaleText($application->main_contents_language, $row->description);

                $row->media_uri = $mediaUri;
                $row->url = "/admin/content/" . strtolower($row->type) . "/" . $row->id . "/";
            }
        }

        return $rows;
    }

    public function getContents($draw, $start, $length, $cols, $order, $search)
    {
        $res = array('draw' => $draw, 'data' => array());

        if (!is_null($this->dbApp)) {
            $orderBy = "ORDER BY ";

            foreach ($order as $ord) {
                $id = $ord['column'];
                $dir = $ord['dir'];
                $field = $cols[$id]['name'];

                $orderBy .= "`" . $field . "` " . $dir;
            }

            $sql = sprintf(self::$sqlContentList, $orderBy);

            $stmt = $this->dbApp->prepare($sql);

            $filter = "%";
            if ($search['value'] != "") $filter = "%" . $search['value'] . "%";

            $stmt->bindParam(":start", $start, \PDO::PARAM_INT);
            $stmt->bindParam(":length", $length, \PDO::PARAM_INT);
            $stmt->bindParam(":title", $filter, \PDO::PARAM_STR);
            $stmt->bindParam(":descr", $filter, \PDO::PARAM_STR);
            $stmt->bindParam(":tags", $filter, \PDO::PARAM_STR);

            $stmt->execute();

            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            $application = $this->aclHelper->getApp();
            foreach ($rows as $row) {
                $label = $this->getTypeLabel($row->type, 'content_types');
                $mediaUri = "";
                $mediaModel = new MediaModel($this->serviceLocator);
                if (!is_null($row->cover_id) && $row->cover_id > 0) {
                    $media = $mediaModel->getMediaById($row->cover_id);
                    if (isset($media->uri)) $mediaUri = $media->uri;
                }

                $res['data'][] = array(
                    'DT_RowAttr' => array(
                        'data-content-id' => $row->id,
                        'data-content-type' => $row->type,
                    ),
                    $row->id,
                    $mediaUri,
                    $row->updated_at,
                    $label,
                    $this->getLocaleText($application->main_contents_language, $row->title),
                    $this->getLocaleText($application->main_contents_language, $row->description),
                    $this->renderTags($row->tags),
                    $this->renderTags($row->tags_user),
                    $row->status,
                    '<button type="button" class="delete_icon btn btn-default" content-title="' . $this->getLocaleText($application->main_contents_language, $row->title) . '" content-id="' . $row->id . '" content-type="' . $row->type . '"><i class="fa fa-trash-o fa-2x" aria-hidden="true"></i></button>',
                );
            }

            $totalRows = $this->getFoundRows();

            $res['recordsFiltered'] = $totalRows;
            $res['recordsTotal'] = $totalRows;
        }

        return $res;
    }

    private function renderTags($tags)
    {
        $tagDecoded = json_decode($tags);
        if ($tagDecoded !== null) {
            $tags = $tagDecoded;
        } else {
            $tags = explode(",", $tags);
        }

        $renderTags = array();
        foreach ($tags as $tag) {
            if ($tag !== "") {
                $renderTags[] = '<div><span class="label label-primary">' . $tag . '</span></div>';
            }
        }

        $userTagsString = "";
        if (count($renderTags) > 0) {
            $userTagsString = implode("", $renderTags);
        }

        return $userTagsString;
    }

    public function getContentSimpleList()
    {
        $data = array();
        $config = $this->serviceLocator->get('config');

        if (!is_null($this->dbApp)) {
            $stmt = $this->dbApp->prepare(self::$sqlContentSimpleList);

            if ($stmt->execute()) {
                $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
                $application = $this->aclHelper->getApp();
                foreach ($rows as $row) {
                    $type = $config['content_types'][$row->type]["label"];
                    $data [$row->id] = $type . " > " . $this->getLocaleText($application->main_contents_language, $row->title);
                }
            }
        }

        return $data;
    }

    private function getResource($type, $id)
    {
        return "/" . strtolower($type) . "/" . $id;
    }

    /**
     * @param $id
     * @param integer $s
     * @param NotificationModel $notificationModel
     * @return bool|string
     * @throws MessagesServicePushException
     * @throws PropelException
     */
    public function setContentStatus($id, $s, NotificationModel $notificationModel)
    {
        $res = true;

        $post = PostQuery::create()
            ->findPk($id);
        $post->setStatus($s)->save();

        if ($s === self::CONTENT_STATUS_PUBLISHED) {
            $postConnector = PostConnectorQuery::create()
                ->findOneByPostId($id);

            if ($postConnector && $postConnector->getType() !== LayoutForm::CONNECTOR_TYPE_HUB) {
                $this->callRemoteConnector($postConnector);
            }

            // Create push notification
            $content = PostQuery::create()->findPk($id);
            $rowData = (array)json_decode($content->getRowData());

            $eP = array();
            $data = array(
                'title' => $content->getTitle(),
                'description' => $content->getDescription(),
            );
            foreach ($rowData as $rd) {
                if (isset($rd->extraParam)) {
                    $eP [] = array("key" => $rd->key, "value" => $rd->value);
                } else {
                    $data[$rd->key] = $rd->value;
                }
            }

            if (isset($data['content_push_notification_enable']) && $data['content_push_notification_enable'] === true) {
                $createPushNotificationResponse = $this->createPushNotification($id, $data, $rowData, $notificationModel, $content);
            }
        } else {
            // Remove push notification not sent
            $pushNotifications = $notificationModel->getPushNotifications("content", $id);

            if (isset($pushNotifications) && $pushNotifications->count() > 0) {
                foreach ($pushNotifications as $pushNotification) {
                    $res = $notificationModel->removeNotification($pushNotification->getId());;
                }
            }
        }

        return $res;
    }

    public function deleteContent($id, $type)
    {
        $stmt = $this->dbApp->prepare(self::$sqlContentDelete);
        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
        $res = $stmt->execute();

        if ($res) {
            $actModel = new ActionModel($this->serviceLocator);
            $actModel->deleteActionAfterContentDelete($id);

            switch ($type) {
                case ContentForm::POI:
                    $res = $this->deletePoiContent($id);
                    break;
                case ContentForm::PERSON:
                    $res = $this->deletePersonContent($id);
                    break;
                case ContentForm::EVENT:
                    $res = $this->deleteEventContent($id);
                    break;
            }
        }

        return $res;
    }

    public function deletePoiContent($id)
    {
        $stmt = $this->dbApp->prepare(self::$sqlPoiContentDelete);

        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        return $stmt->execute();
    }

    public function deletePersonContent($id)
    {
        $stmt = $this->dbApp->prepare(self::$sqlPersonContentDelete);

        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        return $stmt->execute();
    }

    public function deleteEventContent($id)
    {
        $stmt = $this->dbApp->prepare(self::$sqlEventContentDelete);

        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        return $stmt->execute();
    }

    /**
     * @param $data
     * @return bool
     * @throws PropelException
     */
    public function createContent($data)
    {
        $rowData = $this->createRowData($data);
        switch ($data["type"]) {
            case ContentForm::PERSON :
                $insertContent = $this->createPersonContent($data, $rowData);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertContentConnector($this->lastContentId, $data) : $insertConnector = true;
                return ($insertContent && $insertConnector);
                break;
            case ContentForm::EVENT :
                $insertContent = $this->createEventContent($data, $rowData);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertContentConnector($this->lastContentId, $data) : $insertConnector = true;
                return ($insertContent && $insertConnector);
                break;
            case ContentForm::POI :
                $insertContent = $this->createPoiContent($data, $rowData);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertContentConnector($this->lastContentId, $data) : $insertConnector = true;
                return ($insertContent && $insertConnector);
                break;
            case ContentForm::GENERIC :
                $insertContent = $this->createGenericContent($data, $rowData);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertContentConnector($this->lastContentId, $data) : $insertConnector = true;
                return ($insertContent && $insertConnector);
                break;
            case ContentForm::WEB :
                $insertContent = $this->createWebContent($data, $rowData);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertContentConnector($this->lastContentId, $data) : $insertConnector = true;
                return ($insertContent && $insertConnector);
                break;
            default:
                return false;
        }
    }

    public function getContentConnector($post_id)
    {
        $postConnector = PostConnectorQuery::create()
            ->filterByPostId($post_id)
            ->findOne();

        return $postConnector;
    }

    /**
     * @param $post_id
     * @param $data
     * @return bool
     * @throws PropelException
     */
    public function insertContentConnector($post_id, $data)
    {
        $postConnector = PostConnectorQuery::create()
            ->filterByPostId($post_id)
            ->findOneOrCreate();

        $postConnector
            ->setPostId($post_id)
            ->setProviderId($data['provider'])
            ->setType($data['connector'])
            ->setBaseurl($data['connector_base_url'])
            ->setRemotePostId(isset($data['remote_post_id']) ? $data['remote_post_id'] : null)
            ->save();

        if ($data['connector'] !== LayoutForm::CONNECTOR_TYPE_HUB && $postConnector->getPost()->getStatus() === 0) {
            $this->callRemoteConnector($postConnector);
        }

        $result = true;

        return $result;
    }

    /**
     * @param $postId
     * @return bool
     * @throws PropelException
     */
    public function deletePostConnector($postId)
    {
        $postsConnector = PostConnectorQuery::create()
            ->filterByPostId($postId)
            ->find();

        foreach ($postsConnector as $postConnector) {
            $postConnector->delete();
        }
        return true;
    }

    /**
     * @param $id
     * @param $data
     * @param $notificationModel
     * @return bool
     * @throws MessagesServicePushException
     * @throws PropelException
     */
    public function saveContent($id, $data, $notificationModel)
    {
        $rowData = $this->createRowData($data);
        switch ($data["type"]) {
            case ContentForm::PERSON :
                $updateContent = $this->updatePersonContent($id, $data, $rowData);
                $updateConnector = $this->menageConnector($id, $data);
                return ($updateContent && $updateConnector);
                break;
            case ContentForm::EVENT :
                $updateContent = $this->updateEventContent($id, $data, $rowData, $notificationModel);
                $updateConnector = $this->menageConnector($id, $data);
                return ($updateContent && $updateConnector);
                break;
            case ContentForm::POI :
                $updateContent = $this->updatePoiContent($id, $data, $rowData);
                $updateConnector = $this->menageConnector($id, $data);
                return ($updateContent && $updateConnector);
                break;
            case ContentForm::GENERIC :
                $updateContent = $this->updateGenericContent($id, $data, $rowData);
                $updateConnector = $this->menageConnector($id, $data);
                return ($updateContent && $updateConnector);
                break;
            case ContentForm::WEB :
                $updateContent = $this->updateWebContent($id, $data, $rowData);
                $updateConnector = $this->menageConnector($id, $data);
                return ($updateContent && $updateConnector);
                break;
            default:
                return false;
        }
    }

    private function updateContent($id, $data, $layout, $rowData = array())
    {
        $encodedLayout = json_encode($layout);
        $search = strtolower($data["title"] . " " . $data["subtitle"] . " " . $data["description"] . " " . strip_tags(html_entity_decode($data["body"])));
        $galleryIds = explode(",", $data['gallery_ids']);
        $cover_id = ($data["cover_id"] !== '' && in_array($data["cover_id"], $galleryIds)) ? $data["cover_id"] : null;
        $template_id = $data['template_id'] !== '' ? $data['template_id'] : null;

        try {
            $content = PostQuery::create()
                ->findOneById($id);

            $slug = ($content->getSlug() === null || $content->getSlug() === "") ? SecurityHelper::createSlug($data["title"]) : $content->getSlug();

            $this->generateCoverImages($layout, $cover_id, $content);

            // Multi language fields
            $title = $this->setMultiLanguageObject($data["content_lang"], $content->getTitle(), $data["title"]);
            $description = $this->setMultiLanguageObject($data["content_lang"], $content->getDescription(), $data["description"]);
            $body = $this->setMultiLanguageObject($data["content_lang"], $content->getBody(), $data["body"]);
            $userTags = $data['user_tags'] !== null && $data['user_tags'] !== "" ? json_encode($data['user_tags']) : null;

            switch ($content->getType()) {
                case ContentForm::GENERIC:
                case ContentForm::WEB:
                    $subtitle = $this->setMultiLanguageObject($data["content_lang"], $content->getSubtitle(), $data['subtitle']);
                    break;
                default:
                    $subtitle = $data["subtitle"];
                    break;
            }

            $content
                ->setType($data["type"])
                ->setCoverId($cover_id)
                ->setSlug($slug)
                ->setTitle($title)
                ->setSubtitle($subtitle)
                ->setDescription($description)
                ->setBody($body)
                ->setSearch($search)
                ->setTemplateId($template_id)
                ->setLayout($encodedLayout)
                ->setTags($data["tags"])
                ->setTagsUser($userTags)
                ->setRowData(json_encode($rowData));
            $content->save();

            $res = true;

            $deletedActions = $data['deleted_actions'];
            $actionModel = new ActionModel($this->serviceLocator);
            if ($actionModel->deletedActions($deletedActions)) {
                $res = $actionModel->generateActions($id, json_decode($data['actions']), $data["content_lang"]);
            }
        } catch (PropelException $exception) {
            $res = false;
        }

        return $res;
    }

    private function createRowData($data)
    {
        $rowData = array();
        if ($data['extra-params'] != "") {
            $extraParams = json_decode($data['extra-params']);

            foreach ($extraParams as $eP) {
                $temp['key'] = $eP->key;
                $temp['value'] = $eP->value;
                $temp['extraParam'] = true;

                $rowData[] = $temp;
            }
        }

        return $rowData;
    }

    private function insertContent($data, $layout, $rowData = array())
    {
        $slug = SecurityHelper::createSlug($data["title"]);
        $search = strtolower($data["title"] . " " . $data["subtitle"] . " " . $data["description"] . " " . strip_tags(html_entity_decode($data["body"])));
        $coverId = $data["cover_id"] !== '' ? $data["cover_id"] : null;
        $template_id = $data['template_id'] !== '' ? $data['template_id'] : null;

        try {
            // Multi language fields
            $title = $this->setMultiLanguageObject($data["content_lang"], $data["title"], $data["title"]);
            $description = $this->setMultiLanguageObject($data["content_lang"], $data["description"], $data["description"]);
            $body = $this->setMultiLanguageObject($data["content_lang"], $data["body"], $data["body"]);
            $userTags = $data['user_tags'] !== null && $data['user_tags'] !== "" ? json_encode($data['user_tags']) : null;

            $content = new Post();

            $this->generateCoverImages($layout, $coverId, $content);

            switch ($content->getType()) {
                case ContentForm::GENERIC:
                case ContentForm::WEB:
                    $subtitle = $this->setMultiLanguageObject($data["content_lang"], $data['subtitle'], $data['subtitle']);
                    break;
                default:
                    $subtitle = $data["subtitle"];
                    break;
            }

            $content
                ->setType($data["type"])
                ->setStatus(self::CONTENT_STATUS_DRAFT)
                ->setAuthorId($this->aclHelper->getUser()->id)
                ->setCoverId($coverId)
                ->setSlug($slug)
                ->setTitle($title)
                ->setSubtitle($subtitle)
                ->setDescription($description)
                ->setBody($body)
                ->setSearch($search)
                ->setTemplateId($template_id)
                ->setLayout(json_encode($layout))
                ->setTags($data["tags"])
                ->setTagsUser($userTags)
                ->setRowData(json_encode($rowData));
            $content->save();

            $this->lastContentId = $content->getId();

            $res = true;

            $deletedActions = $data['deleted_actions'];
            $actionModel = new ActionModel($this->serviceLocator);
            if ($actionModel->deletedActions($deletedActions)) {
                $res = $actionModel->generateActions($content->getId(), json_decode($data['actions']), $data["content_lang"]);
            }
        } catch (PropelException $exception) {
            $res = false;
        }

        return $res;
    }

    /**
     * @param $data
     * @param $rowData
     * @return bool
     */
    private function createEventContent($data, $rowData)
    {
        $this->parseTag($data);
        $layout = $this->parseLayout($data);

        $rowData[] = array('key' => 'start-date', 'value' => $data['start-date']);
        $rowData[] = array('key' => 'end-date', 'value' => $data['end-date']);
        $rowData[] = array('key' => 'content_push_notification_enable', 'value' => $data['content_push_notification_enable'] === "0" ? false : true);
        $rowData[] = array('key' => 'content_push_notification_send_date', 'value' => ($data['content_push_notification_enable'] !== "0" && isset($data['content_push_notification_send_date']) && $data['content_push_notification_send_date'] !== "") ? $data['content_push_notification_send_date'] : null);
        $rowData[] = array('key' => 'content_push_notification_send_time', 'value' => ($data['content_push_notification_enable'] !== "0" && isset($data['content_push_notification_send_time']) && $data['content_push_notification_send_time'] !== "") ? $data['content_push_notification_send_time'] : null);

        $startDate = $data["start-date"];
        $endDate = $data["end-date"];

        $data['subtitle'] = ($startDate === $endDate) ? $startDate : $startDate . " / " . $endDate;

        if ($this->insertContent($data, $layout, $rowData)) {
            $postId = $this->lastContentId;
            $stmt = $this->dbApp->prepare(self::$sqlEventCreate);

            $stmt->bindParam(":post_id", $postId, \PDO::PARAM_INT);
            $stmt->bindParam(":start_at", $startDate, \PDO::PARAM_STR);
            $stmt->bindParam(":end_at", $endDate, \PDO::PARAM_STR);

            return $stmt->execute();
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @param $data
     * @param $rowData
     * @param NotificationModel $notificationModel
     * @return bool|string
     * @throws MessagesServicePushException
     * @throws PropelException
     */
    private function updateEventContent($id, $data, $rowData, NotificationModel $notificationModel)
    {
        $this->parseTag($data);
        $layout = $this->parseLayout($data);

        $rowData[] = array('key' => 'start-date', 'value' => $data['start-date']);
        $rowData[] = array('key' => 'end-date', 'value' => $data['end-date']);
        $rowData[] = array('key' => 'content_push_notification_enable', 'value' => $data['content_push_notification_enable_hidden'] === "0" ? false : true);
        $rowData[] = array('key' => 'content_push_notification_id', 'value' => isset($data['content_push_notification_id']) && $data['content_push_notification_enable_hidden'] === "1" ? json_decode($data['content_push_notification_id']) : null);

        $content = PostQuery::create()->findPk($id);
        if ($data['content_push_notification_enable_hidden'] !== $data['content_push_notification_enable'] && $data['content_push_notification_enable_hidden'] === "1") {

            $row_data = json_decode($content->getRowData());

            $eP = array();
            foreach ($row_data as $rd) {
                if (isset($rd->extraParam)) {
                    $eP [] = array("key" => $rd->key, "value" => $rd->value);
                } else {
                    $data[$rd->key] = $rd->value;
                }
            }
        }

        $rowData[] = array('key' => 'content_push_notification_send_date', 'value' => ($data['content_push_notification_enable_hidden'] === "1" && isset($data['content_push_notification_send_date']) && $data['content_push_notification_send_date'] !== "") ? $data['content_push_notification_send_date'] : null);
        $rowData[] = array('key' => 'content_push_notification_send_time', 'value' => ($data['content_push_notification_enable_hidden'] === "1" && isset($data['content_push_notification_send_time']) && $data['content_push_notification_send_time'] !== "") ? $data['content_push_notification_send_time'] : null);

        $startDate = $data["start-date"];
        $endDate = $data["end-date"];

        $data['subtitle'] = ($startDate === $endDate) ? $startDate : $startDate . " / " . $endDate;
//        var_dump($data);
//        var_dump($rowData);
//        die();

        if ($this->updateContent($id, $data, $layout, $rowData)) {
            $stmt = $this->dbApp->prepare(self::$sqlEventUpdate);

            $stmt->bindParam(":post_id", $id, \PDO::PARAM_INT);
            $stmt->bindParam(":start_at", $startDate, \PDO::PARAM_STR);
            $stmt->bindParam(":end_at", $endDate, \PDO::PARAM_STR);

            $res = $stmt->execute();
            if ($res && (int)$data['content_status'] === self::CONTENT_STATUS_PUBLISHED && $data['content_push_notification_enable_hidden'] === "1") {
                if (isset($data['content_push_notification_id']) && $data['content_push_notification_id'] !== "") {
                    $pushNotificationIds = json_decode($data['content_push_notification_id']);
//                    $rowData[] = array('key' => 'content_push_notification_id', 'value' => json_encode($pushNotificationIds));
                    $pushNotifications = PushNotificationQuery::create()
                        ->filterById($pushNotificationIds)
                        ->filterByLang($data['content_lang'])
                        ->find();

                    foreach ($pushNotifications as $pushNotification) {
                        $data['content_push_notification_send_date'] = $pushNotification->getSendDate()->format("Y-m-d");
                        $data['content_push_notification_send_time'] = $pushNotification->getSendDate()->format("H:i:s");
                        if ($pushNotification->getSendDate()->getTimestamp() > time()) {
                            // Push notification not send jet, then Update push notification
                            $res = $this->updatePushNotification($pushNotification, $data, $notificationModel);
                        }
                    }

                    $content
                        ->setRowData(json_encode($rowData))
                        ->save();
                } else {
                    // Create push notification
                    $res = $this->createPushNotification($id, $data, $rowData, $notificationModel, $content);
                }
            } elseif(isset($data['content_push_notification_id']) && $data['content_push_notification_id'] !== "" && $data['content_push_notification_enable_hidden'] === "0") {
                // Remove Notification
                $pushNotificationIds = json_decode($data['content_push_notification_id']);
                $pushNotifications = PushNotificationQuery::create()
                    ->filterById($pushNotificationIds)
                    ->filterByLang($data['content_lang'])
                    ->find();

                foreach ($pushNotifications as $pushNotification) {
                    if ($pushNotification->getSendDate()->getTimestamp() > time()) {
                        // Push notification not send jet, then Cancel push notification
                        $res = $this->removePushNotification($pushNotification, $notificationModel);
                    }
                }
            }

            return $res;
        } else {
            return false;
        }
    }

    private function createPersonContent($data, $rowData)
    {
        $this->parseTag($data);
        $layout = $this->parseLayout($data);

        $rowData[] = array('key' => 'firstname', 'value' => $data['firstname']);
        $rowData[] = array('key' => 'lastname', 'value' => $data['lastname']);
        $rowData[] = array('key' => 'birthday', 'value' => $data['birthday']);
        $rowData[] = array('key' => 'death', 'value' => $data['death']);

        $data['title'] = $data['firstname'] . " " . $data['lastname'];
        $data['subtitle'] = (($data['birthday'] != "") ? $data['birthday'] : "-") . " / " .
            (($data['death'] != "") ? $data['death'] : "-");

        if ($this->insertContent($data, $layout, $rowData)) {
            $stmt = $this->dbApp->prepare(self::$sqlPersonCreate);

            $birthday = $data["birthday"] !== "" ? $data["birthday"] : null;
            $death = $data["death"] !== "" ? $data["death"] : null;

            $stmt->bindParam(":post_id", $this->lastContentId, \PDO::PARAM_INT);
            $stmt->bindParam(":firstname", $data["firstname"], \PDO::PARAM_STR);
            $stmt->bindParam(":lastname", $data["lastname"], \PDO::PARAM_STR);
            $stmt->bindParam(":birthday", $birthday, \PDO::PARAM_STR);
            $stmt->bindParam(":death", $death, \PDO::PARAM_STR);

            return $stmt->execute();
        } else {
            return false;
        }
    }

    private function updatePersonContent($id, $data, $rowData)
    {
        $this->parseTag($data);
        $layout = $this->parseLayout($data);

        $rowData[] = array('key' => 'firstname', 'value' => $data['firstname']);
        $rowData[] = array('key' => 'lastname', 'value' => $data['lastname']);
        $rowData[] = array('key' => 'birthday', 'value' => $data['birthday']);
        $rowData[] = array('key' => 'death', 'value' => $data['death']);

        $data['title'] = $data['firstname'] . " " . $data['lastname'];
        $data['subtitle'] = (($data['birthday'] != "") ? $data['birthday'] : "-") . " / " .
            (($data['death'] != "") ? $data['death'] : "-");

        if ($this->updateContent($id, $data, $layout, $rowData)) {
            $stmt = $this->dbApp->prepare(self::$sqlPersonUpdate);

            $birthday = $data["birthday"] !== "" ? $data["birthday"] : null;
            $death = $data["death"] !== "" ? $data["death"] : null;

            $stmt->bindParam(":post_id", $id, \PDO::PARAM_INT);
            $stmt->bindParam(":firstname", $data["firstname"], \PDO::PARAM_STR);
            $stmt->bindParam(":lastname", $data["lastname"], \PDO::PARAM_STR);
            $stmt->bindParam(":birthday", $birthday, \PDO::PARAM_STR);
            $stmt->bindParam(":death", $death, \PDO::PARAM_STR);

            return $stmt->execute();
        } else {
            return false;
        }
    }

    public function createPoiContent($data, $rowData)
    {
        $this->parseTag($data);
        $layout = $this->parseLayout($data);

        $pin = $data['pin_map'] === "" ? null : $data['pin_map'];
        $data['subtitle'] = ($data['region'] == "beacon_region") ? $data['subtitle'] : $data['address'];

        $rowData[] = array('key' => 'subtitle', 'value' => $data['subtitle']);
        $rowData[] = array('key' => 'region', 'value' => $data['region']);
        $rowData[] = array('key' => 'poi_notification', 'value' => $data['poi_notification']);
        switch ($data['region']) {
            case "circular_region":
                $rowData[] = array('key' => 'latitude', 'value' => $data['latitude']);
                $rowData[] = array('key' => 'longitude', 'value' => $data['longitude']);
                $rowData[] = array('key' => 'radius', 'value' => $data['radius']);
                $rowData[] = array('key' => 'pin_map', 'value' => $pin);
                break;
            case "beacon_region":
                $rowData[] = array('key' => 'uuid', 'value' => $data['uuid']);
                $rowData[] = array('key' => 'major', 'value' => $data['major']);
                $rowData[] = array('key' => 'minor', 'value' => $data['minor']);
                $rowData[] = array('key' => 'beacon_range', 'value' => $data['beacon_range']);
                break;
        }

        if ($this->insertContent($data, $layout, $rowData)) {
            $identifier = $data["region"] . "_" . $this->lastContentId;

            $major = $data["major"] === "" ? null : (int)$data["major"];
            $minor = $data["minor"] === "" ? null : (int)$data["minor"];
            $latitude = $data["latitude"] === "" ? null : (double)$data["latitude"];
            $longitude = $data["longitude"] === "" ? null : (double)$data["longitude"];

            try {
                $postPoi = new PostPoi();
                $postPoi
                    ->setPostId($this->lastContentId)
                    ->setIdentifier($identifier)
                    ->setType($data["region"])
                    ->setLat($latitude)
                    ->setLng($longitude)
                    ->setRadius($data["radius"])
                    ->setUuid($data["uuid"])
                    ->setMajor($major)
                    ->setMinor($minor)
                    ->setDistance($data["beacon_range"])
                    ->setPin($pin)
                    ->setPoiNotification((bool)$data['poi_notification']);
                $postPoi->save();

                return true;
            } catch (PropelException $exception) {
                return false;
            }
        } else {
            return false;
        }
    }

    public function updatePoiContent($id, $data, $rowData)
    {
        $this->parseTag($data);
        $layout = $this->parseLayout($data);

        $pin = $data['pin_map'] === "" ? null : $data['pin_map'];
        $data['subtitle'] = ($data['region'] == "beacon_region") ? $data['subtitle'] : $data['address'];

        $rowData[] = array('key' => 'subtitle', 'value' => $data['subtitle']);
        $rowData[] = array('key' => 'region', 'value' => $data['region']);
        $rowData[] = array('key' => 'poi_notification', 'value' => $data['poi_notification']);
        switch ($data['region']) {
            case "circular_region":
                $rowData[] = array('key' => 'latitude', 'value' => $data['latitude']);
                $rowData[] = array('key' => 'longitude', 'value' => $data['longitude']);
                $rowData[] = array('key' => 'radius', 'value' => $data['radius']);
                $rowData[] = array('key' => 'pin_map', 'value' => $pin);
                break;
            case "beacon_region":
                $rowData[] = array('key' => 'uuid', 'value' => $data['uuid']);
                $rowData[] = array('key' => 'major', 'value' => $data['major']);
                $rowData[] = array('key' => 'minor', 'value' => $data['minor']);
                $rowData[] = array('key' => 'beacon_range', 'value' => $data['beacon_range']);
                break;
        }

        if ($this->updateContent($id, $data, $layout, $rowData)) {
            $identifier = $data["region"] . "_" . $id;

            $major = $data["major"] === "" ? null : (int)$data["major"];
            $minor = $data["minor"] === "" ? null : (int)$data["minor"];
            $latitude = $data["latitude"] === "" ? null : (double)$data["latitude"];
            $longitude = $data["longitude"] === "" ? null : (double)$data["longitude"];

            try {
                $postPoi = PostPoiQuery::create()
                    ->filterByPostId($id)
                    ->findOne();

                $postPoi
                    ->setIdentifier($identifier)
                    ->setType($data["region"])
                    ->setLat($latitude)
                    ->setLng($longitude)
                    ->setRadius($data["radius"])
                    ->setUuid($data["uuid"])
                    ->setMajor($major)
                    ->setMinor($minor)
                    ->setDistance($data["beacon_range"])
                    ->setPin($pin)
                    ->setPoiNotification((bool)$data['poi_notification']);
                $postPoi->save();

                return true;
            } catch (PropelException $exception) {
                return false;
            }
        } else {
            return false;
        }
    }

    private function createGenericContent($data, $rowData)
    {
        $this->parseTag($data);
        $layout = $this->parseLayout($data);

        $data['subtitle'] = ($data['subtitle'] != "") ? $data['subtitle'] : $data['description'];

        if ($this->insertContent($data, $layout, $rowData)) {
            return true;
        } else {
            return false;
        }
    }

    private function updateGenericContent($id, $data, $rowData)
    {
        $this->parseTag($data);
        $layout = $this->parseLayout($data);

        $data['subtitle'] = ($data['subtitle'] != "") ? $data['subtitle'] : $data['description'];

        if ($this->updateContent($id, $data, $layout, $rowData)) {
            return true;
        } else {
            return false;
        }
    }

    private function createWebContent($data, $rowData)
    {
        $this->parseTag($data);
        $layout = $this->parseLayout($data);
        $layout['open_in_app'] = (boolval($data['open_in_app']));

        $rowData[] = array('key' => 'custom_css', 'value' => $data['custom_css']);
        $rowData[] = array('key' => 'custom_js', 'value' => $data['custom_js']);

        $data['subtitle'] = ($data['subtitle'] != "") ? $data['subtitle'] : $data['description'];

        if ($this->insertContent($data, $layout, $rowData)) {
            return true;
        } else {
            return false;
        }
    }

    private function updateWebContent($id, $data, $rowData)
    {
        $this->parseTag($data);
        $layout = $this->parseLayout($data);
        $layout['open_in_app'] = (boolval($data['open_in_app']));

        $rowData[] = array('key' => 'custom_css', 'value' => $data['custom_css']);
        $rowData[] = array('key' => 'custom_js', 'value' => $data['custom_js']);

        $data['subtitle'] = ($data['subtitle'] != "") ? $data['subtitle'] : $data['description'];

        if ($this->updateContent($id, $data, $layout, $rowData)) {
            return true;
        } else {
            return false;
        }
    }

    public function getContentLayoutsAllover($template_id, $old_layout, $new_layout)
    {

        $stmt = $this->dbApp->prepare(self::$sqlAppContentsList);

        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_OBJ);
        $hpModel = new HpModel($this->serviceLocator);

        $differentLayoutContents = array();
        foreach ($result as $res) {
            $content_layout = json_decode($res->layout);
            $contentCheck = $hpModel->checkLayoutEquality($content_layout, $old_layout);

            if ($contentCheck) {
                $content_layout->theme = $new_layout;
                self::updateContentTheme($template_id, $res->id, json_encode($content_layout));
            } else {
                $differentLayoutContents[] = $res;
            }
        }

        return $differentLayoutContents;
    }

    public function setContentLayoutsAllover($template_id, $new_layout, $contents_id)
    {
        $res = false;
        foreach ($contents_id as $cont_id) {
            $stmt = $this->dbApp->prepare(self::$sqlGetContentLayout);
            $stmt->bindParam(":id", $cont_id, \PDO::PARAM_INT);

            $stmt->execute();

            $result = $stmt->fetchAll(\PDO::FETCH_OBJ);

            $content_layout = json_decode($result[0]->layout);

            $content_layout->theme = $new_layout;

            $res = self::updateContentTheme($template_id, $cont_id, json_encode($content_layout));
        }

        return $res;
    }

    private function updateContentTheme($template_id, $id, $layout)
    {
        $stmt = $this->dbApp->prepare(self::$sqlSetContentLayout);

        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
        $stmt->bindParam(":temp_id", $template_id, \PDO::PARAM_INT);
        $stmt->bindParam(":layout", $layout, \PDO::PARAM_STR);

        return $stmt->execute();
    }

    public function publishedContentsList()
    {
        $res = array();

        $stmt = $this->dbApp->prepare(self::$sqlGetPublishedContents);

        if ($stmt->execute()) {
            $list = $stmt->fetchAll(\PDO::FETCH_OBJ);
            foreach ($list as $l) {
                $res[$l->id] = $l->title;
            }
        }

        return $res;
    }

    public function importPerson()
    {

        $oldDb = $this->dbHelper->getDatabase("mysql:host=127.0.0.1;dbname=contini_old;charset=utf8", "root", "");
        $stmt = $oldDb->prepare("SELECT *, artist.id AS artist_id FROM post
                                JOIN artist ON post.id = artist.post_id
                                LEFT JOIN attachment ON post.cover_image_id=attachment.id
                                WHERE post.deletedAt IS NULL AND artist.deletedAt IS NULL AND attachment.deletedAt IS NULL;");

        $user_id = $this->aclHelper->getUser()->id;

        $mediaModel = new MediaModel($this->serviceLocator);

        if ($stmt->execute()) {
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            foreach ($rows as $row) {
                $mediaIds = array();

                $downaloadUrl = "http://osboxes.loc/contini_img/" . $row->uri;
                $mediaId = $this->downloadMediaFile("/temp/", $downaloadUrl, $user_id);

                $tags = "#" . implode(", #", explode(",", str_replace(" ", "-", $row->tags)));
                $data = array(
                    'firstname' => $row->firstname,
                    'lastname' => $row->lastname,
                    'birthday' => $row->birthday,
                    'death' => $row->death,
                    'type' => ContentForm::PERSON,
                    'cover_id' => $mediaId,
                    'template_id' => 1,
                    'description' => strip_tags($row->biography),
                    'body' => $row->biography . (($row->publication != null) ? $row->publication : ""),
                    'tags' => ($tags != "#") ? $tags : "",
                );
                $this->createPersonContent($data, array());
                $post_id = $this->lastContentId;


                //$mediaModel = new MediaModel($this->serviceLocator);
                //$mediaModel->setMediaRel($post_id, "".$mediaId);
                $mediaIds [] = $mediaId;

                echo "> " . $user_id . " | " . $row->name . " - " . $downaloadUrl . " ($mediaId) => $post_id \n";

                $stmt2 = $oldDb->prepare("SELECT artwork.title, artwork.description, artwork.`year`, artwork.edition, artwork.`technique`, artwork.`height`, artwork.`width`, artwork.`depth`,
                                attachment.* FROM post
                                JOIN artwork ON post.id = artwork.post_id
                                JOIN attachment ON post.cover_image_id=attachment.id
                                WHERE
                                post.deletedAt IS NULL AND artwork.deletedAt IS NULL AND attachment.deletedAt IS NULL AND
                                artwork.artist_id = " . $row->artist_id);

                if ($stmt2->execute()) {
                    $attachments = $stmt2->fetchAll(\PDO::FETCH_OBJ);
                    foreach ($attachments as $attachment) {
                        $downaloadUrl2 = "http://osboxes.loc/contini_img/" . $attachment->uri;
                        $mediaId = $this->downloadMediaFile("/temp/", $downaloadUrl2, $user_id);
//                        $mediaModel = new MediaModel($this->serviceLocator);
                        //$mediaModel->setMediaRel($post_id, "".$mediaId);

                        $mediaIds [] = $mediaId;

                        $description = implode(", ", array($attachment->year, $attachment->edition, $attachment->technique)) . "\n" .
                            implode(" x ", array($attachment->height, $attachment->width, $attachment->depth)) . "\n" .
                            (($attachment->description != null) ? strip_tags($attachment->description) : "");

                        $mediaModel->setInfo($mediaId, $attachment->title, $description);
                        echo "  - " . $mediaId . " | " . $attachment->title . " - " . $attachment->uri . "\n";
                    }
                }

                $mediaModel->setMediaRel($post_id, implode(",", $mediaIds));
            }
        }

    }

    private function downloadMediaFile($saveDir, $url, $user_id)
    {
        $start_time = time();
        $config = $this->serviceLocator->get('config');
        $cdnBasePath = $config['cdn']['basepath'];

        $filepath = $cdnBasePath . $saveDir . basename($url);
        if (file_put_contents($filepath, fopen($url, 'r')) !== false) {
            $imgHelper = new ImageHelper();

            //$type       = mime_content_type($filepath); //pathinfo($filepath, PATHINFO_EXTENSION); //$_FILES['upload-archive']['type'];
//            $finfo = finfo_open(FILEINFO_MIME_TYPE);
//            $type = finfo_file($finfo, $filepath);
//            finfo_close($finfo);
            $type = "";
            $ext = strtolower(end(explode(".", $filepath)));
            switch ($ext) {
                case "jpeg":
                case "jpg":
                    $type = "image/jpeg";
                    break;
                case "png":
                    $type = "image/png";
                    break;
                case "gif":
                    $type = "image/gif";
                    break;
                default:
                    $type = "-";
            }

            $size = 0;
            $name = pathinfo($filepath, PATHINFO_BASENAME);

            $filename = $imgHelper->getRandomFilename($type);
            $ext = $imgHelper->getExtensionByType($type);

            $tmb_url = $cdnBasePath . ImageHelper::getMediaThumbUrl();
            $low_url = $cdnBasePath . ImageHelper::getMediaLowUrl();
            $mdm_url = $cdnBasePath . ImageHelper::getMediaMediumUrl();
            $hgh_url = $cdnBasePath . ImageHelper::getMediaHighUrl();
            $upl_url = $cdnBasePath . ImageHelper::getMediaUploadedUrl();

            $mediaModel = new MediaModel($this->serviceLocator);
            $mediaId = $mediaModel->add("IMAGE", $name, $filename, $type, $ext, $size);

            $imgHelper->saveImageCropped(ImageHelper::TMB_SIZE, $filepath, $tmb_url, $filename, false);
            $imgHelper->saveImageScaled(ImageHelper::LOW_SIZE, $filepath, $low_url, $filename, false);
            $imgHelper->saveImageScaled(ImageHelper::MDM_SIZE, $filepath, $mdm_url, $filename, false);
            $imgHelper->saveImageScaled(ImageHelper::HTH_SIZE, $filepath, $hgh_url, $filename, false);
            $imgHelper->saveImage($filepath, $upl_url, $filename, false);

            $end_time = time();

            //echo ($end_time - $start_time);

            return $mediaId;
        } else {
            //return $this->downloadMediaFile($saveDir, $url, $user_id);
            return -2;
        }

    }

    /**
     * @param $layout
     * @param $cover_id
     * @param Post $content
     * @throws PropelException
     */
    private function generateCoverImages($layout, $cover_id, $content)
    {
        if ($cover_id !== null && $cover_id !== $content->getCoverId()) {
            $config = $this->serviceLocator->get('config');
            $imageHelper = new ImageHelper();

            $media = MediaQuery::create()
                ->findOneById($cover_id);

            $formats = json_decode($media->getFormat());
            if ($formats === null) {
                $formats = array();
            }

            $formatAvailable = json_decode($media->getFormat());
            $icon_original = $this->getCdnBasePath() . ImageHelper::getMediaUploadedUrl();
            $icon_original_dest = $this->getCdnBasePath() . ImageHelper::getCoverIconFormatUrl(ImageHelper::UPL_FOLDER);

            $imageHelper->saveImage($icon_original . $media->getUri(), $icon_original_dest, $media->getUri(), false);

            $thumbFormat = ImageHelper::TMB_SIZE . "x" . ImageHelper::TMB_SIZE;
            if ($formatAvailable !== null && !in_array($thumbFormat, $formatAvailable)) {
                $icon_thumb = $this->getCdnBasePath() . ImageHelper::getCoverIconFormatUrl($thumbFormat);
                $imageHelper->saveImageCropped($thumbFormat, $icon_original . $media->getUri(), $icon_thumb, $media->getUri(), false);

                $formats[] = $thumbFormat;
            }

            $layoutName = $layout['name'];
            $coverFormat = $config['list_page_types'][$layoutName]['cover_format'];
            if ($formatAvailable !== null && !in_array($coverFormat, $formatAvailable)) {
                $icon_format = $this->getCdnBasePath() . ImageHelper::getCoverIconFormatUrl($coverFormat);
                $imageHelper->saveImageCropped($coverFormat, $icon_original . $media->getUri(), $icon_format, $media->getUri(), false);

                $formats[] = $coverFormat;
            }

            $media->setFormat(json_encode($formats));
            $media->save();
        }
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     * @throws PropelException
     */
    private function menageConnector($id, $data)
    {
        if ($data['connector'] !== LayoutForm::CONNECTOR_TYPE_HUB) {
            $updateConnector = $this->insertContentConnector($id, $data);
            $deleteConnector = false;
        } else {
            $updateConnector = false;
            $deleteConnector = $this->deletePostConnector($id);
        }
        return ($updateConnector || $deleteConnector);
    }

    /**
     * @param PostConnector $postConnector
     * @return array|null
     * @throws PropelException
     */
    private function callRemoteConnector(PostConnector $postConnector)
    {
        $res = null;
        $config = $this->serviceLocator->get('config');
        $post = $postConnector->getPost();

        $providerUUID = $postConnector->getRemoteProvider()->getUuid();
        $remoteUrl = $postConnector->getBaseurl();

        $layout = json_decode($post->getLayout());

        $coverFormat = $config['list_page_types'][$layout->name]['cover_format'];
        $cover = null;
        $coverId = $post->getCoverId();
        if (isset($coverId) && $coverId > 0) {
            $media = MediaQuery::create()
                ->findPk($coverId);
            $cover = $this->getCdnBaseurl() . ImageHelper::getCoverIconFormatUrl($coverFormat) . "/" . $media->getUri();
        }

        $medias = array();
        $mediasObj = GalleryQuery::create()
            ->filterByPostId($post->getId())
            ->orderByWeight()
            ->find();

        foreach ($mediasObj as $mediaObj) {
            $media = $mediaObj->getMedia();
            if ($media->getType() === MediaModel::MEDIA_TYPE_IMAGE) {
                $thumb = $this->getCdnBaseurl() . ImageHelper::getCoverIconThumbUrl() . "/" . $media->getUri();
                $resource = $this->getCdnBaseurl() . ImageHelper::getCoverIconFormatUrl($coverFormat) . "/" . $media->getUri();
            } else {
                $thumb = $media->getUriThumb();
                $resource = $media->getUri();
            }

            $medias[] = array(
                "type" => $media->getType(),
                "thumb" => $thumb,
                "resource" => $resource,
                "title" => $media->getTitle(),
                "subtitle" => $media->getDescription(),
            );
        }

        $contentData = array(
            "title" => json_decode($post->getTitle()),
            "subtitle" => $post->getSubtitle(),
            "description" => json_decode($post->getDescription()),
            "layout" => null,
            "cover" => $cover,
            "body" => json_decode($post->getBody()),
            "gallery" => $medias,
            "data" => json_decode($post->getRowData()),
        );

        $contentDataJson = json_encode($contentData);

        $headers = array(
            "Content-HMAC: " . base64_encode(hash_hmac("sha256", $contentDataJson, $providerUUID)),
            "cache-control: no-cache",
            "Hp-Response: content",
            "Hp-Hash-Id: " . md5($post->getId()),
            "Hp-Content-Type: " . $post->getType(),
            "Content-Type: application/json",
            "Content-Length: " . strlen($contentDataJson),
        );

        try {
            $success = true;
            $data = json_decode($this->connectorRequest($contentDataJson, $remoteUrl, $providerUUID, $headers));
            $message = "";
        } catch (RestRemoteException $exception) {
            $success = false;
            $data = null;
            $message = $exception->getMessage();
        } catch (RestSignatureSecurityException $exception) {
            $success = false;
            $data = null;
            $message = $exception->getMessage();
        }

        $res = array(
            "success" => $success,
            "data" => $data,
            "message" => $message,
        );

        return $res;
    }

    /**
     * @param $fields
     * @param $url
     * @param null $secret_key
     * @param array $headers
     * @return bool|null|string
     * @throws RestRemoteException
     * @throws RestSignatureSecurityException
     */
    private function connectorRequest($fields, $url, $secret_key = null, $headers = array())
    {
        $data = null;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HEADER => 1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => $fields,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $info = curl_getinfo($curl);

        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $responseHeaders = substr($response, 0, $header_size);
        $content = substr($response, $header_size);

        curl_close($curl);

//        var_dump($response);
//        var_dump($err);
//        var_dump($info);
//        var_dump($content);die();

        if ($err || $info['http_code'] >= 400) {
            //echo "cURL Error #:" . $err;
            $remoteException = new RestRemoteException();
            $remoteException->setRemoteStatusCode($info['http_code']);
            throw $remoteException;
        } else {
            $sign = null;
            $responseHeaders = explode("\n", $responseHeaders);
            foreach ($responseHeaders as $header) {
                if (stripos($header, 'Content-HMAC: ') !== false) {
                    $sign = base64_decode(trim(explode(":", $header)[1]));
                }
            }
            if (!$this->isValidSignature($content, $secret_key, $sign)) {
                throw new RestSignatureSecurityException();
            } else {
                $data = $content;
            }
        }

        return $data;
    }

    private function isValidSignature($public_key, $secret_key, $sign)
    {
        return (hash_hmac("sha256", $public_key, $secret_key) == $sign);
    }

    /**
     * @param $id
     * @param $data
     * @param $rowData
     * @param NotificationModel $notificationModel
     * @param Post $content
     * @return bool
     * @throws PropelException
     */
    private function createPushNotification($id, $data, $rowData, NotificationModel $notificationModel, Post $content)
    {
        $notificationData = $this->getNotificationData($id, $data);
        $userLanguages = $this->getUserLanguages();

        $notificationIds = array();
        foreach ($userLanguages as $userLanguage) {
            $notificationRes = $notificationModel->appendNotification($notificationData, $userLanguage);
            if (property_exists($notificationRes, 'success')) {
                if ($notificationRes->success) {
                    $notificationIds[] = $notificationRes->notification_id;
                }
            }
        }

        if (count($notificationIds) > 0) {
            $rowData[] = array('key' => 'content_push_notification_id', 'value' => $notificationIds);
            $content
                ->setRowData(json_encode($rowData))
                ->save();

            $res = true;
        } else {
            $res = false;
        }

        return $res;
    }

    /**
     * @param PushNotification $pushNotification
     * @param $data
     * @param NotificationModel $notificationModel
     * @return bool|string
     * @throws PropelException
     * @throws \MessagesService\Exception\MessagesServicePushException
     */
    private function updatePushNotification(PushNotification $pushNotification, $data, NotificationModel $notificationModel)
    {
        $notificationData = $this->getNotificationData($pushNotification->getPostId(), $data);

        return $notificationModel->updateNotification($pushNotification->getId(), $notificationData);
    }

    /**
     * @param PushNotification $pushNotification
     * @param NotificationModel $notificationModel
     * @return bool|string
     * @throws MessagesServicePushException
     * @throws PropelException
     */
    private function removePushNotification(PushNotification $pushNotification, NotificationModel $notificationModel)
    {
        return $notificationModel->removeNotification($pushNotification->getId());
    }

    /**
     * @param $contentId
     * @param $data
     * @return array
     */
    private function getNotificationData($contentId, $data)
    {
        list($year, $month, $day) = explode("-", $data['content_push_notification_send_date']);
        $notificationData = array(
            "to" => "",
            "send_date" => "$day-$month-$year",
            "send_time" => $data['content_push_notification_send_time'],
            "action" => NotificationForm::OPEN_CONTENT,
            "section" => null,
            "content" => $contentId,
            "message" => $data['description'],
            "title" => $data['title'],
        );
        return $notificationData;
    }
}