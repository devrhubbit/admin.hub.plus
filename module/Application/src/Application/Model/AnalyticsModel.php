<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 30/07/16
 * Time: 16:01
 */

namespace Application\Model;

use Zend\I18n\Validator\DateTime;

class AnalyticsModel extends HpModel {

    private static $sqlDeviceCount = "SELECT COUNT(id) as total   FROM device WHERE deleted_at IS NULL;";
    private static $sqlDeviceMonth = "SELECT COUNT(id) as monthly FROM device WHERE deleted_at IS NULL AND created_at > :date;";

    private static $sqlPostViewCount = "SELECT count(k) as total FROM
                                          (SELECT CONCAT('post_', id) as k, `type` FROM post_extra_log UNION
                                          (SELECT CONCAT('remote_', id) as k, `type` FROM remote_extra_log WHERE `content_type`='POST')) as t
                                        WHERE `type` = 'VIEW';";
    private static $sqlPostViewMonth = "SELECT count(k) as monthly FROM
                                          (SELECT CONCAT('post_', id) as k, `type`, timestamp FROM post_extra_log UNION
                                          (SELECT CONCAT('remote_', id) as k, `type`, timestamp FROM remote_extra_log WHERE `content_type`='POST')) as t
                                        WHERE `type` = 'VIEW' AND timestamp > :date;";

    private static $sqlPostLikeCount = "SELECT count(k) as total FROM
                                          (SELECT CONCAT('post_', id) as k, `type` FROM post_extra_log UNION
                                          (SELECT CONCAT('remote_', id) as k, `type` FROM remote_extra_log WHERE `content_type`='POST')) as t
                                        WHERE type='LIKE';";
    private static $sqlPostLikeMonth = "SELECT count(k) as monthly FROM
                                          (SELECT CONCAT('post_', id) as k, `type`, timestamp FROM post_extra_log UNION
                                          (SELECT CONCAT('remote_', id) as k, `type`, timestamp FROM remote_extra_log WHERE `content_type`='POST')) as t
                                        WHERE type='LIKE' AND timestamp > :date;";

    private static $sqlPostUnlikeCount = "SELECT count(k) as total FROM
                                            (SELECT CONCAT('post_', id) as k, `type` FROM post_extra_log UNION
                                            (SELECT CONCAT('remote_', id) as k, `type` FROM remote_extra_log WHERE `content_type`='POST')) as t
                                          WHERE  type='UNLIKE';";
    private static $sqlPostUnlikeMonth = "SELECT count(k) as monthly FROM
                                            (SELECT CONCAT('post_', id) as k, `type`, timestamp FROM post_extra_log UNION
                                            (SELECT CONCAT('remote_', id) as k, `type`, timestamp FROM remote_extra_log WHERE `content_type`='POST')) as t
                                          WHERE type='UNLIKE' AND timestamp > :date;";


    private static $sqlPostShareCount = "SELECT count(k) as total FROM
                                          (SELECT CONCAT('post_', id) as k, `type` FROM post_extra_log UNION
                                          (SELECT CONCAT('remote_', id) as k, `type` FROM remote_extra_log WHERE `content_type`='POST')) as t
                                         WHERE type='SHARE';";
    private static $sqlPostShareMonth = "SELECT count(k) as monthly FROM
                                          (SELECT CONCAT('post_', id) as k, `type`, timestamp FROM post_extra_log UNION
                                          (SELECT CONCAT('remote_', id) as k, `type`, timestamp FROM remote_extra_log WHERE `content_type`='POST')) as t
                                         WHERE type='SHARE' AND timestamp > :date;";

    private static $sqlDeviceData     = "SELECT os, type, Count(id) as count FROM device
                                          WHERE deleted_at IS NULL AND updated_at BETWEEN :from AND :to
                                          %s %s
                                          GROUP BY os, type;";

    private static $sqlPostsData = "SELECT YEAR(timestamp) as y, MONTH(timestamp) as m, DAY(timestamp) as d, type, Count(type) as count
                                      FROM post_extra_log
                                      WHERE timestamp BETWEEN :from AND :to
                                      %s
                                      GROUP BY y, m, d, type ORDER BY y, m, d;";


    public function getDeviceData($os, $type, $from, $to) {
        $data = array();

        $search1 = array();
        $osSearch = "";
        if ($os) {
            foreach ($os as $key => $value) {
                if ($value) {
                    $search1[] = "'$key'";
                }
            }
            $search1 = implode(", ", $search1);
            $osSearch = "AND os IN (".$search1.")";
        }

        $search2 = array();
        $typeSearch = "";
        if ($type) {
            foreach ($type as $key => $value) {
                if ($value) {
                    $search2[] = "'$key'";
                }
            }
            $search2 = implode(", ", $search2);
            $typeSearch = "AND type IN (".$search2.")";
        }

        $sql = sprintf(self::$sqlDeviceData, $osSearch, $typeSearch);

        $stmt = $this->dbApp->prepare($sql);
        $stmt->bindParam(":from", $from, \PDO::PARAM_STR);
        $stmt->bindParam(":to",   $to,   \PDO::PARAM_STR);

        if ($stmt->execute()) {
            $data = $stmt->fetchAll(\PDO::FETCH_OBJ);

            if ($data != null) {
                $data = self::applePieChartIt($data);
            }
        }

        return $data;
    }

    private function applePieChartIt($data) {
        $result = new \StdClass();
        $cols = array();
        $cols []= array("id" => "devices", "label" => "Devices", "pattern" => "", "type" => "string");
        $cols []= array("id" => "slices", "label" => "Slices",  "pattern" => "", "type" => "number");

        $rows = array();
        foreach ($data as $d) {
            $row = new \StdClass();
            $title = ucfirst($d->os)." ".ucfirst($d->type);
            $count = intval($d->count);

            $chartTitle = array("v" => $title, "f" => null);
            $chartCount = array("v" => $count, "f" => null);

            $row->c = array($chartTitle, $chartCount);
            $rows []= $row;
        }

        $result->cols = $cols;
        $result->rows = $rows;

        return $result;
    }

    public function getPostsData($types, $from, $to) {
        $data = array();

        $search = array();
        $typeSearch = "";
        if ($types) {
            foreach ($types as $key => $value) {
                if ($value) {
                    $search[] = "'$key'";
                }
            }
            $search = implode(", ", $search);
            $typeSearch = "AND type IN (".$search.")";
        }

        $sql = sprintf(self::$sqlPostsData, $typeSearch);

        $stmt = $this->dbApp->prepare($sql);
        $stmt->bindParam(":from", $from, \PDO::PARAM_STR);
        $stmt->bindParam(":to",   $to,   \PDO::PARAM_STR);

        if ($stmt->execute()) {
            $data = $stmt->fetchAll(\PDO::FETCH_OBJ);

            if ($data != null) {
                $data = $this->lineChartIt($types, $data);
            }
        }

        return $data;
    }

    private function lineChartIt($types, $data) {
        $result = new \StdClass();

        $cols = array();
        $cols []= array("id" => "day", "label" => "Day", "type" => "string");
        if($types['VIEW']   == 1) $cols []= array("id" => "view", "label" => "View", "type" => "number");
        if($types['SHARE']  == 1) $cols []= array("id" => "share", "label" => "Share", "type" => "number");
        if($types['LIKE']   == 1) $cols []= array("id" => "like", "label" => "Like", "type" => "number");
        if($types['UNLIKE'] == 1) $cols []= array("id" => "unlike", "label" => "Unlike", "type" => "number");

        $rows = array();
        $lastDate   = null;
        $lastLike   = 0;
        $lastUnlike = 0;
        $lastView   = 0;
        $lastShare  = 0;
        foreach($data as $d) {
            $timestamp = $d->y."-".$d->m."-".$d->d;

            $day = new \DateTime($timestamp);
            $timestamp = $day->format('M d');

            if($lastDate != $timestamp) {
                if($lastDate != null) {
                    $rows []= $this->lineChartCycle($lastDate, $types, $lastLike, $lastUnlike, $lastView, $lastShare);
                }
                $lastDate = $timestamp;
                $lastLike   = 0;
                $lastUnlike = 0;
                $lastView   = 0;
                $lastShare  = 0;
            }

            switch($d->type) {
                case "VIEW":    $lastView   = intval($d->count); break;
                case "SHARE":   $lastShare  = intval($d->count); break;
                case "LIKE":    $lastLike   = intval($d->count); break;
                case "UNLIKE":  $lastUnlike = intval($d->count); break;
            }
        }

        $rows []= $this->lineChartCycle($lastDate, $types, $lastLike, $lastUnlike, $lastView, $lastShare);

        $result->cols = $cols;
        $result->rows = $rows;

        return $result;
    }

    private function lineChartCycle($lastDate, $types, $lastLike, $lastUnlike, $lastView, $lastShare) {
        $row = new \StdClass();
        $row->c = array();

        $obj = new \StdClass();
        $obj->v = $lastDate;
        $row->c[]=$obj;

        if($types['VIEW'] == 1) {
            $obj = new \StdClass();
            $obj->v = $lastView;
            $row->c[]=$obj;
        }
        if($types['SHARE'] == 1) {
            $obj = new \StdClass();
            $obj->v = $lastShare;
            $row->c[]=$obj;
        }
        if($types['LIKE'] == 1) {
            $obj = new \StdClass();
            $obj->v = $lastLike;
            $row->c[]=$obj;
        }
        if($types['UNLIKE'] == 1) {
            $obj = new \StdClass();
            $obj->v = $lastUnlike;
            $row->c[]=$obj;
        }

        return $row;
    }

    /**
     * @return \stdClass with two attrs: total registrerd device and last month registered device
     */
    public function getSimpleDeviceData() {
        $data = new \stdClass();

        $stmt1 = $this->dbApp->prepare(self::$sqlDeviceCount);
        if($stmt1->execute()) {
            $data->total = $stmt1->fetchObject()->total;
        } else {
            $data->total = 0;
        }

        $lastMonth = date('Y-m-d G:i:s', strtotime('-1 month'));

        $stmt2 = $this->dbApp->prepare(self::$sqlDeviceMonth);
        $stmt2->bindParam(":date", $lastMonth, \PDO::PARAM_STR);

        if($stmt2->execute()) {
            $data->monthly = $stmt2->fetchObject()->monthly;
        } else {
            $data->monthly = 0;
        }

        if($data->total > 0) $data->ratio = round(($data->monthly / $data->total) * 100, 2, PHP_ROUND_HALF_UP);
        else $data->ratio = 0;

        return $data;
    }

    /**
     * @return \stdClass with two attrs: total registrerd device and last month registered device
     */
    public function getSimplePostViewData() {
        $data = new \stdClass();

        $stmt1 = $this->dbApp->prepare(self::$sqlPostViewCount);
        if($stmt1->execute()) {
            $data->total = $stmt1->fetchObject()->total;
        } else {
            $data->total = 0;
        }

        $lastMonth = date('Y-m-d G:i:s', strtotime('-1 month'));

        $stmt2 = $this->dbApp->prepare(self::$sqlPostViewMonth);
        $stmt2->bindParam(":date", $lastMonth, \PDO::PARAM_STR);

        if($stmt2->execute()) {
            $data->monthly = $stmt2->fetchObject()->monthly;
        } else {
            $data->monthly = 0;
        }

        if($data->total > 0) $data->ratio = round(($data->monthly / $data->total) * 100, 2, PHP_ROUND_HALF_UP);
        else $data->ratio = 0;

        return $data;
    }

    /**
     * @return \stdClass with two attrs: total registrerd device and last month registered device
     */
    public function getSimplePostShareData() {
        $data = new \stdClass();

        $stmt1 = $this->dbApp->prepare(self::$sqlPostShareCount);
        if($stmt1->execute()) {
            $data->total = $stmt1->fetchObject()->total;
        } else {
            $data->total = 0;
        }

        $lastMonth = date('Y-m-d G:i:s', strtotime('-1 month'));

        $stmt2 = $this->dbApp->prepare(self::$sqlPostShareMonth);
        $stmt2->bindParam(":date", $lastMonth, \PDO::PARAM_STR);

        if($stmt2->execute()) {
            $data->monthly = $stmt2->fetchObject()->monthly;
        } else {
            $data->monthly = 0;
        }

        if($data->total > 0) $data->ratio = round(($data->monthly / $data->total) * 100, 2, PHP_ROUND_HALF_UP);
        else $data->ratio = 0;

        return $data;
    }

    /**
     * @return \stdClass with two attrs: total registrerd device and last month registered device
     */
    public function getSimplePostLikeData() {
        $data1 = new \stdClass();

        $stmt1 = $this->dbApp->prepare(self::$sqlPostLikeCount);
        if($stmt1->execute()) {
            $data1->total = $stmt1->fetchObject()->total;
        } else {
            $data1->total = 0;
        }

        $lastMonth = date('Y-m-d G:i:s', strtotime('-1 month'));

        $stmt2 = $this->dbApp->prepare(self::$sqlPostLikeMonth);
        $stmt2->bindParam(":date", $lastMonth, \PDO::PARAM_STR);

        if($stmt2->execute()) {
            $data1->monthly = $stmt2->fetchObject()->monthly;
        } else {
            $data1->monthly = 0;
        }

        $data2 = new \stdClass();

        $stmt3 = $this->dbApp->prepare(self::$sqlPostUnlikeCount);
        if($stmt3->execute()) {
            $data2->total = $stmt3->fetchObject()->total;
        } else {
            $data2->total = 0;
        }

        $lastMonth = date('Y-m-d G:i:s', strtotime('-1 month'));

        $stmt4 = $this->dbApp->prepare(self::$sqlPostUnlikeMonth);
        $stmt4->bindParam(":date", $lastMonth, \PDO::PARAM_STR);

        if($stmt4->execute()) {
            $data2->monthly = $stmt4->fetchObject()->monthly;
        } else {
            $data2->monthly = 0;
        }

        $data = new \stdClass();

        $data->total   = $data1->total - $data2->total;
        $data->monthly = $data1->monthly - $data2->monthly;

        if($data->total > 0) $data->ratio = round(($data->monthly / $data->total) * 100, 2, PHP_ROUND_HALF_UP);
        else $data->ratio = 0;

        return $data;
    }
}