<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 05/06/16
 * Time: 18:58
 */

namespace Application\Model;

use Common\Helper\AclHelper;
use Common\Helper\ImageHelper;
use Database\HubPlus\MediaQuery;
use Database\HubPlus\SectionConnector;
use Database\HubPlus\SectionConnectorQuery;
use Database\HubPlus\SectionRelatedTo;
use Database\HubPlus\SectionRelatedToQuery;
use Form\Section\CustomForm\CustomFieldForm;
use Form\Section\CustomForm\CustomFormSectionForm;
use Form\Section\Poi\PoiSectionForm;
use Form\Section\SectionForm;
use Form\LayoutForm;
use Common\Helper\SecurityHelper;
use Application\Model\SectionModel as ApplicationSectionModel;
use Database\HubPlus\Section,
    Database\HubPlus\SectionQuery;

use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;

class SectionModel extends HpModel
{
    const SECTION_STATUS_PUBLISHED = 0;
    const SECTION_STATUS_DRAFT = 1;

    const RESOURCE_PARAMS_SEPARATOR = "|";

    const RESOURCE_EVENT_LIST = "/event/list/";
    const RESOURCE_PERSON_LIST = "/person/list/";
    const RESOURCE_EXTERNAL_LIST = "/external/list/%d/";
    const RESOURCE_FAVOURITES_LIST = "/favourite/list/";
    const RESOURCE_POI_LIST = "/poi/list/";
    const RESOURCE_GENERIC_LIST = "/generic/list/";
    const RESOURCE_WEB_LIST = "/web/list/";
    const RESOURCE_REGISTER_USER = "/user/register/%d";
    const RESOURCE_FORM_USER = "/user/form/%d";
    const RESOURCE_SECTION_LIST = "/section/list/";
    const RESOURCE_USER_LIST = "/user/list/";

    private $lastSectionId = 0;

    private static $sqlAppSectionsList = "SELECT * FROM `section` WHERE deleted_at IS NULL ORDER BY title";
    private static $sqlGetPublishedSections = "SELECT `id`, `title` FROM `section` WHERE deleted_at IS NULL AND status = 0 ORDER BY title";
    private static $sqlGetSectionLayout = "SELECT `layout` FROM `section` WHERE id = :id";
    private static $sqlSetSectionLayout = "UPDATE `section` SET `layout` = :layout, `template_id` = :temp_id, updated_at = NOW() WHERE `id` = :id";

    private static $sqlSectionsList = "SELECT * FROM `section` WHERE deleted_at IS NULL AND `area` = :area ORDER BY weight";
    private static $sqlSectionsLast = "SELECT * FROM `section` WHERE deleted_at IS NULL ORDER BY updated_at DESC LIMIT :start , :length";
    private static $sqlSectionSimpleList = "SELECT id, area, type, title FROM `section` WHERE `deleted_at` IS NULL AND `status` = 0 ORDER BY area, type, title;";


    private static $sqlSectionRes = "UPDATE `section` SET `resource` = :r, updated_at = NOW() WHERE `id` = :id";
    private static $sqlSectionWeight = "UPDATE `section` SET `weight` = :w, updated_at = NOW() WHERE `id` = :id";

    private static $sqlCheckIfRegistration = "SELECT `type`, `params` FROM `section` WHERE deleted_at IS NULL AND `id` = :id";
    private static $sqlRegistrationUniqueness = "SELECT `id`, `params` FROM `section` WHERE deleted_at IS NULL AND `id` != :id AND `status` = 0 AND `type` = :type";

    private static $sqlGetCustomFormList = "SELECT `id`, `title`, `params`, `status` FROM `section` WHERE deleted_at IS NULL AND `type` = :type ORDER BY `title`";

    private static $sqlSectionGroupCreate = "INSERT INTO `group` (`id`, `label`, `created_at`, `updated_at`) VALUES (NULL, :label, NOW(), NOW())";
    private static $sqlGetSectionGroupList = "SELECT * FROM `group` WHERE deleted_at IS NULL ORDER BY label ASC";
    private static $sqlGetActualSectionGroupList = "SELECT * FROM `group` WHERE deleted_at IS NULL AND label NOT IN(%s) ORDER BY label ASC";

    private static $sqlSectionDetail = "SELECT * FROM `section` WHERE deleted_at IS NULL AND id = :id";
    private static $sqlGetSectionConnector = "SELECT * FROM `section_connector` WHERE deleted_at IS NULL AND `section_id` = :sectionId";

    private static $sqlFormUsersData = "SELECT SQL_CALC_FOUND_ROWS cf.*, cf.updated_at AS `last modified`, ua.username, ua.created_at AS `registered at`, cf.id AS form_user_id FROM custom_form_%s AS cf
                                          LEFT JOIN `user_app` AS ua ON user_id = ua.id
                                          WHERE `deleted_at` IS NULL AND row_data LIKE :filter
                                          %s
                                          LIMIT :start_row , :length";

    private static $sqlFormUserDataById = "SELECT cf.* FROM custom_form_%s AS cf WHERE cf.id = :row_id";

    private static $sqlFormUsersAddress = "SELECT * FROM `address` WHERE `id` = :id";

    private static $sqlCheckFieldContent = "SELECT `%s` FROM custom_form_%s";

    const HPFORM_INPUT_DESCR = "DESCRIPTION";
    const HPFORM_INPUT_TEXT = "INPUT_TEXT";
    const HPFORM_TEXTAREA = "TEXTAREA";
    const HPFORM_IMAGE = "IMAGE";
    const HPFORM_CHECKBOX = "CHECKBOX";
    const HPFORM_RADIOBOX = "RADIOBOX";
    const HPFORM_SELECT = "SELECT";
    const HPFORM_ADDRESS = "ADDRESS";
    const HPFORM_TAG_GROUP = "TAG_GROUP";
    const CUSTOM_FORM_TYPE_SUFFIX = "_FIELD";

    private static $sqlCreateForm = "CREATE TABLE `custom_form_%s` (
                                        `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                                        `device_id` INT(11) UNSIGNED DEFAULT NULL,
                                        `user_id` INT(11) UNSIGNED DEFAULT NULL,
                                        `cover_id` INT(11) DEFAULT NULL,
                                        `row_data` LONGTEXT,
                                        `created_at` DATETIME DEFAULT NULL,
                                        `updated_at` DATETIME DEFAULT NULL,
                                        %s
                                        PRIMARY KEY (`id`),
                                        CONSTRAINT `custom_form_ibfk_%s` FOREIGN KEY (`user_id`) REFERENCES `user_app` (`id`),
                                        CONSTRAINT `custom_form_dvfk_%s` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
                                     ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

    private static $sqlUpdateForm = "ALTER TABLE `custom_form_%s` %s ;";

    private static $selectedType = null;

    public static function setSelectedType($type)
    {
        self::$selectedType = strtoupper($type);
    }

    public static function getSelectedType()
    {
        return self::$selectedType;
    }

    public function isValidType($type)
    {
        $config = $this->serviceLocator->get('config');
        return array_key_exists(strtoupper($type), $config['section_types']);
    }

    public function getByType($type)
    {
        $config = $this->serviceLocator->get('config');
        $params = $config['section_types'][strtoupper($type)];
        $params['section_type'] = strtoupper($type);
        return $params;
    }

    public function getSectionTypes($selected_type = null)
    {
        $config = $this->serviceLocator->get('config');
        $setting = $this->aclHelper->getCategorySetting();

        foreach ($setting->section_types as $type) {
            $config['section_types'][$type]["enabled"] = true;
        }

        if ($selected_type != null) {
            $config['section_types'][strtoupper($selected_type)]["active"] = true;
        }

        return $config['section_types'];
    }

    public function getSections($area)
    {
        $result = null;

        if (!is_null($this->dbApp)) {
            $stmt = $this->dbApp->prepare(self::$sqlSectionsList);

            $stmt->bindParam(":area", $area, \PDO::PARAM_STR);

            $stmt->execute();

            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            $mediaModel = new MediaModel($this->serviceLocator);

            $application = $this->aclHelper->getApp();

            foreach ($result as $key => $res) {
                $label = $this->getTypeLabel($res['type'], 'section_types');
                $result[$key]['label'] = $label;

                $iconId = $res['icon_id'];
                if (is_integer($iconId)) {
                    $icon = $mediaModel->getMediaById($iconId);
                    $result[$key]['section_icon_name'] = $icon->uri;
                }

                // Multi language fields
                $result[$key]['title'] = $this->getLocaleText($application->main_contents_language, $res['title']);
                $result[$key]['description'] = $this->getLocaleText($application->main_contents_language, $res['description']);
            }
        }

        return $result;
    }

    public function getSectionSimpleList()
    {
        $data = array();
        $config = $this->serviceLocator->get('config');
        if (!is_null($this->dbApp)) {
            $stmt = $this->dbApp->prepare(self::$sqlSectionSimpleList);

            if ($stmt->execute()) {
                $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
                $application = $this->aclHelper->getApp();
                foreach ($rows as $row) {
                    $type = $config['section_types'][$row->type]["label"];
                    $data [$row->id] = $type . " > " . $this->getLocaleText($application->main_contents_language, $row->title);
                }
            }
        }

        return $data;
    }

    public function getLastSections($start, $length)
    {
        $rows = null;

        if (!is_null($this->dbApp)) {
            $stmt = $this->dbApp->prepare(self::$sqlSectionsLast);

            $stmt->bindParam(":start", $start, \PDO::PARAM_INT);
            $stmt->bindParam(":length", $length, \PDO::PARAM_INT);

            $stmt->execute();

            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);
            $application = $this->aclHelper->getApp();
            foreach ($rows as $row) {
                // Multi language fields
                $row->title = $this->getLocaleText($application->main_contents_language, $row->title);
                $row->description = $this->getLocaleText($application->main_contents_language, $row->description);

                $row->url = "/admin/section/" . strtolower($row->type) . "/" . $row->id . "/";
            }
        }

        return $rows;
    }

    public function setSectionWeight($id, $w)
    {
        $res = false;

        if (!is_null($this->dbApp)) {
            $stmt = $this->dbApp->prepare(self::$sqlSectionWeight);

            $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
            $stmt->bindParam(":w", $w, \PDO::PARAM_INT);

            $res = $stmt->execute();
        }

        return $res;
    }

    /**
     * @param $id
     * @param $override
     * @param $status
     * @param $oldRegistrationId
     * @param NotificationModel $notificationModel
     * @return array
     * @throws \MessagesService\Exception\MessagesServicePushException
     */
    public function chooseActionStatus($id, $override, $status, $oldRegistrationId, NotificationModel $notificationModel)
    {
        $res = array('result' => false, 'unique' => null);

        if (boolval($status)) {
            $res['result'] = $this->setSectionStatus($id, (int)$status, $notificationModel);
        } else {
            $isCustomFormRegistration = $this->checkIfRegistrationForm($id);
            if ($isCustomFormRegistration) {
                if ($override) {
                    $activateNewRegistration = $this->setSectionStatus($id, 0, $notificationModel);
                    $deactivateOldRegistration = $this->setSectionStatus($oldRegistrationId, 1, $notificationModel);
                    $res['result'] = ($activateNewRegistration && $deactivateOldRegistration);
                } else {
                    $res['unique'] = $this->checkRegistrationUniqueness($id);
                    if ($res['unique'] == null) {
                        $res['result'] = $this->setSectionStatus($id, (int)$status, $notificationModel);
                    }
                }
            } else {
                $res['result'] = $this->setSectionStatus($id, (int)$status, $notificationModel);
            }
        }

        return $res;
    }

    public function getRegistrationForm()
    {
        $sections = SectionQuery::create()
            ->filterByStatus(0)
            ->filterByType(CustomFormSectionForm::CUSTOM_FORM)
            ->find();

        foreach ($sections as $section) {
            $params = json_decode($section->getParams());

            if ($params !== null && $params->form_type === CustomFormSectionForm::REGISTRATION_FORM) {
                return $section;
            }
        }

        return null;
    }

    private function checkIfRegistrationForm($id)
    {
        $res = false;
        $stmt = $this->dbApp->prepare(self::$sqlCheckIfRegistration);

        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $row = $stmt->fetchObject();
            if ($row->type == SectionForm::CUSTOM_FORM) {
                $params = json_decode($row->params);
                if ($params->form_type == CustomFormSectionForm::REGISTRATION_FORM) {
                    $res = true;
                }
            }
        }

        return $res;
    }

    private function checkRegistrationUniqueness($id)
    {
        $res = null;
        $stmt = $this->dbApp->prepare(self::$sqlRegistrationUniqueness);
        $type = SectionForm::CUSTOM_FORM;

        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
        $stmt->bindParam(":type", $type, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

            foreach ($rows as $row) {
                $params = json_decode($row->params);
                if ($params->form_type == CustomFormSectionForm::REGISTRATION_FORM) {
                    $res = $row->id;
                }
            }
        }

        return $res;
    }

    /**
     * @param $id
     * @param $s
     * @param NotificationModel $notificationModel
     * @return bool|string
     * @throws \MessagesService\Exception\MessagesServicePushException
     */
    private function setSectionStatus($id, $s, NotificationModel $notificationModel)
    {
        $res = true;

        $section = SectionQuery::create()
            ->findPk($id);
        try {
            $section->setStatus($s)->save();

            if ($s === self::SECTION_STATUS_DRAFT) {
                // Remove push notification not sent
                $pushNotifications = $notificationModel->getPushNotifications("section", $id);

                if (isset($pushNotifications) && $pushNotifications->count() > 0) {
                    foreach ($pushNotifications as $pushNotification) {
                        $res = $notificationModel->removeNotification($pushNotification->getId());
                    }
                }
            }
        } catch (PropelException $exception) {
            $res = false;
        }

        return $res;
    }

    public function deleteSectionSwitch($id)
    {
        $deleteSection = $this->deleteSection($id);
        $deleteConnector = $this->deleteSectionConnector($id);
        return ($deleteSection && $deleteConnector);
    }

    public function deleteSection($id)
    {
        $res = false;

        $section = SectionQuery::create()
            ->findOneById($id);
        if ($section) {
            $section->delete();
            $actModel = new ActionModel($this->serviceLocator);
            $res = $actModel->deleteActionAfterSectionDelete($id);
        }

        return $res;
    }

    public function deleteSectionConnector($id)
    {
        $sectionsConnector = SectionConnectorQuery::create()
            ->filterBySectionId($id)
            ->find();

        foreach ($sectionsConnector as $sectionConnector) {
            $sectionConnector->delete();
        }
        return true;
    }

    public function createSection($data)
    {
        switch ($data["type"]) {
            case SectionForm::RSS :
                return array("result" => $this->createRssSection($data), "area" => $data['area']);
                break;
            case SectionForm::PODCAST :
                return array("result" => $this->createPodcastSection($data), "area" => $data['area']);
                break;
            case SectionForm::TWITTER :
                return array("result" => $this->createTwitterSection($data), "area" => $data['area']);
                break;
            case SectionForm::REGISTER :
                return array("result" => $this->createRegisterSection($data), "area" => $data['area']);
                break;
            case SectionForm::INFO :
                return array("result" => $this->createInfoSection($data), "area" => $data['area']);
                break;
            case SectionForm::FAVOURITE :
                $insertSection = $this->createFavouritesSection($data);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertSectionConnector($this->lastSectionId, $data) : $insertConnector = true;
                return array("result" => ($insertSection && $insertConnector), "area" => $data['area']);
                break;
            case SectionForm::PERSON :
                $insertSection = $this->createPersonSection($data);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertSectionConnector($this->lastSectionId, $data) : $insertConnector = true;
                return array("result" => ($insertSection && $insertConnector), "area" => $data['area']);
                break;
            case SectionForm::EVENT :
                $insertSection = $this->createEventSection($data);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertSectionConnector($this->lastSectionId, $data) : $insertConnector = true;
                return array("result" => ($insertSection && $insertConnector), "area" => $data['area']);
                break;
            case SectionForm::POI :
                $insertSection = $this->createPoiSection($data);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertSectionConnector($this->lastSectionId, $data) : $insertConnector = true;
                return array("result" => ($insertSection && $insertConnector), "area" => $data['area']);
                break;
            case SectionForm::GENERIC :
                $insertSection = $this->createGenericSection($data);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertSectionConnector($this->lastSectionId, $data) : $insertConnector = true;
                return array("result" => ($insertSection && $insertConnector), "area" => $data['area']);
                break;
            case SectionForm::CUSTOM_FORM :
                $insertSection = $this->createCustomFormSection($data);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertSectionConnector($this->lastSectionId, $data) : $insertConnector = true;
                return array("result" => ($insertSection && $insertConnector), "area" => $data['area']);
            case SectionForm::WEB :
                $insertSection = $this->createWebSection($data);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertSectionConnector($this->lastSectionId, $data) : $insertConnector = true;
                return array("result" => ($insertSection && $insertConnector), "area" => $data['area']);
                break;
            case SectionForm::SECTION :
                $insertSection = $this->createSectionSection($data);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertSectionConnector($this->lastSectionId, $data) : $insertConnector = true;
                return array("result" => ($insertSection && $insertConnector), "area" => $data['area']);
                break;
            case SectionForm::DEV:
                $insertSection = $this->createDevSection($data);
                return array("result" => ($insertSection), "area" => $data['area']);
                break;
            case SectionForm::QR_CODE:
                $insertSection = $this->createQrCodeSection($data);
                return array("result" => ($insertSection), "area" => $data['area']);
                break;
            case SectionForm::USER:
                $insertSection = $this->createUserSection($data);
                ($data['connector'] != "HUB") ? $insertConnector = $this->insertSectionConnector($this->lastSectionId, $data) : $insertConnector = true;
                return array("result" => ($insertSection && $insertConnector), "area" => $data['area']);
                break;
            default:
                return false;
        }

    }

    public function saveSection($id, $data)
    {
        switch ($data["type"]) {
            case SectionForm::RSS :
                return array("result" => $this->updateRssSection($id, $data), "area" => $data['area']);
                break;
            case SectionForm::PODCAST :
                return array("result" => $this->updatePodcastSection($id, $data), "area" => $data['area']);
                break;
            case SectionForm::TWITTER :
                return array("result" => $this->updateTwitterSection($id, $data), "area" => $data['area']);
                break;
            case SectionForm::REGISTER :
                return array("result" => $this->updateRegisterSection($id, $data), "area" => $data['area']);
                break;
            case SectionForm::INFO :
                return array("result" => $this->updateInfoSection($id, $data), "area" => $data['area']);
                break;
            case SectionForm::FAVOURITE :
                $updateSection = $this->updateFavouritesSection($id, $data);
                if ($data['connector'] != LayoutForm::CONNECTOR_TYPE_HUB) {
                    $checkConnector = $this->getSectionConnector($id);
                    if ($checkConnector) {
                        $updateConnector = $this->updateSectionConnector($id, $data);
                    } else {
                        $updateConnector = $this->insertSectionConnector($id, $data);
                    }
                    $deleteConnector = false;
                } else {
                    $updateConnector = false;
                    $deleteConnector = $this->deleteSectionConnector($id);
                }
                return array("result" => ($updateSection && ($updateConnector || $deleteConnector)), "area" => $data['area']);
                break;
            case SectionForm::PERSON :
                $updateSection = $this->updatePersonSection($id, $data);
                if ($data['connector'] != LayoutForm::CONNECTOR_TYPE_HUB) {
                    $checkConnector = $this->getSectionConnector($id);
                    if ($checkConnector) {
                        $updateConnector = $this->updateSectionConnector($id, $data);
                    } else {
                        $updateConnector = $this->insertSectionConnector($id, $data);
                    }
                    $deleteConnector = false;
                } else {
                    $updateConnector = false;
                    $deleteConnector = $this->deleteSectionConnector($id);
                }
                return array("result" => ($updateSection && ($updateConnector || $deleteConnector)), "area" => $data['area']);
                break;
            case SectionForm::EVENT :
                $updateSection = $this->updateEventSection($id, $data);
                if ($data['connector'] != LayoutForm::CONNECTOR_TYPE_HUB) {
                    $checkConnector = $this->getSectionConnector($id);
                    if ($checkConnector) {
                        $updateConnector = $this->updateSectionConnector($id, $data);
                    } else {
                        $updateConnector = $this->insertSectionConnector($id, $data);
                    }
                    $deleteConnector = false;
                } else {
                    $updateConnector = false;
                    $deleteConnector = $this->deleteSectionConnector($id);
                }
                return array("result" => ($updateSection && ($updateConnector || $deleteConnector)), "area" => $data['area']);
                break;
            case SectionForm::POI :
                $updateSection = $this->updatePoiSection($id, $data);
                if ($data['connector'] != LayoutForm::CONNECTOR_TYPE_HUB) {
                    $checkConnector = $this->getSectionConnector($id);
                    if ($checkConnector) {
                        $updateConnector = $this->updateSectionConnector($id, $data);
                    } else {
                        $updateConnector = $this->insertSectionConnector($id, $data);
                    }
                    $deleteConnector = false;
                } else {
                    $updateConnector = false;
                    $deleteConnector = $this->deleteSectionConnector($id);
                }
                return array("result" => ($updateSection && ($updateConnector || $deleteConnector)), "area" => $data['area']);
                break;
            case SectionForm::GENERIC :
                $updateSection = $this->updateGenericSection($id, $data);
                if ($data['connector'] != LayoutForm::CONNECTOR_TYPE_HUB) {
                    $checkConnector = $this->getSectionConnector($id);
                    if ($checkConnector) {
                        $updateConnector = $this->updateSectionConnector($id, $data);
                    } else {
                        $updateConnector = $this->insertSectionConnector($id, $data);
                    }
                    $deleteConnector = false;
                } else {
                    $updateConnector = false;
                    $deleteConnector = $this->deleteSectionConnector($id);
                }
                return array("result" => ($updateSection && ($updateConnector || $deleteConnector)), "area" => $data['area']);
                break;
            case SectionForm::CUSTOM_FORM :
                $updateSection = $this->updateCustomFormSection($id, $data);
                if ($data['connector'] != LayoutForm::CONNECTOR_TYPE_HUB) {
                    $checkConnector = $this->getSectionConnector($id);
                    if ($checkConnector) {
                        $updateConnector = $this->updateSectionConnector($id, $data);
                    } else {
                        $updateConnector = $this->insertSectionConnector($id, $data);
                    }
                    $deleteConnector = false;
                } else {
                    $updateConnector = false;
                    $deleteConnector = $this->deleteSectionConnector($id);
                }
                return array("result" => ($updateSection && ($updateConnector || $deleteConnector)), "area" => $data['area']);
                break;
            case SectionForm::WEB :
                $updateSection = $this->updateWebSection($id, $data);
                if ($data['connector'] != LayoutForm::CONNECTOR_TYPE_HUB) {
                    $checkConnector = $this->getSectionConnector($id);
                    if ($checkConnector) {
                        $updateConnector = $this->updateSectionConnector($id, $data);
                    } else {
                        $updateConnector = $this->insertSectionConnector($id, $data);
                    }
                    $deleteConnector = false;
                } else {
                    $updateConnector = false;
                    $deleteConnector = $this->deleteSectionConnector($id);
                }
                return array("result" => ($updateSection && ($updateConnector || $deleteConnector)), "area" => $data['area']);
                break;
            case SectionForm::SECTION :
                $updateSection = $this->updateSectionSection($id, $data);
                if ($data['connector'] != LayoutForm::CONNECTOR_TYPE_HUB) {
                    $checkConnector = $this->getSectionConnector($id);
                    if ($checkConnector) {
                        $updateConnector = $this->updateSectionConnector($id, $data);
                    } else {
                        $updateConnector = $this->insertSectionConnector($id, $data);
                    }
                    $deleteConnector = false;
                } else {
                    $updateConnector = false;
                    $deleteConnector = $this->deleteSectionConnector($id);
                }
                return array("result" => ($updateSection && ($updateConnector || $deleteConnector)), "area" => $data['area']);
                break;
            case SectionForm::DEV:
                $updateSection = $this->updateDevSection($id, $data);
                return array("result" => ($updateSection), "area" => $data['area']);
                break;
            case SectionForm::QR_CODE:
                $updateSection = $this->updateQrCodeSection($id, $data);
                return array("result" => ($updateSection), "area" => $data['area']);
                break;
            case SectionForm::USER:
                $updateSection = $this->updateUserSection($id, $data);
                if ($data['connector'] != LayoutForm::CONNECTOR_TYPE_HUB) {
                    $checkConnector = $this->getSectionConnector($id);
                    if ($checkConnector) {
                        $updateConnector = $this->updateSectionConnector($id, $data);
                    } else {
                        $updateConnector = $this->insertSectionConnector($id, $data);
                    }
                    $deleteConnector = false;
                } else {
                    $updateConnector = false;
                    $deleteConnector = $this->deleteSectionConnector($id);
                }
                return array("result" => ($updateSection && ($updateConnector || $deleteConnector)), "area" => $data['area']);
                break;
            default:
                return false;
        }

    }

    private function updateSection($id, $data, $layout, $params, $tags = array())
    {
        $slug = SecurityHelper::createSlug($data["title"]);

        $resource = isset($data["resource"]) ? $data["resource"] : null;
        if ($data['type'] == SectionForm::SECTION || $data['type'] == SectionForm::USER) {

            $url_parts = parse_url($resource);
            $query_string = array();
            if (isset($url_parts['query'])) {
                parse_str($url_parts['query'], $query_string);
            }
            $query_string['id'] = $id;
            $url_parts['query'] = http_build_query($query_string);

            $resource = $url_parts['path'] . '?' . $url_parts['query'];
        }

        $json_params = json_encode($params);
        $json_layout = json_encode($layout);
        $json_tags = json_encode($tags);

        $iconId = $data["section_icon_id"] !== "" ? (int)$data["section_icon_id"] : null;

        try {
            $section = SectionQuery::create()
                ->findOneById($id);

            // Multi language fields
            $title = $this->setMultiLanguageObject($data["content_lang"], $section->getTitle(), $data["title"]);
            $description = $this->setMultiLanguageObject($data["content_lang"], $section->getDescription(), $data["description"]);

            $userTags = $data['user_tags'] !== null && $data['user_tags'] !== "" ? json_encode($data['user_tags']) : null;

            $iconIdOld = $section->getIconId();

            $section
                ->setArea($data["area"])
                ->setType($data["type"])
                ->setTitle($title)
                ->setDescription($description)
                ->setSlug($slug)
                ->setIconId($iconId)
                ->setIconSelectedId($iconId)
                ->setResource($resource)
                ->setParams($json_params)
                ->setTemplateId($data['template_id'])
                ->setLayout($json_layout)
                ->setTags($json_tags)
                ->setTagsUser($userTags)
                ->setParsedTags(isset($data["tags"]) ? $data["tags"] : null);
            $section->save();

            if ($iconId !== $iconIdOld && $iconIdOld !== null) {
                $media = MediaQuery::create()
                    ->findOneById($iconIdOld);

                if ($media) {
                    $media->delete();
                }
            }

            $relatedSection = SectionRelatedToQuery::create()
                ->findOneByFromSectionId($section->getId());
            if ($relatedSection) {
                $relatedSection->delete();
            }

            if (isset($data['custom_form_list']) && $data['custom_form_list'] !== "") {
                $relatedSection = new SectionRelatedTo();
                $relatedSection
                    ->setFromSectionId($section->getId())
                    ->setToSectionId($data['custom_form_list'])
                    ->save();
            }

            $response = true;
        } catch (PropelException $exception) {
            $response = false;
        }

        return $response;
    }

    private function insertSection($data, $layout, $params, $tags = array())
    {
        try {
            $section = SectionQuery::create()
                ->select(array('m'))
                ->withColumn('MAX(Section.Weight)', 'm')
                ->find();
            $maxWeightData = $section->getData();
            $maxWeight = $maxWeightData[0] === null ? 0 : $maxWeightData[0];

            $slug = SecurityHelper::createSlug($data["title"]);

            $resource = isset($data["resource"]) ? $data["resource"] : null;
            $json_params = json_encode($params);
            $json_layout = json_encode($layout);
            $json_tags = json_encode($tags);
            $dataTags = isset($data["tags"]) ? $data["tags"] : null;

            $iconId = $data["section_icon_id"] !== "" ? $data["section_icon_id"] : null;

            // Multi language fields
            $title = $this->setMultiLanguageObject($data["content_lang"], $data["title"], $data["title"]);
            $description = $this->setMultiLanguageObject($data["content_lang"], $data["description"], $data["description"]);

            $userTags = $data['user_tags'] !== null && $data['user_tags'] !== "" ? json_encode($data['user_tags']) : null;

            $section = new Section();
            $section
                ->setArea($data["area"])
                ->setType($data["type"])
                ->setTitle($title)
                ->setDescription($description)
                ->setSlug($slug)
                ->setIconId($iconId)
                ->setIconSelectedId($iconId)
                ->setResource($resource)
                ->setParams($json_params)
                ->setTemplateId($data['template_id'])
                ->setLayout($json_layout)
                ->setTags($json_tags)
                ->setTagsUser($userTags)
                ->setParsedTags($dataTags)
                ->setStatus(self::SECTION_STATUS_DRAFT)
                ->setAuthorId($this->aclHelper->getUser()->id)
                ->setWeight($maxWeight + 10);
            $section->save();

            $this->lastSectionId = $section->getId();
            if ($data['type'] == SectionForm::SECTION || $data['type'] == SectionForm::USER) {
                $id = $this->lastSectionId;

                $url_parts = parse_url($resource);
                $query_string = array();
                if (isset($url_parts['query'])) {
                    parse_str($url_parts['query'], $query_string);
                }
                $query_string['id'] = $id;
                $url_parts['query'] = http_build_query($query_string);

                $resource = $url_parts['path'] . '?' . $url_parts['query'];

                $section->setResource($resource);
                $section->save();
            }

            if (isset($data['custom_form_list']) && $data['custom_form_list'] !== "") {
                $relatedSection = new SectionRelatedTo();
                $relatedSection
                    ->setFromSectionId($section->getId())
                    ->setToSectionId($data['custom_form_list'])
                    ->save();
            }

            $result = true;
        } catch (PropelException $exception) {
            $result = false;
        }

        return $result;
    }

    private function updateSectionResource($section_id, $resource)
    {
        try {
            $section = SectionQuery::create()
                ->findOneById($section_id);

            $section->setResource($resource);
            $section->save();
            $response = true;
        } catch (PropelException $exception) {
            $response = false;
        }

        return $response;
    }

    public function insertSectionConnector($section_id, $data)
    {
        $sectionConnector = new SectionConnector();
        $sectionConnector
            ->setSectionId($section_id)
            ->setProviderId($data['provider'])
            ->setType($data['connector'])
            ->setBaseurl($data['connector_base_url'])
            ->setRemoteSectionId(isset($data['remote_section_id']) ? $data['remote_section_id'] : null)
            ->save();

        $result = true;

        switch ($data["type"]) {
            case SectionForm::CUSTOM_FORM:
                break;
            default:
                $resource = sprintf(self::RESOURCE_EXTERNAL_LIST, $sectionConnector->getId());
                $fields = $this->getResourceQueryString($data);

                if (count($fields)) {
                    $resource .= "?" . http_build_query($fields, '', "&");
                }

                $result = $this->updateSectionResource($section_id, $resource);
                break;
        }

        return $result;
    }

    public function updateSectionConnector($section_id, $data)
    {
        $sectionConnector = SectionConnectorQuery::create()
            ->findOneBySectionId($section_id);

        $sectionConnector
            ->setProviderId($data['provider'])
            ->setType($data['connector'])
            ->setBaseurl($data['connector_base_url'])
            ->setRemoteSectionId(isset($data['remote_section_id']) ? $data['remote_section_id'] : null)
            ->save();

        $result = true;

        switch ($data["type"]) {
            case SectionForm::CUSTOM_FORM:
                break;
            default:
                $connector = $this->getSectionConnector($section_id);
                $resource = sprintf(self::RESOURCE_EXTERNAL_LIST, $connector->id);
                $fields = $this->getResourceQueryString($data);

                if (count($fields)) {
                    $resource .= "?" . http_build_query($fields, '', "&");
                }

                $result = $this->updateSectionResource($section_id, $resource);
                break;
        }

        return $result;
    }

    public function getSectionById($id)
    {
        $data = array();
        if (!is_null($this->dbApp)) {
            $stmt = $this->dbApp->prepare(self::$sqlSectionDetail);

            $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
            $stmt->execute();

            $section = $stmt->fetchObject();

            $params = json_decode($section->params);

            $data = $this->flatLayoutJson($section->layout, null, $section->template_id, $section->type);
            $lang = $this->aclHelper->session->currentLang;

            switch ($section->type) {
                case SectionForm::RSS :
                    $data["custom_css"] = $params->custom_css;
                    $data["custom_js"] = $params->custom_js;
                    $data['tags'] = $this->flatTagsJson($section->tags);
                    break;
                case SectionForm::PODCAST :
                    break;
                case SectionForm::TWITTER :
                    $data["enable_retweet"] = $params->enable_retweet;
                    $data["tw_filter"] = str_replace(" OR ", ",", $params->filter);
                    $data["retweet"] = str_replace(" ", ",", $params->retweet);
                    $data['tags'] = $this->flatTagsJson($section->tags);
                    break;
//                case SectionForm::REGISTER :
//                    $data['privacy_link'] = $params->privacy_link;
//                    $data['terms_link'] = $params->terms_link;
//                    $data['tags'] = $this->flatGroupedTagsJson($section->tags);
//                    break;
                case SectionForm::INFO :
                    $data["email"] = $params->email;
                    $data["phone"] = $params->phone;
                    $data["site"] = $params->site;
                    $data["info-description"] = isset($params->body->{$lang}) ? $params->body->{$lang} : $params->body;
                    if ($params->map) {
                        $data["latitude"] = $params->map->latitude;
                        $data["longitude"] = $params->map->longitude;
                    }
                    $data['tags'] = $this->flatTagsJson($section->tags);
                    break;
                case SectionForm::PERSON :
                    $data['tags'] = $this->flatTagsJson($section->tags);
                    $data["order"] = isset($params->order) ? $params->order : "ASC";
                    $sectionConnector = $this->getSectionConnector($id);
                    if ($sectionConnector) {
                        $data['connector'] = $sectionConnector->type;
                        $data['provider'] = $sectionConnector->provider_id;
                        $data['connector_base_url'] = $sectionConnector->baseurl;
                        $data['remote_section_id'] = $sectionConnector->remote_section_id;
                    } else {
                        $data['connector'] = LayoutForm::CONNECTOR_TYPE_HUB;
                    }
                    break;
                case SectionForm::EVENT :
                    $data['tags'] = $this->flatTagsJson($section->tags);
                    $data["order"] = isset($params->order) ? $params->order : "ASC";
                    if (isset($params->from)) $data["start-date"] = $params->from;
                    if (isset($params->to)) $data["end-date"] = $params->to;
                    if (isset($params->from_current)) $data["start-current-date"] = $params->from_current;
                    if (isset($params->to_current)) $data["end-current-date"] = $params->to_current;
                    $sectionConnector = $this->getSectionConnector($id);
                    if ($sectionConnector) {
                        $data['connector'] = $sectionConnector->type;
                        $data['provider'] = $sectionConnector->provider_id;
                        $data['connector_base_url'] = $sectionConnector->baseurl;
                        $data['remote_section_id'] = $sectionConnector->remote_section_id;
                    } else {
                        $data['connector'] = LayoutForm::CONNECTOR_TYPE_HUB;
                    }
                    break;
                case SectionForm::FAVOURITE :
                    $data['event'] = (boolean)in_array("EVENT", $params->types);
                    $data['person'] = (boolean)in_array("PERSON", $params->types);
                    $data['poi'] = (boolean)in_array("POI", $params->types);
                    $data['generic'] = (boolean)in_array("GENERIC", $params->types);
                    $data['web'] = (boolean)in_array("WEB", $params->types);
                    $data['tags'] = $this->flatTagsJson($section->tags);
                    $sectionConnector = $this->getSectionConnector($id);
                    if ($sectionConnector) {
                        $data['connector'] = $sectionConnector->type;
                        $data['provider'] = $sectionConnector->provider_id;
                        $data['connector_base_url'] = $sectionConnector->baseurl;
                        $data['remote_section_id'] = $sectionConnector->remote_section_id;
                    } else {
                        $data['connector'] = LayoutForm::CONNECTOR_TYPE_HUB;
                    }
                    break;
                case SectionForm::POI :
                    $data['tags'] = $this->flatTagsJson($section->tags);
                    $data['scan'] = $params->scan;
                    $data['mode'] = $params->mode;
                    $data['region'] = $params->region->type;
                    if ($params->region->type == "beacon_region") {
                        $data['subtitle'] = $params->region->identifier;
                        $data['uuid'] = $params->region->uuid;
                        $data['major'] = $params->region->major;
                        $data['minor'] = $params->region->minor;
                    } elseif ($params->region->type == "circular_region") {
                        $data['address'] = $params->region->identifier;
                        $data['latitude'] = $params->region->latitude;
                        $data['longitude'] = $params->region->longitude;
                        $data['zoom_map'] = isset($params->region->zoom) ? $params->region->zoom : PoiSectionForm::ZOOM_MAP_DEFAULT;
                    }
                    $sectionConnector = $this->getSectionConnector($id);
                    if ($sectionConnector) {
                        $data['connector'] = $sectionConnector->type;
                        $data['provider'] = $sectionConnector->provider_id;
                        $data['connector_base_url'] = $sectionConnector->baseurl;
                        $data['remote_section_id'] = $sectionConnector->remote_section_id;
                    } else {
                        $data['connector'] = LayoutForm::CONNECTOR_TYPE_HUB;
                    }
                    break;
                case SectionForm::GENERIC :
                    $data['tags'] = $this->flatTagsJson($section->tags);
                    $data["order"] = isset($params->order) ? $params->order : "ASC";
                    $sectionConnector = $this->getSectionConnector($id);
                    if ($sectionConnector) {
                        $data['connector'] = $sectionConnector->type;
                        $data['provider'] = $sectionConnector->provider_id;
                        $data['connector_base_url'] = $sectionConnector->baseurl;
                        $data['remote_section_id'] = $sectionConnector->remote_section_id;
                    } else {
                        $data['connector'] = LayoutForm::CONNECTOR_TYPE_HUB;
                    }
                    break;
                case SectionForm::CUSTOM_FORM :
                    $successMessage = isset($params->success_message) ? (is_string($params->success_message) ? $params->success_message : $this->getLocaleText($lang, json_encode($params->success_message))) : "";

                    $data['form_type'] = $params->form_type;
                    $data['privacy_link'] = $params->privacy_link;
                    $data['terms_link'] = $params->terms_link;
                    $data['forward_to'] = $params->forward_to;
                    $data['success_message'] = $successMessage;
                    $data['tags'] = $this->flatTagsJson($section->tags);

                    $cleanedJson = $this->emptyOldLabelValue($params->fieldsets);
                    $obj = new \stdClass();
                    $obj->fieldsets = $cleanedJson;
                    $data['custom_forms_details'] = json_encode($obj);
                    $sectionConnector = $this->getSectionConnector($id);
                    if ($sectionConnector) {
                        $data['connector'] = $sectionConnector->type;
                        $data['provider'] = $sectionConnector->provider_id;
                        $data['connector_base_url'] = $sectionConnector->baseurl;
                        $data['remote_section_id'] = $sectionConnector->remote_section_id;
                    } else {
                        $data['connector'] = LayoutForm::CONNECTOR_TYPE_HUB;
                    }
                    break;
                case SectionForm::WEB :
                    $data['tags'] = $this->flatTagsJson($section->tags);
                    $data["order"] = isset($params->order) ? $params->order : "ASC";
                    $sectionConnector = $this->getSectionConnector($id);
                    if ($sectionConnector) {
                        $data['connector'] = $sectionConnector->type;
                        $data['provider'] = $sectionConnector->provider_id;
                        $data['connector_base_url'] = $sectionConnector->baseurl;
                        $data['remote_section_id'] = $sectionConnector->remote_section_id;
                    } else {
                        $data['connector'] = LayoutForm::CONNECTOR_TYPE_HUB;
                    }
                    break;
                case SectionForm::SECTION :
                    $data['tags'] = $this->flatTagsJson($section->tags);
                    $data["order"] = isset($params->order) ? $params->order : "ASC";
                    $sectionConnector = $this->getSectionConnector($id);
                    if ($sectionConnector) {
                        $data['connector'] = $sectionConnector->type;
                        $data['provider'] = $sectionConnector->provider_id;
                        $data['connector_base_url'] = $sectionConnector->baseurl;
                        $data['remote_section_id'] = $sectionConnector->remote_section_id;
                    } else {
                        $data['connector'] = LayoutForm::CONNECTOR_TYPE_HUB;
                    }
                    break;
                case SectionForm::DEV:
                    $data['dev_section_uri'] = $params->controller_uri->uri;
                    $data['dev_section_ios'] = $params->controller_uri->ios;
                    $data['dev_section_android'] = $params->controller_uri->android;
                    $data['section_dev_extra_params'] = json_encode($params->extra_params);
                    break;
                case SectionForm::USER:
                    $data['tags'] = $this->flatTagsJson($section->tags);
                    $sectionConnector = $this->getSectionConnector($id);
                    if ($sectionConnector) {
                        $data['connector'] = $sectionConnector->type;
                        $data['provider'] = $sectionConnector->provider_id;
                        $data['connector_base_url'] = $sectionConnector->baseurl;
                        $data['remote_section_id'] = $sectionConnector->remote_section_id;
                    } else {
                        $data['connector'] = LayoutForm::CONNECTOR_TYPE_HUB;
                    }

                    $data['custom_form_list'] = $params->custom_form_id;
                    $data['user_section_detail_layout'] = $params->detail_layout;
                    $data['user_section_content_filter'] = $params->content_filter;
                    $data['user_section_custom_form_field_map_json'] = json_encode($params->fields_map);
                    $data['upload_user_section_add_icon_position'] = $params->add_icon->position;
                    $data["user_section_add_icon_id"] = $params->add_icon->id;
                    $data["upload_user_section_add_icon_label"] = isset($params->add_icon->label) ? $this->getLocaleText($lang, json_encode($params->add_icon->label)) : "";
                    $iconId = $data["user_section_add_icon_id"];
                    if (is_numeric($iconId)) {
                        $mediaModel = new MediaModel($this->serviceLocator);
                        $icon = $mediaModel->getMediaById($iconId);
                        $data['user_section_add_icon'] = $icon->uri;
                    }

                    break;
//                default:
//                    return false;
            }

            // Multi language fields
            $title = $this->getLocaleText($lang, $section->title);
            $description = $this->getLocaleText($lang, $section->description);

            $data["content_lang"] = $lang;
            $data["title"] = $title;
            $data["description"] = $description;

            $data["area"] = $section->area;
            $data["user_tags"] = json_decode($section->tags_user);
            $data["type"] = $section->type;
            $data["section_icon_id"] = $section->icon_id;
            $data["resource"] = $section->resource;
            $data["reload_content"] = 0;

            $iconId = $data["section_icon_id"];
            if (is_integer($iconId)) {
                $mediaModel = new MediaModel($this->serviceLocator);
                $icon = $mediaModel->getMediaById($iconId);
                $data['section_icon_name'] = $icon->uri;
            }
        }

        return $data;
    }

    public function getSectionByPk($sectionId)
    {
        $section = SectionQuery::create()
            ->findPk($sectionId);

        return $section;
    }

    public function getSectionParamsById($sectionId)
    {
        $params = array();
        $section = SectionQuery::create()
            ->findPk($sectionId);

        if ($section) {
            $params = json_decode($section->getParams());
        }

        return $params;
    }

    private function emptyOldLabelValue($fieldsets)
    {
        foreach ($fieldsets as $fieldset) {
            foreach ($fieldset->fields as $field) {
                $field->old_label = $field->label;
            }
        }

        return $fieldsets;
    }

    private function getSectionConnector($section_id)
    {
        $stmt = $this->dbApp->prepare(self::$sqlGetSectionConnector);

        $stmt->bindParam(":sectionId", $section_id, \PDO::PARAM_INT);

        $stmt->execute();

        $sectionConnector = $stmt->fetchObject();

        return $sectionConnector;
    }

    private function createRssSection($data)
    {
        $params = array("paged" => boolval($data["paged"]), "custom_css" => $data['custom_css'], "custom_js" => $data['custom_js']);
        $layout = $this->parseLayout($data);
        $layout['paged'] = (boolean)$data['paged'];
        $layout['open_in_app'] = (boolean)$data['open_in_app'];

        $tags = $this->parseTag($data);

        return $this->insertSection($data, $layout, $params, $tags);
    }

    private function updateRssSection($id, $data)
    {
        $params = array("paged" => boolval($data["paged"]), "custom_css" => $data['custom_css'], "custom_js" => $data['custom_js']);
        $layout = $this->parseLayout($data);
        $layout['paged'] = (boolean)$data['paged'];
        $layout['open_in_app'] = (boolean)$data['open_in_app'];

        $tags = $this->parseTag($data);

        return $this->updateSection($id, $data, $layout, $params, $tags);
    }

    private function createPodcastSection($data)
    {
        $params = array();
        $layout = $this->parseLayout($data);
        $layout['paged'] = (boolean)$data['paged'];
        $layout['autoplay'] = (boolean)$data['autoplay'];
        $layout['streaming'] = (boolean)$data['streaming'];

        $tags = $this->parseTag($data);

        return $this->insertSection($data, $layout, $params, $tags);
    }

    private function updatePodcastSection($id, $data)
    {
        $params = array();
        $layout = $this->parseLayout($data);
        $layout['paged'] = (boolean)$data['paged'];
        $layout['autoplay'] = (boolean)$data['autoplay'];
        $layout['streaming'] = (boolean)$data['streaming'];

        $tags = $this->parseTag($data);

        return $this->updateSection($id, $data, $layout, $params, $tags);
    }

    private function createPersonSection($data)
    {
        $data["resource"] = self::RESOURCE_PERSON_LIST;

        $params = array(
            'order' => $data['order'],
        );
        $layout = $this->parseLayout($data);
        $tags = $this->parseTag($data);

        $fields = array(
            'order' => $data['order']
        );
        if (count($tags)) {
            $ids = array();
            foreach ($tags as $tag) {
                $ids [] = $tag["key"];
            }
            $fields['tags'] = implode(SectionModel::RESOURCE_PARAMS_SEPARATOR, $ids);
        }

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        return $this->insertSection($data, $layout, $params, $tags);
    }

    private function updatePersonSection($id, $data)
    {
        $data["resource"] = self::RESOURCE_PERSON_LIST;

        $params = array(
            'order' => $data['order'],
        );
        $layout = $this->parseLayout($data);
        $tags = $this->parseTag($data);

        $fields = array(
            'order' => $data['order']
        );
        if (count($tags)) {
            $ids = array();
            foreach ($tags as $tag) {
                $ids [] = $tag["key"];
            }
            $fields['tags'] = implode(SectionModel::RESOURCE_PARAMS_SEPARATOR, $ids);
        }

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        return $this->updateSection($id, $data, $layout, $params, $tags);
    }

    private function createPoiSection($data)
    {
        list($data, $params, $layout, $tags) = $this->getPoiSectionData($data);
        return $this->insertSection($data, $layout, $params, $tags);
    }

    private function updatePoiSection($id, $data)
    {
        list($data, $params, $layout, $tags) = $this->getPoiSectionData($data);
        return $this->updateSection($id, $data, $layout, $params, $tags);
    }

    private function createGenericSection($data)
    {
        $data["resource"] = self::RESOURCE_GENERIC_LIST;

        $params = array(
            'order' => $data['order'],
        );
        $layout = $this->parseLayout($data);
        $tags = $this->parseTag($data);

        $fields = array(
            'order' => $data['order']
        );
        if (count($tags)) {
            $ids = array();
            foreach ($tags as $tag) {
                $ids [] = $tag["key"];
            }
            $fields['tags'] = implode(self::RESOURCE_PARAMS_SEPARATOR, $ids);
        }

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        return $this->insertSection($data, $layout, $params, $tags);
    }

    private function updateGenericSection($id, $data)
    {
        $data["resource"] = self::RESOURCE_GENERIC_LIST;

        $params = array(
            'order' => $data['order'],
        );
        $layout = $this->parseLayout($data);
        $tags = $this->parseTag($data);

        $fields = array(
            'order' => $data['order']
        );
        if (count($tags)) {
            $ids = array();
            foreach ($tags as $tag) {
                $ids [] = $tag["key"];
            }
            $fields['tags'] = implode(self::RESOURCE_PARAMS_SEPARATOR, $ids);
        }

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        return $this->updateSection($id, $data, $layout, $params, $tags);
    }

    private function createSectionSection($data)
    {
        $data["resource"] = self::RESOURCE_SECTION_LIST;

        $params = array(
            'order' => $data['order'],
        );
        $layout = $this->parseLayout($data);
        $tags = $this->parseTag($data);

        $fields = array(
            'order' => $data['order']
        );
        if (count($tags)) {
            $ids = array();
            foreach ($tags as $tag) {
                $ids [] = $tag["key"];
            }
            $fields['tags'] = implode(self::RESOURCE_PARAMS_SEPARATOR, $ids);
        }

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        return $this->insertSection($data, $layout, $params, $tags);
    }

    private function updateSectionSection($id, $data)
    {
        $data["resource"] = self::RESOURCE_SECTION_LIST;

        $params = array(
            'order' => $data['order'],
        );
        $layout = $this->parseLayout($data);
        $tags = $this->parseTag($data);

        $fields = array(
            'order' => $data['order']
        );
        if (count($tags)) {
            $ids = array();
            foreach ($tags as $tag) {
                $ids [] = $tag["key"];
            }
            $fields['tags'] = implode(self::RESOURCE_PARAMS_SEPARATOR, $ids);
        }

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        return $this->updateSection($id, $data, $layout, $params, $tags);
    }

    private function createDevSection($data)
    {
        $data["resource"] = $data["dev_section_uri"];

        $layout = $this->parseLayout($data);
        $params['extra_params'] = json_decode($data["section_dev_extra_params"]);
        $params['controller_uri']['uri'] = $data["dev_section_uri"];
        $params['controller_uri']['android'] = $data["dev_section_android"];
        $params['controller_uri']['ios'] = $data["dev_section_ios"];

        return $this->insertSection($data, $layout, $params);
    }

    private function updateDevSection($id, $data)
    {
        $data["resource"] = $data["dev_section_uri"];

        $layout = $this->parseLayout($data);
        $params['extra_params'] = json_decode($data["section_dev_extra_params"]);
        $params['controller_uri']['uri'] = $data["dev_section_uri"];
        $params['controller_uri']['android'] = $data["dev_section_android"];
        $params['controller_uri']['ios'] = $data["dev_section_ios"];

        return $this->updateSection($id, $data, $layout, $params);
    }

    private function createQrCodeSection($data)
    {
        $data["resource"] = null;

        $layout = $this->parseLayout($data);
        $params = array();

        return $this->insertSection($data, $layout, $params);
    }

    private function updateQrCodeSection($id, $data)
    {
        $data["resource"] = null;

        $layout = $this->parseLayout($data);
        $params = array();

        return $this->updateSection($id, $data, $layout, $params);
    }

    private function createUserSection($data)
    {
        $data["resource"] = self::RESOURCE_USER_LIST;

        $userSectionAddIconPlaceholder = $this->getBaseurl() . "/img/placeholder/add.png";
        $defaultPosition = "FLOAT";

        $layout = $this->parseLayout($data);
        $tags = $this->parseTag($data);

        $iconSize = ImageHelper::ICON_GLOBAL_SIZE;
        $iconSizeFormat = $iconSize . "x" . $iconSize;
        $params = array();
        $params['custom_form_id'] = $data['custom_form_list'];
        $params['detail_layout'] = $data['user_section_detail_layout'];
        $params['content_filter'] = $data['user_section_content_filter'];
        $params['fields_map'] = json_decode($data['user_section_custom_form_field_map_json']);
        $params['add_icon'] = array(
            'id' => $data['user_section_add_icon_id'] !== "" ? $data['user_section_add_icon_id'] : null,
            'label' => $data['upload_user_section_add_icon_label'] !== "" ? $this->setMultiLanguageObject($data["content_lang"], $data['upload_user_section_add_icon_label'], $data['upload_user_section_add_icon_label'], false) : null,
            'url' => $data['user_section_add_icon'] !== "" ? $this->getCdnBaseurl() . ImageHelper::getActionIconUrl($iconSizeFormat) . $data['user_section_add_icon'] : $userSectionAddIconPlaceholder,
            'thumb' => $data['user_section_add_icon'] !== "" ? $this->getCdnBaseurl() . ImageHelper::getActionIconThumbUrl() . $data['user_section_add_icon'] : $userSectionAddIconPlaceholder,
            'position' => $data['upload_user_section_add_icon_position'] !== "" ? $data['upload_user_section_add_icon_position'] : $defaultPosition
        );

//        var_dump($data);
//        var_dump($params);
//
//        die();

        $fields = array();
        if (count($tags)) {
            $ids = array();
            foreach ($tags as $tag) {
                $ids [] = $tag["key"];
            }
            $fields['tags'] = implode(self::RESOURCE_PARAMS_SEPARATOR, $ids);
        }

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        return $this->insertSection($data, $layout, $params, $tags);
    }

    private function updateUserSection($id, $data)
    {
        $userSectionAddIconPlaceholder = $this->getBaseurl() . "/img/placeholder/add.png";
        $defaultPosition = "FLOAT";

        $section = SectionQuery::create()
            ->findPk($id);

        $sectionParams = json_decode($section->getParams());

        $data["resource"] = self::RESOURCE_USER_LIST;
        $layout = $this->parseLayout($data);
        $tags = $this->parseTag($data);

        $iconSize = ImageHelper::ICON_GLOBAL_SIZE;
        $iconSizeFormat = $iconSize . "x" . $iconSize;
        $params = array();
        $params['custom_form_id'] = $data['custom_form_list'];
        $params['detail_layout'] = $data['user_section_detail_layout'];
        $params['content_filter'] = $data['user_section_content_filter'];
        $params['fields_map'] = json_decode($data['user_section_custom_form_field_map_json']);
        $params['add_icon'] = array(
            'id' => $data['user_section_add_icon_id'] !== "" ? $data['user_section_add_icon_id'] : null,
            'label' => $data['upload_user_section_add_icon_label'] !== "" ? $this->setMultiLanguageObject($data["content_lang"], json_encode($sectionParams->add_icon->label), $data['upload_user_section_add_icon_label'], false) : null,
            'url' => $data['user_section_add_icon'] !== "" ? $this->getCdnBaseurl() . ImageHelper::getActionIconUrl($iconSizeFormat) . $data['user_section_add_icon'] : $userSectionAddIconPlaceholder,
            'thumb' => $data['user_section_add_icon'] !== "" ? $this->getCdnBaseurl() . ImageHelper::getActionIconThumbUrl() . $data['user_section_add_icon'] : $userSectionAddIconPlaceholder,
            'position' => $data['upload_user_section_add_icon_position'] !== "" ? $data['upload_user_section_add_icon_position'] : $defaultPosition
        );

//        var_dump($data);
//        var_dump($params);
//
//        die();

        $fields = array();
        if (count($tags)) {
            $ids = array();
            foreach ($tags as $tag) {
                $ids [] = $tag["key"];
            }
            $fields['tags'] = implode(self::RESOURCE_PARAMS_SEPARATOR, $ids);
        }

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        return $this->updateSection($id, $data, $layout, $params, $tags);
    }

    private function createWebSection($data)
    {
        $data["resource"] = self::RESOURCE_WEB_LIST;

        $params = array(
            'order' => $data['order'],
        );
        $layout = $this->parseLayout($data);
        $tags = $this->parseTag($data);

        $fields = array(
            'order' => $data['order']
        );
        if (count($tags)) {
            $ids = array();
            foreach ($tags as $tag) {
                $ids [] = $tag["key"];
            }
            $fields['tags'] = implode(self::RESOURCE_PARAMS_SEPARATOR, $ids);
        }

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        return $this->insertSection($data, $layout, $params, $tags);
    }

    private function updateWebSection($id, $data)
    {
        $data["resource"] = self::RESOURCE_WEB_LIST;

        $params = array(
            'order' => $data['order'],
        );
        $layout = $this->parseLayout($data);
        $tags = $this->parseTag($data);

        $fields = array(
            'order' => $data['order']
        );
        if (count($tags)) {
            $ids = array();
            foreach ($tags as $tag) {
                $ids [] = $tag["key"];
            }
            $fields['tags'] = implode(self::RESOURCE_PARAMS_SEPARATOR, $ids);
        }

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        return $this->updateSection($id, $data, $layout, $params, $tags);
    }

    private function createEventSection($data)
    {
        $data["resource"] = self::RESOURCE_EVENT_LIST;

        $data['start-current-date'] = $data['start-current-date'] === '0' ? false : true;
        $data['end-current-date'] = $data['end-current-date'] === '0' ? false : true;
        $data['start-date'] = isset($data['start-date']) && $data['start-date'] !== "" && !$data['start-current-date'] ? $data['start-date'] . ":00" : "";
        $data['end-date'] = isset($data['end-date']) && $data['end-date'] !== "" && !$data['end-current-date'] ? $data['end-date'] . ":00" : "";

        $params = array(
            'from' => $data['start-date'],
            'to' => $data['end-date'],
            'from_current' => $data['start-current-date'],
            'to_current' => $data['end-current-date'],
            'order' => $data['order'],
        );


        $layout = $this->parseLayout($data);
        $fields = $this->getResourceQueryString($data);
        $tags = $this->parseTag($data);

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        return $this->insertSection($data, $layout, $params, $tags);
    }

    private function updateEventSection($id, $data)
    {
        $data["resource"] = self::RESOURCE_EVENT_LIST;

        $data['start-current-date'] = $data['start-current-date'] === '0' ? false : true;
        $data['end-current-date'] = $data['end-current-date'] === '0' ? false : true;
        $data['start-date'] = isset($data['start-date']) && $data['start-date'] !== "" && !$data['start-current-date'] ? $data['start-date'] . ":00" : "";
        $data['end-date'] = isset($data['end-date']) && $data['end-date'] !== "" && !$data['end-current-date'] ? $data['end-date'] . ":00" : "";

        $params = array(
            'from' => $data['start-date'],
            'to' => $data['end-date'],
            'from_current' => $data['start-current-date'],
            'to_current' => $data['end-current-date'],
            'order' => $data['order'],
        );
        $layout = $this->parseLayout($data);
        $fields = $this->getResourceQueryString($data);
        $tags = $this->parseTag($data);

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        return $this->updateSection($id, $data, $layout, $params, $tags);
    }

    private function createFavouritesSection($data)
    {
        $data["resource"] = self::RESOURCE_FAVOURITES_LIST;

        $config = $this->serviceLocator->get('config');
        $content_types = $config['content_types'];

        if ($data['connector'] !== LayoutForm::CONNECTOR_TYPE_HUB) {
            $data['like'] = "0";
            $data['event'] = "0";
            $data['person'] = "0";
            $data['poi'] = "0";
            $data['generic'] = "0";
            $data['web'] = "0";
        }

        $params = array('types' => array());
        $layout = $this->parseLayout($data);

        foreach ($content_types as $type => $content) {
            if ($data[strtolower($type)] != 0) {
                $params['types'][] = $type;
            }
        }

        if (count($params['types']) > 0) {
            $fields = array(
                'types' => implode(self::RESOURCE_PARAMS_SEPARATOR, $params['types'])
            );
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        $tags = $this->parseTag($data);

        return $this->insertSection($data, $layout, $params, $tags);
    }

    private function updateFavouritesSection($id, $data)
    {
        $data["resource"] = self::RESOURCE_FAVOURITES_LIST;

        $config = $this->serviceLocator->get('config');
        $content_types = $config['content_types'];

        if ($data['connector'] !== LayoutForm::CONNECTOR_TYPE_HUB) {
            $data['like'] = "0";
            $data['event'] = "0";
            $data['person'] = "0";
            $data['poi'] = "0";
            $data['generic'] = "0";
            $data['web'] = "0";
        }

        $params = array('types' => array());
        $layout = $this->parseLayout($data);

        foreach ($content_types as $type => $content) {
            if ($data[strtolower($type)] != 0) {
                $params['types'][] = $type;
            }
        }

        if (count($params['types']) > 0) {
            $fields = array(
                'types' => implode(self::RESOURCE_PARAMS_SEPARATOR, $params['types'])
            );
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        $tags = $this->parseTag($data);

        return $this->updateSection($id, $data, $layout, $params, $tags);
    }

    private function createTwitterSection($data)
    {
        $params = array(
            "enable_retweet" => (boolean)$data["enable_retweet"],
            "filter" => str_replace(",", " OR ", $data["tw_filter"]),
            "retweet" => str_replace(",", " ", $data["retweet"]),
        );
        $layout = $this->parseLayout($data);

        $data["resource"] = str_replace(",", " OR ", $data["tw_filter"]);
        $tags = $this->parseTag($data);

        return $this->insertSection($data, $layout, $params, $tags);
    }

    private function updateTwitterSection($id, $data)
    {
        $params = array(
            "enable_retweet" => (boolean)$data["enable_retweet"],
            "filter" => str_replace(",", " OR ", $data["tw_filter"]),
            "retweet" => str_replace(",", " ", $data["retweet"]),
        );
        $layout = $this->parseLayout($data);

        $data["resource"] = str_replace(",", " OR ", $data["tw_filter"]);
        $tags = $this->parseTag($data);

        return $this->updateSection($id, $data, $layout, $params, $tags);
    }

    private function createRegisterSection($data)
    {
        $data["resource"] = "";

        $params = array();
        $params['privacy_link'] = $data['privacy_link'];
        $params['terms_link'] = $data['terms_link'];

        $layout = $this->parseLayout($data);

        $tagModel = new TagModel($this->serviceLocator);
        $type = $data['type'];
        $tags = json_decode($data['tags']);

        $parsedTags = array();

        foreach ($tags as $t) {
            $newTags = $tagModel->parseTagList(explode(",", $t->tags), $type, $t->label);
            $parsedTags = array_merge($parsedTags, $newTags);
        }

        $this->insertSection($data, $layout, $params, $parsedTags);

        $res = sprintf(self::RESOURCE_REGISTER_USER, $this->lastSectionId);
        $stmt = $this->dbApp->prepare(self::$sqlSectionRes);

        $stmt->bindParam(":id", $this->lastSectionId, \PDO::PARAM_INT);
        $stmt->bindParam(":r", $res, \PDO::PARAM_STR);

        return $stmt->execute();
    }

    private function updateRegisterSection($id, $data)
    {
        $data["resource"] = sprintf(self::RESOURCE_REGISTER_USER, $id);

        $params = array();
        $params['privacy_link'] = $data['privacy_link'];
        $params['terms_link'] = $data['terms_link'];

        $layout = $this->parseLayout($data);

        $tagModel = new TagModel($this->serviceLocator);
        $type = $data['type'];
        $tags = json_decode($data['tags']);

        $parsedTags = array();

        foreach ($tags as $t) {
            $newTags = $tagModel->parseTagList(explode(",", $t->tags), $type, $t->label);
            $parsedTags = array_merge($parsedTags, $newTags);
        }

        return $this->updateSection($id, $data, $layout, $params, $parsedTags);
    }

    private function createInfoSection($data)
    {
        $params = array(
            'phone' => ($data['phone'] == "") ? null : $data['phone'],
            'email' => ($data['email'] == "") ? null : $data['email'],
            'site' => ($data['site'] == "") ? null : $data['site'],
            'body' => $this->setMultiLanguageObject($data["content_lang"], $data['info-description'], $data['info-description'], $toJson = false),
            'map' => null
        );

        if ($data['address']) {
            $map = array(
                "latitude" => (double)$data['latitude'],
                "longitude" => (double)$data['longitude'],
                "address" => $data['address'],
                "street" => $data['street'],
                "state" => $data['state'],
                "city" => $data['city'],
                "country" => $data['country'],
                "zipcode" => $data['zip'],
            );
            $params['map'] = $map;
        }
        $layout = $this->parseLayout($data);

        $tags = $this->parseTag($data);

        return $this->insertSection($data, $layout, $params, $tags);
    }

    private function updateInfoSection($id, $data)
    {
        $section = SectionQuery::create()
            ->findPk($id);

        $oldParams = $section->getParams();
        $oldParamsObj = json_decode($oldParams);

        if ($oldParamsObj) {
            if (is_string($oldParamsObj->body)) {
                $body = $oldParamsObj->body;
            } else {
                $body = json_encode($oldParamsObj->body);
            }
        } else {
            $body = $data['info-description'];
        }

        $params = array(
            'phone' => ($data['phone'] == "") ? null : $data['phone'],
            'email' => ($data['email'] == "") ? null : $data['email'],
            'site' => ($data['site'] == "") ? null : $data['site'],
            'body' => $this->setMultiLanguageObject($data["content_lang"], $body, $data['info-description'], $toJson = false),
            'map' => null
        );

        if ($data['address']) {
            $map = array(
                "latitude" => (double)$data['latitude'],
                "longitude" => (double)$data['longitude'],
                "address" => $data['address'],
                "street" => $data['street'],
                "state" => $data['state'],
                "city" => $data['city'],
                "country" => $data['country'],
                "zipcode" => $data['zip'],
            );
            $params['map'] = $map;
        }
        $layout = $this->parseLayout($data);

        $tags = $this->parseTag($data);

        return $this->updateSection($id, $data, $layout, $params, $tags);
    }

    private function createCustomFormSection($data)
    {
        $data["resource"] = "";

        $privacy_link = null;
        $terms_link = null;
        $forward_to = null;

        switch ($data['form_type']) {
            case CustomFormSectionForm::USER_FORM:
                $data['area'] = SectionForm::AREA_HIDDEN_LABEL;
                $data['auth'] = LayoutForm::AUTH_MODE_MANDATORY;
                break;
            case CustomFormSectionForm::REGISTRATION_FORM:
                $privacy_link = $data['privacy_link'];
                $terms_link = $data['terms_link'];
                $data['auth'] = LayoutForm::AUTH_MODE_OPTIONAL;
                break;
            case CustomFormSectionForm::CONTACT_FORM:
                $forward_to = $data['forward_to'];
                break;
        }

        $fieldSets = json_decode($data['custom_forms_details'])->fieldsets;

        $tagModel = new TagModel($this->serviceLocator);
        $type = $data['type'];

        foreach ($fieldSets as $fieldSet) {
            foreach ($fieldSet->fields as $field) {
                if ($field->type == "TAG_GROUP") {
//                    $tagModel->parseTagList(explode(",", $field->value), $type);
                    $tagModel->parseTagListObject($field->value, $type . self::CUSTOM_FORM_TYPE_SUFFIX);
                }
            }
        }

        $params = array(
            'form_type' => $data['form_type'],
            'privacy_link' => $privacy_link,
            'terms_link' => $terms_link,
            'forward_to' => $forward_to,
            'success_message' => $this->setMultiLanguageObject($data["content_lang"], $data['success_message'], $data['success_message'], $toJson = false),
            'fieldsets' => $fieldSets,
        );

        $layout = $this->parseLayout($data);

        $fields = array();

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        $tags = $this->parseTag($data);

        $res = $this->insertSection($data, $layout, $params, $tags);

        $resource = sprintf(SectionModel::RESOURCE_FORM_USER, $this->lastSectionId);
        $stmt = $this->dbApp->prepare(self::$sqlSectionRes);

        $stmt->bindParam(":id", $this->lastSectionId, \PDO::PARAM_INT);
        $stmt->bindParam(":r", $resource, \PDO::PARAM_STR);

        $stmt->execute();

        $this->createCustomFormTable($this->lastSectionId, $params);

        return $res;
    }

    private function createCustomFormTable($section_id, $params)
    {
        $sqlFields = array();
        $fieldsets = $params['fieldsets'];
        foreach ($fieldsets as $fieldset) {
            $fields = $fieldset->fields;
            foreach ($fields as $field) {
                $label = $field->label;
                if (isset($field->name)) {
                    $label = $field->name;
                }
                $sqlFields [] = $this->getAddFormFieldSql($label, $field->type);
            }
        }

        $sql = sprintf(self::$sqlCreateForm, $section_id, implode("", $sqlFields), $section_id, $section_id);
        $stmt = $this->dbApp->prepare($sql);

        return $stmt->execute();
    }

    private function updateCustomFormTable($section_id, $params)
    {
        $sqlFields = array();

        $deletedFields = $params['deleted_fields'];
        if ($deletedFields != null) {
            foreach ($deletedFields as $field) {
                $encode_label = base64_encode($field);
                $sqlFields [] = "DROP `$encode_label` ";
            }
        }

        $fieldsets = $params['fieldsets'];
        foreach ($fieldsets as $fieldset) {
            $fields = $fieldset->fields;
            foreach ($fields as $field) {
                $label = $field->label;
                if (isset($field->name)) {
                    $label = $field->name;
                }
                $sqlPart = $this->getUpdateFormFieldSql($label, $field->old_label, $field->type);
                if ($sqlPart != null) {
                    $sqlFields [] = $sqlPart;
                }
            }
        }

        $sql = sprintf(self::$sqlUpdateForm, $section_id, implode(", ", $sqlFields));
        $stmt = $this->dbApp->prepare($sql);

        return $stmt->execute();
    }

    private function getAddFormFieldSql($label, $type)
    {
        $result = "";
        $encode_label = base64_encode($label);
        switch ($type) {
            case self::HPFORM_INPUT_TEXT:
                $result = "`$encode_label` varchar(1024) DEFAULT NULL, ";
                break;
            case self::HPFORM_TEXTAREA:
                $result = "`$encode_label` longtext, ";
                break;
            case self::HPFORM_IMAGE:
                $result = "`$encode_label` int(11) unsigned DEFAULT NULL, ";
                break;
            case self::HPFORM_CHECKBOX:
                $result = "`$encode_label` longtext, "; //$result = "`$encode_label` tinyint(1) unsigned DEFAULT NULL, ";
                break;
            case self::HPFORM_RADIOBOX:
                $result = "`$encode_label` longtext, "; //$result = "`$encode_label` tinyint(1) unsigned DEFAULT NULL, ";
                break;
            case self::HPFORM_ADDRESS:
                $result = "`$encode_label` int(11) unsigned DEFAULT NULL, ";
                break;
            case self::HPFORM_TAG_GROUP:
                $result = "`$encode_label` longtext, ";
                break;
        }

        return $result;
    }

    private function getUpdateFormFieldSql($label, $old_label, $type)
    {
        $result = "";
        $encode_label = base64_encode($label);

        if ($old_label == null || $old_label == "") {
            switch ($type) {
                case self::HPFORM_INPUT_TEXT:
                    $result = " ADD `$encode_label` varchar(1024) DEFAULT NULL";
                    break;
                case self::HPFORM_TEXTAREA:
                    $result = " ADD `$encode_label` longtext";
                    break;
                case self::HPFORM_IMAGE:
                    $result = " ADD `$encode_label` int(11) unsigned DEFAULT NULL";
                    break;
                case self::HPFORM_CHECKBOX:
                    $result = " ADD `$encode_label` longtext";
                    break;
                case self::HPFORM_RADIOBOX:
                    $result = " ADD `$encode_label` longtext";
                    break;
                case self::HPFORM_ADDRESS:
                    $result = " ADD `$encode_label` int(11) unsigned DEFAULT NULL";
                    break;
                case self::HPFORM_TAG_GROUP:
                    $result = " ADD `$encode_label` longtext";
                    break;
                case self::HPFORM_SELECT:
                    $result = " ADD `$encode_label` longtext";
                    break;
            }
        }

        return $result;
    }

    private function updateCustomFormSection($id, $data)
    {
        $section = SectionQuery::create()
            ->findPk($id);

        $oldParams = $section->getParams();
        $oldParamsObj = json_decode($oldParams);

        if ($oldParamsObj) {
            if (is_string($oldParamsObj->success_message)) {
                $successMessage = $oldParamsObj->success_message;
            } else {
                $successMessage = json_encode($oldParamsObj->success_message);
            }
        } else {
            $successMessage = $data['success_message'];
        }

        $data["resource"] = sprintf(SectionModel::RESOURCE_FORM_USER, $id);

        $privacy_link = null;
        $terms_link = null;
        $forward_to = null;

        switch ($data['form_type']) {
            case CustomFormSectionForm::USER_FORM:
                $data['area'] = SectionForm::AREA_HIDDEN_LABEL;
                $data['auth'] = LayoutForm::AUTH_MODE_MANDATORY;
                break;
            case CustomFormSectionForm::REGISTRATION_FORM:
                $privacy_link = $data['privacy_link'];
                $terms_link = $data['terms_link'];
                $data['auth'] = LayoutForm::AUTH_MODE_OPTIONAL;
                break;
            case CustomFormSectionForm::CONTACT_FORM:
                $forward_to = $data['forward_to'];
                break;
        }

        $fieldsets = json_decode($data['custom_forms_details'])->fieldsets;
        $deleted_fields = json_decode($data['custom_forms_deleted']);

        $params = array(
            'form_type' => $data['form_type'],
            'privacy_link' => $privacy_link,
            'terms_link' => $terms_link,
            'forward_to' => $forward_to,
            'success_message' => $this->setMultiLanguageObject($data["content_lang"], $successMessage, $data['success_message'], $toJson = false),
            'fieldsets' => $fieldsets,
            'deleted_fields' => $deleted_fields,
        );

        $tagModel = new TagModel($this->serviceLocator);
        $type = $data['type'];

        foreach ($fieldsets as $fields) {
            foreach ($fields->fields as $field) {
                if ($field->type == "TAG_GROUP") {
//                    $tagModel->parseTagList(explode(",", $field->value), $type);
                    $tagModel->parseTagListObject($field->value, $type . self::CUSTOM_FORM_TYPE_SUFFIX);
                }
            }
        }

        $layout = $this->parseLayout($data);

        $fields = array();

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }

        $tags = $this->parseTag($data);

        $res = $this->updateSection($id, $data, $layout, $params, $tags);

        $this->updateCustomFormTable($id, $params);

        return $res;
    }

    public function createCustomFormGroup($label)
    {
        $result = false;
        $label = ucfirst(strtolower($label));

        $stmt = $this->dbApp->prepare(self::$sqlSectionGroupCreate);

        $stmt->bindParam(":label", $label, \PDO::PARAM_STR);

        if ($stmt->execute()) {
            $result = $this->getSectionGroupList();
        }

        return $result;
    }

    public function getCustomFormValidatorsByType($type)
    {
        $config = $this->serviceLocator->get('config');

        $result = array();
        foreach ($config['custom_form_validators'] as $cftv) {
            if (in_array($type, $cftv["types"])) {
                $result [] = array(
                    'selected' => $cftv["selected"],
                    'value' => $cftv["value"],
                    'label' => $cftv["label"],
                );
            }
        }

        return $result;
    }

    public function getSectionGroupList()
    {
        $stmt = $this->dbApp->prepare(self::$sqlGetSectionGroupList);

        $stmt->execute();
        $groups = $stmt->fetchAll(\PDO::FETCH_OBJ);

        $res = array();
        foreach ($groups as $g) {
            $res [] = array('value' => $g->id, 'label' => $g->label);
        }

        return $res;
    }

    public function getActualSectionGroupList($existingFieldsets)
    {
        $usedGroups = '""';
        if ($existingFieldsets) {
            $usedGroups = '"' . implode('","', $existingFieldsets) . '"';
        }

        $stmt = $this->dbApp->prepare(sprintf(self::$sqlGetActualSectionGroupList, $usedGroups));

        $stmt->execute();
        $groups = $stmt->fetchAll(\PDO::FETCH_OBJ);

        $res = array();
        foreach ($groups as $g) {
            $res [] = array('value' => $g->id, 'label' => $g->label);
        }

        return $res;
    }

    public function getSectionLayoutsAllover($template_id, $old_layout, $new_layout)
    {

        $stmt = $this->dbApp->prepare(self::$sqlAppSectionsList);

        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_OBJ);
        $hpModel = new HpModel($this->serviceLocator);

        $differentLayoutSections = array();
        foreach ($result as $res) {
            $section_layout = json_decode($res->layout);
            $sectionCheck = $hpModel->checkLayoutEquality($section_layout, $old_layout);

            if ($sectionCheck) {
                $section_layout->theme = $new_layout;
                self::updateSectionTheme($template_id, $res->id, json_encode($section_layout));
            } else {
                $differentLayoutSections[] = $res;
            }
        }

        return $differentLayoutSections;
    }

    public function setSectionLayoutsAllover($template_id, $new_layout, $sections_id)
    {
        $res = false;
        foreach ($sections_id as $sec_id) {
            $stmt = $this->dbApp->prepare(self::$sqlGetSectionLayout);
            $stmt->bindParam(":id", $sec_id, \PDO::PARAM_INT);

            $stmt->execute();

            $result = $stmt->fetchAll(\PDO::FETCH_OBJ);

            $section_layout = json_decode($result[0]->layout);

            $section_layout->theme = $new_layout;

            $res = $this->updateSectionTheme($template_id, $sec_id, json_encode($section_layout));
        }

        return $res;
    }

    private function updateSectionTheme($template_id, $id, $layout)
    {
        $stmt = $this->dbApp->prepare(self::$sqlSetSectionLayout);

        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
        $stmt->bindParam(":temp_id", $template_id, \PDO::PARAM_INT);
        $stmt->bindParam(":layout", $layout, \PDO::PARAM_STR);

        return $stmt->execute();
    }

    public function getCustomFormList($config)
    {
        $res = null;

        if (!is_null($this->dbApp)) {
            $stmt = $this->dbApp->prepare(self::$sqlGetCustomFormList);
            $type = "CUSTOM_FORM";

            $stmt->bindParam(":type", $type, \PDO::PARAM_INT);

            if ($stmt->execute()) {
                $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

                if (empty($rows)) {
                    $res [] = $config["menu"]["new_custom_form"];
                } else {
                    foreach ($rows as $row) {
                        $jsonSubMenu = $this->createFormJsonSubMenu($row);
                        $res [] = $jsonSubMenu;
                    }
                }
            }
        }

        return $res;
    }

    public function getCustomFormSelectList($customFormType = CustomFormSectionForm::USER_FORM)
    {
        $customForms = SectionQuery::create()
            ->filterByType(CustomFormSectionForm::CUSTOM_FORM)
            ->filterByStatus(self::SECTION_STATUS_PUBLISHED)
            ->find();

        $res[] = array(
            'value' => null,
            'label' => 'Select Custom form'
        );
        foreach ($customForms as $customForm) {
            $params = json_decode($customForm->getParams());
            if ($params !== null && $params->form_type === $customFormType) {
                $title = $customForm->getTitle();
                $titleObj = json_decode($title);
                if ($titleObj !== null) {
                    $application = $this->aclHelper->getApp();
                    $title = $titleObj->{$application->main_contents_language};
                }
                $res [] = array(
                    'value' => $customForm->getId(),
                    'label' => $title
                );
            }
        }

        return $res;
    }

    private function createFormJsonSubMenu($row)
    {
        $lang = $this->aclHelper->session->currentLang;
        $formType = json_decode($row->params)->form_type;

        $jsonSubMenu = array();
        $jsonSubMenu['id'] = "section_" . $row->id;
        $jsonSubMenu['type'] = "item";
        $jsonSubMenu['icon_class'] = $this->getFormIcon($formType, $row->status);
        $jsonSubMenu['title'] = $this->getLocaleText($lang, $row->title);
        $jsonSubMenu['active'] = false;
        $jsonSubMenu['target'] = "/admin/form/" . $row->id;

        return $jsonSubMenu;
    }

    private function getFormIcon($formType, $status)
    {
        $res = null;
        switch ($formType) {
            case \Form\Section\CustomForm\CustomFormSectionForm::CONTACT_FORM:
                $res = "fa-link";
                break;
            case \Form\Section\CustomForm\CustomFormSectionForm::REGISTRATION_FORM:
                if ($status == 0) {
                    $res = "fa-registered";
                } else {
                    $res = "fa-ban";
                }
                break;
            case \Form\Section\CustomForm\CustomFormSectionForm::SURVEY_FORM:
                $res = "fa-eye";
                break;
            default:
                $res = "fa-deaf";
        }

        return $res;
    }

    public function getCustomFormDataDetails($id)
    {
        $sectionTitle = null;
        $res = array();

        $section = SectionQuery::create()
            ->findOneById($id);

        if ($section) {
            $lang = $this->aclHelper->session->currentLang;
            $sectionTitle = $this->getLocaleText($lang, $section->getTitle());
            $params = json_decode($section->getParams());

            $res[] = array("label" => "last modified", "icon" => "fa fa-clock-o", "class" => "clickable");

            if ($params->form_type == CustomFormSectionForm::REGISTRATION_FORM) {
                $res[] = array("label" => "username", "icon" => "fa fa-user", "class" => "clickable");
                $res[] = array("label" => "registered at", "icon" => "fa fa-clock-o", "class" => "clickable");
            }

            $sectionFieldSets = $params->fieldsets;

            foreach ($sectionFieldSets as $fieldSets) {
                $sectionFields = $fieldSets->fields;
                $clickableClassFields = array(
                    ApplicationSectionModel::HPFORM_CHECKBOX,
                    ApplicationSectionModel::HPFORM_INPUT_TEXT,
                    ApplicationSectionModel::HPFORM_RADIOBOX,
                    ApplicationSectionModel::HPFORM_SELECT,
                    ApplicationSectionModel::HPFORM_TAG_GROUP,
                    ApplicationSectionModel::HPFORM_TEXTAREA
                );

                foreach ($sectionFields as $fields) {
                    if ($fields->type !== ApplicationSectionModel::HPFORM_INPUT_DESCR) {
                        $res [] = array(
                            "label" => is_string($fields->label) ? $fields->label : $fields->name,
                            "icon" => $this->getCustomFormIcon($fields),
                            "class" => in_array($fields->type, $clickableClassFields) ? "clickable" : ""
                        );
                    }
                }
            }
        }

        return array("title" => $sectionTitle, "details" => $res);
    }

    private function getCustomFormIcon(\stdClass $fields = null)
    {
        $field_icon = "glyphicon glyphicon-bishop";

        if ($fields !== null) {
            $type = $fields->type;
            $validator = $fields->validator;

            switch ($type) {
                case ApplicationSectionModel::HPFORM_INPUT_DESCR:
                    $field_icon = 'fa fa-file-text-o';
                    break;
                case ApplicationSectionModel::HPFORM_INPUT_TEXT:
                    switch ($validator) {
                        case "NONE":
                            $field_icon = 'fa fa-text-width';
                            break;
                        case "VALID_PHONE":
                            $field_icon = 'fa fa-phone';
                            break;
                        case "VALID_EMAIL":
                            $field_icon = 'fa fa-envelope-o';
                            break;
                        case "VALID_URL":
                            $field_icon = 'fa fa-link';
                            break;
                        case "NUMERIC":
                            $field_icon = 'fa fa-calculator';
                            break;
                    }
                    break;
                case ApplicationSectionModel::HPFORM_TEXTAREA:
                    $field_icon = 'fa fa-align-justify';
                    break;
                case ApplicationSectionModel::HPFORM_IMAGE:
                    if ($fields->is_avatar === true) {
                        $field_icon = 'fa fa-user-circle-o';
                    } else {
                        switch ($validator) {
                            case "SINGLE":
                                $field_icon = 'fa fa-picture-o';
                                break;
                            case "GALLERY":
                                $field_icon = 'fa fa-window-restore';
                                break;
                        }
                    }
                    break;
                case ApplicationSectionModel::HPFORM_CHECKBOX:
                    $field_icon = 'fa fa-check-square-o';
                    break;
                case ApplicationSectionModel::HPFORM_RADIOBOX:
                    $field_icon = 'fa fa-check-circle-o';
                    break;
                case ApplicationSectionModel::HPFORM_ADDRESS:
                    $field_icon = 'fa fa-map-marker';
                    break;
                case ApplicationSectionModel::HPFORM_TAG_GROUP:
                    switch ($validator) {
                        case "TAG_SINGLE":
                            $field_icon = 'fa fa-tag';
                            break;
                        case "TAG_MULTIPLE":
                            $field_icon = 'fa fa-tags';
                            break;
                    }
                    break;
                case ApplicationSectionModel::HPFORM_SELECT:
                    switch ($fields->value->origin_data) {
                        case CustomFieldForm::HP_FORM_SELECT_TYPE_LOCALE:
                            $field_icon = 'fa fa-bars';
                            break;
                        case CustomFieldForm::HP_FORM_SELECT_TYPE_REMOTE:
                            $field_icon = 'fa fa-list';
                            break;
                        default:
                            $field_icon = 'fa fa-bars';
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        return $field_icon;
    }

    public function getFormFields($formId, $rowId) {
        $res = array('fieldset'=>null,'data'=>null);

        $res['fieldset'] = $this->getSectionParamsById($formId)->fieldsets;
        $res['data'] = $this->getFormUserDataById($formId, $rowId);

        //var_dump($res['fieldset'][0]->fields);
        return $res;
    }

    public function getFormUserDataById($formId, $rowId) {
        $data = array();
        $sql = sprintf(self::$sqlFormUserDataById, $formId);

        $stmt = $this->dbApp->prepare($sql);
        $stmt->bindParam(":row_id", $rowId, \PDO::PARAM_INT);
        $stmt->execute();

        $rows = (array) $stmt->fetchObject();
        $cols = array_diff(array_keys($rows), ['id', 'user_id', 'cover_id', 'device_id', 'row_data', 'created_at', 'updated_at']);

        foreach ($cols as $col) {
            $data[$col] = $rows[$col];
        }
        //var_dump($cols, $data, $rows);

        return $data;
    }

    public function setFormUserDataById($formId, $rowId, $data) {
        $sql = "UPDATE `custom_form_%s` SET %s WHERE `id` = :row_id ";
        //`bG9uZ190ZXh0` = 'lorem ipsum xxx...'

        $parsedSqlSet = array();
        $parsedData = array();
        $i = 0;
        foreach ($data as $key => $value) {
            if($key !== "save") {
                //$bindKey = ":bind_$i";
                $value = addslashes( (is_array($value) ? implode(",",$value) : $value) );
                $parsedSqlSet[]= "`".$key."` = '$value'";
                $i++;
            }
        }
        $sql = sprintf($sql, $formId, implode(", ",$parsedSqlSet));

        $stmt = $this->dbApp->prepare($sql);
        $stmt->bindParam(":row_id", $rowId, \PDO::PARAM_INT);

        /*foreach ($parsedSqlSet as $key => $value) {
            $data = is_array($parsedData[$key]) ? implode(",",$parsedData[$key]) : $parsedData[$key];
            $stmt->bindParam($key, $data,\PDO::PARAM_STR);
            echo "$key -> $data <br/>";
        }*/
        //echo "<br/>SQL: ".$sql;
        return $stmt->execute();
    }

    public function getFormUsersData($formId, $draw, $start, $length, $cols, $order, $search)
    {
        $res = array('draw' => $draw, 'data' => array());

        if (!is_null($this->dbApp)) {

            $stmt1 = $this->dbApp->prepare(self::$sqlSectionDetail);

            $stmt1->bindParam(":id", $formId, \PDO::PARAM_INT);

            if ($stmt1->execute()) {
                $sectionContent = $stmt1->fetchAll(\PDO::FETCH_OBJ);

                $sectionFieldsets = json_decode($sectionContent[0]->params)->fieldsets;

                $orderBy = "ORDER BY ";

                foreach ($order as $ord) {
                    $id = $ord['column'];
                    $dir = $ord['dir'];
                    $field = $cols[$id]['name'];

                    $orderBy .= "`" . $field . "` " . $dir;
                }

                $filter = "%";
                if ($search['value'] != "") $filter = "%" . $search['value'] . "%";

                $sql = sprintf(self::$sqlFormUsersData, $formId, $orderBy);

                $stmt2 = $this->dbApp->prepare($sql);

                $stmt2->bindParam(":start_row", $start, \PDO::PARAM_INT);
                $stmt2->bindParam(":length", $length, \PDO::PARAM_INT);
                $stmt2->bindParam(":filter", $filter, \PDO::PARAM_STR);

                $stmt2->execute();

                $rows = $stmt2->fetchAll(\PDO::FETCH_OBJ);

                $rowsData = array();

                foreach ($rows as $row) {
                    $rowData = array(
                        'DT_RowAttr' => array(
                            'data-row-id' => $row->id,
                        ),
                    );

                    foreach ($cols as $col) {
                        $rowData [] = $this->manipulateRowData($formId, $sectionFieldsets, $col, $row);
                    }

                    $rowsData [] = $rowData;
                }

                $res['data'] = $rowsData;

                $totalRows = $this->getFoundRows();

                $res['recordsFiltered'] = $totalRows;
                $res['recordsTotal'] = $totalRows;
            }
        }

        return $res;
    }

    public function manipulateRowData($formId, $sectionFieldsets, $col, $row)
    {
        $colData = " - ";

        if (
            $col['name'] == "user_id" ||
            $col['name'] == "username" ||
            $col['name'] == "last modified" ||
            $col['name'] == "registered at"
        ) {
            if ($row->$col['name'] != null) $colData = $row->$col['name'];
        } else {
            foreach ($sectionFieldsets as $sectionFieldset) {
                foreach ($sectionFieldset->fields as $sectionField) {
                    $fieldName = isset($sectionField->name) ? $sectionField->name : $sectionField->label;

                    if (base64_encode($fieldName) == $col['name']) {
                        switch ($sectionField->type) {
                            case "INPUT_TEXT":
                                $colData = $this->inputTextRowDataOutput($sectionField->validator, $row->$col['name']);
                                break;
                            case "TEXTAREA":
                                $colData = $this->textAreaRowDataOutput($row->$col['name'], base64_decode($col['name']));
                                break;
                            case "IMAGE":
                                $colData = $this->imageRowDataOutput($formId, $row->form_user_id, $col['name'], $sectionField->validator, $row->$col['name']);
                                break;
                            case "CHECKBOX":
//                                $colData = $this->boxesRowDataOutput($row->$col['name']);
                                $colData = $this->optionGroupRowDataOutput($row->$col['name']);
                                break;
                            case "RADIOBOX":
//                                $colData = $this->boxesRowDataOutput($row->$col['name']);
                                $colData = $this->optionGroupRowDataOutput($row->$col['name']);
                                break;
                            case "ADDRESS":
                                $colData = $this->addressRowDataOutput($row->$col['name']);
                                break;
                            case "TAG_GROUP":
                                $colData = $this->tagGroupRowDataOutput($sectionField->validator, $row->$col['name']);
                                break;
                            default:
                                if ($row->$col['name'] != null) $colData = $row->$col['name'];
                        }
                    }
                }
            }
        }

        return $colData;
    }

    private function inputTextRowDataOutput($validator, $value)
    {
        $res = " - ";

        switch ($validator) {
            case "NONE":
            case "VALID_EMAIL":
            case "VALID_PHONE":
            case "NUMERIC":
                if ($value != null) $res = $value;
                break;
            case "VALID_URL":
                if ($value != null) $res = "<a href='$value' target='_blank'>$value</a>";
                break;
            default:
                if ($value != null) $res = $value;
                break;
        }

        return $res;
    }

    private function textAreaRowDataOutput($value, $headerValue)
    {
        $res = " - ";

        if ($value != null) {
            $preview = $value;
            if (strlen($value) > 13) {
                $preview = trim(substr($value, 0, 10)) . "...";
            }
            $id = md5(rand() . $value);

            $res = '<div class="btn" data-toggle="modal" data-target="#cf-longtext-modal-' . $id . '">' . $preview . '</div>';
            $res .= '<!-- Modal --><div id="cf-longtext-modal-' . $id . '" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">' . $headerValue . '</h4>
      </div>
      <div class="modal-body">
        <p>' . nl2br($value) . '</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>';

        }

        return $res;
    }

    private function imageRowDataOutput($formId, $rowId, $colName, $validator, $value)
    {
        $res = " - ";
        $mediaModel = new MediaModel($this->serviceLocator);
        $user_id = (new AclHelper($this->serviceLocator))->getUser()->id;

        switch ($validator) {
            case "SINGLE":
                if ($value != null) {
                    $image = $mediaModel->getMediaById($value);
                    $res = " - ";
                    if(isset($image->uri)) {
                        $url = $this->getCdnBaseurl() . \Common\Helper\ImageHelper::getFormThumbUrl($formId) . $image->uri;
                        $detailedUrl = $this->getCdnBaseurl() . \Common\Helper\ImageHelper::getFormHighUrl($formId) . $image->uri;
                        $res = '<div class="form_user_image btn" detailed-url="' . $detailedUrl . '" style="width: 50px; height: 50px; background: url(' . $url . '); background-size: 50px 50px; background-repeat: no-repeat; margin-bottom: 0;"></div>';
                    }

                }
                break;
            case "GALLERY":
                $gallery = $mediaModel->getFormUserMediaGallery($formId, $rowId, $colName);
                if ($gallery) {
                    $galleryUrls = array();
                    foreach ($gallery as $image) {
                        $galleryUrls [] = $this->getCdnBaseurl() . \Common\Helper\ImageHelper::getFormHighUrl($formId) . $image->uri;
                    }

                    $res = "<div class='form_user_gallery btn btn-default btn-flat' gallery-urls='" . json_encode($galleryUrls) . "' style='width: 50px; margin-bottom: 0;'><i class='fa fa-picture-o' aria-hidden='true'></i></div>";
                }
                break;
            default:
                if ($value != null) $res = " - ";
        }

        return $res;
    }

    private function boxesRowDataOutput($value)
    {
        $res = '<i class="fa fa-times fa-2x" aria-hidden="true"></i>';

        if ($value) $res = '<i class="fa fa-check fa-2x" aria-hidden="true"></i>';

        return $res;
    }

    private function addressRowDataOutput($value)
    {
        $res = " - ";

        if ($value != null) {
            $stmt = $this->dbApp->prepare(self::$sqlFormUsersAddress);

            $stmt->bindParam(":id", $value, \PDO::PARAM_INT);

            if ($stmt->execute()) {
                $address = $stmt->fetchAll(\PDO::FETCH_OBJ);
                if ($address) {
                    $userAddressValue = $address[0]->address . " " . $address[0]->zipcode . " " . $address[0]->city . " " . $address[0]->country . " " . $address[0]->state;
                    $userAddressLabel = $address[0]->address . "<br/>" . $address[0]->zipcode . " " . $address[0]->city;
                    $res = '<div class="form_user_address btn" user-address="' . $userAddressValue . '" style="margin-bottom: 0;">' . $userAddressLabel . '</div>';
                }
            }
        }

        return $res;
    }

    private function tagGroupRowDataOutput($validator, $value)
    {
        $res = " - ";

        switch ($validator) {
            case "TAG_SINGLE":
                if ($value != null) $res = '<div><span class="label label-primary">' . $value . '</span></div>';
                break;
            case "TAG_MULTIPLE":
                if ($value != null) {
                    $res = "";
                    $tagsValue = explode(",", $value);
                    foreach ($tagsValue as $tagValue) {
                        $res .= '<div><span class="label label-primary">' . trim($tagValue) . '</span></div>';
                    }
                }
                break;
            default:
                if ($value != null) $res = $value;
        }

        return $res;
    }

    private function optionGroupRowDataOutput($value)
    {
        $res = " - ";

        if ($value != null) {
            $res = "";
            $tagsValue = explode(",", $value);
            foreach ($tagsValue as $tagValue) {
                $res .= '<div><span class="label label-primary">' . trim($tagValue) . '</span></div>';
            }
        }

        return $res;
    }

    public function checkSectionFieldContent($fieldLabel, $customFormId)
    {
        $res = false;
        try {
            $stmt = $this->dbApp->prepare(sprintf(self::$sqlCheckFieldContent, $fieldLabel, $customFormId));

            if ($stmt->execute()) {
                $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

                foreach ($rows as $row) {
                    foreach ($row as $k => $v) {
                        if ($v != null) {
                            $res = true;
                        }
                    }
                }

                if (!$res) {
                    $mediaModel = new MediaModel($this->serviceLocator);
                    $res = $mediaModel->checkSectionFieldGalleryContent($fieldLabel, $customFormId);
                }
            }
        } catch (\PDOException $exception) {
            $res = false;
        }

        return $res;
    }

    public function publishedSectionsList()
    {
        $res = array();

        $stmt = $this->dbApp->prepare(self::$sqlGetPublishedSections);

        if ($stmt->execute()) {
            $list = $stmt->fetchAll(\PDO::FETCH_OBJ);
            foreach ($list as $l) {
                $res[$l->id] = $l->title;
            }
        }

        return $res;
    }

    public function getRemoteSections()
    {
        $config = $this->serviceLocator->get('config');
        $validRemoteSections = $config['remote_sections'];

        $sections = SectionQuery::create()
            ->filterByType($validRemoteSections)
            ->filterByStatus(0)
            ->find();

        $remoteSections = array();
        foreach ($sections as $section) {
            $remoteSections[] = array('value' => $section->getId(), 'label' => $section->getTitle());
        }

        return $remoteSections;
    }

    /**
     * @param $data
     * @return array
     */
    private function getResourceQueryString($data)
    {
        $fields = array();

        if( isset($data['order']) && !is_null($data['order']) ) {
            $fields['order'] = $data['order'];
        }

        if ($data['connector'] !== LayoutForm::CONNECTOR_TYPE_HUB) {
            $fields['type'] = strtolower($data['type']);
        }

        $tags = $this->parseTag($data);
        if (count($tags)) {
            $tagsQueryString = array();
            foreach ($tags as $tag) {
                if ($data['connector'] !== LayoutForm::CONNECTOR_TYPE_HUB) {
                    $tagsQueryString[] = $this->slugify($tag["value"]);
                } else {
                    $tagsQueryString[] = $tag["key"];
                }
            }
            $fields['tags'] = implode(self::RESOURCE_PARAMS_SEPARATOR, $tagsQueryString);
        }

        switch ($data['type']) {
            case SectionForm::EVENT:
                // Event params
                if (isset($data['start-date']) && $data['start-date'] !== "") $fields['from'] = strtotime($data['start-date']);
                if (isset($data['start-current-date']) && $data['start-current-date']) $fields['from_current'] = $data['start-current-date'];
                if (isset($data['end-date']) && $data['end-date'] !== "") $fields['to'] = strtotime($data['end-date']);
                if (isset($data['end-current-date']) && $data['end-current-date']) $fields['to_current'] = $data['end-current-date'];
                break;
            case SectionForm::POI:
                list($data, $params, $layout, $tags) = $this->getPoiSectionData($data);
                $fields['scan'] = $params['scan'];
                $fields['mode'] = $params['mode'];
                break;
            case SectionForm::PERSON:
            case SectionForm::GENERIC:
            case SectionForm::CUSTOM_FORM:
            case SectionForm::WEB:
            case SectionForm::SECTION:
                break;
        }

        return $fields;
    }

    private function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    /**
     * @param $data
     * @return array
     */
    private function getPoiSectionData($data)
    {
        $data["resource"] = self::RESOURCE_POI_LIST;
        $scan = boolval($data['scan']);
        $region = "";

        $major = $data['major'] === "" ? null : (int)$data['major'];
        $minor = $data['minor'] === "" ? null : (int)$data['minor'];
        $zoom = $data['zoom_map'] === "" ? null : (int)$data['zoom_map'];

        switch ($data['region']) {
            case "circular_region":
                $region = array("identifier" => $data['address'], "type" => $data['region'], "latitude" => (double)$data['latitude'], "longitude" => (double)$data['longitude'], "zoom" => $zoom);
                break;
            case "beacon_region":
                $region = array("identifier" => $data['subtitle'], "type" => $data['region'], "uuid" => $data['uuid'], "major" => $major, "minor" => $minor);
                break;
        }

        $params = array("scan" => $scan, "mode" => $data['mode'], "region" => $region);
        $layout = $this->parseLayout($data);
        $tags = $this->parseTag($data);

        $fields = array();
        if (count($tags)) {
            $ids = array();
            foreach ($tags as $tag) {
                $ids [] = $tag["key"];
            }
            $fields['tags'] = implode(self::RESOURCE_PARAMS_SEPARATOR, $ids);
        }

        $fields['mode'] = $data['mode'];
        if (isset($scan)) $fields['scan'] = $scan;

        if (count($fields)) {
            $data["resource"] .= "?" . http_build_query($fields, '', "&");
        }
        return array($data, $params, $layout, $tags);
    }
}