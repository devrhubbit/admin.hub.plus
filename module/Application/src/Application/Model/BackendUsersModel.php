<?php

namespace Application\Model;

use Common\Helper\ImageHelper;
use Common\Helper\SecurityHelper;
use Database\HubPlus\UserBackend;
use Database\HubPlus\UserBackendQuery;
use Propel\Runtime\Exception\PropelException;
use MessagesService\Exception\MessagesServiceMailException;
use MessagesService\Model\MessagesModel,
    MessagesService\Model\MessageModel as Message,
    MessagesService\Service\Factory as MessagesFactory;

class BackendUsersModel extends HpModel
{

    const SUPER_ADMIN_ROLE_ID = 1;
    const ADMIN_ROLE_ID = 2;
    const USER = 3;

    private static $sqlGetBackendUsers = "SELECT SQL_CALC_FOUND_ROWS *
                                        FROM `user_backend` WHERE `deleted_at` IS NULL AND
                                        ((firstname LIKE :fn) OR (lastname LIKE :ln) OR (email LIKE :em))
                                        %s
                                        LIMIT :start, :length;";

    private static $sqlChangeBackendUserRole = "UPDATE `user_backend` SET role_id = :role, updated_at = NOW() WHERE `id` = :id";

    private static $sqlAuthenticate = "SELECT `user_backend`.id, imagePath, firstname, lastname, email, locked, expired, privacy, terms, role.id AS role_id, role.name AS role, role.icon
                                       FROM `user_backend` JOIN role ON `user_backend`.role_id = role.id
                                       WHERE email=:email AND password=:password AND deleted_at IS NULL;";

    private static $sqlLastLogin = "UPDATE `user_backend` SET last_login=NOW(), updated_at=NOW() WHERE id=:id ;";

    private static $sqlAuthenticateById = "SELECT `user_backend`.id, imagePath, firstname, lastname, email, role.id AS role_id, role.name AS role, role.icon
                                           FROM `user_backend` JOIN role ON `user_backend`.role_id = role.id
                                           WHERE locked=0 AND expired=0 AND privacy=1 AND terms=1 AND `user_backend`.id = :id AND deleted_at IS NULL;";

    private static $sqlUpdProfile = "UPDATE `user_backend` SET `firstname`=:firstname, `lastname`=:lastname, `email`=:email,
                                      `imagePath`=:avatar, updated_at=NOW() WHERE id=:id ;";

    private static $sqlLockUser = "UPDATE `user_backend` SET `locked` = :locked, `locked_at` = NOW(), `updated_at` = NOW() WHERE `id` = :id;";

    private static $sqlUpdPassword = "UPDATE `user_backend` SET `password`=:password, updated_at=NOW() WHERE id=:id ;";

    private static $sqlUpdAvatar = "UPDATE `user_backend` SET `imagePath`=:avatar, updated_at=NOW() WHERE id=:id ;";

    private static $sqlCheckEmail = "SELECT `user_backend`.id, imagePath, firstname, lastname, email FROM `user_backend`
                                     WHERE locked=0 AND expired=0 AND privacy=1 AND terms=1 AND email=:email AND deleted_at IS NULL;";

    private static $sqlGetSuperAdminProfile = "SELECT SQL_CALC_FOUND_ROWS * FROM `user_backend` WHERE `role_id` = :role_id AND `deleted_at` IS NULL;";

    private static $sqlFoundRows = "SELECT FOUND_ROWS() as row_number;";

    public function getBackendUsers($draw, $start, $length, $cols, $order, $search)
    {
        $res = array('draw' => $draw, 'data' => array());

        if (!is_null($this->db)) {
            $orderBy = "ORDER BY ";

            foreach ($order as $ord) {
                $id = $ord['column'];
                $dir = $ord['dir'];
                $field = $cols[$id]['name'];

                $orderBy .= "`" . $field . "` " . $dir;
            }

            $sql = sprintf(self::$sqlGetBackendUsers, $orderBy);

            $stmt = $this->db->prepare($sql);

            $filter = "%";
            if ($search['value'] != "") $filter = "%" . $search['value'] . "%";

            $stmt->bindParam(":start", $start, \PDO::PARAM_INT);
            $stmt->bindParam(":length", $length, \PDO::PARAM_INT);
            $stmt->bindParam(":fn", $filter, \PDO::PARAM_STR);
            $stmt->bindParam(":ln", $filter, \PDO::PARAM_STR);
            $stmt->bindParam(":em", $filter, \PDO::PARAM_STR);

            $stmt->execute();

            $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

            $imageHelper = new ImageHelper();

            foreach ($rows as $row) {
                $imageShown = $row->imagePath;
                if ($imageShown != null) $imageShown = $imageHelper->getUserAvatarThumb($row->id) . $row->imagePath;

                $lockUnlockIcons = "fa-lock";
                $lockUnlockBackgroundIcons = "btn-default";
                if ($row->locked === 1) {
                    $lockUnlockIcons = "fa-unlock-alt";
                    $lockUnlockBackgroundIcons = "btn-warning";
                }

                $iconsActions =
                    '<div style="white-space: nowrap;">' .
                    '<button type="button" class="lock-unlock-backend-user btn ' . $lockUnlockBackgroundIcons . '" data-href="" data-toggle="modal" data-target="#confirm-backend-user-lock-unlock" data-backend-user-id="' . $row->id . '" data-backend-user-lock-value="' . $row->locked . '" data-backend-user-email="' . $row->email . '" style="margin-bottom: 0; margin-right: 5px;">
                            <i class="fa ' . $lockUnlockIcons . ' fa-2x" aria-hidden="true"></i>
                        </button>' .
                    '<button type="button" class="delete-backend-user btn btn-default" data-href="" data-toggle="modal" data-target="#confirm-backend-user-delete" data-backend-user-id="' . $row->id . '" data-backend-user-email="' . $row->email . '" style="margin-bottom: 0; margin-right: 5px;">
                            <i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>
                        </button>' .
                    '</div>';

                $res['data'][] = array(
                    'DT_RowAttr' => array(
                        'data-template-id' => $row->id,
                    ),
                    $row->id,
                    $imageShown,
                    $row->role_id,
                    $row->email,
                    ($row->firstname == "") ? "-" : $row->firstname,
                    ($row->lastname == "") ? "-" : $row->lastname,
                    $row->last_login,
                    $row->privacy_at,
                    $row->terms_at,
                    $iconsActions
                );
            }

            $totalRows = $this->getFoundRows();

            $res['recordsFiltered'] = $totalRows;
            $res['recordsTotal'] = $totalRows;
        }

        return $res;
    }

    public function getAdmins()
    {
        $res = false;

        $roleId = self::SUPER_ADMIN_ROLE_ID;

        $stmt = $this->db->prepare(self::$sqlGetSuperAdminProfile);
        $stmt->bindParam(":role_id", $roleId, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $res = $stmt->fetchAll(\PDO::FETCH_OBJ);
        }

        return $res;
    }

    public function changeBackendUserRole($id, $role)
    {
        $res = false;

        $stmt = $this->db->prepare(self::$sqlChangeBackendUserRole);
        $stmt->bindParam(":id", $id, \PDO::PARAM_INT);
        $stmt->bindParam(":role", $role, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $res = true;
        }

        return $res;
    }

    /**
     * @param $email
     * @param $password
     * @param $privacy
     * @param string $firstName
     * @param string $lastName
     * @param int $roleId
     * @return null|string
     */
    public function registerUser($email, $password, $privacy, $firstName = "", $lastName = "", $roleId = self::USER)
    {
        try {
            $userBackend = new UserBackend();
            $userBackend
                ->setFirstname($firstName)
                ->setLastname($lastName)
                ->setEmail($email)
                ->setPassword(md5($password))
                ->setRoleId($roleId)
                ->setPrivacy(($privacy == "yes"))
                ->setPrivacyAt(time())
                ->setTerms(($privacy == "yes"))
                ->setTermsAt(time());
            $userBackend->save();

            $res = $userBackend->getId();
        } catch (PropelException $exception) {
            $res = null;
        }

        return $res;
    }

    public function authenticateUser($email, $password)
    {
        $stmt = $this->db->prepare(self::$sqlAuthenticate);

        $stmt->bindValue(":email", $email);
        $stmt->bindValue(":password", md5($password));

        $res = null;

        if ($stmt->execute()) {
            $res = $stmt->fetchObject();
        }

        return $res;
    }

    public function validateLogin($userData)
    {
        $success = true;
        $message = "";

        if ($userData->locked !== 0) {
            $success = false;
            $message = "User inactive.";
        }

        if ($userData->expired !== 0) {
            $success = false;
            $message = "User expired.";
        }

        if ($userData->privacy !== 1) {
            $success = false;
            $message = "Privacy policy not accepted.";
        }

        if ($userData->terms !== 1) {
            $success = false;
            $message = "Terms policy not accepted.";
        }

        if ($success === true) {
            $stmt = $this->db->prepare(self::$sqlLastLogin);
            $stmt->bindValue(":id", $userData->id, \PDO::PARAM_INT);
            $stmt->execute();
        }

        $response = new \stdClass();
        $response->success = $success;
        $response->message = $message;

        return $response;
    }

    public function authenticateUserById($id)
    {
        $res = null;

        $stmt = $this->db->prepare(self::$sqlAuthenticateById);

        $stmt->bindValue(":id", $id);

        if ($stmt->execute()) {
            $res = $stmt->fetchObject();
        }

        return $res;
    }

    public function updateUser($id, $email, $password, $firstname = "", $lastname = "", $avatar = "")
    {
        $stmt1 = $this->db->prepare(self::$sqlUpdProfile);

        $stmt1->bindValue(":id", $id, \PDO::PARAM_INT);
        $stmt1->bindValue(":firstname", $firstname);
        $stmt1->bindValue(":lastname", $lastname);
        $stmt1->bindValue(":email", $email);
        $stmt1->bindValue(":avatar", $avatar);

        $res = $stmt1->execute();

        if ($res && $password != null && $password != "") {
            $stmt2 = $this->db->prepare(self::$sqlUpdPassword);

            $stmt2->bindValue(":id", $id, \PDO::PARAM_INT);
            $stmt2->bindValue(":password", md5($password));

            $res = $stmt2->execute();
        }

        return $res;
    }

    public function lockUnlockUser($id, $lock = 1)
    {
        $response = array(
            'success' => false,
            'message' => "An error occurred"
        );

        $stmt = $this->db->prepare(self::$sqlLockUser);

        $stmt->bindValue(":id", $id, \PDO::PARAM_INT);
        $stmt->bindValue(":locked", $lock, \PDO::PARAM_INT);

        if ($stmt->execute()) {
            $response['success'] = true;
            $response['message'] = "OK";
        }

        return $response;
    }

    public function deleteUser($id)
    {
        $response = array(
            'success' => false,
            'message' => "An error occurred"
        );

        $stmt = $this->db->prepare(self::$sqlAuthenticateById);
        $stmt->bindValue(":id", $id);

        if ($stmt->execute()) {
            $user = $stmt->fetchObject();

            // Check if deleting yourself
            if ($this->aclHelper->getUser()->id !== $user->id) {
                // Check if deleting user is Super Admin
                $totalSuperAdmin = 0;
                if ($user->role_id === self::SUPER_ADMIN_ROLE_ID) {
                    $stmt2 = $this->db->prepare(self::$sqlGetSuperAdminProfile);
                    $stmt2->bindValue(":role_id", self::SUPER_ADMIN_ROLE_ID, \PDO::PARAM_INT);
                    if ($stmt2->execute()) {
                        $totalSuperAdmin = $this->getFoundRows();
                    }
                }

                if ($user->role_id !== self::SUPER_ADMIN_ROLE_ID || ($user->role_id === self::SUPER_ADMIN_ROLE_ID && $totalSuperAdmin > 1)) {
                    $backendUser = UserBackendQuery::create()
                        ->findPk($id);

                    $backendUser->delete();
                    $response['success'] = true;
                    $response['message'] = "OK";
                } else {
                    $response['message'] = "You can't remove last Super Admin user";
                }
            } else {
                $response['message'] = "You can't remove yourself!";
            }
        }

        return $response;
    }

    public function updateAvatar($id, $filename)
    {
        $stmt = $this->db->prepare(self::$sqlUpdAvatar);

        $stmt->bindValue(":id", $id, \PDO::PARAM_INT);
        $stmt->bindValue(":avatar", $filename);

        return $stmt->execute();
    }

    public function isValidEmail($email)
    {
        $res = null;
        $stmt = $this->db->prepare(self::$sqlCheckEmail);

        $stmt->bindValue(":email", $email);

        if ($stmt->execute()) {
            $res = $stmt->fetchObject();
        }

        return $res;
    }

    public function newPasswordAndEmail($user_id, $user_email)
    {
        $config = $this->serviceLocator->get('config');
        $backendName = $config['backend_name'];

        $pwd = SecurityHelper::generateSalt(4);

        $userBackend = UserBackendQuery::create()
            ->findOneById($user_id);

        $userBackend
            ->setPassword(md5($pwd))
            ->setPasswordRequestedAt(time())
            ->save();

        $res = null;

        $message = new Message("...");
        $message->setOption('jsonParams', "params from application config");
        $message->setOption('from', $config['email_message']['sender.email']);
        $message->setOption('senderLabel', $backendName);
        $message->setOption('to', $user_email);
        $message->setOption('subject', $backendName . ": new password");
        $message->setOption('templateLayout', 'email/admin/password-recovery');
        $message->setOption('templateParams', json_encode(['password' => $pwd, 'token' => base64_encode($user_email . ":" . $pwd), 'backend_name' => $backendName]));

        $msgModel = new MessagesModel($this->serviceLocator, MessagesFactory::MODE_MAIL);
        try {
            $pullRes = $msgModel->pullMessage($message);
            if ($pullRes) {
                $res = 'New password has been sent to your email';
            } else {
                $res = 'An error occurred sending email with new password.';
            }
        } catch (MessagesServiceMailException $exception) {
            $res = sprintf('An error occurred. Exception: \n %s', $exception->getTraceAsString());
        }

        return $res;
    }

    public function getFoundRows()
    {
        $rows = 0;

        if (!is_null($this->db)) {
            $stmt = $this->db->prepare(self::$sqlFoundRows);
            $stmt->execute();

            $rows = $stmt->fetchObject()->row_number;
        }

        return $rows;
    }

    /**
     * @param $email
     * @param null $password
     * @param string $firstName
     * @param string $lastName
     * @param string $tpl_name
     * @param string $homePageUrl
     */
    public function sendRegistrationEmail($email, $password = null, $firstName = "", $lastName = "", $tpl_name = "email/admin/registered", $homePageUrl = "")
    {
        $config = $this->serviceLocator->get('config');

        $name = "";
        $backendName = $config['backend_name'];

        if (strlen($firstName . $lastName) > 0) {
            $name = $firstName . " " . $lastName;
        }

        $subject = $backendName . " | " . $email . " registered";
        if ($homePageUrl !== "") {
            $subject = $backendName . " | " . $homePageUrl . " registered";
        }

        $message = new Message("...");
        $message->setOption('jsonParams', "params from application config");
        $message->setOption('from', $config['email_message']['sender.email']);
        $message->setOption('senderLabel', $backendName);
        $message->setOption('to', $email);
        $message->setOption('subject', $subject);
        $message->setOption('templateLayout', $tpl_name);
        $message->setOption('templateParams', json_encode(['email' => $email, 'password' => $password, 'name' => $name, 'homePage' => $homePageUrl, 'backend_name' => $backendName]));

        $msgModel = new MessagesModel($this->serviceLocator, MessagesFactory::MODE_MAIL);
        $msgModel->pullMessage($message);
    }

    public function sendRegistrationEmailToAdmins($email, $firstName = "", $lastName = "")
    {
        $config = $this->serviceLocator->get('config');

        $name = "";
        $backendName = $config['backend_name'];

        if (strlen($firstName . $lastName) > 0) {
            $name = $firstName . " " . $lastName;
        }

        $msgModel = new MessagesModel($this->serviceLocator, MessagesFactory::MODE_MAIL);

        $message = new Message("...");
        $message->setOption('jsonParams', "params from application config");
        $message->setOption('from', $config['email_message']['sender.email']);
        $message->setOption('senderLabel', $backendName);
        $message->setOption('subject', $backendName . ": user registered");
        $message->setOption('templateLayout', 'email/admin/admin-registered');
        $message->setOption('templateParams', json_encode(['email' => $email, 'name' => $name, 'backend_name' => $backendName]));

        $admins = $this->getAdmins();
        if ($admins !== false) {
            foreach ($admins as $admin) {
                $message->setOption('to', $admin->email);
                $msgModel->pullMessage($message);
            }
        }

        $message->setOption('to', 'info@hub.plus');
        $msgModel->pullMessage($message);
    }

    public function sendNotificationLimitEmailToAdmins()
    {
        $appTitle = $this->aclHelper->getApp()->title;
        $appBundle = $this->aclHelper->getApp()->bundle;

        $config = $this->serviceLocator->get('config');
        $backendName = $config['backend_name'];

        $msgModel = new MessagesModel($this->serviceLocator, MessagesFactory::MODE_MAIL);

        $message = new Message(sprintf('<b>%s (%s)</b> do not have enough credit to send this notifications!', $appTitle, $appBundle));
        $message->setOption('jsonParams', "params from application config");
        $message->setOption('from', $config['email_message']['sender.email']);
        $message->setOption('senderLabel', $backendName);
        $message->setOption('subject', $backendName . ": Notification limit");
        $message->setOption('templateLayout', 'email/admin/simple');

        $admins = $this->getAdmins();
        if ($admins !== false) {
            foreach ($admins as $admin) {
                $message->setOption('to', $admin->email);
                $msgModel->pullMessage($message);
            }
        }
    }
}