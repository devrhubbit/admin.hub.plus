<?php

namespace Application\ViewHelper;

use Zend\View\Helper\AbstractHelper;

class HpRenderFormError extends AbstractHelper {

    const LEFT_PLACEMENT    = "left";
    const RIGHT_PLACEMENT   = "right";
    const TOP_PLACEMENT     = "top";
    const BOTTOM_PLACEMENT  = "bottom";

    public function __invoke(\Zend\Form\ElementInterface $formElement, $placement) {
        $res = "<ul class='form_errors'>";

        $errors = $formElement->getMessages();
        $fieldName = $formElement->getName();
        foreach( $errors as $error ) {
//            $res .= '<a href="#" data-related="'.$fieldName.'" class="hp_tooltip hp_error_tooltip_'.$fieldName.'" data-placement="'.$placement.'" title="'.$error.'"></a>';
            $res .= "<li>$error</li>";
        }
        $res .="</ul>";

        return $res;
    }
}