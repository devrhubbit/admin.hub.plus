<?php

return array(
    'router' => array(
        'routes' => array(
            'form-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/form/:id[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Form',
                        'action'     => 'formList',
                    ),
                ),
            ),
            'form-row-edit' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/form/:id/row/:row_id[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Form',
                        'action'     => 'formRowEdit',
                    ),
                ),
            ),
        ),
    ),
);