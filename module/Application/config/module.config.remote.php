<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 07/09/17
 * Time: 16:10
 */

return array(
    'router' => array(
        'routes' => array(
            'get-page-archive-remote-media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/remote/get-page/:media_id',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Remote',
                        'action'     => 'getRemoteMediaPage',
                    ),
                ),
            ),
            'get-page-remote-media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/remote/get-page-remote/:type/:remote_url',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Remote',
                        'action'     => 'getPageRemoteMediaPage',
                    ),
                ),
            ),
        ),
    ),
);