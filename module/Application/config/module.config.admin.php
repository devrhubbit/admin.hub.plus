<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'admin' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Admin',
                        'action'     => 'admin',
                    ),
                ),
            ),
            'profile' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/profile[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Admin',
                        'action'     => 'profile',
                    ),
                ),
            ),
            'register-app' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/app[/:id]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Admin',
                        'action'     => 'registerApp',
                    ),
                ),
            ),
            'archive' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/archive[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Admin',
                        'action'     => 'archive',
                    ),
                ),
            ),
            'provider' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/provider[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Admin',
                        'action'     => 'provider',
                    ),
                ),
            ),
            'templates' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/templates[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Admin',
                        'action'     => 'templates',
                    ),
                ),
            ),
        ),
    ),
);
