<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'app-users-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/user/[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\User',
                        'action'     => 'usersList',
                    ),
                ),
            ),
            'users-tag-restore' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/user/tags/restore[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\User',
                        'action'     => 'restore-tags',
                    ),
                ),
            ),
        ),
    ),
);
