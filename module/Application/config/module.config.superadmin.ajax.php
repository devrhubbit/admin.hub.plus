<?php

return array(
    'router' => array(
        'routes' => array(
            'backend-users-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/superadmin/backend-users/list[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\SuperadminData',
                        'action'     => 'backendUsersList',
                    ),
                ),
            ),
            'backend-user-change-role' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/superadmin/backend-user/change/role[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\SuperadminData',
                        'action'     => 'backendUserChangeRole',
                    ),
                ),
            ),
            'delete-backend-user' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/superadmin/backend-user/:id/delete[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\SuperadminData',
                        'action'     => 'deleteBackendUser',
                    ),
                ),
            ),
            'lock-unlock-backend-user' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/superadmin/backend-user/:id/lock-unlock/:lock[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\SuperadminData',
                        'action'     => 'lockUnlockBackendUser',
                    ),
                ),
            ),
        ),
    ),
);