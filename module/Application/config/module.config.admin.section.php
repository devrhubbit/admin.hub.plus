<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'sections' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/section[/:area][/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Section',
                        'action'     => 'section-list',
                    ),
                ),
            ),
            'section-create' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/section/:type/create[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Section',
                        'action'     => 'section-create',
                    ),
                ),
            ),
            'section-update' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/section/:type/:id/update[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Section',
                        'action'     => 'section-update',
                    ),
                ),
            ),
        ),
    ),
);
