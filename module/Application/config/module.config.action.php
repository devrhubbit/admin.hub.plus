<?php

return array(
    'router' => array(
        'routes' => array(
//            'action-list' => array(
//                'type' => 'Zend\Mvc\Router\Http\Segment',
//                'options' => array(
//                    'route'    => '/action/:id/list[/]',
//                    'defaults' => array(
//                        'controller' => 'Application\Controller\Action',
//                        'action'     => 'actionList',
//                    ),
//                ),
//            ),
            'attachment-modal' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/attachment/modal[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'attachmentModal',
                    ),
                ),
            ),
            'attachment-validator' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/attachment/validator[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'attachmentValidator',
                    ),
                ),
            ),
            'calendar-modal' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/calendar/modal[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'calendarModal',
                    ),
                ),
            ),
            'calendar-validator' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/calendar/validator[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'calendarValidator',
                    ),
                ),
            ),
            'content-modal' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/content/modal[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'contentModal',
                    ),
                ),
            ),
            'content-validator' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/content/validator[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'contentValidator',
                    ),
                ),
            ),
            'email-modal' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/email/modal[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'emailModal',
                    ),
                ),
            ),
            'email-validator' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/email/validator[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'emailValidator',
                    ),
                ),
            ),
            'link-modal' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/link/modal[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'linkModal',
                    ),
                ),
            ),
            'link-validator' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/link/validator[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'linkValidator',
                    ),
                ),
            ),
            'map-modal' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/map/modal[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'mapModal',
                    ),
                ),
            ),
            'map-validator' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/map/validator[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'mapValidator',
                    ),
                ),
            ),
            'phone-modal' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/phone/modal[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'phoneModal',
                    ),
                ),
            ),
            'phone-validator' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/phone/validator[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'phoneValidator',
                    ),
                ),
            ),
            'section-modal' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/section/modal[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'sectionModal',
                    ),
                ),
            ),
            'section-validator' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/action/section/validator[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Action',
                        'action'     => 'sectionValidator',
                    ),
                ),
            ),
        ),
    ),
);