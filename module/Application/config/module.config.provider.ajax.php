<?php

return array(
    'router' => array(
        'routes' => array(
            'get-provider' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/ajax/provider/:id[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Provider',
                        'action'     => 'getProvider',
                    ),
                ),
            ),
            'provider-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/ajax/provider/list[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Provider',
                        'action'     => 'providerList',
                    ),
                ),
            ),
            'provider-create' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/ajax/provider/create[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Provider',
                        'action'     => 'createProvider',
                    ),
                ),
            ),
            'provider-update' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/ajax/provider/update/:id[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Provider',
                        'action'     => 'updateProvider',
                    ),
                ),
            ),
            'provider-delete' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/ajax/provider/delete/:id[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Provider',
                        'action'     => 'deleteProvider',
                    ),
                ),
            ),
        ),
    ),
);