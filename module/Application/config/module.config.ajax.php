<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'set-lang' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/lang/:locale[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'setLanguage',
                    ),
                ),
            ),
            'template-carousel' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/template/:id/carousel[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'template-carousel',
                    ),
                ),
            ),
            'templates-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/templates/list[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action'     => 'templatesList',
                    ),
                ),
            ),
            'templates-details' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/superadmin/template/details[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action'     => 'templateDetails',
                    ),
                ),
            ),
            'template-delete' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/superadmin/template/delete[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action'     => 'templateDelete',
                    ),
                ),
            ),
            'delete-app' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/app/:id/delete[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'deleteApp',
                    ),
                ),
            ),
            'get-section-layouts-allover' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/app/get/section/layouts[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'getSectionLayoutsAllover',
                    ),
                ),
            ),
            'get-section-params' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/get-section-params/:sectionId[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'getSectionParams',
                    ),
                ),
            ),
            'set-section-layouts-allover' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/app/set/section/layouts[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'setSectionLayoutsAllover',
                    ),
                ),
            ),
            'get-content-layouts-allover' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/app/get/content/layouts[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'getContentLayoutsAllover',
                    ),
                ),
            ),
            'set-content-layouts-allover' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/app/set/content/layouts[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'setContentLayoutsAllover',
                    ),
                ),
            ),
            'tags-content' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/tags/content[/:type][/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'tagsContentList',
                    ),
                ),
            ),
            'tags-user' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/tags/user[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'tagsUserList',
                    ),
                ),
            ),
            'sections-weight' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/sections/weight[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'sectionsWeight',
                    ),
                ),
            ),
            'sections-custom-form-group' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/sections/custom/form/:group[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'sectionsCustomFormGroup',
                    ),
                ),
            ),
            'sections-custom-form-actual-group-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/sections/custom/form/actual/group/list[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'sectionsCustomFormActualGroupList',
                    ),
                ),
            ),
            'sections-custom-form-validators' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/sections/custom/form/validators/:type[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'sectionsCustomFormValidators',
                    ),
                ),
            ),
            'section-status' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/section/:id/status[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'sectionStatus',
                    ),
                ),
            ),
            'section-delete' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/section/:id/:type/delete[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'sectionDelete',
                    ),
                ),
            ),
            'check-section-field-content' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/check/section/field/content[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'checkSectionFieldContent',
                    ),
                ),
            ),
            'user-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/user/list[/:id][/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'userList',
                    ),
                ),
            ),
            'form-users-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/form/users/list/:id[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'formUsersList',
                    ),
                ),
            ),
            'content-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/content/list[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'contentList',
                    ),
                ),
            ),
            'content-status' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/content/:id/status[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'contentStatus',
                    ),
                ),
            ),
            'content-delete' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/content/:type/:id/delete[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'contentDelete',
                    ),
                ),
            ),
            'palette-list-dt' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/palette/list[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'paletteList',
                    ),
                ),
            ),
            'palette-theme' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/palette/:id/theme[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'paletteTheme',
                    ),
                ),
            ),
            'check-session-timeout' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/check-session-timeout[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'checkSessionTimeout',
                    ),
                ),
            ),
            'get-composite-image' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/image/get-composite-image[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'getCompositeImage',
                    ),
                ),
            ),
            'download-file-from-path' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/file/download[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'getFileFromPath',
                    ),
                ),
            ),
            'action-reorder' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/ajax/action/reorder[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Ajax',
                        'action' => 'actionsReorder',
                    ),
                ),
            ),
        ),
    ),
);
