<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
//            'error' => array(
//                'type' => 'Zend\Mvc\Router\Http\Segment',
//                'options' => array(
//                    'route'    => '/error[/]',
//                    'defaults' => array(
//                        'controller' => 'Application\Controller\Index',
//                        'action'     => 'error',
//                    ),
//                ),
//            ),
//        	'tracks' => array(
//        		'type' => 'Zend\Mvc\Router\Http\Literal',
//        			'options' => array(
//        				'route'    => '/tracks',
//        				'defaults' => array(
//        				'controller' => 'Application\Controller\Index',
//        				'action'     => 'tracks',
//        			),
//        		),
//        	),
//        	'rates' => array(
//        		'type' => 'Zend\Mvc\Router\Http\Literal',
//        			'options' => array(
//        				'route'    => '/rates',
//        				'defaults' => array(
//        				'controller' => 'Application\Controller\Index',
//        				'action'     => 'rates',
//        			),
//        		),
//        	),
//        	'stats' => array(
//        		'type' => 'Zend\Mvc\Router\Http\Literal',
//        			'options' => array(
//        				'route'    => '/stats',
//        				'defaults' => array(
//        				'controller' => 'Application\Controller\Index',
//        				'action'     => 'stats',
//        			),
//        		),
//        	),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Action'         => 'Application\Controller\ActionController',
            'Application\Controller\Admin'          => 'Application\Controller\AdminController',
            'Application\Controller\Ajax '          => 'Application\Controller\AjaxController',
            'Application\Controller\Analytics'      => 'Application\Controller\AnalyticsController',
            'Application\Controller\AnalyticsData'  => 'Application\Controller\AnalyticsDataController',
            'Application\Controller\Content'        => 'Application\Controller\ContentController',
            'Application\Controller\Error'          => 'Application\Controller\ErrorController',
            'Application\Controller\Form'           => 'Application\Controller\FormController',
            'Application\Controller\Index'          => 'Application\Controller\IndexController',
            'Application\Controller\Media '         => 'Application\Controller\MediaController',
            'Application\Controller\Notification'   => 'Application\Controller\NotificationController',
            'Application\Controller\Provider'       => 'Application\Controller\ProviderController',
            'Application\Controller\Section'        => 'Application\Controller\SectionController',
            'Application\Controller\Superadmin'     => 'Application\Controller\SuperadminController',
            'Application\Controller\SuperadminData' => 'Application\Controller\SuperadminDataController',
            'Application\Controller\Remote'         => 'Application\Controller\RemoteController',
            'Application\Controller\User'           => 'Application\Controller\UserController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'action/response'                   => __DIR__ . '/../view/application/action/response.phtml',
            'action/validate'                   => __DIR__ . '/../view/application/action/validate.phtml',
            'ajax/response'	    	            => __DIR__ . '/../view/application/ajax/response.phtml',
            'ajax/template-carousel'            => __DIR__ . '/../view/application/ajax/template-carousel.phtml',
            'attachment/archive'	    	    => __DIR__ . '/../view/application/attachment/archive.phtml',
            'attachment/response'	    	    => __DIR__ . '/../view/application/attachment/response.phtml',
            'remote/remote-media-page'	    	=> __DIR__ . '/../view/application/remote/remote-media-page.phtml',
            'layout/ajax'		    	        => __DIR__ . '/../view/layout/ajax.phtml',
            'layout/attachment'		    	    => __DIR__ . '/../view/layout/attachment.phtml',
            'layout/error'           		 	=> __DIR__ . '/../view/layout/error.phtml',
            'layout/layout'           		 	=> __DIR__ . '/../view/layout/layout.phtml',
            'layout/starter'           		 	=> __DIR__ . '/../view/layout/starter.phtml',
            'error/404'               		 	=> __DIR__ . '/../view/error/404.phtml',
            'error/index'             		 	=> __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
            __DIR__ . '/../view/partial',       //aggiungo un path in cui cercare le view per nome
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
