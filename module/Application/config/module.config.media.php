<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'modal-archive' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/archive/modal[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'modalArchive',
                    ),
                ),
            ),
            'info-archive-media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/archive/info[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'infoArchive',
                    ),
                ),
            ),
            'delete-archive-media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/archive/delete[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'deleteArchive',
                    ),
                ),
            ),
            'add-attachment-media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/attachment/add[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'addAttachmentMedia',
                    ),
                ),
            ),
            'add-archive-media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/archive/add[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'addArchive',
                    ),
                ),
            ),
            'check-archive-remote-media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/remote/check[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'checkRemoteMedia',
                    ),
                ),
            ),
            'add-archive-remote-media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/remote/add[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'uploadRemoteMedia',
                    ),
                ),
            ),
            'unlink-post-media' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/post/unlink[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'unlinkPost',
                    ),
                ),
            ),
            'avatar' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/avatar[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'avatar',
                    ),
                ),
            ),
            'app_icon' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/app/icon[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'appIcon',
                    ),
                ),
            ),
            'app_system_icon' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/app/system/icon/:system_type[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'appSystemIcon',
                    ),
                ),
            ),
            'app_logo' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/app/logo[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'appLogo',
                    ),
                ),
            ),
            'app_splash' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/app/splash[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'appSplash',
                    ),
                ),
            ),
            'pin_map' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/map/pin[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'pinMap',
                    ),
                ),
            ),
            'section_icon' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/section/icon[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'sectionIcon',
                    ),
                ),
            ),
            'section_user_add_icon' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/section/user/add-icon[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'userSectionAddIcon',
                    ),
                ),
            ),
            'custom_form_icon' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/custom_form/icon[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'customFormIcon',
                    ),
                ),
            ),
            'provider_icon' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/provider/icon[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'providerIcon',
                    ),
                ),
            ),
            'template_icon' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/template/icon[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'templateIcon',
                    ),
                ),
            ),
            'action_icon' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/media/action/icon[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Media',
                        'action'     => 'actionIcon',
                    ),
                ),
            ),
        ),
    ),
);
