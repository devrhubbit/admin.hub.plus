<?php

return array(
    'router' => array(
        'routes' => array(
            'customers' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/superadmin/backend-users[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Superadmin',
                        'action'     => 'backendUsers',
                    ),
                ),
            ),
            'app-settings' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/superadmin/app-settings[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Superadmin',
                        'action'     => 'appSettings',
                    ),
                ),
            ),
        ),
    ),
);