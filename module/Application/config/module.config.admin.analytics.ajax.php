<?php
return array(
    'router' => array(
        'routes' => array(
            'device-data' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/ajax/analytics/device/data[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\AnalyticsData',
                        'action'     => 'deviceData',
                    ),
                ),
            ),
            'posts-data' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/ajax/analytics/posts/data[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\AnalyticsData',
                        'action'     => 'postsData',
                    ),
                ),
            ),
        ),
    ),
);