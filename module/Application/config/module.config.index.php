<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'login' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/login/:token[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'logout' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/logout[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'logout',
                    ),
                ),
            ),
            'register-user' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/register/user[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'registerUser',
                    ),
                ),
            ),
            'register-user-thank-you' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/register-user-thank-you',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'registerUserThankYou',
                    ),
                ),
            ),
            'password-recovery' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/password-recovery[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'passwordRecovery',
                    ),
                ),
            ),
            'password-recovered' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route' => '/password-recovered/:message',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'passwordRecovered',
                    ),
                ),
            ),
        ),
    ),
);
