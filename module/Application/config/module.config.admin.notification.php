<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'notification-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/notification/list[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Notification',
                        'action'     => 'notification-list',
                    ),
                ),
            ),
            'ajax-notification-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/ajax/notification/list[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Notification',
                        'action'     => 'ajax-notification-list',
                    ),
                ),
            ),
            'notification-create' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/notification[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Notification',
                        'action'     => 'notification-create',
                    ),
                ),
            ),
            'notification-append' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/notification/append[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Notification',
                        'action'     => 'notification-append',
                    ),
                ),
            ),
        ),
    ),
);
