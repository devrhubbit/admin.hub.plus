<?php

return array(
    'router' => array(
        'routes' => array(
            'analytics-list' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/analytics[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Analytics',
                        'action'     => 'analyticsList',
                    ),
                ),
            ),
            'device-data-details' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/analytics/device/data/details[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Analytics',
                        'action'     => 'deviceDataDetails',
                    ),
                ),
            ),
            'posts-data-details' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/analytics/posts/data/details[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Analytics',
                        'action'     => 'postsDataDetails',
                    ),
                ),
            ),
        ),
    ),
);