<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'contents' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/content[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Content',
                        'action'     => 'content-list',
                    ),
                ),
            ),
            'content-create' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/content/:type[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Content',
                        'action'     => 'content-create',
                    ),
                ),
            ),
            'content-update' => array(
                'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
                    'route'    => '/admin/content/:type/:id[/]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Content',
                        'action'     => 'content-update',
                    ),
                ),
            ),
        ),
    ),
);
