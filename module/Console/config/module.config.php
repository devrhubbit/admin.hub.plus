<?php

return array(
    'console' => array(
        'router' => array(
            'routes' => array(
                'test' => array(
                    'options' => array(
                        // add [ and ] if optional ( ex : [<doname>] )
                        'route' => 'hp test [--debug|-d] <msg>',
                        'defaults' => array(
                            'controller' => 'Console\Controller\Console',
                            'action' => 'test',
                        ),
                    ),
                ),
                'machine-import' => array(
                    'options' => array(
                        'route' => 'hp import [--debug|-d] [--limit=] <email> <password>',
                        'defaults' => array(
                            'controller' => 'Console\Controller\Console',
                            'action' => 'import',
                        ),
                    ),
                ),
                'install-hub-plus-instance' => array(
                    'options' => array(
                        'route' => 'hp install [--debug|-d] <appName> <domain>',
                        'defaults' => array(
                            'controller' => 'Console\Controller\Console',
                            'action' => 'installHubPlusInstance',
                        ),
                    ),
                ),
            ),
        ),
    ),

    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_GB',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../languages',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Console\Controller\Console' => 'Console\Controller\ConsoleController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(),
        'template_path_stack' => array(
            __DIR__ . '/../view',
            __DIR__ . '/../view/partial',       //aggiungo un path in cui cercare le view per nome
        ),
    ),
);
