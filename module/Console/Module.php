<?php
namespace Console;

use Common\Helper\AclHelper;
use Zend\Console\Adapter\AdapterInterface;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Session\Config\SessionConfig;
use Zend\Session\SessionManager;
use Zend\Session\Container;

class Module implements ConsoleBannerProviderInterface, ConsoleUsageProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        
        $this->initSession(array(
        	'remember_me_seconds' 	=> 180,
        	'use_cookies' 			=> true,
        	'cookie_httponly' 		=> true,
        ));

        $serviceManager = $e->getApplication()->getServiceManager();
        //$this->initTranslator($e);

        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();

        $config = $serviceManager->get('config');
        $viewModel->cdnBaseurl = $config['cdn']['baseurl'];
        $viewModel->googleAnalytics = $config['google-analytics'];


//        if(!$config['debug']) {
//            //handle the dispatch error (exception)
//            $eventManager->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'handleDispatchError'), 100);
//
//            //handle the view render error (exception)
//            $eventManager->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER_ERROR, array($this, 'handleDispatchError'), 100);
//        }

    }

    public function getConfig()
    {
        $config = array();

        $configFiles = array(
            __DIR__ . '/config/module.config.php',                  // Module main config
        );

        // Merge all module config options
        foreach ($configFiles as $configFile) {
            $config = \Zend\Stdlib\ArrayUtils::merge($config, include $configFile);
        }

        return $config;
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function initSession($config)
    {
    	$sessionConfig = new SessionConfig();
    	$sessionConfig->setOptions($config);
    	$sessionManager = new SessionManager($sessionConfig);
    	$sessionManager->start();
    	Container::setDefaultManager($sessionManager);
    }

//    public function handleDispatchError(MvcEvent $e){
//        $action = null;
//        $error  = $e->getError();
//
//        switch($error){
//            case \Zend\Mvc\Application::ERROR_CONTROLLER_NOT_FOUND:
//                $action = "controllerNotFound";
//                break;
//            case \Zend\Mvc\Application::ERROR_CONTROLLER_INVALID:
//                $action = "controllerInvalid";
//                break;
//            case \Zend\Mvc\Application::ERROR_CONTROLLER_CANNOT_DISPATCH:
//                $action = "controllerCannotDispatch";
//                break;
//            case \Zend\Mvc\Application::ERROR_ROUTER_NO_MATCH:
//                $action = "routerNoMatch";
//                break;
//            case \Zend\Mvc\Application::ERROR_EXCEPTION:
//                $action = "exception";
//                break;
//            default:
//                $action = "generic";
//        }
//
//        // Here is where the "re-dispatch" happens, you can change the controller and action to your needs
//        $em = $e->getApplication()->getEventManager();
//        $routerMatch = new \Zend\Mvc\Router\RouteMatch(array('controller' => 'Application\Controller\Error', 'action' => $action, 'exception' => $e->getParam('exception')));
//
//        $errorEvent = clone $e;
//        $errorEvent->setRouteMatch($routerMatch);
//        $e->stopPropagation(true);
//        $em->trigger('dispatch', $errorEvent);
//    }

    public function initTranslator(MvcEvent $e) {
        $serviceManager = $e->getApplication()->getServiceManager();
        $aclHelper      = new AclHelper($serviceManager);

        $lang = ($aclHelper->getLang() != null) ? $aclHelper->getLang() : substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

        switch ($lang){
            case "it":
                $serviceManager->get('MvcTranslator')->setLocale('it_IT');
                $aclHelper->setLang("it");
                break;
            default:
                $aclHelper->setLang("en");
                break;
        }

        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        $viewModel->setVariable('lang', $aclHelper->getLang());
    }

    /**
     * Returns a string containing a banner text, that describes the module and/or the application.
     * The banner is shown in the console window, when the user supplies invalid command-line parameters or invokes
     * the application with no parameters.
     *
     * The method is called with active Zend\Console\Adapter\AdapterInterface that can be used to directly access Console and send
     * output.
     *
     * @param AdapterInterface $console
     * @return string|null
     */
    public function getConsoleBanner(AdapterInterface $console)
    {
        return
            "\n\n\n".
            "#################################\n".
            "#        HUB+ console 1.0       #  \n".
            "#################################";
    }

    /**
     * Returns an array or a string containing usage information for this module's Console commands.
     * The method is called with active Zend\Console\Adapter\AdapterInterface that can be used to directly access
     * Console and send output.
     *
     * If the result is a string it will be shown directly in the console window.
     * If the result is an array, its contents will be formatted to console window width. The array must
     * have the following format:
     *
     *     return array(
     *                'Usage information line that should be shown as-is',
     *                'Another line of usage info',
     *
     *                '--parameter'        =>   'A short description of that parameter',
     *                '-another-parameter' =>   'A short description of another parameter',
     *                ...
     *            )
     *
     * @param AdapterInterface $console
     * @return array|string|null
     */
    public function getConsoleUsage(AdapterInterface $console)
    {
        return array(
            'hp test [--debug|-d] <msg>' =>  "\nTest command to echo message and check if console routing works:".
                                               "\n'[--verbose|-v]' > enable verbose mode printing all params received in console".
                                               "\n'<msg>'          > the text message to print\n",

//            'hp send <type> [--limit=] [--debug|-d] [--verbose|-v] [--log|-l]' => "\nCommand to send SMS or EMAIL from related message queue extracting the oldest:".
//                                                                                  "\n'<type>'         > message type to send, accept only 'email' or 'sms' string".
//                                                                                  "\n'[--limit=]'     > number of messages extraced ('--limit' default value is 10)".
//                                                                                  "\n'[--debug|-d]'   > enable debug mode printing all params received in console".
//                                                                                  "\n'[--verbose|-v]' > enable verbose mode printing send status log in console".
//                                                                                  "\n'[--log|-l]'     > enable log writing send status log in queue log file\n"
        );
    }
}
