<?php

namespace Console\Controller;

use Application\Model\BackendUsersModel;
use Application\Model\ContentModel;
use Common\Helper\AclHelper;
use Propel\Runtime\Exception\PropelException;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Common\Helper\SecurityHelper;
use Database\HubPlus\Role,
    Database\HubPlus\Category,
    Database\HubPlus\Template;

class ConsoleController extends AbstractActionController
{
    private $config;

    /* @var $route_params RouteMatch */
    private $route_params = null;

    private $verboseMode = false;
    private $debugMode = false;
    private $logMode = false;

    public function onDispatch(MvcEvent $e)
    {
        $request = $this->getRequest();

        if (!$request instanceof \Zend\Console\Request) {
            die('ACCESS DANIED: You can only use this action from a console!');
        }

        $this->config = $this->getServiceLocator()->get('config');

        $this->route_params = $this->getEvent()->getRouteMatch();
        $this->verboseMode = ($this->route_params->getParam('verbose') || $this->route_params->getParam('v'));
        $this->debugMode = ($this->route_params->getParam('debug') || $this->route_params->getParam('d'));
        $this->logMode = ($this->route_params->getParam('log') || $this->route_params->getParam('l'));

        if ($this->debugMode) echo print_r($this->route_params->getParams(), true) . "\n";

        return parent::onDispatch($e);
    }

    public function testAction()
    {
        $msg = $this->route_params->getParam('msg');
        echo "test " . $msg . "\n";
    }

    public function importAction()
    {
        error_reporting(E_ERROR | E_WARNING | E_PARSE);

        $limit = (int)$this->route_params->getParam('limit');
        $email = $this->route_params->getParam('email');
        $pwd = $this->route_params->getParam('password');

        $aclHelper = new AclHelper(@$this->getServiceLocator());
        $aclHelper->login($email, $pwd);

        if (!$aclHelper->isLogged()) {
            echo "LOGIN Failed!";
        } else {
            echo "[START import]\n";

            $start_time = time();
            $cntModel = new ContentModel(@$this->getServiceLocator());
            $cntModel->importPerson();

            $end_time = time();
            echo "[" . ($end_time - $start_time) . "] seconds elapsed\n";
        }
//        var_dump($json);
    }

    public function installHubPlusInstanceAction()
    {
        try {
            $appName = $this->route_params->getParam('appName');
            $domain = $this->route_params->getParam('domain');

            // Add roles
            $roles = array(
                array(
                    "name" => "SUPERADMIN",
                    "description" => "Super Administrator",
                    "icon" => null,
                ),
                array(
                    "name" => "ADMIN",
                    "description" => "Administrator",
                    "icon" => null,
                ),
                array(
                    "name" => "USER",
                    "description" => "User",
                    "icon" => null,
                ),
            );

            foreach ($roles as $role) {
                try {
                    $roleDB = new Role();
                    $roleDB->setName($role['name'])
                        ->setDescription($role['description'])
                        ->setIcon($role['icon']);
                    $roleDB->save();
                } catch (PropelException $exception) {
                    echo "\e[31m " . $exception->getMessage() . "\e[0m\n\n";
                    exit(1);
                }
            }

            // Add Categories
            $categories = array(
                array(
                    'name' => 'Event and Exhibitions',
                    'weight' => 1,
                    'visible' => 1,
                    'setting' => '{ "section_types": ["EVENT", "PERSON", "POI"] , "content_types": ["EVENT", "PERSON", "POI"] }'
                ),
                array(
                    'name' => 'Store and Retail',
                    'weight' => 2,
                    'visible' => 1,
                    'setting' => '{ "section_types": ["EVENT", "PERSON", "POI"] , "content_types": ["EVENT", "PERSON", "POI"] }'
                ),
                array(
                    'name' => 'Musem and Art Gallery',
                    'weight' => 3,
                    'visible' => 1,
                    'setting' => '{ "section_types": ["EVENT", "PERSON", "POI"] , "content_types": ["EVENT", "PERSON", "POI"] }'
                ),
                array(
                    'name' => 'Sport & Wellness',
                    'weight' => 4,
                    'visible' => 1,
                    'setting' => '{ "section_types": ["EVENT", "PERSON", "POI"] , "content_types": ["EVENT", "PERSON", "POI"] }'
                ),
                array(
                    'name' => 'Tourism',
                    'weight' => 5,
                    'visible' => 1,
                    'setting' => '{ "section_types": ["EVENT", "PERSON", "POI"] , "content_types": ["EVENT", "PERSON", "POI"] }'
                ),
            );

            foreach ($categories as $category) {
                try {
                    $categoryDB = new Category();
                    $categoryDB->setName($category['name'])
                        ->setWeight($category['weight'])
                        ->setVisible($category['visible'])
                        ->setSetting($category['setting']);
                    $categoryDB->save();
                } catch (PropelException $exception) {
                    echo "\e[31m " . $exception->getMessage() . "\e[0m\n\n";
                    exit(1);
                }
            }

            // Add templates
            $templates = array(
                array(
                    'title' => 'custom',
                    'subtitle' => '',
                    'description' => '',
                    'icon' => null,
                    'cover' => '',
                    'json_views' => '[]',
                    'layout' => '{ "name": "CUSTOM_LAYOUT", "like": false, "share": false, "filter": false, "sort": false, "page_size": 10, "auth": { "mode": "POPUP", "login": "SIMPLE" }, "theme": { "color": { "global_bg": "#ffffff", "primary_bg": "#1a7197", "secondary_bg": "#e6e6e6", "accent_bg": "#e83185", "light_txt": "#e6e6e6", "dark_txt": "#000000" }, "font": { "primary": "System font", "secondary": "System font" } } }',
                    'enabled' => 1,
                ),
                array(
                    'title' => 'anakin',
                    'subtitle' => '',
                    'description' => '',
                    'icon' => null,
                    'cover' => '',
                    'json_views' => '[]',
                    'layout' => '{ "name": "ANAKIN_LAYOUT", "like": false, "share": false, "filter": false, "sort": false, "page_size": 10, "auth": { "mode": "POPUP", "login": "SIMPLE" }, "theme": { "color": { "global_bg": "#ffffff", "primary_bg": "#3b3a36", "secondary_bg": "#b3c2bf", "accent_bg": "#e9ece5", "light_txt": "#ffffff", "dark_txt": "#3b3a36" }, "font": { "primary": "System font", "secondary": "System font" } } }',
                    'enabled' => 1,
                ),
                array(
                    'title' => 'avenir',
                    'subtitle' => '',
                    'description' => '',
                    'icon' => null,
                    'cover' => '',
                    'json_views' => '[]',
                    'layout' => '{ "name": "AVENIR_LAYOUT", "like": false, "share": false, "filter": false, "sort": false, "page_size": 10, "auth": { "mode": "POPUP", "login": "SIMPLE" }, "theme": { "color": { "global_bg": "#e1e8f0", "primary_bg": "#6ed3cf", "secondary_bg": "#9068be", "accent_bg": "#e62739", "light_txt": "#e1e8f0", "dark_txt": "#9068be" }, "font": { "primary": "System font", "secondary": "System font" } } }',
                    'enabled' => 1,
                ),
                array(
                    'title' => 'bbq',
                    'subtitle' => '',
                    'description' => '',
                    'icon' => null,
                    'cover' => '',
                    'json_views' => '[]',
                    'layout' => '{ "name": "BBQ_LAYOUT", "like": false, "share": false, "filter": false, "sort": false, "page_size": 10, "auth": { "mode": "POPUP", "login": "SIMPLE" }, "theme": { "color": { "global_bg": "#bcd5d1", "primary_bg": "#1d2120", "secondary_bg": "#5a5c51", "accent_bg": "#ba9077", "light_txt": "#bcd5d1", "dark_txt": "#1d2120" }, "font": { "primary": "System font", "secondary": "System font" } } }',
                    'enabled' => 1,
                ),
                array(
                    'title' => 'ceramic',
                    'subtitle' => '',
                    'description' => '',
                    'icon' => null,
                    'cover' => '',
                    'json_views' => '[]',
                    'layout' => '{ "name": "CERAMIC_LAYOUT", "like": false, "share": false, "filter": false, "sort": false, "page_size": 10, "auth": { "mode": "POPUP", "login": "SIMPLE" }, "theme": { "color": { "global_bg": "#f2efe8", "primary_bg": "#b0aac2", "secondary_bg": "#c2d4d8", "accent_bg": "#a6ada4", "light_txt": "#f2efe8", "dark_txt": "#777382" }, "font": { "primary": "System font", "secondary": "System font" } } }',
                    'enabled' => 1,
                ),
                array(
                    'title' => 'denkwerk',
                    'subtitle' => '',
                    'description' => '',
                    'icon' => null,
                    'cover' => '',
                    'json_views' => '[]',
                    'layout' => '{ "name": "DENKWERK_LAYOUT", "like": false, "share": false, "filter": false, "sort": false, "page_size": 10, "auth": { "mode": "POPUP", "login": "SIMPLE" }, "theme": { "color": { "global_bg": "#feffff", "primary_bg": "#312c32", "secondary_bg": "#daad86", "accent_bg": "#98dafc", "light_txt": "#feffff", "dark_txt": "#312c32" }, "font": { "primary": "System font", "secondary": "System font" } } }',
                    'enabled' => 1,
                ),
                array(
                    'title' => 'dream-team',
                    'subtitle' => '',
                    'description' => '',
                    'icon' => null,
                    'cover' => '',
                    'json_views' => '[]',
                    'layout' => '{ "name": "DREAMTEAM_LAYOUT", "like": false, "share": false, "filter": false, "sort": false, "page_size": 10, "auth": { "mode": "POPUP", "login": "SIMPLE" }, "theme": { "color": { "global_bg": "#e3e3e3", "primary_bg": "#c9c9c9", "secondary_bg": "#9ad3de", "accent_bg": "#89bdd3", "light_txt": "#e3e3e3", "dark_txt": "#787878" }, "font": { "primary": "System font", "secondary": "System font" } } }',
                    'enabled' => 1,
                ),
                array(
                    'title' => 'landscape',
                    'subtitle' => '',
                    'description' => '',
                    'icon' => null,
                    'cover' => '',
                    'json_views' => '[]',
                    'layout' => '{ "name": "LANDSCAPE_LAYOUT", "like": false, "share": false, "filter": false, "sort": false, "page_size": 10, "auth": { "mode": "POPUP", "login": "SIMPLE" }, "theme": { "color": { "global_bg": "#dddfd4", "primary_bg": "#173e43", "secondary_bg": "#3fb0ac", "accent_bg": "#173e43", "light_txt": "#dddfd4", "dark_txt": "#3fb0ac" }, "font": { "primary": "System font", "secondary": "System font" } } }',
                    'enabled' => 1,
                ),
                array(
                    'title' => 'lookbook',
                    'subtitle' => '',
                    'description' => '',
                    'icon' => null,
                    'cover' => '',
                    'json_views' => '[]',
                    'layout' => '{ "name": "LOOKBOOK_LAYOUT", "like": false, "share": false, "filter": false, "sort": false, "page_size": 10, "auth": { "mode": "POPUP", "login": "SIMPLE" }, "theme": { "color": { "global_bg": "#ffffff", "primary_bg": "#9fa8a3", "secondary_bg": "#c5d5cb", "accent_bg": "#9fa8a3", "light_txt": "#ffffff", "dark_txt": "#595959" }, "font": { "primary": "System font", "secondary": "System font" } } }',
                    'enabled' => 1,
                ),
                array(
                    'title' => 'quary',
                    'subtitle' => '',
                    'description' => '',
                    'icon' => null,
                    'cover' => '',
                    'json_views' => '[]',
                    'layout' => '{ "name": "QUARY_LAYOUT", "like": false, "share": false, "filter": false, "sort": false, "page_size": 10, "auth": { "mode": "POPUP", "login": "SIMPLE" }, "theme": { "color": { "global_bg": "#e8edf3", "primary_bg": "#22264b", "secondary_bg": "#b56969", "accent_bg": "#e6cf8b", "light_txt": "#e8edf3", "dark_txt": "#22264b" }, "font": { "primary": "System font", "secondary": "System font" } } }',
                    'enabled' => 1,
                ),
                array(
                    'title' => 'relax',
                    'subtitle' => '',
                    'description' => '',
                    'icon' => null,
                    'cover' => '',
                    'json_views' => '[]',
                    'layout' => '{ "name": "RELAX_LAYOUT", "like": false, "share": false, "filter": false, "sort": false, "page_size": 10, "auth": { "mode": "POPUP", "login": "SIMPLE" }, "theme": { "color": { "global_bg": "#ffffff", "primary_bg": "#7d4627", "secondary_bg": "#a8b6bf", "accent_bg": "#edd9c0", "light_txt": "#ffffff",  "dark_txt": "#7d4627" }, "font": { "primary": "System font", "secondary": "System font" } } }',
                    'enabled' => 1,
                ),
                array(
                    'title' => 'sunglass',
                    'subtitle' => '',
                    'description' => '',
                    'icon' => null,
                    'cover' => '',
                    'json_views' => '[]',
                    'layout' => '{ "name": "SUNGLASSES_LAYOUT", "like": false, "share": false, "filter": false, "sort": false, "page_size": 10, "auth": { "mode": "POPUP", "login": "SIMPLE" }, "theme": { "color": { "global_bg": "#f0eceb", "primary_bg": "#283018", "secondary_bg": "#aa863a", "accent_bg": "#729f98", "light_txt": "#f0eceb", "dark_txt": "#283018" }, "font": { "primary": "System font", "secondary": "System font" } } }',
                    'enabled' => 1,
                ),
            );

            foreach ($templates as $template) {
                try {
                    $templateDB = new Template();
                    $templateDB->setTitle($template['title'])
                        ->setSubtitle($template['subtitle'])
                        ->setDescription($template['description'])
                        ->setIcon($template['icon'])
                        ->setCover($template['cover'])
                        ->setJsonViews($template['json_views'])
                        ->setLayout($template['layout'])
                        ->setEnabled($template['enabled']);
                    $templateDB->save();
                } catch (PropelException $exception) {
                    echo "\e[31m " . $exception->getMessage() . "\e[0m\n\n";
                    exit(1);
                }
            }

            // Add Super Admin user
            $BackendUsersModel = new BackendUsersModel($this->getServiceLocator());
            $email = "info@rhubbit.com";
            $password = SecurityHelper::generateString(8);
            $firstName = "Super";
            $lastName = "Admin";

            $res = $BackendUsersModel->registerUser($email, $password, "yes", $firstName, $lastName, 1);
            if ($res === null) {
                echo "\e[31m Super Admin User not created.\e[0m\n\n";
                exit(1);
            } else {
                $BackendUsersModel->sendRegistrationEmail($email, $password, $firstName, $lastName, "email/admin/super-admin-created", "http://" . $appName . "." . $domain);
            }
        } catch (\Exception $exception) {
            echo "\e[31m " . $exception->getMessage() . "\e[0m\n\n";
            exit(1);
        }

    }

}
