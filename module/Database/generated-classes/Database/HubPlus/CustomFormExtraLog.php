<?php

namespace Database\HubPlus;

use Database\HubPlus\Base\CustomFormExtraLog as BaseCustomFormExtraLog;

/**
 * Skeleton subclass for representing a row from the 'custom_form_extra_log' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomFormExtraLog extends BaseCustomFormExtraLog
{

}
