<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\PostPoiArchive as ChildPostPoiArchive;
use Database\HubPlus\PostPoiArchiveQuery as ChildPostPoiArchiveQuery;
use Database\HubPlus\Map\PostPoiArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'post_poi_archive' table.
 *
 *
 *
 * @method     ChildPostPoiArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPostPoiArchiveQuery orderByPostId($order = Criteria::ASC) Order by the post_id column
 * @method     ChildPostPoiArchiveQuery orderByIdentifier($order = Criteria::ASC) Order by the identifier column
 * @method     ChildPostPoiArchiveQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildPostPoiArchiveQuery orderByLat($order = Criteria::ASC) Order by the lat column
 * @method     ChildPostPoiArchiveQuery orderByLng($order = Criteria::ASC) Order by the lng column
 * @method     ChildPostPoiArchiveQuery orderByRadius($order = Criteria::ASC) Order by the radius column
 * @method     ChildPostPoiArchiveQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildPostPoiArchiveQuery orderByMajor($order = Criteria::ASC) Order by the major column
 * @method     ChildPostPoiArchiveQuery orderByMinor($order = Criteria::ASC) Order by the minor column
 * @method     ChildPostPoiArchiveQuery orderByDistance($order = Criteria::ASC) Order by the distance column
 * @method     ChildPostPoiArchiveQuery orderByPin($order = Criteria::ASC) Order by the pin column
 * @method     ChildPostPoiArchiveQuery orderByPoiNotification($order = Criteria::ASC) Order by the poi_notification column
 * @method     ChildPostPoiArchiveQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildPostPoiArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPostPoiArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildPostPoiArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildPostPoiArchiveQuery groupById() Group by the id column
 * @method     ChildPostPoiArchiveQuery groupByPostId() Group by the post_id column
 * @method     ChildPostPoiArchiveQuery groupByIdentifier() Group by the identifier column
 * @method     ChildPostPoiArchiveQuery groupByType() Group by the type column
 * @method     ChildPostPoiArchiveQuery groupByLat() Group by the lat column
 * @method     ChildPostPoiArchiveQuery groupByLng() Group by the lng column
 * @method     ChildPostPoiArchiveQuery groupByRadius() Group by the radius column
 * @method     ChildPostPoiArchiveQuery groupByUuid() Group by the uuid column
 * @method     ChildPostPoiArchiveQuery groupByMajor() Group by the major column
 * @method     ChildPostPoiArchiveQuery groupByMinor() Group by the minor column
 * @method     ChildPostPoiArchiveQuery groupByDistance() Group by the distance column
 * @method     ChildPostPoiArchiveQuery groupByPin() Group by the pin column
 * @method     ChildPostPoiArchiveQuery groupByPoiNotification() Group by the poi_notification column
 * @method     ChildPostPoiArchiveQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildPostPoiArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPostPoiArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildPostPoiArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildPostPoiArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPostPoiArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPostPoiArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPostPoiArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPostPoiArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPostPoiArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPostPoiArchive findOne(ConnectionInterface $con = null) Return the first ChildPostPoiArchive matching the query
 * @method     ChildPostPoiArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPostPoiArchive matching the query, or a new ChildPostPoiArchive object populated from the query conditions when no match is found
 *
 * @method     ChildPostPoiArchive findOneById(int $id) Return the first ChildPostPoiArchive filtered by the id column
 * @method     ChildPostPoiArchive findOneByPostId(int $post_id) Return the first ChildPostPoiArchive filtered by the post_id column
 * @method     ChildPostPoiArchive findOneByIdentifier(string $identifier) Return the first ChildPostPoiArchive filtered by the identifier column
 * @method     ChildPostPoiArchive findOneByType(string $type) Return the first ChildPostPoiArchive filtered by the type column
 * @method     ChildPostPoiArchive findOneByLat(double $lat) Return the first ChildPostPoiArchive filtered by the lat column
 * @method     ChildPostPoiArchive findOneByLng(double $lng) Return the first ChildPostPoiArchive filtered by the lng column
 * @method     ChildPostPoiArchive findOneByRadius(int $radius) Return the first ChildPostPoiArchive filtered by the radius column
 * @method     ChildPostPoiArchive findOneByUuid(string $uuid) Return the first ChildPostPoiArchive filtered by the uuid column
 * @method     ChildPostPoiArchive findOneByMajor(int $major) Return the first ChildPostPoiArchive filtered by the major column
 * @method     ChildPostPoiArchive findOneByMinor(int $minor) Return the first ChildPostPoiArchive filtered by the minor column
 * @method     ChildPostPoiArchive findOneByDistance(string $distance) Return the first ChildPostPoiArchive filtered by the distance column
 * @method     ChildPostPoiArchive findOneByPin(string $pin) Return the first ChildPostPoiArchive filtered by the pin column
 * @method     ChildPostPoiArchive findOneByPoiNotification(boolean $poi_notification) Return the first ChildPostPoiArchive filtered by the poi_notification column
 * @method     ChildPostPoiArchive findOneByDeletedAt(string $deleted_at) Return the first ChildPostPoiArchive filtered by the deleted_at column
 * @method     ChildPostPoiArchive findOneByCreatedAt(string $created_at) Return the first ChildPostPoiArchive filtered by the created_at column
 * @method     ChildPostPoiArchive findOneByUpdatedAt(string $updated_at) Return the first ChildPostPoiArchive filtered by the updated_at column
 * @method     ChildPostPoiArchive findOneByArchivedAt(string $archived_at) Return the first ChildPostPoiArchive filtered by the archived_at column *

 * @method     ChildPostPoiArchive requirePk($key, ConnectionInterface $con = null) Return the ChildPostPoiArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOne(ConnectionInterface $con = null) Return the first ChildPostPoiArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPostPoiArchive requireOneById(int $id) Return the first ChildPostPoiArchive filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByPostId(int $post_id) Return the first ChildPostPoiArchive filtered by the post_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByIdentifier(string $identifier) Return the first ChildPostPoiArchive filtered by the identifier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByType(string $type) Return the first ChildPostPoiArchive filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByLat(double $lat) Return the first ChildPostPoiArchive filtered by the lat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByLng(double $lng) Return the first ChildPostPoiArchive filtered by the lng column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByRadius(int $radius) Return the first ChildPostPoiArchive filtered by the radius column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByUuid(string $uuid) Return the first ChildPostPoiArchive filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByMajor(int $major) Return the first ChildPostPoiArchive filtered by the major column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByMinor(int $minor) Return the first ChildPostPoiArchive filtered by the minor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByDistance(string $distance) Return the first ChildPostPoiArchive filtered by the distance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByPin(string $pin) Return the first ChildPostPoiArchive filtered by the pin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByPoiNotification(boolean $poi_notification) Return the first ChildPostPoiArchive filtered by the poi_notification column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByDeletedAt(string $deleted_at) Return the first ChildPostPoiArchive filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByCreatedAt(string $created_at) Return the first ChildPostPoiArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildPostPoiArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoiArchive requireOneByArchivedAt(string $archived_at) Return the first ChildPostPoiArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPostPoiArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPostPoiArchive objects based on current ModelCriteria
 * @method     ChildPostPoiArchive[]|ObjectCollection findById(int $id) Return ChildPostPoiArchive objects filtered by the id column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByPostId(int $post_id) Return ChildPostPoiArchive objects filtered by the post_id column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByIdentifier(string $identifier) Return ChildPostPoiArchive objects filtered by the identifier column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByType(string $type) Return ChildPostPoiArchive objects filtered by the type column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByLat(double $lat) Return ChildPostPoiArchive objects filtered by the lat column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByLng(double $lng) Return ChildPostPoiArchive objects filtered by the lng column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByRadius(int $radius) Return ChildPostPoiArchive objects filtered by the radius column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByUuid(string $uuid) Return ChildPostPoiArchive objects filtered by the uuid column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByMajor(int $major) Return ChildPostPoiArchive objects filtered by the major column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByMinor(int $minor) Return ChildPostPoiArchive objects filtered by the minor column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByDistance(string $distance) Return ChildPostPoiArchive objects filtered by the distance column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByPin(string $pin) Return ChildPostPoiArchive objects filtered by the pin column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByPoiNotification(boolean $poi_notification) Return ChildPostPoiArchive objects filtered by the poi_notification column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildPostPoiArchive objects filtered by the deleted_at column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPostPoiArchive objects filtered by the created_at column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPostPoiArchive objects filtered by the updated_at column
 * @method     ChildPostPoiArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildPostPoiArchive objects filtered by the archived_at column
 * @method     ChildPostPoiArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PostPoiArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\PostPoiArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\PostPoiArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPostPoiArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPostPoiArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPostPoiArchiveQuery) {
            return $criteria;
        }
        $query = new ChildPostPoiArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPostPoiArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PostPoiArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PostPoiArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPostPoiArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, post_id, identifier, type, lat, lng, radius, uuid, major, minor, distance, pin, poi_notification, deleted_at, created_at, updated_at, archived_at FROM post_poi_archive WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPostPoiArchive $obj */
            $obj = new ChildPostPoiArchive();
            $obj->hydrate($row);
            PostPoiArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPostPoiArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the post_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPostId(1234); // WHERE post_id = 1234
     * $query->filterByPostId(array(12, 34)); // WHERE post_id IN (12, 34)
     * $query->filterByPostId(array('min' => 12)); // WHERE post_id > 12
     * </code>
     *
     * @param     mixed $postId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByPostId($postId = null, $comparison = null)
    {
        if (is_array($postId)) {
            $useMinMax = false;
            if (isset($postId['min'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_POST_ID, $postId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($postId['max'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_POST_ID, $postId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_POST_ID, $postId, $comparison);
    }

    /**
     * Filter the query on the identifier column
     *
     * Example usage:
     * <code>
     * $query->filterByIdentifier('fooValue');   // WHERE identifier = 'fooValue'
     * $query->filterByIdentifier('%fooValue%', Criteria::LIKE); // WHERE identifier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $identifier The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByIdentifier($identifier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($identifier)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_IDENTIFIER, $identifier, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the lat column
     *
     * Example usage:
     * <code>
     * $query->filterByLat(1234); // WHERE lat = 1234
     * $query->filterByLat(array(12, 34)); // WHERE lat IN (12, 34)
     * $query->filterByLat(array('min' => 12)); // WHERE lat > 12
     * </code>
     *
     * @param     mixed $lat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByLat($lat = null, $comparison = null)
    {
        if (is_array($lat)) {
            $useMinMax = false;
            if (isset($lat['min'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_LAT, $lat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lat['max'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_LAT, $lat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_LAT, $lat, $comparison);
    }

    /**
     * Filter the query on the lng column
     *
     * Example usage:
     * <code>
     * $query->filterByLng(1234); // WHERE lng = 1234
     * $query->filterByLng(array(12, 34)); // WHERE lng IN (12, 34)
     * $query->filterByLng(array('min' => 12)); // WHERE lng > 12
     * </code>
     *
     * @param     mixed $lng The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByLng($lng = null, $comparison = null)
    {
        if (is_array($lng)) {
            $useMinMax = false;
            if (isset($lng['min'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_LNG, $lng['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lng['max'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_LNG, $lng['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_LNG, $lng, $comparison);
    }

    /**
     * Filter the query on the radius column
     *
     * Example usage:
     * <code>
     * $query->filterByRadius(1234); // WHERE radius = 1234
     * $query->filterByRadius(array(12, 34)); // WHERE radius IN (12, 34)
     * $query->filterByRadius(array('min' => 12)); // WHERE radius > 12
     * </code>
     *
     * @param     mixed $radius The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByRadius($radius = null, $comparison = null)
    {
        if (is_array($radius)) {
            $useMinMax = false;
            if (isset($radius['min'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_RADIUS, $radius['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($radius['max'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_RADIUS, $radius['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_RADIUS, $radius, $comparison);
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uuid The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_UUID, $uuid, $comparison);
    }

    /**
     * Filter the query on the major column
     *
     * Example usage:
     * <code>
     * $query->filterByMajor(1234); // WHERE major = 1234
     * $query->filterByMajor(array(12, 34)); // WHERE major IN (12, 34)
     * $query->filterByMajor(array('min' => 12)); // WHERE major > 12
     * </code>
     *
     * @param     mixed $major The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByMajor($major = null, $comparison = null)
    {
        if (is_array($major)) {
            $useMinMax = false;
            if (isset($major['min'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_MAJOR, $major['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($major['max'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_MAJOR, $major['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_MAJOR, $major, $comparison);
    }

    /**
     * Filter the query on the minor column
     *
     * Example usage:
     * <code>
     * $query->filterByMinor(1234); // WHERE minor = 1234
     * $query->filterByMinor(array(12, 34)); // WHERE minor IN (12, 34)
     * $query->filterByMinor(array('min' => 12)); // WHERE minor > 12
     * </code>
     *
     * @param     mixed $minor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByMinor($minor = null, $comparison = null)
    {
        if (is_array($minor)) {
            $useMinMax = false;
            if (isset($minor['min'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_MINOR, $minor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($minor['max'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_MINOR, $minor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_MINOR, $minor, $comparison);
    }

    /**
     * Filter the query on the distance column
     *
     * Example usage:
     * <code>
     * $query->filterByDistance('fooValue');   // WHERE distance = 'fooValue'
     * $query->filterByDistance('%fooValue%', Criteria::LIKE); // WHERE distance LIKE '%fooValue%'
     * </code>
     *
     * @param     string $distance The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByDistance($distance = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($distance)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_DISTANCE, $distance, $comparison);
    }

    /**
     * Filter the query on the pin column
     *
     * Example usage:
     * <code>
     * $query->filterByPin('fooValue');   // WHERE pin = 'fooValue'
     * $query->filterByPin('%fooValue%', Criteria::LIKE); // WHERE pin LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pin The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByPin($pin = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pin)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_PIN, $pin, $comparison);
    }

    /**
     * Filter the query on the poi_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByPoiNotification(true); // WHERE poi_notification = true
     * $query->filterByPoiNotification('yes'); // WHERE poi_notification = true
     * </code>
     *
     * @param     boolean|string $poiNotification The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByPoiNotification($poiNotification = null, $comparison = null)
    {
        if (is_string($poiNotification)) {
            $poiNotification = in_array(strtolower($poiNotification), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_POI_NOTIFICATION, $poiNotification, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(PostPoiArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPostPoiArchive $postPoiArchive Object to remove from the list of results
     *
     * @return $this|ChildPostPoiArchiveQuery The current query, for fluid interface
     */
    public function prune($postPoiArchive = null)
    {
        if ($postPoiArchive) {
            $this->addUsingAlias(PostPoiArchiveTableMap::COL_ID, $postPoiArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the post_poi_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostPoiArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PostPoiArchiveTableMap::clearInstancePool();
            PostPoiArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostPoiArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PostPoiArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PostPoiArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PostPoiArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PostPoiArchiveQuery
