<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\SectionArchive as ChildSectionArchive;
use Database\HubPlus\SectionArchiveQuery as ChildSectionArchiveQuery;
use Database\HubPlus\Map\SectionArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'section_archive' table.
 *
 *
 *
 * @method     ChildSectionArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSectionArchiveQuery orderByArea($order = Criteria::ASC) Order by the area column
 * @method     ChildSectionArchiveQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildSectionArchiveQuery orderByAuthorId($order = Criteria::ASC) Order by the author_id column
 * @method     ChildSectionArchiveQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildSectionArchiveQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildSectionArchiveQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 * @method     ChildSectionArchiveQuery orderByIconId($order = Criteria::ASC) Order by the icon_id column
 * @method     ChildSectionArchiveQuery orderByIconSelectedId($order = Criteria::ASC) Order by the icon_selected_id column
 * @method     ChildSectionArchiveQuery orderByResource($order = Criteria::ASC) Order by the resource column
 * @method     ChildSectionArchiveQuery orderByParams($order = Criteria::ASC) Order by the params column
 * @method     ChildSectionArchiveQuery orderByTemplateId($order = Criteria::ASC) Order by the template_id column
 * @method     ChildSectionArchiveQuery orderByLayout($order = Criteria::ASC) Order by the layout column
 * @method     ChildSectionArchiveQuery orderByTags($order = Criteria::ASC) Order by the tags column
 * @method     ChildSectionArchiveQuery orderByTagsUser($order = Criteria::ASC) Order by the tags_user column
 * @method     ChildSectionArchiveQuery orderByParsedTags($order = Criteria::ASC) Order by the parsed_tags column
 * @method     ChildSectionArchiveQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildSectionArchiveQuery orderByWeight($order = Criteria::ASC) Order by the weight column
 * @method     ChildSectionArchiveQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildSectionArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildSectionArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildSectionArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildSectionArchiveQuery groupById() Group by the id column
 * @method     ChildSectionArchiveQuery groupByArea() Group by the area column
 * @method     ChildSectionArchiveQuery groupByType() Group by the type column
 * @method     ChildSectionArchiveQuery groupByAuthorId() Group by the author_id column
 * @method     ChildSectionArchiveQuery groupByTitle() Group by the title column
 * @method     ChildSectionArchiveQuery groupByDescription() Group by the description column
 * @method     ChildSectionArchiveQuery groupBySlug() Group by the slug column
 * @method     ChildSectionArchiveQuery groupByIconId() Group by the icon_id column
 * @method     ChildSectionArchiveQuery groupByIconSelectedId() Group by the icon_selected_id column
 * @method     ChildSectionArchiveQuery groupByResource() Group by the resource column
 * @method     ChildSectionArchiveQuery groupByParams() Group by the params column
 * @method     ChildSectionArchiveQuery groupByTemplateId() Group by the template_id column
 * @method     ChildSectionArchiveQuery groupByLayout() Group by the layout column
 * @method     ChildSectionArchiveQuery groupByTags() Group by the tags column
 * @method     ChildSectionArchiveQuery groupByTagsUser() Group by the tags_user column
 * @method     ChildSectionArchiveQuery groupByParsedTags() Group by the parsed_tags column
 * @method     ChildSectionArchiveQuery groupByStatus() Group by the status column
 * @method     ChildSectionArchiveQuery groupByWeight() Group by the weight column
 * @method     ChildSectionArchiveQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildSectionArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildSectionArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildSectionArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildSectionArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSectionArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSectionArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSectionArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSectionArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSectionArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSectionArchive findOne(ConnectionInterface $con = null) Return the first ChildSectionArchive matching the query
 * @method     ChildSectionArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSectionArchive matching the query, or a new ChildSectionArchive object populated from the query conditions when no match is found
 *
 * @method     ChildSectionArchive findOneById(int $id) Return the first ChildSectionArchive filtered by the id column
 * @method     ChildSectionArchive findOneByArea(string $area) Return the first ChildSectionArchive filtered by the area column
 * @method     ChildSectionArchive findOneByType(string $type) Return the first ChildSectionArchive filtered by the type column
 * @method     ChildSectionArchive findOneByAuthorId(int $author_id) Return the first ChildSectionArchive filtered by the author_id column
 * @method     ChildSectionArchive findOneByTitle(string $title) Return the first ChildSectionArchive filtered by the title column
 * @method     ChildSectionArchive findOneByDescription(string $description) Return the first ChildSectionArchive filtered by the description column
 * @method     ChildSectionArchive findOneBySlug(string $slug) Return the first ChildSectionArchive filtered by the slug column
 * @method     ChildSectionArchive findOneByIconId(int $icon_id) Return the first ChildSectionArchive filtered by the icon_id column
 * @method     ChildSectionArchive findOneByIconSelectedId(int $icon_selected_id) Return the first ChildSectionArchive filtered by the icon_selected_id column
 * @method     ChildSectionArchive findOneByResource(string $resource) Return the first ChildSectionArchive filtered by the resource column
 * @method     ChildSectionArchive findOneByParams(string $params) Return the first ChildSectionArchive filtered by the params column
 * @method     ChildSectionArchive findOneByTemplateId(int $template_id) Return the first ChildSectionArchive filtered by the template_id column
 * @method     ChildSectionArchive findOneByLayout(string $layout) Return the first ChildSectionArchive filtered by the layout column
 * @method     ChildSectionArchive findOneByTags(string $tags) Return the first ChildSectionArchive filtered by the tags column
 * @method     ChildSectionArchive findOneByTagsUser(string $tags_user) Return the first ChildSectionArchive filtered by the tags_user column
 * @method     ChildSectionArchive findOneByParsedTags(string $parsed_tags) Return the first ChildSectionArchive filtered by the parsed_tags column
 * @method     ChildSectionArchive findOneByStatus(boolean $status) Return the first ChildSectionArchive filtered by the status column
 * @method     ChildSectionArchive findOneByWeight(int $weight) Return the first ChildSectionArchive filtered by the weight column
 * @method     ChildSectionArchive findOneByDeletedAt(string $deleted_at) Return the first ChildSectionArchive filtered by the deleted_at column
 * @method     ChildSectionArchive findOneByCreatedAt(string $created_at) Return the first ChildSectionArchive filtered by the created_at column
 * @method     ChildSectionArchive findOneByUpdatedAt(string $updated_at) Return the first ChildSectionArchive filtered by the updated_at column
 * @method     ChildSectionArchive findOneByArchivedAt(string $archived_at) Return the first ChildSectionArchive filtered by the archived_at column *

 * @method     ChildSectionArchive requirePk($key, ConnectionInterface $con = null) Return the ChildSectionArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOne(ConnectionInterface $con = null) Return the first ChildSectionArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSectionArchive requireOneById(int $id) Return the first ChildSectionArchive filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByArea(string $area) Return the first ChildSectionArchive filtered by the area column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByType(string $type) Return the first ChildSectionArchive filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByAuthorId(int $author_id) Return the first ChildSectionArchive filtered by the author_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByTitle(string $title) Return the first ChildSectionArchive filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByDescription(string $description) Return the first ChildSectionArchive filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneBySlug(string $slug) Return the first ChildSectionArchive filtered by the slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByIconId(int $icon_id) Return the first ChildSectionArchive filtered by the icon_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByIconSelectedId(int $icon_selected_id) Return the first ChildSectionArchive filtered by the icon_selected_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByResource(string $resource) Return the first ChildSectionArchive filtered by the resource column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByParams(string $params) Return the first ChildSectionArchive filtered by the params column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByTemplateId(int $template_id) Return the first ChildSectionArchive filtered by the template_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByLayout(string $layout) Return the first ChildSectionArchive filtered by the layout column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByTags(string $tags) Return the first ChildSectionArchive filtered by the tags column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByTagsUser(string $tags_user) Return the first ChildSectionArchive filtered by the tags_user column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByParsedTags(string $parsed_tags) Return the first ChildSectionArchive filtered by the parsed_tags column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByStatus(boolean $status) Return the first ChildSectionArchive filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByWeight(int $weight) Return the first ChildSectionArchive filtered by the weight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByDeletedAt(string $deleted_at) Return the first ChildSectionArchive filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByCreatedAt(string $created_at) Return the first ChildSectionArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildSectionArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionArchive requireOneByArchivedAt(string $archived_at) Return the first ChildSectionArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSectionArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSectionArchive objects based on current ModelCriteria
 * @method     ChildSectionArchive[]|ObjectCollection findById(int $id) Return ChildSectionArchive objects filtered by the id column
 * @method     ChildSectionArchive[]|ObjectCollection findByArea(string $area) Return ChildSectionArchive objects filtered by the area column
 * @method     ChildSectionArchive[]|ObjectCollection findByType(string $type) Return ChildSectionArchive objects filtered by the type column
 * @method     ChildSectionArchive[]|ObjectCollection findByAuthorId(int $author_id) Return ChildSectionArchive objects filtered by the author_id column
 * @method     ChildSectionArchive[]|ObjectCollection findByTitle(string $title) Return ChildSectionArchive objects filtered by the title column
 * @method     ChildSectionArchive[]|ObjectCollection findByDescription(string $description) Return ChildSectionArchive objects filtered by the description column
 * @method     ChildSectionArchive[]|ObjectCollection findBySlug(string $slug) Return ChildSectionArchive objects filtered by the slug column
 * @method     ChildSectionArchive[]|ObjectCollection findByIconId(int $icon_id) Return ChildSectionArchive objects filtered by the icon_id column
 * @method     ChildSectionArchive[]|ObjectCollection findByIconSelectedId(int $icon_selected_id) Return ChildSectionArchive objects filtered by the icon_selected_id column
 * @method     ChildSectionArchive[]|ObjectCollection findByResource(string $resource) Return ChildSectionArchive objects filtered by the resource column
 * @method     ChildSectionArchive[]|ObjectCollection findByParams(string $params) Return ChildSectionArchive objects filtered by the params column
 * @method     ChildSectionArchive[]|ObjectCollection findByTemplateId(int $template_id) Return ChildSectionArchive objects filtered by the template_id column
 * @method     ChildSectionArchive[]|ObjectCollection findByLayout(string $layout) Return ChildSectionArchive objects filtered by the layout column
 * @method     ChildSectionArchive[]|ObjectCollection findByTags(string $tags) Return ChildSectionArchive objects filtered by the tags column
 * @method     ChildSectionArchive[]|ObjectCollection findByTagsUser(string $tags_user) Return ChildSectionArchive objects filtered by the tags_user column
 * @method     ChildSectionArchive[]|ObjectCollection findByParsedTags(string $parsed_tags) Return ChildSectionArchive objects filtered by the parsed_tags column
 * @method     ChildSectionArchive[]|ObjectCollection findByStatus(boolean $status) Return ChildSectionArchive objects filtered by the status column
 * @method     ChildSectionArchive[]|ObjectCollection findByWeight(int $weight) Return ChildSectionArchive objects filtered by the weight column
 * @method     ChildSectionArchive[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildSectionArchive objects filtered by the deleted_at column
 * @method     ChildSectionArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildSectionArchive objects filtered by the created_at column
 * @method     ChildSectionArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildSectionArchive objects filtered by the updated_at column
 * @method     ChildSectionArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildSectionArchive objects filtered by the archived_at column
 * @method     ChildSectionArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SectionArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\SectionArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\SectionArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSectionArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSectionArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSectionArchiveQuery) {
            return $criteria;
        }
        $query = new ChildSectionArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSectionArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SectionArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SectionArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSectionArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, area, type, author_id, title, description, slug, icon_id, icon_selected_id, resource, params, template_id, layout, tags, tags_user, parsed_tags, status, weight, deleted_at, created_at, updated_at, archived_at FROM section_archive WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSectionArchive $obj */
            $obj = new ChildSectionArchive();
            $obj->hydrate($row);
            SectionArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSectionArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SectionArchiveTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SectionArchiveTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the area column
     *
     * Example usage:
     * <code>
     * $query->filterByArea('fooValue');   // WHERE area = 'fooValue'
     * $query->filterByArea('%fooValue%', Criteria::LIKE); // WHERE area LIKE '%fooValue%'
     * </code>
     *
     * @param     string $area The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByArea($area = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($area)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_AREA, $area, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the author_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAuthorId(1234); // WHERE author_id = 1234
     * $query->filterByAuthorId(array(12, 34)); // WHERE author_id IN (12, 34)
     * $query->filterByAuthorId(array('min' => 12)); // WHERE author_id > 12
     * </code>
     *
     * @param     mixed $authorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByAuthorId($authorId = null, $comparison = null)
    {
        if (is_array($authorId)) {
            $useMinMax = false;
            if (isset($authorId['min'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_AUTHOR_ID, $authorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($authorId['max'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_AUTHOR_ID, $authorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_AUTHOR_ID, $authorId, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%', Criteria::LIKE); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the icon_id column
     *
     * Example usage:
     * <code>
     * $query->filterByIconId(1234); // WHERE icon_id = 1234
     * $query->filterByIconId(array(12, 34)); // WHERE icon_id IN (12, 34)
     * $query->filterByIconId(array('min' => 12)); // WHERE icon_id > 12
     * </code>
     *
     * @param     mixed $iconId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByIconId($iconId = null, $comparison = null)
    {
        if (is_array($iconId)) {
            $useMinMax = false;
            if (isset($iconId['min'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_ICON_ID, $iconId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($iconId['max'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_ICON_ID, $iconId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_ICON_ID, $iconId, $comparison);
    }

    /**
     * Filter the query on the icon_selected_id column
     *
     * Example usage:
     * <code>
     * $query->filterByIconSelectedId(1234); // WHERE icon_selected_id = 1234
     * $query->filterByIconSelectedId(array(12, 34)); // WHERE icon_selected_id IN (12, 34)
     * $query->filterByIconSelectedId(array('min' => 12)); // WHERE icon_selected_id > 12
     * </code>
     *
     * @param     mixed $iconSelectedId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByIconSelectedId($iconSelectedId = null, $comparison = null)
    {
        if (is_array($iconSelectedId)) {
            $useMinMax = false;
            if (isset($iconSelectedId['min'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_ICON_SELECTED_ID, $iconSelectedId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($iconSelectedId['max'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_ICON_SELECTED_ID, $iconSelectedId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_ICON_SELECTED_ID, $iconSelectedId, $comparison);
    }

    /**
     * Filter the query on the resource column
     *
     * Example usage:
     * <code>
     * $query->filterByResource('fooValue');   // WHERE resource = 'fooValue'
     * $query->filterByResource('%fooValue%', Criteria::LIKE); // WHERE resource LIKE '%fooValue%'
     * </code>
     *
     * @param     string $resource The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByResource($resource = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($resource)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_RESOURCE, $resource, $comparison);
    }

    /**
     * Filter the query on the params column
     *
     * Example usage:
     * <code>
     * $query->filterByParams('fooValue');   // WHERE params = 'fooValue'
     * $query->filterByParams('%fooValue%', Criteria::LIKE); // WHERE params LIKE '%fooValue%'
     * </code>
     *
     * @param     string $params The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByParams($params = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($params)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_PARAMS, $params, $comparison);
    }

    /**
     * Filter the query on the template_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTemplateId(1234); // WHERE template_id = 1234
     * $query->filterByTemplateId(array(12, 34)); // WHERE template_id IN (12, 34)
     * $query->filterByTemplateId(array('min' => 12)); // WHERE template_id > 12
     * </code>
     *
     * @param     mixed $templateId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByTemplateId($templateId = null, $comparison = null)
    {
        if (is_array($templateId)) {
            $useMinMax = false;
            if (isset($templateId['min'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_TEMPLATE_ID, $templateId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($templateId['max'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_TEMPLATE_ID, $templateId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_TEMPLATE_ID, $templateId, $comparison);
    }

    /**
     * Filter the query on the layout column
     *
     * Example usage:
     * <code>
     * $query->filterByLayout('fooValue');   // WHERE layout = 'fooValue'
     * $query->filterByLayout('%fooValue%', Criteria::LIKE); // WHERE layout LIKE '%fooValue%'
     * </code>
     *
     * @param     string $layout The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByLayout($layout = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($layout)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_LAYOUT, $layout, $comparison);
    }

    /**
     * Filter the query on the tags column
     *
     * Example usage:
     * <code>
     * $query->filterByTags('fooValue');   // WHERE tags = 'fooValue'
     * $query->filterByTags('%fooValue%', Criteria::LIKE); // WHERE tags LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tags The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByTags($tags = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tags)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_TAGS, $tags, $comparison);
    }

    /**
     * Filter the query on the tags_user column
     *
     * Example usage:
     * <code>
     * $query->filterByTagsUser('fooValue');   // WHERE tags_user = 'fooValue'
     * $query->filterByTagsUser('%fooValue%', Criteria::LIKE); // WHERE tags_user LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tagsUser The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByTagsUser($tagsUser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tagsUser)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_TAGS_USER, $tagsUser, $comparison);
    }

    /**
     * Filter the query on the parsed_tags column
     *
     * Example usage:
     * <code>
     * $query->filterByParsedTags('fooValue');   // WHERE parsed_tags = 'fooValue'
     * $query->filterByParsedTags('%fooValue%', Criteria::LIKE); // WHERE parsed_tags LIKE '%fooValue%'
     * </code>
     *
     * @param     string $parsedTags The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByParsedTags($parsedTags = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($parsedTags)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_PARSED_TAGS, $parsedTags, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(true); // WHERE status = true
     * $query->filterByStatus('yes'); // WHERE status = true
     * </code>
     *
     * @param     boolean|string $status The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_string($status)) {
            $status = in_array(strtolower($status), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the weight column
     *
     * Example usage:
     * <code>
     * $query->filterByWeight(1234); // WHERE weight = 1234
     * $query->filterByWeight(array(12, 34)); // WHERE weight IN (12, 34)
     * $query->filterByWeight(array('min' => 12)); // WHERE weight > 12
     * </code>
     *
     * @param     mixed $weight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByWeight($weight = null, $comparison = null)
    {
        if (is_array($weight)) {
            $useMinMax = false;
            if (isset($weight['min'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_WEIGHT, $weight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($weight['max'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_WEIGHT, $weight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_WEIGHT, $weight, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(SectionArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSectionArchive $sectionArchive Object to remove from the list of results
     *
     * @return $this|ChildSectionArchiveQuery The current query, for fluid interface
     */
    public function prune($sectionArchive = null)
    {
        if ($sectionArchive) {
            $this->addUsingAlias(SectionArchiveTableMap::COL_ID, $sectionArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the section_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SectionArchiveTableMap::clearInstancePool();
            SectionArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SectionArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SectionArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SectionArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SectionArchiveQuery
