<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\UserBackend as ChildUserBackend;
use Database\HubPlus\UserBackendArchive as ChildUserBackendArchive;
use Database\HubPlus\UserBackendQuery as ChildUserBackendQuery;
use Database\HubPlus\Map\UserBackendTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'user_backend' table.
 *
 *
 *
 * @method     ChildUserBackendQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserBackendQuery orderByImagepath($order = Criteria::ASC) Order by the imagePath column
 * @method     ChildUserBackendQuery orderByFirstname($order = Criteria::ASC) Order by the firstname column
 * @method     ChildUserBackendQuery orderByLastname($order = Criteria::ASC) Order by the lastname column
 * @method     ChildUserBackendQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildUserBackendQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildUserBackendQuery orderByPasswordRequestedAt($order = Criteria::ASC) Order by the password_requested_at column
 * @method     ChildUserBackendQuery orderByConfirmationToken($order = Criteria::ASC) Order by the confirmation_token column
 * @method     ChildUserBackendQuery orderByLastLogin($order = Criteria::ASC) Order by the last_login column
 * @method     ChildUserBackendQuery orderByLocked($order = Criteria::ASC) Order by the locked column
 * @method     ChildUserBackendQuery orderByLockedAt($order = Criteria::ASC) Order by the locked_at column
 * @method     ChildUserBackendQuery orderByExpired($order = Criteria::ASC) Order by the expired column
 * @method     ChildUserBackendQuery orderByExpiredAt($order = Criteria::ASC) Order by the expired_at column
 * @method     ChildUserBackendQuery orderByRoleId($order = Criteria::ASC) Order by the role_id column
 * @method     ChildUserBackendQuery orderByPrivacy($order = Criteria::ASC) Order by the privacy column
 * @method     ChildUserBackendQuery orderByPrivacyAt($order = Criteria::ASC) Order by the privacy_at column
 * @method     ChildUserBackendQuery orderByTerms($order = Criteria::ASC) Order by the terms column
 * @method     ChildUserBackendQuery orderByTermsAt($order = Criteria::ASC) Order by the terms_at column
 * @method     ChildUserBackendQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildUserBackendQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildUserBackendQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildUserBackendQuery groupById() Group by the id column
 * @method     ChildUserBackendQuery groupByImagepath() Group by the imagePath column
 * @method     ChildUserBackendQuery groupByFirstname() Group by the firstname column
 * @method     ChildUserBackendQuery groupByLastname() Group by the lastname column
 * @method     ChildUserBackendQuery groupByEmail() Group by the email column
 * @method     ChildUserBackendQuery groupByPassword() Group by the password column
 * @method     ChildUserBackendQuery groupByPasswordRequestedAt() Group by the password_requested_at column
 * @method     ChildUserBackendQuery groupByConfirmationToken() Group by the confirmation_token column
 * @method     ChildUserBackendQuery groupByLastLogin() Group by the last_login column
 * @method     ChildUserBackendQuery groupByLocked() Group by the locked column
 * @method     ChildUserBackendQuery groupByLockedAt() Group by the locked_at column
 * @method     ChildUserBackendQuery groupByExpired() Group by the expired column
 * @method     ChildUserBackendQuery groupByExpiredAt() Group by the expired_at column
 * @method     ChildUserBackendQuery groupByRoleId() Group by the role_id column
 * @method     ChildUserBackendQuery groupByPrivacy() Group by the privacy column
 * @method     ChildUserBackendQuery groupByPrivacyAt() Group by the privacy_at column
 * @method     ChildUserBackendQuery groupByTerms() Group by the terms column
 * @method     ChildUserBackendQuery groupByTermsAt() Group by the terms_at column
 * @method     ChildUserBackendQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildUserBackendQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildUserBackendQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildUserBackendQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserBackendQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserBackendQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserBackendQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserBackendQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserBackendQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserBackendQuery leftJoinPost($relationAlias = null) Adds a LEFT JOIN clause to the query using the Post relation
 * @method     ChildUserBackendQuery rightJoinPost($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Post relation
 * @method     ChildUserBackendQuery innerJoinPost($relationAlias = null) Adds a INNER JOIN clause to the query using the Post relation
 *
 * @method     ChildUserBackendQuery joinWithPost($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Post relation
 *
 * @method     ChildUserBackendQuery leftJoinWithPost() Adds a LEFT JOIN clause and with to the query using the Post relation
 * @method     ChildUserBackendQuery rightJoinWithPost() Adds a RIGHT JOIN clause and with to the query using the Post relation
 * @method     ChildUserBackendQuery innerJoinWithPost() Adds a INNER JOIN clause and with to the query using the Post relation
 *
 * @method     ChildUserBackendQuery leftJoinSection($relationAlias = null) Adds a LEFT JOIN clause to the query using the Section relation
 * @method     ChildUserBackendQuery rightJoinSection($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Section relation
 * @method     ChildUserBackendQuery innerJoinSection($relationAlias = null) Adds a INNER JOIN clause to the query using the Section relation
 *
 * @method     ChildUserBackendQuery joinWithSection($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Section relation
 *
 * @method     ChildUserBackendQuery leftJoinWithSection() Adds a LEFT JOIN clause and with to the query using the Section relation
 * @method     ChildUserBackendQuery rightJoinWithSection() Adds a RIGHT JOIN clause and with to the query using the Section relation
 * @method     ChildUserBackendQuery innerJoinWithSection() Adds a INNER JOIN clause and with to the query using the Section relation
 *
 * @method     ChildUserBackendQuery leftJoinUserApplication($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserApplication relation
 * @method     ChildUserBackendQuery rightJoinUserApplication($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserApplication relation
 * @method     ChildUserBackendQuery innerJoinUserApplication($relationAlias = null) Adds a INNER JOIN clause to the query using the UserApplication relation
 *
 * @method     ChildUserBackendQuery joinWithUserApplication($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserApplication relation
 *
 * @method     ChildUserBackendQuery leftJoinWithUserApplication() Adds a LEFT JOIN clause and with to the query using the UserApplication relation
 * @method     ChildUserBackendQuery rightJoinWithUserApplication() Adds a RIGHT JOIN clause and with to the query using the UserApplication relation
 * @method     ChildUserBackendQuery innerJoinWithUserApplication() Adds a INNER JOIN clause and with to the query using the UserApplication relation
 *
 * @method     \Database\HubPlus\PostQuery|\Database\HubPlus\SectionQuery|\Database\HubPlus\UserApplicationQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUserBackend findOne(ConnectionInterface $con = null) Return the first ChildUserBackend matching the query
 * @method     ChildUserBackend findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserBackend matching the query, or a new ChildUserBackend object populated from the query conditions when no match is found
 *
 * @method     ChildUserBackend findOneById(int $id) Return the first ChildUserBackend filtered by the id column
 * @method     ChildUserBackend findOneByImagepath(string $imagePath) Return the first ChildUserBackend filtered by the imagePath column
 * @method     ChildUserBackend findOneByFirstname(string $firstname) Return the first ChildUserBackend filtered by the firstname column
 * @method     ChildUserBackend findOneByLastname(string $lastname) Return the first ChildUserBackend filtered by the lastname column
 * @method     ChildUserBackend findOneByEmail(string $email) Return the first ChildUserBackend filtered by the email column
 * @method     ChildUserBackend findOneByPassword(string $password) Return the first ChildUserBackend filtered by the password column
 * @method     ChildUserBackend findOneByPasswordRequestedAt(int $password_requested_at) Return the first ChildUserBackend filtered by the password_requested_at column
 * @method     ChildUserBackend findOneByConfirmationToken(string $confirmation_token) Return the first ChildUserBackend filtered by the confirmation_token column
 * @method     ChildUserBackend findOneByLastLogin(string $last_login) Return the first ChildUserBackend filtered by the last_login column
 * @method     ChildUserBackend findOneByLocked(boolean $locked) Return the first ChildUserBackend filtered by the locked column
 * @method     ChildUserBackend findOneByLockedAt(string $locked_at) Return the first ChildUserBackend filtered by the locked_at column
 * @method     ChildUserBackend findOneByExpired(boolean $expired) Return the first ChildUserBackend filtered by the expired column
 * @method     ChildUserBackend findOneByExpiredAt(string $expired_at) Return the first ChildUserBackend filtered by the expired_at column
 * @method     ChildUserBackend findOneByRoleId(int $role_id) Return the first ChildUserBackend filtered by the role_id column
 * @method     ChildUserBackend findOneByPrivacy(boolean $privacy) Return the first ChildUserBackend filtered by the privacy column
 * @method     ChildUserBackend findOneByPrivacyAt(string $privacy_at) Return the first ChildUserBackend filtered by the privacy_at column
 * @method     ChildUserBackend findOneByTerms(boolean $terms) Return the first ChildUserBackend filtered by the terms column
 * @method     ChildUserBackend findOneByTermsAt(string $terms_at) Return the first ChildUserBackend filtered by the terms_at column
 * @method     ChildUserBackend findOneByDeletedAt(string $deleted_at) Return the first ChildUserBackend filtered by the deleted_at column
 * @method     ChildUserBackend findOneByCreatedAt(string $created_at) Return the first ChildUserBackend filtered by the created_at column
 * @method     ChildUserBackend findOneByUpdatedAt(string $updated_at) Return the first ChildUserBackend filtered by the updated_at column *

 * @method     ChildUserBackend requirePk($key, ConnectionInterface $con = null) Return the ChildUserBackend by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOne(ConnectionInterface $con = null) Return the first ChildUserBackend matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserBackend requireOneById(int $id) Return the first ChildUserBackend filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByImagepath(string $imagePath) Return the first ChildUserBackend filtered by the imagePath column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByFirstname(string $firstname) Return the first ChildUserBackend filtered by the firstname column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByLastname(string $lastname) Return the first ChildUserBackend filtered by the lastname column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByEmail(string $email) Return the first ChildUserBackend filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByPassword(string $password) Return the first ChildUserBackend filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByPasswordRequestedAt(int $password_requested_at) Return the first ChildUserBackend filtered by the password_requested_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByConfirmationToken(string $confirmation_token) Return the first ChildUserBackend filtered by the confirmation_token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByLastLogin(string $last_login) Return the first ChildUserBackend filtered by the last_login column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByLocked(boolean $locked) Return the first ChildUserBackend filtered by the locked column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByLockedAt(string $locked_at) Return the first ChildUserBackend filtered by the locked_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByExpired(boolean $expired) Return the first ChildUserBackend filtered by the expired column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByExpiredAt(string $expired_at) Return the first ChildUserBackend filtered by the expired_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByRoleId(int $role_id) Return the first ChildUserBackend filtered by the role_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByPrivacy(boolean $privacy) Return the first ChildUserBackend filtered by the privacy column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByPrivacyAt(string $privacy_at) Return the first ChildUserBackend filtered by the privacy_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByTerms(boolean $terms) Return the first ChildUserBackend filtered by the terms column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByTermsAt(string $terms_at) Return the first ChildUserBackend filtered by the terms_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByDeletedAt(string $deleted_at) Return the first ChildUserBackend filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByCreatedAt(string $created_at) Return the first ChildUserBackend filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackend requireOneByUpdatedAt(string $updated_at) Return the first ChildUserBackend filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserBackend[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserBackend objects based on current ModelCriteria
 * @method     ChildUserBackend[]|ObjectCollection findById(int $id) Return ChildUserBackend objects filtered by the id column
 * @method     ChildUserBackend[]|ObjectCollection findByImagepath(string $imagePath) Return ChildUserBackend objects filtered by the imagePath column
 * @method     ChildUserBackend[]|ObjectCollection findByFirstname(string $firstname) Return ChildUserBackend objects filtered by the firstname column
 * @method     ChildUserBackend[]|ObjectCollection findByLastname(string $lastname) Return ChildUserBackend objects filtered by the lastname column
 * @method     ChildUserBackend[]|ObjectCollection findByEmail(string $email) Return ChildUserBackend objects filtered by the email column
 * @method     ChildUserBackend[]|ObjectCollection findByPassword(string $password) Return ChildUserBackend objects filtered by the password column
 * @method     ChildUserBackend[]|ObjectCollection findByPasswordRequestedAt(int $password_requested_at) Return ChildUserBackend objects filtered by the password_requested_at column
 * @method     ChildUserBackend[]|ObjectCollection findByConfirmationToken(string $confirmation_token) Return ChildUserBackend objects filtered by the confirmation_token column
 * @method     ChildUserBackend[]|ObjectCollection findByLastLogin(string $last_login) Return ChildUserBackend objects filtered by the last_login column
 * @method     ChildUserBackend[]|ObjectCollection findByLocked(boolean $locked) Return ChildUserBackend objects filtered by the locked column
 * @method     ChildUserBackend[]|ObjectCollection findByLockedAt(string $locked_at) Return ChildUserBackend objects filtered by the locked_at column
 * @method     ChildUserBackend[]|ObjectCollection findByExpired(boolean $expired) Return ChildUserBackend objects filtered by the expired column
 * @method     ChildUserBackend[]|ObjectCollection findByExpiredAt(string $expired_at) Return ChildUserBackend objects filtered by the expired_at column
 * @method     ChildUserBackend[]|ObjectCollection findByRoleId(int $role_id) Return ChildUserBackend objects filtered by the role_id column
 * @method     ChildUserBackend[]|ObjectCollection findByPrivacy(boolean $privacy) Return ChildUserBackend objects filtered by the privacy column
 * @method     ChildUserBackend[]|ObjectCollection findByPrivacyAt(string $privacy_at) Return ChildUserBackend objects filtered by the privacy_at column
 * @method     ChildUserBackend[]|ObjectCollection findByTerms(boolean $terms) Return ChildUserBackend objects filtered by the terms column
 * @method     ChildUserBackend[]|ObjectCollection findByTermsAt(string $terms_at) Return ChildUserBackend objects filtered by the terms_at column
 * @method     ChildUserBackend[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildUserBackend objects filtered by the deleted_at column
 * @method     ChildUserBackend[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildUserBackend objects filtered by the created_at column
 * @method     ChildUserBackend[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildUserBackend objects filtered by the updated_at column
 * @method     ChildUserBackend[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserBackendQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\UserBackendQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\UserBackend', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserBackendQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserBackendQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserBackendQuery) {
            return $criteria;
        }
        $query = new ChildUserBackendQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserBackend|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserBackendTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserBackendTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserBackend A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, imagePath, firstname, lastname, email, password, password_requested_at, confirmation_token, last_login, locked, locked_at, expired, expired_at, role_id, privacy, privacy_at, terms, terms_at, deleted_at, created_at, updated_at FROM user_backend WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserBackend $obj */
            $obj = new ChildUserBackend();
            $obj->hydrate($row);
            UserBackendTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserBackend|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserBackendTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserBackendTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the imagePath column
     *
     * Example usage:
     * <code>
     * $query->filterByImagepath('fooValue');   // WHERE imagePath = 'fooValue'
     * $query->filterByImagepath('%fooValue%', Criteria::LIKE); // WHERE imagePath LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imagepath The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByImagepath($imagepath = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imagepath)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_IMAGEPATH, $imagepath, $comparison);
    }

    /**
     * Filter the query on the firstname column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstname('fooValue');   // WHERE firstname = 'fooValue'
     * $query->filterByFirstname('%fooValue%', Criteria::LIKE); // WHERE firstname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByFirstname($firstname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_FIRSTNAME, $firstname, $comparison);
    }

    /**
     * Filter the query on the lastname column
     *
     * Example usage:
     * <code>
     * $query->filterByLastname('fooValue');   // WHERE lastname = 'fooValue'
     * $query->filterByLastname('%fooValue%', Criteria::LIKE); // WHERE lastname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByLastname($lastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_LASTNAME, $lastname, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the password_requested_at column
     *
     * Example usage:
     * <code>
     * $query->filterByPasswordRequestedAt(1234); // WHERE password_requested_at = 1234
     * $query->filterByPasswordRequestedAt(array(12, 34)); // WHERE password_requested_at IN (12, 34)
     * $query->filterByPasswordRequestedAt(array('min' => 12)); // WHERE password_requested_at > 12
     * </code>
     *
     * @param     mixed $passwordRequestedAt The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByPasswordRequestedAt($passwordRequestedAt = null, $comparison = null)
    {
        if (is_array($passwordRequestedAt)) {
            $useMinMax = false;
            if (isset($passwordRequestedAt['min'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_PASSWORD_REQUESTED_AT, $passwordRequestedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($passwordRequestedAt['max'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_PASSWORD_REQUESTED_AT, $passwordRequestedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_PASSWORD_REQUESTED_AT, $passwordRequestedAt, $comparison);
    }

    /**
     * Filter the query on the confirmation_token column
     *
     * Example usage:
     * <code>
     * $query->filterByConfirmationToken('fooValue');   // WHERE confirmation_token = 'fooValue'
     * $query->filterByConfirmationToken('%fooValue%', Criteria::LIKE); // WHERE confirmation_token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $confirmationToken The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByConfirmationToken($confirmationToken = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($confirmationToken)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_CONFIRMATION_TOKEN, $confirmationToken, $comparison);
    }

    /**
     * Filter the query on the last_login column
     *
     * Example usage:
     * <code>
     * $query->filterByLastLogin('2011-03-14'); // WHERE last_login = '2011-03-14'
     * $query->filterByLastLogin('now'); // WHERE last_login = '2011-03-14'
     * $query->filterByLastLogin(array('max' => 'yesterday')); // WHERE last_login > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastLogin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByLastLogin($lastLogin = null, $comparison = null)
    {
        if (is_array($lastLogin)) {
            $useMinMax = false;
            if (isset($lastLogin['min'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_LAST_LOGIN, $lastLogin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastLogin['max'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_LAST_LOGIN, $lastLogin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_LAST_LOGIN, $lastLogin, $comparison);
    }

    /**
     * Filter the query on the locked column
     *
     * Example usage:
     * <code>
     * $query->filterByLocked(true); // WHERE locked = true
     * $query->filterByLocked('yes'); // WHERE locked = true
     * </code>
     *
     * @param     boolean|string $locked The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByLocked($locked = null, $comparison = null)
    {
        if (is_string($locked)) {
            $locked = in_array(strtolower($locked), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_LOCKED, $locked, $comparison);
    }

    /**
     * Filter the query on the locked_at column
     *
     * Example usage:
     * <code>
     * $query->filterByLockedAt('2011-03-14'); // WHERE locked_at = '2011-03-14'
     * $query->filterByLockedAt('now'); // WHERE locked_at = '2011-03-14'
     * $query->filterByLockedAt(array('max' => 'yesterday')); // WHERE locked_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $lockedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByLockedAt($lockedAt = null, $comparison = null)
    {
        if (is_array($lockedAt)) {
            $useMinMax = false;
            if (isset($lockedAt['min'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_LOCKED_AT, $lockedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lockedAt['max'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_LOCKED_AT, $lockedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_LOCKED_AT, $lockedAt, $comparison);
    }

    /**
     * Filter the query on the expired column
     *
     * Example usage:
     * <code>
     * $query->filterByExpired(true); // WHERE expired = true
     * $query->filterByExpired('yes'); // WHERE expired = true
     * </code>
     *
     * @param     boolean|string $expired The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByExpired($expired = null, $comparison = null)
    {
        if (is_string($expired)) {
            $expired = in_array(strtolower($expired), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_EXPIRED, $expired, $comparison);
    }

    /**
     * Filter the query on the expired_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiredAt('2011-03-14'); // WHERE expired_at = '2011-03-14'
     * $query->filterByExpiredAt('now'); // WHERE expired_at = '2011-03-14'
     * $query->filterByExpiredAt(array('max' => 'yesterday')); // WHERE expired_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiredAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByExpiredAt($expiredAt = null, $comparison = null)
    {
        if (is_array($expiredAt)) {
            $useMinMax = false;
            if (isset($expiredAt['min'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_EXPIRED_AT, $expiredAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiredAt['max'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_EXPIRED_AT, $expiredAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_EXPIRED_AT, $expiredAt, $comparison);
    }

    /**
     * Filter the query on the role_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRoleId(1234); // WHERE role_id = 1234
     * $query->filterByRoleId(array(12, 34)); // WHERE role_id IN (12, 34)
     * $query->filterByRoleId(array('min' => 12)); // WHERE role_id > 12
     * </code>
     *
     * @param     mixed $roleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByRoleId($roleId = null, $comparison = null)
    {
        if (is_array($roleId)) {
            $useMinMax = false;
            if (isset($roleId['min'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_ROLE_ID, $roleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($roleId['max'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_ROLE_ID, $roleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_ROLE_ID, $roleId, $comparison);
    }

    /**
     * Filter the query on the privacy column
     *
     * Example usage:
     * <code>
     * $query->filterByPrivacy(true); // WHERE privacy = true
     * $query->filterByPrivacy('yes'); // WHERE privacy = true
     * </code>
     *
     * @param     boolean|string $privacy The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByPrivacy($privacy = null, $comparison = null)
    {
        if (is_string($privacy)) {
            $privacy = in_array(strtolower($privacy), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_PRIVACY, $privacy, $comparison);
    }

    /**
     * Filter the query on the privacy_at column
     *
     * Example usage:
     * <code>
     * $query->filterByPrivacyAt('2011-03-14'); // WHERE privacy_at = '2011-03-14'
     * $query->filterByPrivacyAt('now'); // WHERE privacy_at = '2011-03-14'
     * $query->filterByPrivacyAt(array('max' => 'yesterday')); // WHERE privacy_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $privacyAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByPrivacyAt($privacyAt = null, $comparison = null)
    {
        if (is_array($privacyAt)) {
            $useMinMax = false;
            if (isset($privacyAt['min'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_PRIVACY_AT, $privacyAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($privacyAt['max'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_PRIVACY_AT, $privacyAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_PRIVACY_AT, $privacyAt, $comparison);
    }

    /**
     * Filter the query on the terms column
     *
     * Example usage:
     * <code>
     * $query->filterByTerms(true); // WHERE terms = true
     * $query->filterByTerms('yes'); // WHERE terms = true
     * </code>
     *
     * @param     boolean|string $terms The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByTerms($terms = null, $comparison = null)
    {
        if (is_string($terms)) {
            $terms = in_array(strtolower($terms), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_TERMS, $terms, $comparison);
    }

    /**
     * Filter the query on the terms_at column
     *
     * Example usage:
     * <code>
     * $query->filterByTermsAt('2011-03-14'); // WHERE terms_at = '2011-03-14'
     * $query->filterByTermsAt('now'); // WHERE terms_at = '2011-03-14'
     * $query->filterByTermsAt(array('max' => 'yesterday')); // WHERE terms_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $termsAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByTermsAt($termsAt = null, $comparison = null)
    {
        if (is_array($termsAt)) {
            $useMinMax = false;
            if (isset($termsAt['min'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_TERMS_AT, $termsAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($termsAt['max'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_TERMS_AT, $termsAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_TERMS_AT, $termsAt, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(UserBackendTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Post object
     *
     * @param \Database\HubPlus\Post|ObjectCollection $post the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByPost($post, $comparison = null)
    {
        if ($post instanceof \Database\HubPlus\Post) {
            return $this
                ->addUsingAlias(UserBackendTableMap::COL_ID, $post->getAuthorId(), $comparison);
        } elseif ($post instanceof ObjectCollection) {
            return $this
                ->usePostQuery()
                ->filterByPrimaryKeys($post->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPost() only accepts arguments of type \Database\HubPlus\Post or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Post relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function joinPost($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Post');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Post');
        }

        return $this;
    }

    /**
     * Use the Post relation Post object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostQuery A secondary query class using the current class as primary query
     */
    public function usePostQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPost($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Post', '\Database\HubPlus\PostQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Section object
     *
     * @param \Database\HubPlus\Section|ObjectCollection $section the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterBySection($section, $comparison = null)
    {
        if ($section instanceof \Database\HubPlus\Section) {
            return $this
                ->addUsingAlias(UserBackendTableMap::COL_ID, $section->getAuthorId(), $comparison);
        } elseif ($section instanceof ObjectCollection) {
            return $this
                ->useSectionQuery()
                ->filterByPrimaryKeys($section->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySection() only accepts arguments of type \Database\HubPlus\Section or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Section relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function joinSection($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Section');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Section');
        }

        return $this;
    }

    /**
     * Use the Section relation Section object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionQuery A secondary query class using the current class as primary query
     */
    public function useSectionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSection($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Section', '\Database\HubPlus\SectionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\UserApplication object
     *
     * @param \Database\HubPlus\UserApplication|ObjectCollection $userApplication the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserBackendQuery The current query, for fluid interface
     */
    public function filterByUserApplication($userApplication, $comparison = null)
    {
        if ($userApplication instanceof \Database\HubPlus\UserApplication) {
            return $this
                ->addUsingAlias(UserBackendTableMap::COL_ID, $userApplication->getUserId(), $comparison);
        } elseif ($userApplication instanceof ObjectCollection) {
            return $this
                ->useUserApplicationQuery()
                ->filterByPrimaryKeys($userApplication->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserApplication() only accepts arguments of type \Database\HubPlus\UserApplication or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserApplication relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function joinUserApplication($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserApplication');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserApplication');
        }

        return $this;
    }

    /**
     * Use the UserApplication relation UserApplication object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\UserApplicationQuery A secondary query class using the current class as primary query
     */
    public function useUserApplicationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserApplication($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserApplication', '\Database\HubPlus\UserApplicationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserBackend $userBackend Object to remove from the list of results
     *
     * @return $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function prune($userBackend = null)
    {
        if ($userBackend) {
            $this->addUsingAlias(UserBackendTableMap::COL_ID, $userBackend->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the user_backend table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserBackendTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserBackendTableMap::clearInstancePool();
            UserBackendTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserBackendTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserBackendTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserBackendTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserBackendTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(UserBackendTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(UserBackendTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(UserBackendTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(UserBackendTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(UserBackendTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildUserBackendQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(UserBackendTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildUserBackendArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserBackendTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // UserBackendQuery
