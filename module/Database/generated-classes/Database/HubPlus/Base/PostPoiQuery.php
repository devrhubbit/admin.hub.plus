<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\PostPoi as ChildPostPoi;
use Database\HubPlus\PostPoiArchive as ChildPostPoiArchive;
use Database\HubPlus\PostPoiQuery as ChildPostPoiQuery;
use Database\HubPlus\Map\PostPoiTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'post_poi' table.
 *
 *
 *
 * @method     ChildPostPoiQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPostPoiQuery orderByPostId($order = Criteria::ASC) Order by the post_id column
 * @method     ChildPostPoiQuery orderByIdentifier($order = Criteria::ASC) Order by the identifier column
 * @method     ChildPostPoiQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildPostPoiQuery orderByLat($order = Criteria::ASC) Order by the lat column
 * @method     ChildPostPoiQuery orderByLng($order = Criteria::ASC) Order by the lng column
 * @method     ChildPostPoiQuery orderByRadius($order = Criteria::ASC) Order by the radius column
 * @method     ChildPostPoiQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildPostPoiQuery orderByMajor($order = Criteria::ASC) Order by the major column
 * @method     ChildPostPoiQuery orderByMinor($order = Criteria::ASC) Order by the minor column
 * @method     ChildPostPoiQuery orderByDistance($order = Criteria::ASC) Order by the distance column
 * @method     ChildPostPoiQuery orderByPin($order = Criteria::ASC) Order by the pin column
 * @method     ChildPostPoiQuery orderByPoiNotification($order = Criteria::ASC) Order by the poi_notification column
 * @method     ChildPostPoiQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildPostPoiQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPostPoiQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPostPoiQuery groupById() Group by the id column
 * @method     ChildPostPoiQuery groupByPostId() Group by the post_id column
 * @method     ChildPostPoiQuery groupByIdentifier() Group by the identifier column
 * @method     ChildPostPoiQuery groupByType() Group by the type column
 * @method     ChildPostPoiQuery groupByLat() Group by the lat column
 * @method     ChildPostPoiQuery groupByLng() Group by the lng column
 * @method     ChildPostPoiQuery groupByRadius() Group by the radius column
 * @method     ChildPostPoiQuery groupByUuid() Group by the uuid column
 * @method     ChildPostPoiQuery groupByMajor() Group by the major column
 * @method     ChildPostPoiQuery groupByMinor() Group by the minor column
 * @method     ChildPostPoiQuery groupByDistance() Group by the distance column
 * @method     ChildPostPoiQuery groupByPin() Group by the pin column
 * @method     ChildPostPoiQuery groupByPoiNotification() Group by the poi_notification column
 * @method     ChildPostPoiQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildPostPoiQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPostPoiQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPostPoiQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPostPoiQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPostPoiQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPostPoiQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPostPoiQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPostPoiQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPostPoiQuery leftJoinPost($relationAlias = null) Adds a LEFT JOIN clause to the query using the Post relation
 * @method     ChildPostPoiQuery rightJoinPost($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Post relation
 * @method     ChildPostPoiQuery innerJoinPost($relationAlias = null) Adds a INNER JOIN clause to the query using the Post relation
 *
 * @method     ChildPostPoiQuery joinWithPost($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Post relation
 *
 * @method     ChildPostPoiQuery leftJoinWithPost() Adds a LEFT JOIN clause and with to the query using the Post relation
 * @method     ChildPostPoiQuery rightJoinWithPost() Adds a RIGHT JOIN clause and with to the query using the Post relation
 * @method     ChildPostPoiQuery innerJoinWithPost() Adds a INNER JOIN clause and with to the query using the Post relation
 *
 * @method     \Database\HubPlus\PostQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPostPoi findOne(ConnectionInterface $con = null) Return the first ChildPostPoi matching the query
 * @method     ChildPostPoi findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPostPoi matching the query, or a new ChildPostPoi object populated from the query conditions when no match is found
 *
 * @method     ChildPostPoi findOneById(int $id) Return the first ChildPostPoi filtered by the id column
 * @method     ChildPostPoi findOneByPostId(int $post_id) Return the first ChildPostPoi filtered by the post_id column
 * @method     ChildPostPoi findOneByIdentifier(string $identifier) Return the first ChildPostPoi filtered by the identifier column
 * @method     ChildPostPoi findOneByType(string $type) Return the first ChildPostPoi filtered by the type column
 * @method     ChildPostPoi findOneByLat(double $lat) Return the first ChildPostPoi filtered by the lat column
 * @method     ChildPostPoi findOneByLng(double $lng) Return the first ChildPostPoi filtered by the lng column
 * @method     ChildPostPoi findOneByRadius(int $radius) Return the first ChildPostPoi filtered by the radius column
 * @method     ChildPostPoi findOneByUuid(string $uuid) Return the first ChildPostPoi filtered by the uuid column
 * @method     ChildPostPoi findOneByMajor(int $major) Return the first ChildPostPoi filtered by the major column
 * @method     ChildPostPoi findOneByMinor(int $minor) Return the first ChildPostPoi filtered by the minor column
 * @method     ChildPostPoi findOneByDistance(string $distance) Return the first ChildPostPoi filtered by the distance column
 * @method     ChildPostPoi findOneByPin(string $pin) Return the first ChildPostPoi filtered by the pin column
 * @method     ChildPostPoi findOneByPoiNotification(boolean $poi_notification) Return the first ChildPostPoi filtered by the poi_notification column
 * @method     ChildPostPoi findOneByDeletedAt(string $deleted_at) Return the first ChildPostPoi filtered by the deleted_at column
 * @method     ChildPostPoi findOneByCreatedAt(string $created_at) Return the first ChildPostPoi filtered by the created_at column
 * @method     ChildPostPoi findOneByUpdatedAt(string $updated_at) Return the first ChildPostPoi filtered by the updated_at column *

 * @method     ChildPostPoi requirePk($key, ConnectionInterface $con = null) Return the ChildPostPoi by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOne(ConnectionInterface $con = null) Return the first ChildPostPoi matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPostPoi requireOneById(int $id) Return the first ChildPostPoi filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByPostId(int $post_id) Return the first ChildPostPoi filtered by the post_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByIdentifier(string $identifier) Return the first ChildPostPoi filtered by the identifier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByType(string $type) Return the first ChildPostPoi filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByLat(double $lat) Return the first ChildPostPoi filtered by the lat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByLng(double $lng) Return the first ChildPostPoi filtered by the lng column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByRadius(int $radius) Return the first ChildPostPoi filtered by the radius column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByUuid(string $uuid) Return the first ChildPostPoi filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByMajor(int $major) Return the first ChildPostPoi filtered by the major column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByMinor(int $minor) Return the first ChildPostPoi filtered by the minor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByDistance(string $distance) Return the first ChildPostPoi filtered by the distance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByPin(string $pin) Return the first ChildPostPoi filtered by the pin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByPoiNotification(boolean $poi_notification) Return the first ChildPostPoi filtered by the poi_notification column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByDeletedAt(string $deleted_at) Return the first ChildPostPoi filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByCreatedAt(string $created_at) Return the first ChildPostPoi filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostPoi requireOneByUpdatedAt(string $updated_at) Return the first ChildPostPoi filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPostPoi[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPostPoi objects based on current ModelCriteria
 * @method     ChildPostPoi[]|ObjectCollection findById(int $id) Return ChildPostPoi objects filtered by the id column
 * @method     ChildPostPoi[]|ObjectCollection findByPostId(int $post_id) Return ChildPostPoi objects filtered by the post_id column
 * @method     ChildPostPoi[]|ObjectCollection findByIdentifier(string $identifier) Return ChildPostPoi objects filtered by the identifier column
 * @method     ChildPostPoi[]|ObjectCollection findByType(string $type) Return ChildPostPoi objects filtered by the type column
 * @method     ChildPostPoi[]|ObjectCollection findByLat(double $lat) Return ChildPostPoi objects filtered by the lat column
 * @method     ChildPostPoi[]|ObjectCollection findByLng(double $lng) Return ChildPostPoi objects filtered by the lng column
 * @method     ChildPostPoi[]|ObjectCollection findByRadius(int $radius) Return ChildPostPoi objects filtered by the radius column
 * @method     ChildPostPoi[]|ObjectCollection findByUuid(string $uuid) Return ChildPostPoi objects filtered by the uuid column
 * @method     ChildPostPoi[]|ObjectCollection findByMajor(int $major) Return ChildPostPoi objects filtered by the major column
 * @method     ChildPostPoi[]|ObjectCollection findByMinor(int $minor) Return ChildPostPoi objects filtered by the minor column
 * @method     ChildPostPoi[]|ObjectCollection findByDistance(string $distance) Return ChildPostPoi objects filtered by the distance column
 * @method     ChildPostPoi[]|ObjectCollection findByPin(string $pin) Return ChildPostPoi objects filtered by the pin column
 * @method     ChildPostPoi[]|ObjectCollection findByPoiNotification(boolean $poi_notification) Return ChildPostPoi objects filtered by the poi_notification column
 * @method     ChildPostPoi[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildPostPoi objects filtered by the deleted_at column
 * @method     ChildPostPoi[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPostPoi objects filtered by the created_at column
 * @method     ChildPostPoi[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPostPoi objects filtered by the updated_at column
 * @method     ChildPostPoi[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PostPoiQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\PostPoiQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\PostPoi', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPostPoiQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPostPoiQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPostPoiQuery) {
            return $criteria;
        }
        $query = new ChildPostPoiQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPostPoi|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PostPoiTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PostPoiTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPostPoi A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, post_id, identifier, type, lat, lng, radius, uuid, major, minor, distance, pin, poi_notification, deleted_at, created_at, updated_at FROM post_poi WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPostPoi $obj */
            $obj = new ChildPostPoi();
            $obj->hydrate($row);
            PostPoiTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPostPoi|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PostPoiTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PostPoiTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the post_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPostId(1234); // WHERE post_id = 1234
     * $query->filterByPostId(array(12, 34)); // WHERE post_id IN (12, 34)
     * $query->filterByPostId(array('min' => 12)); // WHERE post_id > 12
     * </code>
     *
     * @see       filterByPost()
     *
     * @param     mixed $postId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByPostId($postId = null, $comparison = null)
    {
        if (is_array($postId)) {
            $useMinMax = false;
            if (isset($postId['min'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_POST_ID, $postId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($postId['max'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_POST_ID, $postId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_POST_ID, $postId, $comparison);
    }

    /**
     * Filter the query on the identifier column
     *
     * Example usage:
     * <code>
     * $query->filterByIdentifier('fooValue');   // WHERE identifier = 'fooValue'
     * $query->filterByIdentifier('%fooValue%', Criteria::LIKE); // WHERE identifier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $identifier The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByIdentifier($identifier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($identifier)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_IDENTIFIER, $identifier, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the lat column
     *
     * Example usage:
     * <code>
     * $query->filterByLat(1234); // WHERE lat = 1234
     * $query->filterByLat(array(12, 34)); // WHERE lat IN (12, 34)
     * $query->filterByLat(array('min' => 12)); // WHERE lat > 12
     * </code>
     *
     * @param     mixed $lat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByLat($lat = null, $comparison = null)
    {
        if (is_array($lat)) {
            $useMinMax = false;
            if (isset($lat['min'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_LAT, $lat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lat['max'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_LAT, $lat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_LAT, $lat, $comparison);
    }

    /**
     * Filter the query on the lng column
     *
     * Example usage:
     * <code>
     * $query->filterByLng(1234); // WHERE lng = 1234
     * $query->filterByLng(array(12, 34)); // WHERE lng IN (12, 34)
     * $query->filterByLng(array('min' => 12)); // WHERE lng > 12
     * </code>
     *
     * @param     mixed $lng The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByLng($lng = null, $comparison = null)
    {
        if (is_array($lng)) {
            $useMinMax = false;
            if (isset($lng['min'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_LNG, $lng['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lng['max'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_LNG, $lng['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_LNG, $lng, $comparison);
    }

    /**
     * Filter the query on the radius column
     *
     * Example usage:
     * <code>
     * $query->filterByRadius(1234); // WHERE radius = 1234
     * $query->filterByRadius(array(12, 34)); // WHERE radius IN (12, 34)
     * $query->filterByRadius(array('min' => 12)); // WHERE radius > 12
     * </code>
     *
     * @param     mixed $radius The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByRadius($radius = null, $comparison = null)
    {
        if (is_array($radius)) {
            $useMinMax = false;
            if (isset($radius['min'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_RADIUS, $radius['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($radius['max'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_RADIUS, $radius['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_RADIUS, $radius, $comparison);
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uuid The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_UUID, $uuid, $comparison);
    }

    /**
     * Filter the query on the major column
     *
     * Example usage:
     * <code>
     * $query->filterByMajor(1234); // WHERE major = 1234
     * $query->filterByMajor(array(12, 34)); // WHERE major IN (12, 34)
     * $query->filterByMajor(array('min' => 12)); // WHERE major > 12
     * </code>
     *
     * @param     mixed $major The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByMajor($major = null, $comparison = null)
    {
        if (is_array($major)) {
            $useMinMax = false;
            if (isset($major['min'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_MAJOR, $major['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($major['max'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_MAJOR, $major['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_MAJOR, $major, $comparison);
    }

    /**
     * Filter the query on the minor column
     *
     * Example usage:
     * <code>
     * $query->filterByMinor(1234); // WHERE minor = 1234
     * $query->filterByMinor(array(12, 34)); // WHERE minor IN (12, 34)
     * $query->filterByMinor(array('min' => 12)); // WHERE minor > 12
     * </code>
     *
     * @param     mixed $minor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByMinor($minor = null, $comparison = null)
    {
        if (is_array($minor)) {
            $useMinMax = false;
            if (isset($minor['min'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_MINOR, $minor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($minor['max'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_MINOR, $minor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_MINOR, $minor, $comparison);
    }

    /**
     * Filter the query on the distance column
     *
     * Example usage:
     * <code>
     * $query->filterByDistance('fooValue');   // WHERE distance = 'fooValue'
     * $query->filterByDistance('%fooValue%', Criteria::LIKE); // WHERE distance LIKE '%fooValue%'
     * </code>
     *
     * @param     string $distance The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByDistance($distance = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($distance)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_DISTANCE, $distance, $comparison);
    }

    /**
     * Filter the query on the pin column
     *
     * Example usage:
     * <code>
     * $query->filterByPin('fooValue');   // WHERE pin = 'fooValue'
     * $query->filterByPin('%fooValue%', Criteria::LIKE); // WHERE pin LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pin The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByPin($pin = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pin)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_PIN, $pin, $comparison);
    }

    /**
     * Filter the query on the poi_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByPoiNotification(true); // WHERE poi_notification = true
     * $query->filterByPoiNotification('yes'); // WHERE poi_notification = true
     * </code>
     *
     * @param     boolean|string $poiNotification The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByPoiNotification($poiNotification = null, $comparison = null)
    {
        if (is_string($poiNotification)) {
            $poiNotification = in_array(strtolower($poiNotification), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_POI_NOTIFICATION, $poiNotification, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PostPoiTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostPoiTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Post object
     *
     * @param \Database\HubPlus\Post|ObjectCollection $post The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPostPoiQuery The current query, for fluid interface
     */
    public function filterByPost($post, $comparison = null)
    {
        if ($post instanceof \Database\HubPlus\Post) {
            return $this
                ->addUsingAlias(PostPoiTableMap::COL_POST_ID, $post->getId(), $comparison);
        } elseif ($post instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PostPoiTableMap::COL_POST_ID, $post->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPost() only accepts arguments of type \Database\HubPlus\Post or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Post relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function joinPost($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Post');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Post');
        }

        return $this;
    }

    /**
     * Use the Post relation Post object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostQuery A secondary query class using the current class as primary query
     */
    public function usePostQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPost($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Post', '\Database\HubPlus\PostQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPostPoi $postPoi Object to remove from the list of results
     *
     * @return $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function prune($postPoi = null)
    {
        if ($postPoi) {
            $this->addUsingAlias(PostPoiTableMap::COL_ID, $postPoi->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the post_poi table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostPoiTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PostPoiTableMap::clearInstancePool();
            PostPoiTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostPoiTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PostPoiTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PostPoiTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PostPoiTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(PostPoiTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(PostPoiTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(PostPoiTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(PostPoiTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(PostPoiTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildPostPoiQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(PostPoiTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildPostPoiArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostPoiTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // PostPoiQuery
