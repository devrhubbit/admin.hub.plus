<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\ApplicationArchive as ChildApplicationArchive;
use Database\HubPlus\ApplicationArchiveQuery as ChildApplicationArchiveQuery;
use Database\HubPlus\Map\ApplicationArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'application_archive' table.
 *
 *
 *
 * @method     ChildApplicationArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildApplicationArchiveQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildApplicationArchiveQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 * @method     ChildApplicationArchiveQuery orderByBundle($order = Criteria::ASC) Order by the bundle column
 * @method     ChildApplicationArchiveQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildApplicationArchiveQuery orderByAppKey($order = Criteria::ASC) Order by the app_key column
 * @method     ChildApplicationArchiveQuery orderByDsn($order = Criteria::ASC) Order by the dsn column
 * @method     ChildApplicationArchiveQuery orderByDbHost($order = Criteria::ASC) Order by the db_host column
 * @method     ChildApplicationArchiveQuery orderByDbName($order = Criteria::ASC) Order by the db_name column
 * @method     ChildApplicationArchiveQuery orderByDbUser($order = Criteria::ASC) Order by the db_user column
 * @method     ChildApplicationArchiveQuery orderByDbPwd($order = Criteria::ASC) Order by the db_pwd column
 * @method     ChildApplicationArchiveQuery orderByApiVersion($order = Criteria::ASC) Order by the api_version column
 * @method     ChildApplicationArchiveQuery orderByPackId($order = Criteria::ASC) Order by the pack_id column
 * @method     ChildApplicationArchiveQuery orderByCategoryId($order = Criteria::ASC) Order by the category_id column
 * @method     ChildApplicationArchiveQuery orderByTemplateId($order = Criteria::ASC) Order by the template_id column
 * @method     ChildApplicationArchiveQuery orderByPushNotification($order = Criteria::ASC) Order by the push_notification column
 * @method     ChildApplicationArchiveQuery orderByMaxNotification($order = Criteria::ASC) Order by the max_notification column
 * @method     ChildApplicationArchiveQuery orderByMaxAdviceHour($order = Criteria::ASC) Order by the max_advice_hour column
 * @method     ChildApplicationArchiveQuery orderByAdviceHourDone($order = Criteria::ASC) Order by the advice_hour_done column
 * @method     ChildApplicationArchiveQuery orderByMaxContentInsert($order = Criteria::ASC) Order by the max_content_insert column
 * @method     ChildApplicationArchiveQuery orderByIcon($order = Criteria::ASC) Order by the icon column
 * @method     ChildApplicationArchiveQuery orderBySplashScreen($order = Criteria::ASC) Order by the splash_screen column
 * @method     ChildApplicationArchiveQuery orderByBackgroundColor($order = Criteria::ASC) Order by the background_color column
 * @method     ChildApplicationArchiveQuery orderByAppLogo($order = Criteria::ASC) Order by the app_logo column
 * @method     ChildApplicationArchiveQuery orderByLayout($order = Criteria::ASC) Order by the layout column
 * @method     ChildApplicationArchiveQuery orderByWhiteLabel($order = Criteria::ASC) Order by the white_label column
 * @method     ChildApplicationArchiveQuery orderByDemo($order = Criteria::ASC) Order by the demo column
 * @method     ChildApplicationArchiveQuery orderByFacebookToken($order = Criteria::ASC) Order by the facebook_token column
 * @method     ChildApplicationArchiveQuery orderByAppleId($order = Criteria::ASC) Order by the apple_id column
 * @method     ChildApplicationArchiveQuery orderByAppleIdPassword($order = Criteria::ASC) Order by the apple_id_password column
 * @method     ChildApplicationArchiveQuery orderByAppleStoreAppLink($order = Criteria::ASC) Order by the apple_store_app_link column
 * @method     ChildApplicationArchiveQuery orderByPlayStoreId($order = Criteria::ASC) Order by the play_store_id column
 * @method     ChildApplicationArchiveQuery orderByPlayStoreIdPassword($order = Criteria::ASC) Order by the play_store_id_password column
 * @method     ChildApplicationArchiveQuery orderByPlayStoreAppLink($order = Criteria::ASC) Order by the play_store_app_link column
 * @method     ChildApplicationArchiveQuery orderByControllerUri($order = Criteria::ASC) Order by the controller_uri column
 * @method     ChildApplicationArchiveQuery orderByGoogleAnalyticsUa($order = Criteria::ASC) Order by the google_analytics_ua column
 * @method     ChildApplicationArchiveQuery orderByPublished($order = Criteria::ASC) Order by the published column
 * @method     ChildApplicationArchiveQuery orderByExpiredAt($order = Criteria::ASC) Order by the expired_at column
 * @method     ChildApplicationArchiveQuery orderByMainContentsLanguage($order = Criteria::ASC) Order by the main_contents_language column
 * @method     ChildApplicationArchiveQuery orderByOtherContentsLanguages($order = Criteria::ASC) Order by the other_contents_languages column
 * @method     ChildApplicationArchiveQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildApplicationArchiveQuery orderBySystemIcons($order = Criteria::ASC) Order by the system_icons column
 * @method     ChildApplicationArchiveQuery orderByToolbar($order = Criteria::ASC) Order by the toolbar column
 * @method     ChildApplicationArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildApplicationArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildApplicationArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildApplicationArchiveQuery groupById() Group by the id column
 * @method     ChildApplicationArchiveQuery groupByTitle() Group by the title column
 * @method     ChildApplicationArchiveQuery groupBySlug() Group by the slug column
 * @method     ChildApplicationArchiveQuery groupByBundle() Group by the bundle column
 * @method     ChildApplicationArchiveQuery groupByDescription() Group by the description column
 * @method     ChildApplicationArchiveQuery groupByAppKey() Group by the app_key column
 * @method     ChildApplicationArchiveQuery groupByDsn() Group by the dsn column
 * @method     ChildApplicationArchiveQuery groupByDbHost() Group by the db_host column
 * @method     ChildApplicationArchiveQuery groupByDbName() Group by the db_name column
 * @method     ChildApplicationArchiveQuery groupByDbUser() Group by the db_user column
 * @method     ChildApplicationArchiveQuery groupByDbPwd() Group by the db_pwd column
 * @method     ChildApplicationArchiveQuery groupByApiVersion() Group by the api_version column
 * @method     ChildApplicationArchiveQuery groupByPackId() Group by the pack_id column
 * @method     ChildApplicationArchiveQuery groupByCategoryId() Group by the category_id column
 * @method     ChildApplicationArchiveQuery groupByTemplateId() Group by the template_id column
 * @method     ChildApplicationArchiveQuery groupByPushNotification() Group by the push_notification column
 * @method     ChildApplicationArchiveQuery groupByMaxNotification() Group by the max_notification column
 * @method     ChildApplicationArchiveQuery groupByMaxAdviceHour() Group by the max_advice_hour column
 * @method     ChildApplicationArchiveQuery groupByAdviceHourDone() Group by the advice_hour_done column
 * @method     ChildApplicationArchiveQuery groupByMaxContentInsert() Group by the max_content_insert column
 * @method     ChildApplicationArchiveQuery groupByIcon() Group by the icon column
 * @method     ChildApplicationArchiveQuery groupBySplashScreen() Group by the splash_screen column
 * @method     ChildApplicationArchiveQuery groupByBackgroundColor() Group by the background_color column
 * @method     ChildApplicationArchiveQuery groupByAppLogo() Group by the app_logo column
 * @method     ChildApplicationArchiveQuery groupByLayout() Group by the layout column
 * @method     ChildApplicationArchiveQuery groupByWhiteLabel() Group by the white_label column
 * @method     ChildApplicationArchiveQuery groupByDemo() Group by the demo column
 * @method     ChildApplicationArchiveQuery groupByFacebookToken() Group by the facebook_token column
 * @method     ChildApplicationArchiveQuery groupByAppleId() Group by the apple_id column
 * @method     ChildApplicationArchiveQuery groupByAppleIdPassword() Group by the apple_id_password column
 * @method     ChildApplicationArchiveQuery groupByAppleStoreAppLink() Group by the apple_store_app_link column
 * @method     ChildApplicationArchiveQuery groupByPlayStoreId() Group by the play_store_id column
 * @method     ChildApplicationArchiveQuery groupByPlayStoreIdPassword() Group by the play_store_id_password column
 * @method     ChildApplicationArchiveQuery groupByPlayStoreAppLink() Group by the play_store_app_link column
 * @method     ChildApplicationArchiveQuery groupByControllerUri() Group by the controller_uri column
 * @method     ChildApplicationArchiveQuery groupByGoogleAnalyticsUa() Group by the google_analytics_ua column
 * @method     ChildApplicationArchiveQuery groupByPublished() Group by the published column
 * @method     ChildApplicationArchiveQuery groupByExpiredAt() Group by the expired_at column
 * @method     ChildApplicationArchiveQuery groupByMainContentsLanguage() Group by the main_contents_language column
 * @method     ChildApplicationArchiveQuery groupByOtherContentsLanguages() Group by the other_contents_languages column
 * @method     ChildApplicationArchiveQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildApplicationArchiveQuery groupBySystemIcons() Group by the system_icons column
 * @method     ChildApplicationArchiveQuery groupByToolbar() Group by the toolbar column
 * @method     ChildApplicationArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildApplicationArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildApplicationArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildApplicationArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApplicationArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApplicationArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApplicationArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApplicationArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApplicationArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApplicationArchive findOne(ConnectionInterface $con = null) Return the first ChildApplicationArchive matching the query
 * @method     ChildApplicationArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApplicationArchive matching the query, or a new ChildApplicationArchive object populated from the query conditions when no match is found
 *
 * @method     ChildApplicationArchive findOneById(int $id) Return the first ChildApplicationArchive filtered by the id column
 * @method     ChildApplicationArchive findOneByTitle(string $title) Return the first ChildApplicationArchive filtered by the title column
 * @method     ChildApplicationArchive findOneBySlug(string $slug) Return the first ChildApplicationArchive filtered by the slug column
 * @method     ChildApplicationArchive findOneByBundle(string $bundle) Return the first ChildApplicationArchive filtered by the bundle column
 * @method     ChildApplicationArchive findOneByDescription(string $description) Return the first ChildApplicationArchive filtered by the description column
 * @method     ChildApplicationArchive findOneByAppKey(string $app_key) Return the first ChildApplicationArchive filtered by the app_key column
 * @method     ChildApplicationArchive findOneByDsn(string $dsn) Return the first ChildApplicationArchive filtered by the dsn column
 * @method     ChildApplicationArchive findOneByDbHost(string $db_host) Return the first ChildApplicationArchive filtered by the db_host column
 * @method     ChildApplicationArchive findOneByDbName(string $db_name) Return the first ChildApplicationArchive filtered by the db_name column
 * @method     ChildApplicationArchive findOneByDbUser(string $db_user) Return the first ChildApplicationArchive filtered by the db_user column
 * @method     ChildApplicationArchive findOneByDbPwd(string $db_pwd) Return the first ChildApplicationArchive filtered by the db_pwd column
 * @method     ChildApplicationArchive findOneByApiVersion(string $api_version) Return the first ChildApplicationArchive filtered by the api_version column
 * @method     ChildApplicationArchive findOneByPackId(int $pack_id) Return the first ChildApplicationArchive filtered by the pack_id column
 * @method     ChildApplicationArchive findOneByCategoryId(int $category_id) Return the first ChildApplicationArchive filtered by the category_id column
 * @method     ChildApplicationArchive findOneByTemplateId(int $template_id) Return the first ChildApplicationArchive filtered by the template_id column
 * @method     ChildApplicationArchive findOneByPushNotification(string $push_notification) Return the first ChildApplicationArchive filtered by the push_notification column
 * @method     ChildApplicationArchive findOneByMaxNotification(int $max_notification) Return the first ChildApplicationArchive filtered by the max_notification column
 * @method     ChildApplicationArchive findOneByMaxAdviceHour(int $max_advice_hour) Return the first ChildApplicationArchive filtered by the max_advice_hour column
 * @method     ChildApplicationArchive findOneByAdviceHourDone(int $advice_hour_done) Return the first ChildApplicationArchive filtered by the advice_hour_done column
 * @method     ChildApplicationArchive findOneByMaxContentInsert(int $max_content_insert) Return the first ChildApplicationArchive filtered by the max_content_insert column
 * @method     ChildApplicationArchive findOneByIcon(string $icon) Return the first ChildApplicationArchive filtered by the icon column
 * @method     ChildApplicationArchive findOneBySplashScreen(string $splash_screen) Return the first ChildApplicationArchive filtered by the splash_screen column
 * @method     ChildApplicationArchive findOneByBackgroundColor(string $background_color) Return the first ChildApplicationArchive filtered by the background_color column
 * @method     ChildApplicationArchive findOneByAppLogo(string $app_logo) Return the first ChildApplicationArchive filtered by the app_logo column
 * @method     ChildApplicationArchive findOneByLayout(string $layout) Return the first ChildApplicationArchive filtered by the layout column
 * @method     ChildApplicationArchive findOneByWhiteLabel(boolean $white_label) Return the first ChildApplicationArchive filtered by the white_label column
 * @method     ChildApplicationArchive findOneByDemo(boolean $demo) Return the first ChildApplicationArchive filtered by the demo column
 * @method     ChildApplicationArchive findOneByFacebookToken(string $facebook_token) Return the first ChildApplicationArchive filtered by the facebook_token column
 * @method     ChildApplicationArchive findOneByAppleId(string $apple_id) Return the first ChildApplicationArchive filtered by the apple_id column
 * @method     ChildApplicationArchive findOneByAppleIdPassword(string $apple_id_password) Return the first ChildApplicationArchive filtered by the apple_id_password column
 * @method     ChildApplicationArchive findOneByAppleStoreAppLink(string $apple_store_app_link) Return the first ChildApplicationArchive filtered by the apple_store_app_link column
 * @method     ChildApplicationArchive findOneByPlayStoreId(string $play_store_id) Return the first ChildApplicationArchive filtered by the play_store_id column
 * @method     ChildApplicationArchive findOneByPlayStoreIdPassword(string $play_store_id_password) Return the first ChildApplicationArchive filtered by the play_store_id_password column
 * @method     ChildApplicationArchive findOneByPlayStoreAppLink(string $play_store_app_link) Return the first ChildApplicationArchive filtered by the play_store_app_link column
 * @method     ChildApplicationArchive findOneByControllerUri(string $controller_uri) Return the first ChildApplicationArchive filtered by the controller_uri column
 * @method     ChildApplicationArchive findOneByGoogleAnalyticsUa(string $google_analytics_ua) Return the first ChildApplicationArchive filtered by the google_analytics_ua column
 * @method     ChildApplicationArchive findOneByPublished(int $published) Return the first ChildApplicationArchive filtered by the published column
 * @method     ChildApplicationArchive findOneByExpiredAt(string $expired_at) Return the first ChildApplicationArchive filtered by the expired_at column
 * @method     ChildApplicationArchive findOneByMainContentsLanguage(string $main_contents_language) Return the first ChildApplicationArchive filtered by the main_contents_language column
 * @method     ChildApplicationArchive findOneByOtherContentsLanguages(string $other_contents_languages) Return the first ChildApplicationArchive filtered by the other_contents_languages column
 * @method     ChildApplicationArchive findOneByDeletedAt(string $deleted_at) Return the first ChildApplicationArchive filtered by the deleted_at column
 * @method     ChildApplicationArchive findOneBySystemIcons(string $system_icons) Return the first ChildApplicationArchive filtered by the system_icons column
 * @method     ChildApplicationArchive findOneByToolbar(string $toolbar) Return the first ChildApplicationArchive filtered by the toolbar column
 * @method     ChildApplicationArchive findOneByCreatedAt(string $created_at) Return the first ChildApplicationArchive filtered by the created_at column
 * @method     ChildApplicationArchive findOneByUpdatedAt(string $updated_at) Return the first ChildApplicationArchive filtered by the updated_at column
 * @method     ChildApplicationArchive findOneByArchivedAt(string $archived_at) Return the first ChildApplicationArchive filtered by the archived_at column *

 * @method     ChildApplicationArchive requirePk($key, ConnectionInterface $con = null) Return the ChildApplicationArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOne(ConnectionInterface $con = null) Return the first ChildApplicationArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApplicationArchive requireOneById(int $id) Return the first ChildApplicationArchive filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByTitle(string $title) Return the first ChildApplicationArchive filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneBySlug(string $slug) Return the first ChildApplicationArchive filtered by the slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByBundle(string $bundle) Return the first ChildApplicationArchive filtered by the bundle column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByDescription(string $description) Return the first ChildApplicationArchive filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByAppKey(string $app_key) Return the first ChildApplicationArchive filtered by the app_key column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByDsn(string $dsn) Return the first ChildApplicationArchive filtered by the dsn column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByDbHost(string $db_host) Return the first ChildApplicationArchive filtered by the db_host column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByDbName(string $db_name) Return the first ChildApplicationArchive filtered by the db_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByDbUser(string $db_user) Return the first ChildApplicationArchive filtered by the db_user column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByDbPwd(string $db_pwd) Return the first ChildApplicationArchive filtered by the db_pwd column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByApiVersion(string $api_version) Return the first ChildApplicationArchive filtered by the api_version column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByPackId(int $pack_id) Return the first ChildApplicationArchive filtered by the pack_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByCategoryId(int $category_id) Return the first ChildApplicationArchive filtered by the category_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByTemplateId(int $template_id) Return the first ChildApplicationArchive filtered by the template_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByPushNotification(string $push_notification) Return the first ChildApplicationArchive filtered by the push_notification column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByMaxNotification(int $max_notification) Return the first ChildApplicationArchive filtered by the max_notification column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByMaxAdviceHour(int $max_advice_hour) Return the first ChildApplicationArchive filtered by the max_advice_hour column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByAdviceHourDone(int $advice_hour_done) Return the first ChildApplicationArchive filtered by the advice_hour_done column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByMaxContentInsert(int $max_content_insert) Return the first ChildApplicationArchive filtered by the max_content_insert column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByIcon(string $icon) Return the first ChildApplicationArchive filtered by the icon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneBySplashScreen(string $splash_screen) Return the first ChildApplicationArchive filtered by the splash_screen column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByBackgroundColor(string $background_color) Return the first ChildApplicationArchive filtered by the background_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByAppLogo(string $app_logo) Return the first ChildApplicationArchive filtered by the app_logo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByLayout(string $layout) Return the first ChildApplicationArchive filtered by the layout column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByWhiteLabel(boolean $white_label) Return the first ChildApplicationArchive filtered by the white_label column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByDemo(boolean $demo) Return the first ChildApplicationArchive filtered by the demo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByFacebookToken(string $facebook_token) Return the first ChildApplicationArchive filtered by the facebook_token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByAppleId(string $apple_id) Return the first ChildApplicationArchive filtered by the apple_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByAppleIdPassword(string $apple_id_password) Return the first ChildApplicationArchive filtered by the apple_id_password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByAppleStoreAppLink(string $apple_store_app_link) Return the first ChildApplicationArchive filtered by the apple_store_app_link column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByPlayStoreId(string $play_store_id) Return the first ChildApplicationArchive filtered by the play_store_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByPlayStoreIdPassword(string $play_store_id_password) Return the first ChildApplicationArchive filtered by the play_store_id_password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByPlayStoreAppLink(string $play_store_app_link) Return the first ChildApplicationArchive filtered by the play_store_app_link column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByControllerUri(string $controller_uri) Return the first ChildApplicationArchive filtered by the controller_uri column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByGoogleAnalyticsUa(string $google_analytics_ua) Return the first ChildApplicationArchive filtered by the google_analytics_ua column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByPublished(int $published) Return the first ChildApplicationArchive filtered by the published column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByExpiredAt(string $expired_at) Return the first ChildApplicationArchive filtered by the expired_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByMainContentsLanguage(string $main_contents_language) Return the first ChildApplicationArchive filtered by the main_contents_language column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByOtherContentsLanguages(string $other_contents_languages) Return the first ChildApplicationArchive filtered by the other_contents_languages column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByDeletedAt(string $deleted_at) Return the first ChildApplicationArchive filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneBySystemIcons(string $system_icons) Return the first ChildApplicationArchive filtered by the system_icons column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByToolbar(string $toolbar) Return the first ChildApplicationArchive filtered by the toolbar column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByCreatedAt(string $created_at) Return the first ChildApplicationArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildApplicationArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplicationArchive requireOneByArchivedAt(string $archived_at) Return the first ChildApplicationArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApplicationArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApplicationArchive objects based on current ModelCriteria
 * @method     ChildApplicationArchive[]|ObjectCollection findById(int $id) Return ChildApplicationArchive objects filtered by the id column
 * @method     ChildApplicationArchive[]|ObjectCollection findByTitle(string $title) Return ChildApplicationArchive objects filtered by the title column
 * @method     ChildApplicationArchive[]|ObjectCollection findBySlug(string $slug) Return ChildApplicationArchive objects filtered by the slug column
 * @method     ChildApplicationArchive[]|ObjectCollection findByBundle(string $bundle) Return ChildApplicationArchive objects filtered by the bundle column
 * @method     ChildApplicationArchive[]|ObjectCollection findByDescription(string $description) Return ChildApplicationArchive objects filtered by the description column
 * @method     ChildApplicationArchive[]|ObjectCollection findByAppKey(string $app_key) Return ChildApplicationArchive objects filtered by the app_key column
 * @method     ChildApplicationArchive[]|ObjectCollection findByDsn(string $dsn) Return ChildApplicationArchive objects filtered by the dsn column
 * @method     ChildApplicationArchive[]|ObjectCollection findByDbHost(string $db_host) Return ChildApplicationArchive objects filtered by the db_host column
 * @method     ChildApplicationArchive[]|ObjectCollection findByDbName(string $db_name) Return ChildApplicationArchive objects filtered by the db_name column
 * @method     ChildApplicationArchive[]|ObjectCollection findByDbUser(string $db_user) Return ChildApplicationArchive objects filtered by the db_user column
 * @method     ChildApplicationArchive[]|ObjectCollection findByDbPwd(string $db_pwd) Return ChildApplicationArchive objects filtered by the db_pwd column
 * @method     ChildApplicationArchive[]|ObjectCollection findByApiVersion(string $api_version) Return ChildApplicationArchive objects filtered by the api_version column
 * @method     ChildApplicationArchive[]|ObjectCollection findByPackId(int $pack_id) Return ChildApplicationArchive objects filtered by the pack_id column
 * @method     ChildApplicationArchive[]|ObjectCollection findByCategoryId(int $category_id) Return ChildApplicationArchive objects filtered by the category_id column
 * @method     ChildApplicationArchive[]|ObjectCollection findByTemplateId(int $template_id) Return ChildApplicationArchive objects filtered by the template_id column
 * @method     ChildApplicationArchive[]|ObjectCollection findByPushNotification(string $push_notification) Return ChildApplicationArchive objects filtered by the push_notification column
 * @method     ChildApplicationArchive[]|ObjectCollection findByMaxNotification(int $max_notification) Return ChildApplicationArchive objects filtered by the max_notification column
 * @method     ChildApplicationArchive[]|ObjectCollection findByMaxAdviceHour(int $max_advice_hour) Return ChildApplicationArchive objects filtered by the max_advice_hour column
 * @method     ChildApplicationArchive[]|ObjectCollection findByAdviceHourDone(int $advice_hour_done) Return ChildApplicationArchive objects filtered by the advice_hour_done column
 * @method     ChildApplicationArchive[]|ObjectCollection findByMaxContentInsert(int $max_content_insert) Return ChildApplicationArchive objects filtered by the max_content_insert column
 * @method     ChildApplicationArchive[]|ObjectCollection findByIcon(string $icon) Return ChildApplicationArchive objects filtered by the icon column
 * @method     ChildApplicationArchive[]|ObjectCollection findBySplashScreen(string $splash_screen) Return ChildApplicationArchive objects filtered by the splash_screen column
 * @method     ChildApplicationArchive[]|ObjectCollection findByBackgroundColor(string $background_color) Return ChildApplicationArchive objects filtered by the background_color column
 * @method     ChildApplicationArchive[]|ObjectCollection findByAppLogo(string $app_logo) Return ChildApplicationArchive objects filtered by the app_logo column
 * @method     ChildApplicationArchive[]|ObjectCollection findByLayout(string $layout) Return ChildApplicationArchive objects filtered by the layout column
 * @method     ChildApplicationArchive[]|ObjectCollection findByWhiteLabel(boolean $white_label) Return ChildApplicationArchive objects filtered by the white_label column
 * @method     ChildApplicationArchive[]|ObjectCollection findByDemo(boolean $demo) Return ChildApplicationArchive objects filtered by the demo column
 * @method     ChildApplicationArchive[]|ObjectCollection findByFacebookToken(string $facebook_token) Return ChildApplicationArchive objects filtered by the facebook_token column
 * @method     ChildApplicationArchive[]|ObjectCollection findByAppleId(string $apple_id) Return ChildApplicationArchive objects filtered by the apple_id column
 * @method     ChildApplicationArchive[]|ObjectCollection findByAppleIdPassword(string $apple_id_password) Return ChildApplicationArchive objects filtered by the apple_id_password column
 * @method     ChildApplicationArchive[]|ObjectCollection findByAppleStoreAppLink(string $apple_store_app_link) Return ChildApplicationArchive objects filtered by the apple_store_app_link column
 * @method     ChildApplicationArchive[]|ObjectCollection findByPlayStoreId(string $play_store_id) Return ChildApplicationArchive objects filtered by the play_store_id column
 * @method     ChildApplicationArchive[]|ObjectCollection findByPlayStoreIdPassword(string $play_store_id_password) Return ChildApplicationArchive objects filtered by the play_store_id_password column
 * @method     ChildApplicationArchive[]|ObjectCollection findByPlayStoreAppLink(string $play_store_app_link) Return ChildApplicationArchive objects filtered by the play_store_app_link column
 * @method     ChildApplicationArchive[]|ObjectCollection findByControllerUri(string $controller_uri) Return ChildApplicationArchive objects filtered by the controller_uri column
 * @method     ChildApplicationArchive[]|ObjectCollection findByGoogleAnalyticsUa(string $google_analytics_ua) Return ChildApplicationArchive objects filtered by the google_analytics_ua column
 * @method     ChildApplicationArchive[]|ObjectCollection findByPublished(int $published) Return ChildApplicationArchive objects filtered by the published column
 * @method     ChildApplicationArchive[]|ObjectCollection findByExpiredAt(string $expired_at) Return ChildApplicationArchive objects filtered by the expired_at column
 * @method     ChildApplicationArchive[]|ObjectCollection findByMainContentsLanguage(string $main_contents_language) Return ChildApplicationArchive objects filtered by the main_contents_language column
 * @method     ChildApplicationArchive[]|ObjectCollection findByOtherContentsLanguages(string $other_contents_languages) Return ChildApplicationArchive objects filtered by the other_contents_languages column
 * @method     ChildApplicationArchive[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildApplicationArchive objects filtered by the deleted_at column
 * @method     ChildApplicationArchive[]|ObjectCollection findBySystemIcons(string $system_icons) Return ChildApplicationArchive objects filtered by the system_icons column
 * @method     ChildApplicationArchive[]|ObjectCollection findByToolbar(string $toolbar) Return ChildApplicationArchive objects filtered by the toolbar column
 * @method     ChildApplicationArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildApplicationArchive objects filtered by the created_at column
 * @method     ChildApplicationArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildApplicationArchive objects filtered by the updated_at column
 * @method     ChildApplicationArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildApplicationArchive objects filtered by the archived_at column
 * @method     ChildApplicationArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApplicationArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\ApplicationArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\ApplicationArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApplicationArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApplicationArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApplicationArchiveQuery) {
            return $criteria;
        }
        $query = new ChildApplicationArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApplicationArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApplicationArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApplicationArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApplicationArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, title, slug, bundle, description, app_key, dsn, db_host, db_name, db_user, db_pwd, api_version, pack_id, category_id, template_id, push_notification, max_notification, max_advice_hour, advice_hour_done, max_content_insert, icon, splash_screen, background_color, app_logo, layout, white_label, demo, facebook_token, apple_id, apple_id_password, apple_store_app_link, play_store_id, play_store_id_password, play_store_app_link, controller_uri, google_analytics_ua, published, expired_at, main_contents_language, other_contents_languages, deleted_at, system_icons, toolbar, created_at, updated_at, archived_at FROM application_archive WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApplicationArchive $obj */
            $obj = new ChildApplicationArchive();
            $obj->hydrate($row);
            ApplicationArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApplicationArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%', Criteria::LIKE); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the bundle column
     *
     * Example usage:
     * <code>
     * $query->filterByBundle('fooValue');   // WHERE bundle = 'fooValue'
     * $query->filterByBundle('%fooValue%', Criteria::LIKE); // WHERE bundle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bundle The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByBundle($bundle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bundle)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_BUNDLE, $bundle, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the app_key column
     *
     * Example usage:
     * <code>
     * $query->filterByAppKey('fooValue');   // WHERE app_key = 'fooValue'
     * $query->filterByAppKey('%fooValue%', Criteria::LIKE); // WHERE app_key LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appKey The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByAppKey($appKey = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appKey)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_APP_KEY, $appKey, $comparison);
    }

    /**
     * Filter the query on the dsn column
     *
     * Example usage:
     * <code>
     * $query->filterByDsn('fooValue');   // WHERE dsn = 'fooValue'
     * $query->filterByDsn('%fooValue%', Criteria::LIKE); // WHERE dsn LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dsn The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByDsn($dsn = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dsn)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_DSN, $dsn, $comparison);
    }

    /**
     * Filter the query on the db_host column
     *
     * Example usage:
     * <code>
     * $query->filterByDbHost('fooValue');   // WHERE db_host = 'fooValue'
     * $query->filterByDbHost('%fooValue%', Criteria::LIKE); // WHERE db_host LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dbHost The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByDbHost($dbHost = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dbHost)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_DB_HOST, $dbHost, $comparison);
    }

    /**
     * Filter the query on the db_name column
     *
     * Example usage:
     * <code>
     * $query->filterByDbName('fooValue');   // WHERE db_name = 'fooValue'
     * $query->filterByDbName('%fooValue%', Criteria::LIKE); // WHERE db_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dbName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByDbName($dbName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dbName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_DB_NAME, $dbName, $comparison);
    }

    /**
     * Filter the query on the db_user column
     *
     * Example usage:
     * <code>
     * $query->filterByDbUser('fooValue');   // WHERE db_user = 'fooValue'
     * $query->filterByDbUser('%fooValue%', Criteria::LIKE); // WHERE db_user LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dbUser The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByDbUser($dbUser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dbUser)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_DB_USER, $dbUser, $comparison);
    }

    /**
     * Filter the query on the db_pwd column
     *
     * Example usage:
     * <code>
     * $query->filterByDbPwd('fooValue');   // WHERE db_pwd = 'fooValue'
     * $query->filterByDbPwd('%fooValue%', Criteria::LIKE); // WHERE db_pwd LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dbPwd The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByDbPwd($dbPwd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dbPwd)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_DB_PWD, $dbPwd, $comparison);
    }

    /**
     * Filter the query on the api_version column
     *
     * Example usage:
     * <code>
     * $query->filterByApiVersion('fooValue');   // WHERE api_version = 'fooValue'
     * $query->filterByApiVersion('%fooValue%', Criteria::LIKE); // WHERE api_version LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiVersion The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByApiVersion($apiVersion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiVersion)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_API_VERSION, $apiVersion, $comparison);
    }

    /**
     * Filter the query on the pack_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPackId(1234); // WHERE pack_id = 1234
     * $query->filterByPackId(array(12, 34)); // WHERE pack_id IN (12, 34)
     * $query->filterByPackId(array('min' => 12)); // WHERE pack_id > 12
     * </code>
     *
     * @param     mixed $packId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByPackId($packId = null, $comparison = null)
    {
        if (is_array($packId)) {
            $useMinMax = false;
            if (isset($packId['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_PACK_ID, $packId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($packId['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_PACK_ID, $packId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_PACK_ID, $packId, $comparison);
    }

    /**
     * Filter the query on the category_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryId(1234); // WHERE category_id = 1234
     * $query->filterByCategoryId(array(12, 34)); // WHERE category_id IN (12, 34)
     * $query->filterByCategoryId(array('min' => 12)); // WHERE category_id > 12
     * </code>
     *
     * @param     mixed $categoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByCategoryId($categoryId = null, $comparison = null)
    {
        if (is_array($categoryId)) {
            $useMinMax = false;
            if (isset($categoryId['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_CATEGORY_ID, $categoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categoryId['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_CATEGORY_ID, $categoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_CATEGORY_ID, $categoryId, $comparison);
    }

    /**
     * Filter the query on the template_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTemplateId(1234); // WHERE template_id = 1234
     * $query->filterByTemplateId(array(12, 34)); // WHERE template_id IN (12, 34)
     * $query->filterByTemplateId(array('min' => 12)); // WHERE template_id > 12
     * </code>
     *
     * @param     mixed $templateId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByTemplateId($templateId = null, $comparison = null)
    {
        if (is_array($templateId)) {
            $useMinMax = false;
            if (isset($templateId['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_TEMPLATE_ID, $templateId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($templateId['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_TEMPLATE_ID, $templateId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_TEMPLATE_ID, $templateId, $comparison);
    }

    /**
     * Filter the query on the push_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByPushNotification('fooValue');   // WHERE push_notification = 'fooValue'
     * $query->filterByPushNotification('%fooValue%', Criteria::LIKE); // WHERE push_notification LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pushNotification The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByPushNotification($pushNotification = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pushNotification)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_PUSH_NOTIFICATION, $pushNotification, $comparison);
    }

    /**
     * Filter the query on the max_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxNotification(1234); // WHERE max_notification = 1234
     * $query->filterByMaxNotification(array(12, 34)); // WHERE max_notification IN (12, 34)
     * $query->filterByMaxNotification(array('min' => 12)); // WHERE max_notification > 12
     * </code>
     *
     * @param     mixed $maxNotification The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByMaxNotification($maxNotification = null, $comparison = null)
    {
        if (is_array($maxNotification)) {
            $useMinMax = false;
            if (isset($maxNotification['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_MAX_NOTIFICATION, $maxNotification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxNotification['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_MAX_NOTIFICATION, $maxNotification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_MAX_NOTIFICATION, $maxNotification, $comparison);
    }

    /**
     * Filter the query on the max_advice_hour column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxAdviceHour(1234); // WHERE max_advice_hour = 1234
     * $query->filterByMaxAdviceHour(array(12, 34)); // WHERE max_advice_hour IN (12, 34)
     * $query->filterByMaxAdviceHour(array('min' => 12)); // WHERE max_advice_hour > 12
     * </code>
     *
     * @param     mixed $maxAdviceHour The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByMaxAdviceHour($maxAdviceHour = null, $comparison = null)
    {
        if (is_array($maxAdviceHour)) {
            $useMinMax = false;
            if (isset($maxAdviceHour['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_MAX_ADVICE_HOUR, $maxAdviceHour['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxAdviceHour['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_MAX_ADVICE_HOUR, $maxAdviceHour['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_MAX_ADVICE_HOUR, $maxAdviceHour, $comparison);
    }

    /**
     * Filter the query on the advice_hour_done column
     *
     * Example usage:
     * <code>
     * $query->filterByAdviceHourDone(1234); // WHERE advice_hour_done = 1234
     * $query->filterByAdviceHourDone(array(12, 34)); // WHERE advice_hour_done IN (12, 34)
     * $query->filterByAdviceHourDone(array('min' => 12)); // WHERE advice_hour_done > 12
     * </code>
     *
     * @param     mixed $adviceHourDone The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByAdviceHourDone($adviceHourDone = null, $comparison = null)
    {
        if (is_array($adviceHourDone)) {
            $useMinMax = false;
            if (isset($adviceHourDone['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_ADVICE_HOUR_DONE, $adviceHourDone['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($adviceHourDone['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_ADVICE_HOUR_DONE, $adviceHourDone['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_ADVICE_HOUR_DONE, $adviceHourDone, $comparison);
    }

    /**
     * Filter the query on the max_content_insert column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxContentInsert(1234); // WHERE max_content_insert = 1234
     * $query->filterByMaxContentInsert(array(12, 34)); // WHERE max_content_insert IN (12, 34)
     * $query->filterByMaxContentInsert(array('min' => 12)); // WHERE max_content_insert > 12
     * </code>
     *
     * @param     mixed $maxContentInsert The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByMaxContentInsert($maxContentInsert = null, $comparison = null)
    {
        if (is_array($maxContentInsert)) {
            $useMinMax = false;
            if (isset($maxContentInsert['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_MAX_CONTENT_INSERT, $maxContentInsert['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxContentInsert['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_MAX_CONTENT_INSERT, $maxContentInsert['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_MAX_CONTENT_INSERT, $maxContentInsert, $comparison);
    }

    /**
     * Filter the query on the icon column
     *
     * Example usage:
     * <code>
     * $query->filterByIcon('fooValue');   // WHERE icon = 'fooValue'
     * $query->filterByIcon('%fooValue%', Criteria::LIKE); // WHERE icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $icon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByIcon($icon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($icon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_ICON, $icon, $comparison);
    }

    /**
     * Filter the query on the splash_screen column
     *
     * Example usage:
     * <code>
     * $query->filterBySplashScreen('fooValue');   // WHERE splash_screen = 'fooValue'
     * $query->filterBySplashScreen('%fooValue%', Criteria::LIKE); // WHERE splash_screen LIKE '%fooValue%'
     * </code>
     *
     * @param     string $splashScreen The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterBySplashScreen($splashScreen = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($splashScreen)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_SPLASH_SCREEN, $splashScreen, $comparison);
    }

    /**
     * Filter the query on the background_color column
     *
     * Example usage:
     * <code>
     * $query->filterByBackgroundColor('fooValue');   // WHERE background_color = 'fooValue'
     * $query->filterByBackgroundColor('%fooValue%', Criteria::LIKE); // WHERE background_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $backgroundColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByBackgroundColor($backgroundColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($backgroundColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_BACKGROUND_COLOR, $backgroundColor, $comparison);
    }

    /**
     * Filter the query on the app_logo column
     *
     * Example usage:
     * <code>
     * $query->filterByAppLogo('fooValue');   // WHERE app_logo = 'fooValue'
     * $query->filterByAppLogo('%fooValue%', Criteria::LIKE); // WHERE app_logo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appLogo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByAppLogo($appLogo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appLogo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_APP_LOGO, $appLogo, $comparison);
    }

    /**
     * Filter the query on the layout column
     *
     * Example usage:
     * <code>
     * $query->filterByLayout('fooValue');   // WHERE layout = 'fooValue'
     * $query->filterByLayout('%fooValue%', Criteria::LIKE); // WHERE layout LIKE '%fooValue%'
     * </code>
     *
     * @param     string $layout The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByLayout($layout = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($layout)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_LAYOUT, $layout, $comparison);
    }

    /**
     * Filter the query on the white_label column
     *
     * Example usage:
     * <code>
     * $query->filterByWhiteLabel(true); // WHERE white_label = true
     * $query->filterByWhiteLabel('yes'); // WHERE white_label = true
     * </code>
     *
     * @param     boolean|string $whiteLabel The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByWhiteLabel($whiteLabel = null, $comparison = null)
    {
        if (is_string($whiteLabel)) {
            $whiteLabel = in_array(strtolower($whiteLabel), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_WHITE_LABEL, $whiteLabel, $comparison);
    }

    /**
     * Filter the query on the demo column
     *
     * Example usage:
     * <code>
     * $query->filterByDemo(true); // WHERE demo = true
     * $query->filterByDemo('yes'); // WHERE demo = true
     * </code>
     *
     * @param     boolean|string $demo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByDemo($demo = null, $comparison = null)
    {
        if (is_string($demo)) {
            $demo = in_array(strtolower($demo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_DEMO, $demo, $comparison);
    }

    /**
     * Filter the query on the facebook_token column
     *
     * Example usage:
     * <code>
     * $query->filterByFacebookToken('fooValue');   // WHERE facebook_token = 'fooValue'
     * $query->filterByFacebookToken('%fooValue%', Criteria::LIKE); // WHERE facebook_token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $facebookToken The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByFacebookToken($facebookToken = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($facebookToken)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_FACEBOOK_TOKEN, $facebookToken, $comparison);
    }

    /**
     * Filter the query on the apple_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAppleId('fooValue');   // WHERE apple_id = 'fooValue'
     * $query->filterByAppleId('%fooValue%', Criteria::LIKE); // WHERE apple_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appleId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByAppleId($appleId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appleId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_APPLE_ID, $appleId, $comparison);
    }

    /**
     * Filter the query on the apple_id_password column
     *
     * Example usage:
     * <code>
     * $query->filterByAppleIdPassword('fooValue');   // WHERE apple_id_password = 'fooValue'
     * $query->filterByAppleIdPassword('%fooValue%', Criteria::LIKE); // WHERE apple_id_password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appleIdPassword The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByAppleIdPassword($appleIdPassword = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appleIdPassword)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_APPLE_ID_PASSWORD, $appleIdPassword, $comparison);
    }

    /**
     * Filter the query on the apple_store_app_link column
     *
     * Example usage:
     * <code>
     * $query->filterByAppleStoreAppLink('fooValue');   // WHERE apple_store_app_link = 'fooValue'
     * $query->filterByAppleStoreAppLink('%fooValue%', Criteria::LIKE); // WHERE apple_store_app_link LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appleStoreAppLink The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByAppleStoreAppLink($appleStoreAppLink = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appleStoreAppLink)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_APPLE_STORE_APP_LINK, $appleStoreAppLink, $comparison);
    }

    /**
     * Filter the query on the play_store_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayStoreId('fooValue');   // WHERE play_store_id = 'fooValue'
     * $query->filterByPlayStoreId('%fooValue%', Criteria::LIKE); // WHERE play_store_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $playStoreId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByPlayStoreId($playStoreId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($playStoreId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_PLAY_STORE_ID, $playStoreId, $comparison);
    }

    /**
     * Filter the query on the play_store_id_password column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayStoreIdPassword('fooValue');   // WHERE play_store_id_password = 'fooValue'
     * $query->filterByPlayStoreIdPassword('%fooValue%', Criteria::LIKE); // WHERE play_store_id_password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $playStoreIdPassword The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByPlayStoreIdPassword($playStoreIdPassword = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($playStoreIdPassword)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_PLAY_STORE_ID_PASSWORD, $playStoreIdPassword, $comparison);
    }

    /**
     * Filter the query on the play_store_app_link column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayStoreAppLink('fooValue');   // WHERE play_store_app_link = 'fooValue'
     * $query->filterByPlayStoreAppLink('%fooValue%', Criteria::LIKE); // WHERE play_store_app_link LIKE '%fooValue%'
     * </code>
     *
     * @param     string $playStoreAppLink The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByPlayStoreAppLink($playStoreAppLink = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($playStoreAppLink)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_PLAY_STORE_APP_LINK, $playStoreAppLink, $comparison);
    }

    /**
     * Filter the query on the controller_uri column
     *
     * Example usage:
     * <code>
     * $query->filterByControllerUri('fooValue');   // WHERE controller_uri = 'fooValue'
     * $query->filterByControllerUri('%fooValue%', Criteria::LIKE); // WHERE controller_uri LIKE '%fooValue%'
     * </code>
     *
     * @param     string $controllerUri The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByControllerUri($controllerUri = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($controllerUri)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_CONTROLLER_URI, $controllerUri, $comparison);
    }

    /**
     * Filter the query on the google_analytics_ua column
     *
     * Example usage:
     * <code>
     * $query->filterByGoogleAnalyticsUa('fooValue');   // WHERE google_analytics_ua = 'fooValue'
     * $query->filterByGoogleAnalyticsUa('%fooValue%', Criteria::LIKE); // WHERE google_analytics_ua LIKE '%fooValue%'
     * </code>
     *
     * @param     string $googleAnalyticsUa The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByGoogleAnalyticsUa($googleAnalyticsUa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($googleAnalyticsUa)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_GOOGLE_ANALYTICS_UA, $googleAnalyticsUa, $comparison);
    }

    /**
     * Filter the query on the published column
     *
     * Example usage:
     * <code>
     * $query->filterByPublished(1234); // WHERE published = 1234
     * $query->filterByPublished(array(12, 34)); // WHERE published IN (12, 34)
     * $query->filterByPublished(array('min' => 12)); // WHERE published > 12
     * </code>
     *
     * @param     mixed $published The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByPublished($published = null, $comparison = null)
    {
        if (is_array($published)) {
            $useMinMax = false;
            if (isset($published['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_PUBLISHED, $published['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($published['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_PUBLISHED, $published['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_PUBLISHED, $published, $comparison);
    }

    /**
     * Filter the query on the expired_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiredAt('2011-03-14'); // WHERE expired_at = '2011-03-14'
     * $query->filterByExpiredAt('now'); // WHERE expired_at = '2011-03-14'
     * $query->filterByExpiredAt(array('max' => 'yesterday')); // WHERE expired_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiredAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByExpiredAt($expiredAt = null, $comparison = null)
    {
        if (is_array($expiredAt)) {
            $useMinMax = false;
            if (isset($expiredAt['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_EXPIRED_AT, $expiredAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiredAt['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_EXPIRED_AT, $expiredAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_EXPIRED_AT, $expiredAt, $comparison);
    }

    /**
     * Filter the query on the main_contents_language column
     *
     * Example usage:
     * <code>
     * $query->filterByMainContentsLanguage('fooValue');   // WHERE main_contents_language = 'fooValue'
     * $query->filterByMainContentsLanguage('%fooValue%', Criteria::LIKE); // WHERE main_contents_language LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mainContentsLanguage The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByMainContentsLanguage($mainContentsLanguage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mainContentsLanguage)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_MAIN_CONTENTS_LANGUAGE, $mainContentsLanguage, $comparison);
    }

    /**
     * Filter the query on the other_contents_languages column
     *
     * Example usage:
     * <code>
     * $query->filterByOtherContentsLanguages('fooValue');   // WHERE other_contents_languages = 'fooValue'
     * $query->filterByOtherContentsLanguages('%fooValue%', Criteria::LIKE); // WHERE other_contents_languages LIKE '%fooValue%'
     * </code>
     *
     * @param     string $otherContentsLanguages The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByOtherContentsLanguages($otherContentsLanguages = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($otherContentsLanguages)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_OTHER_CONTENTS_LANGUAGES, $otherContentsLanguages, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the system_icons column
     *
     * Example usage:
     * <code>
     * $query->filterBySystemIcons('fooValue');   // WHERE system_icons = 'fooValue'
     * $query->filterBySystemIcons('%fooValue%', Criteria::LIKE); // WHERE system_icons LIKE '%fooValue%'
     * </code>
     *
     * @param     string $systemIcons The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterBySystemIcons($systemIcons = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($systemIcons)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_SYSTEM_ICONS, $systemIcons, $comparison);
    }

    /**
     * Filter the query on the toolbar column
     *
     * Example usage:
     * <code>
     * $query->filterByToolbar('fooValue');   // WHERE toolbar = 'fooValue'
     * $query->filterByToolbar('%fooValue%', Criteria::LIKE); // WHERE toolbar LIKE '%fooValue%'
     * </code>
     *
     * @param     string $toolbar The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByToolbar($toolbar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($toolbar)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_TOOLBAR, $toolbar, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(ApplicationArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApplicationArchive $applicationArchive Object to remove from the list of results
     *
     * @return $this|ChildApplicationArchiveQuery The current query, for fluid interface
     */
    public function prune($applicationArchive = null)
    {
        if ($applicationArchive) {
            $this->addUsingAlias(ApplicationArchiveTableMap::COL_ID, $applicationArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the application_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicationArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApplicationArchiveTableMap::clearInstancePool();
            ApplicationArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicationArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApplicationArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApplicationArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApplicationArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ApplicationArchiveQuery
