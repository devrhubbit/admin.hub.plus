<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\RemoteLog as ChildRemoteLog;
use Database\HubPlus\RemoteLogQuery as ChildRemoteLogQuery;
use Database\HubPlus\Map\RemoteLogTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'remote_log' table.
 *
 *
 *
 * @method     ChildRemoteLogQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildRemoteLogQuery orderByConnectorId($order = Criteria::ASC) Order by the connector_id column
 * @method     ChildRemoteLogQuery orderByRemoteId($order = Criteria::ASC) Order by the remote_id column
 * @method     ChildRemoteLogQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildRemoteLogQuery orderByContentType($order = Criteria::ASC) Order by the content_type column
 * @method     ChildRemoteLogQuery orderByLikedAt($order = Criteria::ASC) Order by the liked_at column
 * @method     ChildRemoteLogQuery orderBySharedAt($order = Criteria::ASC) Order by the shared_at column
 * @method     ChildRemoteLogQuery orderByShareCount($order = Criteria::ASC) Order by the share_count column
 * @method     ChildRemoteLogQuery orderByViewCount($order = Criteria::ASC) Order by the view_count column
 * @method     ChildRemoteLogQuery orderByLikeCount($order = Criteria::ASC) Order by the like_count column
 *
 * @method     ChildRemoteLogQuery groupById() Group by the id column
 * @method     ChildRemoteLogQuery groupByConnectorId() Group by the connector_id column
 * @method     ChildRemoteLogQuery groupByRemoteId() Group by the remote_id column
 * @method     ChildRemoteLogQuery groupByUserId() Group by the user_id column
 * @method     ChildRemoteLogQuery groupByContentType() Group by the content_type column
 * @method     ChildRemoteLogQuery groupByLikedAt() Group by the liked_at column
 * @method     ChildRemoteLogQuery groupBySharedAt() Group by the shared_at column
 * @method     ChildRemoteLogQuery groupByShareCount() Group by the share_count column
 * @method     ChildRemoteLogQuery groupByViewCount() Group by the view_count column
 * @method     ChildRemoteLogQuery groupByLikeCount() Group by the like_count column
 *
 * @method     ChildRemoteLogQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRemoteLogQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRemoteLogQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRemoteLogQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRemoteLogQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRemoteLogQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRemoteLogQuery leftJoinSectionConnector($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionConnector relation
 * @method     ChildRemoteLogQuery rightJoinSectionConnector($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionConnector relation
 * @method     ChildRemoteLogQuery innerJoinSectionConnector($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionConnector relation
 *
 * @method     ChildRemoteLogQuery joinWithSectionConnector($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionConnector relation
 *
 * @method     ChildRemoteLogQuery leftJoinWithSectionConnector() Adds a LEFT JOIN clause and with to the query using the SectionConnector relation
 * @method     ChildRemoteLogQuery rightJoinWithSectionConnector() Adds a RIGHT JOIN clause and with to the query using the SectionConnector relation
 * @method     ChildRemoteLogQuery innerJoinWithSectionConnector() Adds a INNER JOIN clause and with to the query using the SectionConnector relation
 *
 * @method     ChildRemoteLogQuery leftJoinUserApp($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserApp relation
 * @method     ChildRemoteLogQuery rightJoinUserApp($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserApp relation
 * @method     ChildRemoteLogQuery innerJoinUserApp($relationAlias = null) Adds a INNER JOIN clause to the query using the UserApp relation
 *
 * @method     ChildRemoteLogQuery joinWithUserApp($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserApp relation
 *
 * @method     ChildRemoteLogQuery leftJoinWithUserApp() Adds a LEFT JOIN clause and with to the query using the UserApp relation
 * @method     ChildRemoteLogQuery rightJoinWithUserApp() Adds a RIGHT JOIN clause and with to the query using the UserApp relation
 * @method     ChildRemoteLogQuery innerJoinWithUserApp() Adds a INNER JOIN clause and with to the query using the UserApp relation
 *
 * @method     \Database\HubPlus\SectionConnectorQuery|\Database\HubPlus\UserAppQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRemoteLog findOne(ConnectionInterface $con = null) Return the first ChildRemoteLog matching the query
 * @method     ChildRemoteLog findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRemoteLog matching the query, or a new ChildRemoteLog object populated from the query conditions when no match is found
 *
 * @method     ChildRemoteLog findOneById(int $id) Return the first ChildRemoteLog filtered by the id column
 * @method     ChildRemoteLog findOneByConnectorId(int $connector_id) Return the first ChildRemoteLog filtered by the connector_id column
 * @method     ChildRemoteLog findOneByRemoteId(string $remote_id) Return the first ChildRemoteLog filtered by the remote_id column
 * @method     ChildRemoteLog findOneByUserId(int $user_id) Return the first ChildRemoteLog filtered by the user_id column
 * @method     ChildRemoteLog findOneByContentType(string $content_type) Return the first ChildRemoteLog filtered by the content_type column
 * @method     ChildRemoteLog findOneByLikedAt(string $liked_at) Return the first ChildRemoteLog filtered by the liked_at column
 * @method     ChildRemoteLog findOneBySharedAt(string $shared_at) Return the first ChildRemoteLog filtered by the shared_at column
 * @method     ChildRemoteLog findOneByShareCount(int $share_count) Return the first ChildRemoteLog filtered by the share_count column
 * @method     ChildRemoteLog findOneByViewCount(int $view_count) Return the first ChildRemoteLog filtered by the view_count column
 * @method     ChildRemoteLog findOneByLikeCount(int $like_count) Return the first ChildRemoteLog filtered by the like_count column *

 * @method     ChildRemoteLog requirePk($key, ConnectionInterface $con = null) Return the ChildRemoteLog by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteLog requireOne(ConnectionInterface $con = null) Return the first ChildRemoteLog matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRemoteLog requireOneById(int $id) Return the first ChildRemoteLog filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteLog requireOneByConnectorId(int $connector_id) Return the first ChildRemoteLog filtered by the connector_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteLog requireOneByRemoteId(string $remote_id) Return the first ChildRemoteLog filtered by the remote_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteLog requireOneByUserId(int $user_id) Return the first ChildRemoteLog filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteLog requireOneByContentType(string $content_type) Return the first ChildRemoteLog filtered by the content_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteLog requireOneByLikedAt(string $liked_at) Return the first ChildRemoteLog filtered by the liked_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteLog requireOneBySharedAt(string $shared_at) Return the first ChildRemoteLog filtered by the shared_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteLog requireOneByShareCount(int $share_count) Return the first ChildRemoteLog filtered by the share_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteLog requireOneByViewCount(int $view_count) Return the first ChildRemoteLog filtered by the view_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteLog requireOneByLikeCount(int $like_count) Return the first ChildRemoteLog filtered by the like_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRemoteLog[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRemoteLog objects based on current ModelCriteria
 * @method     ChildRemoteLog[]|ObjectCollection findById(int $id) Return ChildRemoteLog objects filtered by the id column
 * @method     ChildRemoteLog[]|ObjectCollection findByConnectorId(int $connector_id) Return ChildRemoteLog objects filtered by the connector_id column
 * @method     ChildRemoteLog[]|ObjectCollection findByRemoteId(string $remote_id) Return ChildRemoteLog objects filtered by the remote_id column
 * @method     ChildRemoteLog[]|ObjectCollection findByUserId(int $user_id) Return ChildRemoteLog objects filtered by the user_id column
 * @method     ChildRemoteLog[]|ObjectCollection findByContentType(string $content_type) Return ChildRemoteLog objects filtered by the content_type column
 * @method     ChildRemoteLog[]|ObjectCollection findByLikedAt(string $liked_at) Return ChildRemoteLog objects filtered by the liked_at column
 * @method     ChildRemoteLog[]|ObjectCollection findBySharedAt(string $shared_at) Return ChildRemoteLog objects filtered by the shared_at column
 * @method     ChildRemoteLog[]|ObjectCollection findByShareCount(int $share_count) Return ChildRemoteLog objects filtered by the share_count column
 * @method     ChildRemoteLog[]|ObjectCollection findByViewCount(int $view_count) Return ChildRemoteLog objects filtered by the view_count column
 * @method     ChildRemoteLog[]|ObjectCollection findByLikeCount(int $like_count) Return ChildRemoteLog objects filtered by the like_count column
 * @method     ChildRemoteLog[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RemoteLogQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\RemoteLogQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\RemoteLog', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRemoteLogQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRemoteLogQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRemoteLogQuery) {
            return $criteria;
        }
        $query = new ChildRemoteLogQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRemoteLog|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RemoteLogTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RemoteLogTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRemoteLog A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, connector_id, remote_id, user_id, content_type, liked_at, shared_at, share_count, view_count, like_count FROM remote_log WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRemoteLog $obj */
            $obj = new ChildRemoteLog();
            $obj->hydrate($row);
            RemoteLogTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRemoteLog|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RemoteLogTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RemoteLogTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteLogTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the connector_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConnectorId(1234); // WHERE connector_id = 1234
     * $query->filterByConnectorId(array(12, 34)); // WHERE connector_id IN (12, 34)
     * $query->filterByConnectorId(array('min' => 12)); // WHERE connector_id > 12
     * </code>
     *
     * @see       filterBySectionConnector()
     *
     * @param     mixed $connectorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterByConnectorId($connectorId = null, $comparison = null)
    {
        if (is_array($connectorId)) {
            $useMinMax = false;
            if (isset($connectorId['min'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_CONNECTOR_ID, $connectorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($connectorId['max'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_CONNECTOR_ID, $connectorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteLogTableMap::COL_CONNECTOR_ID, $connectorId, $comparison);
    }

    /**
     * Filter the query on the remote_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRemoteId('fooValue');   // WHERE remote_id = 'fooValue'
     * $query->filterByRemoteId('%fooValue%', Criteria::LIKE); // WHERE remote_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remoteId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterByRemoteId($remoteId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remoteId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteLogTableMap::COL_REMOTE_ID, $remoteId, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUserApp()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteLogTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the content_type column
     *
     * Example usage:
     * <code>
     * $query->filterByContentType('fooValue');   // WHERE content_type = 'fooValue'
     * $query->filterByContentType('%fooValue%', Criteria::LIKE); // WHERE content_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contentType The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterByContentType($contentType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contentType)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteLogTableMap::COL_CONTENT_TYPE, $contentType, $comparison);
    }

    /**
     * Filter the query on the liked_at column
     *
     * Example usage:
     * <code>
     * $query->filterByLikedAt('2011-03-14'); // WHERE liked_at = '2011-03-14'
     * $query->filterByLikedAt('now'); // WHERE liked_at = '2011-03-14'
     * $query->filterByLikedAt(array('max' => 'yesterday')); // WHERE liked_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $likedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterByLikedAt($likedAt = null, $comparison = null)
    {
        if (is_array($likedAt)) {
            $useMinMax = false;
            if (isset($likedAt['min'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_LIKED_AT, $likedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($likedAt['max'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_LIKED_AT, $likedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteLogTableMap::COL_LIKED_AT, $likedAt, $comparison);
    }

    /**
     * Filter the query on the shared_at column
     *
     * Example usage:
     * <code>
     * $query->filterBySharedAt('2011-03-14'); // WHERE shared_at = '2011-03-14'
     * $query->filterBySharedAt('now'); // WHERE shared_at = '2011-03-14'
     * $query->filterBySharedAt(array('max' => 'yesterday')); // WHERE shared_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $sharedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterBySharedAt($sharedAt = null, $comparison = null)
    {
        if (is_array($sharedAt)) {
            $useMinMax = false;
            if (isset($sharedAt['min'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_SHARED_AT, $sharedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sharedAt['max'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_SHARED_AT, $sharedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteLogTableMap::COL_SHARED_AT, $sharedAt, $comparison);
    }

    /**
     * Filter the query on the share_count column
     *
     * Example usage:
     * <code>
     * $query->filterByShareCount(1234); // WHERE share_count = 1234
     * $query->filterByShareCount(array(12, 34)); // WHERE share_count IN (12, 34)
     * $query->filterByShareCount(array('min' => 12)); // WHERE share_count > 12
     * </code>
     *
     * @param     mixed $shareCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterByShareCount($shareCount = null, $comparison = null)
    {
        if (is_array($shareCount)) {
            $useMinMax = false;
            if (isset($shareCount['min'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_SHARE_COUNT, $shareCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shareCount['max'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_SHARE_COUNT, $shareCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteLogTableMap::COL_SHARE_COUNT, $shareCount, $comparison);
    }

    /**
     * Filter the query on the view_count column
     *
     * Example usage:
     * <code>
     * $query->filterByViewCount(1234); // WHERE view_count = 1234
     * $query->filterByViewCount(array(12, 34)); // WHERE view_count IN (12, 34)
     * $query->filterByViewCount(array('min' => 12)); // WHERE view_count > 12
     * </code>
     *
     * @param     mixed $viewCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterByViewCount($viewCount = null, $comparison = null)
    {
        if (is_array($viewCount)) {
            $useMinMax = false;
            if (isset($viewCount['min'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_VIEW_COUNT, $viewCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($viewCount['max'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_VIEW_COUNT, $viewCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteLogTableMap::COL_VIEW_COUNT, $viewCount, $comparison);
    }

    /**
     * Filter the query on the like_count column
     *
     * Example usage:
     * <code>
     * $query->filterByLikeCount(1234); // WHERE like_count = 1234
     * $query->filterByLikeCount(array(12, 34)); // WHERE like_count IN (12, 34)
     * $query->filterByLikeCount(array('min' => 12)); // WHERE like_count > 12
     * </code>
     *
     * @param     mixed $likeCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterByLikeCount($likeCount = null, $comparison = null)
    {
        if (is_array($likeCount)) {
            $useMinMax = false;
            if (isset($likeCount['min'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_LIKE_COUNT, $likeCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($likeCount['max'])) {
                $this->addUsingAlias(RemoteLogTableMap::COL_LIKE_COUNT, $likeCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteLogTableMap::COL_LIKE_COUNT, $likeCount, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\SectionConnector object
     *
     * @param \Database\HubPlus\SectionConnector|ObjectCollection $sectionConnector The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterBySectionConnector($sectionConnector, $comparison = null)
    {
        if ($sectionConnector instanceof \Database\HubPlus\SectionConnector) {
            return $this
                ->addUsingAlias(RemoteLogTableMap::COL_CONNECTOR_ID, $sectionConnector->getId(), $comparison);
        } elseif ($sectionConnector instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RemoteLogTableMap::COL_CONNECTOR_ID, $sectionConnector->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySectionConnector() only accepts arguments of type \Database\HubPlus\SectionConnector or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionConnector relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function joinSectionConnector($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionConnector');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionConnector');
        }

        return $this;
    }

    /**
     * Use the SectionConnector relation SectionConnector object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionConnectorQuery A secondary query class using the current class as primary query
     */
    public function useSectionConnectorQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSectionConnector($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionConnector', '\Database\HubPlus\SectionConnectorQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\UserApp object
     *
     * @param \Database\HubPlus\UserApp|ObjectCollection $userApp The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRemoteLogQuery The current query, for fluid interface
     */
    public function filterByUserApp($userApp, $comparison = null)
    {
        if ($userApp instanceof \Database\HubPlus\UserApp) {
            return $this
                ->addUsingAlias(RemoteLogTableMap::COL_USER_ID, $userApp->getId(), $comparison);
        } elseif ($userApp instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RemoteLogTableMap::COL_USER_ID, $userApp->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUserApp() only accepts arguments of type \Database\HubPlus\UserApp or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserApp relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function joinUserApp($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserApp');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserApp');
        }

        return $this;
    }

    /**
     * Use the UserApp relation UserApp object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\UserAppQuery A secondary query class using the current class as primary query
     */
    public function useUserAppQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserApp($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserApp', '\Database\HubPlus\UserAppQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRemoteLog $remoteLog Object to remove from the list of results
     *
     * @return $this|ChildRemoteLogQuery The current query, for fluid interface
     */
    public function prune($remoteLog = null)
    {
        if ($remoteLog) {
            $this->addUsingAlias(RemoteLogTableMap::COL_ID, $remoteLog->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the remote_log table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteLogTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RemoteLogTableMap::clearInstancePool();
            RemoteLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteLogTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RemoteLogTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RemoteLogTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RemoteLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RemoteLogQuery
