<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\Section as ChildSection;
use Database\HubPlus\SectionArchive as ChildSectionArchive;
use Database\HubPlus\SectionQuery as ChildSectionQuery;
use Database\HubPlus\Map\SectionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'section' table.
 *
 *
 *
 * @method     ChildSectionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSectionQuery orderByArea($order = Criteria::ASC) Order by the area column
 * @method     ChildSectionQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildSectionQuery orderByAuthorId($order = Criteria::ASC) Order by the author_id column
 * @method     ChildSectionQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildSectionQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildSectionQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 * @method     ChildSectionQuery orderByIconId($order = Criteria::ASC) Order by the icon_id column
 * @method     ChildSectionQuery orderByIconSelectedId($order = Criteria::ASC) Order by the icon_selected_id column
 * @method     ChildSectionQuery orderByResource($order = Criteria::ASC) Order by the resource column
 * @method     ChildSectionQuery orderByParams($order = Criteria::ASC) Order by the params column
 * @method     ChildSectionQuery orderByTemplateId($order = Criteria::ASC) Order by the template_id column
 * @method     ChildSectionQuery orderByLayout($order = Criteria::ASC) Order by the layout column
 * @method     ChildSectionQuery orderByTags($order = Criteria::ASC) Order by the tags column
 * @method     ChildSectionQuery orderByTagsUser($order = Criteria::ASC) Order by the tags_user column
 * @method     ChildSectionQuery orderByParsedTags($order = Criteria::ASC) Order by the parsed_tags column
 * @method     ChildSectionQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildSectionQuery orderByWeight($order = Criteria::ASC) Order by the weight column
 * @method     ChildSectionQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildSectionQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildSectionQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildSectionQuery groupById() Group by the id column
 * @method     ChildSectionQuery groupByArea() Group by the area column
 * @method     ChildSectionQuery groupByType() Group by the type column
 * @method     ChildSectionQuery groupByAuthorId() Group by the author_id column
 * @method     ChildSectionQuery groupByTitle() Group by the title column
 * @method     ChildSectionQuery groupByDescription() Group by the description column
 * @method     ChildSectionQuery groupBySlug() Group by the slug column
 * @method     ChildSectionQuery groupByIconId() Group by the icon_id column
 * @method     ChildSectionQuery groupByIconSelectedId() Group by the icon_selected_id column
 * @method     ChildSectionQuery groupByResource() Group by the resource column
 * @method     ChildSectionQuery groupByParams() Group by the params column
 * @method     ChildSectionQuery groupByTemplateId() Group by the template_id column
 * @method     ChildSectionQuery groupByLayout() Group by the layout column
 * @method     ChildSectionQuery groupByTags() Group by the tags column
 * @method     ChildSectionQuery groupByTagsUser() Group by the tags_user column
 * @method     ChildSectionQuery groupByParsedTags() Group by the parsed_tags column
 * @method     ChildSectionQuery groupByStatus() Group by the status column
 * @method     ChildSectionQuery groupByWeight() Group by the weight column
 * @method     ChildSectionQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildSectionQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildSectionQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildSectionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSectionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSectionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSectionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSectionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSectionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSectionQuery leftJoinTemplate($relationAlias = null) Adds a LEFT JOIN clause to the query using the Template relation
 * @method     ChildSectionQuery rightJoinTemplate($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Template relation
 * @method     ChildSectionQuery innerJoinTemplate($relationAlias = null) Adds a INNER JOIN clause to the query using the Template relation
 *
 * @method     ChildSectionQuery joinWithTemplate($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Template relation
 *
 * @method     ChildSectionQuery leftJoinWithTemplate() Adds a LEFT JOIN clause and with to the query using the Template relation
 * @method     ChildSectionQuery rightJoinWithTemplate() Adds a RIGHT JOIN clause and with to the query using the Template relation
 * @method     ChildSectionQuery innerJoinWithTemplate() Adds a INNER JOIN clause and with to the query using the Template relation
 *
 * @method     ChildSectionQuery leftJoinMediaRelatedByIconId($relationAlias = null) Adds a LEFT JOIN clause to the query using the MediaRelatedByIconId relation
 * @method     ChildSectionQuery rightJoinMediaRelatedByIconId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MediaRelatedByIconId relation
 * @method     ChildSectionQuery innerJoinMediaRelatedByIconId($relationAlias = null) Adds a INNER JOIN clause to the query using the MediaRelatedByIconId relation
 *
 * @method     ChildSectionQuery joinWithMediaRelatedByIconId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MediaRelatedByIconId relation
 *
 * @method     ChildSectionQuery leftJoinWithMediaRelatedByIconId() Adds a LEFT JOIN clause and with to the query using the MediaRelatedByIconId relation
 * @method     ChildSectionQuery rightJoinWithMediaRelatedByIconId() Adds a RIGHT JOIN clause and with to the query using the MediaRelatedByIconId relation
 * @method     ChildSectionQuery innerJoinWithMediaRelatedByIconId() Adds a INNER JOIN clause and with to the query using the MediaRelatedByIconId relation
 *
 * @method     ChildSectionQuery leftJoinMediaRelatedByIconSelectedId($relationAlias = null) Adds a LEFT JOIN clause to the query using the MediaRelatedByIconSelectedId relation
 * @method     ChildSectionQuery rightJoinMediaRelatedByIconSelectedId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MediaRelatedByIconSelectedId relation
 * @method     ChildSectionQuery innerJoinMediaRelatedByIconSelectedId($relationAlias = null) Adds a INNER JOIN clause to the query using the MediaRelatedByIconSelectedId relation
 *
 * @method     ChildSectionQuery joinWithMediaRelatedByIconSelectedId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MediaRelatedByIconSelectedId relation
 *
 * @method     ChildSectionQuery leftJoinWithMediaRelatedByIconSelectedId() Adds a LEFT JOIN clause and with to the query using the MediaRelatedByIconSelectedId relation
 * @method     ChildSectionQuery rightJoinWithMediaRelatedByIconSelectedId() Adds a RIGHT JOIN clause and with to the query using the MediaRelatedByIconSelectedId relation
 * @method     ChildSectionQuery innerJoinWithMediaRelatedByIconSelectedId() Adds a INNER JOIN clause and with to the query using the MediaRelatedByIconSelectedId relation
 *
 * @method     ChildSectionQuery leftJoinUserBackend($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserBackend relation
 * @method     ChildSectionQuery rightJoinUserBackend($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserBackend relation
 * @method     ChildSectionQuery innerJoinUserBackend($relationAlias = null) Adds a INNER JOIN clause to the query using the UserBackend relation
 *
 * @method     ChildSectionQuery joinWithUserBackend($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserBackend relation
 *
 * @method     ChildSectionQuery leftJoinWithUserBackend() Adds a LEFT JOIN clause and with to the query using the UserBackend relation
 * @method     ChildSectionQuery rightJoinWithUserBackend() Adds a RIGHT JOIN clause and with to the query using the UserBackend relation
 * @method     ChildSectionQuery innerJoinWithUserBackend() Adds a INNER JOIN clause and with to the query using the UserBackend relation
 *
 * @method     ChildSectionQuery leftJoinCustomFormExtraLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomFormExtraLog relation
 * @method     ChildSectionQuery rightJoinCustomFormExtraLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomFormExtraLog relation
 * @method     ChildSectionQuery innerJoinCustomFormExtraLog($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomFormExtraLog relation
 *
 * @method     ChildSectionQuery joinWithCustomFormExtraLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomFormExtraLog relation
 *
 * @method     ChildSectionQuery leftJoinWithCustomFormExtraLog() Adds a LEFT JOIN clause and with to the query using the CustomFormExtraLog relation
 * @method     ChildSectionQuery rightJoinWithCustomFormExtraLog() Adds a RIGHT JOIN clause and with to the query using the CustomFormExtraLog relation
 * @method     ChildSectionQuery innerJoinWithCustomFormExtraLog() Adds a INNER JOIN clause and with to the query using the CustomFormExtraLog relation
 *
 * @method     ChildSectionQuery leftJoinCustomFormLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomFormLog relation
 * @method     ChildSectionQuery rightJoinCustomFormLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomFormLog relation
 * @method     ChildSectionQuery innerJoinCustomFormLog($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomFormLog relation
 *
 * @method     ChildSectionQuery joinWithCustomFormLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomFormLog relation
 *
 * @method     ChildSectionQuery leftJoinWithCustomFormLog() Adds a LEFT JOIN clause and with to the query using the CustomFormLog relation
 * @method     ChildSectionQuery rightJoinWithCustomFormLog() Adds a RIGHT JOIN clause and with to the query using the CustomFormLog relation
 * @method     ChildSectionQuery innerJoinWithCustomFormLog() Adds a INNER JOIN clause and with to the query using the CustomFormLog relation
 *
 * @method     ChildSectionQuery leftJoinPostAction($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostAction relation
 * @method     ChildSectionQuery rightJoinPostAction($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostAction relation
 * @method     ChildSectionQuery innerJoinPostAction($relationAlias = null) Adds a INNER JOIN clause to the query using the PostAction relation
 *
 * @method     ChildSectionQuery joinWithPostAction($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostAction relation
 *
 * @method     ChildSectionQuery leftJoinWithPostAction() Adds a LEFT JOIN clause and with to the query using the PostAction relation
 * @method     ChildSectionQuery rightJoinWithPostAction() Adds a RIGHT JOIN clause and with to the query using the PostAction relation
 * @method     ChildSectionQuery innerJoinWithPostAction() Adds a INNER JOIN clause and with to the query using the PostAction relation
 *
 * @method     ChildSectionQuery leftJoinPushNotification($relationAlias = null) Adds a LEFT JOIN clause to the query using the PushNotification relation
 * @method     ChildSectionQuery rightJoinPushNotification($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PushNotification relation
 * @method     ChildSectionQuery innerJoinPushNotification($relationAlias = null) Adds a INNER JOIN clause to the query using the PushNotification relation
 *
 * @method     ChildSectionQuery joinWithPushNotification($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PushNotification relation
 *
 * @method     ChildSectionQuery leftJoinWithPushNotification() Adds a LEFT JOIN clause and with to the query using the PushNotification relation
 * @method     ChildSectionQuery rightJoinWithPushNotification() Adds a RIGHT JOIN clause and with to the query using the PushNotification relation
 * @method     ChildSectionQuery innerJoinWithPushNotification() Adds a INNER JOIN clause and with to the query using the PushNotification relation
 *
 * @method     ChildSectionQuery leftJoinSectionConnectorRelatedBySectionId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionConnectorRelatedBySectionId relation
 * @method     ChildSectionQuery rightJoinSectionConnectorRelatedBySectionId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionConnectorRelatedBySectionId relation
 * @method     ChildSectionQuery innerJoinSectionConnectorRelatedBySectionId($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionConnectorRelatedBySectionId relation
 *
 * @method     ChildSectionQuery joinWithSectionConnectorRelatedBySectionId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionConnectorRelatedBySectionId relation
 *
 * @method     ChildSectionQuery leftJoinWithSectionConnectorRelatedBySectionId() Adds a LEFT JOIN clause and with to the query using the SectionConnectorRelatedBySectionId relation
 * @method     ChildSectionQuery rightJoinWithSectionConnectorRelatedBySectionId() Adds a RIGHT JOIN clause and with to the query using the SectionConnectorRelatedBySectionId relation
 * @method     ChildSectionQuery innerJoinWithSectionConnectorRelatedBySectionId() Adds a INNER JOIN clause and with to the query using the SectionConnectorRelatedBySectionId relation
 *
 * @method     ChildSectionQuery leftJoinSectionConnectorRelatedByRemoteSectionId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionConnectorRelatedByRemoteSectionId relation
 * @method     ChildSectionQuery rightJoinSectionConnectorRelatedByRemoteSectionId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionConnectorRelatedByRemoteSectionId relation
 * @method     ChildSectionQuery innerJoinSectionConnectorRelatedByRemoteSectionId($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionConnectorRelatedByRemoteSectionId relation
 *
 * @method     ChildSectionQuery joinWithSectionConnectorRelatedByRemoteSectionId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionConnectorRelatedByRemoteSectionId relation
 *
 * @method     ChildSectionQuery leftJoinWithSectionConnectorRelatedByRemoteSectionId() Adds a LEFT JOIN clause and with to the query using the SectionConnectorRelatedByRemoteSectionId relation
 * @method     ChildSectionQuery rightJoinWithSectionConnectorRelatedByRemoteSectionId() Adds a RIGHT JOIN clause and with to the query using the SectionConnectorRelatedByRemoteSectionId relation
 * @method     ChildSectionQuery innerJoinWithSectionConnectorRelatedByRemoteSectionId() Adds a INNER JOIN clause and with to the query using the SectionConnectorRelatedByRemoteSectionId relation
 *
 * @method     ChildSectionQuery leftJoinSectionRelatedToRelatedByFromSectionId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionRelatedToRelatedByFromSectionId relation
 * @method     ChildSectionQuery rightJoinSectionRelatedToRelatedByFromSectionId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionRelatedToRelatedByFromSectionId relation
 * @method     ChildSectionQuery innerJoinSectionRelatedToRelatedByFromSectionId($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionRelatedToRelatedByFromSectionId relation
 *
 * @method     ChildSectionQuery joinWithSectionRelatedToRelatedByFromSectionId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionRelatedToRelatedByFromSectionId relation
 *
 * @method     ChildSectionQuery leftJoinWithSectionRelatedToRelatedByFromSectionId() Adds a LEFT JOIN clause and with to the query using the SectionRelatedToRelatedByFromSectionId relation
 * @method     ChildSectionQuery rightJoinWithSectionRelatedToRelatedByFromSectionId() Adds a RIGHT JOIN clause and with to the query using the SectionRelatedToRelatedByFromSectionId relation
 * @method     ChildSectionQuery innerJoinWithSectionRelatedToRelatedByFromSectionId() Adds a INNER JOIN clause and with to the query using the SectionRelatedToRelatedByFromSectionId relation
 *
 * @method     ChildSectionQuery leftJoinSectionRelatedToRelatedByToSectionId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionRelatedToRelatedByToSectionId relation
 * @method     ChildSectionQuery rightJoinSectionRelatedToRelatedByToSectionId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionRelatedToRelatedByToSectionId relation
 * @method     ChildSectionQuery innerJoinSectionRelatedToRelatedByToSectionId($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionRelatedToRelatedByToSectionId relation
 *
 * @method     ChildSectionQuery joinWithSectionRelatedToRelatedByToSectionId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionRelatedToRelatedByToSectionId relation
 *
 * @method     ChildSectionQuery leftJoinWithSectionRelatedToRelatedByToSectionId() Adds a LEFT JOIN clause and with to the query using the SectionRelatedToRelatedByToSectionId relation
 * @method     ChildSectionQuery rightJoinWithSectionRelatedToRelatedByToSectionId() Adds a RIGHT JOIN clause and with to the query using the SectionRelatedToRelatedByToSectionId relation
 * @method     ChildSectionQuery innerJoinWithSectionRelatedToRelatedByToSectionId() Adds a INNER JOIN clause and with to the query using the SectionRelatedToRelatedByToSectionId relation
 *
 * @method     ChildSectionQuery leftJoinUserApp($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserApp relation
 * @method     ChildSectionQuery rightJoinUserApp($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserApp relation
 * @method     ChildSectionQuery innerJoinUserApp($relationAlias = null) Adds a INNER JOIN clause to the query using the UserApp relation
 *
 * @method     ChildSectionQuery joinWithUserApp($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserApp relation
 *
 * @method     ChildSectionQuery leftJoinWithUserApp() Adds a LEFT JOIN clause and with to the query using the UserApp relation
 * @method     ChildSectionQuery rightJoinWithUserApp() Adds a RIGHT JOIN clause and with to the query using the UserApp relation
 * @method     ChildSectionQuery innerJoinWithUserApp() Adds a INNER JOIN clause and with to the query using the UserApp relation
 *
 * @method     ChildSectionQuery leftJoinWebhook($relationAlias = null) Adds a LEFT JOIN clause to the query using the Webhook relation
 * @method     ChildSectionQuery rightJoinWebhook($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Webhook relation
 * @method     ChildSectionQuery innerJoinWebhook($relationAlias = null) Adds a INNER JOIN clause to the query using the Webhook relation
 *
 * @method     ChildSectionQuery joinWithWebhook($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Webhook relation
 *
 * @method     ChildSectionQuery leftJoinWithWebhook() Adds a LEFT JOIN clause and with to the query using the Webhook relation
 * @method     ChildSectionQuery rightJoinWithWebhook() Adds a RIGHT JOIN clause and with to the query using the Webhook relation
 * @method     ChildSectionQuery innerJoinWithWebhook() Adds a INNER JOIN clause and with to the query using the Webhook relation
 *
 * @method     \Database\HubPlus\TemplateQuery|\Database\HubPlus\MediaQuery|\Database\HubPlus\UserBackendQuery|\Database\HubPlus\CustomFormExtraLogQuery|\Database\HubPlus\CustomFormLogQuery|\Database\HubPlus\PostActionQuery|\Database\HubPlus\PushNotificationQuery|\Database\HubPlus\SectionConnectorQuery|\Database\HubPlus\SectionRelatedToQuery|\Database\HubPlus\UserAppQuery|\Database\HubPlus\WebhookQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSection findOne(ConnectionInterface $con = null) Return the first ChildSection matching the query
 * @method     ChildSection findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSection matching the query, or a new ChildSection object populated from the query conditions when no match is found
 *
 * @method     ChildSection findOneById(int $id) Return the first ChildSection filtered by the id column
 * @method     ChildSection findOneByArea(string $area) Return the first ChildSection filtered by the area column
 * @method     ChildSection findOneByType(string $type) Return the first ChildSection filtered by the type column
 * @method     ChildSection findOneByAuthorId(int $author_id) Return the first ChildSection filtered by the author_id column
 * @method     ChildSection findOneByTitle(string $title) Return the first ChildSection filtered by the title column
 * @method     ChildSection findOneByDescription(string $description) Return the first ChildSection filtered by the description column
 * @method     ChildSection findOneBySlug(string $slug) Return the first ChildSection filtered by the slug column
 * @method     ChildSection findOneByIconId(int $icon_id) Return the first ChildSection filtered by the icon_id column
 * @method     ChildSection findOneByIconSelectedId(int $icon_selected_id) Return the first ChildSection filtered by the icon_selected_id column
 * @method     ChildSection findOneByResource(string $resource) Return the first ChildSection filtered by the resource column
 * @method     ChildSection findOneByParams(string $params) Return the first ChildSection filtered by the params column
 * @method     ChildSection findOneByTemplateId(int $template_id) Return the first ChildSection filtered by the template_id column
 * @method     ChildSection findOneByLayout(string $layout) Return the first ChildSection filtered by the layout column
 * @method     ChildSection findOneByTags(string $tags) Return the first ChildSection filtered by the tags column
 * @method     ChildSection findOneByTagsUser(string $tags_user) Return the first ChildSection filtered by the tags_user column
 * @method     ChildSection findOneByParsedTags(string $parsed_tags) Return the first ChildSection filtered by the parsed_tags column
 * @method     ChildSection findOneByStatus(boolean $status) Return the first ChildSection filtered by the status column
 * @method     ChildSection findOneByWeight(int $weight) Return the first ChildSection filtered by the weight column
 * @method     ChildSection findOneByDeletedAt(string $deleted_at) Return the first ChildSection filtered by the deleted_at column
 * @method     ChildSection findOneByCreatedAt(string $created_at) Return the first ChildSection filtered by the created_at column
 * @method     ChildSection findOneByUpdatedAt(string $updated_at) Return the first ChildSection filtered by the updated_at column *

 * @method     ChildSection requirePk($key, ConnectionInterface $con = null) Return the ChildSection by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOne(ConnectionInterface $con = null) Return the first ChildSection matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSection requireOneById(int $id) Return the first ChildSection filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByArea(string $area) Return the first ChildSection filtered by the area column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByType(string $type) Return the first ChildSection filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByAuthorId(int $author_id) Return the first ChildSection filtered by the author_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByTitle(string $title) Return the first ChildSection filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByDescription(string $description) Return the first ChildSection filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneBySlug(string $slug) Return the first ChildSection filtered by the slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByIconId(int $icon_id) Return the first ChildSection filtered by the icon_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByIconSelectedId(int $icon_selected_id) Return the first ChildSection filtered by the icon_selected_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByResource(string $resource) Return the first ChildSection filtered by the resource column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByParams(string $params) Return the first ChildSection filtered by the params column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByTemplateId(int $template_id) Return the first ChildSection filtered by the template_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByLayout(string $layout) Return the first ChildSection filtered by the layout column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByTags(string $tags) Return the first ChildSection filtered by the tags column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByTagsUser(string $tags_user) Return the first ChildSection filtered by the tags_user column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByParsedTags(string $parsed_tags) Return the first ChildSection filtered by the parsed_tags column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByStatus(boolean $status) Return the first ChildSection filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByWeight(int $weight) Return the first ChildSection filtered by the weight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByDeletedAt(string $deleted_at) Return the first ChildSection filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByCreatedAt(string $created_at) Return the first ChildSection filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSection requireOneByUpdatedAt(string $updated_at) Return the first ChildSection filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSection[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSection objects based on current ModelCriteria
 * @method     ChildSection[]|ObjectCollection findById(int $id) Return ChildSection objects filtered by the id column
 * @method     ChildSection[]|ObjectCollection findByArea(string $area) Return ChildSection objects filtered by the area column
 * @method     ChildSection[]|ObjectCollection findByType(string $type) Return ChildSection objects filtered by the type column
 * @method     ChildSection[]|ObjectCollection findByAuthorId(int $author_id) Return ChildSection objects filtered by the author_id column
 * @method     ChildSection[]|ObjectCollection findByTitle(string $title) Return ChildSection objects filtered by the title column
 * @method     ChildSection[]|ObjectCollection findByDescription(string $description) Return ChildSection objects filtered by the description column
 * @method     ChildSection[]|ObjectCollection findBySlug(string $slug) Return ChildSection objects filtered by the slug column
 * @method     ChildSection[]|ObjectCollection findByIconId(int $icon_id) Return ChildSection objects filtered by the icon_id column
 * @method     ChildSection[]|ObjectCollection findByIconSelectedId(int $icon_selected_id) Return ChildSection objects filtered by the icon_selected_id column
 * @method     ChildSection[]|ObjectCollection findByResource(string $resource) Return ChildSection objects filtered by the resource column
 * @method     ChildSection[]|ObjectCollection findByParams(string $params) Return ChildSection objects filtered by the params column
 * @method     ChildSection[]|ObjectCollection findByTemplateId(int $template_id) Return ChildSection objects filtered by the template_id column
 * @method     ChildSection[]|ObjectCollection findByLayout(string $layout) Return ChildSection objects filtered by the layout column
 * @method     ChildSection[]|ObjectCollection findByTags(string $tags) Return ChildSection objects filtered by the tags column
 * @method     ChildSection[]|ObjectCollection findByTagsUser(string $tags_user) Return ChildSection objects filtered by the tags_user column
 * @method     ChildSection[]|ObjectCollection findByParsedTags(string $parsed_tags) Return ChildSection objects filtered by the parsed_tags column
 * @method     ChildSection[]|ObjectCollection findByStatus(boolean $status) Return ChildSection objects filtered by the status column
 * @method     ChildSection[]|ObjectCollection findByWeight(int $weight) Return ChildSection objects filtered by the weight column
 * @method     ChildSection[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildSection objects filtered by the deleted_at column
 * @method     ChildSection[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildSection objects filtered by the created_at column
 * @method     ChildSection[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildSection objects filtered by the updated_at column
 * @method     ChildSection[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SectionQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\SectionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\Section', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSectionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSectionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSectionQuery) {
            return $criteria;
        }
        $query = new ChildSectionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSection|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SectionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SectionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSection A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, area, type, author_id, title, description, slug, icon_id, icon_selected_id, resource, params, template_id, layout, tags, tags_user, parsed_tags, status, weight, deleted_at, created_at, updated_at FROM section WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSection $obj */
            $obj = new ChildSection();
            $obj->hydrate($row);
            SectionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSection|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SectionTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SectionTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SectionTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SectionTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the area column
     *
     * Example usage:
     * <code>
     * $query->filterByArea('fooValue');   // WHERE area = 'fooValue'
     * $query->filterByArea('%fooValue%', Criteria::LIKE); // WHERE area LIKE '%fooValue%'
     * </code>
     *
     * @param     string $area The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByArea($area = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($area)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_AREA, $area, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the author_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAuthorId(1234); // WHERE author_id = 1234
     * $query->filterByAuthorId(array(12, 34)); // WHERE author_id IN (12, 34)
     * $query->filterByAuthorId(array('min' => 12)); // WHERE author_id > 12
     * </code>
     *
     * @see       filterByUserBackend()
     *
     * @param     mixed $authorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByAuthorId($authorId = null, $comparison = null)
    {
        if (is_array($authorId)) {
            $useMinMax = false;
            if (isset($authorId['min'])) {
                $this->addUsingAlias(SectionTableMap::COL_AUTHOR_ID, $authorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($authorId['max'])) {
                $this->addUsingAlias(SectionTableMap::COL_AUTHOR_ID, $authorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_AUTHOR_ID, $authorId, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%', Criteria::LIKE); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the icon_id column
     *
     * Example usage:
     * <code>
     * $query->filterByIconId(1234); // WHERE icon_id = 1234
     * $query->filterByIconId(array(12, 34)); // WHERE icon_id IN (12, 34)
     * $query->filterByIconId(array('min' => 12)); // WHERE icon_id > 12
     * </code>
     *
     * @see       filterByMediaRelatedByIconId()
     *
     * @param     mixed $iconId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByIconId($iconId = null, $comparison = null)
    {
        if (is_array($iconId)) {
            $useMinMax = false;
            if (isset($iconId['min'])) {
                $this->addUsingAlias(SectionTableMap::COL_ICON_ID, $iconId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($iconId['max'])) {
                $this->addUsingAlias(SectionTableMap::COL_ICON_ID, $iconId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_ICON_ID, $iconId, $comparison);
    }

    /**
     * Filter the query on the icon_selected_id column
     *
     * Example usage:
     * <code>
     * $query->filterByIconSelectedId(1234); // WHERE icon_selected_id = 1234
     * $query->filterByIconSelectedId(array(12, 34)); // WHERE icon_selected_id IN (12, 34)
     * $query->filterByIconSelectedId(array('min' => 12)); // WHERE icon_selected_id > 12
     * </code>
     *
     * @see       filterByMediaRelatedByIconSelectedId()
     *
     * @param     mixed $iconSelectedId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByIconSelectedId($iconSelectedId = null, $comparison = null)
    {
        if (is_array($iconSelectedId)) {
            $useMinMax = false;
            if (isset($iconSelectedId['min'])) {
                $this->addUsingAlias(SectionTableMap::COL_ICON_SELECTED_ID, $iconSelectedId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($iconSelectedId['max'])) {
                $this->addUsingAlias(SectionTableMap::COL_ICON_SELECTED_ID, $iconSelectedId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_ICON_SELECTED_ID, $iconSelectedId, $comparison);
    }

    /**
     * Filter the query on the resource column
     *
     * Example usage:
     * <code>
     * $query->filterByResource('fooValue');   // WHERE resource = 'fooValue'
     * $query->filterByResource('%fooValue%', Criteria::LIKE); // WHERE resource LIKE '%fooValue%'
     * </code>
     *
     * @param     string $resource The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByResource($resource = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($resource)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_RESOURCE, $resource, $comparison);
    }

    /**
     * Filter the query on the params column
     *
     * Example usage:
     * <code>
     * $query->filterByParams('fooValue');   // WHERE params = 'fooValue'
     * $query->filterByParams('%fooValue%', Criteria::LIKE); // WHERE params LIKE '%fooValue%'
     * </code>
     *
     * @param     string $params The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByParams($params = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($params)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_PARAMS, $params, $comparison);
    }

    /**
     * Filter the query on the template_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTemplateId(1234); // WHERE template_id = 1234
     * $query->filterByTemplateId(array(12, 34)); // WHERE template_id IN (12, 34)
     * $query->filterByTemplateId(array('min' => 12)); // WHERE template_id > 12
     * </code>
     *
     * @see       filterByTemplate()
     *
     * @param     mixed $templateId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByTemplateId($templateId = null, $comparison = null)
    {
        if (is_array($templateId)) {
            $useMinMax = false;
            if (isset($templateId['min'])) {
                $this->addUsingAlias(SectionTableMap::COL_TEMPLATE_ID, $templateId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($templateId['max'])) {
                $this->addUsingAlias(SectionTableMap::COL_TEMPLATE_ID, $templateId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_TEMPLATE_ID, $templateId, $comparison);
    }

    /**
     * Filter the query on the layout column
     *
     * Example usage:
     * <code>
     * $query->filterByLayout('fooValue');   // WHERE layout = 'fooValue'
     * $query->filterByLayout('%fooValue%', Criteria::LIKE); // WHERE layout LIKE '%fooValue%'
     * </code>
     *
     * @param     string $layout The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByLayout($layout = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($layout)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_LAYOUT, $layout, $comparison);
    }

    /**
     * Filter the query on the tags column
     *
     * Example usage:
     * <code>
     * $query->filterByTags('fooValue');   // WHERE tags = 'fooValue'
     * $query->filterByTags('%fooValue%', Criteria::LIKE); // WHERE tags LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tags The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByTags($tags = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tags)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_TAGS, $tags, $comparison);
    }

    /**
     * Filter the query on the tags_user column
     *
     * Example usage:
     * <code>
     * $query->filterByTagsUser('fooValue');   // WHERE tags_user = 'fooValue'
     * $query->filterByTagsUser('%fooValue%', Criteria::LIKE); // WHERE tags_user LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tagsUser The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByTagsUser($tagsUser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tagsUser)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_TAGS_USER, $tagsUser, $comparison);
    }

    /**
     * Filter the query on the parsed_tags column
     *
     * Example usage:
     * <code>
     * $query->filterByParsedTags('fooValue');   // WHERE parsed_tags = 'fooValue'
     * $query->filterByParsedTags('%fooValue%', Criteria::LIKE); // WHERE parsed_tags LIKE '%fooValue%'
     * </code>
     *
     * @param     string $parsedTags The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByParsedTags($parsedTags = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($parsedTags)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_PARSED_TAGS, $parsedTags, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(true); // WHERE status = true
     * $query->filterByStatus('yes'); // WHERE status = true
     * </code>
     *
     * @param     boolean|string $status The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_string($status)) {
            $status = in_array(strtolower($status), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SectionTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the weight column
     *
     * Example usage:
     * <code>
     * $query->filterByWeight(1234); // WHERE weight = 1234
     * $query->filterByWeight(array(12, 34)); // WHERE weight IN (12, 34)
     * $query->filterByWeight(array('min' => 12)); // WHERE weight > 12
     * </code>
     *
     * @param     mixed $weight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByWeight($weight = null, $comparison = null)
    {
        if (is_array($weight)) {
            $useMinMax = false;
            if (isset($weight['min'])) {
                $this->addUsingAlias(SectionTableMap::COL_WEIGHT, $weight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($weight['max'])) {
                $this->addUsingAlias(SectionTableMap::COL_WEIGHT, $weight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_WEIGHT, $weight, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(SectionTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(SectionTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(SectionTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(SectionTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(SectionTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(SectionTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Template object
     *
     * @param \Database\HubPlus\Template|ObjectCollection $template The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterByTemplate($template, $comparison = null)
    {
        if ($template instanceof \Database\HubPlus\Template) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_TEMPLATE_ID, $template->getId(), $comparison);
        } elseif ($template instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SectionTableMap::COL_TEMPLATE_ID, $template->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTemplate() only accepts arguments of type \Database\HubPlus\Template or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Template relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinTemplate($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Template');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Template');
        }

        return $this;
    }

    /**
     * Use the Template relation Template object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\TemplateQuery A secondary query class using the current class as primary query
     */
    public function useTemplateQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTemplate($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Template', '\Database\HubPlus\TemplateQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Media object
     *
     * @param \Database\HubPlus\Media|ObjectCollection $media The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterByMediaRelatedByIconId($media, $comparison = null)
    {
        if ($media instanceof \Database\HubPlus\Media) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_ICON_ID, $media->getId(), $comparison);
        } elseif ($media instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SectionTableMap::COL_ICON_ID, $media->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMediaRelatedByIconId() only accepts arguments of type \Database\HubPlus\Media or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MediaRelatedByIconId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinMediaRelatedByIconId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MediaRelatedByIconId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MediaRelatedByIconId');
        }

        return $this;
    }

    /**
     * Use the MediaRelatedByIconId relation Media object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\MediaQuery A secondary query class using the current class as primary query
     */
    public function useMediaRelatedByIconIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMediaRelatedByIconId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MediaRelatedByIconId', '\Database\HubPlus\MediaQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Media object
     *
     * @param \Database\HubPlus\Media|ObjectCollection $media The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterByMediaRelatedByIconSelectedId($media, $comparison = null)
    {
        if ($media instanceof \Database\HubPlus\Media) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_ICON_SELECTED_ID, $media->getId(), $comparison);
        } elseif ($media instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SectionTableMap::COL_ICON_SELECTED_ID, $media->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMediaRelatedByIconSelectedId() only accepts arguments of type \Database\HubPlus\Media or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MediaRelatedByIconSelectedId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinMediaRelatedByIconSelectedId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MediaRelatedByIconSelectedId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MediaRelatedByIconSelectedId');
        }

        return $this;
    }

    /**
     * Use the MediaRelatedByIconSelectedId relation Media object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\MediaQuery A secondary query class using the current class as primary query
     */
    public function useMediaRelatedByIconSelectedIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMediaRelatedByIconSelectedId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MediaRelatedByIconSelectedId', '\Database\HubPlus\MediaQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\UserBackend object
     *
     * @param \Database\HubPlus\UserBackend|ObjectCollection $userBackend The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterByUserBackend($userBackend, $comparison = null)
    {
        if ($userBackend instanceof \Database\HubPlus\UserBackend) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_AUTHOR_ID, $userBackend->getId(), $comparison);
        } elseif ($userBackend instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SectionTableMap::COL_AUTHOR_ID, $userBackend->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUserBackend() only accepts arguments of type \Database\HubPlus\UserBackend or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserBackend relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinUserBackend($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserBackend');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserBackend');
        }

        return $this;
    }

    /**
     * Use the UserBackend relation UserBackend object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\UserBackendQuery A secondary query class using the current class as primary query
     */
    public function useUserBackendQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserBackend($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserBackend', '\Database\HubPlus\UserBackendQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\CustomFormExtraLog object
     *
     * @param \Database\HubPlus\CustomFormExtraLog|ObjectCollection $customFormExtraLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterByCustomFormExtraLog($customFormExtraLog, $comparison = null)
    {
        if ($customFormExtraLog instanceof \Database\HubPlus\CustomFormExtraLog) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_ID, $customFormExtraLog->getFormId(), $comparison);
        } elseif ($customFormExtraLog instanceof ObjectCollection) {
            return $this
                ->useCustomFormExtraLogQuery()
                ->filterByPrimaryKeys($customFormExtraLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomFormExtraLog() only accepts arguments of type \Database\HubPlus\CustomFormExtraLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomFormExtraLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinCustomFormExtraLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomFormExtraLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomFormExtraLog');
        }

        return $this;
    }

    /**
     * Use the CustomFormExtraLog relation CustomFormExtraLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\CustomFormExtraLogQuery A secondary query class using the current class as primary query
     */
    public function useCustomFormExtraLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCustomFormExtraLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomFormExtraLog', '\Database\HubPlus\CustomFormExtraLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\CustomFormLog object
     *
     * @param \Database\HubPlus\CustomFormLog|ObjectCollection $customFormLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterByCustomFormLog($customFormLog, $comparison = null)
    {
        if ($customFormLog instanceof \Database\HubPlus\CustomFormLog) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_ID, $customFormLog->getFormId(), $comparison);
        } elseif ($customFormLog instanceof ObjectCollection) {
            return $this
                ->useCustomFormLogQuery()
                ->filterByPrimaryKeys($customFormLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomFormLog() only accepts arguments of type \Database\HubPlus\CustomFormLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomFormLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinCustomFormLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomFormLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomFormLog');
        }

        return $this;
    }

    /**
     * Use the CustomFormLog relation CustomFormLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\CustomFormLogQuery A secondary query class using the current class as primary query
     */
    public function useCustomFormLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCustomFormLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomFormLog', '\Database\HubPlus\CustomFormLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostAction object
     *
     * @param \Database\HubPlus\PostAction|ObjectCollection $postAction the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterByPostAction($postAction, $comparison = null)
    {
        if ($postAction instanceof \Database\HubPlus\PostAction) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_ID, $postAction->getSectionId(), $comparison);
        } elseif ($postAction instanceof ObjectCollection) {
            return $this
                ->usePostActionQuery()
                ->filterByPrimaryKeys($postAction->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostAction() only accepts arguments of type \Database\HubPlus\PostAction or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostAction relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinPostAction($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostAction');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostAction');
        }

        return $this;
    }

    /**
     * Use the PostAction relation PostAction object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostActionQuery A secondary query class using the current class as primary query
     */
    public function usePostActionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPostAction($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostAction', '\Database\HubPlus\PostActionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PushNotification object
     *
     * @param \Database\HubPlus\PushNotification|ObjectCollection $pushNotification the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterByPushNotification($pushNotification, $comparison = null)
    {
        if ($pushNotification instanceof \Database\HubPlus\PushNotification) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_ID, $pushNotification->getSectionId(), $comparison);
        } elseif ($pushNotification instanceof ObjectCollection) {
            return $this
                ->usePushNotificationQuery()
                ->filterByPrimaryKeys($pushNotification->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPushNotification() only accepts arguments of type \Database\HubPlus\PushNotification or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PushNotification relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinPushNotification($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PushNotification');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PushNotification');
        }

        return $this;
    }

    /**
     * Use the PushNotification relation PushNotification object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PushNotificationQuery A secondary query class using the current class as primary query
     */
    public function usePushNotificationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPushNotification($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PushNotification', '\Database\HubPlus\PushNotificationQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\SectionConnector object
     *
     * @param \Database\HubPlus\SectionConnector|ObjectCollection $sectionConnector the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterBySectionConnectorRelatedBySectionId($sectionConnector, $comparison = null)
    {
        if ($sectionConnector instanceof \Database\HubPlus\SectionConnector) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_ID, $sectionConnector->getSectionId(), $comparison);
        } elseif ($sectionConnector instanceof ObjectCollection) {
            return $this
                ->useSectionConnectorRelatedBySectionIdQuery()
                ->filterByPrimaryKeys($sectionConnector->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySectionConnectorRelatedBySectionId() only accepts arguments of type \Database\HubPlus\SectionConnector or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionConnectorRelatedBySectionId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinSectionConnectorRelatedBySectionId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionConnectorRelatedBySectionId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionConnectorRelatedBySectionId');
        }

        return $this;
    }

    /**
     * Use the SectionConnectorRelatedBySectionId relation SectionConnector object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionConnectorQuery A secondary query class using the current class as primary query
     */
    public function useSectionConnectorRelatedBySectionIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSectionConnectorRelatedBySectionId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionConnectorRelatedBySectionId', '\Database\HubPlus\SectionConnectorQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\SectionConnector object
     *
     * @param \Database\HubPlus\SectionConnector|ObjectCollection $sectionConnector the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterBySectionConnectorRelatedByRemoteSectionId($sectionConnector, $comparison = null)
    {
        if ($sectionConnector instanceof \Database\HubPlus\SectionConnector) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_ID, $sectionConnector->getRemoteSectionId(), $comparison);
        } elseif ($sectionConnector instanceof ObjectCollection) {
            return $this
                ->useSectionConnectorRelatedByRemoteSectionIdQuery()
                ->filterByPrimaryKeys($sectionConnector->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySectionConnectorRelatedByRemoteSectionId() only accepts arguments of type \Database\HubPlus\SectionConnector or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionConnectorRelatedByRemoteSectionId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinSectionConnectorRelatedByRemoteSectionId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionConnectorRelatedByRemoteSectionId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionConnectorRelatedByRemoteSectionId');
        }

        return $this;
    }

    /**
     * Use the SectionConnectorRelatedByRemoteSectionId relation SectionConnector object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionConnectorQuery A secondary query class using the current class as primary query
     */
    public function useSectionConnectorRelatedByRemoteSectionIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSectionConnectorRelatedByRemoteSectionId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionConnectorRelatedByRemoteSectionId', '\Database\HubPlus\SectionConnectorQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\SectionRelatedTo object
     *
     * @param \Database\HubPlus\SectionRelatedTo|ObjectCollection $sectionRelatedTo the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterBySectionRelatedToRelatedByFromSectionId($sectionRelatedTo, $comparison = null)
    {
        if ($sectionRelatedTo instanceof \Database\HubPlus\SectionRelatedTo) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_ID, $sectionRelatedTo->getFromSectionId(), $comparison);
        } elseif ($sectionRelatedTo instanceof ObjectCollection) {
            return $this
                ->useSectionRelatedToRelatedByFromSectionIdQuery()
                ->filterByPrimaryKeys($sectionRelatedTo->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySectionRelatedToRelatedByFromSectionId() only accepts arguments of type \Database\HubPlus\SectionRelatedTo or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionRelatedToRelatedByFromSectionId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinSectionRelatedToRelatedByFromSectionId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionRelatedToRelatedByFromSectionId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionRelatedToRelatedByFromSectionId');
        }

        return $this;
    }

    /**
     * Use the SectionRelatedToRelatedByFromSectionId relation SectionRelatedTo object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionRelatedToQuery A secondary query class using the current class as primary query
     */
    public function useSectionRelatedToRelatedByFromSectionIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSectionRelatedToRelatedByFromSectionId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionRelatedToRelatedByFromSectionId', '\Database\HubPlus\SectionRelatedToQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\SectionRelatedTo object
     *
     * @param \Database\HubPlus\SectionRelatedTo|ObjectCollection $sectionRelatedTo the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterBySectionRelatedToRelatedByToSectionId($sectionRelatedTo, $comparison = null)
    {
        if ($sectionRelatedTo instanceof \Database\HubPlus\SectionRelatedTo) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_ID, $sectionRelatedTo->getToSectionId(), $comparison);
        } elseif ($sectionRelatedTo instanceof ObjectCollection) {
            return $this
                ->useSectionRelatedToRelatedByToSectionIdQuery()
                ->filterByPrimaryKeys($sectionRelatedTo->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySectionRelatedToRelatedByToSectionId() only accepts arguments of type \Database\HubPlus\SectionRelatedTo or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionRelatedToRelatedByToSectionId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinSectionRelatedToRelatedByToSectionId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionRelatedToRelatedByToSectionId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionRelatedToRelatedByToSectionId');
        }

        return $this;
    }

    /**
     * Use the SectionRelatedToRelatedByToSectionId relation SectionRelatedTo object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionRelatedToQuery A secondary query class using the current class as primary query
     */
    public function useSectionRelatedToRelatedByToSectionIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSectionRelatedToRelatedByToSectionId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionRelatedToRelatedByToSectionId', '\Database\HubPlus\SectionRelatedToQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\UserApp object
     *
     * @param \Database\HubPlus\UserApp|ObjectCollection $userApp the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterByUserApp($userApp, $comparison = null)
    {
        if ($userApp instanceof \Database\HubPlus\UserApp) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_ID, $userApp->getSectionId(), $comparison);
        } elseif ($userApp instanceof ObjectCollection) {
            return $this
                ->useUserAppQuery()
                ->filterByPrimaryKeys($userApp->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserApp() only accepts arguments of type \Database\HubPlus\UserApp or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserApp relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinUserApp($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserApp');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserApp');
        }

        return $this;
    }

    /**
     * Use the UserApp relation UserApp object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\UserAppQuery A secondary query class using the current class as primary query
     */
    public function useUserAppQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserApp($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserApp', '\Database\HubPlus\UserAppQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Webhook object
     *
     * @param \Database\HubPlus\Webhook|ObjectCollection $webhook the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSectionQuery The current query, for fluid interface
     */
    public function filterByWebhook($webhook, $comparison = null)
    {
        if ($webhook instanceof \Database\HubPlus\Webhook) {
            return $this
                ->addUsingAlias(SectionTableMap::COL_ID, $webhook->getSectionId(), $comparison);
        } elseif ($webhook instanceof ObjectCollection) {
            return $this
                ->useWebhookQuery()
                ->filterByPrimaryKeys($webhook->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWebhook() only accepts arguments of type \Database\HubPlus\Webhook or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Webhook relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function joinWebhook($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Webhook');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Webhook');
        }

        return $this;
    }

    /**
     * Use the Webhook relation Webhook object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\WebhookQuery A secondary query class using the current class as primary query
     */
    public function useWebhookQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinWebhook($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Webhook', '\Database\HubPlus\WebhookQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSection $section Object to remove from the list of results
     *
     * @return $this|ChildSectionQuery The current query, for fluid interface
     */
    public function prune($section = null)
    {
        if ($section) {
            $this->addUsingAlias(SectionTableMap::COL_ID, $section->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the section table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SectionTableMap::clearInstancePool();
            SectionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SectionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SectionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SectionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildSectionQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(SectionTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildSectionQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(SectionTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildSectionQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(SectionTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildSectionQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(SectionTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildSectionQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(SectionTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildSectionQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(SectionTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildSectionArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // SectionQuery
