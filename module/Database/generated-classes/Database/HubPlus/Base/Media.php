<?php

namespace Database\HubPlus\Base;

use \DateTime;
use \Exception;
use \PDO;
use Database\HubPlus\Device as ChildDevice;
use Database\HubPlus\DeviceQuery as ChildDeviceQuery;
use Database\HubPlus\Gallery as ChildGallery;
use Database\HubPlus\GalleryForm as ChildGalleryForm;
use Database\HubPlus\GalleryFormQuery as ChildGalleryFormQuery;
use Database\HubPlus\GalleryQuery as ChildGalleryQuery;
use Database\HubPlus\Media as ChildMedia;
use Database\HubPlus\MediaArchive as ChildMediaArchive;
use Database\HubPlus\MediaArchiveQuery as ChildMediaArchiveQuery;
use Database\HubPlus\MediaExtraLog as ChildMediaExtraLog;
use Database\HubPlus\MediaExtraLogQuery as ChildMediaExtraLogQuery;
use Database\HubPlus\MediaLog as ChildMediaLog;
use Database\HubPlus\MediaLogQuery as ChildMediaLogQuery;
use Database\HubPlus\MediaQuery as ChildMediaQuery;
use Database\HubPlus\Post as ChildPost;
use Database\HubPlus\PostAction as ChildPostAction;
use Database\HubPlus\PostActionQuery as ChildPostActionQuery;
use Database\HubPlus\PostQuery as ChildPostQuery;
use Database\HubPlus\Section as ChildSection;
use Database\HubPlus\SectionQuery as ChildSectionQuery;
use Database\HubPlus\Map\GalleryFormTableMap;
use Database\HubPlus\Map\GalleryTableMap;
use Database\HubPlus\Map\MediaExtraLogTableMap;
use Database\HubPlus\Map\MediaLogTableMap;
use Database\HubPlus\Map\MediaTableMap;
use Database\HubPlus\Map\PostActionTableMap;
use Database\HubPlus\Map\PostTableMap;
use Database\HubPlus\Map\SectionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'media' table.
 *
 *
 *
 * @package    propel.generator.Database.HubPlus.Base
 */
abstract class Media implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Database\\HubPlus\\Map\\MediaTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the device_id field.
     *
     * @var        int
     */
    protected $device_id;

    /**
     * The value for the is_public field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $is_public;

    /**
     * The value for the type field.
     *
     * @var        string
     */
    protected $type;

    /**
     * The value for the uri field.
     *
     * @var        string
     */
    protected $uri;

    /**
     * The value for the uri_thumb field.
     *
     * @var        string
     */
    protected $uri_thumb;

    /**
     * The value for the extra_params field.
     *
     * @var        string
     */
    protected $extra_params;

    /**
     * The value for the weight field.
     *
     * @var        int
     */
    protected $weight;

    /**
     * The value for the title field.
     *
     * @var        string
     */
    protected $title;

    /**
     * The value for the description field.
     *
     * @var        string
     */
    protected $description;

    /**
     * The value for the extension field.
     *
     * @var        string
     */
    protected $extension;

    /**
     * The value for the mime_type field.
     *
     * @var        string
     */
    protected $mime_type;

    /**
     * The value for the size field.
     *
     * @var        int
     */
    protected $size;

    /**
     * The value for the count_like field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $count_like;

    /**
     * The value for the count_share field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $count_share;

    /**
     * The value for the format field.
     *
     * @var        string
     */
    protected $format;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildDevice
     */
    protected $aDevice;

    /**
     * @var        ObjectCollection|ChildGallery[] Collection to store aggregation of ChildGallery objects.
     */
    protected $collGalleries;
    protected $collGalleriesPartial;

    /**
     * @var        ObjectCollection|ChildGalleryForm[] Collection to store aggregation of ChildGalleryForm objects.
     */
    protected $collGalleryForms;
    protected $collGalleryFormsPartial;

    /**
     * @var        ObjectCollection|ChildMediaExtraLog[] Collection to store aggregation of ChildMediaExtraLog objects.
     */
    protected $collMediaExtraLogs;
    protected $collMediaExtraLogsPartial;

    /**
     * @var        ObjectCollection|ChildMediaLog[] Collection to store aggregation of ChildMediaLog objects.
     */
    protected $collMediaLogs;
    protected $collMediaLogsPartial;

    /**
     * @var        ObjectCollection|ChildPost[] Collection to store aggregation of ChildPost objects.
     */
    protected $collPosts;
    protected $collPostsPartial;

    /**
     * @var        ObjectCollection|ChildPostAction[] Collection to store aggregation of ChildPostAction objects.
     */
    protected $collPostActions;
    protected $collPostActionsPartial;

    /**
     * @var        ObjectCollection|ChildSection[] Collection to store aggregation of ChildSection objects.
     */
    protected $collSectionsRelatedByIconId;
    protected $collSectionsRelatedByIconIdPartial;

    /**
     * @var        ObjectCollection|ChildSection[] Collection to store aggregation of ChildSection objects.
     */
    protected $collSectionsRelatedByIconSelectedId;
    protected $collSectionsRelatedByIconSelectedIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildGallery[]
     */
    protected $galleriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildGalleryForm[]
     */
    protected $galleryFormsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMediaExtraLog[]
     */
    protected $mediaExtraLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMediaLog[]
     */
    protected $mediaLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPost[]
     */
    protected $postsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostAction[]
     */
    protected $postActionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSection[]
     */
    protected $sectionsRelatedByIconIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSection[]
     */
    protected $sectionsRelatedByIconSelectedIdScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_public = 1;
        $this->count_like = 0;
        $this->count_share = 0;
    }

    /**
     * Initializes internal state of Database\HubPlus\Base\Media object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Media</code> instance.  If
     * <code>obj</code> is an instance of <code>Media</code>, delegates to
     * <code>equals(Media)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Media The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [device_id] column value.
     *
     * @return int
     */
    public function getDeviceId()
    {
        return $this->device_id;
    }

    /**
     * Get the [is_public] column value.
     *
     * @return int
     */
    public function getIsPublic()
    {
        return $this->is_public;
    }

    /**
     * Get the [type] column value.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the [uri] column value.
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Get the [uri_thumb] column value.
     *
     * @return string
     */
    public function getUriThumb()
    {
        return $this->uri_thumb;
    }

    /**
     * Get the [extra_params] column value.
     *
     * @return string
     */
    public function getExtraParams()
    {
        return $this->extra_params;
    }

    /**
     * Get the [weight] column value.
     *
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [extension] column value.
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Get the [mime_type] column value.
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mime_type;
    }

    /**
     * Get the [size] column value.
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Get the [count_like] column value.
     *
     * @return int
     */
    public function getCountLike()
    {
        return $this->count_like;
    }

    /**
     * Get the [count_share] column value.
     *
     * @return int
     */
    public function getCountShare()
    {
        return $this->count_share;
    }

    /**
     * Get the [format] column value.
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[MediaTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [device_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setDeviceId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->device_id !== $v) {
            $this->device_id = $v;
            $this->modifiedColumns[MediaTableMap::COL_DEVICE_ID] = true;
        }

        if ($this->aDevice !== null && $this->aDevice->getId() !== $v) {
            $this->aDevice = null;
        }

        return $this;
    } // setDeviceId()

    /**
     * Set the value of [is_public] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setIsPublic($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->is_public !== $v) {
            $this->is_public = $v;
            $this->modifiedColumns[MediaTableMap::COL_IS_PUBLIC] = true;
        }

        return $this;
    } // setIsPublic()

    /**
     * Set the value of [type] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[MediaTableMap::COL_TYPE] = true;
        }

        return $this;
    } // setType()

    /**
     * Set the value of [uri] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setUri($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uri !== $v) {
            $this->uri = $v;
            $this->modifiedColumns[MediaTableMap::COL_URI] = true;
        }

        return $this;
    } // setUri()

    /**
     * Set the value of [uri_thumb] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setUriThumb($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uri_thumb !== $v) {
            $this->uri_thumb = $v;
            $this->modifiedColumns[MediaTableMap::COL_URI_THUMB] = true;
        }

        return $this;
    } // setUriThumb()

    /**
     * Set the value of [extra_params] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setExtraParams($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->extra_params !== $v) {
            $this->extra_params = $v;
            $this->modifiedColumns[MediaTableMap::COL_EXTRA_PARAMS] = true;
        }

        return $this;
    } // setExtraParams()

    /**
     * Set the value of [weight] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setWeight($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->weight !== $v) {
            $this->weight = $v;
            $this->modifiedColumns[MediaTableMap::COL_WEIGHT] = true;
        }

        return $this;
    } // setWeight()

    /**
     * Set the value of [title] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[MediaTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [description] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[MediaTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [extension] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setExtension($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->extension !== $v) {
            $this->extension = $v;
            $this->modifiedColumns[MediaTableMap::COL_EXTENSION] = true;
        }

        return $this;
    } // setExtension()

    /**
     * Set the value of [mime_type] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setMimeType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mime_type !== $v) {
            $this->mime_type = $v;
            $this->modifiedColumns[MediaTableMap::COL_MIME_TYPE] = true;
        }

        return $this;
    } // setMimeType()

    /**
     * Set the value of [size] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setSize($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->size !== $v) {
            $this->size = $v;
            $this->modifiedColumns[MediaTableMap::COL_SIZE] = true;
        }

        return $this;
    } // setSize()

    /**
     * Set the value of [count_like] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setCountLike($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->count_like !== $v) {
            $this->count_like = $v;
            $this->modifiedColumns[MediaTableMap::COL_COUNT_LIKE] = true;
        }

        return $this;
    } // setCountLike()

    /**
     * Set the value of [count_share] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setCountShare($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->count_share !== $v) {
            $this->count_share = $v;
            $this->modifiedColumns[MediaTableMap::COL_COUNT_SHARE] = true;
        }

        return $this;
    } // setCountShare()

    /**
     * Set the value of [format] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setFormat($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->format !== $v) {
            $this->format = $v;
            $this->modifiedColumns[MediaTableMap::COL_FORMAT] = true;
        }

        return $this;
    } // setFormat()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MediaTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MediaTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MediaTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_public !== 1) {
                return false;
            }

            if ($this->count_like !== 0) {
                return false;
            }

            if ($this->count_share !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : MediaTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : MediaTableMap::translateFieldName('DeviceId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->device_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : MediaTableMap::translateFieldName('IsPublic', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_public = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : MediaTableMap::translateFieldName('Type', TableMap::TYPE_PHPNAME, $indexType)];
            $this->type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : MediaTableMap::translateFieldName('Uri', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uri = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : MediaTableMap::translateFieldName('UriThumb', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uri_thumb = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : MediaTableMap::translateFieldName('ExtraParams', TableMap::TYPE_PHPNAME, $indexType)];
            $this->extra_params = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : MediaTableMap::translateFieldName('Weight', TableMap::TYPE_PHPNAME, $indexType)];
            $this->weight = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : MediaTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : MediaTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : MediaTableMap::translateFieldName('Extension', TableMap::TYPE_PHPNAME, $indexType)];
            $this->extension = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : MediaTableMap::translateFieldName('MimeType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mime_type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : MediaTableMap::translateFieldName('Size', TableMap::TYPE_PHPNAME, $indexType)];
            $this->size = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : MediaTableMap::translateFieldName('CountLike', TableMap::TYPE_PHPNAME, $indexType)];
            $this->count_like = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : MediaTableMap::translateFieldName('CountShare', TableMap::TYPE_PHPNAME, $indexType)];
            $this->count_share = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : MediaTableMap::translateFieldName('Format', TableMap::TYPE_PHPNAME, $indexType)];
            $this->format = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : MediaTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : MediaTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : MediaTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 19; // 19 = MediaTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Database\\HubPlus\\Media'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aDevice !== null && $this->device_id !== $this->aDevice->getId()) {
            $this->aDevice = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MediaTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildMediaQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aDevice = null;
            $this->collGalleries = null;

            $this->collGalleryForms = null;

            $this->collMediaExtraLogs = null;

            $this->collMediaLogs = null;

            $this->collPosts = null;

            $this->collPostActions = null;

            $this->collSectionsRelatedByIconId = null;

            $this->collSectionsRelatedByIconSelectedId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Media::setDeleted()
     * @see Media::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MediaTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildMediaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildMediaQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MediaTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(MediaTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(MediaTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(MediaTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MediaTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aDevice !== null) {
                if ($this->aDevice->isModified() || $this->aDevice->isNew()) {
                    $affectedRows += $this->aDevice->save($con);
                }
                $this->setDevice($this->aDevice);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->galleriesScheduledForDeletion !== null) {
                if (!$this->galleriesScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\GalleryQuery::create()
                        ->filterByPrimaryKeys($this->galleriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->galleriesScheduledForDeletion = null;
                }
            }

            if ($this->collGalleries !== null) {
                foreach ($this->collGalleries as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->galleryFormsScheduledForDeletion !== null) {
                if (!$this->galleryFormsScheduledForDeletion->isEmpty()) {
                    foreach ($this->galleryFormsScheduledForDeletion as $galleryForm) {
                        // need to save related object because we set the relation to null
                        $galleryForm->save($con);
                    }
                    $this->galleryFormsScheduledForDeletion = null;
                }
            }

            if ($this->collGalleryForms !== null) {
                foreach ($this->collGalleryForms as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->mediaExtraLogsScheduledForDeletion !== null) {
                if (!$this->mediaExtraLogsScheduledForDeletion->isEmpty()) {
                    foreach ($this->mediaExtraLogsScheduledForDeletion as $mediaExtraLog) {
                        // need to save related object because we set the relation to null
                        $mediaExtraLog->save($con);
                    }
                    $this->mediaExtraLogsScheduledForDeletion = null;
                }
            }

            if ($this->collMediaExtraLogs !== null) {
                foreach ($this->collMediaExtraLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->mediaLogsScheduledForDeletion !== null) {
                if (!$this->mediaLogsScheduledForDeletion->isEmpty()) {
                    foreach ($this->mediaLogsScheduledForDeletion as $mediaLog) {
                        // need to save related object because we set the relation to null
                        $mediaLog->save($con);
                    }
                    $this->mediaLogsScheduledForDeletion = null;
                }
            }

            if ($this->collMediaLogs !== null) {
                foreach ($this->collMediaLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postsScheduledForDeletion !== null) {
                if (!$this->postsScheduledForDeletion->isEmpty()) {
                    foreach ($this->postsScheduledForDeletion as $post) {
                        // need to save related object because we set the relation to null
                        $post->save($con);
                    }
                    $this->postsScheduledForDeletion = null;
                }
            }

            if ($this->collPosts !== null) {
                foreach ($this->collPosts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postActionsScheduledForDeletion !== null) {
                if (!$this->postActionsScheduledForDeletion->isEmpty()) {
                    foreach ($this->postActionsScheduledForDeletion as $postAction) {
                        // need to save related object because we set the relation to null
                        $postAction->save($con);
                    }
                    $this->postActionsScheduledForDeletion = null;
                }
            }

            if ($this->collPostActions !== null) {
                foreach ($this->collPostActions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sectionsRelatedByIconIdScheduledForDeletion !== null) {
                if (!$this->sectionsRelatedByIconIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->sectionsRelatedByIconIdScheduledForDeletion as $sectionRelatedByIconId) {
                        // need to save related object because we set the relation to null
                        $sectionRelatedByIconId->save($con);
                    }
                    $this->sectionsRelatedByIconIdScheduledForDeletion = null;
                }
            }

            if ($this->collSectionsRelatedByIconId !== null) {
                foreach ($this->collSectionsRelatedByIconId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sectionsRelatedByIconSelectedIdScheduledForDeletion !== null) {
                if (!$this->sectionsRelatedByIconSelectedIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->sectionsRelatedByIconSelectedIdScheduledForDeletion as $sectionRelatedByIconSelectedId) {
                        // need to save related object because we set the relation to null
                        $sectionRelatedByIconSelectedId->save($con);
                    }
                    $this->sectionsRelatedByIconSelectedIdScheduledForDeletion = null;
                }
            }

            if ($this->collSectionsRelatedByIconSelectedId !== null) {
                foreach ($this->collSectionsRelatedByIconSelectedId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[MediaTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . MediaTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(MediaTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(MediaTableMap::COL_DEVICE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'device_id';
        }
        if ($this->isColumnModified(MediaTableMap::COL_IS_PUBLIC)) {
            $modifiedColumns[':p' . $index++]  = 'is_public';
        }
        if ($this->isColumnModified(MediaTableMap::COL_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'type';
        }
        if ($this->isColumnModified(MediaTableMap::COL_URI)) {
            $modifiedColumns[':p' . $index++]  = 'uri';
        }
        if ($this->isColumnModified(MediaTableMap::COL_URI_THUMB)) {
            $modifiedColumns[':p' . $index++]  = 'uri_thumb';
        }
        if ($this->isColumnModified(MediaTableMap::COL_EXTRA_PARAMS)) {
            $modifiedColumns[':p' . $index++]  = 'extra_params';
        }
        if ($this->isColumnModified(MediaTableMap::COL_WEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'weight';
        }
        if ($this->isColumnModified(MediaTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(MediaTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(MediaTableMap::COL_EXTENSION)) {
            $modifiedColumns[':p' . $index++]  = 'extension';
        }
        if ($this->isColumnModified(MediaTableMap::COL_MIME_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'mime_type';
        }
        if ($this->isColumnModified(MediaTableMap::COL_SIZE)) {
            $modifiedColumns[':p' . $index++]  = 'size';
        }
        if ($this->isColumnModified(MediaTableMap::COL_COUNT_LIKE)) {
            $modifiedColumns[':p' . $index++]  = 'count_like';
        }
        if ($this->isColumnModified(MediaTableMap::COL_COUNT_SHARE)) {
            $modifiedColumns[':p' . $index++]  = 'count_share';
        }
        if ($this->isColumnModified(MediaTableMap::COL_FORMAT)) {
            $modifiedColumns[':p' . $index++]  = 'format';
        }
        if ($this->isColumnModified(MediaTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }
        if ($this->isColumnModified(MediaTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(MediaTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO media (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'device_id':
                        $stmt->bindValue($identifier, $this->device_id, PDO::PARAM_INT);
                        break;
                    case 'is_public':
                        $stmt->bindValue($identifier, $this->is_public, PDO::PARAM_INT);
                        break;
                    case 'type':
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_STR);
                        break;
                    case 'uri':
                        $stmt->bindValue($identifier, $this->uri, PDO::PARAM_STR);
                        break;
                    case 'uri_thumb':
                        $stmt->bindValue($identifier, $this->uri_thumb, PDO::PARAM_STR);
                        break;
                    case 'extra_params':
                        $stmt->bindValue($identifier, $this->extra_params, PDO::PARAM_STR);
                        break;
                    case 'weight':
                        $stmt->bindValue($identifier, $this->weight, PDO::PARAM_INT);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'extension':
                        $stmt->bindValue($identifier, $this->extension, PDO::PARAM_STR);
                        break;
                    case 'mime_type':
                        $stmt->bindValue($identifier, $this->mime_type, PDO::PARAM_STR);
                        break;
                    case 'size':
                        $stmt->bindValue($identifier, $this->size, PDO::PARAM_INT);
                        break;
                    case 'count_like':
                        $stmt->bindValue($identifier, $this->count_like, PDO::PARAM_INT);
                        break;
                    case 'count_share':
                        $stmt->bindValue($identifier, $this->count_share, PDO::PARAM_INT);
                        break;
                    case 'format':
                        $stmt->bindValue($identifier, $this->format, PDO::PARAM_STR);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MediaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getDeviceId();
                break;
            case 2:
                return $this->getIsPublic();
                break;
            case 3:
                return $this->getType();
                break;
            case 4:
                return $this->getUri();
                break;
            case 5:
                return $this->getUriThumb();
                break;
            case 6:
                return $this->getExtraParams();
                break;
            case 7:
                return $this->getWeight();
                break;
            case 8:
                return $this->getTitle();
                break;
            case 9:
                return $this->getDescription();
                break;
            case 10:
                return $this->getExtension();
                break;
            case 11:
                return $this->getMimeType();
                break;
            case 12:
                return $this->getSize();
                break;
            case 13:
                return $this->getCountLike();
                break;
            case 14:
                return $this->getCountShare();
                break;
            case 15:
                return $this->getFormat();
                break;
            case 16:
                return $this->getDeletedAt();
                break;
            case 17:
                return $this->getCreatedAt();
                break;
            case 18:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Media'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Media'][$this->hashCode()] = true;
        $keys = MediaTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getDeviceId(),
            $keys[2] => $this->getIsPublic(),
            $keys[3] => $this->getType(),
            $keys[4] => $this->getUri(),
            $keys[5] => $this->getUriThumb(),
            $keys[6] => $this->getExtraParams(),
            $keys[7] => $this->getWeight(),
            $keys[8] => $this->getTitle(),
            $keys[9] => $this->getDescription(),
            $keys[10] => $this->getExtension(),
            $keys[11] => $this->getMimeType(),
            $keys[12] => $this->getSize(),
            $keys[13] => $this->getCountLike(),
            $keys[14] => $this->getCountShare(),
            $keys[15] => $this->getFormat(),
            $keys[16] => $this->getDeletedAt(),
            $keys[17] => $this->getCreatedAt(),
            $keys[18] => $this->getUpdatedAt(),
        );
        if ($result[$keys[16]] instanceof \DateTimeInterface) {
            $result[$keys[16]] = $result[$keys[16]]->format('c');
        }

        if ($result[$keys[17]] instanceof \DateTimeInterface) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTimeInterface) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aDevice) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'device';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'device';
                        break;
                    default:
                        $key = 'Device';
                }

                $result[$key] = $this->aDevice->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collGalleries) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'galleries';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'galleries';
                        break;
                    default:
                        $key = 'Galleries';
                }

                $result[$key] = $this->collGalleries->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collGalleryForms) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'galleryForms';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'gallery_forms';
                        break;
                    default:
                        $key = 'GalleryForms';
                }

                $result[$key] = $this->collGalleryForms->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMediaExtraLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mediaExtraLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'media_extra_logs';
                        break;
                    default:
                        $key = 'MediaExtraLogs';
                }

                $result[$key] = $this->collMediaExtraLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMediaLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mediaLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'media_logs';
                        break;
                    default:
                        $key = 'MediaLogs';
                }

                $result[$key] = $this->collMediaLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPosts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'posts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'posts';
                        break;
                    default:
                        $key = 'Posts';
                }

                $result[$key] = $this->collPosts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPostActions) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postActions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_actions';
                        break;
                    default:
                        $key = 'PostActions';
                }

                $result[$key] = $this->collPostActions->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSectionsRelatedByIconId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sections';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sections';
                        break;
                    default:
                        $key = 'Sections';
                }

                $result[$key] = $this->collSectionsRelatedByIconId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSectionsRelatedByIconSelectedId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sections';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sections';
                        break;
                    default:
                        $key = 'Sections';
                }

                $result[$key] = $this->collSectionsRelatedByIconSelectedId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Database\HubPlus\Media
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MediaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Database\HubPlus\Media
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setDeviceId($value);
                break;
            case 2:
                $this->setIsPublic($value);
                break;
            case 3:
                $this->setType($value);
                break;
            case 4:
                $this->setUri($value);
                break;
            case 5:
                $this->setUriThumb($value);
                break;
            case 6:
                $this->setExtraParams($value);
                break;
            case 7:
                $this->setWeight($value);
                break;
            case 8:
                $this->setTitle($value);
                break;
            case 9:
                $this->setDescription($value);
                break;
            case 10:
                $this->setExtension($value);
                break;
            case 11:
                $this->setMimeType($value);
                break;
            case 12:
                $this->setSize($value);
                break;
            case 13:
                $this->setCountLike($value);
                break;
            case 14:
                $this->setCountShare($value);
                break;
            case 15:
                $this->setFormat($value);
                break;
            case 16:
                $this->setDeletedAt($value);
                break;
            case 17:
                $this->setCreatedAt($value);
                break;
            case 18:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = MediaTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setDeviceId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setIsPublic($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setType($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setUri($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setUriThumb($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setExtraParams($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setWeight($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTitle($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setDescription($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setExtension($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setMimeType($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setSize($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setCountLike($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setCountShare($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setFormat($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setDeletedAt($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setCreatedAt($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setUpdatedAt($arr[$keys[18]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Database\HubPlus\Media The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(MediaTableMap::DATABASE_NAME);

        if ($this->isColumnModified(MediaTableMap::COL_ID)) {
            $criteria->add(MediaTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(MediaTableMap::COL_DEVICE_ID)) {
            $criteria->add(MediaTableMap::COL_DEVICE_ID, $this->device_id);
        }
        if ($this->isColumnModified(MediaTableMap::COL_IS_PUBLIC)) {
            $criteria->add(MediaTableMap::COL_IS_PUBLIC, $this->is_public);
        }
        if ($this->isColumnModified(MediaTableMap::COL_TYPE)) {
            $criteria->add(MediaTableMap::COL_TYPE, $this->type);
        }
        if ($this->isColumnModified(MediaTableMap::COL_URI)) {
            $criteria->add(MediaTableMap::COL_URI, $this->uri);
        }
        if ($this->isColumnModified(MediaTableMap::COL_URI_THUMB)) {
            $criteria->add(MediaTableMap::COL_URI_THUMB, $this->uri_thumb);
        }
        if ($this->isColumnModified(MediaTableMap::COL_EXTRA_PARAMS)) {
            $criteria->add(MediaTableMap::COL_EXTRA_PARAMS, $this->extra_params);
        }
        if ($this->isColumnModified(MediaTableMap::COL_WEIGHT)) {
            $criteria->add(MediaTableMap::COL_WEIGHT, $this->weight);
        }
        if ($this->isColumnModified(MediaTableMap::COL_TITLE)) {
            $criteria->add(MediaTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(MediaTableMap::COL_DESCRIPTION)) {
            $criteria->add(MediaTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(MediaTableMap::COL_EXTENSION)) {
            $criteria->add(MediaTableMap::COL_EXTENSION, $this->extension);
        }
        if ($this->isColumnModified(MediaTableMap::COL_MIME_TYPE)) {
            $criteria->add(MediaTableMap::COL_MIME_TYPE, $this->mime_type);
        }
        if ($this->isColumnModified(MediaTableMap::COL_SIZE)) {
            $criteria->add(MediaTableMap::COL_SIZE, $this->size);
        }
        if ($this->isColumnModified(MediaTableMap::COL_COUNT_LIKE)) {
            $criteria->add(MediaTableMap::COL_COUNT_LIKE, $this->count_like);
        }
        if ($this->isColumnModified(MediaTableMap::COL_COUNT_SHARE)) {
            $criteria->add(MediaTableMap::COL_COUNT_SHARE, $this->count_share);
        }
        if ($this->isColumnModified(MediaTableMap::COL_FORMAT)) {
            $criteria->add(MediaTableMap::COL_FORMAT, $this->format);
        }
        if ($this->isColumnModified(MediaTableMap::COL_DELETED_AT)) {
            $criteria->add(MediaTableMap::COL_DELETED_AT, $this->deleted_at);
        }
        if ($this->isColumnModified(MediaTableMap::COL_CREATED_AT)) {
            $criteria->add(MediaTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(MediaTableMap::COL_UPDATED_AT)) {
            $criteria->add(MediaTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildMediaQuery::create();
        $criteria->add(MediaTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Database\HubPlus\Media (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDeviceId($this->getDeviceId());
        $copyObj->setIsPublic($this->getIsPublic());
        $copyObj->setType($this->getType());
        $copyObj->setUri($this->getUri());
        $copyObj->setUriThumb($this->getUriThumb());
        $copyObj->setExtraParams($this->getExtraParams());
        $copyObj->setWeight($this->getWeight());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setExtension($this->getExtension());
        $copyObj->setMimeType($this->getMimeType());
        $copyObj->setSize($this->getSize());
        $copyObj->setCountLike($this->getCountLike());
        $copyObj->setCountShare($this->getCountShare());
        $copyObj->setFormat($this->getFormat());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getGalleries() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addGallery($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getGalleryForms() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addGalleryForm($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMediaExtraLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMediaExtraLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMediaLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMediaLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPosts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPost($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPostActions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostAction($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSectionsRelatedByIconId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSectionRelatedByIconId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSectionsRelatedByIconSelectedId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSectionRelatedByIconSelectedId($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Database\HubPlus\Media Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildDevice object.
     *
     * @param  ChildDevice $v
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     * @throws PropelException
     */
    public function setDevice(ChildDevice $v = null)
    {
        if ($v === null) {
            $this->setDeviceId(NULL);
        } else {
            $this->setDeviceId($v->getId());
        }

        $this->aDevice = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildDevice object, it will not be re-added.
        if ($v !== null) {
            $v->addMedia($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildDevice object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildDevice The associated ChildDevice object.
     * @throws PropelException
     */
    public function getDevice(ConnectionInterface $con = null)
    {
        if ($this->aDevice === null && ($this->device_id != 0)) {
            $this->aDevice = ChildDeviceQuery::create()->findPk($this->device_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aDevice->addMedias($this);
             */
        }

        return $this->aDevice;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Gallery' == $relationName) {
            $this->initGalleries();
            return;
        }
        if ('GalleryForm' == $relationName) {
            $this->initGalleryForms();
            return;
        }
        if ('MediaExtraLog' == $relationName) {
            $this->initMediaExtraLogs();
            return;
        }
        if ('MediaLog' == $relationName) {
            $this->initMediaLogs();
            return;
        }
        if ('Post' == $relationName) {
            $this->initPosts();
            return;
        }
        if ('PostAction' == $relationName) {
            $this->initPostActions();
            return;
        }
        if ('SectionRelatedByIconId' == $relationName) {
            $this->initSectionsRelatedByIconId();
            return;
        }
        if ('SectionRelatedByIconSelectedId' == $relationName) {
            $this->initSectionsRelatedByIconSelectedId();
            return;
        }
    }

    /**
     * Clears out the collGalleries collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addGalleries()
     */
    public function clearGalleries()
    {
        $this->collGalleries = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collGalleries collection loaded partially.
     */
    public function resetPartialGalleries($v = true)
    {
        $this->collGalleriesPartial = $v;
    }

    /**
     * Initializes the collGalleries collection.
     *
     * By default this just sets the collGalleries collection to an empty array (like clearcollGalleries());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initGalleries($overrideExisting = true)
    {
        if (null !== $this->collGalleries && !$overrideExisting) {
            return;
        }

        $collectionClassName = GalleryTableMap::getTableMap()->getCollectionClassName();

        $this->collGalleries = new $collectionClassName;
        $this->collGalleries->setModel('\Database\HubPlus\Gallery');
    }

    /**
     * Gets an array of ChildGallery objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMedia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildGallery[] List of ChildGallery objects
     * @throws PropelException
     */
    public function getGalleries(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collGalleriesPartial && !$this->isNew();
        if (null === $this->collGalleries || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collGalleries) {
                // return empty collection
                $this->initGalleries();
            } else {
                $collGalleries = ChildGalleryQuery::create(null, $criteria)
                    ->filterByMedia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collGalleriesPartial && count($collGalleries)) {
                        $this->initGalleries(false);

                        foreach ($collGalleries as $obj) {
                            if (false == $this->collGalleries->contains($obj)) {
                                $this->collGalleries->append($obj);
                            }
                        }

                        $this->collGalleriesPartial = true;
                    }

                    return $collGalleries;
                }

                if ($partial && $this->collGalleries) {
                    foreach ($this->collGalleries as $obj) {
                        if ($obj->isNew()) {
                            $collGalleries[] = $obj;
                        }
                    }
                }

                $this->collGalleries = $collGalleries;
                $this->collGalleriesPartial = false;
            }
        }

        return $this->collGalleries;
    }

    /**
     * Sets a collection of ChildGallery objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $galleries A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function setGalleries(Collection $galleries, ConnectionInterface $con = null)
    {
        /** @var ChildGallery[] $galleriesToDelete */
        $galleriesToDelete = $this->getGalleries(new Criteria(), $con)->diff($galleries);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->galleriesScheduledForDeletion = clone $galleriesToDelete;

        foreach ($galleriesToDelete as $galleryRemoved) {
            $galleryRemoved->setMedia(null);
        }

        $this->collGalleries = null;
        foreach ($galleries as $gallery) {
            $this->addGallery($gallery);
        }

        $this->collGalleries = $galleries;
        $this->collGalleriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Gallery objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Gallery objects.
     * @throws PropelException
     */
    public function countGalleries(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collGalleriesPartial && !$this->isNew();
        if (null === $this->collGalleries || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collGalleries) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getGalleries());
            }

            $query = ChildGalleryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMedia($this)
                ->count($con);
        }

        return count($this->collGalleries);
    }

    /**
     * Method called to associate a ChildGallery object to this object
     * through the ChildGallery foreign key attribute.
     *
     * @param  ChildGallery $l ChildGallery
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function addGallery(ChildGallery $l)
    {
        if ($this->collGalleries === null) {
            $this->initGalleries();
            $this->collGalleriesPartial = true;
        }

        if (!$this->collGalleries->contains($l)) {
            $this->doAddGallery($l);

            if ($this->galleriesScheduledForDeletion and $this->galleriesScheduledForDeletion->contains($l)) {
                $this->galleriesScheduledForDeletion->remove($this->galleriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildGallery $gallery The ChildGallery object to add.
     */
    protected function doAddGallery(ChildGallery $gallery)
    {
        $this->collGalleries[]= $gallery;
        $gallery->setMedia($this);
    }

    /**
     * @param  ChildGallery $gallery The ChildGallery object to remove.
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function removeGallery(ChildGallery $gallery)
    {
        if ($this->getGalleries()->contains($gallery)) {
            $pos = $this->collGalleries->search($gallery);
            $this->collGalleries->remove($pos);
            if (null === $this->galleriesScheduledForDeletion) {
                $this->galleriesScheduledForDeletion = clone $this->collGalleries;
                $this->galleriesScheduledForDeletion->clear();
            }
            $this->galleriesScheduledForDeletion[]= clone $gallery;
            $gallery->setMedia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Media is new, it will return
     * an empty collection; or if this Media has previously
     * been saved, it will retrieve related Galleries from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Media.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildGallery[] List of ChildGallery objects
     */
    public function getGalleriesJoinPost(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildGalleryQuery::create(null, $criteria);
        $query->joinWith('Post', $joinBehavior);

        return $this->getGalleries($query, $con);
    }

    /**
     * Clears out the collGalleryForms collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addGalleryForms()
     */
    public function clearGalleryForms()
    {
        $this->collGalleryForms = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collGalleryForms collection loaded partially.
     */
    public function resetPartialGalleryForms($v = true)
    {
        $this->collGalleryFormsPartial = $v;
    }

    /**
     * Initializes the collGalleryForms collection.
     *
     * By default this just sets the collGalleryForms collection to an empty array (like clearcollGalleryForms());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initGalleryForms($overrideExisting = true)
    {
        if (null !== $this->collGalleryForms && !$overrideExisting) {
            return;
        }

        $collectionClassName = GalleryFormTableMap::getTableMap()->getCollectionClassName();

        $this->collGalleryForms = new $collectionClassName;
        $this->collGalleryForms->setModel('\Database\HubPlus\GalleryForm');
    }

    /**
     * Gets an array of ChildGalleryForm objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMedia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildGalleryForm[] List of ChildGalleryForm objects
     * @throws PropelException
     */
    public function getGalleryForms(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collGalleryFormsPartial && !$this->isNew();
        if (null === $this->collGalleryForms || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collGalleryForms) {
                // return empty collection
                $this->initGalleryForms();
            } else {
                $collGalleryForms = ChildGalleryFormQuery::create(null, $criteria)
                    ->filterByMedia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collGalleryFormsPartial && count($collGalleryForms)) {
                        $this->initGalleryForms(false);

                        foreach ($collGalleryForms as $obj) {
                            if (false == $this->collGalleryForms->contains($obj)) {
                                $this->collGalleryForms->append($obj);
                            }
                        }

                        $this->collGalleryFormsPartial = true;
                    }

                    return $collGalleryForms;
                }

                if ($partial && $this->collGalleryForms) {
                    foreach ($this->collGalleryForms as $obj) {
                        if ($obj->isNew()) {
                            $collGalleryForms[] = $obj;
                        }
                    }
                }

                $this->collGalleryForms = $collGalleryForms;
                $this->collGalleryFormsPartial = false;
            }
        }

        return $this->collGalleryForms;
    }

    /**
     * Sets a collection of ChildGalleryForm objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $galleryForms A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function setGalleryForms(Collection $galleryForms, ConnectionInterface $con = null)
    {
        /** @var ChildGalleryForm[] $galleryFormsToDelete */
        $galleryFormsToDelete = $this->getGalleryForms(new Criteria(), $con)->diff($galleryForms);


        $this->galleryFormsScheduledForDeletion = $galleryFormsToDelete;

        foreach ($galleryFormsToDelete as $galleryFormRemoved) {
            $galleryFormRemoved->setMedia(null);
        }

        $this->collGalleryForms = null;
        foreach ($galleryForms as $galleryForm) {
            $this->addGalleryForm($galleryForm);
        }

        $this->collGalleryForms = $galleryForms;
        $this->collGalleryFormsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related GalleryForm objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related GalleryForm objects.
     * @throws PropelException
     */
    public function countGalleryForms(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collGalleryFormsPartial && !$this->isNew();
        if (null === $this->collGalleryForms || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collGalleryForms) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getGalleryForms());
            }

            $query = ChildGalleryFormQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMedia($this)
                ->count($con);
        }

        return count($this->collGalleryForms);
    }

    /**
     * Method called to associate a ChildGalleryForm object to this object
     * through the ChildGalleryForm foreign key attribute.
     *
     * @param  ChildGalleryForm $l ChildGalleryForm
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function addGalleryForm(ChildGalleryForm $l)
    {
        if ($this->collGalleryForms === null) {
            $this->initGalleryForms();
            $this->collGalleryFormsPartial = true;
        }

        if (!$this->collGalleryForms->contains($l)) {
            $this->doAddGalleryForm($l);

            if ($this->galleryFormsScheduledForDeletion and $this->galleryFormsScheduledForDeletion->contains($l)) {
                $this->galleryFormsScheduledForDeletion->remove($this->galleryFormsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildGalleryForm $galleryForm The ChildGalleryForm object to add.
     */
    protected function doAddGalleryForm(ChildGalleryForm $galleryForm)
    {
        $this->collGalleryForms[]= $galleryForm;
        $galleryForm->setMedia($this);
    }

    /**
     * @param  ChildGalleryForm $galleryForm The ChildGalleryForm object to remove.
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function removeGalleryForm(ChildGalleryForm $galleryForm)
    {
        if ($this->getGalleryForms()->contains($galleryForm)) {
            $pos = $this->collGalleryForms->search($galleryForm);
            $this->collGalleryForms->remove($pos);
            if (null === $this->galleryFormsScheduledForDeletion) {
                $this->galleryFormsScheduledForDeletion = clone $this->collGalleryForms;
                $this->galleryFormsScheduledForDeletion->clear();
            }
            $this->galleryFormsScheduledForDeletion[]= $galleryForm;
            $galleryForm->setMedia(null);
        }

        return $this;
    }

    /**
     * Clears out the collMediaExtraLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMediaExtraLogs()
     */
    public function clearMediaExtraLogs()
    {
        $this->collMediaExtraLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMediaExtraLogs collection loaded partially.
     */
    public function resetPartialMediaExtraLogs($v = true)
    {
        $this->collMediaExtraLogsPartial = $v;
    }

    /**
     * Initializes the collMediaExtraLogs collection.
     *
     * By default this just sets the collMediaExtraLogs collection to an empty array (like clearcollMediaExtraLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMediaExtraLogs($overrideExisting = true)
    {
        if (null !== $this->collMediaExtraLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = MediaExtraLogTableMap::getTableMap()->getCollectionClassName();

        $this->collMediaExtraLogs = new $collectionClassName;
        $this->collMediaExtraLogs->setModel('\Database\HubPlus\MediaExtraLog');
    }

    /**
     * Gets an array of ChildMediaExtraLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMedia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMediaExtraLog[] List of ChildMediaExtraLog objects
     * @throws PropelException
     */
    public function getMediaExtraLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMediaExtraLogsPartial && !$this->isNew();
        if (null === $this->collMediaExtraLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMediaExtraLogs) {
                // return empty collection
                $this->initMediaExtraLogs();
            } else {
                $collMediaExtraLogs = ChildMediaExtraLogQuery::create(null, $criteria)
                    ->filterByMedia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMediaExtraLogsPartial && count($collMediaExtraLogs)) {
                        $this->initMediaExtraLogs(false);

                        foreach ($collMediaExtraLogs as $obj) {
                            if (false == $this->collMediaExtraLogs->contains($obj)) {
                                $this->collMediaExtraLogs->append($obj);
                            }
                        }

                        $this->collMediaExtraLogsPartial = true;
                    }

                    return $collMediaExtraLogs;
                }

                if ($partial && $this->collMediaExtraLogs) {
                    foreach ($this->collMediaExtraLogs as $obj) {
                        if ($obj->isNew()) {
                            $collMediaExtraLogs[] = $obj;
                        }
                    }
                }

                $this->collMediaExtraLogs = $collMediaExtraLogs;
                $this->collMediaExtraLogsPartial = false;
            }
        }

        return $this->collMediaExtraLogs;
    }

    /**
     * Sets a collection of ChildMediaExtraLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $mediaExtraLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function setMediaExtraLogs(Collection $mediaExtraLogs, ConnectionInterface $con = null)
    {
        /** @var ChildMediaExtraLog[] $mediaExtraLogsToDelete */
        $mediaExtraLogsToDelete = $this->getMediaExtraLogs(new Criteria(), $con)->diff($mediaExtraLogs);


        $this->mediaExtraLogsScheduledForDeletion = $mediaExtraLogsToDelete;

        foreach ($mediaExtraLogsToDelete as $mediaExtraLogRemoved) {
            $mediaExtraLogRemoved->setMedia(null);
        }

        $this->collMediaExtraLogs = null;
        foreach ($mediaExtraLogs as $mediaExtraLog) {
            $this->addMediaExtraLog($mediaExtraLog);
        }

        $this->collMediaExtraLogs = $mediaExtraLogs;
        $this->collMediaExtraLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MediaExtraLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MediaExtraLog objects.
     * @throws PropelException
     */
    public function countMediaExtraLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMediaExtraLogsPartial && !$this->isNew();
        if (null === $this->collMediaExtraLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMediaExtraLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMediaExtraLogs());
            }

            $query = ChildMediaExtraLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMedia($this)
                ->count($con);
        }

        return count($this->collMediaExtraLogs);
    }

    /**
     * Method called to associate a ChildMediaExtraLog object to this object
     * through the ChildMediaExtraLog foreign key attribute.
     *
     * @param  ChildMediaExtraLog $l ChildMediaExtraLog
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function addMediaExtraLog(ChildMediaExtraLog $l)
    {
        if ($this->collMediaExtraLogs === null) {
            $this->initMediaExtraLogs();
            $this->collMediaExtraLogsPartial = true;
        }

        if (!$this->collMediaExtraLogs->contains($l)) {
            $this->doAddMediaExtraLog($l);

            if ($this->mediaExtraLogsScheduledForDeletion and $this->mediaExtraLogsScheduledForDeletion->contains($l)) {
                $this->mediaExtraLogsScheduledForDeletion->remove($this->mediaExtraLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMediaExtraLog $mediaExtraLog The ChildMediaExtraLog object to add.
     */
    protected function doAddMediaExtraLog(ChildMediaExtraLog $mediaExtraLog)
    {
        $this->collMediaExtraLogs[]= $mediaExtraLog;
        $mediaExtraLog->setMedia($this);
    }

    /**
     * @param  ChildMediaExtraLog $mediaExtraLog The ChildMediaExtraLog object to remove.
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function removeMediaExtraLog(ChildMediaExtraLog $mediaExtraLog)
    {
        if ($this->getMediaExtraLogs()->contains($mediaExtraLog)) {
            $pos = $this->collMediaExtraLogs->search($mediaExtraLog);
            $this->collMediaExtraLogs->remove($pos);
            if (null === $this->mediaExtraLogsScheduledForDeletion) {
                $this->mediaExtraLogsScheduledForDeletion = clone $this->collMediaExtraLogs;
                $this->mediaExtraLogsScheduledForDeletion->clear();
            }
            $this->mediaExtraLogsScheduledForDeletion[]= $mediaExtraLog;
            $mediaExtraLog->setMedia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Media is new, it will return
     * an empty collection; or if this Media has previously
     * been saved, it will retrieve related MediaExtraLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Media.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMediaExtraLog[] List of ChildMediaExtraLog objects
     */
    public function getMediaExtraLogsJoinDevice(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMediaExtraLogQuery::create(null, $criteria);
        $query->joinWith('Device', $joinBehavior);

        return $this->getMediaExtraLogs($query, $con);
    }

    /**
     * Clears out the collMediaLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMediaLogs()
     */
    public function clearMediaLogs()
    {
        $this->collMediaLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMediaLogs collection loaded partially.
     */
    public function resetPartialMediaLogs($v = true)
    {
        $this->collMediaLogsPartial = $v;
    }

    /**
     * Initializes the collMediaLogs collection.
     *
     * By default this just sets the collMediaLogs collection to an empty array (like clearcollMediaLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMediaLogs($overrideExisting = true)
    {
        if (null !== $this->collMediaLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = MediaLogTableMap::getTableMap()->getCollectionClassName();

        $this->collMediaLogs = new $collectionClassName;
        $this->collMediaLogs->setModel('\Database\HubPlus\MediaLog');
    }

    /**
     * Gets an array of ChildMediaLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMedia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMediaLog[] List of ChildMediaLog objects
     * @throws PropelException
     */
    public function getMediaLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMediaLogsPartial && !$this->isNew();
        if (null === $this->collMediaLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMediaLogs) {
                // return empty collection
                $this->initMediaLogs();
            } else {
                $collMediaLogs = ChildMediaLogQuery::create(null, $criteria)
                    ->filterByMedia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMediaLogsPartial && count($collMediaLogs)) {
                        $this->initMediaLogs(false);

                        foreach ($collMediaLogs as $obj) {
                            if (false == $this->collMediaLogs->contains($obj)) {
                                $this->collMediaLogs->append($obj);
                            }
                        }

                        $this->collMediaLogsPartial = true;
                    }

                    return $collMediaLogs;
                }

                if ($partial && $this->collMediaLogs) {
                    foreach ($this->collMediaLogs as $obj) {
                        if ($obj->isNew()) {
                            $collMediaLogs[] = $obj;
                        }
                    }
                }

                $this->collMediaLogs = $collMediaLogs;
                $this->collMediaLogsPartial = false;
            }
        }

        return $this->collMediaLogs;
    }

    /**
     * Sets a collection of ChildMediaLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $mediaLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function setMediaLogs(Collection $mediaLogs, ConnectionInterface $con = null)
    {
        /** @var ChildMediaLog[] $mediaLogsToDelete */
        $mediaLogsToDelete = $this->getMediaLogs(new Criteria(), $con)->diff($mediaLogs);


        $this->mediaLogsScheduledForDeletion = $mediaLogsToDelete;

        foreach ($mediaLogsToDelete as $mediaLogRemoved) {
            $mediaLogRemoved->setMedia(null);
        }

        $this->collMediaLogs = null;
        foreach ($mediaLogs as $mediaLog) {
            $this->addMediaLog($mediaLog);
        }

        $this->collMediaLogs = $mediaLogs;
        $this->collMediaLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MediaLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MediaLog objects.
     * @throws PropelException
     */
    public function countMediaLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMediaLogsPartial && !$this->isNew();
        if (null === $this->collMediaLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMediaLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMediaLogs());
            }

            $query = ChildMediaLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMedia($this)
                ->count($con);
        }

        return count($this->collMediaLogs);
    }

    /**
     * Method called to associate a ChildMediaLog object to this object
     * through the ChildMediaLog foreign key attribute.
     *
     * @param  ChildMediaLog $l ChildMediaLog
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function addMediaLog(ChildMediaLog $l)
    {
        if ($this->collMediaLogs === null) {
            $this->initMediaLogs();
            $this->collMediaLogsPartial = true;
        }

        if (!$this->collMediaLogs->contains($l)) {
            $this->doAddMediaLog($l);

            if ($this->mediaLogsScheduledForDeletion and $this->mediaLogsScheduledForDeletion->contains($l)) {
                $this->mediaLogsScheduledForDeletion->remove($this->mediaLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMediaLog $mediaLog The ChildMediaLog object to add.
     */
    protected function doAddMediaLog(ChildMediaLog $mediaLog)
    {
        $this->collMediaLogs[]= $mediaLog;
        $mediaLog->setMedia($this);
    }

    /**
     * @param  ChildMediaLog $mediaLog The ChildMediaLog object to remove.
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function removeMediaLog(ChildMediaLog $mediaLog)
    {
        if ($this->getMediaLogs()->contains($mediaLog)) {
            $pos = $this->collMediaLogs->search($mediaLog);
            $this->collMediaLogs->remove($pos);
            if (null === $this->mediaLogsScheduledForDeletion) {
                $this->mediaLogsScheduledForDeletion = clone $this->collMediaLogs;
                $this->mediaLogsScheduledForDeletion->clear();
            }
            $this->mediaLogsScheduledForDeletion[]= $mediaLog;
            $mediaLog->setMedia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Media is new, it will return
     * an empty collection; or if this Media has previously
     * been saved, it will retrieve related MediaLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Media.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMediaLog[] List of ChildMediaLog objects
     */
    public function getMediaLogsJoinUserApp(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMediaLogQuery::create(null, $criteria);
        $query->joinWith('UserApp', $joinBehavior);

        return $this->getMediaLogs($query, $con);
    }

    /**
     * Clears out the collPosts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPosts()
     */
    public function clearPosts()
    {
        $this->collPosts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPosts collection loaded partially.
     */
    public function resetPartialPosts($v = true)
    {
        $this->collPostsPartial = $v;
    }

    /**
     * Initializes the collPosts collection.
     *
     * By default this just sets the collPosts collection to an empty array (like clearcollPosts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPosts($overrideExisting = true)
    {
        if (null !== $this->collPosts && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostTableMap::getTableMap()->getCollectionClassName();

        $this->collPosts = new $collectionClassName;
        $this->collPosts->setModel('\Database\HubPlus\Post');
    }

    /**
     * Gets an array of ChildPost objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMedia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPost[] List of ChildPost objects
     * @throws PropelException
     */
    public function getPosts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostsPartial && !$this->isNew();
        if (null === $this->collPosts || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPosts) {
                // return empty collection
                $this->initPosts();
            } else {
                $collPosts = ChildPostQuery::create(null, $criteria)
                    ->filterByMedia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostsPartial && count($collPosts)) {
                        $this->initPosts(false);

                        foreach ($collPosts as $obj) {
                            if (false == $this->collPosts->contains($obj)) {
                                $this->collPosts->append($obj);
                            }
                        }

                        $this->collPostsPartial = true;
                    }

                    return $collPosts;
                }

                if ($partial && $this->collPosts) {
                    foreach ($this->collPosts as $obj) {
                        if ($obj->isNew()) {
                            $collPosts[] = $obj;
                        }
                    }
                }

                $this->collPosts = $collPosts;
                $this->collPostsPartial = false;
            }
        }

        return $this->collPosts;
    }

    /**
     * Sets a collection of ChildPost objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $posts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function setPosts(Collection $posts, ConnectionInterface $con = null)
    {
        /** @var ChildPost[] $postsToDelete */
        $postsToDelete = $this->getPosts(new Criteria(), $con)->diff($posts);


        $this->postsScheduledForDeletion = $postsToDelete;

        foreach ($postsToDelete as $postRemoved) {
            $postRemoved->setMedia(null);
        }

        $this->collPosts = null;
        foreach ($posts as $post) {
            $this->addPost($post);
        }

        $this->collPosts = $posts;
        $this->collPostsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Post objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Post objects.
     * @throws PropelException
     */
    public function countPosts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostsPartial && !$this->isNew();
        if (null === $this->collPosts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPosts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPosts());
            }

            $query = ChildPostQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMedia($this)
                ->count($con);
        }

        return count($this->collPosts);
    }

    /**
     * Method called to associate a ChildPost object to this object
     * through the ChildPost foreign key attribute.
     *
     * @param  ChildPost $l ChildPost
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function addPost(ChildPost $l)
    {
        if ($this->collPosts === null) {
            $this->initPosts();
            $this->collPostsPartial = true;
        }

        if (!$this->collPosts->contains($l)) {
            $this->doAddPost($l);

            if ($this->postsScheduledForDeletion and $this->postsScheduledForDeletion->contains($l)) {
                $this->postsScheduledForDeletion->remove($this->postsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPost $post The ChildPost object to add.
     */
    protected function doAddPost(ChildPost $post)
    {
        $this->collPosts[]= $post;
        $post->setMedia($this);
    }

    /**
     * @param  ChildPost $post The ChildPost object to remove.
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function removePost(ChildPost $post)
    {
        if ($this->getPosts()->contains($post)) {
            $pos = $this->collPosts->search($post);
            $this->collPosts->remove($pos);
            if (null === $this->postsScheduledForDeletion) {
                $this->postsScheduledForDeletion = clone $this->collPosts;
                $this->postsScheduledForDeletion->clear();
            }
            $this->postsScheduledForDeletion[]= $post;
            $post->setMedia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Media is new, it will return
     * an empty collection; or if this Media has previously
     * been saved, it will retrieve related Posts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Media.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPost[] List of ChildPost objects
     */
    public function getPostsJoinTemplate(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostQuery::create(null, $criteria);
        $query->joinWith('Template', $joinBehavior);

        return $this->getPosts($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Media is new, it will return
     * an empty collection; or if this Media has previously
     * been saved, it will retrieve related Posts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Media.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPost[] List of ChildPost objects
     */
    public function getPostsJoinUserBackend(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostQuery::create(null, $criteria);
        $query->joinWith('UserBackend', $joinBehavior);

        return $this->getPosts($query, $con);
    }

    /**
     * Clears out the collPostActions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostActions()
     */
    public function clearPostActions()
    {
        $this->collPostActions = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostActions collection loaded partially.
     */
    public function resetPartialPostActions($v = true)
    {
        $this->collPostActionsPartial = $v;
    }

    /**
     * Initializes the collPostActions collection.
     *
     * By default this just sets the collPostActions collection to an empty array (like clearcollPostActions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostActions($overrideExisting = true)
    {
        if (null !== $this->collPostActions && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostActionTableMap::getTableMap()->getCollectionClassName();

        $this->collPostActions = new $collectionClassName;
        $this->collPostActions->setModel('\Database\HubPlus\PostAction');
    }

    /**
     * Gets an array of ChildPostAction objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMedia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     * @throws PropelException
     */
    public function getPostActions(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostActionsPartial && !$this->isNew();
        if (null === $this->collPostActions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostActions) {
                // return empty collection
                $this->initPostActions();
            } else {
                $collPostActions = ChildPostActionQuery::create(null, $criteria)
                    ->filterByMedia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostActionsPartial && count($collPostActions)) {
                        $this->initPostActions(false);

                        foreach ($collPostActions as $obj) {
                            if (false == $this->collPostActions->contains($obj)) {
                                $this->collPostActions->append($obj);
                            }
                        }

                        $this->collPostActionsPartial = true;
                    }

                    return $collPostActions;
                }

                if ($partial && $this->collPostActions) {
                    foreach ($this->collPostActions as $obj) {
                        if ($obj->isNew()) {
                            $collPostActions[] = $obj;
                        }
                    }
                }

                $this->collPostActions = $collPostActions;
                $this->collPostActionsPartial = false;
            }
        }

        return $this->collPostActions;
    }

    /**
     * Sets a collection of ChildPostAction objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postActions A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function setPostActions(Collection $postActions, ConnectionInterface $con = null)
    {
        /** @var ChildPostAction[] $postActionsToDelete */
        $postActionsToDelete = $this->getPostActions(new Criteria(), $con)->diff($postActions);


        $this->postActionsScheduledForDeletion = $postActionsToDelete;

        foreach ($postActionsToDelete as $postActionRemoved) {
            $postActionRemoved->setMedia(null);
        }

        $this->collPostActions = null;
        foreach ($postActions as $postAction) {
            $this->addPostAction($postAction);
        }

        $this->collPostActions = $postActions;
        $this->collPostActionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostAction objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostAction objects.
     * @throws PropelException
     */
    public function countPostActions(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostActionsPartial && !$this->isNew();
        if (null === $this->collPostActions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostActions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostActions());
            }

            $query = ChildPostActionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMedia($this)
                ->count($con);
        }

        return count($this->collPostActions);
    }

    /**
     * Method called to associate a ChildPostAction object to this object
     * through the ChildPostAction foreign key attribute.
     *
     * @param  ChildPostAction $l ChildPostAction
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function addPostAction(ChildPostAction $l)
    {
        if ($this->collPostActions === null) {
            $this->initPostActions();
            $this->collPostActionsPartial = true;
        }

        if (!$this->collPostActions->contains($l)) {
            $this->doAddPostAction($l);

            if ($this->postActionsScheduledForDeletion and $this->postActionsScheduledForDeletion->contains($l)) {
                $this->postActionsScheduledForDeletion->remove($this->postActionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostAction $postAction The ChildPostAction object to add.
     */
    protected function doAddPostAction(ChildPostAction $postAction)
    {
        $this->collPostActions[]= $postAction;
        $postAction->setMedia($this);
    }

    /**
     * @param  ChildPostAction $postAction The ChildPostAction object to remove.
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function removePostAction(ChildPostAction $postAction)
    {
        if ($this->getPostActions()->contains($postAction)) {
            $pos = $this->collPostActions->search($postAction);
            $this->collPostActions->remove($pos);
            if (null === $this->postActionsScheduledForDeletion) {
                $this->postActionsScheduledForDeletion = clone $this->collPostActions;
                $this->postActionsScheduledForDeletion->clear();
            }
            $this->postActionsScheduledForDeletion[]= $postAction;
            $postAction->setMedia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Media is new, it will return
     * an empty collection; or if this Media has previously
     * been saved, it will retrieve related PostActions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Media.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     */
    public function getPostActionsJoinPostRelatedByPostId(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostActionQuery::create(null, $criteria);
        $query->joinWith('PostRelatedByPostId', $joinBehavior);

        return $this->getPostActions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Media is new, it will return
     * an empty collection; or if this Media has previously
     * been saved, it will retrieve related PostActions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Media.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     */
    public function getPostActionsJoinSection(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostActionQuery::create(null, $criteria);
        $query->joinWith('Section', $joinBehavior);

        return $this->getPostActions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Media is new, it will return
     * an empty collection; or if this Media has previously
     * been saved, it will retrieve related PostActions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Media.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     */
    public function getPostActionsJoinPostRelatedByContentId(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostActionQuery::create(null, $criteria);
        $query->joinWith('PostRelatedByContentId', $joinBehavior);

        return $this->getPostActions($query, $con);
    }

    /**
     * Clears out the collSectionsRelatedByIconId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSectionsRelatedByIconId()
     */
    public function clearSectionsRelatedByIconId()
    {
        $this->collSectionsRelatedByIconId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSectionsRelatedByIconId collection loaded partially.
     */
    public function resetPartialSectionsRelatedByIconId($v = true)
    {
        $this->collSectionsRelatedByIconIdPartial = $v;
    }

    /**
     * Initializes the collSectionsRelatedByIconId collection.
     *
     * By default this just sets the collSectionsRelatedByIconId collection to an empty array (like clearcollSectionsRelatedByIconId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSectionsRelatedByIconId($overrideExisting = true)
    {
        if (null !== $this->collSectionsRelatedByIconId && !$overrideExisting) {
            return;
        }

        $collectionClassName = SectionTableMap::getTableMap()->getCollectionClassName();

        $this->collSectionsRelatedByIconId = new $collectionClassName;
        $this->collSectionsRelatedByIconId->setModel('\Database\HubPlus\Section');
    }

    /**
     * Gets an array of ChildSection objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMedia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSection[] List of ChildSection objects
     * @throws PropelException
     */
    public function getSectionsRelatedByIconId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionsRelatedByIconIdPartial && !$this->isNew();
        if (null === $this->collSectionsRelatedByIconId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSectionsRelatedByIconId) {
                // return empty collection
                $this->initSectionsRelatedByIconId();
            } else {
                $collSectionsRelatedByIconId = ChildSectionQuery::create(null, $criteria)
                    ->filterByMediaRelatedByIconId($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSectionsRelatedByIconIdPartial && count($collSectionsRelatedByIconId)) {
                        $this->initSectionsRelatedByIconId(false);

                        foreach ($collSectionsRelatedByIconId as $obj) {
                            if (false == $this->collSectionsRelatedByIconId->contains($obj)) {
                                $this->collSectionsRelatedByIconId->append($obj);
                            }
                        }

                        $this->collSectionsRelatedByIconIdPartial = true;
                    }

                    return $collSectionsRelatedByIconId;
                }

                if ($partial && $this->collSectionsRelatedByIconId) {
                    foreach ($this->collSectionsRelatedByIconId as $obj) {
                        if ($obj->isNew()) {
                            $collSectionsRelatedByIconId[] = $obj;
                        }
                    }
                }

                $this->collSectionsRelatedByIconId = $collSectionsRelatedByIconId;
                $this->collSectionsRelatedByIconIdPartial = false;
            }
        }

        return $this->collSectionsRelatedByIconId;
    }

    /**
     * Sets a collection of ChildSection objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sectionsRelatedByIconId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function setSectionsRelatedByIconId(Collection $sectionsRelatedByIconId, ConnectionInterface $con = null)
    {
        /** @var ChildSection[] $sectionsRelatedByIconIdToDelete */
        $sectionsRelatedByIconIdToDelete = $this->getSectionsRelatedByIconId(new Criteria(), $con)->diff($sectionsRelatedByIconId);


        $this->sectionsRelatedByIconIdScheduledForDeletion = $sectionsRelatedByIconIdToDelete;

        foreach ($sectionsRelatedByIconIdToDelete as $sectionRelatedByIconIdRemoved) {
            $sectionRelatedByIconIdRemoved->setMediaRelatedByIconId(null);
        }

        $this->collSectionsRelatedByIconId = null;
        foreach ($sectionsRelatedByIconId as $sectionRelatedByIconId) {
            $this->addSectionRelatedByIconId($sectionRelatedByIconId);
        }

        $this->collSectionsRelatedByIconId = $sectionsRelatedByIconId;
        $this->collSectionsRelatedByIconIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Section objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Section objects.
     * @throws PropelException
     */
    public function countSectionsRelatedByIconId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionsRelatedByIconIdPartial && !$this->isNew();
        if (null === $this->collSectionsRelatedByIconId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSectionsRelatedByIconId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSectionsRelatedByIconId());
            }

            $query = ChildSectionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMediaRelatedByIconId($this)
                ->count($con);
        }

        return count($this->collSectionsRelatedByIconId);
    }

    /**
     * Method called to associate a ChildSection object to this object
     * through the ChildSection foreign key attribute.
     *
     * @param  ChildSection $l ChildSection
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function addSectionRelatedByIconId(ChildSection $l)
    {
        if ($this->collSectionsRelatedByIconId === null) {
            $this->initSectionsRelatedByIconId();
            $this->collSectionsRelatedByIconIdPartial = true;
        }

        if (!$this->collSectionsRelatedByIconId->contains($l)) {
            $this->doAddSectionRelatedByIconId($l);

            if ($this->sectionsRelatedByIconIdScheduledForDeletion and $this->sectionsRelatedByIconIdScheduledForDeletion->contains($l)) {
                $this->sectionsRelatedByIconIdScheduledForDeletion->remove($this->sectionsRelatedByIconIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSection $sectionRelatedByIconId The ChildSection object to add.
     */
    protected function doAddSectionRelatedByIconId(ChildSection $sectionRelatedByIconId)
    {
        $this->collSectionsRelatedByIconId[]= $sectionRelatedByIconId;
        $sectionRelatedByIconId->setMediaRelatedByIconId($this);
    }

    /**
     * @param  ChildSection $sectionRelatedByIconId The ChildSection object to remove.
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function removeSectionRelatedByIconId(ChildSection $sectionRelatedByIconId)
    {
        if ($this->getSectionsRelatedByIconId()->contains($sectionRelatedByIconId)) {
            $pos = $this->collSectionsRelatedByIconId->search($sectionRelatedByIconId);
            $this->collSectionsRelatedByIconId->remove($pos);
            if (null === $this->sectionsRelatedByIconIdScheduledForDeletion) {
                $this->sectionsRelatedByIconIdScheduledForDeletion = clone $this->collSectionsRelatedByIconId;
                $this->sectionsRelatedByIconIdScheduledForDeletion->clear();
            }
            $this->sectionsRelatedByIconIdScheduledForDeletion[]= $sectionRelatedByIconId;
            $sectionRelatedByIconId->setMediaRelatedByIconId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Media is new, it will return
     * an empty collection; or if this Media has previously
     * been saved, it will retrieve related SectionsRelatedByIconId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Media.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSection[] List of ChildSection objects
     */
    public function getSectionsRelatedByIconIdJoinTemplate(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSectionQuery::create(null, $criteria);
        $query->joinWith('Template', $joinBehavior);

        return $this->getSectionsRelatedByIconId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Media is new, it will return
     * an empty collection; or if this Media has previously
     * been saved, it will retrieve related SectionsRelatedByIconId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Media.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSection[] List of ChildSection objects
     */
    public function getSectionsRelatedByIconIdJoinUserBackend(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSectionQuery::create(null, $criteria);
        $query->joinWith('UserBackend', $joinBehavior);

        return $this->getSectionsRelatedByIconId($query, $con);
    }

    /**
     * Clears out the collSectionsRelatedByIconSelectedId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSectionsRelatedByIconSelectedId()
     */
    public function clearSectionsRelatedByIconSelectedId()
    {
        $this->collSectionsRelatedByIconSelectedId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSectionsRelatedByIconSelectedId collection loaded partially.
     */
    public function resetPartialSectionsRelatedByIconSelectedId($v = true)
    {
        $this->collSectionsRelatedByIconSelectedIdPartial = $v;
    }

    /**
     * Initializes the collSectionsRelatedByIconSelectedId collection.
     *
     * By default this just sets the collSectionsRelatedByIconSelectedId collection to an empty array (like clearcollSectionsRelatedByIconSelectedId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSectionsRelatedByIconSelectedId($overrideExisting = true)
    {
        if (null !== $this->collSectionsRelatedByIconSelectedId && !$overrideExisting) {
            return;
        }

        $collectionClassName = SectionTableMap::getTableMap()->getCollectionClassName();

        $this->collSectionsRelatedByIconSelectedId = new $collectionClassName;
        $this->collSectionsRelatedByIconSelectedId->setModel('\Database\HubPlus\Section');
    }

    /**
     * Gets an array of ChildSection objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMedia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSection[] List of ChildSection objects
     * @throws PropelException
     */
    public function getSectionsRelatedByIconSelectedId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionsRelatedByIconSelectedIdPartial && !$this->isNew();
        if (null === $this->collSectionsRelatedByIconSelectedId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSectionsRelatedByIconSelectedId) {
                // return empty collection
                $this->initSectionsRelatedByIconSelectedId();
            } else {
                $collSectionsRelatedByIconSelectedId = ChildSectionQuery::create(null, $criteria)
                    ->filterByMediaRelatedByIconSelectedId($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSectionsRelatedByIconSelectedIdPartial && count($collSectionsRelatedByIconSelectedId)) {
                        $this->initSectionsRelatedByIconSelectedId(false);

                        foreach ($collSectionsRelatedByIconSelectedId as $obj) {
                            if (false == $this->collSectionsRelatedByIconSelectedId->contains($obj)) {
                                $this->collSectionsRelatedByIconSelectedId->append($obj);
                            }
                        }

                        $this->collSectionsRelatedByIconSelectedIdPartial = true;
                    }

                    return $collSectionsRelatedByIconSelectedId;
                }

                if ($partial && $this->collSectionsRelatedByIconSelectedId) {
                    foreach ($this->collSectionsRelatedByIconSelectedId as $obj) {
                        if ($obj->isNew()) {
                            $collSectionsRelatedByIconSelectedId[] = $obj;
                        }
                    }
                }

                $this->collSectionsRelatedByIconSelectedId = $collSectionsRelatedByIconSelectedId;
                $this->collSectionsRelatedByIconSelectedIdPartial = false;
            }
        }

        return $this->collSectionsRelatedByIconSelectedId;
    }

    /**
     * Sets a collection of ChildSection objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sectionsRelatedByIconSelectedId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function setSectionsRelatedByIconSelectedId(Collection $sectionsRelatedByIconSelectedId, ConnectionInterface $con = null)
    {
        /** @var ChildSection[] $sectionsRelatedByIconSelectedIdToDelete */
        $sectionsRelatedByIconSelectedIdToDelete = $this->getSectionsRelatedByIconSelectedId(new Criteria(), $con)->diff($sectionsRelatedByIconSelectedId);


        $this->sectionsRelatedByIconSelectedIdScheduledForDeletion = $sectionsRelatedByIconSelectedIdToDelete;

        foreach ($sectionsRelatedByIconSelectedIdToDelete as $sectionRelatedByIconSelectedIdRemoved) {
            $sectionRelatedByIconSelectedIdRemoved->setMediaRelatedByIconSelectedId(null);
        }

        $this->collSectionsRelatedByIconSelectedId = null;
        foreach ($sectionsRelatedByIconSelectedId as $sectionRelatedByIconSelectedId) {
            $this->addSectionRelatedByIconSelectedId($sectionRelatedByIconSelectedId);
        }

        $this->collSectionsRelatedByIconSelectedId = $sectionsRelatedByIconSelectedId;
        $this->collSectionsRelatedByIconSelectedIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Section objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Section objects.
     * @throws PropelException
     */
    public function countSectionsRelatedByIconSelectedId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionsRelatedByIconSelectedIdPartial && !$this->isNew();
        if (null === $this->collSectionsRelatedByIconSelectedId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSectionsRelatedByIconSelectedId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSectionsRelatedByIconSelectedId());
            }

            $query = ChildSectionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMediaRelatedByIconSelectedId($this)
                ->count($con);
        }

        return count($this->collSectionsRelatedByIconSelectedId);
    }

    /**
     * Method called to associate a ChildSection object to this object
     * through the ChildSection foreign key attribute.
     *
     * @param  ChildSection $l ChildSection
     * @return $this|\Database\HubPlus\Media The current object (for fluent API support)
     */
    public function addSectionRelatedByIconSelectedId(ChildSection $l)
    {
        if ($this->collSectionsRelatedByIconSelectedId === null) {
            $this->initSectionsRelatedByIconSelectedId();
            $this->collSectionsRelatedByIconSelectedIdPartial = true;
        }

        if (!$this->collSectionsRelatedByIconSelectedId->contains($l)) {
            $this->doAddSectionRelatedByIconSelectedId($l);

            if ($this->sectionsRelatedByIconSelectedIdScheduledForDeletion and $this->sectionsRelatedByIconSelectedIdScheduledForDeletion->contains($l)) {
                $this->sectionsRelatedByIconSelectedIdScheduledForDeletion->remove($this->sectionsRelatedByIconSelectedIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSection $sectionRelatedByIconSelectedId The ChildSection object to add.
     */
    protected function doAddSectionRelatedByIconSelectedId(ChildSection $sectionRelatedByIconSelectedId)
    {
        $this->collSectionsRelatedByIconSelectedId[]= $sectionRelatedByIconSelectedId;
        $sectionRelatedByIconSelectedId->setMediaRelatedByIconSelectedId($this);
    }

    /**
     * @param  ChildSection $sectionRelatedByIconSelectedId The ChildSection object to remove.
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function removeSectionRelatedByIconSelectedId(ChildSection $sectionRelatedByIconSelectedId)
    {
        if ($this->getSectionsRelatedByIconSelectedId()->contains($sectionRelatedByIconSelectedId)) {
            $pos = $this->collSectionsRelatedByIconSelectedId->search($sectionRelatedByIconSelectedId);
            $this->collSectionsRelatedByIconSelectedId->remove($pos);
            if (null === $this->sectionsRelatedByIconSelectedIdScheduledForDeletion) {
                $this->sectionsRelatedByIconSelectedIdScheduledForDeletion = clone $this->collSectionsRelatedByIconSelectedId;
                $this->sectionsRelatedByIconSelectedIdScheduledForDeletion->clear();
            }
            $this->sectionsRelatedByIconSelectedIdScheduledForDeletion[]= $sectionRelatedByIconSelectedId;
            $sectionRelatedByIconSelectedId->setMediaRelatedByIconSelectedId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Media is new, it will return
     * an empty collection; or if this Media has previously
     * been saved, it will retrieve related SectionsRelatedByIconSelectedId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Media.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSection[] List of ChildSection objects
     */
    public function getSectionsRelatedByIconSelectedIdJoinTemplate(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSectionQuery::create(null, $criteria);
        $query->joinWith('Template', $joinBehavior);

        return $this->getSectionsRelatedByIconSelectedId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Media is new, it will return
     * an empty collection; or if this Media has previously
     * been saved, it will retrieve related SectionsRelatedByIconSelectedId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Media.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSection[] List of ChildSection objects
     */
    public function getSectionsRelatedByIconSelectedIdJoinUserBackend(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSectionQuery::create(null, $criteria);
        $query->joinWith('UserBackend', $joinBehavior);

        return $this->getSectionsRelatedByIconSelectedId($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aDevice) {
            $this->aDevice->removeMedia($this);
        }
        $this->id = null;
        $this->device_id = null;
        $this->is_public = null;
        $this->type = null;
        $this->uri = null;
        $this->uri_thumb = null;
        $this->extra_params = null;
        $this->weight = null;
        $this->title = null;
        $this->description = null;
        $this->extension = null;
        $this->mime_type = null;
        $this->size = null;
        $this->count_like = null;
        $this->count_share = null;
        $this->format = null;
        $this->deleted_at = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collGalleries) {
                foreach ($this->collGalleries as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collGalleryForms) {
                foreach ($this->collGalleryForms as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMediaExtraLogs) {
                foreach ($this->collMediaExtraLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMediaLogs) {
                foreach ($this->collMediaLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPosts) {
                foreach ($this->collPosts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPostActions) {
                foreach ($this->collPostActions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSectionsRelatedByIconId) {
                foreach ($this->collSectionsRelatedByIconId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSectionsRelatedByIconSelectedId) {
                foreach ($this->collSectionsRelatedByIconSelectedId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collGalleries = null;
        $this->collGalleryForms = null;
        $this->collMediaExtraLogs = null;
        $this->collMediaLogs = null;
        $this->collPosts = null;
        $this->collPostActions = null;
        $this->collSectionsRelatedByIconId = null;
        $this->collSectionsRelatedByIconSelectedId = null;
        $this->aDevice = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MediaTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildMedia The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[MediaTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildMediaArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildMediaArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildMediaArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildMediaArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildMediaArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildMedia The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setId($archive->getId());
        }
        $this->setDeviceId($archive->getDeviceId());
        $this->setIsPublic($archive->getIsPublic());
        $this->setType($archive->getType());
        $this->setUri($archive->getUri());
        $this->setUriThumb($archive->getUriThumb());
        $this->setExtraParams($archive->getExtraParams());
        $this->setWeight($archive->getWeight());
        $this->setTitle($archive->getTitle());
        $this->setDescription($archive->getDescription());
        $this->setExtension($archive->getExtension());
        $this->setMimeType($archive->getMimeType());
        $this->setSize($archive->getSize());
        $this->setCountLike($archive->getCountLike());
        $this->setCountShare($archive->getCountShare());
        $this->setFormat($archive->getFormat());
        $this->setDeletedAt($archive->getDeletedAt());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildMedia The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
