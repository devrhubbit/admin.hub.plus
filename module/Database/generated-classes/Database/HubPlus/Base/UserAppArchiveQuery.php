<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\UserAppArchive as ChildUserAppArchive;
use Database\HubPlus\UserAppArchiveQuery as ChildUserAppArchiveQuery;
use Database\HubPlus\Map\UserAppArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'user_app_archive' table.
 *
 *
 *
 * @method     ChildUserAppArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserAppArchiveQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method     ChildUserAppArchiveQuery orderByUsernameCanonical($order = Criteria::ASC) Order by the username_canonical column
 * @method     ChildUserAppArchiveQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildUserAppArchiveQuery orderByEmailCanonical($order = Criteria::ASC) Order by the email_canonical column
 * @method     ChildUserAppArchiveQuery orderByEnabled($order = Criteria::ASC) Order by the enabled column
 * @method     ChildUserAppArchiveQuery orderByPrivacy($order = Criteria::ASC) Order by the privacy column
 * @method     ChildUserAppArchiveQuery orderByTerms($order = Criteria::ASC) Order by the terms column
 * @method     ChildUserAppArchiveQuery orderBySalt($order = Criteria::ASC) Order by the salt column
 * @method     ChildUserAppArchiveQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildUserAppArchiveQuery orderByLastLogin($order = Criteria::ASC) Order by the last_login column
 * @method     ChildUserAppArchiveQuery orderByLocked($order = Criteria::ASC) Order by the locked column
 * @method     ChildUserAppArchiveQuery orderByExpired($order = Criteria::ASC) Order by the expired column
 * @method     ChildUserAppArchiveQuery orderByExpiresAt($order = Criteria::ASC) Order by the expires_at column
 * @method     ChildUserAppArchiveQuery orderByConfirmationToken($order = Criteria::ASC) Order by the confirmation_token column
 * @method     ChildUserAppArchiveQuery orderByPasswordRequestedAt($order = Criteria::ASC) Order by the password_requested_at column
 * @method     ChildUserAppArchiveQuery orderByRoles($order = Criteria::ASC) Order by the roles column
 * @method     ChildUserAppArchiveQuery orderByCredentialsExpired($order = Criteria::ASC) Order by the credentials_expired column
 * @method     ChildUserAppArchiveQuery orderByCredentialsExpireAt($order = Criteria::ASC) Order by the credentials_expire_at column
 * @method     ChildUserAppArchiveQuery orderByFirstname($order = Criteria::ASC) Order by the firstname column
 * @method     ChildUserAppArchiveQuery orderByLastname($order = Criteria::ASC) Order by the lastname column
 * @method     ChildUserAppArchiveQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildUserAppArchiveQuery orderByImagepath($order = Criteria::ASC) Order by the imagePath column
 * @method     ChildUserAppArchiveQuery orderByAddressId($order = Criteria::ASC) Order by the address_id column
 * @method     ChildUserAppArchiveQuery orderByTags($order = Criteria::ASC) Order by the tags column
 * @method     ChildUserAppArchiveQuery orderBySectionId($order = Criteria::ASC) Order by the section_id column
 * @method     ChildUserAppArchiveQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildUserAppArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildUserAppArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildUserAppArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildUserAppArchiveQuery groupById() Group by the id column
 * @method     ChildUserAppArchiveQuery groupByUsername() Group by the username column
 * @method     ChildUserAppArchiveQuery groupByUsernameCanonical() Group by the username_canonical column
 * @method     ChildUserAppArchiveQuery groupByEmail() Group by the email column
 * @method     ChildUserAppArchiveQuery groupByEmailCanonical() Group by the email_canonical column
 * @method     ChildUserAppArchiveQuery groupByEnabled() Group by the enabled column
 * @method     ChildUserAppArchiveQuery groupByPrivacy() Group by the privacy column
 * @method     ChildUserAppArchiveQuery groupByTerms() Group by the terms column
 * @method     ChildUserAppArchiveQuery groupBySalt() Group by the salt column
 * @method     ChildUserAppArchiveQuery groupByPassword() Group by the password column
 * @method     ChildUserAppArchiveQuery groupByLastLogin() Group by the last_login column
 * @method     ChildUserAppArchiveQuery groupByLocked() Group by the locked column
 * @method     ChildUserAppArchiveQuery groupByExpired() Group by the expired column
 * @method     ChildUserAppArchiveQuery groupByExpiresAt() Group by the expires_at column
 * @method     ChildUserAppArchiveQuery groupByConfirmationToken() Group by the confirmation_token column
 * @method     ChildUserAppArchiveQuery groupByPasswordRequestedAt() Group by the password_requested_at column
 * @method     ChildUserAppArchiveQuery groupByRoles() Group by the roles column
 * @method     ChildUserAppArchiveQuery groupByCredentialsExpired() Group by the credentials_expired column
 * @method     ChildUserAppArchiveQuery groupByCredentialsExpireAt() Group by the credentials_expire_at column
 * @method     ChildUserAppArchiveQuery groupByFirstname() Group by the firstname column
 * @method     ChildUserAppArchiveQuery groupByLastname() Group by the lastname column
 * @method     ChildUserAppArchiveQuery groupByPhone() Group by the phone column
 * @method     ChildUserAppArchiveQuery groupByImagepath() Group by the imagePath column
 * @method     ChildUserAppArchiveQuery groupByAddressId() Group by the address_id column
 * @method     ChildUserAppArchiveQuery groupByTags() Group by the tags column
 * @method     ChildUserAppArchiveQuery groupBySectionId() Group by the section_id column
 * @method     ChildUserAppArchiveQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildUserAppArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildUserAppArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildUserAppArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildUserAppArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserAppArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserAppArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserAppArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserAppArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserAppArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserAppArchive findOne(ConnectionInterface $con = null) Return the first ChildUserAppArchive matching the query
 * @method     ChildUserAppArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserAppArchive matching the query, or a new ChildUserAppArchive object populated from the query conditions when no match is found
 *
 * @method     ChildUserAppArchive findOneById(int $id) Return the first ChildUserAppArchive filtered by the id column
 * @method     ChildUserAppArchive findOneByUsername(string $username) Return the first ChildUserAppArchive filtered by the username column
 * @method     ChildUserAppArchive findOneByUsernameCanonical(string $username_canonical) Return the first ChildUserAppArchive filtered by the username_canonical column
 * @method     ChildUserAppArchive findOneByEmail(string $email) Return the first ChildUserAppArchive filtered by the email column
 * @method     ChildUserAppArchive findOneByEmailCanonical(string $email_canonical) Return the first ChildUserAppArchive filtered by the email_canonical column
 * @method     ChildUserAppArchive findOneByEnabled(boolean $enabled) Return the first ChildUserAppArchive filtered by the enabled column
 * @method     ChildUserAppArchive findOneByPrivacy(boolean $privacy) Return the first ChildUserAppArchive filtered by the privacy column
 * @method     ChildUserAppArchive findOneByTerms(boolean $terms) Return the first ChildUserAppArchive filtered by the terms column
 * @method     ChildUserAppArchive findOneBySalt(string $salt) Return the first ChildUserAppArchive filtered by the salt column
 * @method     ChildUserAppArchive findOneByPassword(string $password) Return the first ChildUserAppArchive filtered by the password column
 * @method     ChildUserAppArchive findOneByLastLogin(string $last_login) Return the first ChildUserAppArchive filtered by the last_login column
 * @method     ChildUserAppArchive findOneByLocked(boolean $locked) Return the first ChildUserAppArchive filtered by the locked column
 * @method     ChildUserAppArchive findOneByExpired(boolean $expired) Return the first ChildUserAppArchive filtered by the expired column
 * @method     ChildUserAppArchive findOneByExpiresAt(string $expires_at) Return the first ChildUserAppArchive filtered by the expires_at column
 * @method     ChildUserAppArchive findOneByConfirmationToken(string $confirmation_token) Return the first ChildUserAppArchive filtered by the confirmation_token column
 * @method     ChildUserAppArchive findOneByPasswordRequestedAt(string $password_requested_at) Return the first ChildUserAppArchive filtered by the password_requested_at column
 * @method     ChildUserAppArchive findOneByRoles(string $roles) Return the first ChildUserAppArchive filtered by the roles column
 * @method     ChildUserAppArchive findOneByCredentialsExpired(boolean $credentials_expired) Return the first ChildUserAppArchive filtered by the credentials_expired column
 * @method     ChildUserAppArchive findOneByCredentialsExpireAt(string $credentials_expire_at) Return the first ChildUserAppArchive filtered by the credentials_expire_at column
 * @method     ChildUserAppArchive findOneByFirstname(string $firstname) Return the first ChildUserAppArchive filtered by the firstname column
 * @method     ChildUserAppArchive findOneByLastname(string $lastname) Return the first ChildUserAppArchive filtered by the lastname column
 * @method     ChildUserAppArchive findOneByPhone(string $phone) Return the first ChildUserAppArchive filtered by the phone column
 * @method     ChildUserAppArchive findOneByImagepath(string $imagePath) Return the first ChildUserAppArchive filtered by the imagePath column
 * @method     ChildUserAppArchive findOneByAddressId(int $address_id) Return the first ChildUserAppArchive filtered by the address_id column
 * @method     ChildUserAppArchive findOneByTags(string $tags) Return the first ChildUserAppArchive filtered by the tags column
 * @method     ChildUserAppArchive findOneBySectionId(int $section_id) Return the first ChildUserAppArchive filtered by the section_id column
 * @method     ChildUserAppArchive findOneByDeletedAt(string $deleted_at) Return the first ChildUserAppArchive filtered by the deleted_at column
 * @method     ChildUserAppArchive findOneByCreatedAt(string $created_at) Return the first ChildUserAppArchive filtered by the created_at column
 * @method     ChildUserAppArchive findOneByUpdatedAt(string $updated_at) Return the first ChildUserAppArchive filtered by the updated_at column
 * @method     ChildUserAppArchive findOneByArchivedAt(string $archived_at) Return the first ChildUserAppArchive filtered by the archived_at column *

 * @method     ChildUserAppArchive requirePk($key, ConnectionInterface $con = null) Return the ChildUserAppArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOne(ConnectionInterface $con = null) Return the first ChildUserAppArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserAppArchive requireOneById(int $id) Return the first ChildUserAppArchive filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByUsername(string $username) Return the first ChildUserAppArchive filtered by the username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByUsernameCanonical(string $username_canonical) Return the first ChildUserAppArchive filtered by the username_canonical column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByEmail(string $email) Return the first ChildUserAppArchive filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByEmailCanonical(string $email_canonical) Return the first ChildUserAppArchive filtered by the email_canonical column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByEnabled(boolean $enabled) Return the first ChildUserAppArchive filtered by the enabled column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByPrivacy(boolean $privacy) Return the first ChildUserAppArchive filtered by the privacy column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByTerms(boolean $terms) Return the first ChildUserAppArchive filtered by the terms column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneBySalt(string $salt) Return the first ChildUserAppArchive filtered by the salt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByPassword(string $password) Return the first ChildUserAppArchive filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByLastLogin(string $last_login) Return the first ChildUserAppArchive filtered by the last_login column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByLocked(boolean $locked) Return the first ChildUserAppArchive filtered by the locked column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByExpired(boolean $expired) Return the first ChildUserAppArchive filtered by the expired column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByExpiresAt(string $expires_at) Return the first ChildUserAppArchive filtered by the expires_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByConfirmationToken(string $confirmation_token) Return the first ChildUserAppArchive filtered by the confirmation_token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByPasswordRequestedAt(string $password_requested_at) Return the first ChildUserAppArchive filtered by the password_requested_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByRoles(string $roles) Return the first ChildUserAppArchive filtered by the roles column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByCredentialsExpired(boolean $credentials_expired) Return the first ChildUserAppArchive filtered by the credentials_expired column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByCredentialsExpireAt(string $credentials_expire_at) Return the first ChildUserAppArchive filtered by the credentials_expire_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByFirstname(string $firstname) Return the first ChildUserAppArchive filtered by the firstname column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByLastname(string $lastname) Return the first ChildUserAppArchive filtered by the lastname column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByPhone(string $phone) Return the first ChildUserAppArchive filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByImagepath(string $imagePath) Return the first ChildUserAppArchive filtered by the imagePath column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByAddressId(int $address_id) Return the first ChildUserAppArchive filtered by the address_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByTags(string $tags) Return the first ChildUserAppArchive filtered by the tags column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneBySectionId(int $section_id) Return the first ChildUserAppArchive filtered by the section_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByDeletedAt(string $deleted_at) Return the first ChildUserAppArchive filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByCreatedAt(string $created_at) Return the first ChildUserAppArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildUserAppArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserAppArchive requireOneByArchivedAt(string $archived_at) Return the first ChildUserAppArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserAppArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserAppArchive objects based on current ModelCriteria
 * @method     ChildUserAppArchive[]|ObjectCollection findById(int $id) Return ChildUserAppArchive objects filtered by the id column
 * @method     ChildUserAppArchive[]|ObjectCollection findByUsername(string $username) Return ChildUserAppArchive objects filtered by the username column
 * @method     ChildUserAppArchive[]|ObjectCollection findByUsernameCanonical(string $username_canonical) Return ChildUserAppArchive objects filtered by the username_canonical column
 * @method     ChildUserAppArchive[]|ObjectCollection findByEmail(string $email) Return ChildUserAppArchive objects filtered by the email column
 * @method     ChildUserAppArchive[]|ObjectCollection findByEmailCanonical(string $email_canonical) Return ChildUserAppArchive objects filtered by the email_canonical column
 * @method     ChildUserAppArchive[]|ObjectCollection findByEnabled(boolean $enabled) Return ChildUserAppArchive objects filtered by the enabled column
 * @method     ChildUserAppArchive[]|ObjectCollection findByPrivacy(boolean $privacy) Return ChildUserAppArchive objects filtered by the privacy column
 * @method     ChildUserAppArchive[]|ObjectCollection findByTerms(boolean $terms) Return ChildUserAppArchive objects filtered by the terms column
 * @method     ChildUserAppArchive[]|ObjectCollection findBySalt(string $salt) Return ChildUserAppArchive objects filtered by the salt column
 * @method     ChildUserAppArchive[]|ObjectCollection findByPassword(string $password) Return ChildUserAppArchive objects filtered by the password column
 * @method     ChildUserAppArchive[]|ObjectCollection findByLastLogin(string $last_login) Return ChildUserAppArchive objects filtered by the last_login column
 * @method     ChildUserAppArchive[]|ObjectCollection findByLocked(boolean $locked) Return ChildUserAppArchive objects filtered by the locked column
 * @method     ChildUserAppArchive[]|ObjectCollection findByExpired(boolean $expired) Return ChildUserAppArchive objects filtered by the expired column
 * @method     ChildUserAppArchive[]|ObjectCollection findByExpiresAt(string $expires_at) Return ChildUserAppArchive objects filtered by the expires_at column
 * @method     ChildUserAppArchive[]|ObjectCollection findByConfirmationToken(string $confirmation_token) Return ChildUserAppArchive objects filtered by the confirmation_token column
 * @method     ChildUserAppArchive[]|ObjectCollection findByPasswordRequestedAt(string $password_requested_at) Return ChildUserAppArchive objects filtered by the password_requested_at column
 * @method     ChildUserAppArchive[]|ObjectCollection findByRoles(string $roles) Return ChildUserAppArchive objects filtered by the roles column
 * @method     ChildUserAppArchive[]|ObjectCollection findByCredentialsExpired(boolean $credentials_expired) Return ChildUserAppArchive objects filtered by the credentials_expired column
 * @method     ChildUserAppArchive[]|ObjectCollection findByCredentialsExpireAt(string $credentials_expire_at) Return ChildUserAppArchive objects filtered by the credentials_expire_at column
 * @method     ChildUserAppArchive[]|ObjectCollection findByFirstname(string $firstname) Return ChildUserAppArchive objects filtered by the firstname column
 * @method     ChildUserAppArchive[]|ObjectCollection findByLastname(string $lastname) Return ChildUserAppArchive objects filtered by the lastname column
 * @method     ChildUserAppArchive[]|ObjectCollection findByPhone(string $phone) Return ChildUserAppArchive objects filtered by the phone column
 * @method     ChildUserAppArchive[]|ObjectCollection findByImagepath(string $imagePath) Return ChildUserAppArchive objects filtered by the imagePath column
 * @method     ChildUserAppArchive[]|ObjectCollection findByAddressId(int $address_id) Return ChildUserAppArchive objects filtered by the address_id column
 * @method     ChildUserAppArchive[]|ObjectCollection findByTags(string $tags) Return ChildUserAppArchive objects filtered by the tags column
 * @method     ChildUserAppArchive[]|ObjectCollection findBySectionId(int $section_id) Return ChildUserAppArchive objects filtered by the section_id column
 * @method     ChildUserAppArchive[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildUserAppArchive objects filtered by the deleted_at column
 * @method     ChildUserAppArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildUserAppArchive objects filtered by the created_at column
 * @method     ChildUserAppArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildUserAppArchive objects filtered by the updated_at column
 * @method     ChildUserAppArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildUserAppArchive objects filtered by the archived_at column
 * @method     ChildUserAppArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserAppArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\UserAppArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\UserAppArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserAppArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserAppArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserAppArchiveQuery) {
            return $criteria;
        }
        $query = new ChildUserAppArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserAppArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserAppArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserAppArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserAppArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, username, username_canonical, email, email_canonical, enabled, privacy, terms, salt, password, last_login, locked, expired, expires_at, confirmation_token, password_requested_at, roles, credentials_expired, credentials_expire_at, firstname, lastname, phone, imagePath, address_id, tags, section_id, deleted_at, created_at, updated_at, archived_at FROM user_app_archive WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserAppArchive $obj */
            $obj = new ChildUserAppArchive();
            $obj->hydrate($row);
            UserAppArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserAppArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the username_canonical column
     *
     * Example usage:
     * <code>
     * $query->filterByUsernameCanonical('fooValue');   // WHERE username_canonical = 'fooValue'
     * $query->filterByUsernameCanonical('%fooValue%', Criteria::LIKE); // WHERE username_canonical LIKE '%fooValue%'
     * </code>
     *
     * @param     string $usernameCanonical The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByUsernameCanonical($usernameCanonical = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($usernameCanonical)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_USERNAME_CANONICAL, $usernameCanonical, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the email_canonical column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailCanonical('fooValue');   // WHERE email_canonical = 'fooValue'
     * $query->filterByEmailCanonical('%fooValue%', Criteria::LIKE); // WHERE email_canonical LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailCanonical The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByEmailCanonical($emailCanonical = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailCanonical)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_EMAIL_CANONICAL, $emailCanonical, $comparison);
    }

    /**
     * Filter the query on the enabled column
     *
     * Example usage:
     * <code>
     * $query->filterByEnabled(true); // WHERE enabled = true
     * $query->filterByEnabled('yes'); // WHERE enabled = true
     * </code>
     *
     * @param     boolean|string $enabled The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByEnabled($enabled = null, $comparison = null)
    {
        if (is_string($enabled)) {
            $enabled = in_array(strtolower($enabled), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_ENABLED, $enabled, $comparison);
    }

    /**
     * Filter the query on the privacy column
     *
     * Example usage:
     * <code>
     * $query->filterByPrivacy(true); // WHERE privacy = true
     * $query->filterByPrivacy('yes'); // WHERE privacy = true
     * </code>
     *
     * @param     boolean|string $privacy The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByPrivacy($privacy = null, $comparison = null)
    {
        if (is_string($privacy)) {
            $privacy = in_array(strtolower($privacy), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_PRIVACY, $privacy, $comparison);
    }

    /**
     * Filter the query on the terms column
     *
     * Example usage:
     * <code>
     * $query->filterByTerms(true); // WHERE terms = true
     * $query->filterByTerms('yes'); // WHERE terms = true
     * </code>
     *
     * @param     boolean|string $terms The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByTerms($terms = null, $comparison = null)
    {
        if (is_string($terms)) {
            $terms = in_array(strtolower($terms), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_TERMS, $terms, $comparison);
    }

    /**
     * Filter the query on the salt column
     *
     * Example usage:
     * <code>
     * $query->filterBySalt('fooValue');   // WHERE salt = 'fooValue'
     * $query->filterBySalt('%fooValue%', Criteria::LIKE); // WHERE salt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterBySalt($salt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_SALT, $salt, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the last_login column
     *
     * Example usage:
     * <code>
     * $query->filterByLastLogin('2011-03-14'); // WHERE last_login = '2011-03-14'
     * $query->filterByLastLogin('now'); // WHERE last_login = '2011-03-14'
     * $query->filterByLastLogin(array('max' => 'yesterday')); // WHERE last_login > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastLogin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByLastLogin($lastLogin = null, $comparison = null)
    {
        if (is_array($lastLogin)) {
            $useMinMax = false;
            if (isset($lastLogin['min'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_LAST_LOGIN, $lastLogin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastLogin['max'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_LAST_LOGIN, $lastLogin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_LAST_LOGIN, $lastLogin, $comparison);
    }

    /**
     * Filter the query on the locked column
     *
     * Example usage:
     * <code>
     * $query->filterByLocked(true); // WHERE locked = true
     * $query->filterByLocked('yes'); // WHERE locked = true
     * </code>
     *
     * @param     boolean|string $locked The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByLocked($locked = null, $comparison = null)
    {
        if (is_string($locked)) {
            $locked = in_array(strtolower($locked), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_LOCKED, $locked, $comparison);
    }

    /**
     * Filter the query on the expired column
     *
     * Example usage:
     * <code>
     * $query->filterByExpired(true); // WHERE expired = true
     * $query->filterByExpired('yes'); // WHERE expired = true
     * </code>
     *
     * @param     boolean|string $expired The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByExpired($expired = null, $comparison = null)
    {
        if (is_string($expired)) {
            $expired = in_array(strtolower($expired), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_EXPIRED, $expired, $comparison);
    }

    /**
     * Filter the query on the expires_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiresAt('2011-03-14'); // WHERE expires_at = '2011-03-14'
     * $query->filterByExpiresAt('now'); // WHERE expires_at = '2011-03-14'
     * $query->filterByExpiresAt(array('max' => 'yesterday')); // WHERE expires_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiresAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByExpiresAt($expiresAt = null, $comparison = null)
    {
        if (is_array($expiresAt)) {
            $useMinMax = false;
            if (isset($expiresAt['min'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_EXPIRES_AT, $expiresAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiresAt['max'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_EXPIRES_AT, $expiresAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_EXPIRES_AT, $expiresAt, $comparison);
    }

    /**
     * Filter the query on the confirmation_token column
     *
     * Example usage:
     * <code>
     * $query->filterByConfirmationToken('fooValue');   // WHERE confirmation_token = 'fooValue'
     * $query->filterByConfirmationToken('%fooValue%', Criteria::LIKE); // WHERE confirmation_token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $confirmationToken The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByConfirmationToken($confirmationToken = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($confirmationToken)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_CONFIRMATION_TOKEN, $confirmationToken, $comparison);
    }

    /**
     * Filter the query on the password_requested_at column
     *
     * Example usage:
     * <code>
     * $query->filterByPasswordRequestedAt('2011-03-14'); // WHERE password_requested_at = '2011-03-14'
     * $query->filterByPasswordRequestedAt('now'); // WHERE password_requested_at = '2011-03-14'
     * $query->filterByPasswordRequestedAt(array('max' => 'yesterday')); // WHERE password_requested_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $passwordRequestedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByPasswordRequestedAt($passwordRequestedAt = null, $comparison = null)
    {
        if (is_array($passwordRequestedAt)) {
            $useMinMax = false;
            if (isset($passwordRequestedAt['min'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_PASSWORD_REQUESTED_AT, $passwordRequestedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($passwordRequestedAt['max'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_PASSWORD_REQUESTED_AT, $passwordRequestedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_PASSWORD_REQUESTED_AT, $passwordRequestedAt, $comparison);
    }

    /**
     * Filter the query on the roles column
     *
     * Example usage:
     * <code>
     * $query->filterByRoles('fooValue');   // WHERE roles = 'fooValue'
     * $query->filterByRoles('%fooValue%', Criteria::LIKE); // WHERE roles LIKE '%fooValue%'
     * </code>
     *
     * @param     string $roles The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByRoles($roles = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($roles)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_ROLES, $roles, $comparison);
    }

    /**
     * Filter the query on the credentials_expired column
     *
     * Example usage:
     * <code>
     * $query->filterByCredentialsExpired(true); // WHERE credentials_expired = true
     * $query->filterByCredentialsExpired('yes'); // WHERE credentials_expired = true
     * </code>
     *
     * @param     boolean|string $credentialsExpired The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByCredentialsExpired($credentialsExpired = null, $comparison = null)
    {
        if (is_string($credentialsExpired)) {
            $credentialsExpired = in_array(strtolower($credentialsExpired), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_CREDENTIALS_EXPIRED, $credentialsExpired, $comparison);
    }

    /**
     * Filter the query on the credentials_expire_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCredentialsExpireAt('2011-03-14'); // WHERE credentials_expire_at = '2011-03-14'
     * $query->filterByCredentialsExpireAt('now'); // WHERE credentials_expire_at = '2011-03-14'
     * $query->filterByCredentialsExpireAt(array('max' => 'yesterday')); // WHERE credentials_expire_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $credentialsExpireAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByCredentialsExpireAt($credentialsExpireAt = null, $comparison = null)
    {
        if (is_array($credentialsExpireAt)) {
            $useMinMax = false;
            if (isset($credentialsExpireAt['min'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_CREDENTIALS_EXPIRE_AT, $credentialsExpireAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($credentialsExpireAt['max'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_CREDENTIALS_EXPIRE_AT, $credentialsExpireAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_CREDENTIALS_EXPIRE_AT, $credentialsExpireAt, $comparison);
    }

    /**
     * Filter the query on the firstname column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstname('fooValue');   // WHERE firstname = 'fooValue'
     * $query->filterByFirstname('%fooValue%', Criteria::LIKE); // WHERE firstname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByFirstname($firstname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_FIRSTNAME, $firstname, $comparison);
    }

    /**
     * Filter the query on the lastname column
     *
     * Example usage:
     * <code>
     * $query->filterByLastname('fooValue');   // WHERE lastname = 'fooValue'
     * $query->filterByLastname('%fooValue%', Criteria::LIKE); // WHERE lastname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByLastname($lastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_LASTNAME, $lastname, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%', Criteria::LIKE); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the imagePath column
     *
     * Example usage:
     * <code>
     * $query->filterByImagepath('fooValue');   // WHERE imagePath = 'fooValue'
     * $query->filterByImagepath('%fooValue%', Criteria::LIKE); // WHERE imagePath LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imagepath The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByImagepath($imagepath = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imagepath)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_IMAGEPATH, $imagepath, $comparison);
    }

    /**
     * Filter the query on the address_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAddressId(1234); // WHERE address_id = 1234
     * $query->filterByAddressId(array(12, 34)); // WHERE address_id IN (12, 34)
     * $query->filterByAddressId(array('min' => 12)); // WHERE address_id > 12
     * </code>
     *
     * @param     mixed $addressId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByAddressId($addressId = null, $comparison = null)
    {
        if (is_array($addressId)) {
            $useMinMax = false;
            if (isset($addressId['min'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_ADDRESS_ID, $addressId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($addressId['max'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_ADDRESS_ID, $addressId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_ADDRESS_ID, $addressId, $comparison);
    }

    /**
     * Filter the query on the tags column
     *
     * Example usage:
     * <code>
     * $query->filterByTags('fooValue');   // WHERE tags = 'fooValue'
     * $query->filterByTags('%fooValue%', Criteria::LIKE); // WHERE tags LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tags The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByTags($tags = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tags)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_TAGS, $tags, $comparison);
    }

    /**
     * Filter the query on the section_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySectionId(1234); // WHERE section_id = 1234
     * $query->filterBySectionId(array(12, 34)); // WHERE section_id IN (12, 34)
     * $query->filterBySectionId(array('min' => 12)); // WHERE section_id > 12
     * </code>
     *
     * @param     mixed $sectionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterBySectionId($sectionId = null, $comparison = null)
    {
        if (is_array($sectionId)) {
            $useMinMax = false;
            if (isset($sectionId['min'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_SECTION_ID, $sectionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sectionId['max'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_SECTION_ID, $sectionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_SECTION_ID, $sectionId, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(UserAppArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserAppArchive $userAppArchive Object to remove from the list of results
     *
     * @return $this|ChildUserAppArchiveQuery The current query, for fluid interface
     */
    public function prune($userAppArchive = null)
    {
        if ($userAppArchive) {
            $this->addUsingAlias(UserAppArchiveTableMap::COL_ID, $userAppArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the user_app_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAppArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserAppArchiveTableMap::clearInstancePool();
            UserAppArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAppArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserAppArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserAppArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserAppArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UserAppArchiveQuery
