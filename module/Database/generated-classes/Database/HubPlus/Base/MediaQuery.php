<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\Media as ChildMedia;
use Database\HubPlus\MediaArchive as ChildMediaArchive;
use Database\HubPlus\MediaQuery as ChildMediaQuery;
use Database\HubPlus\Map\MediaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'media' table.
 *
 *
 *
 * @method     ChildMediaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildMediaQuery orderByDeviceId($order = Criteria::ASC) Order by the device_id column
 * @method     ChildMediaQuery orderByIsPublic($order = Criteria::ASC) Order by the is_public column
 * @method     ChildMediaQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildMediaQuery orderByUri($order = Criteria::ASC) Order by the uri column
 * @method     ChildMediaQuery orderByUriThumb($order = Criteria::ASC) Order by the uri_thumb column
 * @method     ChildMediaQuery orderByExtraParams($order = Criteria::ASC) Order by the extra_params column
 * @method     ChildMediaQuery orderByWeight($order = Criteria::ASC) Order by the weight column
 * @method     ChildMediaQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildMediaQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildMediaQuery orderByExtension($order = Criteria::ASC) Order by the extension column
 * @method     ChildMediaQuery orderByMimeType($order = Criteria::ASC) Order by the mime_type column
 * @method     ChildMediaQuery orderBySize($order = Criteria::ASC) Order by the size column
 * @method     ChildMediaQuery orderByCountLike($order = Criteria::ASC) Order by the count_like column
 * @method     ChildMediaQuery orderByCountShare($order = Criteria::ASC) Order by the count_share column
 * @method     ChildMediaQuery orderByFormat($order = Criteria::ASC) Order by the format column
 * @method     ChildMediaQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildMediaQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildMediaQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildMediaQuery groupById() Group by the id column
 * @method     ChildMediaQuery groupByDeviceId() Group by the device_id column
 * @method     ChildMediaQuery groupByIsPublic() Group by the is_public column
 * @method     ChildMediaQuery groupByType() Group by the type column
 * @method     ChildMediaQuery groupByUri() Group by the uri column
 * @method     ChildMediaQuery groupByUriThumb() Group by the uri_thumb column
 * @method     ChildMediaQuery groupByExtraParams() Group by the extra_params column
 * @method     ChildMediaQuery groupByWeight() Group by the weight column
 * @method     ChildMediaQuery groupByTitle() Group by the title column
 * @method     ChildMediaQuery groupByDescription() Group by the description column
 * @method     ChildMediaQuery groupByExtension() Group by the extension column
 * @method     ChildMediaQuery groupByMimeType() Group by the mime_type column
 * @method     ChildMediaQuery groupBySize() Group by the size column
 * @method     ChildMediaQuery groupByCountLike() Group by the count_like column
 * @method     ChildMediaQuery groupByCountShare() Group by the count_share column
 * @method     ChildMediaQuery groupByFormat() Group by the format column
 * @method     ChildMediaQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildMediaQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildMediaQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildMediaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMediaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMediaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMediaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMediaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMediaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMediaQuery leftJoinDevice($relationAlias = null) Adds a LEFT JOIN clause to the query using the Device relation
 * @method     ChildMediaQuery rightJoinDevice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Device relation
 * @method     ChildMediaQuery innerJoinDevice($relationAlias = null) Adds a INNER JOIN clause to the query using the Device relation
 *
 * @method     ChildMediaQuery joinWithDevice($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Device relation
 *
 * @method     ChildMediaQuery leftJoinWithDevice() Adds a LEFT JOIN clause and with to the query using the Device relation
 * @method     ChildMediaQuery rightJoinWithDevice() Adds a RIGHT JOIN clause and with to the query using the Device relation
 * @method     ChildMediaQuery innerJoinWithDevice() Adds a INNER JOIN clause and with to the query using the Device relation
 *
 * @method     ChildMediaQuery leftJoinGallery($relationAlias = null) Adds a LEFT JOIN clause to the query using the Gallery relation
 * @method     ChildMediaQuery rightJoinGallery($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Gallery relation
 * @method     ChildMediaQuery innerJoinGallery($relationAlias = null) Adds a INNER JOIN clause to the query using the Gallery relation
 *
 * @method     ChildMediaQuery joinWithGallery($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Gallery relation
 *
 * @method     ChildMediaQuery leftJoinWithGallery() Adds a LEFT JOIN clause and with to the query using the Gallery relation
 * @method     ChildMediaQuery rightJoinWithGallery() Adds a RIGHT JOIN clause and with to the query using the Gallery relation
 * @method     ChildMediaQuery innerJoinWithGallery() Adds a INNER JOIN clause and with to the query using the Gallery relation
 *
 * @method     ChildMediaQuery leftJoinGalleryForm($relationAlias = null) Adds a LEFT JOIN clause to the query using the GalleryForm relation
 * @method     ChildMediaQuery rightJoinGalleryForm($relationAlias = null) Adds a RIGHT JOIN clause to the query using the GalleryForm relation
 * @method     ChildMediaQuery innerJoinGalleryForm($relationAlias = null) Adds a INNER JOIN clause to the query using the GalleryForm relation
 *
 * @method     ChildMediaQuery joinWithGalleryForm($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the GalleryForm relation
 *
 * @method     ChildMediaQuery leftJoinWithGalleryForm() Adds a LEFT JOIN clause and with to the query using the GalleryForm relation
 * @method     ChildMediaQuery rightJoinWithGalleryForm() Adds a RIGHT JOIN clause and with to the query using the GalleryForm relation
 * @method     ChildMediaQuery innerJoinWithGalleryForm() Adds a INNER JOIN clause and with to the query using the GalleryForm relation
 *
 * @method     ChildMediaQuery leftJoinMediaExtraLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the MediaExtraLog relation
 * @method     ChildMediaQuery rightJoinMediaExtraLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MediaExtraLog relation
 * @method     ChildMediaQuery innerJoinMediaExtraLog($relationAlias = null) Adds a INNER JOIN clause to the query using the MediaExtraLog relation
 *
 * @method     ChildMediaQuery joinWithMediaExtraLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MediaExtraLog relation
 *
 * @method     ChildMediaQuery leftJoinWithMediaExtraLog() Adds a LEFT JOIN clause and with to the query using the MediaExtraLog relation
 * @method     ChildMediaQuery rightJoinWithMediaExtraLog() Adds a RIGHT JOIN clause and with to the query using the MediaExtraLog relation
 * @method     ChildMediaQuery innerJoinWithMediaExtraLog() Adds a INNER JOIN clause and with to the query using the MediaExtraLog relation
 *
 * @method     ChildMediaQuery leftJoinMediaLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the MediaLog relation
 * @method     ChildMediaQuery rightJoinMediaLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MediaLog relation
 * @method     ChildMediaQuery innerJoinMediaLog($relationAlias = null) Adds a INNER JOIN clause to the query using the MediaLog relation
 *
 * @method     ChildMediaQuery joinWithMediaLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MediaLog relation
 *
 * @method     ChildMediaQuery leftJoinWithMediaLog() Adds a LEFT JOIN clause and with to the query using the MediaLog relation
 * @method     ChildMediaQuery rightJoinWithMediaLog() Adds a RIGHT JOIN clause and with to the query using the MediaLog relation
 * @method     ChildMediaQuery innerJoinWithMediaLog() Adds a INNER JOIN clause and with to the query using the MediaLog relation
 *
 * @method     ChildMediaQuery leftJoinPost($relationAlias = null) Adds a LEFT JOIN clause to the query using the Post relation
 * @method     ChildMediaQuery rightJoinPost($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Post relation
 * @method     ChildMediaQuery innerJoinPost($relationAlias = null) Adds a INNER JOIN clause to the query using the Post relation
 *
 * @method     ChildMediaQuery joinWithPost($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Post relation
 *
 * @method     ChildMediaQuery leftJoinWithPost() Adds a LEFT JOIN clause and with to the query using the Post relation
 * @method     ChildMediaQuery rightJoinWithPost() Adds a RIGHT JOIN clause and with to the query using the Post relation
 * @method     ChildMediaQuery innerJoinWithPost() Adds a INNER JOIN clause and with to the query using the Post relation
 *
 * @method     ChildMediaQuery leftJoinPostAction($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostAction relation
 * @method     ChildMediaQuery rightJoinPostAction($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostAction relation
 * @method     ChildMediaQuery innerJoinPostAction($relationAlias = null) Adds a INNER JOIN clause to the query using the PostAction relation
 *
 * @method     ChildMediaQuery joinWithPostAction($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostAction relation
 *
 * @method     ChildMediaQuery leftJoinWithPostAction() Adds a LEFT JOIN clause and with to the query using the PostAction relation
 * @method     ChildMediaQuery rightJoinWithPostAction() Adds a RIGHT JOIN clause and with to the query using the PostAction relation
 * @method     ChildMediaQuery innerJoinWithPostAction() Adds a INNER JOIN clause and with to the query using the PostAction relation
 *
 * @method     ChildMediaQuery leftJoinSectionRelatedByIconId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionRelatedByIconId relation
 * @method     ChildMediaQuery rightJoinSectionRelatedByIconId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionRelatedByIconId relation
 * @method     ChildMediaQuery innerJoinSectionRelatedByIconId($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionRelatedByIconId relation
 *
 * @method     ChildMediaQuery joinWithSectionRelatedByIconId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionRelatedByIconId relation
 *
 * @method     ChildMediaQuery leftJoinWithSectionRelatedByIconId() Adds a LEFT JOIN clause and with to the query using the SectionRelatedByIconId relation
 * @method     ChildMediaQuery rightJoinWithSectionRelatedByIconId() Adds a RIGHT JOIN clause and with to the query using the SectionRelatedByIconId relation
 * @method     ChildMediaQuery innerJoinWithSectionRelatedByIconId() Adds a INNER JOIN clause and with to the query using the SectionRelatedByIconId relation
 *
 * @method     ChildMediaQuery leftJoinSectionRelatedByIconSelectedId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionRelatedByIconSelectedId relation
 * @method     ChildMediaQuery rightJoinSectionRelatedByIconSelectedId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionRelatedByIconSelectedId relation
 * @method     ChildMediaQuery innerJoinSectionRelatedByIconSelectedId($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionRelatedByIconSelectedId relation
 *
 * @method     ChildMediaQuery joinWithSectionRelatedByIconSelectedId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionRelatedByIconSelectedId relation
 *
 * @method     ChildMediaQuery leftJoinWithSectionRelatedByIconSelectedId() Adds a LEFT JOIN clause and with to the query using the SectionRelatedByIconSelectedId relation
 * @method     ChildMediaQuery rightJoinWithSectionRelatedByIconSelectedId() Adds a RIGHT JOIN clause and with to the query using the SectionRelatedByIconSelectedId relation
 * @method     ChildMediaQuery innerJoinWithSectionRelatedByIconSelectedId() Adds a INNER JOIN clause and with to the query using the SectionRelatedByIconSelectedId relation
 *
 * @method     \Database\HubPlus\DeviceQuery|\Database\HubPlus\GalleryQuery|\Database\HubPlus\GalleryFormQuery|\Database\HubPlus\MediaExtraLogQuery|\Database\HubPlus\MediaLogQuery|\Database\HubPlus\PostQuery|\Database\HubPlus\PostActionQuery|\Database\HubPlus\SectionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMedia findOne(ConnectionInterface $con = null) Return the first ChildMedia matching the query
 * @method     ChildMedia findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMedia matching the query, or a new ChildMedia object populated from the query conditions when no match is found
 *
 * @method     ChildMedia findOneById(int $id) Return the first ChildMedia filtered by the id column
 * @method     ChildMedia findOneByDeviceId(int $device_id) Return the first ChildMedia filtered by the device_id column
 * @method     ChildMedia findOneByIsPublic(int $is_public) Return the first ChildMedia filtered by the is_public column
 * @method     ChildMedia findOneByType(string $type) Return the first ChildMedia filtered by the type column
 * @method     ChildMedia findOneByUri(string $uri) Return the first ChildMedia filtered by the uri column
 * @method     ChildMedia findOneByUriThumb(string $uri_thumb) Return the first ChildMedia filtered by the uri_thumb column
 * @method     ChildMedia findOneByExtraParams(string $extra_params) Return the first ChildMedia filtered by the extra_params column
 * @method     ChildMedia findOneByWeight(int $weight) Return the first ChildMedia filtered by the weight column
 * @method     ChildMedia findOneByTitle(string $title) Return the first ChildMedia filtered by the title column
 * @method     ChildMedia findOneByDescription(string $description) Return the first ChildMedia filtered by the description column
 * @method     ChildMedia findOneByExtension(string $extension) Return the first ChildMedia filtered by the extension column
 * @method     ChildMedia findOneByMimeType(string $mime_type) Return the first ChildMedia filtered by the mime_type column
 * @method     ChildMedia findOneBySize(int $size) Return the first ChildMedia filtered by the size column
 * @method     ChildMedia findOneByCountLike(int $count_like) Return the first ChildMedia filtered by the count_like column
 * @method     ChildMedia findOneByCountShare(int $count_share) Return the first ChildMedia filtered by the count_share column
 * @method     ChildMedia findOneByFormat(string $format) Return the first ChildMedia filtered by the format column
 * @method     ChildMedia findOneByDeletedAt(string $deleted_at) Return the first ChildMedia filtered by the deleted_at column
 * @method     ChildMedia findOneByCreatedAt(string $created_at) Return the first ChildMedia filtered by the created_at column
 * @method     ChildMedia findOneByUpdatedAt(string $updated_at) Return the first ChildMedia filtered by the updated_at column *

 * @method     ChildMedia requirePk($key, ConnectionInterface $con = null) Return the ChildMedia by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOne(ConnectionInterface $con = null) Return the first ChildMedia matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMedia requireOneById(int $id) Return the first ChildMedia filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByDeviceId(int $device_id) Return the first ChildMedia filtered by the device_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByIsPublic(int $is_public) Return the first ChildMedia filtered by the is_public column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByType(string $type) Return the first ChildMedia filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByUri(string $uri) Return the first ChildMedia filtered by the uri column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByUriThumb(string $uri_thumb) Return the first ChildMedia filtered by the uri_thumb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByExtraParams(string $extra_params) Return the first ChildMedia filtered by the extra_params column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByWeight(int $weight) Return the first ChildMedia filtered by the weight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByTitle(string $title) Return the first ChildMedia filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByDescription(string $description) Return the first ChildMedia filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByExtension(string $extension) Return the first ChildMedia filtered by the extension column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByMimeType(string $mime_type) Return the first ChildMedia filtered by the mime_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneBySize(int $size) Return the first ChildMedia filtered by the size column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByCountLike(int $count_like) Return the first ChildMedia filtered by the count_like column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByCountShare(int $count_share) Return the first ChildMedia filtered by the count_share column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByFormat(string $format) Return the first ChildMedia filtered by the format column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByDeletedAt(string $deleted_at) Return the first ChildMedia filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByCreatedAt(string $created_at) Return the first ChildMedia filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMedia requireOneByUpdatedAt(string $updated_at) Return the first ChildMedia filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMedia[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMedia objects based on current ModelCriteria
 * @method     ChildMedia[]|ObjectCollection findById(int $id) Return ChildMedia objects filtered by the id column
 * @method     ChildMedia[]|ObjectCollection findByDeviceId(int $device_id) Return ChildMedia objects filtered by the device_id column
 * @method     ChildMedia[]|ObjectCollection findByIsPublic(int $is_public) Return ChildMedia objects filtered by the is_public column
 * @method     ChildMedia[]|ObjectCollection findByType(string $type) Return ChildMedia objects filtered by the type column
 * @method     ChildMedia[]|ObjectCollection findByUri(string $uri) Return ChildMedia objects filtered by the uri column
 * @method     ChildMedia[]|ObjectCollection findByUriThumb(string $uri_thumb) Return ChildMedia objects filtered by the uri_thumb column
 * @method     ChildMedia[]|ObjectCollection findByExtraParams(string $extra_params) Return ChildMedia objects filtered by the extra_params column
 * @method     ChildMedia[]|ObjectCollection findByWeight(int $weight) Return ChildMedia objects filtered by the weight column
 * @method     ChildMedia[]|ObjectCollection findByTitle(string $title) Return ChildMedia objects filtered by the title column
 * @method     ChildMedia[]|ObjectCollection findByDescription(string $description) Return ChildMedia objects filtered by the description column
 * @method     ChildMedia[]|ObjectCollection findByExtension(string $extension) Return ChildMedia objects filtered by the extension column
 * @method     ChildMedia[]|ObjectCollection findByMimeType(string $mime_type) Return ChildMedia objects filtered by the mime_type column
 * @method     ChildMedia[]|ObjectCollection findBySize(int $size) Return ChildMedia objects filtered by the size column
 * @method     ChildMedia[]|ObjectCollection findByCountLike(int $count_like) Return ChildMedia objects filtered by the count_like column
 * @method     ChildMedia[]|ObjectCollection findByCountShare(int $count_share) Return ChildMedia objects filtered by the count_share column
 * @method     ChildMedia[]|ObjectCollection findByFormat(string $format) Return ChildMedia objects filtered by the format column
 * @method     ChildMedia[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildMedia objects filtered by the deleted_at column
 * @method     ChildMedia[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildMedia objects filtered by the created_at column
 * @method     ChildMedia[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildMedia objects filtered by the updated_at column
 * @method     ChildMedia[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MediaQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\MediaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\Media', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMediaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMediaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMediaQuery) {
            return $criteria;
        }
        $query = new ChildMediaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMedia|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MediaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MediaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMedia A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, device_id, is_public, type, uri, uri_thumb, extra_params, weight, title, description, extension, mime_type, size, count_like, count_share, format, deleted_at, created_at, updated_at FROM media WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMedia $obj */
            $obj = new ChildMedia();
            $obj->hydrate($row);
            MediaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMedia|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MediaTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MediaTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MediaTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MediaTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the device_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDeviceId(1234); // WHERE device_id = 1234
     * $query->filterByDeviceId(array(12, 34)); // WHERE device_id IN (12, 34)
     * $query->filterByDeviceId(array('min' => 12)); // WHERE device_id > 12
     * </code>
     *
     * @see       filterByDevice()
     *
     * @param     mixed $deviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByDeviceId($deviceId = null, $comparison = null)
    {
        if (is_array($deviceId)) {
            $useMinMax = false;
            if (isset($deviceId['min'])) {
                $this->addUsingAlias(MediaTableMap::COL_DEVICE_ID, $deviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deviceId['max'])) {
                $this->addUsingAlias(MediaTableMap::COL_DEVICE_ID, $deviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_DEVICE_ID, $deviceId, $comparison);
    }

    /**
     * Filter the query on the is_public column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPublic(1234); // WHERE is_public = 1234
     * $query->filterByIsPublic(array(12, 34)); // WHERE is_public IN (12, 34)
     * $query->filterByIsPublic(array('min' => 12)); // WHERE is_public > 12
     * </code>
     *
     * @param     mixed $isPublic The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByIsPublic($isPublic = null, $comparison = null)
    {
        if (is_array($isPublic)) {
            $useMinMax = false;
            if (isset($isPublic['min'])) {
                $this->addUsingAlias(MediaTableMap::COL_IS_PUBLIC, $isPublic['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isPublic['max'])) {
                $this->addUsingAlias(MediaTableMap::COL_IS_PUBLIC, $isPublic['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_IS_PUBLIC, $isPublic, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the uri column
     *
     * Example usage:
     * <code>
     * $query->filterByUri('fooValue');   // WHERE uri = 'fooValue'
     * $query->filterByUri('%fooValue%', Criteria::LIKE); // WHERE uri LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uri The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByUri($uri = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uri)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_URI, $uri, $comparison);
    }

    /**
     * Filter the query on the uri_thumb column
     *
     * Example usage:
     * <code>
     * $query->filterByUriThumb('fooValue');   // WHERE uri_thumb = 'fooValue'
     * $query->filterByUriThumb('%fooValue%', Criteria::LIKE); // WHERE uri_thumb LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uriThumb The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByUriThumb($uriThumb = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uriThumb)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_URI_THUMB, $uriThumb, $comparison);
    }

    /**
     * Filter the query on the extra_params column
     *
     * Example usage:
     * <code>
     * $query->filterByExtraParams('fooValue');   // WHERE extra_params = 'fooValue'
     * $query->filterByExtraParams('%fooValue%', Criteria::LIKE); // WHERE extra_params LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extraParams The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByExtraParams($extraParams = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extraParams)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_EXTRA_PARAMS, $extraParams, $comparison);
    }

    /**
     * Filter the query on the weight column
     *
     * Example usage:
     * <code>
     * $query->filterByWeight(1234); // WHERE weight = 1234
     * $query->filterByWeight(array(12, 34)); // WHERE weight IN (12, 34)
     * $query->filterByWeight(array('min' => 12)); // WHERE weight > 12
     * </code>
     *
     * @param     mixed $weight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByWeight($weight = null, $comparison = null)
    {
        if (is_array($weight)) {
            $useMinMax = false;
            if (isset($weight['min'])) {
                $this->addUsingAlias(MediaTableMap::COL_WEIGHT, $weight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($weight['max'])) {
                $this->addUsingAlias(MediaTableMap::COL_WEIGHT, $weight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_WEIGHT, $weight, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the extension column
     *
     * Example usage:
     * <code>
     * $query->filterByExtension('fooValue');   // WHERE extension = 'fooValue'
     * $query->filterByExtension('%fooValue%', Criteria::LIKE); // WHERE extension LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extension The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByExtension($extension = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extension)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_EXTENSION, $extension, $comparison);
    }

    /**
     * Filter the query on the mime_type column
     *
     * Example usage:
     * <code>
     * $query->filterByMimeType('fooValue');   // WHERE mime_type = 'fooValue'
     * $query->filterByMimeType('%fooValue%', Criteria::LIKE); // WHERE mime_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mimeType The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByMimeType($mimeType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mimeType)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_MIME_TYPE, $mimeType, $comparison);
    }

    /**
     * Filter the query on the size column
     *
     * Example usage:
     * <code>
     * $query->filterBySize(1234); // WHERE size = 1234
     * $query->filterBySize(array(12, 34)); // WHERE size IN (12, 34)
     * $query->filterBySize(array('min' => 12)); // WHERE size > 12
     * </code>
     *
     * @param     mixed $size The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterBySize($size = null, $comparison = null)
    {
        if (is_array($size)) {
            $useMinMax = false;
            if (isset($size['min'])) {
                $this->addUsingAlias(MediaTableMap::COL_SIZE, $size['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($size['max'])) {
                $this->addUsingAlias(MediaTableMap::COL_SIZE, $size['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_SIZE, $size, $comparison);
    }

    /**
     * Filter the query on the count_like column
     *
     * Example usage:
     * <code>
     * $query->filterByCountLike(1234); // WHERE count_like = 1234
     * $query->filterByCountLike(array(12, 34)); // WHERE count_like IN (12, 34)
     * $query->filterByCountLike(array('min' => 12)); // WHERE count_like > 12
     * </code>
     *
     * @param     mixed $countLike The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByCountLike($countLike = null, $comparison = null)
    {
        if (is_array($countLike)) {
            $useMinMax = false;
            if (isset($countLike['min'])) {
                $this->addUsingAlias(MediaTableMap::COL_COUNT_LIKE, $countLike['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countLike['max'])) {
                $this->addUsingAlias(MediaTableMap::COL_COUNT_LIKE, $countLike['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_COUNT_LIKE, $countLike, $comparison);
    }

    /**
     * Filter the query on the count_share column
     *
     * Example usage:
     * <code>
     * $query->filterByCountShare(1234); // WHERE count_share = 1234
     * $query->filterByCountShare(array(12, 34)); // WHERE count_share IN (12, 34)
     * $query->filterByCountShare(array('min' => 12)); // WHERE count_share > 12
     * </code>
     *
     * @param     mixed $countShare The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByCountShare($countShare = null, $comparison = null)
    {
        if (is_array($countShare)) {
            $useMinMax = false;
            if (isset($countShare['min'])) {
                $this->addUsingAlias(MediaTableMap::COL_COUNT_SHARE, $countShare['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countShare['max'])) {
                $this->addUsingAlias(MediaTableMap::COL_COUNT_SHARE, $countShare['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_COUNT_SHARE, $countShare, $comparison);
    }

    /**
     * Filter the query on the format column
     *
     * Example usage:
     * <code>
     * $query->filterByFormat('fooValue');   // WHERE format = 'fooValue'
     * $query->filterByFormat('%fooValue%', Criteria::LIKE); // WHERE format LIKE '%fooValue%'
     * </code>
     *
     * @param     string $format The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByFormat($format = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($format)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_FORMAT, $format, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(MediaTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(MediaTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(MediaTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(MediaTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(MediaTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(MediaTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Device object
     *
     * @param \Database\HubPlus\Device|ObjectCollection $device The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMediaQuery The current query, for fluid interface
     */
    public function filterByDevice($device, $comparison = null)
    {
        if ($device instanceof \Database\HubPlus\Device) {
            return $this
                ->addUsingAlias(MediaTableMap::COL_DEVICE_ID, $device->getId(), $comparison);
        } elseif ($device instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MediaTableMap::COL_DEVICE_ID, $device->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDevice() only accepts arguments of type \Database\HubPlus\Device or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Device relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function joinDevice($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Device');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Device');
        }

        return $this;
    }

    /**
     * Use the Device relation Device object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\DeviceQuery A secondary query class using the current class as primary query
     */
    public function useDeviceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinDevice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Device', '\Database\HubPlus\DeviceQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Gallery object
     *
     * @param \Database\HubPlus\Gallery|ObjectCollection $gallery the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMediaQuery The current query, for fluid interface
     */
    public function filterByGallery($gallery, $comparison = null)
    {
        if ($gallery instanceof \Database\HubPlus\Gallery) {
            return $this
                ->addUsingAlias(MediaTableMap::COL_ID, $gallery->getMediaId(), $comparison);
        } elseif ($gallery instanceof ObjectCollection) {
            return $this
                ->useGalleryQuery()
                ->filterByPrimaryKeys($gallery->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByGallery() only accepts arguments of type \Database\HubPlus\Gallery or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Gallery relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function joinGallery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Gallery');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Gallery');
        }

        return $this;
    }

    /**
     * Use the Gallery relation Gallery object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\GalleryQuery A secondary query class using the current class as primary query
     */
    public function useGalleryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinGallery($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Gallery', '\Database\HubPlus\GalleryQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\GalleryForm object
     *
     * @param \Database\HubPlus\GalleryForm|ObjectCollection $galleryForm the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMediaQuery The current query, for fluid interface
     */
    public function filterByGalleryForm($galleryForm, $comparison = null)
    {
        if ($galleryForm instanceof \Database\HubPlus\GalleryForm) {
            return $this
                ->addUsingAlias(MediaTableMap::COL_ID, $galleryForm->getMediaId(), $comparison);
        } elseif ($galleryForm instanceof ObjectCollection) {
            return $this
                ->useGalleryFormQuery()
                ->filterByPrimaryKeys($galleryForm->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByGalleryForm() only accepts arguments of type \Database\HubPlus\GalleryForm or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the GalleryForm relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function joinGalleryForm($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('GalleryForm');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'GalleryForm');
        }

        return $this;
    }

    /**
     * Use the GalleryForm relation GalleryForm object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\GalleryFormQuery A secondary query class using the current class as primary query
     */
    public function useGalleryFormQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinGalleryForm($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'GalleryForm', '\Database\HubPlus\GalleryFormQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\MediaExtraLog object
     *
     * @param \Database\HubPlus\MediaExtraLog|ObjectCollection $mediaExtraLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMediaQuery The current query, for fluid interface
     */
    public function filterByMediaExtraLog($mediaExtraLog, $comparison = null)
    {
        if ($mediaExtraLog instanceof \Database\HubPlus\MediaExtraLog) {
            return $this
                ->addUsingAlias(MediaTableMap::COL_ID, $mediaExtraLog->getMediaId(), $comparison);
        } elseif ($mediaExtraLog instanceof ObjectCollection) {
            return $this
                ->useMediaExtraLogQuery()
                ->filterByPrimaryKeys($mediaExtraLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMediaExtraLog() only accepts arguments of type \Database\HubPlus\MediaExtraLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MediaExtraLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function joinMediaExtraLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MediaExtraLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MediaExtraLog');
        }

        return $this;
    }

    /**
     * Use the MediaExtraLog relation MediaExtraLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\MediaExtraLogQuery A secondary query class using the current class as primary query
     */
    public function useMediaExtraLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMediaExtraLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MediaExtraLog', '\Database\HubPlus\MediaExtraLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\MediaLog object
     *
     * @param \Database\HubPlus\MediaLog|ObjectCollection $mediaLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMediaQuery The current query, for fluid interface
     */
    public function filterByMediaLog($mediaLog, $comparison = null)
    {
        if ($mediaLog instanceof \Database\HubPlus\MediaLog) {
            return $this
                ->addUsingAlias(MediaTableMap::COL_ID, $mediaLog->getMediaId(), $comparison);
        } elseif ($mediaLog instanceof ObjectCollection) {
            return $this
                ->useMediaLogQuery()
                ->filterByPrimaryKeys($mediaLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMediaLog() only accepts arguments of type \Database\HubPlus\MediaLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MediaLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function joinMediaLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MediaLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MediaLog');
        }

        return $this;
    }

    /**
     * Use the MediaLog relation MediaLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\MediaLogQuery A secondary query class using the current class as primary query
     */
    public function useMediaLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMediaLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MediaLog', '\Database\HubPlus\MediaLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Post object
     *
     * @param \Database\HubPlus\Post|ObjectCollection $post the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMediaQuery The current query, for fluid interface
     */
    public function filterByPost($post, $comparison = null)
    {
        if ($post instanceof \Database\HubPlus\Post) {
            return $this
                ->addUsingAlias(MediaTableMap::COL_ID, $post->getCoverId(), $comparison);
        } elseif ($post instanceof ObjectCollection) {
            return $this
                ->usePostQuery()
                ->filterByPrimaryKeys($post->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPost() only accepts arguments of type \Database\HubPlus\Post or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Post relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function joinPost($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Post');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Post');
        }

        return $this;
    }

    /**
     * Use the Post relation Post object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostQuery A secondary query class using the current class as primary query
     */
    public function usePostQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPost($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Post', '\Database\HubPlus\PostQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostAction object
     *
     * @param \Database\HubPlus\PostAction|ObjectCollection $postAction the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMediaQuery The current query, for fluid interface
     */
    public function filterByPostAction($postAction, $comparison = null)
    {
        if ($postAction instanceof \Database\HubPlus\PostAction) {
            return $this
                ->addUsingAlias(MediaTableMap::COL_ID, $postAction->getCoverId(), $comparison);
        } elseif ($postAction instanceof ObjectCollection) {
            return $this
                ->usePostActionQuery()
                ->filterByPrimaryKeys($postAction->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostAction() only accepts arguments of type \Database\HubPlus\PostAction or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostAction relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function joinPostAction($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostAction');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostAction');
        }

        return $this;
    }

    /**
     * Use the PostAction relation PostAction object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostActionQuery A secondary query class using the current class as primary query
     */
    public function usePostActionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPostAction($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostAction', '\Database\HubPlus\PostActionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Section object
     *
     * @param \Database\HubPlus\Section|ObjectCollection $section the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMediaQuery The current query, for fluid interface
     */
    public function filterBySectionRelatedByIconId($section, $comparison = null)
    {
        if ($section instanceof \Database\HubPlus\Section) {
            return $this
                ->addUsingAlias(MediaTableMap::COL_ID, $section->getIconId(), $comparison);
        } elseif ($section instanceof ObjectCollection) {
            return $this
                ->useSectionRelatedByIconIdQuery()
                ->filterByPrimaryKeys($section->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySectionRelatedByIconId() only accepts arguments of type \Database\HubPlus\Section or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionRelatedByIconId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function joinSectionRelatedByIconId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionRelatedByIconId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionRelatedByIconId');
        }

        return $this;
    }

    /**
     * Use the SectionRelatedByIconId relation Section object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionQuery A secondary query class using the current class as primary query
     */
    public function useSectionRelatedByIconIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSectionRelatedByIconId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionRelatedByIconId', '\Database\HubPlus\SectionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Section object
     *
     * @param \Database\HubPlus\Section|ObjectCollection $section the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMediaQuery The current query, for fluid interface
     */
    public function filterBySectionRelatedByIconSelectedId($section, $comparison = null)
    {
        if ($section instanceof \Database\HubPlus\Section) {
            return $this
                ->addUsingAlias(MediaTableMap::COL_ID, $section->getIconSelectedId(), $comparison);
        } elseif ($section instanceof ObjectCollection) {
            return $this
                ->useSectionRelatedByIconSelectedIdQuery()
                ->filterByPrimaryKeys($section->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySectionRelatedByIconSelectedId() only accepts arguments of type \Database\HubPlus\Section or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionRelatedByIconSelectedId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function joinSectionRelatedByIconSelectedId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionRelatedByIconSelectedId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionRelatedByIconSelectedId');
        }

        return $this;
    }

    /**
     * Use the SectionRelatedByIconSelectedId relation Section object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionQuery A secondary query class using the current class as primary query
     */
    public function useSectionRelatedByIconSelectedIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSectionRelatedByIconSelectedId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionRelatedByIconSelectedId', '\Database\HubPlus\SectionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMedia $media Object to remove from the list of results
     *
     * @return $this|ChildMediaQuery The current query, for fluid interface
     */
    public function prune($media = null)
    {
        if ($media) {
            $this->addUsingAlias(MediaTableMap::COL_ID, $media->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the media table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MediaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MediaTableMap::clearInstancePool();
            MediaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MediaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MediaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MediaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MediaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildMediaQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(MediaTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildMediaQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(MediaTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildMediaQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(MediaTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildMediaQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(MediaTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildMediaQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(MediaTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildMediaQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(MediaTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildMediaArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MediaTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // MediaQuery
