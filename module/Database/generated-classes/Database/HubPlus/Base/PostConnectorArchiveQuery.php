<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\PostConnectorArchive as ChildPostConnectorArchive;
use Database\HubPlus\PostConnectorArchiveQuery as ChildPostConnectorArchiveQuery;
use Database\HubPlus\Map\PostConnectorArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'post_connector_archive' table.
 *
 *
 *
 * @method     ChildPostConnectorArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPostConnectorArchiveQuery orderByPostId($order = Criteria::ASC) Order by the post_id column
 * @method     ChildPostConnectorArchiveQuery orderByProviderId($order = Criteria::ASC) Order by the provider_id column
 * @method     ChildPostConnectorArchiveQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildPostConnectorArchiveQuery orderByBaseurl($order = Criteria::ASC) Order by the baseurl column
 * @method     ChildPostConnectorArchiveQuery orderByRemotePostId($order = Criteria::ASC) Order by the remote_post_id column
 * @method     ChildPostConnectorArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPostConnectorArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildPostConnectorArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildPostConnectorArchiveQuery groupById() Group by the id column
 * @method     ChildPostConnectorArchiveQuery groupByPostId() Group by the post_id column
 * @method     ChildPostConnectorArchiveQuery groupByProviderId() Group by the provider_id column
 * @method     ChildPostConnectorArchiveQuery groupByType() Group by the type column
 * @method     ChildPostConnectorArchiveQuery groupByBaseurl() Group by the baseurl column
 * @method     ChildPostConnectorArchiveQuery groupByRemotePostId() Group by the remote_post_id column
 * @method     ChildPostConnectorArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPostConnectorArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildPostConnectorArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildPostConnectorArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPostConnectorArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPostConnectorArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPostConnectorArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPostConnectorArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPostConnectorArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPostConnectorArchive findOne(ConnectionInterface $con = null) Return the first ChildPostConnectorArchive matching the query
 * @method     ChildPostConnectorArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPostConnectorArchive matching the query, or a new ChildPostConnectorArchive object populated from the query conditions when no match is found
 *
 * @method     ChildPostConnectorArchive findOneById(int $id) Return the first ChildPostConnectorArchive filtered by the id column
 * @method     ChildPostConnectorArchive findOneByPostId(int $post_id) Return the first ChildPostConnectorArchive filtered by the post_id column
 * @method     ChildPostConnectorArchive findOneByProviderId(int $provider_id) Return the first ChildPostConnectorArchive filtered by the provider_id column
 * @method     ChildPostConnectorArchive findOneByType(string $type) Return the first ChildPostConnectorArchive filtered by the type column
 * @method     ChildPostConnectorArchive findOneByBaseurl(string $baseurl) Return the first ChildPostConnectorArchive filtered by the baseurl column
 * @method     ChildPostConnectorArchive findOneByRemotePostId(int $remote_post_id) Return the first ChildPostConnectorArchive filtered by the remote_post_id column
 * @method     ChildPostConnectorArchive findOneByCreatedAt(string $created_at) Return the first ChildPostConnectorArchive filtered by the created_at column
 * @method     ChildPostConnectorArchive findOneByUpdatedAt(string $updated_at) Return the first ChildPostConnectorArchive filtered by the updated_at column
 * @method     ChildPostConnectorArchive findOneByArchivedAt(string $archived_at) Return the first ChildPostConnectorArchive filtered by the archived_at column *

 * @method     ChildPostConnectorArchive requirePk($key, ConnectionInterface $con = null) Return the ChildPostConnectorArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostConnectorArchive requireOne(ConnectionInterface $con = null) Return the first ChildPostConnectorArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPostConnectorArchive requireOneById(int $id) Return the first ChildPostConnectorArchive filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostConnectorArchive requireOneByPostId(int $post_id) Return the first ChildPostConnectorArchive filtered by the post_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostConnectorArchive requireOneByProviderId(int $provider_id) Return the first ChildPostConnectorArchive filtered by the provider_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostConnectorArchive requireOneByType(string $type) Return the first ChildPostConnectorArchive filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostConnectorArchive requireOneByBaseurl(string $baseurl) Return the first ChildPostConnectorArchive filtered by the baseurl column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostConnectorArchive requireOneByRemotePostId(int $remote_post_id) Return the first ChildPostConnectorArchive filtered by the remote_post_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostConnectorArchive requireOneByCreatedAt(string $created_at) Return the first ChildPostConnectorArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostConnectorArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildPostConnectorArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostConnectorArchive requireOneByArchivedAt(string $archived_at) Return the first ChildPostConnectorArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPostConnectorArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPostConnectorArchive objects based on current ModelCriteria
 * @method     ChildPostConnectorArchive[]|ObjectCollection findById(int $id) Return ChildPostConnectorArchive objects filtered by the id column
 * @method     ChildPostConnectorArchive[]|ObjectCollection findByPostId(int $post_id) Return ChildPostConnectorArchive objects filtered by the post_id column
 * @method     ChildPostConnectorArchive[]|ObjectCollection findByProviderId(int $provider_id) Return ChildPostConnectorArchive objects filtered by the provider_id column
 * @method     ChildPostConnectorArchive[]|ObjectCollection findByType(string $type) Return ChildPostConnectorArchive objects filtered by the type column
 * @method     ChildPostConnectorArchive[]|ObjectCollection findByBaseurl(string $baseurl) Return ChildPostConnectorArchive objects filtered by the baseurl column
 * @method     ChildPostConnectorArchive[]|ObjectCollection findByRemotePostId(int $remote_post_id) Return ChildPostConnectorArchive objects filtered by the remote_post_id column
 * @method     ChildPostConnectorArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPostConnectorArchive objects filtered by the created_at column
 * @method     ChildPostConnectorArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPostConnectorArchive objects filtered by the updated_at column
 * @method     ChildPostConnectorArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildPostConnectorArchive objects filtered by the archived_at column
 * @method     ChildPostConnectorArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PostConnectorArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\PostConnectorArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\PostConnectorArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPostConnectorArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPostConnectorArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPostConnectorArchiveQuery) {
            return $criteria;
        }
        $query = new ChildPostConnectorArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPostConnectorArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PostConnectorArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PostConnectorArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPostConnectorArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, post_id, provider_id, type, baseurl, remote_post_id, created_at, updated_at, archived_at FROM post_connector_archive WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPostConnectorArchive $obj */
            $obj = new ChildPostConnectorArchive();
            $obj->hydrate($row);
            PostConnectorArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPostConnectorArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPostConnectorArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PostConnectorArchiveTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPostConnectorArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PostConnectorArchiveTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostConnectorArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostConnectorArchiveTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the post_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPostId(1234); // WHERE post_id = 1234
     * $query->filterByPostId(array(12, 34)); // WHERE post_id IN (12, 34)
     * $query->filterByPostId(array('min' => 12)); // WHERE post_id > 12
     * </code>
     *
     * @param     mixed $postId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostConnectorArchiveQuery The current query, for fluid interface
     */
    public function filterByPostId($postId = null, $comparison = null)
    {
        if (is_array($postId)) {
            $useMinMax = false;
            if (isset($postId['min'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_POST_ID, $postId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($postId['max'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_POST_ID, $postId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostConnectorArchiveTableMap::COL_POST_ID, $postId, $comparison);
    }

    /**
     * Filter the query on the provider_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProviderId(1234); // WHERE provider_id = 1234
     * $query->filterByProviderId(array(12, 34)); // WHERE provider_id IN (12, 34)
     * $query->filterByProviderId(array('min' => 12)); // WHERE provider_id > 12
     * </code>
     *
     * @param     mixed $providerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostConnectorArchiveQuery The current query, for fluid interface
     */
    public function filterByProviderId($providerId = null, $comparison = null)
    {
        if (is_array($providerId)) {
            $useMinMax = false;
            if (isset($providerId['min'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_PROVIDER_ID, $providerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($providerId['max'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_PROVIDER_ID, $providerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostConnectorArchiveTableMap::COL_PROVIDER_ID, $providerId, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostConnectorArchiveQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostConnectorArchiveTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the baseurl column
     *
     * Example usage:
     * <code>
     * $query->filterByBaseurl('fooValue');   // WHERE baseurl = 'fooValue'
     * $query->filterByBaseurl('%fooValue%', Criteria::LIKE); // WHERE baseurl LIKE '%fooValue%'
     * </code>
     *
     * @param     string $baseurl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostConnectorArchiveQuery The current query, for fluid interface
     */
    public function filterByBaseurl($baseurl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($baseurl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostConnectorArchiveTableMap::COL_BASEURL, $baseurl, $comparison);
    }

    /**
     * Filter the query on the remote_post_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRemotePostId(1234); // WHERE remote_post_id = 1234
     * $query->filterByRemotePostId(array(12, 34)); // WHERE remote_post_id IN (12, 34)
     * $query->filterByRemotePostId(array('min' => 12)); // WHERE remote_post_id > 12
     * </code>
     *
     * @param     mixed $remotePostId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostConnectorArchiveQuery The current query, for fluid interface
     */
    public function filterByRemotePostId($remotePostId = null, $comparison = null)
    {
        if (is_array($remotePostId)) {
            $useMinMax = false;
            if (isset($remotePostId['min'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_REMOTE_POST_ID, $remotePostId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($remotePostId['max'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_REMOTE_POST_ID, $remotePostId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostConnectorArchiveTableMap::COL_REMOTE_POST_ID, $remotePostId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostConnectorArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostConnectorArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostConnectorArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostConnectorArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostConnectorArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(PostConnectorArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostConnectorArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPostConnectorArchive $postConnectorArchive Object to remove from the list of results
     *
     * @return $this|ChildPostConnectorArchiveQuery The current query, for fluid interface
     */
    public function prune($postConnectorArchive = null)
    {
        if ($postConnectorArchive) {
            $this->addUsingAlias(PostConnectorArchiveTableMap::COL_ID, $postConnectorArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the post_connector_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostConnectorArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PostConnectorArchiveTableMap::clearInstancePool();
            PostConnectorArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostConnectorArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PostConnectorArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PostConnectorArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PostConnectorArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PostConnectorArchiveQuery
