<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\PushNotificationDevice as ChildPushNotificationDevice;
use Database\HubPlus\PushNotificationDeviceQuery as ChildPushNotificationDeviceQuery;
use Database\HubPlus\Map\PushNotificationDeviceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'push_notification_device' table.
 *
 *
 *
 * @method     ChildPushNotificationDeviceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPushNotificationDeviceQuery orderByNotificationId($order = Criteria::ASC) Order by the notification_id column
 * @method     ChildPushNotificationDeviceQuery orderByDeviceId($order = Criteria::ASC) Order by the device_id column
 * @method     ChildPushNotificationDeviceQuery orderByPushNotificationHashId($order = Criteria::ASC) Order by the push_notification_hash_id column
 * @method     ChildPushNotificationDeviceQuery orderByReadAt($order = Criteria::ASC) Order by the read_at column
 *
 * @method     ChildPushNotificationDeviceQuery groupById() Group by the id column
 * @method     ChildPushNotificationDeviceQuery groupByNotificationId() Group by the notification_id column
 * @method     ChildPushNotificationDeviceQuery groupByDeviceId() Group by the device_id column
 * @method     ChildPushNotificationDeviceQuery groupByPushNotificationHashId() Group by the push_notification_hash_id column
 * @method     ChildPushNotificationDeviceQuery groupByReadAt() Group by the read_at column
 *
 * @method     ChildPushNotificationDeviceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPushNotificationDeviceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPushNotificationDeviceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPushNotificationDeviceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPushNotificationDeviceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPushNotificationDeviceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPushNotificationDeviceQuery leftJoinPushNotification($relationAlias = null) Adds a LEFT JOIN clause to the query using the PushNotification relation
 * @method     ChildPushNotificationDeviceQuery rightJoinPushNotification($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PushNotification relation
 * @method     ChildPushNotificationDeviceQuery innerJoinPushNotification($relationAlias = null) Adds a INNER JOIN clause to the query using the PushNotification relation
 *
 * @method     ChildPushNotificationDeviceQuery joinWithPushNotification($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PushNotification relation
 *
 * @method     ChildPushNotificationDeviceQuery leftJoinWithPushNotification() Adds a LEFT JOIN clause and with to the query using the PushNotification relation
 * @method     ChildPushNotificationDeviceQuery rightJoinWithPushNotification() Adds a RIGHT JOIN clause and with to the query using the PushNotification relation
 * @method     ChildPushNotificationDeviceQuery innerJoinWithPushNotification() Adds a INNER JOIN clause and with to the query using the PushNotification relation
 *
 * @method     ChildPushNotificationDeviceQuery leftJoinDevice($relationAlias = null) Adds a LEFT JOIN clause to the query using the Device relation
 * @method     ChildPushNotificationDeviceQuery rightJoinDevice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Device relation
 * @method     ChildPushNotificationDeviceQuery innerJoinDevice($relationAlias = null) Adds a INNER JOIN clause to the query using the Device relation
 *
 * @method     ChildPushNotificationDeviceQuery joinWithDevice($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Device relation
 *
 * @method     ChildPushNotificationDeviceQuery leftJoinWithDevice() Adds a LEFT JOIN clause and with to the query using the Device relation
 * @method     ChildPushNotificationDeviceQuery rightJoinWithDevice() Adds a RIGHT JOIN clause and with to the query using the Device relation
 * @method     ChildPushNotificationDeviceQuery innerJoinWithDevice() Adds a INNER JOIN clause and with to the query using the Device relation
 *
 * @method     \Database\HubPlus\PushNotificationQuery|\Database\HubPlus\DeviceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPushNotificationDevice findOne(ConnectionInterface $con = null) Return the first ChildPushNotificationDevice matching the query
 * @method     ChildPushNotificationDevice findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPushNotificationDevice matching the query, or a new ChildPushNotificationDevice object populated from the query conditions when no match is found
 *
 * @method     ChildPushNotificationDevice findOneById(int $id) Return the first ChildPushNotificationDevice filtered by the id column
 * @method     ChildPushNotificationDevice findOneByNotificationId(int $notification_id) Return the first ChildPushNotificationDevice filtered by the notification_id column
 * @method     ChildPushNotificationDevice findOneByDeviceId(int $device_id) Return the first ChildPushNotificationDevice filtered by the device_id column
 * @method     ChildPushNotificationDevice findOneByPushNotificationHashId(string $push_notification_hash_id) Return the first ChildPushNotificationDevice filtered by the push_notification_hash_id column
 * @method     ChildPushNotificationDevice findOneByReadAt(string $read_at) Return the first ChildPushNotificationDevice filtered by the read_at column *

 * @method     ChildPushNotificationDevice requirePk($key, ConnectionInterface $con = null) Return the ChildPushNotificationDevice by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationDevice requireOne(ConnectionInterface $con = null) Return the first ChildPushNotificationDevice matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPushNotificationDevice requireOneById(int $id) Return the first ChildPushNotificationDevice filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationDevice requireOneByNotificationId(int $notification_id) Return the first ChildPushNotificationDevice filtered by the notification_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationDevice requireOneByDeviceId(int $device_id) Return the first ChildPushNotificationDevice filtered by the device_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationDevice requireOneByPushNotificationHashId(string $push_notification_hash_id) Return the first ChildPushNotificationDevice filtered by the push_notification_hash_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationDevice requireOneByReadAt(string $read_at) Return the first ChildPushNotificationDevice filtered by the read_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPushNotificationDevice[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPushNotificationDevice objects based on current ModelCriteria
 * @method     ChildPushNotificationDevice[]|ObjectCollection findById(int $id) Return ChildPushNotificationDevice objects filtered by the id column
 * @method     ChildPushNotificationDevice[]|ObjectCollection findByNotificationId(int $notification_id) Return ChildPushNotificationDevice objects filtered by the notification_id column
 * @method     ChildPushNotificationDevice[]|ObjectCollection findByDeviceId(int $device_id) Return ChildPushNotificationDevice objects filtered by the device_id column
 * @method     ChildPushNotificationDevice[]|ObjectCollection findByPushNotificationHashId(string $push_notification_hash_id) Return ChildPushNotificationDevice objects filtered by the push_notification_hash_id column
 * @method     ChildPushNotificationDevice[]|ObjectCollection findByReadAt(string $read_at) Return ChildPushNotificationDevice objects filtered by the read_at column
 * @method     ChildPushNotificationDevice[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PushNotificationDeviceQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\PushNotificationDeviceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\PushNotificationDevice', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPushNotificationDeviceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPushNotificationDeviceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPushNotificationDeviceQuery) {
            return $criteria;
        }
        $query = new ChildPushNotificationDeviceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPushNotificationDevice|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PushNotificationDeviceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PushNotificationDeviceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPushNotificationDevice A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, notification_id, device_id, push_notification_hash_id, read_at FROM push_notification_device WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPushNotificationDevice $obj */
            $obj = new ChildPushNotificationDevice();
            $obj->hydrate($row);
            PushNotificationDeviceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPushNotificationDevice|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPushNotificationDeviceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PushNotificationDeviceTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPushNotificationDeviceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PushNotificationDeviceTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationDeviceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PushNotificationDeviceTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PushNotificationDeviceTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationDeviceTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the notification_id column
     *
     * Example usage:
     * <code>
     * $query->filterByNotificationId(1234); // WHERE notification_id = 1234
     * $query->filterByNotificationId(array(12, 34)); // WHERE notification_id IN (12, 34)
     * $query->filterByNotificationId(array('min' => 12)); // WHERE notification_id > 12
     * </code>
     *
     * @see       filterByPushNotification()
     *
     * @param     mixed $notificationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationDeviceQuery The current query, for fluid interface
     */
    public function filterByNotificationId($notificationId = null, $comparison = null)
    {
        if (is_array($notificationId)) {
            $useMinMax = false;
            if (isset($notificationId['min'])) {
                $this->addUsingAlias(PushNotificationDeviceTableMap::COL_NOTIFICATION_ID, $notificationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($notificationId['max'])) {
                $this->addUsingAlias(PushNotificationDeviceTableMap::COL_NOTIFICATION_ID, $notificationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationDeviceTableMap::COL_NOTIFICATION_ID, $notificationId, $comparison);
    }

    /**
     * Filter the query on the device_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDeviceId(1234); // WHERE device_id = 1234
     * $query->filterByDeviceId(array(12, 34)); // WHERE device_id IN (12, 34)
     * $query->filterByDeviceId(array('min' => 12)); // WHERE device_id > 12
     * </code>
     *
     * @see       filterByDevice()
     *
     * @param     mixed $deviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationDeviceQuery The current query, for fluid interface
     */
    public function filterByDeviceId($deviceId = null, $comparison = null)
    {
        if (is_array($deviceId)) {
            $useMinMax = false;
            if (isset($deviceId['min'])) {
                $this->addUsingAlias(PushNotificationDeviceTableMap::COL_DEVICE_ID, $deviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deviceId['max'])) {
                $this->addUsingAlias(PushNotificationDeviceTableMap::COL_DEVICE_ID, $deviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationDeviceTableMap::COL_DEVICE_ID, $deviceId, $comparison);
    }

    /**
     * Filter the query on the push_notification_hash_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPushNotificationHashId('fooValue');   // WHERE push_notification_hash_id = 'fooValue'
     * $query->filterByPushNotificationHashId('%fooValue%', Criteria::LIKE); // WHERE push_notification_hash_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pushNotificationHashId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationDeviceQuery The current query, for fluid interface
     */
    public function filterByPushNotificationHashId($pushNotificationHashId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pushNotificationHashId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationDeviceTableMap::COL_PUSH_NOTIFICATION_HASH_ID, $pushNotificationHashId, $comparison);
    }

    /**
     * Filter the query on the read_at column
     *
     * Example usage:
     * <code>
     * $query->filterByReadAt('2011-03-14'); // WHERE read_at = '2011-03-14'
     * $query->filterByReadAt('now'); // WHERE read_at = '2011-03-14'
     * $query->filterByReadAt(array('max' => 'yesterday')); // WHERE read_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $readAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationDeviceQuery The current query, for fluid interface
     */
    public function filterByReadAt($readAt = null, $comparison = null)
    {
        if (is_array($readAt)) {
            $useMinMax = false;
            if (isset($readAt['min'])) {
                $this->addUsingAlias(PushNotificationDeviceTableMap::COL_READ_AT, $readAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($readAt['max'])) {
                $this->addUsingAlias(PushNotificationDeviceTableMap::COL_READ_AT, $readAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationDeviceTableMap::COL_READ_AT, $readAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\PushNotification object
     *
     * @param \Database\HubPlus\PushNotification|ObjectCollection $pushNotification The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPushNotificationDeviceQuery The current query, for fluid interface
     */
    public function filterByPushNotification($pushNotification, $comparison = null)
    {
        if ($pushNotification instanceof \Database\HubPlus\PushNotification) {
            return $this
                ->addUsingAlias(PushNotificationDeviceTableMap::COL_NOTIFICATION_ID, $pushNotification->getId(), $comparison);
        } elseif ($pushNotification instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PushNotificationDeviceTableMap::COL_NOTIFICATION_ID, $pushNotification->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPushNotification() only accepts arguments of type \Database\HubPlus\PushNotification or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PushNotification relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPushNotificationDeviceQuery The current query, for fluid interface
     */
    public function joinPushNotification($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PushNotification');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PushNotification');
        }

        return $this;
    }

    /**
     * Use the PushNotification relation PushNotification object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PushNotificationQuery A secondary query class using the current class as primary query
     */
    public function usePushNotificationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPushNotification($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PushNotification', '\Database\HubPlus\PushNotificationQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Device object
     *
     * @param \Database\HubPlus\Device|ObjectCollection $device The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPushNotificationDeviceQuery The current query, for fluid interface
     */
    public function filterByDevice($device, $comparison = null)
    {
        if ($device instanceof \Database\HubPlus\Device) {
            return $this
                ->addUsingAlias(PushNotificationDeviceTableMap::COL_DEVICE_ID, $device->getId(), $comparison);
        } elseif ($device instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PushNotificationDeviceTableMap::COL_DEVICE_ID, $device->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDevice() only accepts arguments of type \Database\HubPlus\Device or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Device relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPushNotificationDeviceQuery The current query, for fluid interface
     */
    public function joinDevice($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Device');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Device');
        }

        return $this;
    }

    /**
     * Use the Device relation Device object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\DeviceQuery A secondary query class using the current class as primary query
     */
    public function useDeviceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDevice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Device', '\Database\HubPlus\DeviceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPushNotificationDevice $pushNotificationDevice Object to remove from the list of results
     *
     * @return $this|ChildPushNotificationDeviceQuery The current query, for fluid interface
     */
    public function prune($pushNotificationDevice = null)
    {
        if ($pushNotificationDevice) {
            $this->addUsingAlias(PushNotificationDeviceTableMap::COL_ID, $pushNotificationDevice->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // aggregate_column_relation_read_message_count behavior
        $this->findRelatedPushNotificationReadMessageCounts($con);

        return $this->preDelete($con);
    }

    /**
     * Code to execute after every DELETE statement
     *
     * @param     int $affectedRows the number of deleted rows
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePostDelete($affectedRows, ConnectionInterface $con)
    {
        // aggregate_column_relation_read_message_count behavior
        $this->updateRelatedPushNotificationReadMessageCounts($con);

        return $this->postDelete($affectedRows, $con);
    }

    /**
     * Code to execute before every UPDATE statement
     *
     * @param     array $values The associative array of columns and values for the update
     * @param     ConnectionInterface $con The connection object used by the query
     * @param     boolean $forceIndividualSaves If false (default), the resulting call is a Criteria::doUpdate(), otherwise it is a series of save() calls on all the found objects
     */
    protected function basePreUpdate(&$values, ConnectionInterface $con, $forceIndividualSaves = false)
    {
        // aggregate_column_relation_read_message_count behavior
        $this->findRelatedPushNotificationReadMessageCounts($con);

        return $this->preUpdate($values, $con, $forceIndividualSaves);
    }

    /**
     * Code to execute after every UPDATE statement
     *
     * @param     int $affectedRows the number of updated rows
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePostUpdate($affectedRows, ConnectionInterface $con)
    {
        // aggregate_column_relation_read_message_count behavior
        $this->updateRelatedPushNotificationReadMessageCounts($con);

        return $this->postUpdate($affectedRows, $con);
    }

    /**
     * Deletes all rows from the push_notification_device table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PushNotificationDeviceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PushNotificationDeviceTableMap::clearInstancePool();
            PushNotificationDeviceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PushNotificationDeviceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PushNotificationDeviceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PushNotificationDeviceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PushNotificationDeviceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // aggregate_column_relation_read_message_count behavior

    /**
     * Finds the related PushNotification objects and keep them for later
     *
     * @param ConnectionInterface $con A connection object
     */
    protected function findRelatedPushNotificationReadMessageCounts($con)
    {
        $criteria = clone $this;
        if ($this->useAliasInSQL) {
            $alias = $this->getModelAlias();
            $criteria->removeAlias($alias);
        } else {
            $alias = '';
        }
        $this->pushNotificationReadMessageCounts = \Database\HubPlus\PushNotificationQuery::create()
            ->joinPushNotificationDevice($alias)
            ->mergeWith($criteria)
            ->find($con);
    }

    protected function updateRelatedPushNotificationReadMessageCounts($con)
    {
        foreach ($this->pushNotificationReadMessageCounts as $pushNotificationReadMessageCount) {
            $pushNotificationReadMessageCount->updateReadMessageCount($con);
        }
        $this->pushNotificationReadMessageCounts = array();
    }

} // PushNotificationDeviceQuery
