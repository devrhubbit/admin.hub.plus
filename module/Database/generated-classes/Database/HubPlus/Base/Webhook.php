<?php

namespace Database\HubPlus\Base;

use \DateTime;
use \Exception;
use \PDO;
use Database\HubPlus\Post as ChildPost;
use Database\HubPlus\PostQuery as ChildPostQuery;
use Database\HubPlus\RemoteProvider as ChildRemoteProvider;
use Database\HubPlus\RemoteProviderQuery as ChildRemoteProviderQuery;
use Database\HubPlus\Section as ChildSection;
use Database\HubPlus\SectionQuery as ChildSectionQuery;
use Database\HubPlus\Webhook as ChildWebhook;
use Database\HubPlus\WebhookQuery as ChildWebhookQuery;
use Database\HubPlus\WebhookQueue as ChildWebhookQueue;
use Database\HubPlus\WebhookQueueQuery as ChildWebhookQueueQuery;
use Database\HubPlus\Map\WebhookQueueTableMap;
use Database\HubPlus\Map\WebhookTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'webhook' table.
 *
 *
 *
 * @package    propel.generator.Database.HubPlus.Base
 */
abstract class Webhook implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Database\\HubPlus\\Map\\WebhookTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the bind_to field.
     *
     * @var        string
     */
    protected $bind_to;

    /**
     * The value for the hash_id field.
     *
     * @var        string
     */
    protected $hash_id;

    /**
     * The value for the url field.
     *
     * @var        string
     */
    protected $url;

    /**
     * The value for the provider_id field.
     *
     * @var        int
     */
    protected $provider_id;

    /**
     * The value for the section_id field.
     *
     * @var        int
     */
    protected $section_id;

    /**
     * The value for the post_id field.
     *
     * @var        int
     */
    protected $post_id;

    /**
     * The value for the seconds_delay field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $seconds_delay;

    /**
     * The value for the max_attempts field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $max_attempts;

    /**
     * The value for the enabled field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $enabled;

    /**
     * The value for the last_trigger_at field.
     *
     * @var        DateTime
     */
    protected $last_trigger_at;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildRemoteProvider
     */
    protected $aRemoteProvider;

    /**
     * @var        ChildSection
     */
    protected $aSection;

    /**
     * @var        ChildPost
     */
    protected $aPost;

    /**
     * @var        ObjectCollection|ChildWebhookQueue[] Collection to store aggregation of ChildWebhookQueue objects.
     */
    protected $collWebhookQueues;
    protected $collWebhookQueuesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildWebhookQueue[]
     */
    protected $webhookQueuesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->seconds_delay = 0;
        $this->max_attempts = 1;
        $this->enabled = 1;
    }

    /**
     * Initializes internal state of Database\HubPlus\Base\Webhook object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Webhook</code> instance.  If
     * <code>obj</code> is an instance of <code>Webhook</code>, delegates to
     * <code>equals(Webhook)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Webhook The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [bind_to] column value.
     *
     * @return string
     */
    public function getBindTo()
    {
        return $this->bind_to;
    }

    /**
     * Get the [hash_id] column value.
     *
     * @return string
     */
    public function getHashId()
    {
        return $this->hash_id;
    }

    /**
     * Get the [url] column value.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the [provider_id] column value.
     *
     * @return int
     */
    public function getProviderId()
    {
        return $this->provider_id;
    }

    /**
     * Get the [section_id] column value.
     *
     * @return int
     */
    public function getSectionId()
    {
        return $this->section_id;
    }

    /**
     * Get the [post_id] column value.
     *
     * @return int
     */
    public function getPostId()
    {
        return $this->post_id;
    }

    /**
     * Get the [seconds_delay] column value.
     *
     * @return int
     */
    public function getSecondsDelay()
    {
        return $this->seconds_delay;
    }

    /**
     * Get the [max_attempts] column value.
     *
     * @return int
     */
    public function getMaxAttempts()
    {
        return $this->max_attempts;
    }

    /**
     * Get the [enabled] column value.
     *
     * @return int
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get the [optionally formatted] temporal [last_trigger_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastTriggerAt($format = NULL)
    {
        if ($format === null) {
            return $this->last_trigger_at;
        } else {
            return $this->last_trigger_at instanceof \DateTimeInterface ? $this->last_trigger_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[WebhookTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [bind_to] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setBindTo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bind_to !== $v) {
            $this->bind_to = $v;
            $this->modifiedColumns[WebhookTableMap::COL_BIND_TO] = true;
        }

        return $this;
    } // setBindTo()

    /**
     * Set the value of [hash_id] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setHashId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->hash_id !== $v) {
            $this->hash_id = $v;
            $this->modifiedColumns[WebhookTableMap::COL_HASH_ID] = true;
        }

        return $this;
    } // setHashId()

    /**
     * Set the value of [url] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[WebhookTableMap::COL_URL] = true;
        }

        return $this;
    } // setUrl()

    /**
     * Set the value of [provider_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setProviderId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->provider_id !== $v) {
            $this->provider_id = $v;
            $this->modifiedColumns[WebhookTableMap::COL_PROVIDER_ID] = true;
        }

        if ($this->aRemoteProvider !== null && $this->aRemoteProvider->getId() !== $v) {
            $this->aRemoteProvider = null;
        }

        return $this;
    } // setProviderId()

    /**
     * Set the value of [section_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setSectionId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->section_id !== $v) {
            $this->section_id = $v;
            $this->modifiedColumns[WebhookTableMap::COL_SECTION_ID] = true;
        }

        if ($this->aSection !== null && $this->aSection->getId() !== $v) {
            $this->aSection = null;
        }

        return $this;
    } // setSectionId()

    /**
     * Set the value of [post_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setPostId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->post_id !== $v) {
            $this->post_id = $v;
            $this->modifiedColumns[WebhookTableMap::COL_POST_ID] = true;
        }

        if ($this->aPost !== null && $this->aPost->getId() !== $v) {
            $this->aPost = null;
        }

        return $this;
    } // setPostId()

    /**
     * Set the value of [seconds_delay] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setSecondsDelay($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->seconds_delay !== $v) {
            $this->seconds_delay = $v;
            $this->modifiedColumns[WebhookTableMap::COL_SECONDS_DELAY] = true;
        }

        return $this;
    } // setSecondsDelay()

    /**
     * Set the value of [max_attempts] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setMaxAttempts($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->max_attempts !== $v) {
            $this->max_attempts = $v;
            $this->modifiedColumns[WebhookTableMap::COL_MAX_ATTEMPTS] = true;
        }

        return $this;
    } // setMaxAttempts()

    /**
     * Set the value of [enabled] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setEnabled($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->enabled !== $v) {
            $this->enabled = $v;
            $this->modifiedColumns[WebhookTableMap::COL_ENABLED] = true;
        }

        return $this;
    } // setEnabled()

    /**
     * Sets the value of [last_trigger_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setLastTriggerAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_trigger_at !== null || $dt !== null) {
            if ($this->last_trigger_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->last_trigger_at->format("Y-m-d H:i:s.u")) {
                $this->last_trigger_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[WebhookTableMap::COL_LAST_TRIGGER_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setLastTriggerAt()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[WebhookTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[WebhookTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->seconds_delay !== 0) {
                return false;
            }

            if ($this->max_attempts !== 1) {
                return false;
            }

            if ($this->enabled !== 1) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : WebhookTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : WebhookTableMap::translateFieldName('BindTo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bind_to = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : WebhookTableMap::translateFieldName('HashId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->hash_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : WebhookTableMap::translateFieldName('Url', TableMap::TYPE_PHPNAME, $indexType)];
            $this->url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : WebhookTableMap::translateFieldName('ProviderId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->provider_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : WebhookTableMap::translateFieldName('SectionId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->section_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : WebhookTableMap::translateFieldName('PostId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->post_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : WebhookTableMap::translateFieldName('SecondsDelay', TableMap::TYPE_PHPNAME, $indexType)];
            $this->seconds_delay = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : WebhookTableMap::translateFieldName('MaxAttempts', TableMap::TYPE_PHPNAME, $indexType)];
            $this->max_attempts = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : WebhookTableMap::translateFieldName('Enabled', TableMap::TYPE_PHPNAME, $indexType)];
            $this->enabled = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : WebhookTableMap::translateFieldName('LastTriggerAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->last_trigger_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : WebhookTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : WebhookTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 13; // 13 = WebhookTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Database\\HubPlus\\Webhook'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aRemoteProvider !== null && $this->provider_id !== $this->aRemoteProvider->getId()) {
            $this->aRemoteProvider = null;
        }
        if ($this->aSection !== null && $this->section_id !== $this->aSection->getId()) {
            $this->aSection = null;
        }
        if ($this->aPost !== null && $this->post_id !== $this->aPost->getId()) {
            $this->aPost = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(WebhookTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildWebhookQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aRemoteProvider = null;
            $this->aSection = null;
            $this->aPost = null;
            $this->collWebhookQueues = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Webhook::setDeleted()
     * @see Webhook::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(WebhookTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildWebhookQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(WebhookTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(WebhookTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(WebhookTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(WebhookTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                WebhookTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aRemoteProvider !== null) {
                if ($this->aRemoteProvider->isModified() || $this->aRemoteProvider->isNew()) {
                    $affectedRows += $this->aRemoteProvider->save($con);
                }
                $this->setRemoteProvider($this->aRemoteProvider);
            }

            if ($this->aSection !== null) {
                if ($this->aSection->isModified() || $this->aSection->isNew()) {
                    $affectedRows += $this->aSection->save($con);
                }
                $this->setSection($this->aSection);
            }

            if ($this->aPost !== null) {
                if ($this->aPost->isModified() || $this->aPost->isNew()) {
                    $affectedRows += $this->aPost->save($con);
                }
                $this->setPost($this->aPost);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->webhookQueuesScheduledForDeletion !== null) {
                if (!$this->webhookQueuesScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\WebhookQueueQuery::create()
                        ->filterByPrimaryKeys($this->webhookQueuesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->webhookQueuesScheduledForDeletion = null;
                }
            }

            if ($this->collWebhookQueues !== null) {
                foreach ($this->collWebhookQueues as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[WebhookTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . WebhookTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(WebhookTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(WebhookTableMap::COL_BIND_TO)) {
            $modifiedColumns[':p' . $index++]  = 'bind_to';
        }
        if ($this->isColumnModified(WebhookTableMap::COL_HASH_ID)) {
            $modifiedColumns[':p' . $index++]  = 'hash_id';
        }
        if ($this->isColumnModified(WebhookTableMap::COL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'url';
        }
        if ($this->isColumnModified(WebhookTableMap::COL_PROVIDER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'provider_id';
        }
        if ($this->isColumnModified(WebhookTableMap::COL_SECTION_ID)) {
            $modifiedColumns[':p' . $index++]  = 'section_id';
        }
        if ($this->isColumnModified(WebhookTableMap::COL_POST_ID)) {
            $modifiedColumns[':p' . $index++]  = 'post_id';
        }
        if ($this->isColumnModified(WebhookTableMap::COL_SECONDS_DELAY)) {
            $modifiedColumns[':p' . $index++]  = 'seconds_delay';
        }
        if ($this->isColumnModified(WebhookTableMap::COL_MAX_ATTEMPTS)) {
            $modifiedColumns[':p' . $index++]  = 'max_attempts';
        }
        if ($this->isColumnModified(WebhookTableMap::COL_ENABLED)) {
            $modifiedColumns[':p' . $index++]  = 'enabled';
        }
        if ($this->isColumnModified(WebhookTableMap::COL_LAST_TRIGGER_AT)) {
            $modifiedColumns[':p' . $index++]  = 'last_trigger_at';
        }
        if ($this->isColumnModified(WebhookTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(WebhookTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO webhook (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'bind_to':
                        $stmt->bindValue($identifier, $this->bind_to, PDO::PARAM_STR);
                        break;
                    case 'hash_id':
                        $stmt->bindValue($identifier, $this->hash_id, PDO::PARAM_STR);
                        break;
                    case 'url':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case 'provider_id':
                        $stmt->bindValue($identifier, $this->provider_id, PDO::PARAM_INT);
                        break;
                    case 'section_id':
                        $stmt->bindValue($identifier, $this->section_id, PDO::PARAM_INT);
                        break;
                    case 'post_id':
                        $stmt->bindValue($identifier, $this->post_id, PDO::PARAM_INT);
                        break;
                    case 'seconds_delay':
                        $stmt->bindValue($identifier, $this->seconds_delay, PDO::PARAM_INT);
                        break;
                    case 'max_attempts':
                        $stmt->bindValue($identifier, $this->max_attempts, PDO::PARAM_INT);
                        break;
                    case 'enabled':
                        $stmt->bindValue($identifier, $this->enabled, PDO::PARAM_INT);
                        break;
                    case 'last_trigger_at':
                        $stmt->bindValue($identifier, $this->last_trigger_at ? $this->last_trigger_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = WebhookTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getBindTo();
                break;
            case 2:
                return $this->getHashId();
                break;
            case 3:
                return $this->getUrl();
                break;
            case 4:
                return $this->getProviderId();
                break;
            case 5:
                return $this->getSectionId();
                break;
            case 6:
                return $this->getPostId();
                break;
            case 7:
                return $this->getSecondsDelay();
                break;
            case 8:
                return $this->getMaxAttempts();
                break;
            case 9:
                return $this->getEnabled();
                break;
            case 10:
                return $this->getLastTriggerAt();
                break;
            case 11:
                return $this->getCreatedAt();
                break;
            case 12:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Webhook'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Webhook'][$this->hashCode()] = true;
        $keys = WebhookTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getBindTo(),
            $keys[2] => $this->getHashId(),
            $keys[3] => $this->getUrl(),
            $keys[4] => $this->getProviderId(),
            $keys[5] => $this->getSectionId(),
            $keys[6] => $this->getPostId(),
            $keys[7] => $this->getSecondsDelay(),
            $keys[8] => $this->getMaxAttempts(),
            $keys[9] => $this->getEnabled(),
            $keys[10] => $this->getLastTriggerAt(),
            $keys[11] => $this->getCreatedAt(),
            $keys[12] => $this->getUpdatedAt(),
        );
        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        if ($result[$keys[12]] instanceof \DateTimeInterface) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aRemoteProvider) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'remoteProvider';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'remote_provider';
                        break;
                    default:
                        $key = 'RemoteProvider';
                }

                $result[$key] = $this->aRemoteProvider->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSection) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'section';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'section';
                        break;
                    default:
                        $key = 'Section';
                }

                $result[$key] = $this->aSection->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPost) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'post';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post';
                        break;
                    default:
                        $key = 'Post';
                }

                $result[$key] = $this->aPost->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collWebhookQueues) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'webhookQueues';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'webhook_queues';
                        break;
                    default:
                        $key = 'WebhookQueues';
                }

                $result[$key] = $this->collWebhookQueues->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Database\HubPlus\Webhook
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = WebhookTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Database\HubPlus\Webhook
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setBindTo($value);
                break;
            case 2:
                $this->setHashId($value);
                break;
            case 3:
                $this->setUrl($value);
                break;
            case 4:
                $this->setProviderId($value);
                break;
            case 5:
                $this->setSectionId($value);
                break;
            case 6:
                $this->setPostId($value);
                break;
            case 7:
                $this->setSecondsDelay($value);
                break;
            case 8:
                $this->setMaxAttempts($value);
                break;
            case 9:
                $this->setEnabled($value);
                break;
            case 10:
                $this->setLastTriggerAt($value);
                break;
            case 11:
                $this->setCreatedAt($value);
                break;
            case 12:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = WebhookTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setBindTo($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setHashId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setUrl($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setProviderId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setSectionId($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setPostId($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setSecondsDelay($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setMaxAttempts($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setEnabled($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setLastTriggerAt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setCreatedAt($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setUpdatedAt($arr[$keys[12]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Database\HubPlus\Webhook The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(WebhookTableMap::DATABASE_NAME);

        if ($this->isColumnModified(WebhookTableMap::COL_ID)) {
            $criteria->add(WebhookTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(WebhookTableMap::COL_BIND_TO)) {
            $criteria->add(WebhookTableMap::COL_BIND_TO, $this->bind_to);
        }
        if ($this->isColumnModified(WebhookTableMap::COL_HASH_ID)) {
            $criteria->add(WebhookTableMap::COL_HASH_ID, $this->hash_id);
        }
        if ($this->isColumnModified(WebhookTableMap::COL_URL)) {
            $criteria->add(WebhookTableMap::COL_URL, $this->url);
        }
        if ($this->isColumnModified(WebhookTableMap::COL_PROVIDER_ID)) {
            $criteria->add(WebhookTableMap::COL_PROVIDER_ID, $this->provider_id);
        }
        if ($this->isColumnModified(WebhookTableMap::COL_SECTION_ID)) {
            $criteria->add(WebhookTableMap::COL_SECTION_ID, $this->section_id);
        }
        if ($this->isColumnModified(WebhookTableMap::COL_POST_ID)) {
            $criteria->add(WebhookTableMap::COL_POST_ID, $this->post_id);
        }
        if ($this->isColumnModified(WebhookTableMap::COL_SECONDS_DELAY)) {
            $criteria->add(WebhookTableMap::COL_SECONDS_DELAY, $this->seconds_delay);
        }
        if ($this->isColumnModified(WebhookTableMap::COL_MAX_ATTEMPTS)) {
            $criteria->add(WebhookTableMap::COL_MAX_ATTEMPTS, $this->max_attempts);
        }
        if ($this->isColumnModified(WebhookTableMap::COL_ENABLED)) {
            $criteria->add(WebhookTableMap::COL_ENABLED, $this->enabled);
        }
        if ($this->isColumnModified(WebhookTableMap::COL_LAST_TRIGGER_AT)) {
            $criteria->add(WebhookTableMap::COL_LAST_TRIGGER_AT, $this->last_trigger_at);
        }
        if ($this->isColumnModified(WebhookTableMap::COL_CREATED_AT)) {
            $criteria->add(WebhookTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(WebhookTableMap::COL_UPDATED_AT)) {
            $criteria->add(WebhookTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildWebhookQuery::create();
        $criteria->add(WebhookTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Database\HubPlus\Webhook (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setBindTo($this->getBindTo());
        $copyObj->setHashId($this->getHashId());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setProviderId($this->getProviderId());
        $copyObj->setSectionId($this->getSectionId());
        $copyObj->setPostId($this->getPostId());
        $copyObj->setSecondsDelay($this->getSecondsDelay());
        $copyObj->setMaxAttempts($this->getMaxAttempts());
        $copyObj->setEnabled($this->getEnabled());
        $copyObj->setLastTriggerAt($this->getLastTriggerAt());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getWebhookQueues() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWebhookQueue($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Database\HubPlus\Webhook Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildRemoteProvider object.
     *
     * @param  ChildRemoteProvider $v
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     * @throws PropelException
     */
    public function setRemoteProvider(ChildRemoteProvider $v = null)
    {
        if ($v === null) {
            $this->setProviderId(NULL);
        } else {
            $this->setProviderId($v->getId());
        }

        $this->aRemoteProvider = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildRemoteProvider object, it will not be re-added.
        if ($v !== null) {
            $v->addWebhook($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildRemoteProvider object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildRemoteProvider The associated ChildRemoteProvider object.
     * @throws PropelException
     */
    public function getRemoteProvider(ConnectionInterface $con = null)
    {
        if ($this->aRemoteProvider === null && ($this->provider_id != 0)) {
            $this->aRemoteProvider = ChildRemoteProviderQuery::create()->findPk($this->provider_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aRemoteProvider->addWebhooks($this);
             */
        }

        return $this->aRemoteProvider;
    }

    /**
     * Declares an association between this object and a ChildSection object.
     *
     * @param  ChildSection $v
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSection(ChildSection $v = null)
    {
        if ($v === null) {
            $this->setSectionId(NULL);
        } else {
            $this->setSectionId($v->getId());
        }

        $this->aSection = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSection object, it will not be re-added.
        if ($v !== null) {
            $v->addWebhook($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSection object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSection The associated ChildSection object.
     * @throws PropelException
     */
    public function getSection(ConnectionInterface $con = null)
    {
        if ($this->aSection === null && ($this->section_id != 0)) {
            $this->aSection = ChildSectionQuery::create()->findPk($this->section_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSection->addWebhooks($this);
             */
        }

        return $this->aSection;
    }

    /**
     * Declares an association between this object and a ChildPost object.
     *
     * @param  ChildPost $v
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPost(ChildPost $v = null)
    {
        if ($v === null) {
            $this->setPostId(NULL);
        } else {
            $this->setPostId($v->getId());
        }

        $this->aPost = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPost object, it will not be re-added.
        if ($v !== null) {
            $v->addWebhook($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPost object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPost The associated ChildPost object.
     * @throws PropelException
     */
    public function getPost(ConnectionInterface $con = null)
    {
        if ($this->aPost === null && ($this->post_id != 0)) {
            $this->aPost = ChildPostQuery::create()->findPk($this->post_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPost->addWebhooks($this);
             */
        }

        return $this->aPost;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('WebhookQueue' == $relationName) {
            $this->initWebhookQueues();
            return;
        }
    }

    /**
     * Clears out the collWebhookQueues collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addWebhookQueues()
     */
    public function clearWebhookQueues()
    {
        $this->collWebhookQueues = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collWebhookQueues collection loaded partially.
     */
    public function resetPartialWebhookQueues($v = true)
    {
        $this->collWebhookQueuesPartial = $v;
    }

    /**
     * Initializes the collWebhookQueues collection.
     *
     * By default this just sets the collWebhookQueues collection to an empty array (like clearcollWebhookQueues());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWebhookQueues($overrideExisting = true)
    {
        if (null !== $this->collWebhookQueues && !$overrideExisting) {
            return;
        }

        $collectionClassName = WebhookQueueTableMap::getTableMap()->getCollectionClassName();

        $this->collWebhookQueues = new $collectionClassName;
        $this->collWebhookQueues->setModel('\Database\HubPlus\WebhookQueue');
    }

    /**
     * Gets an array of ChildWebhookQueue objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildWebhook is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildWebhookQueue[] List of ChildWebhookQueue objects
     * @throws PropelException
     */
    public function getWebhookQueues(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collWebhookQueuesPartial && !$this->isNew();
        if (null === $this->collWebhookQueues || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWebhookQueues) {
                // return empty collection
                $this->initWebhookQueues();
            } else {
                $collWebhookQueues = ChildWebhookQueueQuery::create(null, $criteria)
                    ->filterByWebhook($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collWebhookQueuesPartial && count($collWebhookQueues)) {
                        $this->initWebhookQueues(false);

                        foreach ($collWebhookQueues as $obj) {
                            if (false == $this->collWebhookQueues->contains($obj)) {
                                $this->collWebhookQueues->append($obj);
                            }
                        }

                        $this->collWebhookQueuesPartial = true;
                    }

                    return $collWebhookQueues;
                }

                if ($partial && $this->collWebhookQueues) {
                    foreach ($this->collWebhookQueues as $obj) {
                        if ($obj->isNew()) {
                            $collWebhookQueues[] = $obj;
                        }
                    }
                }

                $this->collWebhookQueues = $collWebhookQueues;
                $this->collWebhookQueuesPartial = false;
            }
        }

        return $this->collWebhookQueues;
    }

    /**
     * Sets a collection of ChildWebhookQueue objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $webhookQueues A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildWebhook The current object (for fluent API support)
     */
    public function setWebhookQueues(Collection $webhookQueues, ConnectionInterface $con = null)
    {
        /** @var ChildWebhookQueue[] $webhookQueuesToDelete */
        $webhookQueuesToDelete = $this->getWebhookQueues(new Criteria(), $con)->diff($webhookQueues);


        $this->webhookQueuesScheduledForDeletion = $webhookQueuesToDelete;

        foreach ($webhookQueuesToDelete as $webhookQueueRemoved) {
            $webhookQueueRemoved->setWebhook(null);
        }

        $this->collWebhookQueues = null;
        foreach ($webhookQueues as $webhookQueue) {
            $this->addWebhookQueue($webhookQueue);
        }

        $this->collWebhookQueues = $webhookQueues;
        $this->collWebhookQueuesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related WebhookQueue objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related WebhookQueue objects.
     * @throws PropelException
     */
    public function countWebhookQueues(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collWebhookQueuesPartial && !$this->isNew();
        if (null === $this->collWebhookQueues || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWebhookQueues) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getWebhookQueues());
            }

            $query = ChildWebhookQueueQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByWebhook($this)
                ->count($con);
        }

        return count($this->collWebhookQueues);
    }

    /**
     * Method called to associate a ChildWebhookQueue object to this object
     * through the ChildWebhookQueue foreign key attribute.
     *
     * @param  ChildWebhookQueue $l ChildWebhookQueue
     * @return $this|\Database\HubPlus\Webhook The current object (for fluent API support)
     */
    public function addWebhookQueue(ChildWebhookQueue $l)
    {
        if ($this->collWebhookQueues === null) {
            $this->initWebhookQueues();
            $this->collWebhookQueuesPartial = true;
        }

        if (!$this->collWebhookQueues->contains($l)) {
            $this->doAddWebhookQueue($l);

            if ($this->webhookQueuesScheduledForDeletion and $this->webhookQueuesScheduledForDeletion->contains($l)) {
                $this->webhookQueuesScheduledForDeletion->remove($this->webhookQueuesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildWebhookQueue $webhookQueue The ChildWebhookQueue object to add.
     */
    protected function doAddWebhookQueue(ChildWebhookQueue $webhookQueue)
    {
        $this->collWebhookQueues[]= $webhookQueue;
        $webhookQueue->setWebhook($this);
    }

    /**
     * @param  ChildWebhookQueue $webhookQueue The ChildWebhookQueue object to remove.
     * @return $this|ChildWebhook The current object (for fluent API support)
     */
    public function removeWebhookQueue(ChildWebhookQueue $webhookQueue)
    {
        if ($this->getWebhookQueues()->contains($webhookQueue)) {
            $pos = $this->collWebhookQueues->search($webhookQueue);
            $this->collWebhookQueues->remove($pos);
            if (null === $this->webhookQueuesScheduledForDeletion) {
                $this->webhookQueuesScheduledForDeletion = clone $this->collWebhookQueues;
                $this->webhookQueuesScheduledForDeletion->clear();
            }
            $this->webhookQueuesScheduledForDeletion[]= clone $webhookQueue;
            $webhookQueue->setWebhook(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aRemoteProvider) {
            $this->aRemoteProvider->removeWebhook($this);
        }
        if (null !== $this->aSection) {
            $this->aSection->removeWebhook($this);
        }
        if (null !== $this->aPost) {
            $this->aPost->removeWebhook($this);
        }
        $this->id = null;
        $this->bind_to = null;
        $this->hash_id = null;
        $this->url = null;
        $this->provider_id = null;
        $this->section_id = null;
        $this->post_id = null;
        $this->seconds_delay = null;
        $this->max_attempts = null;
        $this->enabled = null;
        $this->last_trigger_at = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collWebhookQueues) {
                foreach ($this->collWebhookQueues as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collWebhookQueues = null;
        $this->aRemoteProvider = null;
        $this->aSection = null;
        $this->aPost = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(WebhookTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildWebhook The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[WebhookTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
