<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\PushNotificationArchive as ChildPushNotificationArchive;
use Database\HubPlus\PushNotificationArchiveQuery as ChildPushNotificationArchiveQuery;
use Database\HubPlus\Map\PushNotificationArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'push_notification_archive' table.
 *
 *
 *
 * @method     ChildPushNotificationArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPushNotificationArchiveQuery orderByLang($order = Criteria::ASC) Order by the lang column
 * @method     ChildPushNotificationArchiveQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildPushNotificationArchiveQuery orderByBody($order = Criteria::ASC) Order by the body column
 * @method     ChildPushNotificationArchiveQuery orderBySectionId($order = Criteria::ASC) Order by the section_id column
 * @method     ChildPushNotificationArchiveQuery orderByPostId($order = Criteria::ASC) Order by the post_id column
 * @method     ChildPushNotificationArchiveQuery orderBySendDate($order = Criteria::ASC) Order by the send_date column
 * @method     ChildPushNotificationArchiveQuery orderByReceiverFilter($order = Criteria::ASC) Order by the receiver_filter column
 * @method     ChildPushNotificationArchiveQuery orderByDeviceCount($order = Criteria::ASC) Order by the device_count column
 * @method     ChildPushNotificationArchiveQuery orderByReadMessageCount($order = Criteria::ASC) Order by the read_message_count column
 * @method     ChildPushNotificationArchiveQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildPushNotificationArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPushNotificationArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildPushNotificationArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildPushNotificationArchiveQuery groupById() Group by the id column
 * @method     ChildPushNotificationArchiveQuery groupByLang() Group by the lang column
 * @method     ChildPushNotificationArchiveQuery groupByTitle() Group by the title column
 * @method     ChildPushNotificationArchiveQuery groupByBody() Group by the body column
 * @method     ChildPushNotificationArchiveQuery groupBySectionId() Group by the section_id column
 * @method     ChildPushNotificationArchiveQuery groupByPostId() Group by the post_id column
 * @method     ChildPushNotificationArchiveQuery groupBySendDate() Group by the send_date column
 * @method     ChildPushNotificationArchiveQuery groupByReceiverFilter() Group by the receiver_filter column
 * @method     ChildPushNotificationArchiveQuery groupByDeviceCount() Group by the device_count column
 * @method     ChildPushNotificationArchiveQuery groupByReadMessageCount() Group by the read_message_count column
 * @method     ChildPushNotificationArchiveQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildPushNotificationArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPushNotificationArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildPushNotificationArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildPushNotificationArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPushNotificationArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPushNotificationArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPushNotificationArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPushNotificationArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPushNotificationArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPushNotificationArchive findOne(ConnectionInterface $con = null) Return the first ChildPushNotificationArchive matching the query
 * @method     ChildPushNotificationArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPushNotificationArchive matching the query, or a new ChildPushNotificationArchive object populated from the query conditions when no match is found
 *
 * @method     ChildPushNotificationArchive findOneById(int $id) Return the first ChildPushNotificationArchive filtered by the id column
 * @method     ChildPushNotificationArchive findOneByLang(string $lang) Return the first ChildPushNotificationArchive filtered by the lang column
 * @method     ChildPushNotificationArchive findOneByTitle(string $title) Return the first ChildPushNotificationArchive filtered by the title column
 * @method     ChildPushNotificationArchive findOneByBody(string $body) Return the first ChildPushNotificationArchive filtered by the body column
 * @method     ChildPushNotificationArchive findOneBySectionId(int $section_id) Return the first ChildPushNotificationArchive filtered by the section_id column
 * @method     ChildPushNotificationArchive findOneByPostId(int $post_id) Return the first ChildPushNotificationArchive filtered by the post_id column
 * @method     ChildPushNotificationArchive findOneBySendDate(string $send_date) Return the first ChildPushNotificationArchive filtered by the send_date column
 * @method     ChildPushNotificationArchive findOneByReceiverFilter(string $receiver_filter) Return the first ChildPushNotificationArchive filtered by the receiver_filter column
 * @method     ChildPushNotificationArchive findOneByDeviceCount(int $device_count) Return the first ChildPushNotificationArchive filtered by the device_count column
 * @method     ChildPushNotificationArchive findOneByReadMessageCount(int $read_message_count) Return the first ChildPushNotificationArchive filtered by the read_message_count column
 * @method     ChildPushNotificationArchive findOneByDeletedAt(string $deleted_at) Return the first ChildPushNotificationArchive filtered by the deleted_at column
 * @method     ChildPushNotificationArchive findOneByCreatedAt(string $created_at) Return the first ChildPushNotificationArchive filtered by the created_at column
 * @method     ChildPushNotificationArchive findOneByUpdatedAt(string $updated_at) Return the first ChildPushNotificationArchive filtered by the updated_at column
 * @method     ChildPushNotificationArchive findOneByArchivedAt(string $archived_at) Return the first ChildPushNotificationArchive filtered by the archived_at column *

 * @method     ChildPushNotificationArchive requirePk($key, ConnectionInterface $con = null) Return the ChildPushNotificationArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOne(ConnectionInterface $con = null) Return the first ChildPushNotificationArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPushNotificationArchive requireOneById(int $id) Return the first ChildPushNotificationArchive filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneByLang(string $lang) Return the first ChildPushNotificationArchive filtered by the lang column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneByTitle(string $title) Return the first ChildPushNotificationArchive filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneByBody(string $body) Return the first ChildPushNotificationArchive filtered by the body column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneBySectionId(int $section_id) Return the first ChildPushNotificationArchive filtered by the section_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneByPostId(int $post_id) Return the first ChildPushNotificationArchive filtered by the post_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneBySendDate(string $send_date) Return the first ChildPushNotificationArchive filtered by the send_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneByReceiverFilter(string $receiver_filter) Return the first ChildPushNotificationArchive filtered by the receiver_filter column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneByDeviceCount(int $device_count) Return the first ChildPushNotificationArchive filtered by the device_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneByReadMessageCount(int $read_message_count) Return the first ChildPushNotificationArchive filtered by the read_message_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneByDeletedAt(string $deleted_at) Return the first ChildPushNotificationArchive filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneByCreatedAt(string $created_at) Return the first ChildPushNotificationArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildPushNotificationArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPushNotificationArchive requireOneByArchivedAt(string $archived_at) Return the first ChildPushNotificationArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPushNotificationArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPushNotificationArchive objects based on current ModelCriteria
 * @method     ChildPushNotificationArchive[]|ObjectCollection findById(int $id) Return ChildPushNotificationArchive objects filtered by the id column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findByLang(string $lang) Return ChildPushNotificationArchive objects filtered by the lang column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findByTitle(string $title) Return ChildPushNotificationArchive objects filtered by the title column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findByBody(string $body) Return ChildPushNotificationArchive objects filtered by the body column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findBySectionId(int $section_id) Return ChildPushNotificationArchive objects filtered by the section_id column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findByPostId(int $post_id) Return ChildPushNotificationArchive objects filtered by the post_id column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findBySendDate(string $send_date) Return ChildPushNotificationArchive objects filtered by the send_date column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findByReceiverFilter(string $receiver_filter) Return ChildPushNotificationArchive objects filtered by the receiver_filter column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findByDeviceCount(int $device_count) Return ChildPushNotificationArchive objects filtered by the device_count column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findByReadMessageCount(int $read_message_count) Return ChildPushNotificationArchive objects filtered by the read_message_count column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildPushNotificationArchive objects filtered by the deleted_at column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPushNotificationArchive objects filtered by the created_at column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPushNotificationArchive objects filtered by the updated_at column
 * @method     ChildPushNotificationArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildPushNotificationArchive objects filtered by the archived_at column
 * @method     ChildPushNotificationArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PushNotificationArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\PushNotificationArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\PushNotificationArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPushNotificationArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPushNotificationArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPushNotificationArchiveQuery) {
            return $criteria;
        }
        $query = new ChildPushNotificationArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPushNotificationArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PushNotificationArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PushNotificationArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPushNotificationArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, lang, title, body, section_id, post_id, send_date, receiver_filter, device_count, read_message_count, deleted_at, created_at, updated_at, archived_at FROM push_notification_archive WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPushNotificationArchive $obj */
            $obj = new ChildPushNotificationArchive();
            $obj->hydrate($row);
            PushNotificationArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPushNotificationArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the lang column
     *
     * Example usage:
     * <code>
     * $query->filterByLang('fooValue');   // WHERE lang = 'fooValue'
     * $query->filterByLang('%fooValue%', Criteria::LIKE); // WHERE lang LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lang The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByLang($lang = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lang)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_LANG, $lang, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the body column
     *
     * Example usage:
     * <code>
     * $query->filterByBody('fooValue');   // WHERE body = 'fooValue'
     * $query->filterByBody('%fooValue%', Criteria::LIKE); // WHERE body LIKE '%fooValue%'
     * </code>
     *
     * @param     string $body The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByBody($body = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($body)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_BODY, $body, $comparison);
    }

    /**
     * Filter the query on the section_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySectionId(1234); // WHERE section_id = 1234
     * $query->filterBySectionId(array(12, 34)); // WHERE section_id IN (12, 34)
     * $query->filterBySectionId(array('min' => 12)); // WHERE section_id > 12
     * </code>
     *
     * @param     mixed $sectionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterBySectionId($sectionId = null, $comparison = null)
    {
        if (is_array($sectionId)) {
            $useMinMax = false;
            if (isset($sectionId['min'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_SECTION_ID, $sectionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sectionId['max'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_SECTION_ID, $sectionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_SECTION_ID, $sectionId, $comparison);
    }

    /**
     * Filter the query on the post_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPostId(1234); // WHERE post_id = 1234
     * $query->filterByPostId(array(12, 34)); // WHERE post_id IN (12, 34)
     * $query->filterByPostId(array('min' => 12)); // WHERE post_id > 12
     * </code>
     *
     * @param     mixed $postId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByPostId($postId = null, $comparison = null)
    {
        if (is_array($postId)) {
            $useMinMax = false;
            if (isset($postId['min'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_POST_ID, $postId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($postId['max'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_POST_ID, $postId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_POST_ID, $postId, $comparison);
    }

    /**
     * Filter the query on the send_date column
     *
     * Example usage:
     * <code>
     * $query->filterBySendDate('2011-03-14'); // WHERE send_date = '2011-03-14'
     * $query->filterBySendDate('now'); // WHERE send_date = '2011-03-14'
     * $query->filterBySendDate(array('max' => 'yesterday')); // WHERE send_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $sendDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterBySendDate($sendDate = null, $comparison = null)
    {
        if (is_array($sendDate)) {
            $useMinMax = false;
            if (isset($sendDate['min'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_SEND_DATE, $sendDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sendDate['max'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_SEND_DATE, $sendDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_SEND_DATE, $sendDate, $comparison);
    }

    /**
     * Filter the query on the receiver_filter column
     *
     * Example usage:
     * <code>
     * $query->filterByReceiverFilter('fooValue');   // WHERE receiver_filter = 'fooValue'
     * $query->filterByReceiverFilter('%fooValue%', Criteria::LIKE); // WHERE receiver_filter LIKE '%fooValue%'
     * </code>
     *
     * @param     string $receiverFilter The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByReceiverFilter($receiverFilter = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($receiverFilter)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_RECEIVER_FILTER, $receiverFilter, $comparison);
    }

    /**
     * Filter the query on the device_count column
     *
     * Example usage:
     * <code>
     * $query->filterByDeviceCount(1234); // WHERE device_count = 1234
     * $query->filterByDeviceCount(array(12, 34)); // WHERE device_count IN (12, 34)
     * $query->filterByDeviceCount(array('min' => 12)); // WHERE device_count > 12
     * </code>
     *
     * @param     mixed $deviceCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByDeviceCount($deviceCount = null, $comparison = null)
    {
        if (is_array($deviceCount)) {
            $useMinMax = false;
            if (isset($deviceCount['min'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_DEVICE_COUNT, $deviceCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deviceCount['max'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_DEVICE_COUNT, $deviceCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_DEVICE_COUNT, $deviceCount, $comparison);
    }

    /**
     * Filter the query on the read_message_count column
     *
     * Example usage:
     * <code>
     * $query->filterByReadMessageCount(1234); // WHERE read_message_count = 1234
     * $query->filterByReadMessageCount(array(12, 34)); // WHERE read_message_count IN (12, 34)
     * $query->filterByReadMessageCount(array('min' => 12)); // WHERE read_message_count > 12
     * </code>
     *
     * @param     mixed $readMessageCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByReadMessageCount($readMessageCount = null, $comparison = null)
    {
        if (is_array($readMessageCount)) {
            $useMinMax = false;
            if (isset($readMessageCount['min'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_READ_MESSAGE_COUNT, $readMessageCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($readMessageCount['max'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_READ_MESSAGE_COUNT, $readMessageCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_READ_MESSAGE_COUNT, $readMessageCount, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(PushNotificationArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PushNotificationArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPushNotificationArchive $pushNotificationArchive Object to remove from the list of results
     *
     * @return $this|ChildPushNotificationArchiveQuery The current query, for fluid interface
     */
    public function prune($pushNotificationArchive = null)
    {
        if ($pushNotificationArchive) {
            $this->addUsingAlias(PushNotificationArchiveTableMap::COL_ID, $pushNotificationArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the push_notification_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PushNotificationArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PushNotificationArchiveTableMap::clearInstancePool();
            PushNotificationArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PushNotificationArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PushNotificationArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PushNotificationArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PushNotificationArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PushNotificationArchiveQuery
