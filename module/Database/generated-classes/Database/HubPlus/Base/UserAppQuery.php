<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\UserApp as ChildUserApp;
use Database\HubPlus\UserAppArchive as ChildUserAppArchive;
use Database\HubPlus\UserAppQuery as ChildUserAppQuery;
use Database\HubPlus\Map\UserAppTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'user_app' table.
 *
 *
 *
 * @method     ChildUserAppQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserAppQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method     ChildUserAppQuery orderByUsernameCanonical($order = Criteria::ASC) Order by the username_canonical column
 * @method     ChildUserAppQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildUserAppQuery orderByEmailCanonical($order = Criteria::ASC) Order by the email_canonical column
 * @method     ChildUserAppQuery orderByEnabled($order = Criteria::ASC) Order by the enabled column
 * @method     ChildUserAppQuery orderByPrivacy($order = Criteria::ASC) Order by the privacy column
 * @method     ChildUserAppQuery orderByTerms($order = Criteria::ASC) Order by the terms column
 * @method     ChildUserAppQuery orderBySalt($order = Criteria::ASC) Order by the salt column
 * @method     ChildUserAppQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildUserAppQuery orderByLastLogin($order = Criteria::ASC) Order by the last_login column
 * @method     ChildUserAppQuery orderByLocked($order = Criteria::ASC) Order by the locked column
 * @method     ChildUserAppQuery orderByExpired($order = Criteria::ASC) Order by the expired column
 * @method     ChildUserAppQuery orderByExpiresAt($order = Criteria::ASC) Order by the expires_at column
 * @method     ChildUserAppQuery orderByConfirmationToken($order = Criteria::ASC) Order by the confirmation_token column
 * @method     ChildUserAppQuery orderByPasswordRequestedAt($order = Criteria::ASC) Order by the password_requested_at column
 * @method     ChildUserAppQuery orderByRoles($order = Criteria::ASC) Order by the roles column
 * @method     ChildUserAppQuery orderByCredentialsExpired($order = Criteria::ASC) Order by the credentials_expired column
 * @method     ChildUserAppQuery orderByCredentialsExpireAt($order = Criteria::ASC) Order by the credentials_expire_at column
 * @method     ChildUserAppQuery orderByFirstname($order = Criteria::ASC) Order by the firstname column
 * @method     ChildUserAppQuery orderByLastname($order = Criteria::ASC) Order by the lastname column
 * @method     ChildUserAppQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildUserAppQuery orderByImagepath($order = Criteria::ASC) Order by the imagePath column
 * @method     ChildUserAppQuery orderByAddressId($order = Criteria::ASC) Order by the address_id column
 * @method     ChildUserAppQuery orderByTags($order = Criteria::ASC) Order by the tags column
 * @method     ChildUserAppQuery orderBySectionId($order = Criteria::ASC) Order by the section_id column
 * @method     ChildUserAppQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildUserAppQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildUserAppQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildUserAppQuery groupById() Group by the id column
 * @method     ChildUserAppQuery groupByUsername() Group by the username column
 * @method     ChildUserAppQuery groupByUsernameCanonical() Group by the username_canonical column
 * @method     ChildUserAppQuery groupByEmail() Group by the email column
 * @method     ChildUserAppQuery groupByEmailCanonical() Group by the email_canonical column
 * @method     ChildUserAppQuery groupByEnabled() Group by the enabled column
 * @method     ChildUserAppQuery groupByPrivacy() Group by the privacy column
 * @method     ChildUserAppQuery groupByTerms() Group by the terms column
 * @method     ChildUserAppQuery groupBySalt() Group by the salt column
 * @method     ChildUserAppQuery groupByPassword() Group by the password column
 * @method     ChildUserAppQuery groupByLastLogin() Group by the last_login column
 * @method     ChildUserAppQuery groupByLocked() Group by the locked column
 * @method     ChildUserAppQuery groupByExpired() Group by the expired column
 * @method     ChildUserAppQuery groupByExpiresAt() Group by the expires_at column
 * @method     ChildUserAppQuery groupByConfirmationToken() Group by the confirmation_token column
 * @method     ChildUserAppQuery groupByPasswordRequestedAt() Group by the password_requested_at column
 * @method     ChildUserAppQuery groupByRoles() Group by the roles column
 * @method     ChildUserAppQuery groupByCredentialsExpired() Group by the credentials_expired column
 * @method     ChildUserAppQuery groupByCredentialsExpireAt() Group by the credentials_expire_at column
 * @method     ChildUserAppQuery groupByFirstname() Group by the firstname column
 * @method     ChildUserAppQuery groupByLastname() Group by the lastname column
 * @method     ChildUserAppQuery groupByPhone() Group by the phone column
 * @method     ChildUserAppQuery groupByImagepath() Group by the imagePath column
 * @method     ChildUserAppQuery groupByAddressId() Group by the address_id column
 * @method     ChildUserAppQuery groupByTags() Group by the tags column
 * @method     ChildUserAppQuery groupBySectionId() Group by the section_id column
 * @method     ChildUserAppQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildUserAppQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildUserAppQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildUserAppQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserAppQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserAppQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserAppQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserAppQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserAppQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserAppQuery leftJoinAddress($relationAlias = null) Adds a LEFT JOIN clause to the query using the Address relation
 * @method     ChildUserAppQuery rightJoinAddress($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Address relation
 * @method     ChildUserAppQuery innerJoinAddress($relationAlias = null) Adds a INNER JOIN clause to the query using the Address relation
 *
 * @method     ChildUserAppQuery joinWithAddress($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Address relation
 *
 * @method     ChildUserAppQuery leftJoinWithAddress() Adds a LEFT JOIN clause and with to the query using the Address relation
 * @method     ChildUserAppQuery rightJoinWithAddress() Adds a RIGHT JOIN clause and with to the query using the Address relation
 * @method     ChildUserAppQuery innerJoinWithAddress() Adds a INNER JOIN clause and with to the query using the Address relation
 *
 * @method     ChildUserAppQuery leftJoinSection($relationAlias = null) Adds a LEFT JOIN clause to the query using the Section relation
 * @method     ChildUserAppQuery rightJoinSection($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Section relation
 * @method     ChildUserAppQuery innerJoinSection($relationAlias = null) Adds a INNER JOIN clause to the query using the Section relation
 *
 * @method     ChildUserAppQuery joinWithSection($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Section relation
 *
 * @method     ChildUserAppQuery leftJoinWithSection() Adds a LEFT JOIN clause and with to the query using the Section relation
 * @method     ChildUserAppQuery rightJoinWithSection() Adds a RIGHT JOIN clause and with to the query using the Section relation
 * @method     ChildUserAppQuery innerJoinWithSection() Adds a INNER JOIN clause and with to the query using the Section relation
 *
 * @method     ChildUserAppQuery leftJoinCustomFormLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomFormLog relation
 * @method     ChildUserAppQuery rightJoinCustomFormLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomFormLog relation
 * @method     ChildUserAppQuery innerJoinCustomFormLog($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomFormLog relation
 *
 * @method     ChildUserAppQuery joinWithCustomFormLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomFormLog relation
 *
 * @method     ChildUserAppQuery leftJoinWithCustomFormLog() Adds a LEFT JOIN clause and with to the query using the CustomFormLog relation
 * @method     ChildUserAppQuery rightJoinWithCustomFormLog() Adds a RIGHT JOIN clause and with to the query using the CustomFormLog relation
 * @method     ChildUserAppQuery innerJoinWithCustomFormLog() Adds a INNER JOIN clause and with to the query using the CustomFormLog relation
 *
 * @method     ChildUserAppQuery leftJoinDevice($relationAlias = null) Adds a LEFT JOIN clause to the query using the Device relation
 * @method     ChildUserAppQuery rightJoinDevice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Device relation
 * @method     ChildUserAppQuery innerJoinDevice($relationAlias = null) Adds a INNER JOIN clause to the query using the Device relation
 *
 * @method     ChildUserAppQuery joinWithDevice($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Device relation
 *
 * @method     ChildUserAppQuery leftJoinWithDevice() Adds a LEFT JOIN clause and with to the query using the Device relation
 * @method     ChildUserAppQuery rightJoinWithDevice() Adds a RIGHT JOIN clause and with to the query using the Device relation
 * @method     ChildUserAppQuery innerJoinWithDevice() Adds a INNER JOIN clause and with to the query using the Device relation
 *
 * @method     ChildUserAppQuery leftJoinMediaLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the MediaLog relation
 * @method     ChildUserAppQuery rightJoinMediaLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MediaLog relation
 * @method     ChildUserAppQuery innerJoinMediaLog($relationAlias = null) Adds a INNER JOIN clause to the query using the MediaLog relation
 *
 * @method     ChildUserAppQuery joinWithMediaLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MediaLog relation
 *
 * @method     ChildUserAppQuery leftJoinWithMediaLog() Adds a LEFT JOIN clause and with to the query using the MediaLog relation
 * @method     ChildUserAppQuery rightJoinWithMediaLog() Adds a RIGHT JOIN clause and with to the query using the MediaLog relation
 * @method     ChildUserAppQuery innerJoinWithMediaLog() Adds a INNER JOIN clause and with to the query using the MediaLog relation
 *
 * @method     ChildUserAppQuery leftJoinPostLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostLog relation
 * @method     ChildUserAppQuery rightJoinPostLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostLog relation
 * @method     ChildUserAppQuery innerJoinPostLog($relationAlias = null) Adds a INNER JOIN clause to the query using the PostLog relation
 *
 * @method     ChildUserAppQuery joinWithPostLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostLog relation
 *
 * @method     ChildUserAppQuery leftJoinWithPostLog() Adds a LEFT JOIN clause and with to the query using the PostLog relation
 * @method     ChildUserAppQuery rightJoinWithPostLog() Adds a RIGHT JOIN clause and with to the query using the PostLog relation
 * @method     ChildUserAppQuery innerJoinWithPostLog() Adds a INNER JOIN clause and with to the query using the PostLog relation
 *
 * @method     ChildUserAppQuery leftJoinRemoteLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the RemoteLog relation
 * @method     ChildUserAppQuery rightJoinRemoteLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RemoteLog relation
 * @method     ChildUserAppQuery innerJoinRemoteLog($relationAlias = null) Adds a INNER JOIN clause to the query using the RemoteLog relation
 *
 * @method     ChildUserAppQuery joinWithRemoteLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RemoteLog relation
 *
 * @method     ChildUserAppQuery leftJoinWithRemoteLog() Adds a LEFT JOIN clause and with to the query using the RemoteLog relation
 * @method     ChildUserAppQuery rightJoinWithRemoteLog() Adds a RIGHT JOIN clause and with to the query using the RemoteLog relation
 * @method     ChildUserAppQuery innerJoinWithRemoteLog() Adds a INNER JOIN clause and with to the query using the RemoteLog relation
 *
 * @method     \Database\HubPlus\AddressQuery|\Database\HubPlus\SectionQuery|\Database\HubPlus\CustomFormLogQuery|\Database\HubPlus\DeviceQuery|\Database\HubPlus\MediaLogQuery|\Database\HubPlus\PostLogQuery|\Database\HubPlus\RemoteLogQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUserApp findOne(ConnectionInterface $con = null) Return the first ChildUserApp matching the query
 * @method     ChildUserApp findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserApp matching the query, or a new ChildUserApp object populated from the query conditions when no match is found
 *
 * @method     ChildUserApp findOneById(int $id) Return the first ChildUserApp filtered by the id column
 * @method     ChildUserApp findOneByUsername(string $username) Return the first ChildUserApp filtered by the username column
 * @method     ChildUserApp findOneByUsernameCanonical(string $username_canonical) Return the first ChildUserApp filtered by the username_canonical column
 * @method     ChildUserApp findOneByEmail(string $email) Return the first ChildUserApp filtered by the email column
 * @method     ChildUserApp findOneByEmailCanonical(string $email_canonical) Return the first ChildUserApp filtered by the email_canonical column
 * @method     ChildUserApp findOneByEnabled(boolean $enabled) Return the first ChildUserApp filtered by the enabled column
 * @method     ChildUserApp findOneByPrivacy(boolean $privacy) Return the first ChildUserApp filtered by the privacy column
 * @method     ChildUserApp findOneByTerms(boolean $terms) Return the first ChildUserApp filtered by the terms column
 * @method     ChildUserApp findOneBySalt(string $salt) Return the first ChildUserApp filtered by the salt column
 * @method     ChildUserApp findOneByPassword(string $password) Return the first ChildUserApp filtered by the password column
 * @method     ChildUserApp findOneByLastLogin(string $last_login) Return the first ChildUserApp filtered by the last_login column
 * @method     ChildUserApp findOneByLocked(boolean $locked) Return the first ChildUserApp filtered by the locked column
 * @method     ChildUserApp findOneByExpired(boolean $expired) Return the first ChildUserApp filtered by the expired column
 * @method     ChildUserApp findOneByExpiresAt(string $expires_at) Return the first ChildUserApp filtered by the expires_at column
 * @method     ChildUserApp findOneByConfirmationToken(string $confirmation_token) Return the first ChildUserApp filtered by the confirmation_token column
 * @method     ChildUserApp findOneByPasswordRequestedAt(string $password_requested_at) Return the first ChildUserApp filtered by the password_requested_at column
 * @method     ChildUserApp findOneByRoles(string $roles) Return the first ChildUserApp filtered by the roles column
 * @method     ChildUserApp findOneByCredentialsExpired(boolean $credentials_expired) Return the first ChildUserApp filtered by the credentials_expired column
 * @method     ChildUserApp findOneByCredentialsExpireAt(string $credentials_expire_at) Return the first ChildUserApp filtered by the credentials_expire_at column
 * @method     ChildUserApp findOneByFirstname(string $firstname) Return the first ChildUserApp filtered by the firstname column
 * @method     ChildUserApp findOneByLastname(string $lastname) Return the first ChildUserApp filtered by the lastname column
 * @method     ChildUserApp findOneByPhone(string $phone) Return the first ChildUserApp filtered by the phone column
 * @method     ChildUserApp findOneByImagepath(string $imagePath) Return the first ChildUserApp filtered by the imagePath column
 * @method     ChildUserApp findOneByAddressId(int $address_id) Return the first ChildUserApp filtered by the address_id column
 * @method     ChildUserApp findOneByTags(string $tags) Return the first ChildUserApp filtered by the tags column
 * @method     ChildUserApp findOneBySectionId(int $section_id) Return the first ChildUserApp filtered by the section_id column
 * @method     ChildUserApp findOneByDeletedAt(string $deleted_at) Return the first ChildUserApp filtered by the deleted_at column
 * @method     ChildUserApp findOneByCreatedAt(string $created_at) Return the first ChildUserApp filtered by the created_at column
 * @method     ChildUserApp findOneByUpdatedAt(string $updated_at) Return the first ChildUserApp filtered by the updated_at column *

 * @method     ChildUserApp requirePk($key, ConnectionInterface $con = null) Return the ChildUserApp by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOne(ConnectionInterface $con = null) Return the first ChildUserApp matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserApp requireOneById(int $id) Return the first ChildUserApp filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByUsername(string $username) Return the first ChildUserApp filtered by the username column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByUsernameCanonical(string $username_canonical) Return the first ChildUserApp filtered by the username_canonical column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByEmail(string $email) Return the first ChildUserApp filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByEmailCanonical(string $email_canonical) Return the first ChildUserApp filtered by the email_canonical column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByEnabled(boolean $enabled) Return the first ChildUserApp filtered by the enabled column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByPrivacy(boolean $privacy) Return the first ChildUserApp filtered by the privacy column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByTerms(boolean $terms) Return the first ChildUserApp filtered by the terms column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneBySalt(string $salt) Return the first ChildUserApp filtered by the salt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByPassword(string $password) Return the first ChildUserApp filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByLastLogin(string $last_login) Return the first ChildUserApp filtered by the last_login column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByLocked(boolean $locked) Return the first ChildUserApp filtered by the locked column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByExpired(boolean $expired) Return the first ChildUserApp filtered by the expired column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByExpiresAt(string $expires_at) Return the first ChildUserApp filtered by the expires_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByConfirmationToken(string $confirmation_token) Return the first ChildUserApp filtered by the confirmation_token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByPasswordRequestedAt(string $password_requested_at) Return the first ChildUserApp filtered by the password_requested_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByRoles(string $roles) Return the first ChildUserApp filtered by the roles column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByCredentialsExpired(boolean $credentials_expired) Return the first ChildUserApp filtered by the credentials_expired column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByCredentialsExpireAt(string $credentials_expire_at) Return the first ChildUserApp filtered by the credentials_expire_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByFirstname(string $firstname) Return the first ChildUserApp filtered by the firstname column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByLastname(string $lastname) Return the first ChildUserApp filtered by the lastname column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByPhone(string $phone) Return the first ChildUserApp filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByImagepath(string $imagePath) Return the first ChildUserApp filtered by the imagePath column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByAddressId(int $address_id) Return the first ChildUserApp filtered by the address_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByTags(string $tags) Return the first ChildUserApp filtered by the tags column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneBySectionId(int $section_id) Return the first ChildUserApp filtered by the section_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByDeletedAt(string $deleted_at) Return the first ChildUserApp filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByCreatedAt(string $created_at) Return the first ChildUserApp filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApp requireOneByUpdatedAt(string $updated_at) Return the first ChildUserApp filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserApp[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserApp objects based on current ModelCriteria
 * @method     ChildUserApp[]|ObjectCollection findById(int $id) Return ChildUserApp objects filtered by the id column
 * @method     ChildUserApp[]|ObjectCollection findByUsername(string $username) Return ChildUserApp objects filtered by the username column
 * @method     ChildUserApp[]|ObjectCollection findByUsernameCanonical(string $username_canonical) Return ChildUserApp objects filtered by the username_canonical column
 * @method     ChildUserApp[]|ObjectCollection findByEmail(string $email) Return ChildUserApp objects filtered by the email column
 * @method     ChildUserApp[]|ObjectCollection findByEmailCanonical(string $email_canonical) Return ChildUserApp objects filtered by the email_canonical column
 * @method     ChildUserApp[]|ObjectCollection findByEnabled(boolean $enabled) Return ChildUserApp objects filtered by the enabled column
 * @method     ChildUserApp[]|ObjectCollection findByPrivacy(boolean $privacy) Return ChildUserApp objects filtered by the privacy column
 * @method     ChildUserApp[]|ObjectCollection findByTerms(boolean $terms) Return ChildUserApp objects filtered by the terms column
 * @method     ChildUserApp[]|ObjectCollection findBySalt(string $salt) Return ChildUserApp objects filtered by the salt column
 * @method     ChildUserApp[]|ObjectCollection findByPassword(string $password) Return ChildUserApp objects filtered by the password column
 * @method     ChildUserApp[]|ObjectCollection findByLastLogin(string $last_login) Return ChildUserApp objects filtered by the last_login column
 * @method     ChildUserApp[]|ObjectCollection findByLocked(boolean $locked) Return ChildUserApp objects filtered by the locked column
 * @method     ChildUserApp[]|ObjectCollection findByExpired(boolean $expired) Return ChildUserApp objects filtered by the expired column
 * @method     ChildUserApp[]|ObjectCollection findByExpiresAt(string $expires_at) Return ChildUserApp objects filtered by the expires_at column
 * @method     ChildUserApp[]|ObjectCollection findByConfirmationToken(string $confirmation_token) Return ChildUserApp objects filtered by the confirmation_token column
 * @method     ChildUserApp[]|ObjectCollection findByPasswordRequestedAt(string $password_requested_at) Return ChildUserApp objects filtered by the password_requested_at column
 * @method     ChildUserApp[]|ObjectCollection findByRoles(string $roles) Return ChildUserApp objects filtered by the roles column
 * @method     ChildUserApp[]|ObjectCollection findByCredentialsExpired(boolean $credentials_expired) Return ChildUserApp objects filtered by the credentials_expired column
 * @method     ChildUserApp[]|ObjectCollection findByCredentialsExpireAt(string $credentials_expire_at) Return ChildUserApp objects filtered by the credentials_expire_at column
 * @method     ChildUserApp[]|ObjectCollection findByFirstname(string $firstname) Return ChildUserApp objects filtered by the firstname column
 * @method     ChildUserApp[]|ObjectCollection findByLastname(string $lastname) Return ChildUserApp objects filtered by the lastname column
 * @method     ChildUserApp[]|ObjectCollection findByPhone(string $phone) Return ChildUserApp objects filtered by the phone column
 * @method     ChildUserApp[]|ObjectCollection findByImagepath(string $imagePath) Return ChildUserApp objects filtered by the imagePath column
 * @method     ChildUserApp[]|ObjectCollection findByAddressId(int $address_id) Return ChildUserApp objects filtered by the address_id column
 * @method     ChildUserApp[]|ObjectCollection findByTags(string $tags) Return ChildUserApp objects filtered by the tags column
 * @method     ChildUserApp[]|ObjectCollection findBySectionId(int $section_id) Return ChildUserApp objects filtered by the section_id column
 * @method     ChildUserApp[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildUserApp objects filtered by the deleted_at column
 * @method     ChildUserApp[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildUserApp objects filtered by the created_at column
 * @method     ChildUserApp[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildUserApp objects filtered by the updated_at column
 * @method     ChildUserApp[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserAppQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\UserAppQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\UserApp', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserAppQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserAppQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserAppQuery) {
            return $criteria;
        }
        $query = new ChildUserAppQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserApp|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserAppTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserAppTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserApp A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, username, username_canonical, email, email_canonical, enabled, privacy, terms, salt, password, last_login, locked, expired, expires_at, confirmation_token, password_requested_at, roles, credentials_expired, credentials_expire_at, firstname, lastname, phone, imagePath, address_id, tags, section_id, deleted_at, created_at, updated_at FROM user_app WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserApp $obj */
            $obj = new ChildUserApp();
            $obj->hydrate($row);
            UserAppTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserApp|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserAppTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserAppTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserAppTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserAppTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%', Criteria::LIKE); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the username_canonical column
     *
     * Example usage:
     * <code>
     * $query->filterByUsernameCanonical('fooValue');   // WHERE username_canonical = 'fooValue'
     * $query->filterByUsernameCanonical('%fooValue%', Criteria::LIKE); // WHERE username_canonical LIKE '%fooValue%'
     * </code>
     *
     * @param     string $usernameCanonical The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByUsernameCanonical($usernameCanonical = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($usernameCanonical)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_USERNAME_CANONICAL, $usernameCanonical, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the email_canonical column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailCanonical('fooValue');   // WHERE email_canonical = 'fooValue'
     * $query->filterByEmailCanonical('%fooValue%', Criteria::LIKE); // WHERE email_canonical LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailCanonical The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByEmailCanonical($emailCanonical = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailCanonical)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_EMAIL_CANONICAL, $emailCanonical, $comparison);
    }

    /**
     * Filter the query on the enabled column
     *
     * Example usage:
     * <code>
     * $query->filterByEnabled(true); // WHERE enabled = true
     * $query->filterByEnabled('yes'); // WHERE enabled = true
     * </code>
     *
     * @param     boolean|string $enabled The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByEnabled($enabled = null, $comparison = null)
    {
        if (is_string($enabled)) {
            $enabled = in_array(strtolower($enabled), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserAppTableMap::COL_ENABLED, $enabled, $comparison);
    }

    /**
     * Filter the query on the privacy column
     *
     * Example usage:
     * <code>
     * $query->filterByPrivacy(true); // WHERE privacy = true
     * $query->filterByPrivacy('yes'); // WHERE privacy = true
     * </code>
     *
     * @param     boolean|string $privacy The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByPrivacy($privacy = null, $comparison = null)
    {
        if (is_string($privacy)) {
            $privacy = in_array(strtolower($privacy), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserAppTableMap::COL_PRIVACY, $privacy, $comparison);
    }

    /**
     * Filter the query on the terms column
     *
     * Example usage:
     * <code>
     * $query->filterByTerms(true); // WHERE terms = true
     * $query->filterByTerms('yes'); // WHERE terms = true
     * </code>
     *
     * @param     boolean|string $terms The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByTerms($terms = null, $comparison = null)
    {
        if (is_string($terms)) {
            $terms = in_array(strtolower($terms), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserAppTableMap::COL_TERMS, $terms, $comparison);
    }

    /**
     * Filter the query on the salt column
     *
     * Example usage:
     * <code>
     * $query->filterBySalt('fooValue');   // WHERE salt = 'fooValue'
     * $query->filterBySalt('%fooValue%', Criteria::LIKE); // WHERE salt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterBySalt($salt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_SALT, $salt, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the last_login column
     *
     * Example usage:
     * <code>
     * $query->filterByLastLogin('2011-03-14'); // WHERE last_login = '2011-03-14'
     * $query->filterByLastLogin('now'); // WHERE last_login = '2011-03-14'
     * $query->filterByLastLogin(array('max' => 'yesterday')); // WHERE last_login > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastLogin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByLastLogin($lastLogin = null, $comparison = null)
    {
        if (is_array($lastLogin)) {
            $useMinMax = false;
            if (isset($lastLogin['min'])) {
                $this->addUsingAlias(UserAppTableMap::COL_LAST_LOGIN, $lastLogin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastLogin['max'])) {
                $this->addUsingAlias(UserAppTableMap::COL_LAST_LOGIN, $lastLogin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_LAST_LOGIN, $lastLogin, $comparison);
    }

    /**
     * Filter the query on the locked column
     *
     * Example usage:
     * <code>
     * $query->filterByLocked(true); // WHERE locked = true
     * $query->filterByLocked('yes'); // WHERE locked = true
     * </code>
     *
     * @param     boolean|string $locked The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByLocked($locked = null, $comparison = null)
    {
        if (is_string($locked)) {
            $locked = in_array(strtolower($locked), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserAppTableMap::COL_LOCKED, $locked, $comparison);
    }

    /**
     * Filter the query on the expired column
     *
     * Example usage:
     * <code>
     * $query->filterByExpired(true); // WHERE expired = true
     * $query->filterByExpired('yes'); // WHERE expired = true
     * </code>
     *
     * @param     boolean|string $expired The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByExpired($expired = null, $comparison = null)
    {
        if (is_string($expired)) {
            $expired = in_array(strtolower($expired), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserAppTableMap::COL_EXPIRED, $expired, $comparison);
    }

    /**
     * Filter the query on the expires_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiresAt('2011-03-14'); // WHERE expires_at = '2011-03-14'
     * $query->filterByExpiresAt('now'); // WHERE expires_at = '2011-03-14'
     * $query->filterByExpiresAt(array('max' => 'yesterday')); // WHERE expires_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiresAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByExpiresAt($expiresAt = null, $comparison = null)
    {
        if (is_array($expiresAt)) {
            $useMinMax = false;
            if (isset($expiresAt['min'])) {
                $this->addUsingAlias(UserAppTableMap::COL_EXPIRES_AT, $expiresAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiresAt['max'])) {
                $this->addUsingAlias(UserAppTableMap::COL_EXPIRES_AT, $expiresAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_EXPIRES_AT, $expiresAt, $comparison);
    }

    /**
     * Filter the query on the confirmation_token column
     *
     * Example usage:
     * <code>
     * $query->filterByConfirmationToken('fooValue');   // WHERE confirmation_token = 'fooValue'
     * $query->filterByConfirmationToken('%fooValue%', Criteria::LIKE); // WHERE confirmation_token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $confirmationToken The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByConfirmationToken($confirmationToken = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($confirmationToken)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_CONFIRMATION_TOKEN, $confirmationToken, $comparison);
    }

    /**
     * Filter the query on the password_requested_at column
     *
     * Example usage:
     * <code>
     * $query->filterByPasswordRequestedAt('2011-03-14'); // WHERE password_requested_at = '2011-03-14'
     * $query->filterByPasswordRequestedAt('now'); // WHERE password_requested_at = '2011-03-14'
     * $query->filterByPasswordRequestedAt(array('max' => 'yesterday')); // WHERE password_requested_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $passwordRequestedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByPasswordRequestedAt($passwordRequestedAt = null, $comparison = null)
    {
        if (is_array($passwordRequestedAt)) {
            $useMinMax = false;
            if (isset($passwordRequestedAt['min'])) {
                $this->addUsingAlias(UserAppTableMap::COL_PASSWORD_REQUESTED_AT, $passwordRequestedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($passwordRequestedAt['max'])) {
                $this->addUsingAlias(UserAppTableMap::COL_PASSWORD_REQUESTED_AT, $passwordRequestedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_PASSWORD_REQUESTED_AT, $passwordRequestedAt, $comparison);
    }

    /**
     * Filter the query on the roles column
     *
     * Example usage:
     * <code>
     * $query->filterByRoles('fooValue');   // WHERE roles = 'fooValue'
     * $query->filterByRoles('%fooValue%', Criteria::LIKE); // WHERE roles LIKE '%fooValue%'
     * </code>
     *
     * @param     string $roles The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByRoles($roles = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($roles)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_ROLES, $roles, $comparison);
    }

    /**
     * Filter the query on the credentials_expired column
     *
     * Example usage:
     * <code>
     * $query->filterByCredentialsExpired(true); // WHERE credentials_expired = true
     * $query->filterByCredentialsExpired('yes'); // WHERE credentials_expired = true
     * </code>
     *
     * @param     boolean|string $credentialsExpired The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByCredentialsExpired($credentialsExpired = null, $comparison = null)
    {
        if (is_string($credentialsExpired)) {
            $credentialsExpired = in_array(strtolower($credentialsExpired), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserAppTableMap::COL_CREDENTIALS_EXPIRED, $credentialsExpired, $comparison);
    }

    /**
     * Filter the query on the credentials_expire_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCredentialsExpireAt('2011-03-14'); // WHERE credentials_expire_at = '2011-03-14'
     * $query->filterByCredentialsExpireAt('now'); // WHERE credentials_expire_at = '2011-03-14'
     * $query->filterByCredentialsExpireAt(array('max' => 'yesterday')); // WHERE credentials_expire_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $credentialsExpireAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByCredentialsExpireAt($credentialsExpireAt = null, $comparison = null)
    {
        if (is_array($credentialsExpireAt)) {
            $useMinMax = false;
            if (isset($credentialsExpireAt['min'])) {
                $this->addUsingAlias(UserAppTableMap::COL_CREDENTIALS_EXPIRE_AT, $credentialsExpireAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($credentialsExpireAt['max'])) {
                $this->addUsingAlias(UserAppTableMap::COL_CREDENTIALS_EXPIRE_AT, $credentialsExpireAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_CREDENTIALS_EXPIRE_AT, $credentialsExpireAt, $comparison);
    }

    /**
     * Filter the query on the firstname column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstname('fooValue');   // WHERE firstname = 'fooValue'
     * $query->filterByFirstname('%fooValue%', Criteria::LIKE); // WHERE firstname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByFirstname($firstname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_FIRSTNAME, $firstname, $comparison);
    }

    /**
     * Filter the query on the lastname column
     *
     * Example usage:
     * <code>
     * $query->filterByLastname('fooValue');   // WHERE lastname = 'fooValue'
     * $query->filterByLastname('%fooValue%', Criteria::LIKE); // WHERE lastname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByLastname($lastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_LASTNAME, $lastname, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%', Criteria::LIKE); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the imagePath column
     *
     * Example usage:
     * <code>
     * $query->filterByImagepath('fooValue');   // WHERE imagePath = 'fooValue'
     * $query->filterByImagepath('%fooValue%', Criteria::LIKE); // WHERE imagePath LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imagepath The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByImagepath($imagepath = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imagepath)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_IMAGEPATH, $imagepath, $comparison);
    }

    /**
     * Filter the query on the address_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAddressId(1234); // WHERE address_id = 1234
     * $query->filterByAddressId(array(12, 34)); // WHERE address_id IN (12, 34)
     * $query->filterByAddressId(array('min' => 12)); // WHERE address_id > 12
     * </code>
     *
     * @see       filterByAddress()
     *
     * @param     mixed $addressId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByAddressId($addressId = null, $comparison = null)
    {
        if (is_array($addressId)) {
            $useMinMax = false;
            if (isset($addressId['min'])) {
                $this->addUsingAlias(UserAppTableMap::COL_ADDRESS_ID, $addressId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($addressId['max'])) {
                $this->addUsingAlias(UserAppTableMap::COL_ADDRESS_ID, $addressId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_ADDRESS_ID, $addressId, $comparison);
    }

    /**
     * Filter the query on the tags column
     *
     * Example usage:
     * <code>
     * $query->filterByTags('fooValue');   // WHERE tags = 'fooValue'
     * $query->filterByTags('%fooValue%', Criteria::LIKE); // WHERE tags LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tags The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByTags($tags = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tags)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_TAGS, $tags, $comparison);
    }

    /**
     * Filter the query on the section_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySectionId(1234); // WHERE section_id = 1234
     * $query->filterBySectionId(array(12, 34)); // WHERE section_id IN (12, 34)
     * $query->filterBySectionId(array('min' => 12)); // WHERE section_id > 12
     * </code>
     *
     * @see       filterBySection()
     *
     * @param     mixed $sectionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterBySectionId($sectionId = null, $comparison = null)
    {
        if (is_array($sectionId)) {
            $useMinMax = false;
            if (isset($sectionId['min'])) {
                $this->addUsingAlias(UserAppTableMap::COL_SECTION_ID, $sectionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sectionId['max'])) {
                $this->addUsingAlias(UserAppTableMap::COL_SECTION_ID, $sectionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_SECTION_ID, $sectionId, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(UserAppTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(UserAppTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(UserAppTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(UserAppTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(UserAppTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(UserAppTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserAppTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Address object
     *
     * @param \Database\HubPlus\Address|ObjectCollection $address The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByAddress($address, $comparison = null)
    {
        if ($address instanceof \Database\HubPlus\Address) {
            return $this
                ->addUsingAlias(UserAppTableMap::COL_ADDRESS_ID, $address->getId(), $comparison);
        } elseif ($address instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserAppTableMap::COL_ADDRESS_ID, $address->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAddress() only accepts arguments of type \Database\HubPlus\Address or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Address relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function joinAddress($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Address');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Address');
        }

        return $this;
    }

    /**
     * Use the Address relation Address object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\AddressQuery A secondary query class using the current class as primary query
     */
    public function useAddressQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAddress($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Address', '\Database\HubPlus\AddressQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Section object
     *
     * @param \Database\HubPlus\Section|ObjectCollection $section The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserAppQuery The current query, for fluid interface
     */
    public function filterBySection($section, $comparison = null)
    {
        if ($section instanceof \Database\HubPlus\Section) {
            return $this
                ->addUsingAlias(UserAppTableMap::COL_SECTION_ID, $section->getId(), $comparison);
        } elseif ($section instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserAppTableMap::COL_SECTION_ID, $section->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySection() only accepts arguments of type \Database\HubPlus\Section or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Section relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function joinSection($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Section');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Section');
        }

        return $this;
    }

    /**
     * Use the Section relation Section object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionQuery A secondary query class using the current class as primary query
     */
    public function useSectionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSection($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Section', '\Database\HubPlus\SectionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\CustomFormLog object
     *
     * @param \Database\HubPlus\CustomFormLog|ObjectCollection $customFormLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByCustomFormLog($customFormLog, $comparison = null)
    {
        if ($customFormLog instanceof \Database\HubPlus\CustomFormLog) {
            return $this
                ->addUsingAlias(UserAppTableMap::COL_ID, $customFormLog->getUserId(), $comparison);
        } elseif ($customFormLog instanceof ObjectCollection) {
            return $this
                ->useCustomFormLogQuery()
                ->filterByPrimaryKeys($customFormLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomFormLog() only accepts arguments of type \Database\HubPlus\CustomFormLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomFormLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function joinCustomFormLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomFormLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomFormLog');
        }

        return $this;
    }

    /**
     * Use the CustomFormLog relation CustomFormLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\CustomFormLogQuery A secondary query class using the current class as primary query
     */
    public function useCustomFormLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCustomFormLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomFormLog', '\Database\HubPlus\CustomFormLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Device object
     *
     * @param \Database\HubPlus\Device|ObjectCollection $device the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByDevice($device, $comparison = null)
    {
        if ($device instanceof \Database\HubPlus\Device) {
            return $this
                ->addUsingAlias(UserAppTableMap::COL_ID, $device->getUserId(), $comparison);
        } elseif ($device instanceof ObjectCollection) {
            return $this
                ->useDeviceQuery()
                ->filterByPrimaryKeys($device->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDevice() only accepts arguments of type \Database\HubPlus\Device or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Device relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function joinDevice($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Device');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Device');
        }

        return $this;
    }

    /**
     * Use the Device relation Device object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\DeviceQuery A secondary query class using the current class as primary query
     */
    public function useDeviceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinDevice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Device', '\Database\HubPlus\DeviceQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\MediaLog object
     *
     * @param \Database\HubPlus\MediaLog|ObjectCollection $mediaLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByMediaLog($mediaLog, $comparison = null)
    {
        if ($mediaLog instanceof \Database\HubPlus\MediaLog) {
            return $this
                ->addUsingAlias(UserAppTableMap::COL_ID, $mediaLog->getUserId(), $comparison);
        } elseif ($mediaLog instanceof ObjectCollection) {
            return $this
                ->useMediaLogQuery()
                ->filterByPrimaryKeys($mediaLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMediaLog() only accepts arguments of type \Database\HubPlus\MediaLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MediaLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function joinMediaLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MediaLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MediaLog');
        }

        return $this;
    }

    /**
     * Use the MediaLog relation MediaLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\MediaLogQuery A secondary query class using the current class as primary query
     */
    public function useMediaLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMediaLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MediaLog', '\Database\HubPlus\MediaLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostLog object
     *
     * @param \Database\HubPlus\PostLog|ObjectCollection $postLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByPostLog($postLog, $comparison = null)
    {
        if ($postLog instanceof \Database\HubPlus\PostLog) {
            return $this
                ->addUsingAlias(UserAppTableMap::COL_ID, $postLog->getUserId(), $comparison);
        } elseif ($postLog instanceof ObjectCollection) {
            return $this
                ->usePostLogQuery()
                ->filterByPrimaryKeys($postLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostLog() only accepts arguments of type \Database\HubPlus\PostLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function joinPostLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostLog');
        }

        return $this;
    }

    /**
     * Use the PostLog relation PostLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostLogQuery A secondary query class using the current class as primary query
     */
    public function usePostLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPostLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostLog', '\Database\HubPlus\PostLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\RemoteLog object
     *
     * @param \Database\HubPlus\RemoteLog|ObjectCollection $remoteLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserAppQuery The current query, for fluid interface
     */
    public function filterByRemoteLog($remoteLog, $comparison = null)
    {
        if ($remoteLog instanceof \Database\HubPlus\RemoteLog) {
            return $this
                ->addUsingAlias(UserAppTableMap::COL_ID, $remoteLog->getUserId(), $comparison);
        } elseif ($remoteLog instanceof ObjectCollection) {
            return $this
                ->useRemoteLogQuery()
                ->filterByPrimaryKeys($remoteLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRemoteLog() only accepts arguments of type \Database\HubPlus\RemoteLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RemoteLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function joinRemoteLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RemoteLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RemoteLog');
        }

        return $this;
    }

    /**
     * Use the RemoteLog relation RemoteLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\RemoteLogQuery A secondary query class using the current class as primary query
     */
    public function useRemoteLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRemoteLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RemoteLog', '\Database\HubPlus\RemoteLogQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserApp $userApp Object to remove from the list of results
     *
     * @return $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function prune($userApp = null)
    {
        if ($userApp) {
            $this->addUsingAlias(UserAppTableMap::COL_ID, $userApp->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the user_app table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAppTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserAppTableMap::clearInstancePool();
            UserAppTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAppTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserAppTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserAppTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserAppTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(UserAppTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(UserAppTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(UserAppTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(UserAppTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(UserAppTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildUserAppQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(UserAppTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildUserAppArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAppTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // UserAppQuery
