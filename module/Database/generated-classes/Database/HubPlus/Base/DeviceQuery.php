<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\Device as ChildDevice;
use Database\HubPlus\DeviceArchive as ChildDeviceArchive;
use Database\HubPlus\DeviceQuery as ChildDeviceQuery;
use Database\HubPlus\Map\DeviceTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'device' table.
 *
 *
 *
 * @method     ChildDeviceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildDeviceQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildDeviceQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildDeviceQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildDeviceQuery orderByOs($order = Criteria::ASC) Order by the os column
 * @method     ChildDeviceQuery orderByOsVersion($order = Criteria::ASC) Order by the os_version column
 * @method     ChildDeviceQuery orderByEnableNotification($order = Criteria::ASC) Order by the enable_notification column
 * @method     ChildDeviceQuery orderByApiVersion($order = Criteria::ASC) Order by the api_version column
 * @method     ChildDeviceQuery orderByLanguage($order = Criteria::ASC) Order by the language column
 * @method     ChildDeviceQuery orderByDemo($order = Criteria::ASC) Order by the demo column
 * @method     ChildDeviceQuery orderByToken($order = Criteria::ASC) Order by the token column
 * @method     ChildDeviceQuery orderByApiKey($order = Criteria::ASC) Order by the api_key column
 * @method     ChildDeviceQuery orderByIpAddress($order = Criteria::ASC) Order by the ip_address column
 * @method     ChildDeviceQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildDeviceQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildDeviceQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildDeviceQuery groupById() Group by the id column
 * @method     ChildDeviceQuery groupByUserId() Group by the user_id column
 * @method     ChildDeviceQuery groupByStatus() Group by the status column
 * @method     ChildDeviceQuery groupByType() Group by the type column
 * @method     ChildDeviceQuery groupByOs() Group by the os column
 * @method     ChildDeviceQuery groupByOsVersion() Group by the os_version column
 * @method     ChildDeviceQuery groupByEnableNotification() Group by the enable_notification column
 * @method     ChildDeviceQuery groupByApiVersion() Group by the api_version column
 * @method     ChildDeviceQuery groupByLanguage() Group by the language column
 * @method     ChildDeviceQuery groupByDemo() Group by the demo column
 * @method     ChildDeviceQuery groupByToken() Group by the token column
 * @method     ChildDeviceQuery groupByApiKey() Group by the api_key column
 * @method     ChildDeviceQuery groupByIpAddress() Group by the ip_address column
 * @method     ChildDeviceQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildDeviceQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildDeviceQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildDeviceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildDeviceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildDeviceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildDeviceQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildDeviceQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildDeviceQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildDeviceQuery leftJoinUserApp($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserApp relation
 * @method     ChildDeviceQuery rightJoinUserApp($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserApp relation
 * @method     ChildDeviceQuery innerJoinUserApp($relationAlias = null) Adds a INNER JOIN clause to the query using the UserApp relation
 *
 * @method     ChildDeviceQuery joinWithUserApp($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserApp relation
 *
 * @method     ChildDeviceQuery leftJoinWithUserApp() Adds a LEFT JOIN clause and with to the query using the UserApp relation
 * @method     ChildDeviceQuery rightJoinWithUserApp() Adds a RIGHT JOIN clause and with to the query using the UserApp relation
 * @method     ChildDeviceQuery innerJoinWithUserApp() Adds a INNER JOIN clause and with to the query using the UserApp relation
 *
 * @method     ChildDeviceQuery leftJoinCustomFormExtraLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomFormExtraLog relation
 * @method     ChildDeviceQuery rightJoinCustomFormExtraLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomFormExtraLog relation
 * @method     ChildDeviceQuery innerJoinCustomFormExtraLog($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomFormExtraLog relation
 *
 * @method     ChildDeviceQuery joinWithCustomFormExtraLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomFormExtraLog relation
 *
 * @method     ChildDeviceQuery leftJoinWithCustomFormExtraLog() Adds a LEFT JOIN clause and with to the query using the CustomFormExtraLog relation
 * @method     ChildDeviceQuery rightJoinWithCustomFormExtraLog() Adds a RIGHT JOIN clause and with to the query using the CustomFormExtraLog relation
 * @method     ChildDeviceQuery innerJoinWithCustomFormExtraLog() Adds a INNER JOIN clause and with to the query using the CustomFormExtraLog relation
 *
 * @method     ChildDeviceQuery leftJoinMedia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Media relation
 * @method     ChildDeviceQuery rightJoinMedia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Media relation
 * @method     ChildDeviceQuery innerJoinMedia($relationAlias = null) Adds a INNER JOIN clause to the query using the Media relation
 *
 * @method     ChildDeviceQuery joinWithMedia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Media relation
 *
 * @method     ChildDeviceQuery leftJoinWithMedia() Adds a LEFT JOIN clause and with to the query using the Media relation
 * @method     ChildDeviceQuery rightJoinWithMedia() Adds a RIGHT JOIN clause and with to the query using the Media relation
 * @method     ChildDeviceQuery innerJoinWithMedia() Adds a INNER JOIN clause and with to the query using the Media relation
 *
 * @method     ChildDeviceQuery leftJoinMediaExtraLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the MediaExtraLog relation
 * @method     ChildDeviceQuery rightJoinMediaExtraLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MediaExtraLog relation
 * @method     ChildDeviceQuery innerJoinMediaExtraLog($relationAlias = null) Adds a INNER JOIN clause to the query using the MediaExtraLog relation
 *
 * @method     ChildDeviceQuery joinWithMediaExtraLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MediaExtraLog relation
 *
 * @method     ChildDeviceQuery leftJoinWithMediaExtraLog() Adds a LEFT JOIN clause and with to the query using the MediaExtraLog relation
 * @method     ChildDeviceQuery rightJoinWithMediaExtraLog() Adds a RIGHT JOIN clause and with to the query using the MediaExtraLog relation
 * @method     ChildDeviceQuery innerJoinWithMediaExtraLog() Adds a INNER JOIN clause and with to the query using the MediaExtraLog relation
 *
 * @method     ChildDeviceQuery leftJoinPostExtraLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostExtraLog relation
 * @method     ChildDeviceQuery rightJoinPostExtraLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostExtraLog relation
 * @method     ChildDeviceQuery innerJoinPostExtraLog($relationAlias = null) Adds a INNER JOIN clause to the query using the PostExtraLog relation
 *
 * @method     ChildDeviceQuery joinWithPostExtraLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostExtraLog relation
 *
 * @method     ChildDeviceQuery leftJoinWithPostExtraLog() Adds a LEFT JOIN clause and with to the query using the PostExtraLog relation
 * @method     ChildDeviceQuery rightJoinWithPostExtraLog() Adds a RIGHT JOIN clause and with to the query using the PostExtraLog relation
 * @method     ChildDeviceQuery innerJoinWithPostExtraLog() Adds a INNER JOIN clause and with to the query using the PostExtraLog relation
 *
 * @method     ChildDeviceQuery leftJoinPushNotificationDevice($relationAlias = null) Adds a LEFT JOIN clause to the query using the PushNotificationDevice relation
 * @method     ChildDeviceQuery rightJoinPushNotificationDevice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PushNotificationDevice relation
 * @method     ChildDeviceQuery innerJoinPushNotificationDevice($relationAlias = null) Adds a INNER JOIN clause to the query using the PushNotificationDevice relation
 *
 * @method     ChildDeviceQuery joinWithPushNotificationDevice($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PushNotificationDevice relation
 *
 * @method     ChildDeviceQuery leftJoinWithPushNotificationDevice() Adds a LEFT JOIN clause and with to the query using the PushNotificationDevice relation
 * @method     ChildDeviceQuery rightJoinWithPushNotificationDevice() Adds a RIGHT JOIN clause and with to the query using the PushNotificationDevice relation
 * @method     ChildDeviceQuery innerJoinWithPushNotificationDevice() Adds a INNER JOIN clause and with to the query using the PushNotificationDevice relation
 *
 * @method     ChildDeviceQuery leftJoinRemoteExtraLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the RemoteExtraLog relation
 * @method     ChildDeviceQuery rightJoinRemoteExtraLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RemoteExtraLog relation
 * @method     ChildDeviceQuery innerJoinRemoteExtraLog($relationAlias = null) Adds a INNER JOIN clause to the query using the RemoteExtraLog relation
 *
 * @method     ChildDeviceQuery joinWithRemoteExtraLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RemoteExtraLog relation
 *
 * @method     ChildDeviceQuery leftJoinWithRemoteExtraLog() Adds a LEFT JOIN clause and with to the query using the RemoteExtraLog relation
 * @method     ChildDeviceQuery rightJoinWithRemoteExtraLog() Adds a RIGHT JOIN clause and with to the query using the RemoteExtraLog relation
 * @method     ChildDeviceQuery innerJoinWithRemoteExtraLog() Adds a INNER JOIN clause and with to the query using the RemoteExtraLog relation
 *
 * @method     \Database\HubPlus\UserAppQuery|\Database\HubPlus\CustomFormExtraLogQuery|\Database\HubPlus\MediaQuery|\Database\HubPlus\MediaExtraLogQuery|\Database\HubPlus\PostExtraLogQuery|\Database\HubPlus\PushNotificationDeviceQuery|\Database\HubPlus\RemoteExtraLogQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildDevice findOne(ConnectionInterface $con = null) Return the first ChildDevice matching the query
 * @method     ChildDevice findOneOrCreate(ConnectionInterface $con = null) Return the first ChildDevice matching the query, or a new ChildDevice object populated from the query conditions when no match is found
 *
 * @method     ChildDevice findOneById(int $id) Return the first ChildDevice filtered by the id column
 * @method     ChildDevice findOneByUserId(int $user_id) Return the first ChildDevice filtered by the user_id column
 * @method     ChildDevice findOneByStatus(string $status) Return the first ChildDevice filtered by the status column
 * @method     ChildDevice findOneByType(string $type) Return the first ChildDevice filtered by the type column
 * @method     ChildDevice findOneByOs(string $os) Return the first ChildDevice filtered by the os column
 * @method     ChildDevice findOneByOsVersion(string $os_version) Return the first ChildDevice filtered by the os_version column
 * @method     ChildDevice findOneByEnableNotification(boolean $enable_notification) Return the first ChildDevice filtered by the enable_notification column
 * @method     ChildDevice findOneByApiVersion(string $api_version) Return the first ChildDevice filtered by the api_version column
 * @method     ChildDevice findOneByLanguage(string $language) Return the first ChildDevice filtered by the language column
 * @method     ChildDevice findOneByDemo(boolean $demo) Return the first ChildDevice filtered by the demo column
 * @method     ChildDevice findOneByToken(string $token) Return the first ChildDevice filtered by the token column
 * @method     ChildDevice findOneByApiKey(string $api_key) Return the first ChildDevice filtered by the api_key column
 * @method     ChildDevice findOneByIpAddress(string $ip_address) Return the first ChildDevice filtered by the ip_address column
 * @method     ChildDevice findOneByDeletedAt(string $deleted_at) Return the first ChildDevice filtered by the deleted_at column
 * @method     ChildDevice findOneByCreatedAt(string $created_at) Return the first ChildDevice filtered by the created_at column
 * @method     ChildDevice findOneByUpdatedAt(string $updated_at) Return the first ChildDevice filtered by the updated_at column *

 * @method     ChildDevice requirePk($key, ConnectionInterface $con = null) Return the ChildDevice by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOne(ConnectionInterface $con = null) Return the first ChildDevice matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDevice requireOneById(int $id) Return the first ChildDevice filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByUserId(int $user_id) Return the first ChildDevice filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByStatus(string $status) Return the first ChildDevice filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByType(string $type) Return the first ChildDevice filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByOs(string $os) Return the first ChildDevice filtered by the os column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByOsVersion(string $os_version) Return the first ChildDevice filtered by the os_version column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByEnableNotification(boolean $enable_notification) Return the first ChildDevice filtered by the enable_notification column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByApiVersion(string $api_version) Return the first ChildDevice filtered by the api_version column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByLanguage(string $language) Return the first ChildDevice filtered by the language column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByDemo(boolean $demo) Return the first ChildDevice filtered by the demo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByToken(string $token) Return the first ChildDevice filtered by the token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByApiKey(string $api_key) Return the first ChildDevice filtered by the api_key column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByIpAddress(string $ip_address) Return the first ChildDevice filtered by the ip_address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByDeletedAt(string $deleted_at) Return the first ChildDevice filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByCreatedAt(string $created_at) Return the first ChildDevice filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDevice requireOneByUpdatedAt(string $updated_at) Return the first ChildDevice filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDevice[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildDevice objects based on current ModelCriteria
 * @method     ChildDevice[]|ObjectCollection findById(int $id) Return ChildDevice objects filtered by the id column
 * @method     ChildDevice[]|ObjectCollection findByUserId(int $user_id) Return ChildDevice objects filtered by the user_id column
 * @method     ChildDevice[]|ObjectCollection findByStatus(string $status) Return ChildDevice objects filtered by the status column
 * @method     ChildDevice[]|ObjectCollection findByType(string $type) Return ChildDevice objects filtered by the type column
 * @method     ChildDevice[]|ObjectCollection findByOs(string $os) Return ChildDevice objects filtered by the os column
 * @method     ChildDevice[]|ObjectCollection findByOsVersion(string $os_version) Return ChildDevice objects filtered by the os_version column
 * @method     ChildDevice[]|ObjectCollection findByEnableNotification(boolean $enable_notification) Return ChildDevice objects filtered by the enable_notification column
 * @method     ChildDevice[]|ObjectCollection findByApiVersion(string $api_version) Return ChildDevice objects filtered by the api_version column
 * @method     ChildDevice[]|ObjectCollection findByLanguage(string $language) Return ChildDevice objects filtered by the language column
 * @method     ChildDevice[]|ObjectCollection findByDemo(boolean $demo) Return ChildDevice objects filtered by the demo column
 * @method     ChildDevice[]|ObjectCollection findByToken(string $token) Return ChildDevice objects filtered by the token column
 * @method     ChildDevice[]|ObjectCollection findByApiKey(string $api_key) Return ChildDevice objects filtered by the api_key column
 * @method     ChildDevice[]|ObjectCollection findByIpAddress(string $ip_address) Return ChildDevice objects filtered by the ip_address column
 * @method     ChildDevice[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildDevice objects filtered by the deleted_at column
 * @method     ChildDevice[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildDevice objects filtered by the created_at column
 * @method     ChildDevice[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildDevice objects filtered by the updated_at column
 * @method     ChildDevice[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class DeviceQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\DeviceQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\Device', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildDeviceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildDeviceQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildDeviceQuery) {
            return $criteria;
        }
        $query = new ChildDeviceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildDevice|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DeviceTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = DeviceTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDevice A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, status, type, os, os_version, enable_notification, api_version, language, demo, token, api_key, ip_address, deleted_at, created_at, updated_at FROM device WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildDevice $obj */
            $obj = new ChildDevice();
            $obj->hydrate($row);
            DeviceTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildDevice|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DeviceTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DeviceTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DeviceTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DeviceTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUserApp()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(DeviceTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(DeviceTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%', Criteria::LIKE); // WHERE status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the os column
     *
     * Example usage:
     * <code>
     * $query->filterByOs('fooValue');   // WHERE os = 'fooValue'
     * $query->filterByOs('%fooValue%', Criteria::LIKE); // WHERE os LIKE '%fooValue%'
     * </code>
     *
     * @param     string $os The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByOs($os = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($os)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_OS, $os, $comparison);
    }

    /**
     * Filter the query on the os_version column
     *
     * Example usage:
     * <code>
     * $query->filterByOsVersion('fooValue');   // WHERE os_version = 'fooValue'
     * $query->filterByOsVersion('%fooValue%', Criteria::LIKE); // WHERE os_version LIKE '%fooValue%'
     * </code>
     *
     * @param     string $osVersion The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByOsVersion($osVersion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($osVersion)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_OS_VERSION, $osVersion, $comparison);
    }

    /**
     * Filter the query on the enable_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByEnableNotification(true); // WHERE enable_notification = true
     * $query->filterByEnableNotification('yes'); // WHERE enable_notification = true
     * </code>
     *
     * @param     boolean|string $enableNotification The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByEnableNotification($enableNotification = null, $comparison = null)
    {
        if (is_string($enableNotification)) {
            $enableNotification = in_array(strtolower($enableNotification), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DeviceTableMap::COL_ENABLE_NOTIFICATION, $enableNotification, $comparison);
    }

    /**
     * Filter the query on the api_version column
     *
     * Example usage:
     * <code>
     * $query->filterByApiVersion('fooValue');   // WHERE api_version = 'fooValue'
     * $query->filterByApiVersion('%fooValue%', Criteria::LIKE); // WHERE api_version LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiVersion The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByApiVersion($apiVersion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiVersion)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_API_VERSION, $apiVersion, $comparison);
    }

    /**
     * Filter the query on the language column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguage('fooValue');   // WHERE language = 'fooValue'
     * $query->filterByLanguage('%fooValue%', Criteria::LIKE); // WHERE language LIKE '%fooValue%'
     * </code>
     *
     * @param     string $language The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByLanguage($language = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($language)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_LANGUAGE, $language, $comparison);
    }

    /**
     * Filter the query on the demo column
     *
     * Example usage:
     * <code>
     * $query->filterByDemo(true); // WHERE demo = true
     * $query->filterByDemo('yes'); // WHERE demo = true
     * </code>
     *
     * @param     boolean|string $demo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByDemo($demo = null, $comparison = null)
    {
        if (is_string($demo)) {
            $demo = in_array(strtolower($demo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DeviceTableMap::COL_DEMO, $demo, $comparison);
    }

    /**
     * Filter the query on the token column
     *
     * Example usage:
     * <code>
     * $query->filterByToken('fooValue');   // WHERE token = 'fooValue'
     * $query->filterByToken('%fooValue%', Criteria::LIKE); // WHERE token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $token The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByToken($token = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($token)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_TOKEN, $token, $comparison);
    }

    /**
     * Filter the query on the api_key column
     *
     * Example usage:
     * <code>
     * $query->filterByApiKey('fooValue');   // WHERE api_key = 'fooValue'
     * $query->filterByApiKey('%fooValue%', Criteria::LIKE); // WHERE api_key LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiKey The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByApiKey($apiKey = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiKey)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_API_KEY, $apiKey, $comparison);
    }

    /**
     * Filter the query on the ip_address column
     *
     * Example usage:
     * <code>
     * $query->filterByIpAddress('fooValue');   // WHERE ip_address = 'fooValue'
     * $query->filterByIpAddress('%fooValue%', Criteria::LIKE); // WHERE ip_address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ipAddress The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByIpAddress($ipAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ipAddress)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_IP_ADDRESS, $ipAddress, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(DeviceTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(DeviceTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(DeviceTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(DeviceTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(DeviceTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(DeviceTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\UserApp object
     *
     * @param \Database\HubPlus\UserApp|ObjectCollection $userApp The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByUserApp($userApp, $comparison = null)
    {
        if ($userApp instanceof \Database\HubPlus\UserApp) {
            return $this
                ->addUsingAlias(DeviceTableMap::COL_USER_ID, $userApp->getId(), $comparison);
        } elseif ($userApp instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DeviceTableMap::COL_USER_ID, $userApp->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUserApp() only accepts arguments of type \Database\HubPlus\UserApp or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserApp relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function joinUserApp($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserApp');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserApp');
        }

        return $this;
    }

    /**
     * Use the UserApp relation UserApp object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\UserAppQuery A secondary query class using the current class as primary query
     */
    public function useUserAppQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserApp($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserApp', '\Database\HubPlus\UserAppQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\CustomFormExtraLog object
     *
     * @param \Database\HubPlus\CustomFormExtraLog|ObjectCollection $customFormExtraLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByCustomFormExtraLog($customFormExtraLog, $comparison = null)
    {
        if ($customFormExtraLog instanceof \Database\HubPlus\CustomFormExtraLog) {
            return $this
                ->addUsingAlias(DeviceTableMap::COL_ID, $customFormExtraLog->getDeviceId(), $comparison);
        } elseif ($customFormExtraLog instanceof ObjectCollection) {
            return $this
                ->useCustomFormExtraLogQuery()
                ->filterByPrimaryKeys($customFormExtraLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomFormExtraLog() only accepts arguments of type \Database\HubPlus\CustomFormExtraLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomFormExtraLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function joinCustomFormExtraLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomFormExtraLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomFormExtraLog');
        }

        return $this;
    }

    /**
     * Use the CustomFormExtraLog relation CustomFormExtraLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\CustomFormExtraLogQuery A secondary query class using the current class as primary query
     */
    public function useCustomFormExtraLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCustomFormExtraLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomFormExtraLog', '\Database\HubPlus\CustomFormExtraLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Media object
     *
     * @param \Database\HubPlus\Media|ObjectCollection $media the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByMedia($media, $comparison = null)
    {
        if ($media instanceof \Database\HubPlus\Media) {
            return $this
                ->addUsingAlias(DeviceTableMap::COL_ID, $media->getDeviceId(), $comparison);
        } elseif ($media instanceof ObjectCollection) {
            return $this
                ->useMediaQuery()
                ->filterByPrimaryKeys($media->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMedia() only accepts arguments of type \Database\HubPlus\Media or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Media relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function joinMedia($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Media');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Media');
        }

        return $this;
    }

    /**
     * Use the Media relation Media object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\MediaQuery A secondary query class using the current class as primary query
     */
    public function useMediaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMedia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Media', '\Database\HubPlus\MediaQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\MediaExtraLog object
     *
     * @param \Database\HubPlus\MediaExtraLog|ObjectCollection $mediaExtraLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByMediaExtraLog($mediaExtraLog, $comparison = null)
    {
        if ($mediaExtraLog instanceof \Database\HubPlus\MediaExtraLog) {
            return $this
                ->addUsingAlias(DeviceTableMap::COL_ID, $mediaExtraLog->getDeviceId(), $comparison);
        } elseif ($mediaExtraLog instanceof ObjectCollection) {
            return $this
                ->useMediaExtraLogQuery()
                ->filterByPrimaryKeys($mediaExtraLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMediaExtraLog() only accepts arguments of type \Database\HubPlus\MediaExtraLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MediaExtraLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function joinMediaExtraLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MediaExtraLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MediaExtraLog');
        }

        return $this;
    }

    /**
     * Use the MediaExtraLog relation MediaExtraLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\MediaExtraLogQuery A secondary query class using the current class as primary query
     */
    public function useMediaExtraLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMediaExtraLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MediaExtraLog', '\Database\HubPlus\MediaExtraLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostExtraLog object
     *
     * @param \Database\HubPlus\PostExtraLog|ObjectCollection $postExtraLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByPostExtraLog($postExtraLog, $comparison = null)
    {
        if ($postExtraLog instanceof \Database\HubPlus\PostExtraLog) {
            return $this
                ->addUsingAlias(DeviceTableMap::COL_ID, $postExtraLog->getDeviceId(), $comparison);
        } elseif ($postExtraLog instanceof ObjectCollection) {
            return $this
                ->usePostExtraLogQuery()
                ->filterByPrimaryKeys($postExtraLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostExtraLog() only accepts arguments of type \Database\HubPlus\PostExtraLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostExtraLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function joinPostExtraLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostExtraLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostExtraLog');
        }

        return $this;
    }

    /**
     * Use the PostExtraLog relation PostExtraLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostExtraLogQuery A secondary query class using the current class as primary query
     */
    public function usePostExtraLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPostExtraLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostExtraLog', '\Database\HubPlus\PostExtraLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PushNotificationDevice object
     *
     * @param \Database\HubPlus\PushNotificationDevice|ObjectCollection $pushNotificationDevice the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByPushNotificationDevice($pushNotificationDevice, $comparison = null)
    {
        if ($pushNotificationDevice instanceof \Database\HubPlus\PushNotificationDevice) {
            return $this
                ->addUsingAlias(DeviceTableMap::COL_ID, $pushNotificationDevice->getDeviceId(), $comparison);
        } elseif ($pushNotificationDevice instanceof ObjectCollection) {
            return $this
                ->usePushNotificationDeviceQuery()
                ->filterByPrimaryKeys($pushNotificationDevice->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPushNotificationDevice() only accepts arguments of type \Database\HubPlus\PushNotificationDevice or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PushNotificationDevice relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function joinPushNotificationDevice($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PushNotificationDevice');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PushNotificationDevice');
        }

        return $this;
    }

    /**
     * Use the PushNotificationDevice relation PushNotificationDevice object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PushNotificationDeviceQuery A secondary query class using the current class as primary query
     */
    public function usePushNotificationDeviceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPushNotificationDevice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PushNotificationDevice', '\Database\HubPlus\PushNotificationDeviceQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\RemoteExtraLog object
     *
     * @param \Database\HubPlus\RemoteExtraLog|ObjectCollection $remoteExtraLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDeviceQuery The current query, for fluid interface
     */
    public function filterByRemoteExtraLog($remoteExtraLog, $comparison = null)
    {
        if ($remoteExtraLog instanceof \Database\HubPlus\RemoteExtraLog) {
            return $this
                ->addUsingAlias(DeviceTableMap::COL_ID, $remoteExtraLog->getDeviceId(), $comparison);
        } elseif ($remoteExtraLog instanceof ObjectCollection) {
            return $this
                ->useRemoteExtraLogQuery()
                ->filterByPrimaryKeys($remoteExtraLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRemoteExtraLog() only accepts arguments of type \Database\HubPlus\RemoteExtraLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RemoteExtraLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function joinRemoteExtraLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RemoteExtraLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RemoteExtraLog');
        }

        return $this;
    }

    /**
     * Use the RemoteExtraLog relation RemoteExtraLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\RemoteExtraLogQuery A secondary query class using the current class as primary query
     */
    public function useRemoteExtraLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRemoteExtraLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RemoteExtraLog', '\Database\HubPlus\RemoteExtraLogQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildDevice $device Object to remove from the list of results
     *
     * @return $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function prune($device = null)
    {
        if ($device) {
            $this->addUsingAlias(DeviceTableMap::COL_ID, $device->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the device table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            DeviceTableMap::clearInstancePool();
            DeviceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(DeviceTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            DeviceTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            DeviceTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(DeviceTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(DeviceTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(DeviceTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(DeviceTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(DeviceTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildDeviceQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(DeviceTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildDeviceArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // DeviceQuery
