<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\Post as ChildPost;
use Database\HubPlus\PostArchive as ChildPostArchive;
use Database\HubPlus\PostQuery as ChildPostQuery;
use Database\HubPlus\Map\PostTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'post' table.
 *
 *
 *
 * @method     ChildPostQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPostQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildPostQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildPostQuery orderByAuthorId($order = Criteria::ASC) Order by the author_id column
 * @method     ChildPostQuery orderByCoverId($order = Criteria::ASC) Order by the cover_id column
 * @method     ChildPostQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 * @method     ChildPostQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildPostQuery orderBySubtitle($order = Criteria::ASC) Order by the subtitle column
 * @method     ChildPostQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildPostQuery orderByBody($order = Criteria::ASC) Order by the body column
 * @method     ChildPostQuery orderBySearch($order = Criteria::ASC) Order by the search column
 * @method     ChildPostQuery orderByTemplateId($order = Criteria::ASC) Order by the template_id column
 * @method     ChildPostQuery orderByLayout($order = Criteria::ASC) Order by the layout column
 * @method     ChildPostQuery orderByTags($order = Criteria::ASC) Order by the tags column
 * @method     ChildPostQuery orderByTagsUser($order = Criteria::ASC) Order by the tags_user column
 * @method     ChildPostQuery orderByRowData($order = Criteria::ASC) Order by the row_data column
 * @method     ChildPostQuery orderByCountLike($order = Criteria::ASC) Order by the count_like column
 * @method     ChildPostQuery orderByCountShare($order = Criteria::ASC) Order by the count_share column
 * @method     ChildPostQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildPostQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPostQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildPostQuery groupById() Group by the id column
 * @method     ChildPostQuery groupByType() Group by the type column
 * @method     ChildPostQuery groupByStatus() Group by the status column
 * @method     ChildPostQuery groupByAuthorId() Group by the author_id column
 * @method     ChildPostQuery groupByCoverId() Group by the cover_id column
 * @method     ChildPostQuery groupBySlug() Group by the slug column
 * @method     ChildPostQuery groupByTitle() Group by the title column
 * @method     ChildPostQuery groupBySubtitle() Group by the subtitle column
 * @method     ChildPostQuery groupByDescription() Group by the description column
 * @method     ChildPostQuery groupByBody() Group by the body column
 * @method     ChildPostQuery groupBySearch() Group by the search column
 * @method     ChildPostQuery groupByTemplateId() Group by the template_id column
 * @method     ChildPostQuery groupByLayout() Group by the layout column
 * @method     ChildPostQuery groupByTags() Group by the tags column
 * @method     ChildPostQuery groupByTagsUser() Group by the tags_user column
 * @method     ChildPostQuery groupByRowData() Group by the row_data column
 * @method     ChildPostQuery groupByCountLike() Group by the count_like column
 * @method     ChildPostQuery groupByCountShare() Group by the count_share column
 * @method     ChildPostQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildPostQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPostQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildPostQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPostQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPostQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPostQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPostQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPostQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPostQuery leftJoinTemplate($relationAlias = null) Adds a LEFT JOIN clause to the query using the Template relation
 * @method     ChildPostQuery rightJoinTemplate($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Template relation
 * @method     ChildPostQuery innerJoinTemplate($relationAlias = null) Adds a INNER JOIN clause to the query using the Template relation
 *
 * @method     ChildPostQuery joinWithTemplate($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Template relation
 *
 * @method     ChildPostQuery leftJoinWithTemplate() Adds a LEFT JOIN clause and with to the query using the Template relation
 * @method     ChildPostQuery rightJoinWithTemplate() Adds a RIGHT JOIN clause and with to the query using the Template relation
 * @method     ChildPostQuery innerJoinWithTemplate() Adds a INNER JOIN clause and with to the query using the Template relation
 *
 * @method     ChildPostQuery leftJoinMedia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Media relation
 * @method     ChildPostQuery rightJoinMedia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Media relation
 * @method     ChildPostQuery innerJoinMedia($relationAlias = null) Adds a INNER JOIN clause to the query using the Media relation
 *
 * @method     ChildPostQuery joinWithMedia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Media relation
 *
 * @method     ChildPostQuery leftJoinWithMedia() Adds a LEFT JOIN clause and with to the query using the Media relation
 * @method     ChildPostQuery rightJoinWithMedia() Adds a RIGHT JOIN clause and with to the query using the Media relation
 * @method     ChildPostQuery innerJoinWithMedia() Adds a INNER JOIN clause and with to the query using the Media relation
 *
 * @method     ChildPostQuery leftJoinUserBackend($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserBackend relation
 * @method     ChildPostQuery rightJoinUserBackend($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserBackend relation
 * @method     ChildPostQuery innerJoinUserBackend($relationAlias = null) Adds a INNER JOIN clause to the query using the UserBackend relation
 *
 * @method     ChildPostQuery joinWithUserBackend($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserBackend relation
 *
 * @method     ChildPostQuery leftJoinWithUserBackend() Adds a LEFT JOIN clause and with to the query using the UserBackend relation
 * @method     ChildPostQuery rightJoinWithUserBackend() Adds a RIGHT JOIN clause and with to the query using the UserBackend relation
 * @method     ChildPostQuery innerJoinWithUserBackend() Adds a INNER JOIN clause and with to the query using the UserBackend relation
 *
 * @method     ChildPostQuery leftJoinGallery($relationAlias = null) Adds a LEFT JOIN clause to the query using the Gallery relation
 * @method     ChildPostQuery rightJoinGallery($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Gallery relation
 * @method     ChildPostQuery innerJoinGallery($relationAlias = null) Adds a INNER JOIN clause to the query using the Gallery relation
 *
 * @method     ChildPostQuery joinWithGallery($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Gallery relation
 *
 * @method     ChildPostQuery leftJoinWithGallery() Adds a LEFT JOIN clause and with to the query using the Gallery relation
 * @method     ChildPostQuery rightJoinWithGallery() Adds a RIGHT JOIN clause and with to the query using the Gallery relation
 * @method     ChildPostQuery innerJoinWithGallery() Adds a INNER JOIN clause and with to the query using the Gallery relation
 *
 * @method     ChildPostQuery leftJoinPostActionRelatedByPostId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostActionRelatedByPostId relation
 * @method     ChildPostQuery rightJoinPostActionRelatedByPostId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostActionRelatedByPostId relation
 * @method     ChildPostQuery innerJoinPostActionRelatedByPostId($relationAlias = null) Adds a INNER JOIN clause to the query using the PostActionRelatedByPostId relation
 *
 * @method     ChildPostQuery joinWithPostActionRelatedByPostId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostActionRelatedByPostId relation
 *
 * @method     ChildPostQuery leftJoinWithPostActionRelatedByPostId() Adds a LEFT JOIN clause and with to the query using the PostActionRelatedByPostId relation
 * @method     ChildPostQuery rightJoinWithPostActionRelatedByPostId() Adds a RIGHT JOIN clause and with to the query using the PostActionRelatedByPostId relation
 * @method     ChildPostQuery innerJoinWithPostActionRelatedByPostId() Adds a INNER JOIN clause and with to the query using the PostActionRelatedByPostId relation
 *
 * @method     ChildPostQuery leftJoinPostActionRelatedByContentId($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostActionRelatedByContentId relation
 * @method     ChildPostQuery rightJoinPostActionRelatedByContentId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostActionRelatedByContentId relation
 * @method     ChildPostQuery innerJoinPostActionRelatedByContentId($relationAlias = null) Adds a INNER JOIN clause to the query using the PostActionRelatedByContentId relation
 *
 * @method     ChildPostQuery joinWithPostActionRelatedByContentId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostActionRelatedByContentId relation
 *
 * @method     ChildPostQuery leftJoinWithPostActionRelatedByContentId() Adds a LEFT JOIN clause and with to the query using the PostActionRelatedByContentId relation
 * @method     ChildPostQuery rightJoinWithPostActionRelatedByContentId() Adds a RIGHT JOIN clause and with to the query using the PostActionRelatedByContentId relation
 * @method     ChildPostQuery innerJoinWithPostActionRelatedByContentId() Adds a INNER JOIN clause and with to the query using the PostActionRelatedByContentId relation
 *
 * @method     ChildPostQuery leftJoinPostConnector($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostConnector relation
 * @method     ChildPostQuery rightJoinPostConnector($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostConnector relation
 * @method     ChildPostQuery innerJoinPostConnector($relationAlias = null) Adds a INNER JOIN clause to the query using the PostConnector relation
 *
 * @method     ChildPostQuery joinWithPostConnector($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostConnector relation
 *
 * @method     ChildPostQuery leftJoinWithPostConnector() Adds a LEFT JOIN clause and with to the query using the PostConnector relation
 * @method     ChildPostQuery rightJoinWithPostConnector() Adds a RIGHT JOIN clause and with to the query using the PostConnector relation
 * @method     ChildPostQuery innerJoinWithPostConnector() Adds a INNER JOIN clause and with to the query using the PostConnector relation
 *
 * @method     ChildPostQuery leftJoinPostEvent($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostEvent relation
 * @method     ChildPostQuery rightJoinPostEvent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostEvent relation
 * @method     ChildPostQuery innerJoinPostEvent($relationAlias = null) Adds a INNER JOIN clause to the query using the PostEvent relation
 *
 * @method     ChildPostQuery joinWithPostEvent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostEvent relation
 *
 * @method     ChildPostQuery leftJoinWithPostEvent() Adds a LEFT JOIN clause and with to the query using the PostEvent relation
 * @method     ChildPostQuery rightJoinWithPostEvent() Adds a RIGHT JOIN clause and with to the query using the PostEvent relation
 * @method     ChildPostQuery innerJoinWithPostEvent() Adds a INNER JOIN clause and with to the query using the PostEvent relation
 *
 * @method     ChildPostQuery leftJoinPostExtraLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostExtraLog relation
 * @method     ChildPostQuery rightJoinPostExtraLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostExtraLog relation
 * @method     ChildPostQuery innerJoinPostExtraLog($relationAlias = null) Adds a INNER JOIN clause to the query using the PostExtraLog relation
 *
 * @method     ChildPostQuery joinWithPostExtraLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostExtraLog relation
 *
 * @method     ChildPostQuery leftJoinWithPostExtraLog() Adds a LEFT JOIN clause and with to the query using the PostExtraLog relation
 * @method     ChildPostQuery rightJoinWithPostExtraLog() Adds a RIGHT JOIN clause and with to the query using the PostExtraLog relation
 * @method     ChildPostQuery innerJoinWithPostExtraLog() Adds a INNER JOIN clause and with to the query using the PostExtraLog relation
 *
 * @method     ChildPostQuery leftJoinPostLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostLog relation
 * @method     ChildPostQuery rightJoinPostLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostLog relation
 * @method     ChildPostQuery innerJoinPostLog($relationAlias = null) Adds a INNER JOIN clause to the query using the PostLog relation
 *
 * @method     ChildPostQuery joinWithPostLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostLog relation
 *
 * @method     ChildPostQuery leftJoinWithPostLog() Adds a LEFT JOIN clause and with to the query using the PostLog relation
 * @method     ChildPostQuery rightJoinWithPostLog() Adds a RIGHT JOIN clause and with to the query using the PostLog relation
 * @method     ChildPostQuery innerJoinWithPostLog() Adds a INNER JOIN clause and with to the query using the PostLog relation
 *
 * @method     ChildPostQuery leftJoinPostPerson($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostPerson relation
 * @method     ChildPostQuery rightJoinPostPerson($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostPerson relation
 * @method     ChildPostQuery innerJoinPostPerson($relationAlias = null) Adds a INNER JOIN clause to the query using the PostPerson relation
 *
 * @method     ChildPostQuery joinWithPostPerson($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostPerson relation
 *
 * @method     ChildPostQuery leftJoinWithPostPerson() Adds a LEFT JOIN clause and with to the query using the PostPerson relation
 * @method     ChildPostQuery rightJoinWithPostPerson() Adds a RIGHT JOIN clause and with to the query using the PostPerson relation
 * @method     ChildPostQuery innerJoinWithPostPerson() Adds a INNER JOIN clause and with to the query using the PostPerson relation
 *
 * @method     ChildPostQuery leftJoinPostPoi($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostPoi relation
 * @method     ChildPostQuery rightJoinPostPoi($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostPoi relation
 * @method     ChildPostQuery innerJoinPostPoi($relationAlias = null) Adds a INNER JOIN clause to the query using the PostPoi relation
 *
 * @method     ChildPostQuery joinWithPostPoi($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostPoi relation
 *
 * @method     ChildPostQuery leftJoinWithPostPoi() Adds a LEFT JOIN clause and with to the query using the PostPoi relation
 * @method     ChildPostQuery rightJoinWithPostPoi() Adds a RIGHT JOIN clause and with to the query using the PostPoi relation
 * @method     ChildPostQuery innerJoinWithPostPoi() Adds a INNER JOIN clause and with to the query using the PostPoi relation
 *
 * @method     ChildPostQuery leftJoinPushNotification($relationAlias = null) Adds a LEFT JOIN clause to the query using the PushNotification relation
 * @method     ChildPostQuery rightJoinPushNotification($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PushNotification relation
 * @method     ChildPostQuery innerJoinPushNotification($relationAlias = null) Adds a INNER JOIN clause to the query using the PushNotification relation
 *
 * @method     ChildPostQuery joinWithPushNotification($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PushNotification relation
 *
 * @method     ChildPostQuery leftJoinWithPushNotification() Adds a LEFT JOIN clause and with to the query using the PushNotification relation
 * @method     ChildPostQuery rightJoinWithPushNotification() Adds a RIGHT JOIN clause and with to the query using the PushNotification relation
 * @method     ChildPostQuery innerJoinWithPushNotification() Adds a INNER JOIN clause and with to the query using the PushNotification relation
 *
 * @method     ChildPostQuery leftJoinWebhook($relationAlias = null) Adds a LEFT JOIN clause to the query using the Webhook relation
 * @method     ChildPostQuery rightJoinWebhook($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Webhook relation
 * @method     ChildPostQuery innerJoinWebhook($relationAlias = null) Adds a INNER JOIN clause to the query using the Webhook relation
 *
 * @method     ChildPostQuery joinWithWebhook($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Webhook relation
 *
 * @method     ChildPostQuery leftJoinWithWebhook() Adds a LEFT JOIN clause and with to the query using the Webhook relation
 * @method     ChildPostQuery rightJoinWithWebhook() Adds a RIGHT JOIN clause and with to the query using the Webhook relation
 * @method     ChildPostQuery innerJoinWithWebhook() Adds a INNER JOIN clause and with to the query using the Webhook relation
 *
 * @method     \Database\HubPlus\TemplateQuery|\Database\HubPlus\MediaQuery|\Database\HubPlus\UserBackendQuery|\Database\HubPlus\GalleryQuery|\Database\HubPlus\PostActionQuery|\Database\HubPlus\PostConnectorQuery|\Database\HubPlus\PostEventQuery|\Database\HubPlus\PostExtraLogQuery|\Database\HubPlus\PostLogQuery|\Database\HubPlus\PostPersonQuery|\Database\HubPlus\PostPoiQuery|\Database\HubPlus\PushNotificationQuery|\Database\HubPlus\WebhookQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPost findOne(ConnectionInterface $con = null) Return the first ChildPost matching the query
 * @method     ChildPost findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPost matching the query, or a new ChildPost object populated from the query conditions when no match is found
 *
 * @method     ChildPost findOneById(int $id) Return the first ChildPost filtered by the id column
 * @method     ChildPost findOneByType(string $type) Return the first ChildPost filtered by the type column
 * @method     ChildPost findOneByStatus(int $status) Return the first ChildPost filtered by the status column
 * @method     ChildPost findOneByAuthorId(int $author_id) Return the first ChildPost filtered by the author_id column
 * @method     ChildPost findOneByCoverId(int $cover_id) Return the first ChildPost filtered by the cover_id column
 * @method     ChildPost findOneBySlug(string $slug) Return the first ChildPost filtered by the slug column
 * @method     ChildPost findOneByTitle(string $title) Return the first ChildPost filtered by the title column
 * @method     ChildPost findOneBySubtitle(string $subtitle) Return the first ChildPost filtered by the subtitle column
 * @method     ChildPost findOneByDescription(string $description) Return the first ChildPost filtered by the description column
 * @method     ChildPost findOneByBody(string $body) Return the first ChildPost filtered by the body column
 * @method     ChildPost findOneBySearch(string $search) Return the first ChildPost filtered by the search column
 * @method     ChildPost findOneByTemplateId(int $template_id) Return the first ChildPost filtered by the template_id column
 * @method     ChildPost findOneByLayout(string $layout) Return the first ChildPost filtered by the layout column
 * @method     ChildPost findOneByTags(string $tags) Return the first ChildPost filtered by the tags column
 * @method     ChildPost findOneByTagsUser(string $tags_user) Return the first ChildPost filtered by the tags_user column
 * @method     ChildPost findOneByRowData(string $row_data) Return the first ChildPost filtered by the row_data column
 * @method     ChildPost findOneByCountLike(int $count_like) Return the first ChildPost filtered by the count_like column
 * @method     ChildPost findOneByCountShare(int $count_share) Return the first ChildPost filtered by the count_share column
 * @method     ChildPost findOneByDeletedAt(string $deleted_at) Return the first ChildPost filtered by the deleted_at column
 * @method     ChildPost findOneByCreatedAt(string $created_at) Return the first ChildPost filtered by the created_at column
 * @method     ChildPost findOneByUpdatedAt(string $updated_at) Return the first ChildPost filtered by the updated_at column *

 * @method     ChildPost requirePk($key, ConnectionInterface $con = null) Return the ChildPost by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOne(ConnectionInterface $con = null) Return the first ChildPost matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPost requireOneById(int $id) Return the first ChildPost filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByType(string $type) Return the first ChildPost filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByStatus(int $status) Return the first ChildPost filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByAuthorId(int $author_id) Return the first ChildPost filtered by the author_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByCoverId(int $cover_id) Return the first ChildPost filtered by the cover_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneBySlug(string $slug) Return the first ChildPost filtered by the slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByTitle(string $title) Return the first ChildPost filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneBySubtitle(string $subtitle) Return the first ChildPost filtered by the subtitle column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByDescription(string $description) Return the first ChildPost filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByBody(string $body) Return the first ChildPost filtered by the body column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneBySearch(string $search) Return the first ChildPost filtered by the search column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByTemplateId(int $template_id) Return the first ChildPost filtered by the template_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByLayout(string $layout) Return the first ChildPost filtered by the layout column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByTags(string $tags) Return the first ChildPost filtered by the tags column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByTagsUser(string $tags_user) Return the first ChildPost filtered by the tags_user column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByRowData(string $row_data) Return the first ChildPost filtered by the row_data column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByCountLike(int $count_like) Return the first ChildPost filtered by the count_like column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByCountShare(int $count_share) Return the first ChildPost filtered by the count_share column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByDeletedAt(string $deleted_at) Return the first ChildPost filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByCreatedAt(string $created_at) Return the first ChildPost filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPost requireOneByUpdatedAt(string $updated_at) Return the first ChildPost filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPost[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPost objects based on current ModelCriteria
 * @method     ChildPost[]|ObjectCollection findById(int $id) Return ChildPost objects filtered by the id column
 * @method     ChildPost[]|ObjectCollection findByType(string $type) Return ChildPost objects filtered by the type column
 * @method     ChildPost[]|ObjectCollection findByStatus(int $status) Return ChildPost objects filtered by the status column
 * @method     ChildPost[]|ObjectCollection findByAuthorId(int $author_id) Return ChildPost objects filtered by the author_id column
 * @method     ChildPost[]|ObjectCollection findByCoverId(int $cover_id) Return ChildPost objects filtered by the cover_id column
 * @method     ChildPost[]|ObjectCollection findBySlug(string $slug) Return ChildPost objects filtered by the slug column
 * @method     ChildPost[]|ObjectCollection findByTitle(string $title) Return ChildPost objects filtered by the title column
 * @method     ChildPost[]|ObjectCollection findBySubtitle(string $subtitle) Return ChildPost objects filtered by the subtitle column
 * @method     ChildPost[]|ObjectCollection findByDescription(string $description) Return ChildPost objects filtered by the description column
 * @method     ChildPost[]|ObjectCollection findByBody(string $body) Return ChildPost objects filtered by the body column
 * @method     ChildPost[]|ObjectCollection findBySearch(string $search) Return ChildPost objects filtered by the search column
 * @method     ChildPost[]|ObjectCollection findByTemplateId(int $template_id) Return ChildPost objects filtered by the template_id column
 * @method     ChildPost[]|ObjectCollection findByLayout(string $layout) Return ChildPost objects filtered by the layout column
 * @method     ChildPost[]|ObjectCollection findByTags(string $tags) Return ChildPost objects filtered by the tags column
 * @method     ChildPost[]|ObjectCollection findByTagsUser(string $tags_user) Return ChildPost objects filtered by the tags_user column
 * @method     ChildPost[]|ObjectCollection findByRowData(string $row_data) Return ChildPost objects filtered by the row_data column
 * @method     ChildPost[]|ObjectCollection findByCountLike(int $count_like) Return ChildPost objects filtered by the count_like column
 * @method     ChildPost[]|ObjectCollection findByCountShare(int $count_share) Return ChildPost objects filtered by the count_share column
 * @method     ChildPost[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildPost objects filtered by the deleted_at column
 * @method     ChildPost[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPost objects filtered by the created_at column
 * @method     ChildPost[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPost objects filtered by the updated_at column
 * @method     ChildPost[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PostQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\PostQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\Post', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPostQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPostQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPostQuery) {
            return $criteria;
        }
        $query = new ChildPostQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPost|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PostTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PostTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPost A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, type, status, author_id, cover_id, slug, title, subtitle, description, body, search, template_id, layout, tags, tags_user, row_data, count_like, count_share, deleted_at, created_at, updated_at FROM post WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPost $obj */
            $obj = new ChildPost();
            $obj->hydrate($row);
            PostTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPost|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PostTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PostTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PostTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PostTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(PostTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(PostTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the author_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAuthorId(1234); // WHERE author_id = 1234
     * $query->filterByAuthorId(array(12, 34)); // WHERE author_id IN (12, 34)
     * $query->filterByAuthorId(array('min' => 12)); // WHERE author_id > 12
     * </code>
     *
     * @see       filterByUserBackend()
     *
     * @param     mixed $authorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByAuthorId($authorId = null, $comparison = null)
    {
        if (is_array($authorId)) {
            $useMinMax = false;
            if (isset($authorId['min'])) {
                $this->addUsingAlias(PostTableMap::COL_AUTHOR_ID, $authorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($authorId['max'])) {
                $this->addUsingAlias(PostTableMap::COL_AUTHOR_ID, $authorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_AUTHOR_ID, $authorId, $comparison);
    }

    /**
     * Filter the query on the cover_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCoverId(1234); // WHERE cover_id = 1234
     * $query->filterByCoverId(array(12, 34)); // WHERE cover_id IN (12, 34)
     * $query->filterByCoverId(array('min' => 12)); // WHERE cover_id > 12
     * </code>
     *
     * @see       filterByMedia()
     *
     * @param     mixed $coverId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByCoverId($coverId = null, $comparison = null)
    {
        if (is_array($coverId)) {
            $useMinMax = false;
            if (isset($coverId['min'])) {
                $this->addUsingAlias(PostTableMap::COL_COVER_ID, $coverId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($coverId['max'])) {
                $this->addUsingAlias(PostTableMap::COL_COVER_ID, $coverId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_COVER_ID, $coverId, $comparison);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%', Criteria::LIKE); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the subtitle column
     *
     * Example usage:
     * <code>
     * $query->filterBySubtitle('fooValue');   // WHERE subtitle = 'fooValue'
     * $query->filterBySubtitle('%fooValue%', Criteria::LIKE); // WHERE subtitle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $subtitle The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterBySubtitle($subtitle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($subtitle)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_SUBTITLE, $subtitle, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the body column
     *
     * Example usage:
     * <code>
     * $query->filterByBody('fooValue');   // WHERE body = 'fooValue'
     * $query->filterByBody('%fooValue%', Criteria::LIKE); // WHERE body LIKE '%fooValue%'
     * </code>
     *
     * @param     string $body The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByBody($body = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($body)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_BODY, $body, $comparison);
    }

    /**
     * Filter the query on the search column
     *
     * Example usage:
     * <code>
     * $query->filterBySearch('fooValue');   // WHERE search = 'fooValue'
     * $query->filterBySearch('%fooValue%', Criteria::LIKE); // WHERE search LIKE '%fooValue%'
     * </code>
     *
     * @param     string $search The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterBySearch($search = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($search)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_SEARCH, $search, $comparison);
    }

    /**
     * Filter the query on the template_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTemplateId(1234); // WHERE template_id = 1234
     * $query->filterByTemplateId(array(12, 34)); // WHERE template_id IN (12, 34)
     * $query->filterByTemplateId(array('min' => 12)); // WHERE template_id > 12
     * </code>
     *
     * @see       filterByTemplate()
     *
     * @param     mixed $templateId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByTemplateId($templateId = null, $comparison = null)
    {
        if (is_array($templateId)) {
            $useMinMax = false;
            if (isset($templateId['min'])) {
                $this->addUsingAlias(PostTableMap::COL_TEMPLATE_ID, $templateId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($templateId['max'])) {
                $this->addUsingAlias(PostTableMap::COL_TEMPLATE_ID, $templateId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_TEMPLATE_ID, $templateId, $comparison);
    }

    /**
     * Filter the query on the layout column
     *
     * Example usage:
     * <code>
     * $query->filterByLayout('fooValue');   // WHERE layout = 'fooValue'
     * $query->filterByLayout('%fooValue%', Criteria::LIKE); // WHERE layout LIKE '%fooValue%'
     * </code>
     *
     * @param     string $layout The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByLayout($layout = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($layout)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_LAYOUT, $layout, $comparison);
    }

    /**
     * Filter the query on the tags column
     *
     * Example usage:
     * <code>
     * $query->filterByTags('fooValue');   // WHERE tags = 'fooValue'
     * $query->filterByTags('%fooValue%', Criteria::LIKE); // WHERE tags LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tags The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByTags($tags = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tags)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_TAGS, $tags, $comparison);
    }

    /**
     * Filter the query on the tags_user column
     *
     * Example usage:
     * <code>
     * $query->filterByTagsUser('fooValue');   // WHERE tags_user = 'fooValue'
     * $query->filterByTagsUser('%fooValue%', Criteria::LIKE); // WHERE tags_user LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tagsUser The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByTagsUser($tagsUser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tagsUser)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_TAGS_USER, $tagsUser, $comparison);
    }

    /**
     * Filter the query on the row_data column
     *
     * Example usage:
     * <code>
     * $query->filterByRowData('fooValue');   // WHERE row_data = 'fooValue'
     * $query->filterByRowData('%fooValue%', Criteria::LIKE); // WHERE row_data LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rowData The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByRowData($rowData = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rowData)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_ROW_DATA, $rowData, $comparison);
    }

    /**
     * Filter the query on the count_like column
     *
     * Example usage:
     * <code>
     * $query->filterByCountLike(1234); // WHERE count_like = 1234
     * $query->filterByCountLike(array(12, 34)); // WHERE count_like IN (12, 34)
     * $query->filterByCountLike(array('min' => 12)); // WHERE count_like > 12
     * </code>
     *
     * @param     mixed $countLike The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByCountLike($countLike = null, $comparison = null)
    {
        if (is_array($countLike)) {
            $useMinMax = false;
            if (isset($countLike['min'])) {
                $this->addUsingAlias(PostTableMap::COL_COUNT_LIKE, $countLike['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countLike['max'])) {
                $this->addUsingAlias(PostTableMap::COL_COUNT_LIKE, $countLike['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_COUNT_LIKE, $countLike, $comparison);
    }

    /**
     * Filter the query on the count_share column
     *
     * Example usage:
     * <code>
     * $query->filterByCountShare(1234); // WHERE count_share = 1234
     * $query->filterByCountShare(array(12, 34)); // WHERE count_share IN (12, 34)
     * $query->filterByCountShare(array('min' => 12)); // WHERE count_share > 12
     * </code>
     *
     * @param     mixed $countShare The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByCountShare($countShare = null, $comparison = null)
    {
        if (is_array($countShare)) {
            $useMinMax = false;
            if (isset($countShare['min'])) {
                $this->addUsingAlias(PostTableMap::COL_COUNT_SHARE, $countShare['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countShare['max'])) {
                $this->addUsingAlias(PostTableMap::COL_COUNT_SHARE, $countShare['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_COUNT_SHARE, $countShare, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(PostTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(PostTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PostTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PostTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PostTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PostTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Template object
     *
     * @param \Database\HubPlus\Template|ObjectCollection $template The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByTemplate($template, $comparison = null)
    {
        if ($template instanceof \Database\HubPlus\Template) {
            return $this
                ->addUsingAlias(PostTableMap::COL_TEMPLATE_ID, $template->getId(), $comparison);
        } elseif ($template instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PostTableMap::COL_TEMPLATE_ID, $template->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTemplate() only accepts arguments of type \Database\HubPlus\Template or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Template relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinTemplate($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Template');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Template');
        }

        return $this;
    }

    /**
     * Use the Template relation Template object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\TemplateQuery A secondary query class using the current class as primary query
     */
    public function useTemplateQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTemplate($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Template', '\Database\HubPlus\TemplateQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Media object
     *
     * @param \Database\HubPlus\Media|ObjectCollection $media The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByMedia($media, $comparison = null)
    {
        if ($media instanceof \Database\HubPlus\Media) {
            return $this
                ->addUsingAlias(PostTableMap::COL_COVER_ID, $media->getId(), $comparison);
        } elseif ($media instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PostTableMap::COL_COVER_ID, $media->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMedia() only accepts arguments of type \Database\HubPlus\Media or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Media relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinMedia($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Media');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Media');
        }

        return $this;
    }

    /**
     * Use the Media relation Media object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\MediaQuery A secondary query class using the current class as primary query
     */
    public function useMediaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMedia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Media', '\Database\HubPlus\MediaQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\UserBackend object
     *
     * @param \Database\HubPlus\UserBackend|ObjectCollection $userBackend The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByUserBackend($userBackend, $comparison = null)
    {
        if ($userBackend instanceof \Database\HubPlus\UserBackend) {
            return $this
                ->addUsingAlias(PostTableMap::COL_AUTHOR_ID, $userBackend->getId(), $comparison);
        } elseif ($userBackend instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PostTableMap::COL_AUTHOR_ID, $userBackend->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUserBackend() only accepts arguments of type \Database\HubPlus\UserBackend or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserBackend relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinUserBackend($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserBackend');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserBackend');
        }

        return $this;
    }

    /**
     * Use the UserBackend relation UserBackend object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\UserBackendQuery A secondary query class using the current class as primary query
     */
    public function useUserBackendQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserBackend($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserBackend', '\Database\HubPlus\UserBackendQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Gallery object
     *
     * @param \Database\HubPlus\Gallery|ObjectCollection $gallery the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByGallery($gallery, $comparison = null)
    {
        if ($gallery instanceof \Database\HubPlus\Gallery) {
            return $this
                ->addUsingAlias(PostTableMap::COL_ID, $gallery->getPostId(), $comparison);
        } elseif ($gallery instanceof ObjectCollection) {
            return $this
                ->useGalleryQuery()
                ->filterByPrimaryKeys($gallery->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByGallery() only accepts arguments of type \Database\HubPlus\Gallery or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Gallery relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinGallery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Gallery');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Gallery');
        }

        return $this;
    }

    /**
     * Use the Gallery relation Gallery object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\GalleryQuery A secondary query class using the current class as primary query
     */
    public function useGalleryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinGallery($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Gallery', '\Database\HubPlus\GalleryQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostAction object
     *
     * @param \Database\HubPlus\PostAction|ObjectCollection $postAction the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByPostActionRelatedByPostId($postAction, $comparison = null)
    {
        if ($postAction instanceof \Database\HubPlus\PostAction) {
            return $this
                ->addUsingAlias(PostTableMap::COL_ID, $postAction->getPostId(), $comparison);
        } elseif ($postAction instanceof ObjectCollection) {
            return $this
                ->usePostActionRelatedByPostIdQuery()
                ->filterByPrimaryKeys($postAction->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostActionRelatedByPostId() only accepts arguments of type \Database\HubPlus\PostAction or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostActionRelatedByPostId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinPostActionRelatedByPostId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostActionRelatedByPostId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostActionRelatedByPostId');
        }

        return $this;
    }

    /**
     * Use the PostActionRelatedByPostId relation PostAction object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostActionQuery A secondary query class using the current class as primary query
     */
    public function usePostActionRelatedByPostIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPostActionRelatedByPostId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostActionRelatedByPostId', '\Database\HubPlus\PostActionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostAction object
     *
     * @param \Database\HubPlus\PostAction|ObjectCollection $postAction the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByPostActionRelatedByContentId($postAction, $comparison = null)
    {
        if ($postAction instanceof \Database\HubPlus\PostAction) {
            return $this
                ->addUsingAlias(PostTableMap::COL_ID, $postAction->getContentId(), $comparison);
        } elseif ($postAction instanceof ObjectCollection) {
            return $this
                ->usePostActionRelatedByContentIdQuery()
                ->filterByPrimaryKeys($postAction->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostActionRelatedByContentId() only accepts arguments of type \Database\HubPlus\PostAction or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostActionRelatedByContentId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinPostActionRelatedByContentId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostActionRelatedByContentId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostActionRelatedByContentId');
        }

        return $this;
    }

    /**
     * Use the PostActionRelatedByContentId relation PostAction object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostActionQuery A secondary query class using the current class as primary query
     */
    public function usePostActionRelatedByContentIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPostActionRelatedByContentId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostActionRelatedByContentId', '\Database\HubPlus\PostActionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostConnector object
     *
     * @param \Database\HubPlus\PostConnector|ObjectCollection $postConnector the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByPostConnector($postConnector, $comparison = null)
    {
        if ($postConnector instanceof \Database\HubPlus\PostConnector) {
            return $this
                ->addUsingAlias(PostTableMap::COL_ID, $postConnector->getPostId(), $comparison);
        } elseif ($postConnector instanceof ObjectCollection) {
            return $this
                ->usePostConnectorQuery()
                ->filterByPrimaryKeys($postConnector->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostConnector() only accepts arguments of type \Database\HubPlus\PostConnector or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostConnector relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinPostConnector($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostConnector');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostConnector');
        }

        return $this;
    }

    /**
     * Use the PostConnector relation PostConnector object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostConnectorQuery A secondary query class using the current class as primary query
     */
    public function usePostConnectorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPostConnector($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostConnector', '\Database\HubPlus\PostConnectorQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostEvent object
     *
     * @param \Database\HubPlus\PostEvent|ObjectCollection $postEvent the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByPostEvent($postEvent, $comparison = null)
    {
        if ($postEvent instanceof \Database\HubPlus\PostEvent) {
            return $this
                ->addUsingAlias(PostTableMap::COL_ID, $postEvent->getPostId(), $comparison);
        } elseif ($postEvent instanceof ObjectCollection) {
            return $this
                ->usePostEventQuery()
                ->filterByPrimaryKeys($postEvent->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostEvent() only accepts arguments of type \Database\HubPlus\PostEvent or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostEvent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinPostEvent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostEvent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostEvent');
        }

        return $this;
    }

    /**
     * Use the PostEvent relation PostEvent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostEventQuery A secondary query class using the current class as primary query
     */
    public function usePostEventQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPostEvent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostEvent', '\Database\HubPlus\PostEventQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostExtraLog object
     *
     * @param \Database\HubPlus\PostExtraLog|ObjectCollection $postExtraLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByPostExtraLog($postExtraLog, $comparison = null)
    {
        if ($postExtraLog instanceof \Database\HubPlus\PostExtraLog) {
            return $this
                ->addUsingAlias(PostTableMap::COL_ID, $postExtraLog->getPostId(), $comparison);
        } elseif ($postExtraLog instanceof ObjectCollection) {
            return $this
                ->usePostExtraLogQuery()
                ->filterByPrimaryKeys($postExtraLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostExtraLog() only accepts arguments of type \Database\HubPlus\PostExtraLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostExtraLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinPostExtraLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostExtraLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostExtraLog');
        }

        return $this;
    }

    /**
     * Use the PostExtraLog relation PostExtraLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostExtraLogQuery A secondary query class using the current class as primary query
     */
    public function usePostExtraLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPostExtraLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostExtraLog', '\Database\HubPlus\PostExtraLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostLog object
     *
     * @param \Database\HubPlus\PostLog|ObjectCollection $postLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByPostLog($postLog, $comparison = null)
    {
        if ($postLog instanceof \Database\HubPlus\PostLog) {
            return $this
                ->addUsingAlias(PostTableMap::COL_ID, $postLog->getPostId(), $comparison);
        } elseif ($postLog instanceof ObjectCollection) {
            return $this
                ->usePostLogQuery()
                ->filterByPrimaryKeys($postLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostLog() only accepts arguments of type \Database\HubPlus\PostLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinPostLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostLog');
        }

        return $this;
    }

    /**
     * Use the PostLog relation PostLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostLogQuery A secondary query class using the current class as primary query
     */
    public function usePostLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPostLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostLog', '\Database\HubPlus\PostLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostPerson object
     *
     * @param \Database\HubPlus\PostPerson|ObjectCollection $postPerson the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByPostPerson($postPerson, $comparison = null)
    {
        if ($postPerson instanceof \Database\HubPlus\PostPerson) {
            return $this
                ->addUsingAlias(PostTableMap::COL_ID, $postPerson->getPostId(), $comparison);
        } elseif ($postPerson instanceof ObjectCollection) {
            return $this
                ->usePostPersonQuery()
                ->filterByPrimaryKeys($postPerson->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostPerson() only accepts arguments of type \Database\HubPlus\PostPerson or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostPerson relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinPostPerson($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostPerson');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostPerson');
        }

        return $this;
    }

    /**
     * Use the PostPerson relation PostPerson object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostPersonQuery A secondary query class using the current class as primary query
     */
    public function usePostPersonQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPostPerson($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostPerson', '\Database\HubPlus\PostPersonQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostPoi object
     *
     * @param \Database\HubPlus\PostPoi|ObjectCollection $postPoi the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByPostPoi($postPoi, $comparison = null)
    {
        if ($postPoi instanceof \Database\HubPlus\PostPoi) {
            return $this
                ->addUsingAlias(PostTableMap::COL_ID, $postPoi->getPostId(), $comparison);
        } elseif ($postPoi instanceof ObjectCollection) {
            return $this
                ->usePostPoiQuery()
                ->filterByPrimaryKeys($postPoi->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostPoi() only accepts arguments of type \Database\HubPlus\PostPoi or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostPoi relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinPostPoi($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostPoi');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostPoi');
        }

        return $this;
    }

    /**
     * Use the PostPoi relation PostPoi object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostPoiQuery A secondary query class using the current class as primary query
     */
    public function usePostPoiQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPostPoi($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostPoi', '\Database\HubPlus\PostPoiQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\PushNotification object
     *
     * @param \Database\HubPlus\PushNotification|ObjectCollection $pushNotification the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByPushNotification($pushNotification, $comparison = null)
    {
        if ($pushNotification instanceof \Database\HubPlus\PushNotification) {
            return $this
                ->addUsingAlias(PostTableMap::COL_ID, $pushNotification->getPostId(), $comparison);
        } elseif ($pushNotification instanceof ObjectCollection) {
            return $this
                ->usePushNotificationQuery()
                ->filterByPrimaryKeys($pushNotification->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPushNotification() only accepts arguments of type \Database\HubPlus\PushNotification or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PushNotification relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinPushNotification($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PushNotification');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PushNotification');
        }

        return $this;
    }

    /**
     * Use the PushNotification relation PushNotification object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PushNotificationQuery A secondary query class using the current class as primary query
     */
    public function usePushNotificationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPushNotification($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PushNotification', '\Database\HubPlus\PushNotificationQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Webhook object
     *
     * @param \Database\HubPlus\Webhook|ObjectCollection $webhook the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPostQuery The current query, for fluid interface
     */
    public function filterByWebhook($webhook, $comparison = null)
    {
        if ($webhook instanceof \Database\HubPlus\Webhook) {
            return $this
                ->addUsingAlias(PostTableMap::COL_ID, $webhook->getPostId(), $comparison);
        } elseif ($webhook instanceof ObjectCollection) {
            return $this
                ->useWebhookQuery()
                ->filterByPrimaryKeys($webhook->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWebhook() only accepts arguments of type \Database\HubPlus\Webhook or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Webhook relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function joinWebhook($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Webhook');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Webhook');
        }

        return $this;
    }

    /**
     * Use the Webhook relation Webhook object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\WebhookQuery A secondary query class using the current class as primary query
     */
    public function useWebhookQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinWebhook($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Webhook', '\Database\HubPlus\WebhookQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPost $post Object to remove from the list of results
     *
     * @return $this|ChildPostQuery The current query, for fluid interface
     */
    public function prune($post = null)
    {
        if ($post) {
            $this->addUsingAlias(PostTableMap::COL_ID, $post->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the post table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PostTableMap::clearInstancePool();
            PostTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PostTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PostTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PostTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildPostQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(PostTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildPostQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(PostTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildPostQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(PostTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildPostQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(PostTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildPostQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(PostTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildPostQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(PostTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildPostArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // PostQuery
