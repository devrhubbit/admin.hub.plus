<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\UserApplication as ChildUserApplication;
use Database\HubPlus\UserApplicationArchive as ChildUserApplicationArchive;
use Database\HubPlus\UserApplicationQuery as ChildUserApplicationQuery;
use Database\HubPlus\Map\UserApplicationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'user_application' table.
 *
 *
 *
 * @method     ChildUserApplicationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserApplicationQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildUserApplicationQuery orderByAppId($order = Criteria::ASC) Order by the app_id column
 * @method     ChildUserApplicationQuery orderByConnectedAt($order = Criteria::ASC) Order by the connected_at column
 * @method     ChildUserApplicationQuery orderByDelatedAt($order = Criteria::ASC) Order by the delated_at column
 * @method     ChildUserApplicationQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildUserApplicationQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildUserApplicationQuery groupById() Group by the id column
 * @method     ChildUserApplicationQuery groupByUserId() Group by the user_id column
 * @method     ChildUserApplicationQuery groupByAppId() Group by the app_id column
 * @method     ChildUserApplicationQuery groupByConnectedAt() Group by the connected_at column
 * @method     ChildUserApplicationQuery groupByDelatedAt() Group by the delated_at column
 * @method     ChildUserApplicationQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildUserApplicationQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildUserApplicationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserApplicationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserApplicationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserApplicationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserApplicationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserApplicationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserApplicationQuery leftJoinApplication($relationAlias = null) Adds a LEFT JOIN clause to the query using the Application relation
 * @method     ChildUserApplicationQuery rightJoinApplication($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Application relation
 * @method     ChildUserApplicationQuery innerJoinApplication($relationAlias = null) Adds a INNER JOIN clause to the query using the Application relation
 *
 * @method     ChildUserApplicationQuery joinWithApplication($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Application relation
 *
 * @method     ChildUserApplicationQuery leftJoinWithApplication() Adds a LEFT JOIN clause and with to the query using the Application relation
 * @method     ChildUserApplicationQuery rightJoinWithApplication() Adds a RIGHT JOIN clause and with to the query using the Application relation
 * @method     ChildUserApplicationQuery innerJoinWithApplication() Adds a INNER JOIN clause and with to the query using the Application relation
 *
 * @method     ChildUserApplicationQuery leftJoinUserBackend($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserBackend relation
 * @method     ChildUserApplicationQuery rightJoinUserBackend($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserBackend relation
 * @method     ChildUserApplicationQuery innerJoinUserBackend($relationAlias = null) Adds a INNER JOIN clause to the query using the UserBackend relation
 *
 * @method     ChildUserApplicationQuery joinWithUserBackend($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserBackend relation
 *
 * @method     ChildUserApplicationQuery leftJoinWithUserBackend() Adds a LEFT JOIN clause and with to the query using the UserBackend relation
 * @method     ChildUserApplicationQuery rightJoinWithUserBackend() Adds a RIGHT JOIN clause and with to the query using the UserBackend relation
 * @method     ChildUserApplicationQuery innerJoinWithUserBackend() Adds a INNER JOIN clause and with to the query using the UserBackend relation
 *
 * @method     \Database\HubPlus\ApplicationQuery|\Database\HubPlus\UserBackendQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUserApplication findOne(ConnectionInterface $con = null) Return the first ChildUserApplication matching the query
 * @method     ChildUserApplication findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserApplication matching the query, or a new ChildUserApplication object populated from the query conditions when no match is found
 *
 * @method     ChildUserApplication findOneById(int $id) Return the first ChildUserApplication filtered by the id column
 * @method     ChildUserApplication findOneByUserId(int $user_id) Return the first ChildUserApplication filtered by the user_id column
 * @method     ChildUserApplication findOneByAppId(int $app_id) Return the first ChildUserApplication filtered by the app_id column
 * @method     ChildUserApplication findOneByConnectedAt(string $connected_at) Return the first ChildUserApplication filtered by the connected_at column
 * @method     ChildUserApplication findOneByDelatedAt(string $delated_at) Return the first ChildUserApplication filtered by the delated_at column
 * @method     ChildUserApplication findOneByCreatedAt(string $created_at) Return the first ChildUserApplication filtered by the created_at column
 * @method     ChildUserApplication findOneByUpdatedAt(string $updated_at) Return the first ChildUserApplication filtered by the updated_at column *

 * @method     ChildUserApplication requirePk($key, ConnectionInterface $con = null) Return the ChildUserApplication by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplication requireOne(ConnectionInterface $con = null) Return the first ChildUserApplication matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserApplication requireOneById(int $id) Return the first ChildUserApplication filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplication requireOneByUserId(int $user_id) Return the first ChildUserApplication filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplication requireOneByAppId(int $app_id) Return the first ChildUserApplication filtered by the app_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplication requireOneByConnectedAt(string $connected_at) Return the first ChildUserApplication filtered by the connected_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplication requireOneByDelatedAt(string $delated_at) Return the first ChildUserApplication filtered by the delated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplication requireOneByCreatedAt(string $created_at) Return the first ChildUserApplication filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplication requireOneByUpdatedAt(string $updated_at) Return the first ChildUserApplication filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserApplication[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserApplication objects based on current ModelCriteria
 * @method     ChildUserApplication[]|ObjectCollection findById(int $id) Return ChildUserApplication objects filtered by the id column
 * @method     ChildUserApplication[]|ObjectCollection findByUserId(int $user_id) Return ChildUserApplication objects filtered by the user_id column
 * @method     ChildUserApplication[]|ObjectCollection findByAppId(int $app_id) Return ChildUserApplication objects filtered by the app_id column
 * @method     ChildUserApplication[]|ObjectCollection findByConnectedAt(string $connected_at) Return ChildUserApplication objects filtered by the connected_at column
 * @method     ChildUserApplication[]|ObjectCollection findByDelatedAt(string $delated_at) Return ChildUserApplication objects filtered by the delated_at column
 * @method     ChildUserApplication[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildUserApplication objects filtered by the created_at column
 * @method     ChildUserApplication[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildUserApplication objects filtered by the updated_at column
 * @method     ChildUserApplication[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserApplicationQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\UserApplicationQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\UserApplication', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserApplicationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserApplicationQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserApplicationQuery) {
            return $criteria;
        }
        $query = new ChildUserApplicationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserApplication|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserApplicationTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserApplicationTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserApplication A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, app_id, connected_at, delated_at, created_at, updated_at FROM user_application WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserApplication $obj */
            $obj = new ChildUserApplication();
            $obj->hydrate($row);
            UserApplicationTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserApplication|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserApplicationTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserApplicationTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUserBackend()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the app_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAppId(1234); // WHERE app_id = 1234
     * $query->filterByAppId(array(12, 34)); // WHERE app_id IN (12, 34)
     * $query->filterByAppId(array('min' => 12)); // WHERE app_id > 12
     * </code>
     *
     * @see       filterByApplication()
     *
     * @param     mixed $appId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function filterByAppId($appId = null, $comparison = null)
    {
        if (is_array($appId)) {
            $useMinMax = false;
            if (isset($appId['min'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_APP_ID, $appId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($appId['max'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_APP_ID, $appId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationTableMap::COL_APP_ID, $appId, $comparison);
    }

    /**
     * Filter the query on the connected_at column
     *
     * Example usage:
     * <code>
     * $query->filterByConnectedAt('2011-03-14'); // WHERE connected_at = '2011-03-14'
     * $query->filterByConnectedAt('now'); // WHERE connected_at = '2011-03-14'
     * $query->filterByConnectedAt(array('max' => 'yesterday')); // WHERE connected_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $connectedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function filterByConnectedAt($connectedAt = null, $comparison = null)
    {
        if (is_array($connectedAt)) {
            $useMinMax = false;
            if (isset($connectedAt['min'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_CONNECTED_AT, $connectedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($connectedAt['max'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_CONNECTED_AT, $connectedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationTableMap::COL_CONNECTED_AT, $connectedAt, $comparison);
    }

    /**
     * Filter the query on the delated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDelatedAt('2011-03-14'); // WHERE delated_at = '2011-03-14'
     * $query->filterByDelatedAt('now'); // WHERE delated_at = '2011-03-14'
     * $query->filterByDelatedAt(array('max' => 'yesterday')); // WHERE delated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $delatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function filterByDelatedAt($delatedAt = null, $comparison = null)
    {
        if (is_array($delatedAt)) {
            $useMinMax = false;
            if (isset($delatedAt['min'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_DELATED_AT, $delatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($delatedAt['max'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_DELATED_AT, $delatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationTableMap::COL_DELATED_AT, $delatedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(UserApplicationTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Application object
     *
     * @param \Database\HubPlus\Application|ObjectCollection $application The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserApplicationQuery The current query, for fluid interface
     */
    public function filterByApplication($application, $comparison = null)
    {
        if ($application instanceof \Database\HubPlus\Application) {
            return $this
                ->addUsingAlias(UserApplicationTableMap::COL_APP_ID, $application->getId(), $comparison);
        } elseif ($application instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserApplicationTableMap::COL_APP_ID, $application->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApplication() only accepts arguments of type \Database\HubPlus\Application or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Application relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function joinApplication($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Application');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Application');
        }

        return $this;
    }

    /**
     * Use the Application relation Application object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\ApplicationQuery A secondary query class using the current class as primary query
     */
    public function useApplicationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApplication($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Application', '\Database\HubPlus\ApplicationQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\UserBackend object
     *
     * @param \Database\HubPlus\UserBackend|ObjectCollection $userBackend The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserApplicationQuery The current query, for fluid interface
     */
    public function filterByUserBackend($userBackend, $comparison = null)
    {
        if ($userBackend instanceof \Database\HubPlus\UserBackend) {
            return $this
                ->addUsingAlias(UserApplicationTableMap::COL_USER_ID, $userBackend->getId(), $comparison);
        } elseif ($userBackend instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserApplicationTableMap::COL_USER_ID, $userBackend->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUserBackend() only accepts arguments of type \Database\HubPlus\UserBackend or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserBackend relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function joinUserBackend($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserBackend');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserBackend');
        }

        return $this;
    }

    /**
     * Use the UserBackend relation UserBackend object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\UserBackendQuery A secondary query class using the current class as primary query
     */
    public function useUserBackendQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserBackend($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserBackend', '\Database\HubPlus\UserBackendQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserApplication $userApplication Object to remove from the list of results
     *
     * @return $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function prune($userApplication = null)
    {
        if ($userApplication) {
            $this->addUsingAlias(UserApplicationTableMap::COL_ID, $userApplication->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the user_application table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserApplicationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserApplicationTableMap::clearInstancePool();
            UserApplicationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserApplicationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserApplicationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserApplicationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserApplicationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(UserApplicationTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(UserApplicationTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(UserApplicationTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(UserApplicationTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(UserApplicationTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildUserApplicationQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(UserApplicationTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildUserApplicationArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserApplicationTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // UserApplicationQuery
