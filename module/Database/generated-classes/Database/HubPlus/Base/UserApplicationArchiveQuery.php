<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\UserApplicationArchive as ChildUserApplicationArchive;
use Database\HubPlus\UserApplicationArchiveQuery as ChildUserApplicationArchiveQuery;
use Database\HubPlus\Map\UserApplicationArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'user_application_archive' table.
 *
 *
 *
 * @method     ChildUserApplicationArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserApplicationArchiveQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildUserApplicationArchiveQuery orderByAppId($order = Criteria::ASC) Order by the app_id column
 * @method     ChildUserApplicationArchiveQuery orderByConnectedAt($order = Criteria::ASC) Order by the connected_at column
 * @method     ChildUserApplicationArchiveQuery orderByDelatedAt($order = Criteria::ASC) Order by the delated_at column
 * @method     ChildUserApplicationArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildUserApplicationArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildUserApplicationArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildUserApplicationArchiveQuery groupById() Group by the id column
 * @method     ChildUserApplicationArchiveQuery groupByUserId() Group by the user_id column
 * @method     ChildUserApplicationArchiveQuery groupByAppId() Group by the app_id column
 * @method     ChildUserApplicationArchiveQuery groupByConnectedAt() Group by the connected_at column
 * @method     ChildUserApplicationArchiveQuery groupByDelatedAt() Group by the delated_at column
 * @method     ChildUserApplicationArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildUserApplicationArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildUserApplicationArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildUserApplicationArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserApplicationArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserApplicationArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserApplicationArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserApplicationArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserApplicationArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserApplicationArchive findOne(ConnectionInterface $con = null) Return the first ChildUserApplicationArchive matching the query
 * @method     ChildUserApplicationArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserApplicationArchive matching the query, or a new ChildUserApplicationArchive object populated from the query conditions when no match is found
 *
 * @method     ChildUserApplicationArchive findOneById(int $id) Return the first ChildUserApplicationArchive filtered by the id column
 * @method     ChildUserApplicationArchive findOneByUserId(int $user_id) Return the first ChildUserApplicationArchive filtered by the user_id column
 * @method     ChildUserApplicationArchive findOneByAppId(int $app_id) Return the first ChildUserApplicationArchive filtered by the app_id column
 * @method     ChildUserApplicationArchive findOneByConnectedAt(string $connected_at) Return the first ChildUserApplicationArchive filtered by the connected_at column
 * @method     ChildUserApplicationArchive findOneByDelatedAt(string $delated_at) Return the first ChildUserApplicationArchive filtered by the delated_at column
 * @method     ChildUserApplicationArchive findOneByCreatedAt(string $created_at) Return the first ChildUserApplicationArchive filtered by the created_at column
 * @method     ChildUserApplicationArchive findOneByUpdatedAt(string $updated_at) Return the first ChildUserApplicationArchive filtered by the updated_at column
 * @method     ChildUserApplicationArchive findOneByArchivedAt(string $archived_at) Return the first ChildUserApplicationArchive filtered by the archived_at column *

 * @method     ChildUserApplicationArchive requirePk($key, ConnectionInterface $con = null) Return the ChildUserApplicationArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplicationArchive requireOne(ConnectionInterface $con = null) Return the first ChildUserApplicationArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserApplicationArchive requireOneById(int $id) Return the first ChildUserApplicationArchive filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplicationArchive requireOneByUserId(int $user_id) Return the first ChildUserApplicationArchive filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplicationArchive requireOneByAppId(int $app_id) Return the first ChildUserApplicationArchive filtered by the app_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplicationArchive requireOneByConnectedAt(string $connected_at) Return the first ChildUserApplicationArchive filtered by the connected_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplicationArchive requireOneByDelatedAt(string $delated_at) Return the first ChildUserApplicationArchive filtered by the delated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplicationArchive requireOneByCreatedAt(string $created_at) Return the first ChildUserApplicationArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplicationArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildUserApplicationArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserApplicationArchive requireOneByArchivedAt(string $archived_at) Return the first ChildUserApplicationArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserApplicationArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserApplicationArchive objects based on current ModelCriteria
 * @method     ChildUserApplicationArchive[]|ObjectCollection findById(int $id) Return ChildUserApplicationArchive objects filtered by the id column
 * @method     ChildUserApplicationArchive[]|ObjectCollection findByUserId(int $user_id) Return ChildUserApplicationArchive objects filtered by the user_id column
 * @method     ChildUserApplicationArchive[]|ObjectCollection findByAppId(int $app_id) Return ChildUserApplicationArchive objects filtered by the app_id column
 * @method     ChildUserApplicationArchive[]|ObjectCollection findByConnectedAt(string $connected_at) Return ChildUserApplicationArchive objects filtered by the connected_at column
 * @method     ChildUserApplicationArchive[]|ObjectCollection findByDelatedAt(string $delated_at) Return ChildUserApplicationArchive objects filtered by the delated_at column
 * @method     ChildUserApplicationArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildUserApplicationArchive objects filtered by the created_at column
 * @method     ChildUserApplicationArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildUserApplicationArchive objects filtered by the updated_at column
 * @method     ChildUserApplicationArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildUserApplicationArchive objects filtered by the archived_at column
 * @method     ChildUserApplicationArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserApplicationArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\UserApplicationArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\UserApplicationArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserApplicationArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserApplicationArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserApplicationArchiveQuery) {
            return $criteria;
        }
        $query = new ChildUserApplicationArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserApplicationArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserApplicationArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserApplicationArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserApplicationArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, app_id, connected_at, delated_at, created_at, updated_at, archived_at FROM user_application_archive WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserApplicationArchive $obj */
            $obj = new ChildUserApplicationArchive();
            $obj->hydrate($row);
            UserApplicationArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserApplicationArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserApplicationArchiveTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserApplicationArchiveTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationArchiveTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationArchiveTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the app_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAppId(1234); // WHERE app_id = 1234
     * $query->filterByAppId(array(12, 34)); // WHERE app_id IN (12, 34)
     * $query->filterByAppId(array('min' => 12)); // WHERE app_id > 12
     * </code>
     *
     * @param     mixed $appId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByAppId($appId = null, $comparison = null)
    {
        if (is_array($appId)) {
            $useMinMax = false;
            if (isset($appId['min'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_APP_ID, $appId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($appId['max'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_APP_ID, $appId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationArchiveTableMap::COL_APP_ID, $appId, $comparison);
    }

    /**
     * Filter the query on the connected_at column
     *
     * Example usage:
     * <code>
     * $query->filterByConnectedAt('2011-03-14'); // WHERE connected_at = '2011-03-14'
     * $query->filterByConnectedAt('now'); // WHERE connected_at = '2011-03-14'
     * $query->filterByConnectedAt(array('max' => 'yesterday')); // WHERE connected_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $connectedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByConnectedAt($connectedAt = null, $comparison = null)
    {
        if (is_array($connectedAt)) {
            $useMinMax = false;
            if (isset($connectedAt['min'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_CONNECTED_AT, $connectedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($connectedAt['max'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_CONNECTED_AT, $connectedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationArchiveTableMap::COL_CONNECTED_AT, $connectedAt, $comparison);
    }

    /**
     * Filter the query on the delated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDelatedAt('2011-03-14'); // WHERE delated_at = '2011-03-14'
     * $query->filterByDelatedAt('now'); // WHERE delated_at = '2011-03-14'
     * $query->filterByDelatedAt(array('max' => 'yesterday')); // WHERE delated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $delatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByDelatedAt($delatedAt = null, $comparison = null)
    {
        if (is_array($delatedAt)) {
            $useMinMax = false;
            if (isset($delatedAt['min'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_DELATED_AT, $delatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($delatedAt['max'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_DELATED_AT, $delatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationArchiveTableMap::COL_DELATED_AT, $delatedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserApplicationArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(UserApplicationArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserApplicationArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserApplicationArchive $userApplicationArchive Object to remove from the list of results
     *
     * @return $this|ChildUserApplicationArchiveQuery The current query, for fluid interface
     */
    public function prune($userApplicationArchive = null)
    {
        if ($userApplicationArchive) {
            $this->addUsingAlias(UserApplicationArchiveTableMap::COL_ID, $userApplicationArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the user_application_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserApplicationArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserApplicationArchiveTableMap::clearInstancePool();
            UserApplicationArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserApplicationArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserApplicationArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserApplicationArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserApplicationArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UserApplicationArchiveQuery
