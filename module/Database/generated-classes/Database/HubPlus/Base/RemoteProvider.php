<?php

namespace Database\HubPlus\Base;

use \DateTime;
use \Exception;
use \PDO;
use Database\HubPlus\PostConnector as ChildPostConnector;
use Database\HubPlus\PostConnectorQuery as ChildPostConnectorQuery;
use Database\HubPlus\RemoteProvider as ChildRemoteProvider;
use Database\HubPlus\RemoteProviderArchive as ChildRemoteProviderArchive;
use Database\HubPlus\RemoteProviderArchiveQuery as ChildRemoteProviderArchiveQuery;
use Database\HubPlus\RemoteProviderQuery as ChildRemoteProviderQuery;
use Database\HubPlus\SectionConnector as ChildSectionConnector;
use Database\HubPlus\SectionConnectorQuery as ChildSectionConnectorQuery;
use Database\HubPlus\Webhook as ChildWebhook;
use Database\HubPlus\WebhookQuery as ChildWebhookQuery;
use Database\HubPlus\Map\PostConnectorTableMap;
use Database\HubPlus\Map\RemoteProviderTableMap;
use Database\HubPlus\Map\SectionConnectorTableMap;
use Database\HubPlus\Map\WebhookTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'remote_provider' table.
 *
 *
 *
 * @package    propel.generator.Database.HubPlus.Base
 */
abstract class RemoteProvider implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Database\\HubPlus\\Map\\RemoteProviderTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the uuid field.
     *
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $uuid;

    /**
     * The value for the email field.
     *
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $email;

    /**
     * The value for the email_canonical field.
     *
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $email_canonical;

    /**
     * The value for the name field.
     *
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $name;

    /**
     * The value for the slug field.
     *
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $slug;

    /**
     * The value for the icon field.
     *
     * @var        string
     */
    protected $icon;

    /**
     * The value for the site field.
     *
     * @var        string
     */
    protected $site;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ObjectCollection|ChildPostConnector[] Collection to store aggregation of ChildPostConnector objects.
     */
    protected $collPostConnectors;
    protected $collPostConnectorsPartial;

    /**
     * @var        ObjectCollection|ChildSectionConnector[] Collection to store aggregation of ChildSectionConnector objects.
     */
    protected $collSectionConnectors;
    protected $collSectionConnectorsPartial;

    /**
     * @var        ObjectCollection|ChildWebhook[] Collection to store aggregation of ChildWebhook objects.
     */
    protected $collWebhooks;
    protected $collWebhooksPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostConnector[]
     */
    protected $postConnectorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSectionConnector[]
     */
    protected $sectionConnectorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildWebhook[]
     */
    protected $webhooksScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->uuid = '';
        $this->email = '';
        $this->email_canonical = '';
        $this->name = '';
        $this->slug = '';
    }

    /**
     * Initializes internal state of Database\HubPlus\Base\RemoteProvider object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>RemoteProvider</code> instance.  If
     * <code>obj</code> is an instance of <code>RemoteProvider</code>, delegates to
     * <code>equals(RemoteProvider)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|RemoteProvider The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [uuid] column value.
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [email_canonical] column value.
     *
     * @return string
     */
    public function getEmailCanonical()
    {
        return $this->email_canonical;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [slug] column value.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get the [icon] column value.
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Get the [site] column value.
     *
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[RemoteProviderTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [uuid] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function setUuid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uuid !== $v) {
            $this->uuid = $v;
            $this->modifiedColumns[RemoteProviderTableMap::COL_UUID] = true;
        }

        return $this;
    } // setUuid()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[RemoteProviderTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [email_canonical] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function setEmailCanonical($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email_canonical !== $v) {
            $this->email_canonical = $v;
            $this->modifiedColumns[RemoteProviderTableMap::COL_EMAIL_CANONICAL] = true;
        }

        return $this;
    } // setEmailCanonical()

    /**
     * Set the value of [name] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[RemoteProviderTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [slug] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function setSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->slug !== $v) {
            $this->slug = $v;
            $this->modifiedColumns[RemoteProviderTableMap::COL_SLUG] = true;
        }

        return $this;
    } // setSlug()

    /**
     * Set the value of [icon] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function setIcon($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->icon !== $v) {
            $this->icon = $v;
            $this->modifiedColumns[RemoteProviderTableMap::COL_ICON] = true;
        }

        return $this;
    } // setIcon()

    /**
     * Set the value of [site] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function setSite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->site !== $v) {
            $this->site = $v;
            $this->modifiedColumns[RemoteProviderTableMap::COL_SITE] = true;
        }

        return $this;
    } // setSite()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[RemoteProviderTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[RemoteProviderTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[RemoteProviderTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->uuid !== '') {
                return false;
            }

            if ($this->email !== '') {
                return false;
            }

            if ($this->email_canonical !== '') {
                return false;
            }

            if ($this->name !== '') {
                return false;
            }

            if ($this->slug !== '') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : RemoteProviderTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : RemoteProviderTableMap::translateFieldName('Uuid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uuid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : RemoteProviderTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : RemoteProviderTableMap::translateFieldName('EmailCanonical', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email_canonical = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : RemoteProviderTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : RemoteProviderTableMap::translateFieldName('Slug', TableMap::TYPE_PHPNAME, $indexType)];
            $this->slug = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : RemoteProviderTableMap::translateFieldName('Icon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->icon = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : RemoteProviderTableMap::translateFieldName('Site', TableMap::TYPE_PHPNAME, $indexType)];
            $this->site = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : RemoteProviderTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : RemoteProviderTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : RemoteProviderTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 11; // 11 = RemoteProviderTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Database\\HubPlus\\RemoteProvider'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RemoteProviderTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildRemoteProviderQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collPostConnectors = null;

            $this->collSectionConnectors = null;

            $this->collWebhooks = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see RemoteProvider::setDeleted()
     * @see RemoteProvider::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteProviderTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildRemoteProviderQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildRemoteProviderQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteProviderTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(RemoteProviderTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(RemoteProviderTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(RemoteProviderTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                RemoteProviderTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->postConnectorsScheduledForDeletion !== null) {
                if (!$this->postConnectorsScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\PostConnectorQuery::create()
                        ->filterByPrimaryKeys($this->postConnectorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->postConnectorsScheduledForDeletion = null;
                }
            }

            if ($this->collPostConnectors !== null) {
                foreach ($this->collPostConnectors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sectionConnectorsScheduledForDeletion !== null) {
                if (!$this->sectionConnectorsScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\SectionConnectorQuery::create()
                        ->filterByPrimaryKeys($this->sectionConnectorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sectionConnectorsScheduledForDeletion = null;
                }
            }

            if ($this->collSectionConnectors !== null) {
                foreach ($this->collSectionConnectors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->webhooksScheduledForDeletion !== null) {
                if (!$this->webhooksScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\WebhookQuery::create()
                        ->filterByPrimaryKeys($this->webhooksScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->webhooksScheduledForDeletion = null;
                }
            }

            if ($this->collWebhooks !== null) {
                foreach ($this->collWebhooks as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[RemoteProviderTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . RemoteProviderTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(RemoteProviderTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_UUID)) {
            $modifiedColumns[':p' . $index++]  = 'uuid';
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'email';
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_EMAIL_CANONICAL)) {
            $modifiedColumns[':p' . $index++]  = 'email_canonical';
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_SLUG)) {
            $modifiedColumns[':p' . $index++]  = 'slug';
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_ICON)) {
            $modifiedColumns[':p' . $index++]  = 'icon';
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_SITE)) {
            $modifiedColumns[':p' . $index++]  = 'site';
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO remote_provider (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'uuid':
                        $stmt->bindValue($identifier, $this->uuid, PDO::PARAM_STR);
                        break;
                    case 'email':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case 'email_canonical':
                        $stmt->bindValue($identifier, $this->email_canonical, PDO::PARAM_STR);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'slug':
                        $stmt->bindValue($identifier, $this->slug, PDO::PARAM_STR);
                        break;
                    case 'icon':
                        $stmt->bindValue($identifier, $this->icon, PDO::PARAM_STR);
                        break;
                    case 'site':
                        $stmt->bindValue($identifier, $this->site, PDO::PARAM_STR);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = RemoteProviderTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUuid();
                break;
            case 2:
                return $this->getEmail();
                break;
            case 3:
                return $this->getEmailCanonical();
                break;
            case 4:
                return $this->getName();
                break;
            case 5:
                return $this->getSlug();
                break;
            case 6:
                return $this->getIcon();
                break;
            case 7:
                return $this->getSite();
                break;
            case 8:
                return $this->getDeletedAt();
                break;
            case 9:
                return $this->getCreatedAt();
                break;
            case 10:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['RemoteProvider'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['RemoteProvider'][$this->hashCode()] = true;
        $keys = RemoteProviderTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUuid(),
            $keys[2] => $this->getEmail(),
            $keys[3] => $this->getEmailCanonical(),
            $keys[4] => $this->getName(),
            $keys[5] => $this->getSlug(),
            $keys[6] => $this->getIcon(),
            $keys[7] => $this->getSite(),
            $keys[8] => $this->getDeletedAt(),
            $keys[9] => $this->getCreatedAt(),
            $keys[10] => $this->getUpdatedAt(),
        );
        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        if ($result[$keys[9]] instanceof \DateTimeInterface) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collPostConnectors) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postConnectors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_connectors';
                        break;
                    default:
                        $key = 'PostConnectors';
                }

                $result[$key] = $this->collPostConnectors->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSectionConnectors) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sectionConnectors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'section_connectors';
                        break;
                    default:
                        $key = 'SectionConnectors';
                }

                $result[$key] = $this->collSectionConnectors->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWebhooks) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'webhooks';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'webhooks';
                        break;
                    default:
                        $key = 'Webhooks';
                }

                $result[$key] = $this->collWebhooks->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Database\HubPlus\RemoteProvider
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = RemoteProviderTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Database\HubPlus\RemoteProvider
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUuid($value);
                break;
            case 2:
                $this->setEmail($value);
                break;
            case 3:
                $this->setEmailCanonical($value);
                break;
            case 4:
                $this->setName($value);
                break;
            case 5:
                $this->setSlug($value);
                break;
            case 6:
                $this->setIcon($value);
                break;
            case 7:
                $this->setSite($value);
                break;
            case 8:
                $this->setDeletedAt($value);
                break;
            case 9:
                $this->setCreatedAt($value);
                break;
            case 10:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = RemoteProviderTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUuid($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setEmail($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setEmailCanonical($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setName($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setSlug($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIcon($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setSite($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setDeletedAt($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setCreatedAt($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setUpdatedAt($arr[$keys[10]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Database\HubPlus\RemoteProvider The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(RemoteProviderTableMap::DATABASE_NAME);

        if ($this->isColumnModified(RemoteProviderTableMap::COL_ID)) {
            $criteria->add(RemoteProviderTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_UUID)) {
            $criteria->add(RemoteProviderTableMap::COL_UUID, $this->uuid);
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_EMAIL)) {
            $criteria->add(RemoteProviderTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_EMAIL_CANONICAL)) {
            $criteria->add(RemoteProviderTableMap::COL_EMAIL_CANONICAL, $this->email_canonical);
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_NAME)) {
            $criteria->add(RemoteProviderTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_SLUG)) {
            $criteria->add(RemoteProviderTableMap::COL_SLUG, $this->slug);
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_ICON)) {
            $criteria->add(RemoteProviderTableMap::COL_ICON, $this->icon);
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_SITE)) {
            $criteria->add(RemoteProviderTableMap::COL_SITE, $this->site);
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_DELETED_AT)) {
            $criteria->add(RemoteProviderTableMap::COL_DELETED_AT, $this->deleted_at);
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_CREATED_AT)) {
            $criteria->add(RemoteProviderTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(RemoteProviderTableMap::COL_UPDATED_AT)) {
            $criteria->add(RemoteProviderTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildRemoteProviderQuery::create();
        $criteria->add(RemoteProviderTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Database\HubPlus\RemoteProvider (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUuid($this->getUuid());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setEmailCanonical($this->getEmailCanonical());
        $copyObj->setName($this->getName());
        $copyObj->setSlug($this->getSlug());
        $copyObj->setIcon($this->getIcon());
        $copyObj->setSite($this->getSite());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getPostConnectors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostConnector($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSectionConnectors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSectionConnector($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWebhooks() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWebhook($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Database\HubPlus\RemoteProvider Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('PostConnector' == $relationName) {
            $this->initPostConnectors();
            return;
        }
        if ('SectionConnector' == $relationName) {
            $this->initSectionConnectors();
            return;
        }
        if ('Webhook' == $relationName) {
            $this->initWebhooks();
            return;
        }
    }

    /**
     * Clears out the collPostConnectors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostConnectors()
     */
    public function clearPostConnectors()
    {
        $this->collPostConnectors = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostConnectors collection loaded partially.
     */
    public function resetPartialPostConnectors($v = true)
    {
        $this->collPostConnectorsPartial = $v;
    }

    /**
     * Initializes the collPostConnectors collection.
     *
     * By default this just sets the collPostConnectors collection to an empty array (like clearcollPostConnectors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostConnectors($overrideExisting = true)
    {
        if (null !== $this->collPostConnectors && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostConnectorTableMap::getTableMap()->getCollectionClassName();

        $this->collPostConnectors = new $collectionClassName;
        $this->collPostConnectors->setModel('\Database\HubPlus\PostConnector');
    }

    /**
     * Gets an array of ChildPostConnector objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildRemoteProvider is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostConnector[] List of ChildPostConnector objects
     * @throws PropelException
     */
    public function getPostConnectors(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostConnectorsPartial && !$this->isNew();
        if (null === $this->collPostConnectors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostConnectors) {
                // return empty collection
                $this->initPostConnectors();
            } else {
                $collPostConnectors = ChildPostConnectorQuery::create(null, $criteria)
                    ->filterByRemoteProvider($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostConnectorsPartial && count($collPostConnectors)) {
                        $this->initPostConnectors(false);

                        foreach ($collPostConnectors as $obj) {
                            if (false == $this->collPostConnectors->contains($obj)) {
                                $this->collPostConnectors->append($obj);
                            }
                        }

                        $this->collPostConnectorsPartial = true;
                    }

                    return $collPostConnectors;
                }

                if ($partial && $this->collPostConnectors) {
                    foreach ($this->collPostConnectors as $obj) {
                        if ($obj->isNew()) {
                            $collPostConnectors[] = $obj;
                        }
                    }
                }

                $this->collPostConnectors = $collPostConnectors;
                $this->collPostConnectorsPartial = false;
            }
        }

        return $this->collPostConnectors;
    }

    /**
     * Sets a collection of ChildPostConnector objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postConnectors A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildRemoteProvider The current object (for fluent API support)
     */
    public function setPostConnectors(Collection $postConnectors, ConnectionInterface $con = null)
    {
        /** @var ChildPostConnector[] $postConnectorsToDelete */
        $postConnectorsToDelete = $this->getPostConnectors(new Criteria(), $con)->diff($postConnectors);


        $this->postConnectorsScheduledForDeletion = $postConnectorsToDelete;

        foreach ($postConnectorsToDelete as $postConnectorRemoved) {
            $postConnectorRemoved->setRemoteProvider(null);
        }

        $this->collPostConnectors = null;
        foreach ($postConnectors as $postConnector) {
            $this->addPostConnector($postConnector);
        }

        $this->collPostConnectors = $postConnectors;
        $this->collPostConnectorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostConnector objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostConnector objects.
     * @throws PropelException
     */
    public function countPostConnectors(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostConnectorsPartial && !$this->isNew();
        if (null === $this->collPostConnectors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostConnectors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostConnectors());
            }

            $query = ChildPostConnectorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRemoteProvider($this)
                ->count($con);
        }

        return count($this->collPostConnectors);
    }

    /**
     * Method called to associate a ChildPostConnector object to this object
     * through the ChildPostConnector foreign key attribute.
     *
     * @param  ChildPostConnector $l ChildPostConnector
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function addPostConnector(ChildPostConnector $l)
    {
        if ($this->collPostConnectors === null) {
            $this->initPostConnectors();
            $this->collPostConnectorsPartial = true;
        }

        if (!$this->collPostConnectors->contains($l)) {
            $this->doAddPostConnector($l);

            if ($this->postConnectorsScheduledForDeletion and $this->postConnectorsScheduledForDeletion->contains($l)) {
                $this->postConnectorsScheduledForDeletion->remove($this->postConnectorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostConnector $postConnector The ChildPostConnector object to add.
     */
    protected function doAddPostConnector(ChildPostConnector $postConnector)
    {
        $this->collPostConnectors[]= $postConnector;
        $postConnector->setRemoteProvider($this);
    }

    /**
     * @param  ChildPostConnector $postConnector The ChildPostConnector object to remove.
     * @return $this|ChildRemoteProvider The current object (for fluent API support)
     */
    public function removePostConnector(ChildPostConnector $postConnector)
    {
        if ($this->getPostConnectors()->contains($postConnector)) {
            $pos = $this->collPostConnectors->search($postConnector);
            $this->collPostConnectors->remove($pos);
            if (null === $this->postConnectorsScheduledForDeletion) {
                $this->postConnectorsScheduledForDeletion = clone $this->collPostConnectors;
                $this->postConnectorsScheduledForDeletion->clear();
            }
            $this->postConnectorsScheduledForDeletion[]= clone $postConnector;
            $postConnector->setRemoteProvider(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RemoteProvider is new, it will return
     * an empty collection; or if this RemoteProvider has previously
     * been saved, it will retrieve related PostConnectors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RemoteProvider.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostConnector[] List of ChildPostConnector objects
     */
    public function getPostConnectorsJoinPost(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostConnectorQuery::create(null, $criteria);
        $query->joinWith('Post', $joinBehavior);

        return $this->getPostConnectors($query, $con);
    }

    /**
     * Clears out the collSectionConnectors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSectionConnectors()
     */
    public function clearSectionConnectors()
    {
        $this->collSectionConnectors = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSectionConnectors collection loaded partially.
     */
    public function resetPartialSectionConnectors($v = true)
    {
        $this->collSectionConnectorsPartial = $v;
    }

    /**
     * Initializes the collSectionConnectors collection.
     *
     * By default this just sets the collSectionConnectors collection to an empty array (like clearcollSectionConnectors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSectionConnectors($overrideExisting = true)
    {
        if (null !== $this->collSectionConnectors && !$overrideExisting) {
            return;
        }

        $collectionClassName = SectionConnectorTableMap::getTableMap()->getCollectionClassName();

        $this->collSectionConnectors = new $collectionClassName;
        $this->collSectionConnectors->setModel('\Database\HubPlus\SectionConnector');
    }

    /**
     * Gets an array of ChildSectionConnector objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildRemoteProvider is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSectionConnector[] List of ChildSectionConnector objects
     * @throws PropelException
     */
    public function getSectionConnectors(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionConnectorsPartial && !$this->isNew();
        if (null === $this->collSectionConnectors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSectionConnectors) {
                // return empty collection
                $this->initSectionConnectors();
            } else {
                $collSectionConnectors = ChildSectionConnectorQuery::create(null, $criteria)
                    ->filterByRemoteProvider($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSectionConnectorsPartial && count($collSectionConnectors)) {
                        $this->initSectionConnectors(false);

                        foreach ($collSectionConnectors as $obj) {
                            if (false == $this->collSectionConnectors->contains($obj)) {
                                $this->collSectionConnectors->append($obj);
                            }
                        }

                        $this->collSectionConnectorsPartial = true;
                    }

                    return $collSectionConnectors;
                }

                if ($partial && $this->collSectionConnectors) {
                    foreach ($this->collSectionConnectors as $obj) {
                        if ($obj->isNew()) {
                            $collSectionConnectors[] = $obj;
                        }
                    }
                }

                $this->collSectionConnectors = $collSectionConnectors;
                $this->collSectionConnectorsPartial = false;
            }
        }

        return $this->collSectionConnectors;
    }

    /**
     * Sets a collection of ChildSectionConnector objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sectionConnectors A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildRemoteProvider The current object (for fluent API support)
     */
    public function setSectionConnectors(Collection $sectionConnectors, ConnectionInterface $con = null)
    {
        /** @var ChildSectionConnector[] $sectionConnectorsToDelete */
        $sectionConnectorsToDelete = $this->getSectionConnectors(new Criteria(), $con)->diff($sectionConnectors);


        $this->sectionConnectorsScheduledForDeletion = $sectionConnectorsToDelete;

        foreach ($sectionConnectorsToDelete as $sectionConnectorRemoved) {
            $sectionConnectorRemoved->setRemoteProvider(null);
        }

        $this->collSectionConnectors = null;
        foreach ($sectionConnectors as $sectionConnector) {
            $this->addSectionConnector($sectionConnector);
        }

        $this->collSectionConnectors = $sectionConnectors;
        $this->collSectionConnectorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SectionConnector objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SectionConnector objects.
     * @throws PropelException
     */
    public function countSectionConnectors(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionConnectorsPartial && !$this->isNew();
        if (null === $this->collSectionConnectors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSectionConnectors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSectionConnectors());
            }

            $query = ChildSectionConnectorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRemoteProvider($this)
                ->count($con);
        }

        return count($this->collSectionConnectors);
    }

    /**
     * Method called to associate a ChildSectionConnector object to this object
     * through the ChildSectionConnector foreign key attribute.
     *
     * @param  ChildSectionConnector $l ChildSectionConnector
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function addSectionConnector(ChildSectionConnector $l)
    {
        if ($this->collSectionConnectors === null) {
            $this->initSectionConnectors();
            $this->collSectionConnectorsPartial = true;
        }

        if (!$this->collSectionConnectors->contains($l)) {
            $this->doAddSectionConnector($l);

            if ($this->sectionConnectorsScheduledForDeletion and $this->sectionConnectorsScheduledForDeletion->contains($l)) {
                $this->sectionConnectorsScheduledForDeletion->remove($this->sectionConnectorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSectionConnector $sectionConnector The ChildSectionConnector object to add.
     */
    protected function doAddSectionConnector(ChildSectionConnector $sectionConnector)
    {
        $this->collSectionConnectors[]= $sectionConnector;
        $sectionConnector->setRemoteProvider($this);
    }

    /**
     * @param  ChildSectionConnector $sectionConnector The ChildSectionConnector object to remove.
     * @return $this|ChildRemoteProvider The current object (for fluent API support)
     */
    public function removeSectionConnector(ChildSectionConnector $sectionConnector)
    {
        if ($this->getSectionConnectors()->contains($sectionConnector)) {
            $pos = $this->collSectionConnectors->search($sectionConnector);
            $this->collSectionConnectors->remove($pos);
            if (null === $this->sectionConnectorsScheduledForDeletion) {
                $this->sectionConnectorsScheduledForDeletion = clone $this->collSectionConnectors;
                $this->sectionConnectorsScheduledForDeletion->clear();
            }
            $this->sectionConnectorsScheduledForDeletion[]= clone $sectionConnector;
            $sectionConnector->setRemoteProvider(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RemoteProvider is new, it will return
     * an empty collection; or if this RemoteProvider has previously
     * been saved, it will retrieve related SectionConnectors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RemoteProvider.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSectionConnector[] List of ChildSectionConnector objects
     */
    public function getSectionConnectorsJoinSectionRelatedBySectionId(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSectionConnectorQuery::create(null, $criteria);
        $query->joinWith('SectionRelatedBySectionId', $joinBehavior);

        return $this->getSectionConnectors($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RemoteProvider is new, it will return
     * an empty collection; or if this RemoteProvider has previously
     * been saved, it will retrieve related SectionConnectors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RemoteProvider.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSectionConnector[] List of ChildSectionConnector objects
     */
    public function getSectionConnectorsJoinSectionRelatedByRemoteSectionId(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSectionConnectorQuery::create(null, $criteria);
        $query->joinWith('SectionRelatedByRemoteSectionId', $joinBehavior);

        return $this->getSectionConnectors($query, $con);
    }

    /**
     * Clears out the collWebhooks collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addWebhooks()
     */
    public function clearWebhooks()
    {
        $this->collWebhooks = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collWebhooks collection loaded partially.
     */
    public function resetPartialWebhooks($v = true)
    {
        $this->collWebhooksPartial = $v;
    }

    /**
     * Initializes the collWebhooks collection.
     *
     * By default this just sets the collWebhooks collection to an empty array (like clearcollWebhooks());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWebhooks($overrideExisting = true)
    {
        if (null !== $this->collWebhooks && !$overrideExisting) {
            return;
        }

        $collectionClassName = WebhookTableMap::getTableMap()->getCollectionClassName();

        $this->collWebhooks = new $collectionClassName;
        $this->collWebhooks->setModel('\Database\HubPlus\Webhook');
    }

    /**
     * Gets an array of ChildWebhook objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildRemoteProvider is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildWebhook[] List of ChildWebhook objects
     * @throws PropelException
     */
    public function getWebhooks(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collWebhooksPartial && !$this->isNew();
        if (null === $this->collWebhooks || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWebhooks) {
                // return empty collection
                $this->initWebhooks();
            } else {
                $collWebhooks = ChildWebhookQuery::create(null, $criteria)
                    ->filterByRemoteProvider($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collWebhooksPartial && count($collWebhooks)) {
                        $this->initWebhooks(false);

                        foreach ($collWebhooks as $obj) {
                            if (false == $this->collWebhooks->contains($obj)) {
                                $this->collWebhooks->append($obj);
                            }
                        }

                        $this->collWebhooksPartial = true;
                    }

                    return $collWebhooks;
                }

                if ($partial && $this->collWebhooks) {
                    foreach ($this->collWebhooks as $obj) {
                        if ($obj->isNew()) {
                            $collWebhooks[] = $obj;
                        }
                    }
                }

                $this->collWebhooks = $collWebhooks;
                $this->collWebhooksPartial = false;
            }
        }

        return $this->collWebhooks;
    }

    /**
     * Sets a collection of ChildWebhook objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $webhooks A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildRemoteProvider The current object (for fluent API support)
     */
    public function setWebhooks(Collection $webhooks, ConnectionInterface $con = null)
    {
        /** @var ChildWebhook[] $webhooksToDelete */
        $webhooksToDelete = $this->getWebhooks(new Criteria(), $con)->diff($webhooks);


        $this->webhooksScheduledForDeletion = $webhooksToDelete;

        foreach ($webhooksToDelete as $webhookRemoved) {
            $webhookRemoved->setRemoteProvider(null);
        }

        $this->collWebhooks = null;
        foreach ($webhooks as $webhook) {
            $this->addWebhook($webhook);
        }

        $this->collWebhooks = $webhooks;
        $this->collWebhooksPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Webhook objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Webhook objects.
     * @throws PropelException
     */
    public function countWebhooks(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collWebhooksPartial && !$this->isNew();
        if (null === $this->collWebhooks || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWebhooks) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getWebhooks());
            }

            $query = ChildWebhookQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRemoteProvider($this)
                ->count($con);
        }

        return count($this->collWebhooks);
    }

    /**
     * Method called to associate a ChildWebhook object to this object
     * through the ChildWebhook foreign key attribute.
     *
     * @param  ChildWebhook $l ChildWebhook
     * @return $this|\Database\HubPlus\RemoteProvider The current object (for fluent API support)
     */
    public function addWebhook(ChildWebhook $l)
    {
        if ($this->collWebhooks === null) {
            $this->initWebhooks();
            $this->collWebhooksPartial = true;
        }

        if (!$this->collWebhooks->contains($l)) {
            $this->doAddWebhook($l);

            if ($this->webhooksScheduledForDeletion and $this->webhooksScheduledForDeletion->contains($l)) {
                $this->webhooksScheduledForDeletion->remove($this->webhooksScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildWebhook $webhook The ChildWebhook object to add.
     */
    protected function doAddWebhook(ChildWebhook $webhook)
    {
        $this->collWebhooks[]= $webhook;
        $webhook->setRemoteProvider($this);
    }

    /**
     * @param  ChildWebhook $webhook The ChildWebhook object to remove.
     * @return $this|ChildRemoteProvider The current object (for fluent API support)
     */
    public function removeWebhook(ChildWebhook $webhook)
    {
        if ($this->getWebhooks()->contains($webhook)) {
            $pos = $this->collWebhooks->search($webhook);
            $this->collWebhooks->remove($pos);
            if (null === $this->webhooksScheduledForDeletion) {
                $this->webhooksScheduledForDeletion = clone $this->collWebhooks;
                $this->webhooksScheduledForDeletion->clear();
            }
            $this->webhooksScheduledForDeletion[]= clone $webhook;
            $webhook->setRemoteProvider(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RemoteProvider is new, it will return
     * an empty collection; or if this RemoteProvider has previously
     * been saved, it will retrieve related Webhooks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RemoteProvider.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildWebhook[] List of ChildWebhook objects
     */
    public function getWebhooksJoinSection(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildWebhookQuery::create(null, $criteria);
        $query->joinWith('Section', $joinBehavior);

        return $this->getWebhooks($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RemoteProvider is new, it will return
     * an empty collection; or if this RemoteProvider has previously
     * been saved, it will retrieve related Webhooks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RemoteProvider.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildWebhook[] List of ChildWebhook objects
     */
    public function getWebhooksJoinPost(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildWebhookQuery::create(null, $criteria);
        $query->joinWith('Post', $joinBehavior);

        return $this->getWebhooks($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->uuid = null;
        $this->email = null;
        $this->email_canonical = null;
        $this->name = null;
        $this->slug = null;
        $this->icon = null;
        $this->site = null;
        $this->deleted_at = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collPostConnectors) {
                foreach ($this->collPostConnectors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSectionConnectors) {
                foreach ($this->collSectionConnectors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWebhooks) {
                foreach ($this->collWebhooks as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collPostConnectors = null;
        $this->collSectionConnectors = null;
        $this->collWebhooks = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(RemoteProviderTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildRemoteProvider The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[RemoteProviderTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildRemoteProviderArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildRemoteProviderArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildRemoteProviderArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildRemoteProviderArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildRemoteProvider The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildRemoteProviderArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildRemoteProvider The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setId($archive->getId());
        }
        $this->setUuid($archive->getUuid());
        $this->setEmail($archive->getEmail());
        $this->setEmailCanonical($archive->getEmailCanonical());
        $this->setName($archive->getName());
        $this->setSlug($archive->getSlug());
        $this->setIcon($archive->getIcon());
        $this->setSite($archive->getSite());
        $this->setDeletedAt($archive->getDeletedAt());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildRemoteProvider The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
