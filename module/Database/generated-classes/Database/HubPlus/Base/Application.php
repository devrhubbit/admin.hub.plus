<?php

namespace Database\HubPlus\Base;

use \DateTime;
use \Exception;
use \PDO;
use Database\HubPlus\Application as ChildApplication;
use Database\HubPlus\ApplicationArchive as ChildApplicationArchive;
use Database\HubPlus\ApplicationArchiveQuery as ChildApplicationArchiveQuery;
use Database\HubPlus\ApplicationQuery as ChildApplicationQuery;
use Database\HubPlus\Category as ChildCategory;
use Database\HubPlus\CategoryQuery as ChildCategoryQuery;
use Database\HubPlus\Pack as ChildPack;
use Database\HubPlus\PackQuery as ChildPackQuery;
use Database\HubPlus\Template as ChildTemplate;
use Database\HubPlus\TemplateQuery as ChildTemplateQuery;
use Database\HubPlus\UserApplication as ChildUserApplication;
use Database\HubPlus\UserApplicationQuery as ChildUserApplicationQuery;
use Database\HubPlus\Map\ApplicationTableMap;
use Database\HubPlus\Map\UserApplicationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'application' table.
 *
 *
 *
 * @package    propel.generator.Database.HubPlus.Base
 */
abstract class Application implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Database\\HubPlus\\Map\\ApplicationTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the title field.
     *
     * @var        string
     */
    protected $title;

    /**
     * The value for the slug field.
     *
     * @var        string
     */
    protected $slug;

    /**
     * The value for the bundle field.
     *
     * @var        string
     */
    protected $bundle;

    /**
     * The value for the description field.
     *
     * @var        string
     */
    protected $description;

    /**
     * The value for the app_key field.
     *
     * @var        string
     */
    protected $app_key;

    /**
     * The value for the dsn field.
     *
     * @var        string
     */
    protected $dsn;

    /**
     * The value for the db_host field.
     *
     * @var        string
     */
    protected $db_host;

    /**
     * The value for the db_name field.
     *
     * @var        string
     */
    protected $db_name;

    /**
     * The value for the db_user field.
     *
     * @var        string
     */
    protected $db_user;

    /**
     * The value for the db_pwd field.
     *
     * @var        string
     */
    protected $db_pwd;

    /**
     * The value for the api_version field.
     *
     * @var        string
     */
    protected $api_version;

    /**
     * The value for the pack_id field.
     *
     * @var        int
     */
    protected $pack_id;

    /**
     * The value for the category_id field.
     *
     * @var        int
     */
    protected $category_id;

    /**
     * The value for the template_id field.
     *
     * @var        int
     */
    protected $template_id;

    /**
     * The value for the push_notification field.
     *
     * @var        string
     */
    protected $push_notification;

    /**
     * The value for the max_notification field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $max_notification;

    /**
     * The value for the max_advice_hour field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $max_advice_hour;

    /**
     * The value for the advice_hour_done field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $advice_hour_done;

    /**
     * The value for the max_content_insert field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $max_content_insert;

    /**
     * The value for the icon field.
     *
     * @var        string
     */
    protected $icon;

    /**
     * The value for the splash_screen field.
     *
     * @var        string
     */
    protected $splash_screen;

    /**
     * The value for the background_color field.
     *
     * @var        string
     */
    protected $background_color;

    /**
     * The value for the app_logo field.
     *
     * @var        string
     */
    protected $app_logo;

    /**
     * The value for the layout field.
     *
     * @var        string
     */
    protected $layout;

    /**
     * The value for the white_label field.
     *
     * @var        boolean
     */
    protected $white_label;

    /**
     * The value for the demo field.
     *
     * @var        boolean
     */
    protected $demo;

    /**
     * The value for the facebook_token field.
     *
     * @var        string
     */
    protected $facebook_token;

    /**
     * The value for the apple_id field.
     *
     * @var        string
     */
    protected $apple_id;

    /**
     * The value for the apple_id_password field.
     *
     * @var        string
     */
    protected $apple_id_password;

    /**
     * The value for the apple_store_app_link field.
     *
     * @var        string
     */
    protected $apple_store_app_link;

    /**
     * The value for the play_store_id field.
     *
     * @var        string
     */
    protected $play_store_id;

    /**
     * The value for the play_store_id_password field.
     *
     * @var        string
     */
    protected $play_store_id_password;

    /**
     * The value for the play_store_app_link field.
     *
     * @var        string
     */
    protected $play_store_app_link;

    /**
     * The value for the controller_uri field.
     *
     * @var        string
     */
    protected $controller_uri;

    /**
     * The value for the google_analytics_ua field.
     *
     * @var        string
     */
    protected $google_analytics_ua;

    /**
     * The value for the published field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $published;

    /**
     * The value for the expired_at field.
     *
     * @var        DateTime
     */
    protected $expired_at;

    /**
     * The value for the main_contents_language field.
     *
     * @var        string
     */
    protected $main_contents_language;

    /**
     * The value for the other_contents_languages field.
     *
     * @var        string
     */
    protected $other_contents_languages;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * The value for the system_icons field.
     *
     * @var        string
     */
    protected $system_icons;

    /**
     * The value for the toolbar field.
     *
     * Note: this column has a database default value of: 'SLIDESHOW'
     * @var        string
     */
    protected $toolbar;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildCategory
     */
    protected $aCategory;

    /**
     * @var        ChildTemplate
     */
    protected $aTemplate;

    /**
     * @var        ChildPack
     */
    protected $aPack;

    /**
     * @var        ObjectCollection|ChildUserApplication[] Collection to store aggregation of ChildUserApplication objects.
     */
    protected $collUserApplications;
    protected $collUserApplicationsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUserApplication[]
     */
    protected $userApplicationsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->max_notification = 0;
        $this->max_advice_hour = 0;
        $this->advice_hour_done = 0;
        $this->max_content_insert = 0;
        $this->published = 0;
        $this->toolbar = 'SLIDESHOW';
    }

    /**
     * Initializes internal state of Database\HubPlus\Base\Application object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Application</code> instance.  If
     * <code>obj</code> is an instance of <code>Application</code>, delegates to
     * <code>equals(Application)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Application The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [slug] column value.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get the [bundle] column value.
     *
     * @return string
     */
    public function getBundle()
    {
        return $this->bundle;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [app_key] column value.
     *
     * @return string
     */
    public function getAppKey()
    {
        return $this->app_key;
    }

    /**
     * Get the [dsn] column value.
     *
     * @return string
     */
    public function getDsn()
    {
        return $this->dsn;
    }

    /**
     * Get the [db_host] column value.
     *
     * @return string
     */
    public function getDbHost()
    {
        return $this->db_host;
    }

    /**
     * Get the [db_name] column value.
     *
     * @return string
     */
    public function getDbName()
    {
        return $this->db_name;
    }

    /**
     * Get the [db_user] column value.
     *
     * @return string
     */
    public function getDbUser()
    {
        return $this->db_user;
    }

    /**
     * Get the [db_pwd] column value.
     *
     * @return string
     */
    public function getDbPwd()
    {
        return $this->db_pwd;
    }

    /**
     * Get the [api_version] column value.
     *
     * @return string
     */
    public function getApiVersion()
    {
        return $this->api_version;
    }

    /**
     * Get the [pack_id] column value.
     *
     * @return int
     */
    public function getPackId()
    {
        return $this->pack_id;
    }

    /**
     * Get the [category_id] column value.
     *
     * @return int
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * Get the [template_id] column value.
     *
     * @return int
     */
    public function getTemplateId()
    {
        return $this->template_id;
    }

    /**
     * Get the [push_notification] column value.
     *
     * @return string
     */
    public function getPushNotification()
    {
        return $this->push_notification;
    }

    /**
     * Get the [max_notification] column value.
     *
     * @return int
     */
    public function getMaxNotification()
    {
        return $this->max_notification;
    }

    /**
     * Get the [max_advice_hour] column value.
     *
     * @return int
     */
    public function getMaxAdviceHour()
    {
        return $this->max_advice_hour;
    }

    /**
     * Get the [advice_hour_done] column value.
     *
     * @return int
     */
    public function getAdviceHourDone()
    {
        return $this->advice_hour_done;
    }

    /**
     * Get the [max_content_insert] column value.
     *
     * @return int
     */
    public function getMaxContentInsert()
    {
        return $this->max_content_insert;
    }

    /**
     * Get the [icon] column value.
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Get the [splash_screen] column value.
     *
     * @return string
     */
    public function getSplashScreen()
    {
        return $this->splash_screen;
    }

    /**
     * Get the [background_color] column value.
     *
     * @return string
     */
    public function getBackgroundColor()
    {
        return $this->background_color;
    }

    /**
     * Get the [app_logo] column value.
     *
     * @return string
     */
    public function getAppLogo()
    {
        return $this->app_logo;
    }

    /**
     * Get the [layout] column value.
     *
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Get the [white_label] column value.
     *
     * @return boolean
     */
    public function getWhiteLabel()
    {
        return $this->white_label;
    }

    /**
     * Get the [white_label] column value.
     *
     * @return boolean
     */
    public function isWhiteLabel()
    {
        return $this->getWhiteLabel();
    }

    /**
     * Get the [demo] column value.
     *
     * @return boolean
     */
    public function getDemo()
    {
        return $this->demo;
    }

    /**
     * Get the [demo] column value.
     *
     * @return boolean
     */
    public function isDemo()
    {
        return $this->getDemo();
    }

    /**
     * Get the [facebook_token] column value.
     *
     * @return string
     */
    public function getFacebookToken()
    {
        return $this->facebook_token;
    }

    /**
     * Get the [apple_id] column value.
     *
     * @return string
     */
    public function getAppleId()
    {
        return $this->apple_id;
    }

    /**
     * Get the [apple_id_password] column value.
     *
     * @return string
     */
    public function getAppleIdPassword()
    {
        return $this->apple_id_password;
    }

    /**
     * Get the [apple_store_app_link] column value.
     *
     * @return string
     */
    public function getAppleStoreAppLink()
    {
        return $this->apple_store_app_link;
    }

    /**
     * Get the [play_store_id] column value.
     *
     * @return string
     */
    public function getPlayStoreId()
    {
        return $this->play_store_id;
    }

    /**
     * Get the [play_store_id_password] column value.
     *
     * @return string
     */
    public function getPlayStoreIdPassword()
    {
        return $this->play_store_id_password;
    }

    /**
     * Get the [play_store_app_link] column value.
     *
     * @return string
     */
    public function getPlayStoreAppLink()
    {
        return $this->play_store_app_link;
    }

    /**
     * Get the [controller_uri] column value.
     *
     * @return string
     */
    public function getControllerUri()
    {
        return $this->controller_uri;
    }

    /**
     * Get the [google_analytics_ua] column value.
     *
     * @return string
     */
    public function getGoogleAnalyticsUa()
    {
        return $this->google_analytics_ua;
    }

    /**
     * Get the [published] column value.
     *
     * @return int
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Get the [optionally formatted] temporal [expired_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiredAt($format = NULL)
    {
        if ($format === null) {
            return $this->expired_at;
        } else {
            return $this->expired_at instanceof \DateTimeInterface ? $this->expired_at->format($format) : null;
        }
    }

    /**
     * Get the [main_contents_language] column value.
     *
     * @return string
     */
    public function getMainContentsLanguage()
    {
        return $this->main_contents_language;
    }

    /**
     * Get the [other_contents_languages] column value.
     *
     * @return string
     */
    public function getOtherContentsLanguages()
    {
        return $this->other_contents_languages;
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Get the [system_icons] column value.
     *
     * @return string
     */
    public function getSystemIcons()
    {
        return $this->system_icons;
    }

    /**
     * Get the [toolbar] column value.
     *
     * @return string
     */
    public function getToolbar()
    {
        return $this->toolbar;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [title] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [slug] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->slug !== $v) {
            $this->slug = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_SLUG] = true;
        }

        return $this;
    } // setSlug()

    /**
     * Set the value of [bundle] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setBundle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bundle !== $v) {
            $this->bundle = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_BUNDLE] = true;
        }

        return $this;
    } // setBundle()

    /**
     * Set the value of [description] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [app_key] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setAppKey($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->app_key !== $v) {
            $this->app_key = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_APP_KEY] = true;
        }

        return $this;
    } // setAppKey()

    /**
     * Set the value of [dsn] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setDsn($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dsn !== $v) {
            $this->dsn = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_DSN] = true;
        }

        return $this;
    } // setDsn()

    /**
     * Set the value of [db_host] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setDbHost($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->db_host !== $v) {
            $this->db_host = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_DB_HOST] = true;
        }

        return $this;
    } // setDbHost()

    /**
     * Set the value of [db_name] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setDbName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->db_name !== $v) {
            $this->db_name = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_DB_NAME] = true;
        }

        return $this;
    } // setDbName()

    /**
     * Set the value of [db_user] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setDbUser($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->db_user !== $v) {
            $this->db_user = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_DB_USER] = true;
        }

        return $this;
    } // setDbUser()

    /**
     * Set the value of [db_pwd] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setDbPwd($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->db_pwd !== $v) {
            $this->db_pwd = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_DB_PWD] = true;
        }

        return $this;
    } // setDbPwd()

    /**
     * Set the value of [api_version] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setApiVersion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->api_version !== $v) {
            $this->api_version = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_API_VERSION] = true;
        }

        return $this;
    } // setApiVersion()

    /**
     * Set the value of [pack_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setPackId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pack_id !== $v) {
            $this->pack_id = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_PACK_ID] = true;
        }

        if ($this->aPack !== null && $this->aPack->getId() !== $v) {
            $this->aPack = null;
        }

        return $this;
    } // setPackId()

    /**
     * Set the value of [category_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setCategoryId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->category_id !== $v) {
            $this->category_id = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_CATEGORY_ID] = true;
        }

        if ($this->aCategory !== null && $this->aCategory->getId() !== $v) {
            $this->aCategory = null;
        }

        return $this;
    } // setCategoryId()

    /**
     * Set the value of [template_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setTemplateId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->template_id !== $v) {
            $this->template_id = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_TEMPLATE_ID] = true;
        }

        if ($this->aTemplate !== null && $this->aTemplate->getId() !== $v) {
            $this->aTemplate = null;
        }

        return $this;
    } // setTemplateId()

    /**
     * Set the value of [push_notification] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setPushNotification($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->push_notification !== $v) {
            $this->push_notification = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_PUSH_NOTIFICATION] = true;
        }

        return $this;
    } // setPushNotification()

    /**
     * Set the value of [max_notification] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setMaxNotification($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->max_notification !== $v) {
            $this->max_notification = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_MAX_NOTIFICATION] = true;
        }

        return $this;
    } // setMaxNotification()

    /**
     * Set the value of [max_advice_hour] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setMaxAdviceHour($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->max_advice_hour !== $v) {
            $this->max_advice_hour = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_MAX_ADVICE_HOUR] = true;
        }

        return $this;
    } // setMaxAdviceHour()

    /**
     * Set the value of [advice_hour_done] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setAdviceHourDone($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->advice_hour_done !== $v) {
            $this->advice_hour_done = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_ADVICE_HOUR_DONE] = true;
        }

        return $this;
    } // setAdviceHourDone()

    /**
     * Set the value of [max_content_insert] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setMaxContentInsert($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->max_content_insert !== $v) {
            $this->max_content_insert = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_MAX_CONTENT_INSERT] = true;
        }

        return $this;
    } // setMaxContentInsert()

    /**
     * Set the value of [icon] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setIcon($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->icon !== $v) {
            $this->icon = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_ICON] = true;
        }

        return $this;
    } // setIcon()

    /**
     * Set the value of [splash_screen] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setSplashScreen($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->splash_screen !== $v) {
            $this->splash_screen = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_SPLASH_SCREEN] = true;
        }

        return $this;
    } // setSplashScreen()

    /**
     * Set the value of [background_color] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setBackgroundColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->background_color !== $v) {
            $this->background_color = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_BACKGROUND_COLOR] = true;
        }

        return $this;
    } // setBackgroundColor()

    /**
     * Set the value of [app_logo] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setAppLogo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->app_logo !== $v) {
            $this->app_logo = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_APP_LOGO] = true;
        }

        return $this;
    } // setAppLogo()

    /**
     * Set the value of [layout] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setLayout($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->layout !== $v) {
            $this->layout = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_LAYOUT] = true;
        }

        return $this;
    } // setLayout()

    /**
     * Sets the value of the [white_label] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setWhiteLabel($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->white_label !== $v) {
            $this->white_label = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_WHITE_LABEL] = true;
        }

        return $this;
    } // setWhiteLabel()

    /**
     * Sets the value of the [demo] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setDemo($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->demo !== $v) {
            $this->demo = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_DEMO] = true;
        }

        return $this;
    } // setDemo()

    /**
     * Set the value of [facebook_token] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setFacebookToken($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->facebook_token !== $v) {
            $this->facebook_token = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_FACEBOOK_TOKEN] = true;
        }

        return $this;
    } // setFacebookToken()

    /**
     * Set the value of [apple_id] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setAppleId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->apple_id !== $v) {
            $this->apple_id = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_APPLE_ID] = true;
        }

        return $this;
    } // setAppleId()

    /**
     * Set the value of [apple_id_password] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setAppleIdPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->apple_id_password !== $v) {
            $this->apple_id_password = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_APPLE_ID_PASSWORD] = true;
        }

        return $this;
    } // setAppleIdPassword()

    /**
     * Set the value of [apple_store_app_link] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setAppleStoreAppLink($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->apple_store_app_link !== $v) {
            $this->apple_store_app_link = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_APPLE_STORE_APP_LINK] = true;
        }

        return $this;
    } // setAppleStoreAppLink()

    /**
     * Set the value of [play_store_id] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setPlayStoreId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->play_store_id !== $v) {
            $this->play_store_id = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_PLAY_STORE_ID] = true;
        }

        return $this;
    } // setPlayStoreId()

    /**
     * Set the value of [play_store_id_password] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setPlayStoreIdPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->play_store_id_password !== $v) {
            $this->play_store_id_password = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_PLAY_STORE_ID_PASSWORD] = true;
        }

        return $this;
    } // setPlayStoreIdPassword()

    /**
     * Set the value of [play_store_app_link] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setPlayStoreAppLink($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->play_store_app_link !== $v) {
            $this->play_store_app_link = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_PLAY_STORE_APP_LINK] = true;
        }

        return $this;
    } // setPlayStoreAppLink()

    /**
     * Set the value of [controller_uri] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setControllerUri($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->controller_uri !== $v) {
            $this->controller_uri = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_CONTROLLER_URI] = true;
        }

        return $this;
    } // setControllerUri()

    /**
     * Set the value of [google_analytics_ua] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setGoogleAnalyticsUa($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->google_analytics_ua !== $v) {
            $this->google_analytics_ua = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_GOOGLE_ANALYTICS_UA] = true;
        }

        return $this;
    } // setGoogleAnalyticsUa()

    /**
     * Set the value of [published] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setPublished($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->published !== $v) {
            $this->published = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_PUBLISHED] = true;
        }

        return $this;
    } // setPublished()

    /**
     * Sets the value of [expired_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setExpiredAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expired_at !== null || $dt !== null) {
            if ($this->expired_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->expired_at->format("Y-m-d H:i:s.u")) {
                $this->expired_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApplicationTableMap::COL_EXPIRED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setExpiredAt()

    /**
     * Set the value of [main_contents_language] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setMainContentsLanguage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->main_contents_language !== $v) {
            $this->main_contents_language = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_MAIN_CONTENTS_LANGUAGE] = true;
        }

        return $this;
    } // setMainContentsLanguage()

    /**
     * Set the value of [other_contents_languages] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setOtherContentsLanguages($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->other_contents_languages !== $v) {
            $this->other_contents_languages = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_OTHER_CONTENTS_LANGUAGES] = true;
        }

        return $this;
    } // setOtherContentsLanguages()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApplicationTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Set the value of [system_icons] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setSystemIcons($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->system_icons !== $v) {
            $this->system_icons = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_SYSTEM_ICONS] = true;
        }

        return $this;
    } // setSystemIcons()

    /**
     * Set the value of [toolbar] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setToolbar($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->toolbar !== $v) {
            $this->toolbar = $v;
            $this->modifiedColumns[ApplicationTableMap::COL_TOOLBAR] = true;
        }

        return $this;
    } // setToolbar()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApplicationTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ApplicationTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->max_notification !== 0) {
                return false;
            }

            if ($this->max_advice_hour !== 0) {
                return false;
            }

            if ($this->advice_hour_done !== 0) {
                return false;
            }

            if ($this->max_content_insert !== 0) {
                return false;
            }

            if ($this->published !== 0) {
                return false;
            }

            if ($this->toolbar !== 'SLIDESHOW') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ApplicationTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ApplicationTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ApplicationTableMap::translateFieldName('Slug', TableMap::TYPE_PHPNAME, $indexType)];
            $this->slug = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ApplicationTableMap::translateFieldName('Bundle', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bundle = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ApplicationTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ApplicationTableMap::translateFieldName('AppKey', TableMap::TYPE_PHPNAME, $indexType)];
            $this->app_key = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ApplicationTableMap::translateFieldName('Dsn', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dsn = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ApplicationTableMap::translateFieldName('DbHost', TableMap::TYPE_PHPNAME, $indexType)];
            $this->db_host = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ApplicationTableMap::translateFieldName('DbName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->db_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ApplicationTableMap::translateFieldName('DbUser', TableMap::TYPE_PHPNAME, $indexType)];
            $this->db_user = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ApplicationTableMap::translateFieldName('DbPwd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->db_pwd = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ApplicationTableMap::translateFieldName('ApiVersion', TableMap::TYPE_PHPNAME, $indexType)];
            $this->api_version = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ApplicationTableMap::translateFieldName('PackId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pack_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ApplicationTableMap::translateFieldName('CategoryId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->category_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ApplicationTableMap::translateFieldName('TemplateId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->template_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ApplicationTableMap::translateFieldName('PushNotification', TableMap::TYPE_PHPNAME, $indexType)];
            $this->push_notification = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ApplicationTableMap::translateFieldName('MaxNotification', TableMap::TYPE_PHPNAME, $indexType)];
            $this->max_notification = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ApplicationTableMap::translateFieldName('MaxAdviceHour', TableMap::TYPE_PHPNAME, $indexType)];
            $this->max_advice_hour = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ApplicationTableMap::translateFieldName('AdviceHourDone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->advice_hour_done = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ApplicationTableMap::translateFieldName('MaxContentInsert', TableMap::TYPE_PHPNAME, $indexType)];
            $this->max_content_insert = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : ApplicationTableMap::translateFieldName('Icon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->icon = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : ApplicationTableMap::translateFieldName('SplashScreen', TableMap::TYPE_PHPNAME, $indexType)];
            $this->splash_screen = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : ApplicationTableMap::translateFieldName('BackgroundColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->background_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : ApplicationTableMap::translateFieldName('AppLogo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->app_logo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : ApplicationTableMap::translateFieldName('Layout', TableMap::TYPE_PHPNAME, $indexType)];
            $this->layout = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : ApplicationTableMap::translateFieldName('WhiteLabel', TableMap::TYPE_PHPNAME, $indexType)];
            $this->white_label = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : ApplicationTableMap::translateFieldName('Demo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->demo = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : ApplicationTableMap::translateFieldName('FacebookToken', TableMap::TYPE_PHPNAME, $indexType)];
            $this->facebook_token = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : ApplicationTableMap::translateFieldName('AppleId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->apple_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : ApplicationTableMap::translateFieldName('AppleIdPassword', TableMap::TYPE_PHPNAME, $indexType)];
            $this->apple_id_password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : ApplicationTableMap::translateFieldName('AppleStoreAppLink', TableMap::TYPE_PHPNAME, $indexType)];
            $this->apple_store_app_link = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : ApplicationTableMap::translateFieldName('PlayStoreId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->play_store_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 32 + $startcol : ApplicationTableMap::translateFieldName('PlayStoreIdPassword', TableMap::TYPE_PHPNAME, $indexType)];
            $this->play_store_id_password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 33 + $startcol : ApplicationTableMap::translateFieldName('PlayStoreAppLink', TableMap::TYPE_PHPNAME, $indexType)];
            $this->play_store_app_link = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 34 + $startcol : ApplicationTableMap::translateFieldName('ControllerUri', TableMap::TYPE_PHPNAME, $indexType)];
            $this->controller_uri = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 35 + $startcol : ApplicationTableMap::translateFieldName('GoogleAnalyticsUa', TableMap::TYPE_PHPNAME, $indexType)];
            $this->google_analytics_ua = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 36 + $startcol : ApplicationTableMap::translateFieldName('Published', TableMap::TYPE_PHPNAME, $indexType)];
            $this->published = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 37 + $startcol : ApplicationTableMap::translateFieldName('ExpiredAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->expired_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 38 + $startcol : ApplicationTableMap::translateFieldName('MainContentsLanguage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->main_contents_language = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 39 + $startcol : ApplicationTableMap::translateFieldName('OtherContentsLanguages', TableMap::TYPE_PHPNAME, $indexType)];
            $this->other_contents_languages = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 40 + $startcol : ApplicationTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 41 + $startcol : ApplicationTableMap::translateFieldName('SystemIcons', TableMap::TYPE_PHPNAME, $indexType)];
            $this->system_icons = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 42 + $startcol : ApplicationTableMap::translateFieldName('Toolbar', TableMap::TYPE_PHPNAME, $indexType)];
            $this->toolbar = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 43 + $startcol : ApplicationTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 44 + $startcol : ApplicationTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 45; // 45 = ApplicationTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Database\\HubPlus\\Application'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aPack !== null && $this->pack_id !== $this->aPack->getId()) {
            $this->aPack = null;
        }
        if ($this->aCategory !== null && $this->category_id !== $this->aCategory->getId()) {
            $this->aCategory = null;
        }
        if ($this->aTemplate !== null && $this->template_id !== $this->aTemplate->getId()) {
            $this->aTemplate = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApplicationTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildApplicationQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCategory = null;
            $this->aTemplate = null;
            $this->aPack = null;
            $this->collUserApplications = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Application::setDeleted()
     * @see Application::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicationTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildApplicationQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildApplicationQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicationTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(ApplicationTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(ApplicationTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(ApplicationTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ApplicationTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCategory !== null) {
                if ($this->aCategory->isModified() || $this->aCategory->isNew()) {
                    $affectedRows += $this->aCategory->save($con);
                }
                $this->setCategory($this->aCategory);
            }

            if ($this->aTemplate !== null) {
                if ($this->aTemplate->isModified() || $this->aTemplate->isNew()) {
                    $affectedRows += $this->aTemplate->save($con);
                }
                $this->setTemplate($this->aTemplate);
            }

            if ($this->aPack !== null) {
                if ($this->aPack->isModified() || $this->aPack->isNew()) {
                    $affectedRows += $this->aPack->save($con);
                }
                $this->setPack($this->aPack);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->userApplicationsScheduledForDeletion !== null) {
                if (!$this->userApplicationsScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\UserApplicationQuery::create()
                        ->filterByPrimaryKeys($this->userApplicationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userApplicationsScheduledForDeletion = null;
                }
            }

            if ($this->collUserApplications !== null) {
                foreach ($this->collUserApplications as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ApplicationTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ApplicationTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ApplicationTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_SLUG)) {
            $modifiedColumns[':p' . $index++]  = 'slug';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_BUNDLE)) {
            $modifiedColumns[':p' . $index++]  = 'bundle';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_APP_KEY)) {
            $modifiedColumns[':p' . $index++]  = 'app_key';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DSN)) {
            $modifiedColumns[':p' . $index++]  = 'dsn';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DB_HOST)) {
            $modifiedColumns[':p' . $index++]  = 'db_host';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DB_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'db_name';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DB_USER)) {
            $modifiedColumns[':p' . $index++]  = 'db_user';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DB_PWD)) {
            $modifiedColumns[':p' . $index++]  = 'db_pwd';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_API_VERSION)) {
            $modifiedColumns[':p' . $index++]  = 'api_version';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_PACK_ID)) {
            $modifiedColumns[':p' . $index++]  = 'pack_id';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_CATEGORY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'category_id';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_TEMPLATE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'template_id';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_PUSH_NOTIFICATION)) {
            $modifiedColumns[':p' . $index++]  = 'push_notification';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_MAX_NOTIFICATION)) {
            $modifiedColumns[':p' . $index++]  = 'max_notification';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_MAX_ADVICE_HOUR)) {
            $modifiedColumns[':p' . $index++]  = 'max_advice_hour';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_ADVICE_HOUR_DONE)) {
            $modifiedColumns[':p' . $index++]  = 'advice_hour_done';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_MAX_CONTENT_INSERT)) {
            $modifiedColumns[':p' . $index++]  = 'max_content_insert';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_ICON)) {
            $modifiedColumns[':p' . $index++]  = 'icon';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_SPLASH_SCREEN)) {
            $modifiedColumns[':p' . $index++]  = 'splash_screen';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_BACKGROUND_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'background_color';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_APP_LOGO)) {
            $modifiedColumns[':p' . $index++]  = 'app_logo';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_LAYOUT)) {
            $modifiedColumns[':p' . $index++]  = 'layout';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_WHITE_LABEL)) {
            $modifiedColumns[':p' . $index++]  = 'white_label';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DEMO)) {
            $modifiedColumns[':p' . $index++]  = 'demo';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_FACEBOOK_TOKEN)) {
            $modifiedColumns[':p' . $index++]  = 'facebook_token';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_APPLE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'apple_id';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_APPLE_ID_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'apple_id_password';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_APPLE_STORE_APP_LINK)) {
            $modifiedColumns[':p' . $index++]  = 'apple_store_app_link';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_PLAY_STORE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'play_store_id';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_PLAY_STORE_ID_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'play_store_id_password';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_PLAY_STORE_APP_LINK)) {
            $modifiedColumns[':p' . $index++]  = 'play_store_app_link';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_CONTROLLER_URI)) {
            $modifiedColumns[':p' . $index++]  = 'controller_uri';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_GOOGLE_ANALYTICS_UA)) {
            $modifiedColumns[':p' . $index++]  = 'google_analytics_ua';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_PUBLISHED)) {
            $modifiedColumns[':p' . $index++]  = 'published';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_EXPIRED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'expired_at';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_MAIN_CONTENTS_LANGUAGE)) {
            $modifiedColumns[':p' . $index++]  = 'main_contents_language';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_OTHER_CONTENTS_LANGUAGES)) {
            $modifiedColumns[':p' . $index++]  = 'other_contents_languages';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_SYSTEM_ICONS)) {
            $modifiedColumns[':p' . $index++]  = 'system_icons';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_TOOLBAR)) {
            $modifiedColumns[':p' . $index++]  = 'toolbar';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO application (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'slug':
                        $stmt->bindValue($identifier, $this->slug, PDO::PARAM_STR);
                        break;
                    case 'bundle':
                        $stmt->bindValue($identifier, $this->bundle, PDO::PARAM_STR);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'app_key':
                        $stmt->bindValue($identifier, $this->app_key, PDO::PARAM_STR);
                        break;
                    case 'dsn':
                        $stmt->bindValue($identifier, $this->dsn, PDO::PARAM_STR);
                        break;
                    case 'db_host':
                        $stmt->bindValue($identifier, $this->db_host, PDO::PARAM_STR);
                        break;
                    case 'db_name':
                        $stmt->bindValue($identifier, $this->db_name, PDO::PARAM_STR);
                        break;
                    case 'db_user':
                        $stmt->bindValue($identifier, $this->db_user, PDO::PARAM_STR);
                        break;
                    case 'db_pwd':
                        $stmt->bindValue($identifier, $this->db_pwd, PDO::PARAM_STR);
                        break;
                    case 'api_version':
                        $stmt->bindValue($identifier, $this->api_version, PDO::PARAM_STR);
                        break;
                    case 'pack_id':
                        $stmt->bindValue($identifier, $this->pack_id, PDO::PARAM_INT);
                        break;
                    case 'category_id':
                        $stmt->bindValue($identifier, $this->category_id, PDO::PARAM_INT);
                        break;
                    case 'template_id':
                        $stmt->bindValue($identifier, $this->template_id, PDO::PARAM_INT);
                        break;
                    case 'push_notification':
                        $stmt->bindValue($identifier, $this->push_notification, PDO::PARAM_STR);
                        break;
                    case 'max_notification':
                        $stmt->bindValue($identifier, $this->max_notification, PDO::PARAM_INT);
                        break;
                    case 'max_advice_hour':
                        $stmt->bindValue($identifier, $this->max_advice_hour, PDO::PARAM_INT);
                        break;
                    case 'advice_hour_done':
                        $stmt->bindValue($identifier, $this->advice_hour_done, PDO::PARAM_INT);
                        break;
                    case 'max_content_insert':
                        $stmt->bindValue($identifier, $this->max_content_insert, PDO::PARAM_INT);
                        break;
                    case 'icon':
                        $stmt->bindValue($identifier, $this->icon, PDO::PARAM_STR);
                        break;
                    case 'splash_screen':
                        $stmt->bindValue($identifier, $this->splash_screen, PDO::PARAM_STR);
                        break;
                    case 'background_color':
                        $stmt->bindValue($identifier, $this->background_color, PDO::PARAM_STR);
                        break;
                    case 'app_logo':
                        $stmt->bindValue($identifier, $this->app_logo, PDO::PARAM_STR);
                        break;
                    case 'layout':
                        $stmt->bindValue($identifier, $this->layout, PDO::PARAM_STR);
                        break;
                    case 'white_label':
                        $stmt->bindValue($identifier, (int) $this->white_label, PDO::PARAM_INT);
                        break;
                    case 'demo':
                        $stmt->bindValue($identifier, (int) $this->demo, PDO::PARAM_INT);
                        break;
                    case 'facebook_token':
                        $stmt->bindValue($identifier, $this->facebook_token, PDO::PARAM_STR);
                        break;
                    case 'apple_id':
                        $stmt->bindValue($identifier, $this->apple_id, PDO::PARAM_STR);
                        break;
                    case 'apple_id_password':
                        $stmt->bindValue($identifier, $this->apple_id_password, PDO::PARAM_STR);
                        break;
                    case 'apple_store_app_link':
                        $stmt->bindValue($identifier, $this->apple_store_app_link, PDO::PARAM_STR);
                        break;
                    case 'play_store_id':
                        $stmt->bindValue($identifier, $this->play_store_id, PDO::PARAM_STR);
                        break;
                    case 'play_store_id_password':
                        $stmt->bindValue($identifier, $this->play_store_id_password, PDO::PARAM_STR);
                        break;
                    case 'play_store_app_link':
                        $stmt->bindValue($identifier, $this->play_store_app_link, PDO::PARAM_STR);
                        break;
                    case 'controller_uri':
                        $stmt->bindValue($identifier, $this->controller_uri, PDO::PARAM_STR);
                        break;
                    case 'google_analytics_ua':
                        $stmt->bindValue($identifier, $this->google_analytics_ua, PDO::PARAM_STR);
                        break;
                    case 'published':
                        $stmt->bindValue($identifier, $this->published, PDO::PARAM_INT);
                        break;
                    case 'expired_at':
                        $stmt->bindValue($identifier, $this->expired_at ? $this->expired_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'main_contents_language':
                        $stmt->bindValue($identifier, $this->main_contents_language, PDO::PARAM_STR);
                        break;
                    case 'other_contents_languages':
                        $stmt->bindValue($identifier, $this->other_contents_languages, PDO::PARAM_STR);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'system_icons':
                        $stmt->bindValue($identifier, $this->system_icons, PDO::PARAM_STR);
                        break;
                    case 'toolbar':
                        $stmt->bindValue($identifier, $this->toolbar, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApplicationTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getTitle();
                break;
            case 2:
                return $this->getSlug();
                break;
            case 3:
                return $this->getBundle();
                break;
            case 4:
                return $this->getDescription();
                break;
            case 5:
                return $this->getAppKey();
                break;
            case 6:
                return $this->getDsn();
                break;
            case 7:
                return $this->getDbHost();
                break;
            case 8:
                return $this->getDbName();
                break;
            case 9:
                return $this->getDbUser();
                break;
            case 10:
                return $this->getDbPwd();
                break;
            case 11:
                return $this->getApiVersion();
                break;
            case 12:
                return $this->getPackId();
                break;
            case 13:
                return $this->getCategoryId();
                break;
            case 14:
                return $this->getTemplateId();
                break;
            case 15:
                return $this->getPushNotification();
                break;
            case 16:
                return $this->getMaxNotification();
                break;
            case 17:
                return $this->getMaxAdviceHour();
                break;
            case 18:
                return $this->getAdviceHourDone();
                break;
            case 19:
                return $this->getMaxContentInsert();
                break;
            case 20:
                return $this->getIcon();
                break;
            case 21:
                return $this->getSplashScreen();
                break;
            case 22:
                return $this->getBackgroundColor();
                break;
            case 23:
                return $this->getAppLogo();
                break;
            case 24:
                return $this->getLayout();
                break;
            case 25:
                return $this->getWhiteLabel();
                break;
            case 26:
                return $this->getDemo();
                break;
            case 27:
                return $this->getFacebookToken();
                break;
            case 28:
                return $this->getAppleId();
                break;
            case 29:
                return $this->getAppleIdPassword();
                break;
            case 30:
                return $this->getAppleStoreAppLink();
                break;
            case 31:
                return $this->getPlayStoreId();
                break;
            case 32:
                return $this->getPlayStoreIdPassword();
                break;
            case 33:
                return $this->getPlayStoreAppLink();
                break;
            case 34:
                return $this->getControllerUri();
                break;
            case 35:
                return $this->getGoogleAnalyticsUa();
                break;
            case 36:
                return $this->getPublished();
                break;
            case 37:
                return $this->getExpiredAt();
                break;
            case 38:
                return $this->getMainContentsLanguage();
                break;
            case 39:
                return $this->getOtherContentsLanguages();
                break;
            case 40:
                return $this->getDeletedAt();
                break;
            case 41:
                return $this->getSystemIcons();
                break;
            case 42:
                return $this->getToolbar();
                break;
            case 43:
                return $this->getCreatedAt();
                break;
            case 44:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Application'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Application'][$this->hashCode()] = true;
        $keys = ApplicationTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getTitle(),
            $keys[2] => $this->getSlug(),
            $keys[3] => $this->getBundle(),
            $keys[4] => $this->getDescription(),
            $keys[5] => $this->getAppKey(),
            $keys[6] => $this->getDsn(),
            $keys[7] => $this->getDbHost(),
            $keys[8] => $this->getDbName(),
            $keys[9] => $this->getDbUser(),
            $keys[10] => $this->getDbPwd(),
            $keys[11] => $this->getApiVersion(),
            $keys[12] => $this->getPackId(),
            $keys[13] => $this->getCategoryId(),
            $keys[14] => $this->getTemplateId(),
            $keys[15] => $this->getPushNotification(),
            $keys[16] => $this->getMaxNotification(),
            $keys[17] => $this->getMaxAdviceHour(),
            $keys[18] => $this->getAdviceHourDone(),
            $keys[19] => $this->getMaxContentInsert(),
            $keys[20] => $this->getIcon(),
            $keys[21] => $this->getSplashScreen(),
            $keys[22] => $this->getBackgroundColor(),
            $keys[23] => $this->getAppLogo(),
            $keys[24] => $this->getLayout(),
            $keys[25] => $this->getWhiteLabel(),
            $keys[26] => $this->getDemo(),
            $keys[27] => $this->getFacebookToken(),
            $keys[28] => $this->getAppleId(),
            $keys[29] => $this->getAppleIdPassword(),
            $keys[30] => $this->getAppleStoreAppLink(),
            $keys[31] => $this->getPlayStoreId(),
            $keys[32] => $this->getPlayStoreIdPassword(),
            $keys[33] => $this->getPlayStoreAppLink(),
            $keys[34] => $this->getControllerUri(),
            $keys[35] => $this->getGoogleAnalyticsUa(),
            $keys[36] => $this->getPublished(),
            $keys[37] => $this->getExpiredAt(),
            $keys[38] => $this->getMainContentsLanguage(),
            $keys[39] => $this->getOtherContentsLanguages(),
            $keys[40] => $this->getDeletedAt(),
            $keys[41] => $this->getSystemIcons(),
            $keys[42] => $this->getToolbar(),
            $keys[43] => $this->getCreatedAt(),
            $keys[44] => $this->getUpdatedAt(),
        );
        if ($result[$keys[37]] instanceof \DateTimeInterface) {
            $result[$keys[37]] = $result[$keys[37]]->format('c');
        }

        if ($result[$keys[40]] instanceof \DateTimeInterface) {
            $result[$keys[40]] = $result[$keys[40]]->format('c');
        }

        if ($result[$keys[43]] instanceof \DateTimeInterface) {
            $result[$keys[43]] = $result[$keys[43]]->format('c');
        }

        if ($result[$keys[44]] instanceof \DateTimeInterface) {
            $result[$keys[44]] = $result[$keys[44]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCategory) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'category';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'category';
                        break;
                    default:
                        $key = 'Category';
                }

                $result[$key] = $this->aCategory->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTemplate) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'template';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'template';
                        break;
                    default:
                        $key = 'Template';
                }

                $result[$key] = $this->aTemplate->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPack) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pack';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'pack';
                        break;
                    default:
                        $key = 'Pack';
                }

                $result[$key] = $this->aPack->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collUserApplications) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userApplications';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_applications';
                        break;
                    default:
                        $key = 'UserApplications';
                }

                $result[$key] = $this->collUserApplications->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Database\HubPlus\Application
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ApplicationTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Database\HubPlus\Application
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setTitle($value);
                break;
            case 2:
                $this->setSlug($value);
                break;
            case 3:
                $this->setBundle($value);
                break;
            case 4:
                $this->setDescription($value);
                break;
            case 5:
                $this->setAppKey($value);
                break;
            case 6:
                $this->setDsn($value);
                break;
            case 7:
                $this->setDbHost($value);
                break;
            case 8:
                $this->setDbName($value);
                break;
            case 9:
                $this->setDbUser($value);
                break;
            case 10:
                $this->setDbPwd($value);
                break;
            case 11:
                $this->setApiVersion($value);
                break;
            case 12:
                $this->setPackId($value);
                break;
            case 13:
                $this->setCategoryId($value);
                break;
            case 14:
                $this->setTemplateId($value);
                break;
            case 15:
                $this->setPushNotification($value);
                break;
            case 16:
                $this->setMaxNotification($value);
                break;
            case 17:
                $this->setMaxAdviceHour($value);
                break;
            case 18:
                $this->setAdviceHourDone($value);
                break;
            case 19:
                $this->setMaxContentInsert($value);
                break;
            case 20:
                $this->setIcon($value);
                break;
            case 21:
                $this->setSplashScreen($value);
                break;
            case 22:
                $this->setBackgroundColor($value);
                break;
            case 23:
                $this->setAppLogo($value);
                break;
            case 24:
                $this->setLayout($value);
                break;
            case 25:
                $this->setWhiteLabel($value);
                break;
            case 26:
                $this->setDemo($value);
                break;
            case 27:
                $this->setFacebookToken($value);
                break;
            case 28:
                $this->setAppleId($value);
                break;
            case 29:
                $this->setAppleIdPassword($value);
                break;
            case 30:
                $this->setAppleStoreAppLink($value);
                break;
            case 31:
                $this->setPlayStoreId($value);
                break;
            case 32:
                $this->setPlayStoreIdPassword($value);
                break;
            case 33:
                $this->setPlayStoreAppLink($value);
                break;
            case 34:
                $this->setControllerUri($value);
                break;
            case 35:
                $this->setGoogleAnalyticsUa($value);
                break;
            case 36:
                $this->setPublished($value);
                break;
            case 37:
                $this->setExpiredAt($value);
                break;
            case 38:
                $this->setMainContentsLanguage($value);
                break;
            case 39:
                $this->setOtherContentsLanguages($value);
                break;
            case 40:
                $this->setDeletedAt($value);
                break;
            case 41:
                $this->setSystemIcons($value);
                break;
            case 42:
                $this->setToolbar($value);
                break;
            case 43:
                $this->setCreatedAt($value);
                break;
            case 44:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ApplicationTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTitle($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setSlug($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setBundle($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDescription($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setAppKey($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDsn($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDbHost($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setDbName($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setDbUser($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setDbPwd($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setApiVersion($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setPackId($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setCategoryId($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setTemplateId($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setPushNotification($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setMaxNotification($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setMaxAdviceHour($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setAdviceHourDone($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setMaxContentInsert($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setIcon($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setSplashScreen($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setBackgroundColor($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setAppLogo($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setLayout($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setWhiteLabel($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setDemo($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setFacebookToken($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setAppleId($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setAppleIdPassword($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setAppleStoreAppLink($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setPlayStoreId($arr[$keys[31]]);
        }
        if (array_key_exists($keys[32], $arr)) {
            $this->setPlayStoreIdPassword($arr[$keys[32]]);
        }
        if (array_key_exists($keys[33], $arr)) {
            $this->setPlayStoreAppLink($arr[$keys[33]]);
        }
        if (array_key_exists($keys[34], $arr)) {
            $this->setControllerUri($arr[$keys[34]]);
        }
        if (array_key_exists($keys[35], $arr)) {
            $this->setGoogleAnalyticsUa($arr[$keys[35]]);
        }
        if (array_key_exists($keys[36], $arr)) {
            $this->setPublished($arr[$keys[36]]);
        }
        if (array_key_exists($keys[37], $arr)) {
            $this->setExpiredAt($arr[$keys[37]]);
        }
        if (array_key_exists($keys[38], $arr)) {
            $this->setMainContentsLanguage($arr[$keys[38]]);
        }
        if (array_key_exists($keys[39], $arr)) {
            $this->setOtherContentsLanguages($arr[$keys[39]]);
        }
        if (array_key_exists($keys[40], $arr)) {
            $this->setDeletedAt($arr[$keys[40]]);
        }
        if (array_key_exists($keys[41], $arr)) {
            $this->setSystemIcons($arr[$keys[41]]);
        }
        if (array_key_exists($keys[42], $arr)) {
            $this->setToolbar($arr[$keys[42]]);
        }
        if (array_key_exists($keys[43], $arr)) {
            $this->setCreatedAt($arr[$keys[43]]);
        }
        if (array_key_exists($keys[44], $arr)) {
            $this->setUpdatedAt($arr[$keys[44]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Database\HubPlus\Application The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ApplicationTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ApplicationTableMap::COL_ID)) {
            $criteria->add(ApplicationTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_TITLE)) {
            $criteria->add(ApplicationTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_SLUG)) {
            $criteria->add(ApplicationTableMap::COL_SLUG, $this->slug);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_BUNDLE)) {
            $criteria->add(ApplicationTableMap::COL_BUNDLE, $this->bundle);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DESCRIPTION)) {
            $criteria->add(ApplicationTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_APP_KEY)) {
            $criteria->add(ApplicationTableMap::COL_APP_KEY, $this->app_key);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DSN)) {
            $criteria->add(ApplicationTableMap::COL_DSN, $this->dsn);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DB_HOST)) {
            $criteria->add(ApplicationTableMap::COL_DB_HOST, $this->db_host);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DB_NAME)) {
            $criteria->add(ApplicationTableMap::COL_DB_NAME, $this->db_name);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DB_USER)) {
            $criteria->add(ApplicationTableMap::COL_DB_USER, $this->db_user);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DB_PWD)) {
            $criteria->add(ApplicationTableMap::COL_DB_PWD, $this->db_pwd);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_API_VERSION)) {
            $criteria->add(ApplicationTableMap::COL_API_VERSION, $this->api_version);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_PACK_ID)) {
            $criteria->add(ApplicationTableMap::COL_PACK_ID, $this->pack_id);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_CATEGORY_ID)) {
            $criteria->add(ApplicationTableMap::COL_CATEGORY_ID, $this->category_id);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_TEMPLATE_ID)) {
            $criteria->add(ApplicationTableMap::COL_TEMPLATE_ID, $this->template_id);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_PUSH_NOTIFICATION)) {
            $criteria->add(ApplicationTableMap::COL_PUSH_NOTIFICATION, $this->push_notification);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_MAX_NOTIFICATION)) {
            $criteria->add(ApplicationTableMap::COL_MAX_NOTIFICATION, $this->max_notification);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_MAX_ADVICE_HOUR)) {
            $criteria->add(ApplicationTableMap::COL_MAX_ADVICE_HOUR, $this->max_advice_hour);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_ADVICE_HOUR_DONE)) {
            $criteria->add(ApplicationTableMap::COL_ADVICE_HOUR_DONE, $this->advice_hour_done);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_MAX_CONTENT_INSERT)) {
            $criteria->add(ApplicationTableMap::COL_MAX_CONTENT_INSERT, $this->max_content_insert);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_ICON)) {
            $criteria->add(ApplicationTableMap::COL_ICON, $this->icon);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_SPLASH_SCREEN)) {
            $criteria->add(ApplicationTableMap::COL_SPLASH_SCREEN, $this->splash_screen);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_BACKGROUND_COLOR)) {
            $criteria->add(ApplicationTableMap::COL_BACKGROUND_COLOR, $this->background_color);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_APP_LOGO)) {
            $criteria->add(ApplicationTableMap::COL_APP_LOGO, $this->app_logo);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_LAYOUT)) {
            $criteria->add(ApplicationTableMap::COL_LAYOUT, $this->layout);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_WHITE_LABEL)) {
            $criteria->add(ApplicationTableMap::COL_WHITE_LABEL, $this->white_label);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DEMO)) {
            $criteria->add(ApplicationTableMap::COL_DEMO, $this->demo);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_FACEBOOK_TOKEN)) {
            $criteria->add(ApplicationTableMap::COL_FACEBOOK_TOKEN, $this->facebook_token);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_APPLE_ID)) {
            $criteria->add(ApplicationTableMap::COL_APPLE_ID, $this->apple_id);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_APPLE_ID_PASSWORD)) {
            $criteria->add(ApplicationTableMap::COL_APPLE_ID_PASSWORD, $this->apple_id_password);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_APPLE_STORE_APP_LINK)) {
            $criteria->add(ApplicationTableMap::COL_APPLE_STORE_APP_LINK, $this->apple_store_app_link);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_PLAY_STORE_ID)) {
            $criteria->add(ApplicationTableMap::COL_PLAY_STORE_ID, $this->play_store_id);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_PLAY_STORE_ID_PASSWORD)) {
            $criteria->add(ApplicationTableMap::COL_PLAY_STORE_ID_PASSWORD, $this->play_store_id_password);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_PLAY_STORE_APP_LINK)) {
            $criteria->add(ApplicationTableMap::COL_PLAY_STORE_APP_LINK, $this->play_store_app_link);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_CONTROLLER_URI)) {
            $criteria->add(ApplicationTableMap::COL_CONTROLLER_URI, $this->controller_uri);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_GOOGLE_ANALYTICS_UA)) {
            $criteria->add(ApplicationTableMap::COL_GOOGLE_ANALYTICS_UA, $this->google_analytics_ua);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_PUBLISHED)) {
            $criteria->add(ApplicationTableMap::COL_PUBLISHED, $this->published);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_EXPIRED_AT)) {
            $criteria->add(ApplicationTableMap::COL_EXPIRED_AT, $this->expired_at);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_MAIN_CONTENTS_LANGUAGE)) {
            $criteria->add(ApplicationTableMap::COL_MAIN_CONTENTS_LANGUAGE, $this->main_contents_language);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_OTHER_CONTENTS_LANGUAGES)) {
            $criteria->add(ApplicationTableMap::COL_OTHER_CONTENTS_LANGUAGES, $this->other_contents_languages);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_DELETED_AT)) {
            $criteria->add(ApplicationTableMap::COL_DELETED_AT, $this->deleted_at);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_SYSTEM_ICONS)) {
            $criteria->add(ApplicationTableMap::COL_SYSTEM_ICONS, $this->system_icons);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_TOOLBAR)) {
            $criteria->add(ApplicationTableMap::COL_TOOLBAR, $this->toolbar);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_CREATED_AT)) {
            $criteria->add(ApplicationTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(ApplicationTableMap::COL_UPDATED_AT)) {
            $criteria->add(ApplicationTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildApplicationQuery::create();
        $criteria->add(ApplicationTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Database\HubPlus\Application (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTitle($this->getTitle());
        $copyObj->setSlug($this->getSlug());
        $copyObj->setBundle($this->getBundle());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setAppKey($this->getAppKey());
        $copyObj->setDsn($this->getDsn());
        $copyObj->setDbHost($this->getDbHost());
        $copyObj->setDbName($this->getDbName());
        $copyObj->setDbUser($this->getDbUser());
        $copyObj->setDbPwd($this->getDbPwd());
        $copyObj->setApiVersion($this->getApiVersion());
        $copyObj->setPackId($this->getPackId());
        $copyObj->setCategoryId($this->getCategoryId());
        $copyObj->setTemplateId($this->getTemplateId());
        $copyObj->setPushNotification($this->getPushNotification());
        $copyObj->setMaxNotification($this->getMaxNotification());
        $copyObj->setMaxAdviceHour($this->getMaxAdviceHour());
        $copyObj->setAdviceHourDone($this->getAdviceHourDone());
        $copyObj->setMaxContentInsert($this->getMaxContentInsert());
        $copyObj->setIcon($this->getIcon());
        $copyObj->setSplashScreen($this->getSplashScreen());
        $copyObj->setBackgroundColor($this->getBackgroundColor());
        $copyObj->setAppLogo($this->getAppLogo());
        $copyObj->setLayout($this->getLayout());
        $copyObj->setWhiteLabel($this->getWhiteLabel());
        $copyObj->setDemo($this->getDemo());
        $copyObj->setFacebookToken($this->getFacebookToken());
        $copyObj->setAppleId($this->getAppleId());
        $copyObj->setAppleIdPassword($this->getAppleIdPassword());
        $copyObj->setAppleStoreAppLink($this->getAppleStoreAppLink());
        $copyObj->setPlayStoreId($this->getPlayStoreId());
        $copyObj->setPlayStoreIdPassword($this->getPlayStoreIdPassword());
        $copyObj->setPlayStoreAppLink($this->getPlayStoreAppLink());
        $copyObj->setControllerUri($this->getControllerUri());
        $copyObj->setGoogleAnalyticsUa($this->getGoogleAnalyticsUa());
        $copyObj->setPublished($this->getPublished());
        $copyObj->setExpiredAt($this->getExpiredAt());
        $copyObj->setMainContentsLanguage($this->getMainContentsLanguage());
        $copyObj->setOtherContentsLanguages($this->getOtherContentsLanguages());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setSystemIcons($this->getSystemIcons());
        $copyObj->setToolbar($this->getToolbar());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getUserApplications() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserApplication($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Database\HubPlus\Application Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCategory object.
     *
     * @param  ChildCategory $v
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCategory(ChildCategory $v = null)
    {
        if ($v === null) {
            $this->setCategoryId(NULL);
        } else {
            $this->setCategoryId($v->getId());
        }

        $this->aCategory = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCategory object, it will not be re-added.
        if ($v !== null) {
            $v->addApplication($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCategory object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCategory The associated ChildCategory object.
     * @throws PropelException
     */
    public function getCategory(ConnectionInterface $con = null)
    {
        if ($this->aCategory === null && ($this->category_id != 0)) {
            $this->aCategory = ChildCategoryQuery::create()->findPk($this->category_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCategory->addApplications($this);
             */
        }

        return $this->aCategory;
    }

    /**
     * Declares an association between this object and a ChildTemplate object.
     *
     * @param  ChildTemplate $v
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTemplate(ChildTemplate $v = null)
    {
        if ($v === null) {
            $this->setTemplateId(NULL);
        } else {
            $this->setTemplateId($v->getId());
        }

        $this->aTemplate = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildTemplate object, it will not be re-added.
        if ($v !== null) {
            $v->addApplication($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildTemplate object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildTemplate The associated ChildTemplate object.
     * @throws PropelException
     */
    public function getTemplate(ConnectionInterface $con = null)
    {
        if ($this->aTemplate === null && ($this->template_id != 0)) {
            $this->aTemplate = ChildTemplateQuery::create()->findPk($this->template_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTemplate->addApplications($this);
             */
        }

        return $this->aTemplate;
    }

    /**
     * Declares an association between this object and a ChildPack object.
     *
     * @param  ChildPack $v
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPack(ChildPack $v = null)
    {
        if ($v === null) {
            $this->setPackId(NULL);
        } else {
            $this->setPackId($v->getId());
        }

        $this->aPack = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPack object, it will not be re-added.
        if ($v !== null) {
            $v->addApplication($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPack object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPack The associated ChildPack object.
     * @throws PropelException
     */
    public function getPack(ConnectionInterface $con = null)
    {
        if ($this->aPack === null && ($this->pack_id != 0)) {
            $this->aPack = ChildPackQuery::create()->findPk($this->pack_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPack->addApplications($this);
             */
        }

        return $this->aPack;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('UserApplication' == $relationName) {
            $this->initUserApplications();
            return;
        }
    }

    /**
     * Clears out the collUserApplications collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUserApplications()
     */
    public function clearUserApplications()
    {
        $this->collUserApplications = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUserApplications collection loaded partially.
     */
    public function resetPartialUserApplications($v = true)
    {
        $this->collUserApplicationsPartial = $v;
    }

    /**
     * Initializes the collUserApplications collection.
     *
     * By default this just sets the collUserApplications collection to an empty array (like clearcollUserApplications());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserApplications($overrideExisting = true)
    {
        if (null !== $this->collUserApplications && !$overrideExisting) {
            return;
        }

        $collectionClassName = UserApplicationTableMap::getTableMap()->getCollectionClassName();

        $this->collUserApplications = new $collectionClassName;
        $this->collUserApplications->setModel('\Database\HubPlus\UserApplication');
    }

    /**
     * Gets an array of ChildUserApplication objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildApplication is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUserApplication[] List of ChildUserApplication objects
     * @throws PropelException
     */
    public function getUserApplications(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUserApplicationsPartial && !$this->isNew();
        if (null === $this->collUserApplications || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUserApplications) {
                // return empty collection
                $this->initUserApplications();
            } else {
                $collUserApplications = ChildUserApplicationQuery::create(null, $criteria)
                    ->filterByApplication($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUserApplicationsPartial && count($collUserApplications)) {
                        $this->initUserApplications(false);

                        foreach ($collUserApplications as $obj) {
                            if (false == $this->collUserApplications->contains($obj)) {
                                $this->collUserApplications->append($obj);
                            }
                        }

                        $this->collUserApplicationsPartial = true;
                    }

                    return $collUserApplications;
                }

                if ($partial && $this->collUserApplications) {
                    foreach ($this->collUserApplications as $obj) {
                        if ($obj->isNew()) {
                            $collUserApplications[] = $obj;
                        }
                    }
                }

                $this->collUserApplications = $collUserApplications;
                $this->collUserApplicationsPartial = false;
            }
        }

        return $this->collUserApplications;
    }

    /**
     * Sets a collection of ChildUserApplication objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $userApplications A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildApplication The current object (for fluent API support)
     */
    public function setUserApplications(Collection $userApplications, ConnectionInterface $con = null)
    {
        /** @var ChildUserApplication[] $userApplicationsToDelete */
        $userApplicationsToDelete = $this->getUserApplications(new Criteria(), $con)->diff($userApplications);


        $this->userApplicationsScheduledForDeletion = $userApplicationsToDelete;

        foreach ($userApplicationsToDelete as $userApplicationRemoved) {
            $userApplicationRemoved->setApplication(null);
        }

        $this->collUserApplications = null;
        foreach ($userApplications as $userApplication) {
            $this->addUserApplication($userApplication);
        }

        $this->collUserApplications = $userApplications;
        $this->collUserApplicationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserApplication objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UserApplication objects.
     * @throws PropelException
     */
    public function countUserApplications(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUserApplicationsPartial && !$this->isNew();
        if (null === $this->collUserApplications || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserApplications) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserApplications());
            }

            $query = ChildUserApplicationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByApplication($this)
                ->count($con);
        }

        return count($this->collUserApplications);
    }

    /**
     * Method called to associate a ChildUserApplication object to this object
     * through the ChildUserApplication foreign key attribute.
     *
     * @param  ChildUserApplication $l ChildUserApplication
     * @return $this|\Database\HubPlus\Application The current object (for fluent API support)
     */
    public function addUserApplication(ChildUserApplication $l)
    {
        if ($this->collUserApplications === null) {
            $this->initUserApplications();
            $this->collUserApplicationsPartial = true;
        }

        if (!$this->collUserApplications->contains($l)) {
            $this->doAddUserApplication($l);

            if ($this->userApplicationsScheduledForDeletion and $this->userApplicationsScheduledForDeletion->contains($l)) {
                $this->userApplicationsScheduledForDeletion->remove($this->userApplicationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUserApplication $userApplication The ChildUserApplication object to add.
     */
    protected function doAddUserApplication(ChildUserApplication $userApplication)
    {
        $this->collUserApplications[]= $userApplication;
        $userApplication->setApplication($this);
    }

    /**
     * @param  ChildUserApplication $userApplication The ChildUserApplication object to remove.
     * @return $this|ChildApplication The current object (for fluent API support)
     */
    public function removeUserApplication(ChildUserApplication $userApplication)
    {
        if ($this->getUserApplications()->contains($userApplication)) {
            $pos = $this->collUserApplications->search($userApplication);
            $this->collUserApplications->remove($pos);
            if (null === $this->userApplicationsScheduledForDeletion) {
                $this->userApplicationsScheduledForDeletion = clone $this->collUserApplications;
                $this->userApplicationsScheduledForDeletion->clear();
            }
            $this->userApplicationsScheduledForDeletion[]= $userApplication;
            $userApplication->setApplication(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Application is new, it will return
     * an empty collection; or if this Application has previously
     * been saved, it will retrieve related UserApplications from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Application.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserApplication[] List of ChildUserApplication objects
     */
    public function getUserApplicationsJoinUserBackend(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserApplicationQuery::create(null, $criteria);
        $query->joinWith('UserBackend', $joinBehavior);

        return $this->getUserApplications($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCategory) {
            $this->aCategory->removeApplication($this);
        }
        if (null !== $this->aTemplate) {
            $this->aTemplate->removeApplication($this);
        }
        if (null !== $this->aPack) {
            $this->aPack->removeApplication($this);
        }
        $this->id = null;
        $this->title = null;
        $this->slug = null;
        $this->bundle = null;
        $this->description = null;
        $this->app_key = null;
        $this->dsn = null;
        $this->db_host = null;
        $this->db_name = null;
        $this->db_user = null;
        $this->db_pwd = null;
        $this->api_version = null;
        $this->pack_id = null;
        $this->category_id = null;
        $this->template_id = null;
        $this->push_notification = null;
        $this->max_notification = null;
        $this->max_advice_hour = null;
        $this->advice_hour_done = null;
        $this->max_content_insert = null;
        $this->icon = null;
        $this->splash_screen = null;
        $this->background_color = null;
        $this->app_logo = null;
        $this->layout = null;
        $this->white_label = null;
        $this->demo = null;
        $this->facebook_token = null;
        $this->apple_id = null;
        $this->apple_id_password = null;
        $this->apple_store_app_link = null;
        $this->play_store_id = null;
        $this->play_store_id_password = null;
        $this->play_store_app_link = null;
        $this->controller_uri = null;
        $this->google_analytics_ua = null;
        $this->published = null;
        $this->expired_at = null;
        $this->main_contents_language = null;
        $this->other_contents_languages = null;
        $this->deleted_at = null;
        $this->system_icons = null;
        $this->toolbar = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collUserApplications) {
                foreach ($this->collUserApplications as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collUserApplications = null;
        $this->aCategory = null;
        $this->aTemplate = null;
        $this->aPack = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ApplicationTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildApplication The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[ApplicationTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildApplicationArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildApplicationArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildApplicationArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildApplicationArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildApplication The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildApplicationArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildApplication The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setId($archive->getId());
        }
        $this->setTitle($archive->getTitle());
        $this->setSlug($archive->getSlug());
        $this->setBundle($archive->getBundle());
        $this->setDescription($archive->getDescription());
        $this->setAppKey($archive->getAppKey());
        $this->setDsn($archive->getDsn());
        $this->setDbHost($archive->getDbHost());
        $this->setDbName($archive->getDbName());
        $this->setDbUser($archive->getDbUser());
        $this->setDbPwd($archive->getDbPwd());
        $this->setApiVersion($archive->getApiVersion());
        $this->setPackId($archive->getPackId());
        $this->setCategoryId($archive->getCategoryId());
        $this->setTemplateId($archive->getTemplateId());
        $this->setPushNotification($archive->getPushNotification());
        $this->setMaxNotification($archive->getMaxNotification());
        $this->setMaxAdviceHour($archive->getMaxAdviceHour());
        $this->setAdviceHourDone($archive->getAdviceHourDone());
        $this->setMaxContentInsert($archive->getMaxContentInsert());
        $this->setIcon($archive->getIcon());
        $this->setSplashScreen($archive->getSplashScreen());
        $this->setBackgroundColor($archive->getBackgroundColor());
        $this->setAppLogo($archive->getAppLogo());
        $this->setLayout($archive->getLayout());
        $this->setWhiteLabel($archive->getWhiteLabel());
        $this->setDemo($archive->getDemo());
        $this->setFacebookToken($archive->getFacebookToken());
        $this->setAppleId($archive->getAppleId());
        $this->setAppleIdPassword($archive->getAppleIdPassword());
        $this->setAppleStoreAppLink($archive->getAppleStoreAppLink());
        $this->setPlayStoreId($archive->getPlayStoreId());
        $this->setPlayStoreIdPassword($archive->getPlayStoreIdPassword());
        $this->setPlayStoreAppLink($archive->getPlayStoreAppLink());
        $this->setControllerUri($archive->getControllerUri());
        $this->setGoogleAnalyticsUa($archive->getGoogleAnalyticsUa());
        $this->setPublished($archive->getPublished());
        $this->setExpiredAt($archive->getExpiredAt());
        $this->setMainContentsLanguage($archive->getMainContentsLanguage());
        $this->setOtherContentsLanguages($archive->getOtherContentsLanguages());
        $this->setDeletedAt($archive->getDeletedAt());
        $this->setSystemIcons($archive->getSystemIcons());
        $this->setToolbar($archive->getToolbar());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildApplication The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
