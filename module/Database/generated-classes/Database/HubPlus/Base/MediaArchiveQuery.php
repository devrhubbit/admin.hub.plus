<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\MediaArchive as ChildMediaArchive;
use Database\HubPlus\MediaArchiveQuery as ChildMediaArchiveQuery;
use Database\HubPlus\Map\MediaArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'media_archive' table.
 *
 *
 *
 * @method     ChildMediaArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildMediaArchiveQuery orderByDeviceId($order = Criteria::ASC) Order by the device_id column
 * @method     ChildMediaArchiveQuery orderByIsPublic($order = Criteria::ASC) Order by the is_public column
 * @method     ChildMediaArchiveQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildMediaArchiveQuery orderByUri($order = Criteria::ASC) Order by the uri column
 * @method     ChildMediaArchiveQuery orderByUriThumb($order = Criteria::ASC) Order by the uri_thumb column
 * @method     ChildMediaArchiveQuery orderByExtraParams($order = Criteria::ASC) Order by the extra_params column
 * @method     ChildMediaArchiveQuery orderByWeight($order = Criteria::ASC) Order by the weight column
 * @method     ChildMediaArchiveQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildMediaArchiveQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildMediaArchiveQuery orderByExtension($order = Criteria::ASC) Order by the extension column
 * @method     ChildMediaArchiveQuery orderByMimeType($order = Criteria::ASC) Order by the mime_type column
 * @method     ChildMediaArchiveQuery orderBySize($order = Criteria::ASC) Order by the size column
 * @method     ChildMediaArchiveQuery orderByCountLike($order = Criteria::ASC) Order by the count_like column
 * @method     ChildMediaArchiveQuery orderByCountShare($order = Criteria::ASC) Order by the count_share column
 * @method     ChildMediaArchiveQuery orderByFormat($order = Criteria::ASC) Order by the format column
 * @method     ChildMediaArchiveQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildMediaArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildMediaArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildMediaArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildMediaArchiveQuery groupById() Group by the id column
 * @method     ChildMediaArchiveQuery groupByDeviceId() Group by the device_id column
 * @method     ChildMediaArchiveQuery groupByIsPublic() Group by the is_public column
 * @method     ChildMediaArchiveQuery groupByType() Group by the type column
 * @method     ChildMediaArchiveQuery groupByUri() Group by the uri column
 * @method     ChildMediaArchiveQuery groupByUriThumb() Group by the uri_thumb column
 * @method     ChildMediaArchiveQuery groupByExtraParams() Group by the extra_params column
 * @method     ChildMediaArchiveQuery groupByWeight() Group by the weight column
 * @method     ChildMediaArchiveQuery groupByTitle() Group by the title column
 * @method     ChildMediaArchiveQuery groupByDescription() Group by the description column
 * @method     ChildMediaArchiveQuery groupByExtension() Group by the extension column
 * @method     ChildMediaArchiveQuery groupByMimeType() Group by the mime_type column
 * @method     ChildMediaArchiveQuery groupBySize() Group by the size column
 * @method     ChildMediaArchiveQuery groupByCountLike() Group by the count_like column
 * @method     ChildMediaArchiveQuery groupByCountShare() Group by the count_share column
 * @method     ChildMediaArchiveQuery groupByFormat() Group by the format column
 * @method     ChildMediaArchiveQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildMediaArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildMediaArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildMediaArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildMediaArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMediaArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMediaArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMediaArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMediaArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMediaArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMediaArchive findOne(ConnectionInterface $con = null) Return the first ChildMediaArchive matching the query
 * @method     ChildMediaArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMediaArchive matching the query, or a new ChildMediaArchive object populated from the query conditions when no match is found
 *
 * @method     ChildMediaArchive findOneById(int $id) Return the first ChildMediaArchive filtered by the id column
 * @method     ChildMediaArchive findOneByDeviceId(int $device_id) Return the first ChildMediaArchive filtered by the device_id column
 * @method     ChildMediaArchive findOneByIsPublic(int $is_public) Return the first ChildMediaArchive filtered by the is_public column
 * @method     ChildMediaArchive findOneByType(string $type) Return the first ChildMediaArchive filtered by the type column
 * @method     ChildMediaArchive findOneByUri(string $uri) Return the first ChildMediaArchive filtered by the uri column
 * @method     ChildMediaArchive findOneByUriThumb(string $uri_thumb) Return the first ChildMediaArchive filtered by the uri_thumb column
 * @method     ChildMediaArchive findOneByExtraParams(string $extra_params) Return the first ChildMediaArchive filtered by the extra_params column
 * @method     ChildMediaArchive findOneByWeight(int $weight) Return the first ChildMediaArchive filtered by the weight column
 * @method     ChildMediaArchive findOneByTitle(string $title) Return the first ChildMediaArchive filtered by the title column
 * @method     ChildMediaArchive findOneByDescription(string $description) Return the first ChildMediaArchive filtered by the description column
 * @method     ChildMediaArchive findOneByExtension(string $extension) Return the first ChildMediaArchive filtered by the extension column
 * @method     ChildMediaArchive findOneByMimeType(string $mime_type) Return the first ChildMediaArchive filtered by the mime_type column
 * @method     ChildMediaArchive findOneBySize(int $size) Return the first ChildMediaArchive filtered by the size column
 * @method     ChildMediaArchive findOneByCountLike(int $count_like) Return the first ChildMediaArchive filtered by the count_like column
 * @method     ChildMediaArchive findOneByCountShare(int $count_share) Return the first ChildMediaArchive filtered by the count_share column
 * @method     ChildMediaArchive findOneByFormat(string $format) Return the first ChildMediaArchive filtered by the format column
 * @method     ChildMediaArchive findOneByDeletedAt(string $deleted_at) Return the first ChildMediaArchive filtered by the deleted_at column
 * @method     ChildMediaArchive findOneByCreatedAt(string $created_at) Return the first ChildMediaArchive filtered by the created_at column
 * @method     ChildMediaArchive findOneByUpdatedAt(string $updated_at) Return the first ChildMediaArchive filtered by the updated_at column
 * @method     ChildMediaArchive findOneByArchivedAt(string $archived_at) Return the first ChildMediaArchive filtered by the archived_at column *

 * @method     ChildMediaArchive requirePk($key, ConnectionInterface $con = null) Return the ChildMediaArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOne(ConnectionInterface $con = null) Return the first ChildMediaArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMediaArchive requireOneById(int $id) Return the first ChildMediaArchive filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByDeviceId(int $device_id) Return the first ChildMediaArchive filtered by the device_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByIsPublic(int $is_public) Return the first ChildMediaArchive filtered by the is_public column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByType(string $type) Return the first ChildMediaArchive filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByUri(string $uri) Return the first ChildMediaArchive filtered by the uri column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByUriThumb(string $uri_thumb) Return the first ChildMediaArchive filtered by the uri_thumb column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByExtraParams(string $extra_params) Return the first ChildMediaArchive filtered by the extra_params column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByWeight(int $weight) Return the first ChildMediaArchive filtered by the weight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByTitle(string $title) Return the first ChildMediaArchive filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByDescription(string $description) Return the first ChildMediaArchive filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByExtension(string $extension) Return the first ChildMediaArchive filtered by the extension column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByMimeType(string $mime_type) Return the first ChildMediaArchive filtered by the mime_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneBySize(int $size) Return the first ChildMediaArchive filtered by the size column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByCountLike(int $count_like) Return the first ChildMediaArchive filtered by the count_like column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByCountShare(int $count_share) Return the first ChildMediaArchive filtered by the count_share column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByFormat(string $format) Return the first ChildMediaArchive filtered by the format column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByDeletedAt(string $deleted_at) Return the first ChildMediaArchive filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByCreatedAt(string $created_at) Return the first ChildMediaArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildMediaArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMediaArchive requireOneByArchivedAt(string $archived_at) Return the first ChildMediaArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMediaArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMediaArchive objects based on current ModelCriteria
 * @method     ChildMediaArchive[]|ObjectCollection findById(int $id) Return ChildMediaArchive objects filtered by the id column
 * @method     ChildMediaArchive[]|ObjectCollection findByDeviceId(int $device_id) Return ChildMediaArchive objects filtered by the device_id column
 * @method     ChildMediaArchive[]|ObjectCollection findByIsPublic(int $is_public) Return ChildMediaArchive objects filtered by the is_public column
 * @method     ChildMediaArchive[]|ObjectCollection findByType(string $type) Return ChildMediaArchive objects filtered by the type column
 * @method     ChildMediaArchive[]|ObjectCollection findByUri(string $uri) Return ChildMediaArchive objects filtered by the uri column
 * @method     ChildMediaArchive[]|ObjectCollection findByUriThumb(string $uri_thumb) Return ChildMediaArchive objects filtered by the uri_thumb column
 * @method     ChildMediaArchive[]|ObjectCollection findByExtraParams(string $extra_params) Return ChildMediaArchive objects filtered by the extra_params column
 * @method     ChildMediaArchive[]|ObjectCollection findByWeight(int $weight) Return ChildMediaArchive objects filtered by the weight column
 * @method     ChildMediaArchive[]|ObjectCollection findByTitle(string $title) Return ChildMediaArchive objects filtered by the title column
 * @method     ChildMediaArchive[]|ObjectCollection findByDescription(string $description) Return ChildMediaArchive objects filtered by the description column
 * @method     ChildMediaArchive[]|ObjectCollection findByExtension(string $extension) Return ChildMediaArchive objects filtered by the extension column
 * @method     ChildMediaArchive[]|ObjectCollection findByMimeType(string $mime_type) Return ChildMediaArchive objects filtered by the mime_type column
 * @method     ChildMediaArchive[]|ObjectCollection findBySize(int $size) Return ChildMediaArchive objects filtered by the size column
 * @method     ChildMediaArchive[]|ObjectCollection findByCountLike(int $count_like) Return ChildMediaArchive objects filtered by the count_like column
 * @method     ChildMediaArchive[]|ObjectCollection findByCountShare(int $count_share) Return ChildMediaArchive objects filtered by the count_share column
 * @method     ChildMediaArchive[]|ObjectCollection findByFormat(string $format) Return ChildMediaArchive objects filtered by the format column
 * @method     ChildMediaArchive[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildMediaArchive objects filtered by the deleted_at column
 * @method     ChildMediaArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildMediaArchive objects filtered by the created_at column
 * @method     ChildMediaArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildMediaArchive objects filtered by the updated_at column
 * @method     ChildMediaArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildMediaArchive objects filtered by the archived_at column
 * @method     ChildMediaArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MediaArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\MediaArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\MediaArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMediaArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMediaArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMediaArchiveQuery) {
            return $criteria;
        }
        $query = new ChildMediaArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMediaArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MediaArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MediaArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMediaArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, device_id, is_public, type, uri, uri_thumb, extra_params, weight, title, description, extension, mime_type, size, count_like, count_share, format, deleted_at, created_at, updated_at, archived_at FROM media_archive WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMediaArchive $obj */
            $obj = new ChildMediaArchive();
            $obj->hydrate($row);
            MediaArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMediaArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MediaArchiveTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MediaArchiveTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the device_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDeviceId(1234); // WHERE device_id = 1234
     * $query->filterByDeviceId(array(12, 34)); // WHERE device_id IN (12, 34)
     * $query->filterByDeviceId(array('min' => 12)); // WHERE device_id > 12
     * </code>
     *
     * @param     mixed $deviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByDeviceId($deviceId = null, $comparison = null)
    {
        if (is_array($deviceId)) {
            $useMinMax = false;
            if (isset($deviceId['min'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_DEVICE_ID, $deviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deviceId['max'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_DEVICE_ID, $deviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_DEVICE_ID, $deviceId, $comparison);
    }

    /**
     * Filter the query on the is_public column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPublic(1234); // WHERE is_public = 1234
     * $query->filterByIsPublic(array(12, 34)); // WHERE is_public IN (12, 34)
     * $query->filterByIsPublic(array('min' => 12)); // WHERE is_public > 12
     * </code>
     *
     * @param     mixed $isPublic The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByIsPublic($isPublic = null, $comparison = null)
    {
        if (is_array($isPublic)) {
            $useMinMax = false;
            if (isset($isPublic['min'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_IS_PUBLIC, $isPublic['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isPublic['max'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_IS_PUBLIC, $isPublic['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_IS_PUBLIC, $isPublic, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the uri column
     *
     * Example usage:
     * <code>
     * $query->filterByUri('fooValue');   // WHERE uri = 'fooValue'
     * $query->filterByUri('%fooValue%', Criteria::LIKE); // WHERE uri LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uri The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByUri($uri = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uri)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_URI, $uri, $comparison);
    }

    /**
     * Filter the query on the uri_thumb column
     *
     * Example usage:
     * <code>
     * $query->filterByUriThumb('fooValue');   // WHERE uri_thumb = 'fooValue'
     * $query->filterByUriThumb('%fooValue%', Criteria::LIKE); // WHERE uri_thumb LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uriThumb The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByUriThumb($uriThumb = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uriThumb)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_URI_THUMB, $uriThumb, $comparison);
    }

    /**
     * Filter the query on the extra_params column
     *
     * Example usage:
     * <code>
     * $query->filterByExtraParams('fooValue');   // WHERE extra_params = 'fooValue'
     * $query->filterByExtraParams('%fooValue%', Criteria::LIKE); // WHERE extra_params LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extraParams The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByExtraParams($extraParams = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extraParams)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_EXTRA_PARAMS, $extraParams, $comparison);
    }

    /**
     * Filter the query on the weight column
     *
     * Example usage:
     * <code>
     * $query->filterByWeight(1234); // WHERE weight = 1234
     * $query->filterByWeight(array(12, 34)); // WHERE weight IN (12, 34)
     * $query->filterByWeight(array('min' => 12)); // WHERE weight > 12
     * </code>
     *
     * @param     mixed $weight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByWeight($weight = null, $comparison = null)
    {
        if (is_array($weight)) {
            $useMinMax = false;
            if (isset($weight['min'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_WEIGHT, $weight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($weight['max'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_WEIGHT, $weight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_WEIGHT, $weight, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the extension column
     *
     * Example usage:
     * <code>
     * $query->filterByExtension('fooValue');   // WHERE extension = 'fooValue'
     * $query->filterByExtension('%fooValue%', Criteria::LIKE); // WHERE extension LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extension The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByExtension($extension = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extension)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_EXTENSION, $extension, $comparison);
    }

    /**
     * Filter the query on the mime_type column
     *
     * Example usage:
     * <code>
     * $query->filterByMimeType('fooValue');   // WHERE mime_type = 'fooValue'
     * $query->filterByMimeType('%fooValue%', Criteria::LIKE); // WHERE mime_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mimeType The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByMimeType($mimeType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mimeType)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_MIME_TYPE, $mimeType, $comparison);
    }

    /**
     * Filter the query on the size column
     *
     * Example usage:
     * <code>
     * $query->filterBySize(1234); // WHERE size = 1234
     * $query->filterBySize(array(12, 34)); // WHERE size IN (12, 34)
     * $query->filterBySize(array('min' => 12)); // WHERE size > 12
     * </code>
     *
     * @param     mixed $size The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterBySize($size = null, $comparison = null)
    {
        if (is_array($size)) {
            $useMinMax = false;
            if (isset($size['min'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_SIZE, $size['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($size['max'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_SIZE, $size['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_SIZE, $size, $comparison);
    }

    /**
     * Filter the query on the count_like column
     *
     * Example usage:
     * <code>
     * $query->filterByCountLike(1234); // WHERE count_like = 1234
     * $query->filterByCountLike(array(12, 34)); // WHERE count_like IN (12, 34)
     * $query->filterByCountLike(array('min' => 12)); // WHERE count_like > 12
     * </code>
     *
     * @param     mixed $countLike The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByCountLike($countLike = null, $comparison = null)
    {
        if (is_array($countLike)) {
            $useMinMax = false;
            if (isset($countLike['min'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_COUNT_LIKE, $countLike['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countLike['max'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_COUNT_LIKE, $countLike['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_COUNT_LIKE, $countLike, $comparison);
    }

    /**
     * Filter the query on the count_share column
     *
     * Example usage:
     * <code>
     * $query->filterByCountShare(1234); // WHERE count_share = 1234
     * $query->filterByCountShare(array(12, 34)); // WHERE count_share IN (12, 34)
     * $query->filterByCountShare(array('min' => 12)); // WHERE count_share > 12
     * </code>
     *
     * @param     mixed $countShare The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByCountShare($countShare = null, $comparison = null)
    {
        if (is_array($countShare)) {
            $useMinMax = false;
            if (isset($countShare['min'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_COUNT_SHARE, $countShare['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countShare['max'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_COUNT_SHARE, $countShare['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_COUNT_SHARE, $countShare, $comparison);
    }

    /**
     * Filter the query on the format column
     *
     * Example usage:
     * <code>
     * $query->filterByFormat('fooValue');   // WHERE format = 'fooValue'
     * $query->filterByFormat('%fooValue%', Criteria::LIKE); // WHERE format LIKE '%fooValue%'
     * </code>
     *
     * @param     string $format The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByFormat($format = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($format)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_FORMAT, $format, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(MediaArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MediaArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMediaArchive $mediaArchive Object to remove from the list of results
     *
     * @return $this|ChildMediaArchiveQuery The current query, for fluid interface
     */
    public function prune($mediaArchive = null)
    {
        if ($mediaArchive) {
            $this->addUsingAlias(MediaArchiveTableMap::COL_ID, $mediaArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the media_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MediaArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MediaArchiveTableMap::clearInstancePool();
            MediaArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MediaArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MediaArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MediaArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MediaArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MediaArchiveQuery
