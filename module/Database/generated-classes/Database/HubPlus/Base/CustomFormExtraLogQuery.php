<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\CustomFormExtraLog as ChildCustomFormExtraLog;
use Database\HubPlus\CustomFormExtraLogQuery as ChildCustomFormExtraLogQuery;
use Database\HubPlus\Map\CustomFormExtraLogTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'cf_extra_log' table.
 *
 *
 *
 * @method     ChildCustomFormExtraLogQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCustomFormExtraLogQuery orderByFormId($order = Criteria::ASC) Order by the form_id column
 * @method     ChildCustomFormExtraLogQuery orderByRowId($order = Criteria::ASC) Order by the row_id column
 * @method     ChildCustomFormExtraLogQuery orderByDeviceId($order = Criteria::ASC) Order by the device_id column
 * @method     ChildCustomFormExtraLogQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildCustomFormExtraLogQuery orderByTimestamp($order = Criteria::ASC) Order by the timestamp column
 *
 * @method     ChildCustomFormExtraLogQuery groupById() Group by the id column
 * @method     ChildCustomFormExtraLogQuery groupByFormId() Group by the form_id column
 * @method     ChildCustomFormExtraLogQuery groupByRowId() Group by the row_id column
 * @method     ChildCustomFormExtraLogQuery groupByDeviceId() Group by the device_id column
 * @method     ChildCustomFormExtraLogQuery groupByType() Group by the type column
 * @method     ChildCustomFormExtraLogQuery groupByTimestamp() Group by the timestamp column
 *
 * @method     ChildCustomFormExtraLogQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCustomFormExtraLogQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCustomFormExtraLogQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCustomFormExtraLogQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCustomFormExtraLogQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCustomFormExtraLogQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCustomFormExtraLogQuery leftJoinSection($relationAlias = null) Adds a LEFT JOIN clause to the query using the Section relation
 * @method     ChildCustomFormExtraLogQuery rightJoinSection($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Section relation
 * @method     ChildCustomFormExtraLogQuery innerJoinSection($relationAlias = null) Adds a INNER JOIN clause to the query using the Section relation
 *
 * @method     ChildCustomFormExtraLogQuery joinWithSection($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Section relation
 *
 * @method     ChildCustomFormExtraLogQuery leftJoinWithSection() Adds a LEFT JOIN clause and with to the query using the Section relation
 * @method     ChildCustomFormExtraLogQuery rightJoinWithSection() Adds a RIGHT JOIN clause and with to the query using the Section relation
 * @method     ChildCustomFormExtraLogQuery innerJoinWithSection() Adds a INNER JOIN clause and with to the query using the Section relation
 *
 * @method     ChildCustomFormExtraLogQuery leftJoinDevice($relationAlias = null) Adds a LEFT JOIN clause to the query using the Device relation
 * @method     ChildCustomFormExtraLogQuery rightJoinDevice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Device relation
 * @method     ChildCustomFormExtraLogQuery innerJoinDevice($relationAlias = null) Adds a INNER JOIN clause to the query using the Device relation
 *
 * @method     ChildCustomFormExtraLogQuery joinWithDevice($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Device relation
 *
 * @method     ChildCustomFormExtraLogQuery leftJoinWithDevice() Adds a LEFT JOIN clause and with to the query using the Device relation
 * @method     ChildCustomFormExtraLogQuery rightJoinWithDevice() Adds a RIGHT JOIN clause and with to the query using the Device relation
 * @method     ChildCustomFormExtraLogQuery innerJoinWithDevice() Adds a INNER JOIN clause and with to the query using the Device relation
 *
 * @method     \Database\HubPlus\SectionQuery|\Database\HubPlus\DeviceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCustomFormExtraLog findOne(ConnectionInterface $con = null) Return the first ChildCustomFormExtraLog matching the query
 * @method     ChildCustomFormExtraLog findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCustomFormExtraLog matching the query, or a new ChildCustomFormExtraLog object populated from the query conditions when no match is found
 *
 * @method     ChildCustomFormExtraLog findOneById(int $id) Return the first ChildCustomFormExtraLog filtered by the id column
 * @method     ChildCustomFormExtraLog findOneByFormId(int $form_id) Return the first ChildCustomFormExtraLog filtered by the form_id column
 * @method     ChildCustomFormExtraLog findOneByRowId(int $row_id) Return the first ChildCustomFormExtraLog filtered by the row_id column
 * @method     ChildCustomFormExtraLog findOneByDeviceId(int $device_id) Return the first ChildCustomFormExtraLog filtered by the device_id column
 * @method     ChildCustomFormExtraLog findOneByType(string $type) Return the first ChildCustomFormExtraLog filtered by the type column
 * @method     ChildCustomFormExtraLog findOneByTimestamp(string $timestamp) Return the first ChildCustomFormExtraLog filtered by the timestamp column *

 * @method     ChildCustomFormExtraLog requirePk($key, ConnectionInterface $con = null) Return the ChildCustomFormExtraLog by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormExtraLog requireOne(ConnectionInterface $con = null) Return the first ChildCustomFormExtraLog matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomFormExtraLog requireOneById(int $id) Return the first ChildCustomFormExtraLog filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormExtraLog requireOneByFormId(int $form_id) Return the first ChildCustomFormExtraLog filtered by the form_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormExtraLog requireOneByRowId(int $row_id) Return the first ChildCustomFormExtraLog filtered by the row_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormExtraLog requireOneByDeviceId(int $device_id) Return the first ChildCustomFormExtraLog filtered by the device_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormExtraLog requireOneByType(string $type) Return the first ChildCustomFormExtraLog filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormExtraLog requireOneByTimestamp(string $timestamp) Return the first ChildCustomFormExtraLog filtered by the timestamp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomFormExtraLog[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCustomFormExtraLog objects based on current ModelCriteria
 * @method     ChildCustomFormExtraLog[]|ObjectCollection findById(int $id) Return ChildCustomFormExtraLog objects filtered by the id column
 * @method     ChildCustomFormExtraLog[]|ObjectCollection findByFormId(int $form_id) Return ChildCustomFormExtraLog objects filtered by the form_id column
 * @method     ChildCustomFormExtraLog[]|ObjectCollection findByRowId(int $row_id) Return ChildCustomFormExtraLog objects filtered by the row_id column
 * @method     ChildCustomFormExtraLog[]|ObjectCollection findByDeviceId(int $device_id) Return ChildCustomFormExtraLog objects filtered by the device_id column
 * @method     ChildCustomFormExtraLog[]|ObjectCollection findByType(string $type) Return ChildCustomFormExtraLog objects filtered by the type column
 * @method     ChildCustomFormExtraLog[]|ObjectCollection findByTimestamp(string $timestamp) Return ChildCustomFormExtraLog objects filtered by the timestamp column
 * @method     ChildCustomFormExtraLog[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CustomFormExtraLogQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\CustomFormExtraLogQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\CustomFormExtraLog', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCustomFormExtraLogQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCustomFormExtraLogQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCustomFormExtraLogQuery) {
            return $criteria;
        }
        $query = new ChildCustomFormExtraLogQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCustomFormExtraLog|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CustomFormExtraLogTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CustomFormExtraLogTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomFormExtraLog A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, form_id, row_id, device_id, type, timestamp FROM cf_extra_log WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCustomFormExtraLog $obj */
            $obj = new ChildCustomFormExtraLog();
            $obj->hydrate($row);
            CustomFormExtraLogTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCustomFormExtraLog|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CustomFormExtraLogTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CustomFormExtraLogTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CustomFormExtraLogTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CustomFormExtraLogTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormExtraLogTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the form_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFormId(1234); // WHERE form_id = 1234
     * $query->filterByFormId(array(12, 34)); // WHERE form_id IN (12, 34)
     * $query->filterByFormId(array('min' => 12)); // WHERE form_id > 12
     * </code>
     *
     * @see       filterBySection()
     *
     * @param     mixed $formId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function filterByFormId($formId = null, $comparison = null)
    {
        if (is_array($formId)) {
            $useMinMax = false;
            if (isset($formId['min'])) {
                $this->addUsingAlias(CustomFormExtraLogTableMap::COL_FORM_ID, $formId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($formId['max'])) {
                $this->addUsingAlias(CustomFormExtraLogTableMap::COL_FORM_ID, $formId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormExtraLogTableMap::COL_FORM_ID, $formId, $comparison);
    }

    /**
     * Filter the query on the row_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRowId(1234); // WHERE row_id = 1234
     * $query->filterByRowId(array(12, 34)); // WHERE row_id IN (12, 34)
     * $query->filterByRowId(array('min' => 12)); // WHERE row_id > 12
     * </code>
     *
     * @param     mixed $rowId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function filterByRowId($rowId = null, $comparison = null)
    {
        if (is_array($rowId)) {
            $useMinMax = false;
            if (isset($rowId['min'])) {
                $this->addUsingAlias(CustomFormExtraLogTableMap::COL_ROW_ID, $rowId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rowId['max'])) {
                $this->addUsingAlias(CustomFormExtraLogTableMap::COL_ROW_ID, $rowId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormExtraLogTableMap::COL_ROW_ID, $rowId, $comparison);
    }

    /**
     * Filter the query on the device_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDeviceId(1234); // WHERE device_id = 1234
     * $query->filterByDeviceId(array(12, 34)); // WHERE device_id IN (12, 34)
     * $query->filterByDeviceId(array('min' => 12)); // WHERE device_id > 12
     * </code>
     *
     * @see       filterByDevice()
     *
     * @param     mixed $deviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function filterByDeviceId($deviceId = null, $comparison = null)
    {
        if (is_array($deviceId)) {
            $useMinMax = false;
            if (isset($deviceId['min'])) {
                $this->addUsingAlias(CustomFormExtraLogTableMap::COL_DEVICE_ID, $deviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deviceId['max'])) {
                $this->addUsingAlias(CustomFormExtraLogTableMap::COL_DEVICE_ID, $deviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormExtraLogTableMap::COL_DEVICE_ID, $deviceId, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormExtraLogTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the timestamp column
     *
     * Example usage:
     * <code>
     * $query->filterByTimestamp('2011-03-14'); // WHERE timestamp = '2011-03-14'
     * $query->filterByTimestamp('now'); // WHERE timestamp = '2011-03-14'
     * $query->filterByTimestamp(array('max' => 'yesterday')); // WHERE timestamp > '2011-03-13'
     * </code>
     *
     * @param     mixed $timestamp The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function filterByTimestamp($timestamp = null, $comparison = null)
    {
        if (is_array($timestamp)) {
            $useMinMax = false;
            if (isset($timestamp['min'])) {
                $this->addUsingAlias(CustomFormExtraLogTableMap::COL_TIMESTAMP, $timestamp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($timestamp['max'])) {
                $this->addUsingAlias(CustomFormExtraLogTableMap::COL_TIMESTAMP, $timestamp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormExtraLogTableMap::COL_TIMESTAMP, $timestamp, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Section object
     *
     * @param \Database\HubPlus\Section|ObjectCollection $section The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function filterBySection($section, $comparison = null)
    {
        if ($section instanceof \Database\HubPlus\Section) {
            return $this
                ->addUsingAlias(CustomFormExtraLogTableMap::COL_FORM_ID, $section->getId(), $comparison);
        } elseif ($section instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomFormExtraLogTableMap::COL_FORM_ID, $section->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySection() only accepts arguments of type \Database\HubPlus\Section or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Section relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function joinSection($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Section');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Section');
        }

        return $this;
    }

    /**
     * Use the Section relation Section object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionQuery A secondary query class using the current class as primary query
     */
    public function useSectionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSection($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Section', '\Database\HubPlus\SectionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Device object
     *
     * @param \Database\HubPlus\Device|ObjectCollection $device The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function filterByDevice($device, $comparison = null)
    {
        if ($device instanceof \Database\HubPlus\Device) {
            return $this
                ->addUsingAlias(CustomFormExtraLogTableMap::COL_DEVICE_ID, $device->getId(), $comparison);
        } elseif ($device instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomFormExtraLogTableMap::COL_DEVICE_ID, $device->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDevice() only accepts arguments of type \Database\HubPlus\Device or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Device relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function joinDevice($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Device');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Device');
        }

        return $this;
    }

    /**
     * Use the Device relation Device object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\DeviceQuery A secondary query class using the current class as primary query
     */
    public function useDeviceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinDevice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Device', '\Database\HubPlus\DeviceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCustomFormExtraLog $customFormExtraLog Object to remove from the list of results
     *
     * @return $this|ChildCustomFormExtraLogQuery The current query, for fluid interface
     */
    public function prune($customFormExtraLog = null)
    {
        if ($customFormExtraLog) {
            $this->addUsingAlias(CustomFormExtraLogTableMap::COL_ID, $customFormExtraLog->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the cf_extra_log table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomFormExtraLogTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CustomFormExtraLogTableMap::clearInstancePool();
            CustomFormExtraLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomFormExtraLogTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CustomFormExtraLogTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CustomFormExtraLogTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CustomFormExtraLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CustomFormExtraLogQuery
