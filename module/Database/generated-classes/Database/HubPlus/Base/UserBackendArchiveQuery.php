<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\UserBackendArchive as ChildUserBackendArchive;
use Database\HubPlus\UserBackendArchiveQuery as ChildUserBackendArchiveQuery;
use Database\HubPlus\Map\UserBackendArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'user_backend_archive' table.
 *
 *
 *
 * @method     ChildUserBackendArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserBackendArchiveQuery orderByImagepath($order = Criteria::ASC) Order by the imagePath column
 * @method     ChildUserBackendArchiveQuery orderByFirstname($order = Criteria::ASC) Order by the firstname column
 * @method     ChildUserBackendArchiveQuery orderByLastname($order = Criteria::ASC) Order by the lastname column
 * @method     ChildUserBackendArchiveQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildUserBackendArchiveQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildUserBackendArchiveQuery orderByPasswordRequestedAt($order = Criteria::ASC) Order by the password_requested_at column
 * @method     ChildUserBackendArchiveQuery orderByConfirmationToken($order = Criteria::ASC) Order by the confirmation_token column
 * @method     ChildUserBackendArchiveQuery orderByLastLogin($order = Criteria::ASC) Order by the last_login column
 * @method     ChildUserBackendArchiveQuery orderByLocked($order = Criteria::ASC) Order by the locked column
 * @method     ChildUserBackendArchiveQuery orderByLockedAt($order = Criteria::ASC) Order by the locked_at column
 * @method     ChildUserBackendArchiveQuery orderByExpired($order = Criteria::ASC) Order by the expired column
 * @method     ChildUserBackendArchiveQuery orderByExpiredAt($order = Criteria::ASC) Order by the expired_at column
 * @method     ChildUserBackendArchiveQuery orderByRoleId($order = Criteria::ASC) Order by the role_id column
 * @method     ChildUserBackendArchiveQuery orderByPrivacy($order = Criteria::ASC) Order by the privacy column
 * @method     ChildUserBackendArchiveQuery orderByPrivacyAt($order = Criteria::ASC) Order by the privacy_at column
 * @method     ChildUserBackendArchiveQuery orderByTerms($order = Criteria::ASC) Order by the terms column
 * @method     ChildUserBackendArchiveQuery orderByTermsAt($order = Criteria::ASC) Order by the terms_at column
 * @method     ChildUserBackendArchiveQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildUserBackendArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildUserBackendArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildUserBackendArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildUserBackendArchiveQuery groupById() Group by the id column
 * @method     ChildUserBackendArchiveQuery groupByImagepath() Group by the imagePath column
 * @method     ChildUserBackendArchiveQuery groupByFirstname() Group by the firstname column
 * @method     ChildUserBackendArchiveQuery groupByLastname() Group by the lastname column
 * @method     ChildUserBackendArchiveQuery groupByEmail() Group by the email column
 * @method     ChildUserBackendArchiveQuery groupByPassword() Group by the password column
 * @method     ChildUserBackendArchiveQuery groupByPasswordRequestedAt() Group by the password_requested_at column
 * @method     ChildUserBackendArchiveQuery groupByConfirmationToken() Group by the confirmation_token column
 * @method     ChildUserBackendArchiveQuery groupByLastLogin() Group by the last_login column
 * @method     ChildUserBackendArchiveQuery groupByLocked() Group by the locked column
 * @method     ChildUserBackendArchiveQuery groupByLockedAt() Group by the locked_at column
 * @method     ChildUserBackendArchiveQuery groupByExpired() Group by the expired column
 * @method     ChildUserBackendArchiveQuery groupByExpiredAt() Group by the expired_at column
 * @method     ChildUserBackendArchiveQuery groupByRoleId() Group by the role_id column
 * @method     ChildUserBackendArchiveQuery groupByPrivacy() Group by the privacy column
 * @method     ChildUserBackendArchiveQuery groupByPrivacyAt() Group by the privacy_at column
 * @method     ChildUserBackendArchiveQuery groupByTerms() Group by the terms column
 * @method     ChildUserBackendArchiveQuery groupByTermsAt() Group by the terms_at column
 * @method     ChildUserBackendArchiveQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildUserBackendArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildUserBackendArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildUserBackendArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildUserBackendArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserBackendArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserBackendArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserBackendArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserBackendArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserBackendArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserBackendArchive findOne(ConnectionInterface $con = null) Return the first ChildUserBackendArchive matching the query
 * @method     ChildUserBackendArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUserBackendArchive matching the query, or a new ChildUserBackendArchive object populated from the query conditions when no match is found
 *
 * @method     ChildUserBackendArchive findOneById(int $id) Return the first ChildUserBackendArchive filtered by the id column
 * @method     ChildUserBackendArchive findOneByImagepath(string $imagePath) Return the first ChildUserBackendArchive filtered by the imagePath column
 * @method     ChildUserBackendArchive findOneByFirstname(string $firstname) Return the first ChildUserBackendArchive filtered by the firstname column
 * @method     ChildUserBackendArchive findOneByLastname(string $lastname) Return the first ChildUserBackendArchive filtered by the lastname column
 * @method     ChildUserBackendArchive findOneByEmail(string $email) Return the first ChildUserBackendArchive filtered by the email column
 * @method     ChildUserBackendArchive findOneByPassword(string $password) Return the first ChildUserBackendArchive filtered by the password column
 * @method     ChildUserBackendArchive findOneByPasswordRequestedAt(int $password_requested_at) Return the first ChildUserBackendArchive filtered by the password_requested_at column
 * @method     ChildUserBackendArchive findOneByConfirmationToken(string $confirmation_token) Return the first ChildUserBackendArchive filtered by the confirmation_token column
 * @method     ChildUserBackendArchive findOneByLastLogin(string $last_login) Return the first ChildUserBackendArchive filtered by the last_login column
 * @method     ChildUserBackendArchive findOneByLocked(boolean $locked) Return the first ChildUserBackendArchive filtered by the locked column
 * @method     ChildUserBackendArchive findOneByLockedAt(string $locked_at) Return the first ChildUserBackendArchive filtered by the locked_at column
 * @method     ChildUserBackendArchive findOneByExpired(boolean $expired) Return the first ChildUserBackendArchive filtered by the expired column
 * @method     ChildUserBackendArchive findOneByExpiredAt(string $expired_at) Return the first ChildUserBackendArchive filtered by the expired_at column
 * @method     ChildUserBackendArchive findOneByRoleId(int $role_id) Return the first ChildUserBackendArchive filtered by the role_id column
 * @method     ChildUserBackendArchive findOneByPrivacy(boolean $privacy) Return the first ChildUserBackendArchive filtered by the privacy column
 * @method     ChildUserBackendArchive findOneByPrivacyAt(string $privacy_at) Return the first ChildUserBackendArchive filtered by the privacy_at column
 * @method     ChildUserBackendArchive findOneByTerms(boolean $terms) Return the first ChildUserBackendArchive filtered by the terms column
 * @method     ChildUserBackendArchive findOneByTermsAt(string $terms_at) Return the first ChildUserBackendArchive filtered by the terms_at column
 * @method     ChildUserBackendArchive findOneByDeletedAt(string $deleted_at) Return the first ChildUserBackendArchive filtered by the deleted_at column
 * @method     ChildUserBackendArchive findOneByCreatedAt(string $created_at) Return the first ChildUserBackendArchive filtered by the created_at column
 * @method     ChildUserBackendArchive findOneByUpdatedAt(string $updated_at) Return the first ChildUserBackendArchive filtered by the updated_at column
 * @method     ChildUserBackendArchive findOneByArchivedAt(string $archived_at) Return the first ChildUserBackendArchive filtered by the archived_at column *

 * @method     ChildUserBackendArchive requirePk($key, ConnectionInterface $con = null) Return the ChildUserBackendArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOne(ConnectionInterface $con = null) Return the first ChildUserBackendArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserBackendArchive requireOneById(int $id) Return the first ChildUserBackendArchive filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByImagepath(string $imagePath) Return the first ChildUserBackendArchive filtered by the imagePath column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByFirstname(string $firstname) Return the first ChildUserBackendArchive filtered by the firstname column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByLastname(string $lastname) Return the first ChildUserBackendArchive filtered by the lastname column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByEmail(string $email) Return the first ChildUserBackendArchive filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByPassword(string $password) Return the first ChildUserBackendArchive filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByPasswordRequestedAt(int $password_requested_at) Return the first ChildUserBackendArchive filtered by the password_requested_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByConfirmationToken(string $confirmation_token) Return the first ChildUserBackendArchive filtered by the confirmation_token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByLastLogin(string $last_login) Return the first ChildUserBackendArchive filtered by the last_login column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByLocked(boolean $locked) Return the first ChildUserBackendArchive filtered by the locked column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByLockedAt(string $locked_at) Return the first ChildUserBackendArchive filtered by the locked_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByExpired(boolean $expired) Return the first ChildUserBackendArchive filtered by the expired column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByExpiredAt(string $expired_at) Return the first ChildUserBackendArchive filtered by the expired_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByRoleId(int $role_id) Return the first ChildUserBackendArchive filtered by the role_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByPrivacy(boolean $privacy) Return the first ChildUserBackendArchive filtered by the privacy column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByPrivacyAt(string $privacy_at) Return the first ChildUserBackendArchive filtered by the privacy_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByTerms(boolean $terms) Return the first ChildUserBackendArchive filtered by the terms column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByTermsAt(string $terms_at) Return the first ChildUserBackendArchive filtered by the terms_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByDeletedAt(string $deleted_at) Return the first ChildUserBackendArchive filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByCreatedAt(string $created_at) Return the first ChildUserBackendArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildUserBackendArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUserBackendArchive requireOneByArchivedAt(string $archived_at) Return the first ChildUserBackendArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUserBackendArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUserBackendArchive objects based on current ModelCriteria
 * @method     ChildUserBackendArchive[]|ObjectCollection findById(int $id) Return ChildUserBackendArchive objects filtered by the id column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByImagepath(string $imagePath) Return ChildUserBackendArchive objects filtered by the imagePath column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByFirstname(string $firstname) Return ChildUserBackendArchive objects filtered by the firstname column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByLastname(string $lastname) Return ChildUserBackendArchive objects filtered by the lastname column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByEmail(string $email) Return ChildUserBackendArchive objects filtered by the email column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByPassword(string $password) Return ChildUserBackendArchive objects filtered by the password column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByPasswordRequestedAt(int $password_requested_at) Return ChildUserBackendArchive objects filtered by the password_requested_at column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByConfirmationToken(string $confirmation_token) Return ChildUserBackendArchive objects filtered by the confirmation_token column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByLastLogin(string $last_login) Return ChildUserBackendArchive objects filtered by the last_login column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByLocked(boolean $locked) Return ChildUserBackendArchive objects filtered by the locked column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByLockedAt(string $locked_at) Return ChildUserBackendArchive objects filtered by the locked_at column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByExpired(boolean $expired) Return ChildUserBackendArchive objects filtered by the expired column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByExpiredAt(string $expired_at) Return ChildUserBackendArchive objects filtered by the expired_at column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByRoleId(int $role_id) Return ChildUserBackendArchive objects filtered by the role_id column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByPrivacy(boolean $privacy) Return ChildUserBackendArchive objects filtered by the privacy column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByPrivacyAt(string $privacy_at) Return ChildUserBackendArchive objects filtered by the privacy_at column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByTerms(boolean $terms) Return ChildUserBackendArchive objects filtered by the terms column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByTermsAt(string $terms_at) Return ChildUserBackendArchive objects filtered by the terms_at column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildUserBackendArchive objects filtered by the deleted_at column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildUserBackendArchive objects filtered by the created_at column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildUserBackendArchive objects filtered by the updated_at column
 * @method     ChildUserBackendArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildUserBackendArchive objects filtered by the archived_at column
 * @method     ChildUserBackendArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserBackendArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\UserBackendArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\UserBackendArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserBackendArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserBackendArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserBackendArchiveQuery) {
            return $criteria;
        }
        $query = new ChildUserBackendArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUserBackendArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserBackendArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserBackendArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserBackendArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, imagePath, firstname, lastname, email, password, password_requested_at, confirmation_token, last_login, locked, locked_at, expired, expired_at, role_id, privacy, privacy_at, terms, terms_at, deleted_at, created_at, updated_at, archived_at FROM user_backend_archive WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUserBackendArchive $obj */
            $obj = new ChildUserBackendArchive();
            $obj->hydrate($row);
            UserBackendArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUserBackendArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the imagePath column
     *
     * Example usage:
     * <code>
     * $query->filterByImagepath('fooValue');   // WHERE imagePath = 'fooValue'
     * $query->filterByImagepath('%fooValue%', Criteria::LIKE); // WHERE imagePath LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imagepath The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByImagepath($imagepath = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imagepath)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_IMAGEPATH, $imagepath, $comparison);
    }

    /**
     * Filter the query on the firstname column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstname('fooValue');   // WHERE firstname = 'fooValue'
     * $query->filterByFirstname('%fooValue%', Criteria::LIKE); // WHERE firstname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByFirstname($firstname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_FIRSTNAME, $firstname, $comparison);
    }

    /**
     * Filter the query on the lastname column
     *
     * Example usage:
     * <code>
     * $query->filterByLastname('fooValue');   // WHERE lastname = 'fooValue'
     * $query->filterByLastname('%fooValue%', Criteria::LIKE); // WHERE lastname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastname The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByLastname($lastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastname)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_LASTNAME, $lastname, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the password_requested_at column
     *
     * Example usage:
     * <code>
     * $query->filterByPasswordRequestedAt(1234); // WHERE password_requested_at = 1234
     * $query->filterByPasswordRequestedAt(array(12, 34)); // WHERE password_requested_at IN (12, 34)
     * $query->filterByPasswordRequestedAt(array('min' => 12)); // WHERE password_requested_at > 12
     * </code>
     *
     * @param     mixed $passwordRequestedAt The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByPasswordRequestedAt($passwordRequestedAt = null, $comparison = null)
    {
        if (is_array($passwordRequestedAt)) {
            $useMinMax = false;
            if (isset($passwordRequestedAt['min'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_PASSWORD_REQUESTED_AT, $passwordRequestedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($passwordRequestedAt['max'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_PASSWORD_REQUESTED_AT, $passwordRequestedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_PASSWORD_REQUESTED_AT, $passwordRequestedAt, $comparison);
    }

    /**
     * Filter the query on the confirmation_token column
     *
     * Example usage:
     * <code>
     * $query->filterByConfirmationToken('fooValue');   // WHERE confirmation_token = 'fooValue'
     * $query->filterByConfirmationToken('%fooValue%', Criteria::LIKE); // WHERE confirmation_token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $confirmationToken The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByConfirmationToken($confirmationToken = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($confirmationToken)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_CONFIRMATION_TOKEN, $confirmationToken, $comparison);
    }

    /**
     * Filter the query on the last_login column
     *
     * Example usage:
     * <code>
     * $query->filterByLastLogin('2011-03-14'); // WHERE last_login = '2011-03-14'
     * $query->filterByLastLogin('now'); // WHERE last_login = '2011-03-14'
     * $query->filterByLastLogin(array('max' => 'yesterday')); // WHERE last_login > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastLogin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByLastLogin($lastLogin = null, $comparison = null)
    {
        if (is_array($lastLogin)) {
            $useMinMax = false;
            if (isset($lastLogin['min'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_LAST_LOGIN, $lastLogin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastLogin['max'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_LAST_LOGIN, $lastLogin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_LAST_LOGIN, $lastLogin, $comparison);
    }

    /**
     * Filter the query on the locked column
     *
     * Example usage:
     * <code>
     * $query->filterByLocked(true); // WHERE locked = true
     * $query->filterByLocked('yes'); // WHERE locked = true
     * </code>
     *
     * @param     boolean|string $locked The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByLocked($locked = null, $comparison = null)
    {
        if (is_string($locked)) {
            $locked = in_array(strtolower($locked), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_LOCKED, $locked, $comparison);
    }

    /**
     * Filter the query on the locked_at column
     *
     * Example usage:
     * <code>
     * $query->filterByLockedAt('2011-03-14'); // WHERE locked_at = '2011-03-14'
     * $query->filterByLockedAt('now'); // WHERE locked_at = '2011-03-14'
     * $query->filterByLockedAt(array('max' => 'yesterday')); // WHERE locked_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $lockedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByLockedAt($lockedAt = null, $comparison = null)
    {
        if (is_array($lockedAt)) {
            $useMinMax = false;
            if (isset($lockedAt['min'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_LOCKED_AT, $lockedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lockedAt['max'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_LOCKED_AT, $lockedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_LOCKED_AT, $lockedAt, $comparison);
    }

    /**
     * Filter the query on the expired column
     *
     * Example usage:
     * <code>
     * $query->filterByExpired(true); // WHERE expired = true
     * $query->filterByExpired('yes'); // WHERE expired = true
     * </code>
     *
     * @param     boolean|string $expired The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByExpired($expired = null, $comparison = null)
    {
        if (is_string($expired)) {
            $expired = in_array(strtolower($expired), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_EXPIRED, $expired, $comparison);
    }

    /**
     * Filter the query on the expired_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiredAt('2011-03-14'); // WHERE expired_at = '2011-03-14'
     * $query->filterByExpiredAt('now'); // WHERE expired_at = '2011-03-14'
     * $query->filterByExpiredAt(array('max' => 'yesterday')); // WHERE expired_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiredAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByExpiredAt($expiredAt = null, $comparison = null)
    {
        if (is_array($expiredAt)) {
            $useMinMax = false;
            if (isset($expiredAt['min'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_EXPIRED_AT, $expiredAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiredAt['max'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_EXPIRED_AT, $expiredAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_EXPIRED_AT, $expiredAt, $comparison);
    }

    /**
     * Filter the query on the role_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRoleId(1234); // WHERE role_id = 1234
     * $query->filterByRoleId(array(12, 34)); // WHERE role_id IN (12, 34)
     * $query->filterByRoleId(array('min' => 12)); // WHERE role_id > 12
     * </code>
     *
     * @param     mixed $roleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByRoleId($roleId = null, $comparison = null)
    {
        if (is_array($roleId)) {
            $useMinMax = false;
            if (isset($roleId['min'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_ROLE_ID, $roleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($roleId['max'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_ROLE_ID, $roleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_ROLE_ID, $roleId, $comparison);
    }

    /**
     * Filter the query on the privacy column
     *
     * Example usage:
     * <code>
     * $query->filterByPrivacy(true); // WHERE privacy = true
     * $query->filterByPrivacy('yes'); // WHERE privacy = true
     * </code>
     *
     * @param     boolean|string $privacy The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByPrivacy($privacy = null, $comparison = null)
    {
        if (is_string($privacy)) {
            $privacy = in_array(strtolower($privacy), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_PRIVACY, $privacy, $comparison);
    }

    /**
     * Filter the query on the privacy_at column
     *
     * Example usage:
     * <code>
     * $query->filterByPrivacyAt('2011-03-14'); // WHERE privacy_at = '2011-03-14'
     * $query->filterByPrivacyAt('now'); // WHERE privacy_at = '2011-03-14'
     * $query->filterByPrivacyAt(array('max' => 'yesterday')); // WHERE privacy_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $privacyAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByPrivacyAt($privacyAt = null, $comparison = null)
    {
        if (is_array($privacyAt)) {
            $useMinMax = false;
            if (isset($privacyAt['min'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_PRIVACY_AT, $privacyAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($privacyAt['max'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_PRIVACY_AT, $privacyAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_PRIVACY_AT, $privacyAt, $comparison);
    }

    /**
     * Filter the query on the terms column
     *
     * Example usage:
     * <code>
     * $query->filterByTerms(true); // WHERE terms = true
     * $query->filterByTerms('yes'); // WHERE terms = true
     * </code>
     *
     * @param     boolean|string $terms The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByTerms($terms = null, $comparison = null)
    {
        if (is_string($terms)) {
            $terms = in_array(strtolower($terms), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_TERMS, $terms, $comparison);
    }

    /**
     * Filter the query on the terms_at column
     *
     * Example usage:
     * <code>
     * $query->filterByTermsAt('2011-03-14'); // WHERE terms_at = '2011-03-14'
     * $query->filterByTermsAt('now'); // WHERE terms_at = '2011-03-14'
     * $query->filterByTermsAt(array('max' => 'yesterday')); // WHERE terms_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $termsAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByTermsAt($termsAt = null, $comparison = null)
    {
        if (is_array($termsAt)) {
            $useMinMax = false;
            if (isset($termsAt['min'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_TERMS_AT, $termsAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($termsAt['max'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_TERMS_AT, $termsAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_TERMS_AT, $termsAt, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(UserBackendArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserBackendArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUserBackendArchive $userBackendArchive Object to remove from the list of results
     *
     * @return $this|ChildUserBackendArchiveQuery The current query, for fluid interface
     */
    public function prune($userBackendArchive = null)
    {
        if ($userBackendArchive) {
            $this->addUsingAlias(UserBackendArchiveTableMap::COL_ID, $userBackendArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the user_backend_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserBackendArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserBackendArchiveTableMap::clearInstancePool();
            UserBackendArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserBackendArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserBackendArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserBackendArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserBackendArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UserBackendArchiveQuery
