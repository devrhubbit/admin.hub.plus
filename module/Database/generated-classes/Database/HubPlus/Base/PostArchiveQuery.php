<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\PostArchive as ChildPostArchive;
use Database\HubPlus\PostArchiveQuery as ChildPostArchiveQuery;
use Database\HubPlus\Map\PostArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'post_archive' table.
 *
 *
 *
 * @method     ChildPostArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPostArchiveQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildPostArchiveQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildPostArchiveQuery orderByAuthorId($order = Criteria::ASC) Order by the author_id column
 * @method     ChildPostArchiveQuery orderByCoverId($order = Criteria::ASC) Order by the cover_id column
 * @method     ChildPostArchiveQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 * @method     ChildPostArchiveQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildPostArchiveQuery orderBySubtitle($order = Criteria::ASC) Order by the subtitle column
 * @method     ChildPostArchiveQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildPostArchiveQuery orderByBody($order = Criteria::ASC) Order by the body column
 * @method     ChildPostArchiveQuery orderBySearch($order = Criteria::ASC) Order by the search column
 * @method     ChildPostArchiveQuery orderByTemplateId($order = Criteria::ASC) Order by the template_id column
 * @method     ChildPostArchiveQuery orderByLayout($order = Criteria::ASC) Order by the layout column
 * @method     ChildPostArchiveQuery orderByTags($order = Criteria::ASC) Order by the tags column
 * @method     ChildPostArchiveQuery orderByTagsUser($order = Criteria::ASC) Order by the tags_user column
 * @method     ChildPostArchiveQuery orderByRowData($order = Criteria::ASC) Order by the row_data column
 * @method     ChildPostArchiveQuery orderByCountLike($order = Criteria::ASC) Order by the count_like column
 * @method     ChildPostArchiveQuery orderByCountShare($order = Criteria::ASC) Order by the count_share column
 * @method     ChildPostArchiveQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildPostArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildPostArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildPostArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildPostArchiveQuery groupById() Group by the id column
 * @method     ChildPostArchiveQuery groupByType() Group by the type column
 * @method     ChildPostArchiveQuery groupByStatus() Group by the status column
 * @method     ChildPostArchiveQuery groupByAuthorId() Group by the author_id column
 * @method     ChildPostArchiveQuery groupByCoverId() Group by the cover_id column
 * @method     ChildPostArchiveQuery groupBySlug() Group by the slug column
 * @method     ChildPostArchiveQuery groupByTitle() Group by the title column
 * @method     ChildPostArchiveQuery groupBySubtitle() Group by the subtitle column
 * @method     ChildPostArchiveQuery groupByDescription() Group by the description column
 * @method     ChildPostArchiveQuery groupByBody() Group by the body column
 * @method     ChildPostArchiveQuery groupBySearch() Group by the search column
 * @method     ChildPostArchiveQuery groupByTemplateId() Group by the template_id column
 * @method     ChildPostArchiveQuery groupByLayout() Group by the layout column
 * @method     ChildPostArchiveQuery groupByTags() Group by the tags column
 * @method     ChildPostArchiveQuery groupByTagsUser() Group by the tags_user column
 * @method     ChildPostArchiveQuery groupByRowData() Group by the row_data column
 * @method     ChildPostArchiveQuery groupByCountLike() Group by the count_like column
 * @method     ChildPostArchiveQuery groupByCountShare() Group by the count_share column
 * @method     ChildPostArchiveQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildPostArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildPostArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildPostArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildPostArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPostArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPostArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPostArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPostArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPostArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPostArchive findOne(ConnectionInterface $con = null) Return the first ChildPostArchive matching the query
 * @method     ChildPostArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPostArchive matching the query, or a new ChildPostArchive object populated from the query conditions when no match is found
 *
 * @method     ChildPostArchive findOneById(int $id) Return the first ChildPostArchive filtered by the id column
 * @method     ChildPostArchive findOneByType(string $type) Return the first ChildPostArchive filtered by the type column
 * @method     ChildPostArchive findOneByStatus(int $status) Return the first ChildPostArchive filtered by the status column
 * @method     ChildPostArchive findOneByAuthorId(int $author_id) Return the first ChildPostArchive filtered by the author_id column
 * @method     ChildPostArchive findOneByCoverId(int $cover_id) Return the first ChildPostArchive filtered by the cover_id column
 * @method     ChildPostArchive findOneBySlug(string $slug) Return the first ChildPostArchive filtered by the slug column
 * @method     ChildPostArchive findOneByTitle(string $title) Return the first ChildPostArchive filtered by the title column
 * @method     ChildPostArchive findOneBySubtitle(string $subtitle) Return the first ChildPostArchive filtered by the subtitle column
 * @method     ChildPostArchive findOneByDescription(string $description) Return the first ChildPostArchive filtered by the description column
 * @method     ChildPostArchive findOneByBody(string $body) Return the first ChildPostArchive filtered by the body column
 * @method     ChildPostArchive findOneBySearch(string $search) Return the first ChildPostArchive filtered by the search column
 * @method     ChildPostArchive findOneByTemplateId(int $template_id) Return the first ChildPostArchive filtered by the template_id column
 * @method     ChildPostArchive findOneByLayout(string $layout) Return the first ChildPostArchive filtered by the layout column
 * @method     ChildPostArchive findOneByTags(string $tags) Return the first ChildPostArchive filtered by the tags column
 * @method     ChildPostArchive findOneByTagsUser(string $tags_user) Return the first ChildPostArchive filtered by the tags_user column
 * @method     ChildPostArchive findOneByRowData(string $row_data) Return the first ChildPostArchive filtered by the row_data column
 * @method     ChildPostArchive findOneByCountLike(int $count_like) Return the first ChildPostArchive filtered by the count_like column
 * @method     ChildPostArchive findOneByCountShare(int $count_share) Return the first ChildPostArchive filtered by the count_share column
 * @method     ChildPostArchive findOneByDeletedAt(string $deleted_at) Return the first ChildPostArchive filtered by the deleted_at column
 * @method     ChildPostArchive findOneByCreatedAt(string $created_at) Return the first ChildPostArchive filtered by the created_at column
 * @method     ChildPostArchive findOneByUpdatedAt(string $updated_at) Return the first ChildPostArchive filtered by the updated_at column
 * @method     ChildPostArchive findOneByArchivedAt(string $archived_at) Return the first ChildPostArchive filtered by the archived_at column *

 * @method     ChildPostArchive requirePk($key, ConnectionInterface $con = null) Return the ChildPostArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOne(ConnectionInterface $con = null) Return the first ChildPostArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPostArchive requireOneById(int $id) Return the first ChildPostArchive filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByType(string $type) Return the first ChildPostArchive filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByStatus(int $status) Return the first ChildPostArchive filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByAuthorId(int $author_id) Return the first ChildPostArchive filtered by the author_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByCoverId(int $cover_id) Return the first ChildPostArchive filtered by the cover_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneBySlug(string $slug) Return the first ChildPostArchive filtered by the slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByTitle(string $title) Return the first ChildPostArchive filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneBySubtitle(string $subtitle) Return the first ChildPostArchive filtered by the subtitle column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByDescription(string $description) Return the first ChildPostArchive filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByBody(string $body) Return the first ChildPostArchive filtered by the body column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneBySearch(string $search) Return the first ChildPostArchive filtered by the search column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByTemplateId(int $template_id) Return the first ChildPostArchive filtered by the template_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByLayout(string $layout) Return the first ChildPostArchive filtered by the layout column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByTags(string $tags) Return the first ChildPostArchive filtered by the tags column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByTagsUser(string $tags_user) Return the first ChildPostArchive filtered by the tags_user column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByRowData(string $row_data) Return the first ChildPostArchive filtered by the row_data column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByCountLike(int $count_like) Return the first ChildPostArchive filtered by the count_like column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByCountShare(int $count_share) Return the first ChildPostArchive filtered by the count_share column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByDeletedAt(string $deleted_at) Return the first ChildPostArchive filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByCreatedAt(string $created_at) Return the first ChildPostArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildPostArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPostArchive requireOneByArchivedAt(string $archived_at) Return the first ChildPostArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPostArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPostArchive objects based on current ModelCriteria
 * @method     ChildPostArchive[]|ObjectCollection findById(int $id) Return ChildPostArchive objects filtered by the id column
 * @method     ChildPostArchive[]|ObjectCollection findByType(string $type) Return ChildPostArchive objects filtered by the type column
 * @method     ChildPostArchive[]|ObjectCollection findByStatus(int $status) Return ChildPostArchive objects filtered by the status column
 * @method     ChildPostArchive[]|ObjectCollection findByAuthorId(int $author_id) Return ChildPostArchive objects filtered by the author_id column
 * @method     ChildPostArchive[]|ObjectCollection findByCoverId(int $cover_id) Return ChildPostArchive objects filtered by the cover_id column
 * @method     ChildPostArchive[]|ObjectCollection findBySlug(string $slug) Return ChildPostArchive objects filtered by the slug column
 * @method     ChildPostArchive[]|ObjectCollection findByTitle(string $title) Return ChildPostArchive objects filtered by the title column
 * @method     ChildPostArchive[]|ObjectCollection findBySubtitle(string $subtitle) Return ChildPostArchive objects filtered by the subtitle column
 * @method     ChildPostArchive[]|ObjectCollection findByDescription(string $description) Return ChildPostArchive objects filtered by the description column
 * @method     ChildPostArchive[]|ObjectCollection findByBody(string $body) Return ChildPostArchive objects filtered by the body column
 * @method     ChildPostArchive[]|ObjectCollection findBySearch(string $search) Return ChildPostArchive objects filtered by the search column
 * @method     ChildPostArchive[]|ObjectCollection findByTemplateId(int $template_id) Return ChildPostArchive objects filtered by the template_id column
 * @method     ChildPostArchive[]|ObjectCollection findByLayout(string $layout) Return ChildPostArchive objects filtered by the layout column
 * @method     ChildPostArchive[]|ObjectCollection findByTags(string $tags) Return ChildPostArchive objects filtered by the tags column
 * @method     ChildPostArchive[]|ObjectCollection findByTagsUser(string $tags_user) Return ChildPostArchive objects filtered by the tags_user column
 * @method     ChildPostArchive[]|ObjectCollection findByRowData(string $row_data) Return ChildPostArchive objects filtered by the row_data column
 * @method     ChildPostArchive[]|ObjectCollection findByCountLike(int $count_like) Return ChildPostArchive objects filtered by the count_like column
 * @method     ChildPostArchive[]|ObjectCollection findByCountShare(int $count_share) Return ChildPostArchive objects filtered by the count_share column
 * @method     ChildPostArchive[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildPostArchive objects filtered by the deleted_at column
 * @method     ChildPostArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildPostArchive objects filtered by the created_at column
 * @method     ChildPostArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildPostArchive objects filtered by the updated_at column
 * @method     ChildPostArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildPostArchive objects filtered by the archived_at column
 * @method     ChildPostArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PostArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\PostArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\PostArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPostArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPostArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPostArchiveQuery) {
            return $criteria;
        }
        $query = new ChildPostArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPostArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PostArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PostArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPostArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, type, status, author_id, cover_id, slug, title, subtitle, description, body, search, template_id, layout, tags, tags_user, row_data, count_like, count_share, deleted_at, created_at, updated_at, archived_at FROM post_archive WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPostArchive $obj */
            $obj = new ChildPostArchive();
            $obj->hydrate($row);
            PostArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPostArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PostArchiveTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PostArchiveTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the author_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAuthorId(1234); // WHERE author_id = 1234
     * $query->filterByAuthorId(array(12, 34)); // WHERE author_id IN (12, 34)
     * $query->filterByAuthorId(array('min' => 12)); // WHERE author_id > 12
     * </code>
     *
     * @param     mixed $authorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByAuthorId($authorId = null, $comparison = null)
    {
        if (is_array($authorId)) {
            $useMinMax = false;
            if (isset($authorId['min'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_AUTHOR_ID, $authorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($authorId['max'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_AUTHOR_ID, $authorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_AUTHOR_ID, $authorId, $comparison);
    }

    /**
     * Filter the query on the cover_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCoverId(1234); // WHERE cover_id = 1234
     * $query->filterByCoverId(array(12, 34)); // WHERE cover_id IN (12, 34)
     * $query->filterByCoverId(array('min' => 12)); // WHERE cover_id > 12
     * </code>
     *
     * @param     mixed $coverId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByCoverId($coverId = null, $comparison = null)
    {
        if (is_array($coverId)) {
            $useMinMax = false;
            if (isset($coverId['min'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_COVER_ID, $coverId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($coverId['max'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_COVER_ID, $coverId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_COVER_ID, $coverId, $comparison);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%', Criteria::LIKE); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the subtitle column
     *
     * Example usage:
     * <code>
     * $query->filterBySubtitle('fooValue');   // WHERE subtitle = 'fooValue'
     * $query->filterBySubtitle('%fooValue%', Criteria::LIKE); // WHERE subtitle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $subtitle The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterBySubtitle($subtitle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($subtitle)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_SUBTITLE, $subtitle, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the body column
     *
     * Example usage:
     * <code>
     * $query->filterByBody('fooValue');   // WHERE body = 'fooValue'
     * $query->filterByBody('%fooValue%', Criteria::LIKE); // WHERE body LIKE '%fooValue%'
     * </code>
     *
     * @param     string $body The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByBody($body = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($body)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_BODY, $body, $comparison);
    }

    /**
     * Filter the query on the search column
     *
     * Example usage:
     * <code>
     * $query->filterBySearch('fooValue');   // WHERE search = 'fooValue'
     * $query->filterBySearch('%fooValue%', Criteria::LIKE); // WHERE search LIKE '%fooValue%'
     * </code>
     *
     * @param     string $search The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterBySearch($search = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($search)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_SEARCH, $search, $comparison);
    }

    /**
     * Filter the query on the template_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTemplateId(1234); // WHERE template_id = 1234
     * $query->filterByTemplateId(array(12, 34)); // WHERE template_id IN (12, 34)
     * $query->filterByTemplateId(array('min' => 12)); // WHERE template_id > 12
     * </code>
     *
     * @param     mixed $templateId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByTemplateId($templateId = null, $comparison = null)
    {
        if (is_array($templateId)) {
            $useMinMax = false;
            if (isset($templateId['min'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_TEMPLATE_ID, $templateId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($templateId['max'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_TEMPLATE_ID, $templateId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_TEMPLATE_ID, $templateId, $comparison);
    }

    /**
     * Filter the query on the layout column
     *
     * Example usage:
     * <code>
     * $query->filterByLayout('fooValue');   // WHERE layout = 'fooValue'
     * $query->filterByLayout('%fooValue%', Criteria::LIKE); // WHERE layout LIKE '%fooValue%'
     * </code>
     *
     * @param     string $layout The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByLayout($layout = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($layout)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_LAYOUT, $layout, $comparison);
    }

    /**
     * Filter the query on the tags column
     *
     * Example usage:
     * <code>
     * $query->filterByTags('fooValue');   // WHERE tags = 'fooValue'
     * $query->filterByTags('%fooValue%', Criteria::LIKE); // WHERE tags LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tags The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByTags($tags = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tags)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_TAGS, $tags, $comparison);
    }

    /**
     * Filter the query on the tags_user column
     *
     * Example usage:
     * <code>
     * $query->filterByTagsUser('fooValue');   // WHERE tags_user = 'fooValue'
     * $query->filterByTagsUser('%fooValue%', Criteria::LIKE); // WHERE tags_user LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tagsUser The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByTagsUser($tagsUser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tagsUser)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_TAGS_USER, $tagsUser, $comparison);
    }

    /**
     * Filter the query on the row_data column
     *
     * Example usage:
     * <code>
     * $query->filterByRowData('fooValue');   // WHERE row_data = 'fooValue'
     * $query->filterByRowData('%fooValue%', Criteria::LIKE); // WHERE row_data LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rowData The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByRowData($rowData = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rowData)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_ROW_DATA, $rowData, $comparison);
    }

    /**
     * Filter the query on the count_like column
     *
     * Example usage:
     * <code>
     * $query->filterByCountLike(1234); // WHERE count_like = 1234
     * $query->filterByCountLike(array(12, 34)); // WHERE count_like IN (12, 34)
     * $query->filterByCountLike(array('min' => 12)); // WHERE count_like > 12
     * </code>
     *
     * @param     mixed $countLike The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByCountLike($countLike = null, $comparison = null)
    {
        if (is_array($countLike)) {
            $useMinMax = false;
            if (isset($countLike['min'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_COUNT_LIKE, $countLike['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countLike['max'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_COUNT_LIKE, $countLike['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_COUNT_LIKE, $countLike, $comparison);
    }

    /**
     * Filter the query on the count_share column
     *
     * Example usage:
     * <code>
     * $query->filterByCountShare(1234); // WHERE count_share = 1234
     * $query->filterByCountShare(array(12, 34)); // WHERE count_share IN (12, 34)
     * $query->filterByCountShare(array('min' => 12)); // WHERE count_share > 12
     * </code>
     *
     * @param     mixed $countShare The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByCountShare($countShare = null, $comparison = null)
    {
        if (is_array($countShare)) {
            $useMinMax = false;
            if (isset($countShare['min'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_COUNT_SHARE, $countShare['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countShare['max'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_COUNT_SHARE, $countShare['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_COUNT_SHARE, $countShare, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(PostArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PostArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPostArchive $postArchive Object to remove from the list of results
     *
     * @return $this|ChildPostArchiveQuery The current query, for fluid interface
     */
    public function prune($postArchive = null)
    {
        if ($postArchive) {
            $this->addUsingAlias(PostArchiveTableMap::COL_ID, $postArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the post_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PostArchiveTableMap::clearInstancePool();
            PostArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PostArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PostArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PostArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PostArchiveQuery
