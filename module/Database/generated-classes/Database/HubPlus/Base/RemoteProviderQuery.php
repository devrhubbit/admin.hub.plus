<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\RemoteProvider as ChildRemoteProvider;
use Database\HubPlus\RemoteProviderArchive as ChildRemoteProviderArchive;
use Database\HubPlus\RemoteProviderQuery as ChildRemoteProviderQuery;
use Database\HubPlus\Map\RemoteProviderTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'remote_provider' table.
 *
 *
 *
 * @method     ChildRemoteProviderQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildRemoteProviderQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method     ChildRemoteProviderQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildRemoteProviderQuery orderByEmailCanonical($order = Criteria::ASC) Order by the email_canonical column
 * @method     ChildRemoteProviderQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildRemoteProviderQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 * @method     ChildRemoteProviderQuery orderByIcon($order = Criteria::ASC) Order by the icon column
 * @method     ChildRemoteProviderQuery orderBySite($order = Criteria::ASC) Order by the site column
 * @method     ChildRemoteProviderQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildRemoteProviderQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildRemoteProviderQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildRemoteProviderQuery groupById() Group by the id column
 * @method     ChildRemoteProviderQuery groupByUuid() Group by the uuid column
 * @method     ChildRemoteProviderQuery groupByEmail() Group by the email column
 * @method     ChildRemoteProviderQuery groupByEmailCanonical() Group by the email_canonical column
 * @method     ChildRemoteProviderQuery groupByName() Group by the name column
 * @method     ChildRemoteProviderQuery groupBySlug() Group by the slug column
 * @method     ChildRemoteProviderQuery groupByIcon() Group by the icon column
 * @method     ChildRemoteProviderQuery groupBySite() Group by the site column
 * @method     ChildRemoteProviderQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildRemoteProviderQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildRemoteProviderQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildRemoteProviderQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRemoteProviderQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRemoteProviderQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRemoteProviderQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRemoteProviderQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRemoteProviderQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRemoteProviderQuery leftJoinPostConnector($relationAlias = null) Adds a LEFT JOIN clause to the query using the PostConnector relation
 * @method     ChildRemoteProviderQuery rightJoinPostConnector($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PostConnector relation
 * @method     ChildRemoteProviderQuery innerJoinPostConnector($relationAlias = null) Adds a INNER JOIN clause to the query using the PostConnector relation
 *
 * @method     ChildRemoteProviderQuery joinWithPostConnector($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PostConnector relation
 *
 * @method     ChildRemoteProviderQuery leftJoinWithPostConnector() Adds a LEFT JOIN clause and with to the query using the PostConnector relation
 * @method     ChildRemoteProviderQuery rightJoinWithPostConnector() Adds a RIGHT JOIN clause and with to the query using the PostConnector relation
 * @method     ChildRemoteProviderQuery innerJoinWithPostConnector() Adds a INNER JOIN clause and with to the query using the PostConnector relation
 *
 * @method     ChildRemoteProviderQuery leftJoinSectionConnector($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionConnector relation
 * @method     ChildRemoteProviderQuery rightJoinSectionConnector($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionConnector relation
 * @method     ChildRemoteProviderQuery innerJoinSectionConnector($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionConnector relation
 *
 * @method     ChildRemoteProviderQuery joinWithSectionConnector($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionConnector relation
 *
 * @method     ChildRemoteProviderQuery leftJoinWithSectionConnector() Adds a LEFT JOIN clause and with to the query using the SectionConnector relation
 * @method     ChildRemoteProviderQuery rightJoinWithSectionConnector() Adds a RIGHT JOIN clause and with to the query using the SectionConnector relation
 * @method     ChildRemoteProviderQuery innerJoinWithSectionConnector() Adds a INNER JOIN clause and with to the query using the SectionConnector relation
 *
 * @method     ChildRemoteProviderQuery leftJoinWebhook($relationAlias = null) Adds a LEFT JOIN clause to the query using the Webhook relation
 * @method     ChildRemoteProviderQuery rightJoinWebhook($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Webhook relation
 * @method     ChildRemoteProviderQuery innerJoinWebhook($relationAlias = null) Adds a INNER JOIN clause to the query using the Webhook relation
 *
 * @method     ChildRemoteProviderQuery joinWithWebhook($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Webhook relation
 *
 * @method     ChildRemoteProviderQuery leftJoinWithWebhook() Adds a LEFT JOIN clause and with to the query using the Webhook relation
 * @method     ChildRemoteProviderQuery rightJoinWithWebhook() Adds a RIGHT JOIN clause and with to the query using the Webhook relation
 * @method     ChildRemoteProviderQuery innerJoinWithWebhook() Adds a INNER JOIN clause and with to the query using the Webhook relation
 *
 * @method     \Database\HubPlus\PostConnectorQuery|\Database\HubPlus\SectionConnectorQuery|\Database\HubPlus\WebhookQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRemoteProvider findOne(ConnectionInterface $con = null) Return the first ChildRemoteProvider matching the query
 * @method     ChildRemoteProvider findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRemoteProvider matching the query, or a new ChildRemoteProvider object populated from the query conditions when no match is found
 *
 * @method     ChildRemoteProvider findOneById(int $id) Return the first ChildRemoteProvider filtered by the id column
 * @method     ChildRemoteProvider findOneByUuid(string $uuid) Return the first ChildRemoteProvider filtered by the uuid column
 * @method     ChildRemoteProvider findOneByEmail(string $email) Return the first ChildRemoteProvider filtered by the email column
 * @method     ChildRemoteProvider findOneByEmailCanonical(string $email_canonical) Return the first ChildRemoteProvider filtered by the email_canonical column
 * @method     ChildRemoteProvider findOneByName(string $name) Return the first ChildRemoteProvider filtered by the name column
 * @method     ChildRemoteProvider findOneBySlug(string $slug) Return the first ChildRemoteProvider filtered by the slug column
 * @method     ChildRemoteProvider findOneByIcon(string $icon) Return the first ChildRemoteProvider filtered by the icon column
 * @method     ChildRemoteProvider findOneBySite(string $site) Return the first ChildRemoteProvider filtered by the site column
 * @method     ChildRemoteProvider findOneByDeletedAt(string $deleted_at) Return the first ChildRemoteProvider filtered by the deleted_at column
 * @method     ChildRemoteProvider findOneByCreatedAt(string $created_at) Return the first ChildRemoteProvider filtered by the created_at column
 * @method     ChildRemoteProvider findOneByUpdatedAt(string $updated_at) Return the first ChildRemoteProvider filtered by the updated_at column *

 * @method     ChildRemoteProvider requirePk($key, ConnectionInterface $con = null) Return the ChildRemoteProvider by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteProvider requireOne(ConnectionInterface $con = null) Return the first ChildRemoteProvider matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRemoteProvider requireOneById(int $id) Return the first ChildRemoteProvider filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteProvider requireOneByUuid(string $uuid) Return the first ChildRemoteProvider filtered by the uuid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteProvider requireOneByEmail(string $email) Return the first ChildRemoteProvider filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteProvider requireOneByEmailCanonical(string $email_canonical) Return the first ChildRemoteProvider filtered by the email_canonical column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteProvider requireOneByName(string $name) Return the first ChildRemoteProvider filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteProvider requireOneBySlug(string $slug) Return the first ChildRemoteProvider filtered by the slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteProvider requireOneByIcon(string $icon) Return the first ChildRemoteProvider filtered by the icon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteProvider requireOneBySite(string $site) Return the first ChildRemoteProvider filtered by the site column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteProvider requireOneByDeletedAt(string $deleted_at) Return the first ChildRemoteProvider filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteProvider requireOneByCreatedAt(string $created_at) Return the first ChildRemoteProvider filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteProvider requireOneByUpdatedAt(string $updated_at) Return the first ChildRemoteProvider filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRemoteProvider[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRemoteProvider objects based on current ModelCriteria
 * @method     ChildRemoteProvider[]|ObjectCollection findById(int $id) Return ChildRemoteProvider objects filtered by the id column
 * @method     ChildRemoteProvider[]|ObjectCollection findByUuid(string $uuid) Return ChildRemoteProvider objects filtered by the uuid column
 * @method     ChildRemoteProvider[]|ObjectCollection findByEmail(string $email) Return ChildRemoteProvider objects filtered by the email column
 * @method     ChildRemoteProvider[]|ObjectCollection findByEmailCanonical(string $email_canonical) Return ChildRemoteProvider objects filtered by the email_canonical column
 * @method     ChildRemoteProvider[]|ObjectCollection findByName(string $name) Return ChildRemoteProvider objects filtered by the name column
 * @method     ChildRemoteProvider[]|ObjectCollection findBySlug(string $slug) Return ChildRemoteProvider objects filtered by the slug column
 * @method     ChildRemoteProvider[]|ObjectCollection findByIcon(string $icon) Return ChildRemoteProvider objects filtered by the icon column
 * @method     ChildRemoteProvider[]|ObjectCollection findBySite(string $site) Return ChildRemoteProvider objects filtered by the site column
 * @method     ChildRemoteProvider[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildRemoteProvider objects filtered by the deleted_at column
 * @method     ChildRemoteProvider[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildRemoteProvider objects filtered by the created_at column
 * @method     ChildRemoteProvider[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildRemoteProvider objects filtered by the updated_at column
 * @method     ChildRemoteProvider[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RemoteProviderQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\RemoteProviderQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\RemoteProvider', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRemoteProviderQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRemoteProviderQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRemoteProviderQuery) {
            return $criteria;
        }
        $query = new ChildRemoteProviderQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRemoteProvider|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RemoteProviderTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RemoteProviderTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRemoteProvider A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, uuid, email, email_canonical, name, slug, icon, site, deleted_at, created_at, updated_at FROM remote_provider WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRemoteProvider $obj */
            $obj = new ChildRemoteProvider();
            $obj->hydrate($row);
            RemoteProviderTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRemoteProvider|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RemoteProviderTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RemoteProviderTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RemoteProviderTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RemoteProviderTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteProviderTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%', Criteria::LIKE); // WHERE uuid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uuid The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteProviderTableMap::COL_UUID, $uuid, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteProviderTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the email_canonical column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailCanonical('fooValue');   // WHERE email_canonical = 'fooValue'
     * $query->filterByEmailCanonical('%fooValue%', Criteria::LIKE); // WHERE email_canonical LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailCanonical The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterByEmailCanonical($emailCanonical = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailCanonical)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteProviderTableMap::COL_EMAIL_CANONICAL, $emailCanonical, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteProviderTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%', Criteria::LIKE); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteProviderTableMap::COL_SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the icon column
     *
     * Example usage:
     * <code>
     * $query->filterByIcon('fooValue');   // WHERE icon = 'fooValue'
     * $query->filterByIcon('%fooValue%', Criteria::LIKE); // WHERE icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $icon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterByIcon($icon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($icon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteProviderTableMap::COL_ICON, $icon, $comparison);
    }

    /**
     * Filter the query on the site column
     *
     * Example usage:
     * <code>
     * $query->filterBySite('fooValue');   // WHERE site = 'fooValue'
     * $query->filterBySite('%fooValue%', Criteria::LIKE); // WHERE site LIKE '%fooValue%'
     * </code>
     *
     * @param     string $site The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterBySite($site = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($site)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteProviderTableMap::COL_SITE, $site, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(RemoteProviderTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(RemoteProviderTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteProviderTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(RemoteProviderTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(RemoteProviderTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteProviderTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(RemoteProviderTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(RemoteProviderTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteProviderTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\PostConnector object
     *
     * @param \Database\HubPlus\PostConnector|ObjectCollection $postConnector the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterByPostConnector($postConnector, $comparison = null)
    {
        if ($postConnector instanceof \Database\HubPlus\PostConnector) {
            return $this
                ->addUsingAlias(RemoteProviderTableMap::COL_ID, $postConnector->getProviderId(), $comparison);
        } elseif ($postConnector instanceof ObjectCollection) {
            return $this
                ->usePostConnectorQuery()
                ->filterByPrimaryKeys($postConnector->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPostConnector() only accepts arguments of type \Database\HubPlus\PostConnector or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PostConnector relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function joinPostConnector($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PostConnector');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PostConnector');
        }

        return $this;
    }

    /**
     * Use the PostConnector relation PostConnector object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostConnectorQuery A secondary query class using the current class as primary query
     */
    public function usePostConnectorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPostConnector($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PostConnector', '\Database\HubPlus\PostConnectorQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\SectionConnector object
     *
     * @param \Database\HubPlus\SectionConnector|ObjectCollection $sectionConnector the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterBySectionConnector($sectionConnector, $comparison = null)
    {
        if ($sectionConnector instanceof \Database\HubPlus\SectionConnector) {
            return $this
                ->addUsingAlias(RemoteProviderTableMap::COL_ID, $sectionConnector->getProviderId(), $comparison);
        } elseif ($sectionConnector instanceof ObjectCollection) {
            return $this
                ->useSectionConnectorQuery()
                ->filterByPrimaryKeys($sectionConnector->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySectionConnector() only accepts arguments of type \Database\HubPlus\SectionConnector or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionConnector relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function joinSectionConnector($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionConnector');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionConnector');
        }

        return $this;
    }

    /**
     * Use the SectionConnector relation SectionConnector object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionConnectorQuery A secondary query class using the current class as primary query
     */
    public function useSectionConnectorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSectionConnector($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionConnector', '\Database\HubPlus\SectionConnectorQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Webhook object
     *
     * @param \Database\HubPlus\Webhook|ObjectCollection $webhook the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function filterByWebhook($webhook, $comparison = null)
    {
        if ($webhook instanceof \Database\HubPlus\Webhook) {
            return $this
                ->addUsingAlias(RemoteProviderTableMap::COL_ID, $webhook->getProviderId(), $comparison);
        } elseif ($webhook instanceof ObjectCollection) {
            return $this
                ->useWebhookQuery()
                ->filterByPrimaryKeys($webhook->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWebhook() only accepts arguments of type \Database\HubPlus\Webhook or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Webhook relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function joinWebhook($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Webhook');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Webhook');
        }

        return $this;
    }

    /**
     * Use the Webhook relation Webhook object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\WebhookQuery A secondary query class using the current class as primary query
     */
    public function useWebhookQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWebhook($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Webhook', '\Database\HubPlus\WebhookQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRemoteProvider $remoteProvider Object to remove from the list of results
     *
     * @return $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function prune($remoteProvider = null)
    {
        if ($remoteProvider) {
            $this->addUsingAlias(RemoteProviderTableMap::COL_ID, $remoteProvider->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the remote_provider table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteProviderTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RemoteProviderTableMap::clearInstancePool();
            RemoteProviderTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteProviderTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RemoteProviderTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RemoteProviderTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RemoteProviderTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(RemoteProviderTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(RemoteProviderTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(RemoteProviderTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(RemoteProviderTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(RemoteProviderTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildRemoteProviderQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(RemoteProviderTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildRemoteProviderArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteProviderTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // RemoteProviderQuery
