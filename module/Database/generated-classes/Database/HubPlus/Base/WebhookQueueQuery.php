<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\WebhookQueue as ChildWebhookQueue;
use Database\HubPlus\WebhookQueueQuery as ChildWebhookQueueQuery;
use Database\HubPlus\Map\WebhookQueueTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'webhook_queue' table.
 *
 *
 *
 * @method     ChildWebhookQueueQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildWebhookQueueQuery orderByWebhookId($order = Criteria::ASC) Order by the webhook_id column
 * @method     ChildWebhookQueueQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildWebhookQueueQuery orderByFailedAttempts($order = Criteria::ASC) Order by the failed_attempts column
 * @method     ChildWebhookQueueQuery orderByMaxAttempts($order = Criteria::ASC) Order by the max_attempts column
 * @method     ChildWebhookQueueQuery orderByMethod($order = Criteria::ASC) Order by the method column
 * @method     ChildWebhookQueueQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method     ChildWebhookQueueQuery orderByBundle($order = Criteria::ASC) Order by the bundle column
 * @method     ChildWebhookQueueQuery orderByPayload($order = Criteria::ASC) Order by the payload column
 * @method     ChildWebhookQueueQuery orderByResponseStatus($order = Criteria::ASC) Order by the response_status column
 * @method     ChildWebhookQueueQuery orderByResponseLog($order = Criteria::ASC) Order by the response_log column
 * @method     ChildWebhookQueueQuery orderByToSyncAt($order = Criteria::ASC) Order by the to_sync_at column
 * @method     ChildWebhookQueueQuery orderBySyncedAt($order = Criteria::ASC) Order by the synced_at column
 * @method     ChildWebhookQueueQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildWebhookQueueQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildWebhookQueueQuery groupById() Group by the id column
 * @method     ChildWebhookQueueQuery groupByWebhookId() Group by the webhook_id column
 * @method     ChildWebhookQueueQuery groupByStatus() Group by the status column
 * @method     ChildWebhookQueueQuery groupByFailedAttempts() Group by the failed_attempts column
 * @method     ChildWebhookQueueQuery groupByMaxAttempts() Group by the max_attempts column
 * @method     ChildWebhookQueueQuery groupByMethod() Group by the method column
 * @method     ChildWebhookQueueQuery groupByUrl() Group by the url column
 * @method     ChildWebhookQueueQuery groupByBundle() Group by the bundle column
 * @method     ChildWebhookQueueQuery groupByPayload() Group by the payload column
 * @method     ChildWebhookQueueQuery groupByResponseStatus() Group by the response_status column
 * @method     ChildWebhookQueueQuery groupByResponseLog() Group by the response_log column
 * @method     ChildWebhookQueueQuery groupByToSyncAt() Group by the to_sync_at column
 * @method     ChildWebhookQueueQuery groupBySyncedAt() Group by the synced_at column
 * @method     ChildWebhookQueueQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildWebhookQueueQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildWebhookQueueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildWebhookQueueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildWebhookQueueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildWebhookQueueQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildWebhookQueueQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildWebhookQueueQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildWebhookQueueQuery leftJoinWebhook($relationAlias = null) Adds a LEFT JOIN clause to the query using the Webhook relation
 * @method     ChildWebhookQueueQuery rightJoinWebhook($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Webhook relation
 * @method     ChildWebhookQueueQuery innerJoinWebhook($relationAlias = null) Adds a INNER JOIN clause to the query using the Webhook relation
 *
 * @method     ChildWebhookQueueQuery joinWithWebhook($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Webhook relation
 *
 * @method     ChildWebhookQueueQuery leftJoinWithWebhook() Adds a LEFT JOIN clause and with to the query using the Webhook relation
 * @method     ChildWebhookQueueQuery rightJoinWithWebhook() Adds a RIGHT JOIN clause and with to the query using the Webhook relation
 * @method     ChildWebhookQueueQuery innerJoinWithWebhook() Adds a INNER JOIN clause and with to the query using the Webhook relation
 *
 * @method     \Database\HubPlus\WebhookQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildWebhookQueue findOne(ConnectionInterface $con = null) Return the first ChildWebhookQueue matching the query
 * @method     ChildWebhookQueue findOneOrCreate(ConnectionInterface $con = null) Return the first ChildWebhookQueue matching the query, or a new ChildWebhookQueue object populated from the query conditions when no match is found
 *
 * @method     ChildWebhookQueue findOneById(int $id) Return the first ChildWebhookQueue filtered by the id column
 * @method     ChildWebhookQueue findOneByWebhookId(int $webhook_id) Return the first ChildWebhookQueue filtered by the webhook_id column
 * @method     ChildWebhookQueue findOneByStatus(int $status) Return the first ChildWebhookQueue filtered by the status column
 * @method     ChildWebhookQueue findOneByFailedAttempts(int $failed_attempts) Return the first ChildWebhookQueue filtered by the failed_attempts column
 * @method     ChildWebhookQueue findOneByMaxAttempts(int $max_attempts) Return the first ChildWebhookQueue filtered by the max_attempts column
 * @method     ChildWebhookQueue findOneByMethod(string $method) Return the first ChildWebhookQueue filtered by the method column
 * @method     ChildWebhookQueue findOneByUrl(string $url) Return the first ChildWebhookQueue filtered by the url column
 * @method     ChildWebhookQueue findOneByBundle(string $bundle) Return the first ChildWebhookQueue filtered by the bundle column
 * @method     ChildWebhookQueue findOneByPayload(string $payload) Return the first ChildWebhookQueue filtered by the payload column
 * @method     ChildWebhookQueue findOneByResponseStatus(int $response_status) Return the first ChildWebhookQueue filtered by the response_status column
 * @method     ChildWebhookQueue findOneByResponseLog(string $response_log) Return the first ChildWebhookQueue filtered by the response_log column
 * @method     ChildWebhookQueue findOneByToSyncAt(string $to_sync_at) Return the first ChildWebhookQueue filtered by the to_sync_at column
 * @method     ChildWebhookQueue findOneBySyncedAt(string $synced_at) Return the first ChildWebhookQueue filtered by the synced_at column
 * @method     ChildWebhookQueue findOneByCreatedAt(string $created_at) Return the first ChildWebhookQueue filtered by the created_at column
 * @method     ChildWebhookQueue findOneByUpdatedAt(string $updated_at) Return the first ChildWebhookQueue filtered by the updated_at column *

 * @method     ChildWebhookQueue requirePk($key, ConnectionInterface $con = null) Return the ChildWebhookQueue by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOne(ConnectionInterface $con = null) Return the first ChildWebhookQueue matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildWebhookQueue requireOneById(int $id) Return the first ChildWebhookQueue filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByWebhookId(int $webhook_id) Return the first ChildWebhookQueue filtered by the webhook_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByStatus(int $status) Return the first ChildWebhookQueue filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByFailedAttempts(int $failed_attempts) Return the first ChildWebhookQueue filtered by the failed_attempts column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByMaxAttempts(int $max_attempts) Return the first ChildWebhookQueue filtered by the max_attempts column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByMethod(string $method) Return the first ChildWebhookQueue filtered by the method column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByUrl(string $url) Return the first ChildWebhookQueue filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByBundle(string $bundle) Return the first ChildWebhookQueue filtered by the bundle column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByPayload(string $payload) Return the first ChildWebhookQueue filtered by the payload column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByResponseStatus(int $response_status) Return the first ChildWebhookQueue filtered by the response_status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByResponseLog(string $response_log) Return the first ChildWebhookQueue filtered by the response_log column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByToSyncAt(string $to_sync_at) Return the first ChildWebhookQueue filtered by the to_sync_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneBySyncedAt(string $synced_at) Return the first ChildWebhookQueue filtered by the synced_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByCreatedAt(string $created_at) Return the first ChildWebhookQueue filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhookQueue requireOneByUpdatedAt(string $updated_at) Return the first ChildWebhookQueue filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildWebhookQueue[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildWebhookQueue objects based on current ModelCriteria
 * @method     ChildWebhookQueue[]|ObjectCollection findById(int $id) Return ChildWebhookQueue objects filtered by the id column
 * @method     ChildWebhookQueue[]|ObjectCollection findByWebhookId(int $webhook_id) Return ChildWebhookQueue objects filtered by the webhook_id column
 * @method     ChildWebhookQueue[]|ObjectCollection findByStatus(int $status) Return ChildWebhookQueue objects filtered by the status column
 * @method     ChildWebhookQueue[]|ObjectCollection findByFailedAttempts(int $failed_attempts) Return ChildWebhookQueue objects filtered by the failed_attempts column
 * @method     ChildWebhookQueue[]|ObjectCollection findByMaxAttempts(int $max_attempts) Return ChildWebhookQueue objects filtered by the max_attempts column
 * @method     ChildWebhookQueue[]|ObjectCollection findByMethod(string $method) Return ChildWebhookQueue objects filtered by the method column
 * @method     ChildWebhookQueue[]|ObjectCollection findByUrl(string $url) Return ChildWebhookQueue objects filtered by the url column
 * @method     ChildWebhookQueue[]|ObjectCollection findByBundle(string $bundle) Return ChildWebhookQueue objects filtered by the bundle column
 * @method     ChildWebhookQueue[]|ObjectCollection findByPayload(string $payload) Return ChildWebhookQueue objects filtered by the payload column
 * @method     ChildWebhookQueue[]|ObjectCollection findByResponseStatus(int $response_status) Return ChildWebhookQueue objects filtered by the response_status column
 * @method     ChildWebhookQueue[]|ObjectCollection findByResponseLog(string $response_log) Return ChildWebhookQueue objects filtered by the response_log column
 * @method     ChildWebhookQueue[]|ObjectCollection findByToSyncAt(string $to_sync_at) Return ChildWebhookQueue objects filtered by the to_sync_at column
 * @method     ChildWebhookQueue[]|ObjectCollection findBySyncedAt(string $synced_at) Return ChildWebhookQueue objects filtered by the synced_at column
 * @method     ChildWebhookQueue[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildWebhookQueue objects filtered by the created_at column
 * @method     ChildWebhookQueue[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildWebhookQueue objects filtered by the updated_at column
 * @method     ChildWebhookQueue[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class WebhookQueueQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\WebhookQueueQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\WebhookQueue', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildWebhookQueueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildWebhookQueueQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildWebhookQueueQuery) {
            return $criteria;
        }
        $query = new ChildWebhookQueueQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildWebhookQueue|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(WebhookQueueTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = WebhookQueueTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWebhookQueue A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, webhook_id, status, failed_attempts, max_attempts, method, url, bundle, payload, response_status, response_log, to_sync_at, synced_at, created_at, updated_at FROM webhook_queue WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildWebhookQueue $obj */
            $obj = new ChildWebhookQueue();
            $obj->hydrate($row);
            WebhookQueueTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildWebhookQueue|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(WebhookQueueTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(WebhookQueueTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the webhook_id column
     *
     * Example usage:
     * <code>
     * $query->filterByWebhookId(1234); // WHERE webhook_id = 1234
     * $query->filterByWebhookId(array(12, 34)); // WHERE webhook_id IN (12, 34)
     * $query->filterByWebhookId(array('min' => 12)); // WHERE webhook_id > 12
     * </code>
     *
     * @see       filterByWebhook()
     *
     * @param     mixed $webhookId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByWebhookId($webhookId = null, $comparison = null)
    {
        if (is_array($webhookId)) {
            $useMinMax = false;
            if (isset($webhookId['min'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_WEBHOOK_ID, $webhookId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($webhookId['max'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_WEBHOOK_ID, $webhookId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_WEBHOOK_ID, $webhookId, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the failed_attempts column
     *
     * Example usage:
     * <code>
     * $query->filterByFailedAttempts(1234); // WHERE failed_attempts = 1234
     * $query->filterByFailedAttempts(array(12, 34)); // WHERE failed_attempts IN (12, 34)
     * $query->filterByFailedAttempts(array('min' => 12)); // WHERE failed_attempts > 12
     * </code>
     *
     * @param     mixed $failedAttempts The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByFailedAttempts($failedAttempts = null, $comparison = null)
    {
        if (is_array($failedAttempts)) {
            $useMinMax = false;
            if (isset($failedAttempts['min'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_FAILED_ATTEMPTS, $failedAttempts['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($failedAttempts['max'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_FAILED_ATTEMPTS, $failedAttempts['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_FAILED_ATTEMPTS, $failedAttempts, $comparison);
    }

    /**
     * Filter the query on the max_attempts column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxAttempts(1234); // WHERE max_attempts = 1234
     * $query->filterByMaxAttempts(array(12, 34)); // WHERE max_attempts IN (12, 34)
     * $query->filterByMaxAttempts(array('min' => 12)); // WHERE max_attempts > 12
     * </code>
     *
     * @param     mixed $maxAttempts The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByMaxAttempts($maxAttempts = null, $comparison = null)
    {
        if (is_array($maxAttempts)) {
            $useMinMax = false;
            if (isset($maxAttempts['min'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_MAX_ATTEMPTS, $maxAttempts['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxAttempts['max'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_MAX_ATTEMPTS, $maxAttempts['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_MAX_ATTEMPTS, $maxAttempts, $comparison);
    }

    /**
     * Filter the query on the method column
     *
     * Example usage:
     * <code>
     * $query->filterByMethod('fooValue');   // WHERE method = 'fooValue'
     * $query->filterByMethod('%fooValue%', Criteria::LIKE); // WHERE method LIKE '%fooValue%'
     * </code>
     *
     * @param     string $method The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByMethod($method = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($method)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_METHOD, $method, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the bundle column
     *
     * Example usage:
     * <code>
     * $query->filterByBundle('fooValue');   // WHERE bundle = 'fooValue'
     * $query->filterByBundle('%fooValue%', Criteria::LIKE); // WHERE bundle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bundle The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByBundle($bundle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bundle)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_BUNDLE, $bundle, $comparison);
    }

    /**
     * Filter the query on the payload column
     *
     * Example usage:
     * <code>
     * $query->filterByPayload('fooValue');   // WHERE payload = 'fooValue'
     * $query->filterByPayload('%fooValue%', Criteria::LIKE); // WHERE payload LIKE '%fooValue%'
     * </code>
     *
     * @param     string $payload The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByPayload($payload = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($payload)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_PAYLOAD, $payload, $comparison);
    }

    /**
     * Filter the query on the response_status column
     *
     * Example usage:
     * <code>
     * $query->filterByResponseStatus(1234); // WHERE response_status = 1234
     * $query->filterByResponseStatus(array(12, 34)); // WHERE response_status IN (12, 34)
     * $query->filterByResponseStatus(array('min' => 12)); // WHERE response_status > 12
     * </code>
     *
     * @param     mixed $responseStatus The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByResponseStatus($responseStatus = null, $comparison = null)
    {
        if (is_array($responseStatus)) {
            $useMinMax = false;
            if (isset($responseStatus['min'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_RESPONSE_STATUS, $responseStatus['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($responseStatus['max'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_RESPONSE_STATUS, $responseStatus['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_RESPONSE_STATUS, $responseStatus, $comparison);
    }

    /**
     * Filter the query on the response_log column
     *
     * Example usage:
     * <code>
     * $query->filterByResponseLog('fooValue');   // WHERE response_log = 'fooValue'
     * $query->filterByResponseLog('%fooValue%', Criteria::LIKE); // WHERE response_log LIKE '%fooValue%'
     * </code>
     *
     * @param     string $responseLog The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByResponseLog($responseLog = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($responseLog)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_RESPONSE_LOG, $responseLog, $comparison);
    }

    /**
     * Filter the query on the to_sync_at column
     *
     * Example usage:
     * <code>
     * $query->filterByToSyncAt('2011-03-14'); // WHERE to_sync_at = '2011-03-14'
     * $query->filterByToSyncAt('now'); // WHERE to_sync_at = '2011-03-14'
     * $query->filterByToSyncAt(array('max' => 'yesterday')); // WHERE to_sync_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $toSyncAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByToSyncAt($toSyncAt = null, $comparison = null)
    {
        if (is_array($toSyncAt)) {
            $useMinMax = false;
            if (isset($toSyncAt['min'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_TO_SYNC_AT, $toSyncAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($toSyncAt['max'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_TO_SYNC_AT, $toSyncAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_TO_SYNC_AT, $toSyncAt, $comparison);
    }

    /**
     * Filter the query on the synced_at column
     *
     * Example usage:
     * <code>
     * $query->filterBySyncedAt('2011-03-14'); // WHERE synced_at = '2011-03-14'
     * $query->filterBySyncedAt('now'); // WHERE synced_at = '2011-03-14'
     * $query->filterBySyncedAt(array('max' => 'yesterday')); // WHERE synced_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $syncedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterBySyncedAt($syncedAt = null, $comparison = null)
    {
        if (is_array($syncedAt)) {
            $useMinMax = false;
            if (isset($syncedAt['min'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_SYNCED_AT, $syncedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($syncedAt['max'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_SYNCED_AT, $syncedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_SYNCED_AT, $syncedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(WebhookQueueTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookQueueTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Webhook object
     *
     * @param \Database\HubPlus\Webhook|ObjectCollection $webhook The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function filterByWebhook($webhook, $comparison = null)
    {
        if ($webhook instanceof \Database\HubPlus\Webhook) {
            return $this
                ->addUsingAlias(WebhookQueueTableMap::COL_WEBHOOK_ID, $webhook->getId(), $comparison);
        } elseif ($webhook instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WebhookQueueTableMap::COL_WEBHOOK_ID, $webhook->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByWebhook() only accepts arguments of type \Database\HubPlus\Webhook or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Webhook relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function joinWebhook($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Webhook');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Webhook');
        }

        return $this;
    }

    /**
     * Use the Webhook relation Webhook object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\WebhookQuery A secondary query class using the current class as primary query
     */
    public function useWebhookQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWebhook($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Webhook', '\Database\HubPlus\WebhookQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildWebhookQueue $webhookQueue Object to remove from the list of results
     *
     * @return $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function prune($webhookQueue = null)
    {
        if ($webhookQueue) {
            $this->addUsingAlias(WebhookQueueTableMap::COL_ID, $webhookQueue->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the webhook_queue table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WebhookQueueTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            WebhookQueueTableMap::clearInstancePool();
            WebhookQueueTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WebhookQueueTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(WebhookQueueTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            WebhookQueueTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            WebhookQueueTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(WebhookQueueTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(WebhookQueueTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(WebhookQueueTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(WebhookQueueTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(WebhookQueueTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildWebhookQueueQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(WebhookQueueTableMap::COL_CREATED_AT);
    }

} // WebhookQueueQuery
