<?php

namespace Database\HubPlus\Base;

use \DateTime;
use \Exception;
use \PDO;
use Database\HubPlus\CustomFormExtraLog as ChildCustomFormExtraLog;
use Database\HubPlus\CustomFormExtraLogQuery as ChildCustomFormExtraLogQuery;
use Database\HubPlus\CustomFormLog as ChildCustomFormLog;
use Database\HubPlus\CustomFormLogQuery as ChildCustomFormLogQuery;
use Database\HubPlus\Media as ChildMedia;
use Database\HubPlus\MediaQuery as ChildMediaQuery;
use Database\HubPlus\PostAction as ChildPostAction;
use Database\HubPlus\PostActionQuery as ChildPostActionQuery;
use Database\HubPlus\PushNotification as ChildPushNotification;
use Database\HubPlus\PushNotificationQuery as ChildPushNotificationQuery;
use Database\HubPlus\Section as ChildSection;
use Database\HubPlus\SectionArchive as ChildSectionArchive;
use Database\HubPlus\SectionArchiveQuery as ChildSectionArchiveQuery;
use Database\HubPlus\SectionConnector as ChildSectionConnector;
use Database\HubPlus\SectionConnectorQuery as ChildSectionConnectorQuery;
use Database\HubPlus\SectionQuery as ChildSectionQuery;
use Database\HubPlus\SectionRelatedTo as ChildSectionRelatedTo;
use Database\HubPlus\SectionRelatedToQuery as ChildSectionRelatedToQuery;
use Database\HubPlus\Template as ChildTemplate;
use Database\HubPlus\TemplateQuery as ChildTemplateQuery;
use Database\HubPlus\UserApp as ChildUserApp;
use Database\HubPlus\UserAppQuery as ChildUserAppQuery;
use Database\HubPlus\UserBackend as ChildUserBackend;
use Database\HubPlus\UserBackendQuery as ChildUserBackendQuery;
use Database\HubPlus\Webhook as ChildWebhook;
use Database\HubPlus\WebhookQuery as ChildWebhookQuery;
use Database\HubPlus\Map\CustomFormExtraLogTableMap;
use Database\HubPlus\Map\CustomFormLogTableMap;
use Database\HubPlus\Map\PostActionTableMap;
use Database\HubPlus\Map\PushNotificationTableMap;
use Database\HubPlus\Map\SectionConnectorTableMap;
use Database\HubPlus\Map\SectionRelatedToTableMap;
use Database\HubPlus\Map\SectionTableMap;
use Database\HubPlus\Map\UserAppTableMap;
use Database\HubPlus\Map\WebhookTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'section' table.
 *
 *
 *
 * @package    propel.generator.Database.HubPlus.Base
 */
abstract class Section implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Database\\HubPlus\\Map\\SectionTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the area field.
     *
     * @var        string
     */
    protected $area;

    /**
     * The value for the type field.
     *
     * @var        string
     */
    protected $type;

    /**
     * The value for the author_id field.
     *
     * @var        int
     */
    protected $author_id;

    /**
     * The value for the title field.
     *
     * @var        string
     */
    protected $title;

    /**
     * The value for the description field.
     *
     * @var        string
     */
    protected $description;

    /**
     * The value for the slug field.
     *
     * @var        string
     */
    protected $slug;

    /**
     * The value for the icon_id field.
     *
     * @var        int
     */
    protected $icon_id;

    /**
     * The value for the icon_selected_id field.
     *
     * @var        int
     */
    protected $icon_selected_id;

    /**
     * The value for the resource field.
     *
     * @var        string
     */
    protected $resource;

    /**
     * The value for the params field.
     *
     * @var        string
     */
    protected $params;

    /**
     * The value for the template_id field.
     *
     * @var        int
     */
    protected $template_id;

    /**
     * The value for the layout field.
     *
     * @var        string
     */
    protected $layout;

    /**
     * The value for the tags field.
     *
     * @var        string
     */
    protected $tags;

    /**
     * The value for the tags_user field.
     *
     * @var        string
     */
    protected $tags_user;

    /**
     * The value for the parsed_tags field.
     *
     * @var        string
     */
    protected $parsed_tags;

    /**
     * The value for the status field.
     *
     * @var        boolean
     */
    protected $status;

    /**
     * The value for the weight field.
     *
     * @var        int
     */
    protected $weight;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildTemplate
     */
    protected $aTemplate;

    /**
     * @var        ChildMedia
     */
    protected $aMediaRelatedByIconId;

    /**
     * @var        ChildMedia
     */
    protected $aMediaRelatedByIconSelectedId;

    /**
     * @var        ChildUserBackend
     */
    protected $aUserBackend;

    /**
     * @var        ObjectCollection|ChildCustomFormExtraLog[] Collection to store aggregation of ChildCustomFormExtraLog objects.
     */
    protected $collCustomFormExtraLogs;
    protected $collCustomFormExtraLogsPartial;

    /**
     * @var        ObjectCollection|ChildCustomFormLog[] Collection to store aggregation of ChildCustomFormLog objects.
     */
    protected $collCustomFormLogs;
    protected $collCustomFormLogsPartial;

    /**
     * @var        ObjectCollection|ChildPostAction[] Collection to store aggregation of ChildPostAction objects.
     */
    protected $collPostActions;
    protected $collPostActionsPartial;

    /**
     * @var        ObjectCollection|ChildPushNotification[] Collection to store aggregation of ChildPushNotification objects.
     */
    protected $collPushNotifications;
    protected $collPushNotificationsPartial;

    /**
     * @var        ObjectCollection|ChildSectionConnector[] Collection to store aggregation of ChildSectionConnector objects.
     */
    protected $collSectionConnectorsRelatedBySectionId;
    protected $collSectionConnectorsRelatedBySectionIdPartial;

    /**
     * @var        ObjectCollection|ChildSectionConnector[] Collection to store aggregation of ChildSectionConnector objects.
     */
    protected $collSectionConnectorsRelatedByRemoteSectionId;
    protected $collSectionConnectorsRelatedByRemoteSectionIdPartial;

    /**
     * @var        ObjectCollection|ChildSectionRelatedTo[] Collection to store aggregation of ChildSectionRelatedTo objects.
     */
    protected $collSectionRelatedTosRelatedByFromSectionId;
    protected $collSectionRelatedTosRelatedByFromSectionIdPartial;

    /**
     * @var        ObjectCollection|ChildSectionRelatedTo[] Collection to store aggregation of ChildSectionRelatedTo objects.
     */
    protected $collSectionRelatedTosRelatedByToSectionId;
    protected $collSectionRelatedTosRelatedByToSectionIdPartial;

    /**
     * @var        ObjectCollection|ChildUserApp[] Collection to store aggregation of ChildUserApp objects.
     */
    protected $collUserApps;
    protected $collUserAppsPartial;

    /**
     * @var        ObjectCollection|ChildWebhook[] Collection to store aggregation of ChildWebhook objects.
     */
    protected $collWebhooks;
    protected $collWebhooksPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCustomFormExtraLog[]
     */
    protected $customFormExtraLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCustomFormLog[]
     */
    protected $customFormLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostAction[]
     */
    protected $postActionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPushNotification[]
     */
    protected $pushNotificationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSectionConnector[]
     */
    protected $sectionConnectorsRelatedBySectionIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSectionConnector[]
     */
    protected $sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSectionRelatedTo[]
     */
    protected $sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSectionRelatedTo[]
     */
    protected $sectionRelatedTosRelatedByToSectionIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUserApp[]
     */
    protected $userAppsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildWebhook[]
     */
    protected $webhooksScheduledForDeletion = null;

    /**
     * Initializes internal state of Database\HubPlus\Base\Section object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Section</code> instance.  If
     * <code>obj</code> is an instance of <code>Section</code>, delegates to
     * <code>equals(Section)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Section The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [area] column value.
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Get the [type] column value.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the [author_id] column value.
     *
     * @return int
     */
    public function getAuthorId()
    {
        return $this->author_id;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [slug] column value.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get the [icon_id] column value.
     *
     * @return int
     */
    public function getIconId()
    {
        return $this->icon_id;
    }

    /**
     * Get the [icon_selected_id] column value.
     *
     * @return int
     */
    public function getIconSelectedId()
    {
        return $this->icon_selected_id;
    }

    /**
     * Get the [resource] column value.
     *
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Get the [params] column value.
     *
     * @return string
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Get the [template_id] column value.
     *
     * @return int
     */
    public function getTemplateId()
    {
        return $this->template_id;
    }

    /**
     * Get the [layout] column value.
     *
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Get the [tags] column value.
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get the [tags_user] column value.
     *
     * @return string
     */
    public function getTagsUser()
    {
        return $this->tags_user;
    }

    /**
     * Get the [parsed_tags] column value.
     *
     * @return string
     */
    public function getParsedTags()
    {
        return $this->parsed_tags;
    }

    /**
     * Get the [status] column value.
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [status] column value.
     *
     * @return boolean
     */
    public function isStatus()
    {
        return $this->getStatus();
    }

    /**
     * Get the [weight] column value.
     *
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[SectionTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [area] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setArea($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->area !== $v) {
            $this->area = $v;
            $this->modifiedColumns[SectionTableMap::COL_AREA] = true;
        }

        return $this;
    } // setArea()

    /**
     * Set the value of [type] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[SectionTableMap::COL_TYPE] = true;
        }

        return $this;
    } // setType()

    /**
     * Set the value of [author_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setAuthorId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->author_id !== $v) {
            $this->author_id = $v;
            $this->modifiedColumns[SectionTableMap::COL_AUTHOR_ID] = true;
        }

        if ($this->aUserBackend !== null && $this->aUserBackend->getId() !== $v) {
            $this->aUserBackend = null;
        }

        return $this;
    } // setAuthorId()

    /**
     * Set the value of [title] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[SectionTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [description] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[SectionTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [slug] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->slug !== $v) {
            $this->slug = $v;
            $this->modifiedColumns[SectionTableMap::COL_SLUG] = true;
        }

        return $this;
    } // setSlug()

    /**
     * Set the value of [icon_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setIconId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->icon_id !== $v) {
            $this->icon_id = $v;
            $this->modifiedColumns[SectionTableMap::COL_ICON_ID] = true;
        }

        if ($this->aMediaRelatedByIconId !== null && $this->aMediaRelatedByIconId->getId() !== $v) {
            $this->aMediaRelatedByIconId = null;
        }

        return $this;
    } // setIconId()

    /**
     * Set the value of [icon_selected_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setIconSelectedId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->icon_selected_id !== $v) {
            $this->icon_selected_id = $v;
            $this->modifiedColumns[SectionTableMap::COL_ICON_SELECTED_ID] = true;
        }

        if ($this->aMediaRelatedByIconSelectedId !== null && $this->aMediaRelatedByIconSelectedId->getId() !== $v) {
            $this->aMediaRelatedByIconSelectedId = null;
        }

        return $this;
    } // setIconSelectedId()

    /**
     * Set the value of [resource] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setResource($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->resource !== $v) {
            $this->resource = $v;
            $this->modifiedColumns[SectionTableMap::COL_RESOURCE] = true;
        }

        return $this;
    } // setResource()

    /**
     * Set the value of [params] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setParams($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->params !== $v) {
            $this->params = $v;
            $this->modifiedColumns[SectionTableMap::COL_PARAMS] = true;
        }

        return $this;
    } // setParams()

    /**
     * Set the value of [template_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setTemplateId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->template_id !== $v) {
            $this->template_id = $v;
            $this->modifiedColumns[SectionTableMap::COL_TEMPLATE_ID] = true;
        }

        if ($this->aTemplate !== null && $this->aTemplate->getId() !== $v) {
            $this->aTemplate = null;
        }

        return $this;
    } // setTemplateId()

    /**
     * Set the value of [layout] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setLayout($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->layout !== $v) {
            $this->layout = $v;
            $this->modifiedColumns[SectionTableMap::COL_LAYOUT] = true;
        }

        return $this;
    } // setLayout()

    /**
     * Set the value of [tags] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setTags($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tags !== $v) {
            $this->tags = $v;
            $this->modifiedColumns[SectionTableMap::COL_TAGS] = true;
        }

        return $this;
    } // setTags()

    /**
     * Set the value of [tags_user] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setTagsUser($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tags_user !== $v) {
            $this->tags_user = $v;
            $this->modifiedColumns[SectionTableMap::COL_TAGS_USER] = true;
        }

        return $this;
    } // setTagsUser()

    /**
     * Set the value of [parsed_tags] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setParsedTags($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->parsed_tags !== $v) {
            $this->parsed_tags = $v;
            $this->modifiedColumns[SectionTableMap::COL_PARSED_TAGS] = true;
        }

        return $this;
    } // setParsedTags()

    /**
     * Sets the value of the [status] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[SectionTableMap::COL_STATUS] = true;
        }

        return $this;
    } // setStatus()

    /**
     * Set the value of [weight] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setWeight($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->weight !== $v) {
            $this->weight = $v;
            $this->modifiedColumns[SectionTableMap::COL_WEIGHT] = true;
        }

        return $this;
    } // setWeight()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SectionTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SectionTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SectionTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SectionTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SectionTableMap::translateFieldName('Area', TableMap::TYPE_PHPNAME, $indexType)];
            $this->area = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SectionTableMap::translateFieldName('Type', TableMap::TYPE_PHPNAME, $indexType)];
            $this->type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SectionTableMap::translateFieldName('AuthorId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->author_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SectionTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : SectionTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : SectionTableMap::translateFieldName('Slug', TableMap::TYPE_PHPNAME, $indexType)];
            $this->slug = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : SectionTableMap::translateFieldName('IconId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->icon_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : SectionTableMap::translateFieldName('IconSelectedId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->icon_selected_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : SectionTableMap::translateFieldName('Resource', TableMap::TYPE_PHPNAME, $indexType)];
            $this->resource = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : SectionTableMap::translateFieldName('Params', TableMap::TYPE_PHPNAME, $indexType)];
            $this->params = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : SectionTableMap::translateFieldName('TemplateId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->template_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : SectionTableMap::translateFieldName('Layout', TableMap::TYPE_PHPNAME, $indexType)];
            $this->layout = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : SectionTableMap::translateFieldName('Tags', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tags = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : SectionTableMap::translateFieldName('TagsUser', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tags_user = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : SectionTableMap::translateFieldName('ParsedTags', TableMap::TYPE_PHPNAME, $indexType)];
            $this->parsed_tags = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : SectionTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : SectionTableMap::translateFieldName('Weight', TableMap::TYPE_PHPNAME, $indexType)];
            $this->weight = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : SectionTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : SectionTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : SectionTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 21; // 21 = SectionTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Database\\HubPlus\\Section'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aUserBackend !== null && $this->author_id !== $this->aUserBackend->getId()) {
            $this->aUserBackend = null;
        }
        if ($this->aMediaRelatedByIconId !== null && $this->icon_id !== $this->aMediaRelatedByIconId->getId()) {
            $this->aMediaRelatedByIconId = null;
        }
        if ($this->aMediaRelatedByIconSelectedId !== null && $this->icon_selected_id !== $this->aMediaRelatedByIconSelectedId->getId()) {
            $this->aMediaRelatedByIconSelectedId = null;
        }
        if ($this->aTemplate !== null && $this->template_id !== $this->aTemplate->getId()) {
            $this->aTemplate = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SectionTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSectionQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aTemplate = null;
            $this->aMediaRelatedByIconId = null;
            $this->aMediaRelatedByIconSelectedId = null;
            $this->aUserBackend = null;
            $this->collCustomFormExtraLogs = null;

            $this->collCustomFormLogs = null;

            $this->collPostActions = null;

            $this->collPushNotifications = null;

            $this->collSectionConnectorsRelatedBySectionId = null;

            $this->collSectionConnectorsRelatedByRemoteSectionId = null;

            $this->collSectionRelatedTosRelatedByFromSectionId = null;

            $this->collSectionRelatedTosRelatedByToSectionId = null;

            $this->collUserApps = null;

            $this->collWebhooks = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Section::setDeleted()
     * @see Section::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSectionQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildSectionQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(SectionTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(SectionTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(SectionTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SectionTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aTemplate !== null) {
                if ($this->aTemplate->isModified() || $this->aTemplate->isNew()) {
                    $affectedRows += $this->aTemplate->save($con);
                }
                $this->setTemplate($this->aTemplate);
            }

            if ($this->aMediaRelatedByIconId !== null) {
                if ($this->aMediaRelatedByIconId->isModified() || $this->aMediaRelatedByIconId->isNew()) {
                    $affectedRows += $this->aMediaRelatedByIconId->save($con);
                }
                $this->setMediaRelatedByIconId($this->aMediaRelatedByIconId);
            }

            if ($this->aMediaRelatedByIconSelectedId !== null) {
                if ($this->aMediaRelatedByIconSelectedId->isModified() || $this->aMediaRelatedByIconSelectedId->isNew()) {
                    $affectedRows += $this->aMediaRelatedByIconSelectedId->save($con);
                }
                $this->setMediaRelatedByIconSelectedId($this->aMediaRelatedByIconSelectedId);
            }

            if ($this->aUserBackend !== null) {
                if ($this->aUserBackend->isModified() || $this->aUserBackend->isNew()) {
                    $affectedRows += $this->aUserBackend->save($con);
                }
                $this->setUserBackend($this->aUserBackend);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->customFormExtraLogsScheduledForDeletion !== null) {
                if (!$this->customFormExtraLogsScheduledForDeletion->isEmpty()) {
                    foreach ($this->customFormExtraLogsScheduledForDeletion as $customFormExtraLog) {
                        // need to save related object because we set the relation to null
                        $customFormExtraLog->save($con);
                    }
                    $this->customFormExtraLogsScheduledForDeletion = null;
                }
            }

            if ($this->collCustomFormExtraLogs !== null) {
                foreach ($this->collCustomFormExtraLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->customFormLogsScheduledForDeletion !== null) {
                if (!$this->customFormLogsScheduledForDeletion->isEmpty()) {
                    foreach ($this->customFormLogsScheduledForDeletion as $customFormLog) {
                        // need to save related object because we set the relation to null
                        $customFormLog->save($con);
                    }
                    $this->customFormLogsScheduledForDeletion = null;
                }
            }

            if ($this->collCustomFormLogs !== null) {
                foreach ($this->collCustomFormLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postActionsScheduledForDeletion !== null) {
                if (!$this->postActionsScheduledForDeletion->isEmpty()) {
                    foreach ($this->postActionsScheduledForDeletion as $postAction) {
                        // need to save related object because we set the relation to null
                        $postAction->save($con);
                    }
                    $this->postActionsScheduledForDeletion = null;
                }
            }

            if ($this->collPostActions !== null) {
                foreach ($this->collPostActions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pushNotificationsScheduledForDeletion !== null) {
                if (!$this->pushNotificationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->pushNotificationsScheduledForDeletion as $pushNotification) {
                        // need to save related object because we set the relation to null
                        $pushNotification->save($con);
                    }
                    $this->pushNotificationsScheduledForDeletion = null;
                }
            }

            if ($this->collPushNotifications !== null) {
                foreach ($this->collPushNotifications as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sectionConnectorsRelatedBySectionIdScheduledForDeletion !== null) {
                if (!$this->sectionConnectorsRelatedBySectionIdScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\SectionConnectorQuery::create()
                        ->filterByPrimaryKeys($this->sectionConnectorsRelatedBySectionIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sectionConnectorsRelatedBySectionIdScheduledForDeletion = null;
                }
            }

            if ($this->collSectionConnectorsRelatedBySectionId !== null) {
                foreach ($this->collSectionConnectorsRelatedBySectionId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion !== null) {
                if (!$this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\SectionConnectorQuery::create()
                        ->filterByPrimaryKeys($this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion = null;
                }
            }

            if ($this->collSectionConnectorsRelatedByRemoteSectionId !== null) {
                foreach ($this->collSectionConnectorsRelatedByRemoteSectionId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion !== null) {
                if (!$this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\SectionRelatedToQuery::create()
                        ->filterByPrimaryKeys($this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion = null;
                }
            }

            if ($this->collSectionRelatedTosRelatedByFromSectionId !== null) {
                foreach ($this->collSectionRelatedTosRelatedByFromSectionId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion !== null) {
                if (!$this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\SectionRelatedToQuery::create()
                        ->filterByPrimaryKeys($this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion = null;
                }
            }

            if ($this->collSectionRelatedTosRelatedByToSectionId !== null) {
                foreach ($this->collSectionRelatedTosRelatedByToSectionId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->userAppsScheduledForDeletion !== null) {
                if (!$this->userAppsScheduledForDeletion->isEmpty()) {
                    foreach ($this->userAppsScheduledForDeletion as $userApp) {
                        // need to save related object because we set the relation to null
                        $userApp->save($con);
                    }
                    $this->userAppsScheduledForDeletion = null;
                }
            }

            if ($this->collUserApps !== null) {
                foreach ($this->collUserApps as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->webhooksScheduledForDeletion !== null) {
                if (!$this->webhooksScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\WebhookQuery::create()
                        ->filterByPrimaryKeys($this->webhooksScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->webhooksScheduledForDeletion = null;
                }
            }

            if ($this->collWebhooks !== null) {
                foreach ($this->collWebhooks as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SectionTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SectionTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SectionTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(SectionTableMap::COL_AREA)) {
            $modifiedColumns[':p' . $index++]  = 'area';
        }
        if ($this->isColumnModified(SectionTableMap::COL_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'type';
        }
        if ($this->isColumnModified(SectionTableMap::COL_AUTHOR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'author_id';
        }
        if ($this->isColumnModified(SectionTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(SectionTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(SectionTableMap::COL_SLUG)) {
            $modifiedColumns[':p' . $index++]  = 'slug';
        }
        if ($this->isColumnModified(SectionTableMap::COL_ICON_ID)) {
            $modifiedColumns[':p' . $index++]  = 'icon_id';
        }
        if ($this->isColumnModified(SectionTableMap::COL_ICON_SELECTED_ID)) {
            $modifiedColumns[':p' . $index++]  = 'icon_selected_id';
        }
        if ($this->isColumnModified(SectionTableMap::COL_RESOURCE)) {
            $modifiedColumns[':p' . $index++]  = 'resource';
        }
        if ($this->isColumnModified(SectionTableMap::COL_PARAMS)) {
            $modifiedColumns[':p' . $index++]  = 'params';
        }
        if ($this->isColumnModified(SectionTableMap::COL_TEMPLATE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'template_id';
        }
        if ($this->isColumnModified(SectionTableMap::COL_LAYOUT)) {
            $modifiedColumns[':p' . $index++]  = 'layout';
        }
        if ($this->isColumnModified(SectionTableMap::COL_TAGS)) {
            $modifiedColumns[':p' . $index++]  = 'tags';
        }
        if ($this->isColumnModified(SectionTableMap::COL_TAGS_USER)) {
            $modifiedColumns[':p' . $index++]  = 'tags_user';
        }
        if ($this->isColumnModified(SectionTableMap::COL_PARSED_TAGS)) {
            $modifiedColumns[':p' . $index++]  = 'parsed_tags';
        }
        if ($this->isColumnModified(SectionTableMap::COL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'status';
        }
        if ($this->isColumnModified(SectionTableMap::COL_WEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'weight';
        }
        if ($this->isColumnModified(SectionTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }
        if ($this->isColumnModified(SectionTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(SectionTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO section (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'area':
                        $stmt->bindValue($identifier, $this->area, PDO::PARAM_STR);
                        break;
                    case 'type':
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_STR);
                        break;
                    case 'author_id':
                        $stmt->bindValue($identifier, $this->author_id, PDO::PARAM_INT);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'slug':
                        $stmt->bindValue($identifier, $this->slug, PDO::PARAM_STR);
                        break;
                    case 'icon_id':
                        $stmt->bindValue($identifier, $this->icon_id, PDO::PARAM_INT);
                        break;
                    case 'icon_selected_id':
                        $stmt->bindValue($identifier, $this->icon_selected_id, PDO::PARAM_INT);
                        break;
                    case 'resource':
                        $stmt->bindValue($identifier, $this->resource, PDO::PARAM_STR);
                        break;
                    case 'params':
                        $stmt->bindValue($identifier, $this->params, PDO::PARAM_STR);
                        break;
                    case 'template_id':
                        $stmt->bindValue($identifier, $this->template_id, PDO::PARAM_INT);
                        break;
                    case 'layout':
                        $stmt->bindValue($identifier, $this->layout, PDO::PARAM_STR);
                        break;
                    case 'tags':
                        $stmt->bindValue($identifier, $this->tags, PDO::PARAM_STR);
                        break;
                    case 'tags_user':
                        $stmt->bindValue($identifier, $this->tags_user, PDO::PARAM_STR);
                        break;
                    case 'parsed_tags':
                        $stmt->bindValue($identifier, $this->parsed_tags, PDO::PARAM_STR);
                        break;
                    case 'status':
                        $stmt->bindValue($identifier, (int) $this->status, PDO::PARAM_INT);
                        break;
                    case 'weight':
                        $stmt->bindValue($identifier, $this->weight, PDO::PARAM_INT);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SectionTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getArea();
                break;
            case 2:
                return $this->getType();
                break;
            case 3:
                return $this->getAuthorId();
                break;
            case 4:
                return $this->getTitle();
                break;
            case 5:
                return $this->getDescription();
                break;
            case 6:
                return $this->getSlug();
                break;
            case 7:
                return $this->getIconId();
                break;
            case 8:
                return $this->getIconSelectedId();
                break;
            case 9:
                return $this->getResource();
                break;
            case 10:
                return $this->getParams();
                break;
            case 11:
                return $this->getTemplateId();
                break;
            case 12:
                return $this->getLayout();
                break;
            case 13:
                return $this->getTags();
                break;
            case 14:
                return $this->getTagsUser();
                break;
            case 15:
                return $this->getParsedTags();
                break;
            case 16:
                return $this->getStatus();
                break;
            case 17:
                return $this->getWeight();
                break;
            case 18:
                return $this->getDeletedAt();
                break;
            case 19:
                return $this->getCreatedAt();
                break;
            case 20:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Section'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Section'][$this->hashCode()] = true;
        $keys = SectionTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getArea(),
            $keys[2] => $this->getType(),
            $keys[3] => $this->getAuthorId(),
            $keys[4] => $this->getTitle(),
            $keys[5] => $this->getDescription(),
            $keys[6] => $this->getSlug(),
            $keys[7] => $this->getIconId(),
            $keys[8] => $this->getIconSelectedId(),
            $keys[9] => $this->getResource(),
            $keys[10] => $this->getParams(),
            $keys[11] => $this->getTemplateId(),
            $keys[12] => $this->getLayout(),
            $keys[13] => $this->getTags(),
            $keys[14] => $this->getTagsUser(),
            $keys[15] => $this->getParsedTags(),
            $keys[16] => $this->getStatus(),
            $keys[17] => $this->getWeight(),
            $keys[18] => $this->getDeletedAt(),
            $keys[19] => $this->getCreatedAt(),
            $keys[20] => $this->getUpdatedAt(),
        );
        if ($result[$keys[18]] instanceof \DateTimeInterface) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        if ($result[$keys[19]] instanceof \DateTimeInterface) {
            $result[$keys[19]] = $result[$keys[19]]->format('c');
        }

        if ($result[$keys[20]] instanceof \DateTimeInterface) {
            $result[$keys[20]] = $result[$keys[20]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aTemplate) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'template';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'template';
                        break;
                    default:
                        $key = 'Template';
                }

                $result[$key] = $this->aTemplate->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMediaRelatedByIconId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'media';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'media';
                        break;
                    default:
                        $key = 'Media';
                }

                $result[$key] = $this->aMediaRelatedByIconId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMediaRelatedByIconSelectedId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'media';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'media';
                        break;
                    default:
                        $key = 'Media';
                }

                $result[$key] = $this->aMediaRelatedByIconSelectedId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUserBackend) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userBackend';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_backend';
                        break;
                    default:
                        $key = 'UserBackend';
                }

                $result[$key] = $this->aUserBackend->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCustomFormExtraLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customFormExtraLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cf_extra_logs';
                        break;
                    default:
                        $key = 'CustomFormExtraLogs';
                }

                $result[$key] = $this->collCustomFormExtraLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCustomFormLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customFormLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cf_logs';
                        break;
                    default:
                        $key = 'CustomFormLogs';
                }

                $result[$key] = $this->collCustomFormLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPostActions) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postActions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_actions';
                        break;
                    default:
                        $key = 'PostActions';
                }

                $result[$key] = $this->collPostActions->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPushNotifications) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pushNotifications';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'push_notifications';
                        break;
                    default:
                        $key = 'PushNotifications';
                }

                $result[$key] = $this->collPushNotifications->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSectionConnectorsRelatedBySectionId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sectionConnectors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'section_connectors';
                        break;
                    default:
                        $key = 'SectionConnectors';
                }

                $result[$key] = $this->collSectionConnectorsRelatedBySectionId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSectionConnectorsRelatedByRemoteSectionId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sectionConnectors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'section_connectors';
                        break;
                    default:
                        $key = 'SectionConnectors';
                }

                $result[$key] = $this->collSectionConnectorsRelatedByRemoteSectionId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSectionRelatedTosRelatedByFromSectionId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sectionRelatedTos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'section_related_tos';
                        break;
                    default:
                        $key = 'SectionRelatedTos';
                }

                $result[$key] = $this->collSectionRelatedTosRelatedByFromSectionId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSectionRelatedTosRelatedByToSectionId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sectionRelatedTos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'section_related_tos';
                        break;
                    default:
                        $key = 'SectionRelatedTos';
                }

                $result[$key] = $this->collSectionRelatedTosRelatedByToSectionId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUserApps) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userApps';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_apps';
                        break;
                    default:
                        $key = 'UserApps';
                }

                $result[$key] = $this->collUserApps->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWebhooks) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'webhooks';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'webhooks';
                        break;
                    default:
                        $key = 'Webhooks';
                }

                $result[$key] = $this->collWebhooks->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Database\HubPlus\Section
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SectionTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Database\HubPlus\Section
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setArea($value);
                break;
            case 2:
                $this->setType($value);
                break;
            case 3:
                $this->setAuthorId($value);
                break;
            case 4:
                $this->setTitle($value);
                break;
            case 5:
                $this->setDescription($value);
                break;
            case 6:
                $this->setSlug($value);
                break;
            case 7:
                $this->setIconId($value);
                break;
            case 8:
                $this->setIconSelectedId($value);
                break;
            case 9:
                $this->setResource($value);
                break;
            case 10:
                $this->setParams($value);
                break;
            case 11:
                $this->setTemplateId($value);
                break;
            case 12:
                $this->setLayout($value);
                break;
            case 13:
                $this->setTags($value);
                break;
            case 14:
                $this->setTagsUser($value);
                break;
            case 15:
                $this->setParsedTags($value);
                break;
            case 16:
                $this->setStatus($value);
                break;
            case 17:
                $this->setWeight($value);
                break;
            case 18:
                $this->setDeletedAt($value);
                break;
            case 19:
                $this->setCreatedAt($value);
                break;
            case 20:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SectionTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setArea($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setType($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setAuthorId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setTitle($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDescription($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setSlug($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIconId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setIconSelectedId($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setResource($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setParams($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setTemplateId($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setLayout($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setTags($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setTagsUser($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setParsedTags($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setStatus($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setWeight($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setDeletedAt($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setCreatedAt($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setUpdatedAt($arr[$keys[20]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Database\HubPlus\Section The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SectionTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SectionTableMap::COL_ID)) {
            $criteria->add(SectionTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(SectionTableMap::COL_AREA)) {
            $criteria->add(SectionTableMap::COL_AREA, $this->area);
        }
        if ($this->isColumnModified(SectionTableMap::COL_TYPE)) {
            $criteria->add(SectionTableMap::COL_TYPE, $this->type);
        }
        if ($this->isColumnModified(SectionTableMap::COL_AUTHOR_ID)) {
            $criteria->add(SectionTableMap::COL_AUTHOR_ID, $this->author_id);
        }
        if ($this->isColumnModified(SectionTableMap::COL_TITLE)) {
            $criteria->add(SectionTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(SectionTableMap::COL_DESCRIPTION)) {
            $criteria->add(SectionTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(SectionTableMap::COL_SLUG)) {
            $criteria->add(SectionTableMap::COL_SLUG, $this->slug);
        }
        if ($this->isColumnModified(SectionTableMap::COL_ICON_ID)) {
            $criteria->add(SectionTableMap::COL_ICON_ID, $this->icon_id);
        }
        if ($this->isColumnModified(SectionTableMap::COL_ICON_SELECTED_ID)) {
            $criteria->add(SectionTableMap::COL_ICON_SELECTED_ID, $this->icon_selected_id);
        }
        if ($this->isColumnModified(SectionTableMap::COL_RESOURCE)) {
            $criteria->add(SectionTableMap::COL_RESOURCE, $this->resource);
        }
        if ($this->isColumnModified(SectionTableMap::COL_PARAMS)) {
            $criteria->add(SectionTableMap::COL_PARAMS, $this->params);
        }
        if ($this->isColumnModified(SectionTableMap::COL_TEMPLATE_ID)) {
            $criteria->add(SectionTableMap::COL_TEMPLATE_ID, $this->template_id);
        }
        if ($this->isColumnModified(SectionTableMap::COL_LAYOUT)) {
            $criteria->add(SectionTableMap::COL_LAYOUT, $this->layout);
        }
        if ($this->isColumnModified(SectionTableMap::COL_TAGS)) {
            $criteria->add(SectionTableMap::COL_TAGS, $this->tags);
        }
        if ($this->isColumnModified(SectionTableMap::COL_TAGS_USER)) {
            $criteria->add(SectionTableMap::COL_TAGS_USER, $this->tags_user);
        }
        if ($this->isColumnModified(SectionTableMap::COL_PARSED_TAGS)) {
            $criteria->add(SectionTableMap::COL_PARSED_TAGS, $this->parsed_tags);
        }
        if ($this->isColumnModified(SectionTableMap::COL_STATUS)) {
            $criteria->add(SectionTableMap::COL_STATUS, $this->status);
        }
        if ($this->isColumnModified(SectionTableMap::COL_WEIGHT)) {
            $criteria->add(SectionTableMap::COL_WEIGHT, $this->weight);
        }
        if ($this->isColumnModified(SectionTableMap::COL_DELETED_AT)) {
            $criteria->add(SectionTableMap::COL_DELETED_AT, $this->deleted_at);
        }
        if ($this->isColumnModified(SectionTableMap::COL_CREATED_AT)) {
            $criteria->add(SectionTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(SectionTableMap::COL_UPDATED_AT)) {
            $criteria->add(SectionTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSectionQuery::create();
        $criteria->add(SectionTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Database\HubPlus\Section (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setArea($this->getArea());
        $copyObj->setType($this->getType());
        $copyObj->setAuthorId($this->getAuthorId());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setSlug($this->getSlug());
        $copyObj->setIconId($this->getIconId());
        $copyObj->setIconSelectedId($this->getIconSelectedId());
        $copyObj->setResource($this->getResource());
        $copyObj->setParams($this->getParams());
        $copyObj->setTemplateId($this->getTemplateId());
        $copyObj->setLayout($this->getLayout());
        $copyObj->setTags($this->getTags());
        $copyObj->setTagsUser($this->getTagsUser());
        $copyObj->setParsedTags($this->getParsedTags());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setWeight($this->getWeight());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCustomFormExtraLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomFormExtraLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCustomFormLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomFormLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPostActions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostAction($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPushNotifications() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPushNotification($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSectionConnectorsRelatedBySectionId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSectionConnectorRelatedBySectionId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSectionConnectorsRelatedByRemoteSectionId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSectionConnectorRelatedByRemoteSectionId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSectionRelatedTosRelatedByFromSectionId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSectionRelatedToRelatedByFromSectionId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSectionRelatedTosRelatedByToSectionId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSectionRelatedToRelatedByToSectionId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUserApps() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserApp($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWebhooks() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWebhook($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Database\HubPlus\Section Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildTemplate object.
     *
     * @param  ChildTemplate $v
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTemplate(ChildTemplate $v = null)
    {
        if ($v === null) {
            $this->setTemplateId(NULL);
        } else {
            $this->setTemplateId($v->getId());
        }

        $this->aTemplate = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildTemplate object, it will not be re-added.
        if ($v !== null) {
            $v->addSection($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildTemplate object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildTemplate The associated ChildTemplate object.
     * @throws PropelException
     */
    public function getTemplate(ConnectionInterface $con = null)
    {
        if ($this->aTemplate === null && ($this->template_id != 0)) {
            $this->aTemplate = ChildTemplateQuery::create()->findPk($this->template_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTemplate->addSections($this);
             */
        }

        return $this->aTemplate;
    }

    /**
     * Declares an association between this object and a ChildMedia object.
     *
     * @param  ChildMedia $v
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMediaRelatedByIconId(ChildMedia $v = null)
    {
        if ($v === null) {
            $this->setIconId(NULL);
        } else {
            $this->setIconId($v->getId());
        }

        $this->aMediaRelatedByIconId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMedia object, it will not be re-added.
        if ($v !== null) {
            $v->addSectionRelatedByIconId($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMedia object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildMedia The associated ChildMedia object.
     * @throws PropelException
     */
    public function getMediaRelatedByIconId(ConnectionInterface $con = null)
    {
        if ($this->aMediaRelatedByIconId === null && ($this->icon_id != 0)) {
            $this->aMediaRelatedByIconId = ChildMediaQuery::create()->findPk($this->icon_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMediaRelatedByIconId->addSectionsRelatedByIconId($this);
             */
        }

        return $this->aMediaRelatedByIconId;
    }

    /**
     * Declares an association between this object and a ChildMedia object.
     *
     * @param  ChildMedia $v
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMediaRelatedByIconSelectedId(ChildMedia $v = null)
    {
        if ($v === null) {
            $this->setIconSelectedId(NULL);
        } else {
            $this->setIconSelectedId($v->getId());
        }

        $this->aMediaRelatedByIconSelectedId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMedia object, it will not be re-added.
        if ($v !== null) {
            $v->addSectionRelatedByIconSelectedId($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMedia object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildMedia The associated ChildMedia object.
     * @throws PropelException
     */
    public function getMediaRelatedByIconSelectedId(ConnectionInterface $con = null)
    {
        if ($this->aMediaRelatedByIconSelectedId === null && ($this->icon_selected_id != 0)) {
            $this->aMediaRelatedByIconSelectedId = ChildMediaQuery::create()->findPk($this->icon_selected_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMediaRelatedByIconSelectedId->addSectionsRelatedByIconSelectedId($this);
             */
        }

        return $this->aMediaRelatedByIconSelectedId;
    }

    /**
     * Declares an association between this object and a ChildUserBackend object.
     *
     * @param  ChildUserBackend $v
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUserBackend(ChildUserBackend $v = null)
    {
        if ($v === null) {
            $this->setAuthorId(NULL);
        } else {
            $this->setAuthorId($v->getId());
        }

        $this->aUserBackend = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUserBackend object, it will not be re-added.
        if ($v !== null) {
            $v->addSection($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUserBackend object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUserBackend The associated ChildUserBackend object.
     * @throws PropelException
     */
    public function getUserBackend(ConnectionInterface $con = null)
    {
        if ($this->aUserBackend === null && ($this->author_id != 0)) {
            $this->aUserBackend = ChildUserBackendQuery::create()->findPk($this->author_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUserBackend->addSections($this);
             */
        }

        return $this->aUserBackend;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CustomFormExtraLog' == $relationName) {
            $this->initCustomFormExtraLogs();
            return;
        }
        if ('CustomFormLog' == $relationName) {
            $this->initCustomFormLogs();
            return;
        }
        if ('PostAction' == $relationName) {
            $this->initPostActions();
            return;
        }
        if ('PushNotification' == $relationName) {
            $this->initPushNotifications();
            return;
        }
        if ('SectionConnectorRelatedBySectionId' == $relationName) {
            $this->initSectionConnectorsRelatedBySectionId();
            return;
        }
        if ('SectionConnectorRelatedByRemoteSectionId' == $relationName) {
            $this->initSectionConnectorsRelatedByRemoteSectionId();
            return;
        }
        if ('SectionRelatedToRelatedByFromSectionId' == $relationName) {
            $this->initSectionRelatedTosRelatedByFromSectionId();
            return;
        }
        if ('SectionRelatedToRelatedByToSectionId' == $relationName) {
            $this->initSectionRelatedTosRelatedByToSectionId();
            return;
        }
        if ('UserApp' == $relationName) {
            $this->initUserApps();
            return;
        }
        if ('Webhook' == $relationName) {
            $this->initWebhooks();
            return;
        }
    }

    /**
     * Clears out the collCustomFormExtraLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomFormExtraLogs()
     */
    public function clearCustomFormExtraLogs()
    {
        $this->collCustomFormExtraLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomFormExtraLogs collection loaded partially.
     */
    public function resetPartialCustomFormExtraLogs($v = true)
    {
        $this->collCustomFormExtraLogsPartial = $v;
    }

    /**
     * Initializes the collCustomFormExtraLogs collection.
     *
     * By default this just sets the collCustomFormExtraLogs collection to an empty array (like clearcollCustomFormExtraLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomFormExtraLogs($overrideExisting = true)
    {
        if (null !== $this->collCustomFormExtraLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomFormExtraLogTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomFormExtraLogs = new $collectionClassName;
        $this->collCustomFormExtraLogs->setModel('\Database\HubPlus\CustomFormExtraLog');
    }

    /**
     * Gets an array of ChildCustomFormExtraLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSection is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCustomFormExtraLog[] List of ChildCustomFormExtraLog objects
     * @throws PropelException
     */
    public function getCustomFormExtraLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomFormExtraLogsPartial && !$this->isNew();
        if (null === $this->collCustomFormExtraLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCustomFormExtraLogs) {
                // return empty collection
                $this->initCustomFormExtraLogs();
            } else {
                $collCustomFormExtraLogs = ChildCustomFormExtraLogQuery::create(null, $criteria)
                    ->filterBySection($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomFormExtraLogsPartial && count($collCustomFormExtraLogs)) {
                        $this->initCustomFormExtraLogs(false);

                        foreach ($collCustomFormExtraLogs as $obj) {
                            if (false == $this->collCustomFormExtraLogs->contains($obj)) {
                                $this->collCustomFormExtraLogs->append($obj);
                            }
                        }

                        $this->collCustomFormExtraLogsPartial = true;
                    }

                    return $collCustomFormExtraLogs;
                }

                if ($partial && $this->collCustomFormExtraLogs) {
                    foreach ($this->collCustomFormExtraLogs as $obj) {
                        if ($obj->isNew()) {
                            $collCustomFormExtraLogs[] = $obj;
                        }
                    }
                }

                $this->collCustomFormExtraLogs = $collCustomFormExtraLogs;
                $this->collCustomFormExtraLogsPartial = false;
            }
        }

        return $this->collCustomFormExtraLogs;
    }

    /**
     * Sets a collection of ChildCustomFormExtraLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customFormExtraLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function setCustomFormExtraLogs(Collection $customFormExtraLogs, ConnectionInterface $con = null)
    {
        /** @var ChildCustomFormExtraLog[] $customFormExtraLogsToDelete */
        $customFormExtraLogsToDelete = $this->getCustomFormExtraLogs(new Criteria(), $con)->diff($customFormExtraLogs);


        $this->customFormExtraLogsScheduledForDeletion = $customFormExtraLogsToDelete;

        foreach ($customFormExtraLogsToDelete as $customFormExtraLogRemoved) {
            $customFormExtraLogRemoved->setSection(null);
        }

        $this->collCustomFormExtraLogs = null;
        foreach ($customFormExtraLogs as $customFormExtraLog) {
            $this->addCustomFormExtraLog($customFormExtraLog);
        }

        $this->collCustomFormExtraLogs = $customFormExtraLogs;
        $this->collCustomFormExtraLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CustomFormExtraLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CustomFormExtraLog objects.
     * @throws PropelException
     */
    public function countCustomFormExtraLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomFormExtraLogsPartial && !$this->isNew();
        if (null === $this->collCustomFormExtraLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomFormExtraLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomFormExtraLogs());
            }

            $query = ChildCustomFormExtraLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySection($this)
                ->count($con);
        }

        return count($this->collCustomFormExtraLogs);
    }

    /**
     * Method called to associate a ChildCustomFormExtraLog object to this object
     * through the ChildCustomFormExtraLog foreign key attribute.
     *
     * @param  ChildCustomFormExtraLog $l ChildCustomFormExtraLog
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function addCustomFormExtraLog(ChildCustomFormExtraLog $l)
    {
        if ($this->collCustomFormExtraLogs === null) {
            $this->initCustomFormExtraLogs();
            $this->collCustomFormExtraLogsPartial = true;
        }

        if (!$this->collCustomFormExtraLogs->contains($l)) {
            $this->doAddCustomFormExtraLog($l);

            if ($this->customFormExtraLogsScheduledForDeletion and $this->customFormExtraLogsScheduledForDeletion->contains($l)) {
                $this->customFormExtraLogsScheduledForDeletion->remove($this->customFormExtraLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCustomFormExtraLog $customFormExtraLog The ChildCustomFormExtraLog object to add.
     */
    protected function doAddCustomFormExtraLog(ChildCustomFormExtraLog $customFormExtraLog)
    {
        $this->collCustomFormExtraLogs[]= $customFormExtraLog;
        $customFormExtraLog->setSection($this);
    }

    /**
     * @param  ChildCustomFormExtraLog $customFormExtraLog The ChildCustomFormExtraLog object to remove.
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function removeCustomFormExtraLog(ChildCustomFormExtraLog $customFormExtraLog)
    {
        if ($this->getCustomFormExtraLogs()->contains($customFormExtraLog)) {
            $pos = $this->collCustomFormExtraLogs->search($customFormExtraLog);
            $this->collCustomFormExtraLogs->remove($pos);
            if (null === $this->customFormExtraLogsScheduledForDeletion) {
                $this->customFormExtraLogsScheduledForDeletion = clone $this->collCustomFormExtraLogs;
                $this->customFormExtraLogsScheduledForDeletion->clear();
            }
            $this->customFormExtraLogsScheduledForDeletion[]= $customFormExtraLog;
            $customFormExtraLog->setSection(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Section is new, it will return
     * an empty collection; or if this Section has previously
     * been saved, it will retrieve related CustomFormExtraLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Section.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCustomFormExtraLog[] List of ChildCustomFormExtraLog objects
     */
    public function getCustomFormExtraLogsJoinDevice(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCustomFormExtraLogQuery::create(null, $criteria);
        $query->joinWith('Device', $joinBehavior);

        return $this->getCustomFormExtraLogs($query, $con);
    }

    /**
     * Clears out the collCustomFormLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomFormLogs()
     */
    public function clearCustomFormLogs()
    {
        $this->collCustomFormLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomFormLogs collection loaded partially.
     */
    public function resetPartialCustomFormLogs($v = true)
    {
        $this->collCustomFormLogsPartial = $v;
    }

    /**
     * Initializes the collCustomFormLogs collection.
     *
     * By default this just sets the collCustomFormLogs collection to an empty array (like clearcollCustomFormLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomFormLogs($overrideExisting = true)
    {
        if (null !== $this->collCustomFormLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomFormLogTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomFormLogs = new $collectionClassName;
        $this->collCustomFormLogs->setModel('\Database\HubPlus\CustomFormLog');
    }

    /**
     * Gets an array of ChildCustomFormLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSection is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCustomFormLog[] List of ChildCustomFormLog objects
     * @throws PropelException
     */
    public function getCustomFormLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomFormLogsPartial && !$this->isNew();
        if (null === $this->collCustomFormLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCustomFormLogs) {
                // return empty collection
                $this->initCustomFormLogs();
            } else {
                $collCustomFormLogs = ChildCustomFormLogQuery::create(null, $criteria)
                    ->filterBySection($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomFormLogsPartial && count($collCustomFormLogs)) {
                        $this->initCustomFormLogs(false);

                        foreach ($collCustomFormLogs as $obj) {
                            if (false == $this->collCustomFormLogs->contains($obj)) {
                                $this->collCustomFormLogs->append($obj);
                            }
                        }

                        $this->collCustomFormLogsPartial = true;
                    }

                    return $collCustomFormLogs;
                }

                if ($partial && $this->collCustomFormLogs) {
                    foreach ($this->collCustomFormLogs as $obj) {
                        if ($obj->isNew()) {
                            $collCustomFormLogs[] = $obj;
                        }
                    }
                }

                $this->collCustomFormLogs = $collCustomFormLogs;
                $this->collCustomFormLogsPartial = false;
            }
        }

        return $this->collCustomFormLogs;
    }

    /**
     * Sets a collection of ChildCustomFormLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customFormLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function setCustomFormLogs(Collection $customFormLogs, ConnectionInterface $con = null)
    {
        /** @var ChildCustomFormLog[] $customFormLogsToDelete */
        $customFormLogsToDelete = $this->getCustomFormLogs(new Criteria(), $con)->diff($customFormLogs);


        $this->customFormLogsScheduledForDeletion = $customFormLogsToDelete;

        foreach ($customFormLogsToDelete as $customFormLogRemoved) {
            $customFormLogRemoved->setSection(null);
        }

        $this->collCustomFormLogs = null;
        foreach ($customFormLogs as $customFormLog) {
            $this->addCustomFormLog($customFormLog);
        }

        $this->collCustomFormLogs = $customFormLogs;
        $this->collCustomFormLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CustomFormLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CustomFormLog objects.
     * @throws PropelException
     */
    public function countCustomFormLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomFormLogsPartial && !$this->isNew();
        if (null === $this->collCustomFormLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomFormLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomFormLogs());
            }

            $query = ChildCustomFormLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySection($this)
                ->count($con);
        }

        return count($this->collCustomFormLogs);
    }

    /**
     * Method called to associate a ChildCustomFormLog object to this object
     * through the ChildCustomFormLog foreign key attribute.
     *
     * @param  ChildCustomFormLog $l ChildCustomFormLog
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function addCustomFormLog(ChildCustomFormLog $l)
    {
        if ($this->collCustomFormLogs === null) {
            $this->initCustomFormLogs();
            $this->collCustomFormLogsPartial = true;
        }

        if (!$this->collCustomFormLogs->contains($l)) {
            $this->doAddCustomFormLog($l);

            if ($this->customFormLogsScheduledForDeletion and $this->customFormLogsScheduledForDeletion->contains($l)) {
                $this->customFormLogsScheduledForDeletion->remove($this->customFormLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCustomFormLog $customFormLog The ChildCustomFormLog object to add.
     */
    protected function doAddCustomFormLog(ChildCustomFormLog $customFormLog)
    {
        $this->collCustomFormLogs[]= $customFormLog;
        $customFormLog->setSection($this);
    }

    /**
     * @param  ChildCustomFormLog $customFormLog The ChildCustomFormLog object to remove.
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function removeCustomFormLog(ChildCustomFormLog $customFormLog)
    {
        if ($this->getCustomFormLogs()->contains($customFormLog)) {
            $pos = $this->collCustomFormLogs->search($customFormLog);
            $this->collCustomFormLogs->remove($pos);
            if (null === $this->customFormLogsScheduledForDeletion) {
                $this->customFormLogsScheduledForDeletion = clone $this->collCustomFormLogs;
                $this->customFormLogsScheduledForDeletion->clear();
            }
            $this->customFormLogsScheduledForDeletion[]= $customFormLog;
            $customFormLog->setSection(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Section is new, it will return
     * an empty collection; or if this Section has previously
     * been saved, it will retrieve related CustomFormLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Section.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCustomFormLog[] List of ChildCustomFormLog objects
     */
    public function getCustomFormLogsJoinUserApp(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCustomFormLogQuery::create(null, $criteria);
        $query->joinWith('UserApp', $joinBehavior);

        return $this->getCustomFormLogs($query, $con);
    }

    /**
     * Clears out the collPostActions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostActions()
     */
    public function clearPostActions()
    {
        $this->collPostActions = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostActions collection loaded partially.
     */
    public function resetPartialPostActions($v = true)
    {
        $this->collPostActionsPartial = $v;
    }

    /**
     * Initializes the collPostActions collection.
     *
     * By default this just sets the collPostActions collection to an empty array (like clearcollPostActions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostActions($overrideExisting = true)
    {
        if (null !== $this->collPostActions && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostActionTableMap::getTableMap()->getCollectionClassName();

        $this->collPostActions = new $collectionClassName;
        $this->collPostActions->setModel('\Database\HubPlus\PostAction');
    }

    /**
     * Gets an array of ChildPostAction objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSection is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     * @throws PropelException
     */
    public function getPostActions(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostActionsPartial && !$this->isNew();
        if (null === $this->collPostActions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostActions) {
                // return empty collection
                $this->initPostActions();
            } else {
                $collPostActions = ChildPostActionQuery::create(null, $criteria)
                    ->filterBySection($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostActionsPartial && count($collPostActions)) {
                        $this->initPostActions(false);

                        foreach ($collPostActions as $obj) {
                            if (false == $this->collPostActions->contains($obj)) {
                                $this->collPostActions->append($obj);
                            }
                        }

                        $this->collPostActionsPartial = true;
                    }

                    return $collPostActions;
                }

                if ($partial && $this->collPostActions) {
                    foreach ($this->collPostActions as $obj) {
                        if ($obj->isNew()) {
                            $collPostActions[] = $obj;
                        }
                    }
                }

                $this->collPostActions = $collPostActions;
                $this->collPostActionsPartial = false;
            }
        }

        return $this->collPostActions;
    }

    /**
     * Sets a collection of ChildPostAction objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postActions A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function setPostActions(Collection $postActions, ConnectionInterface $con = null)
    {
        /** @var ChildPostAction[] $postActionsToDelete */
        $postActionsToDelete = $this->getPostActions(new Criteria(), $con)->diff($postActions);


        $this->postActionsScheduledForDeletion = $postActionsToDelete;

        foreach ($postActionsToDelete as $postActionRemoved) {
            $postActionRemoved->setSection(null);
        }

        $this->collPostActions = null;
        foreach ($postActions as $postAction) {
            $this->addPostAction($postAction);
        }

        $this->collPostActions = $postActions;
        $this->collPostActionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostAction objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostAction objects.
     * @throws PropelException
     */
    public function countPostActions(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostActionsPartial && !$this->isNew();
        if (null === $this->collPostActions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostActions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostActions());
            }

            $query = ChildPostActionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySection($this)
                ->count($con);
        }

        return count($this->collPostActions);
    }

    /**
     * Method called to associate a ChildPostAction object to this object
     * through the ChildPostAction foreign key attribute.
     *
     * @param  ChildPostAction $l ChildPostAction
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function addPostAction(ChildPostAction $l)
    {
        if ($this->collPostActions === null) {
            $this->initPostActions();
            $this->collPostActionsPartial = true;
        }

        if (!$this->collPostActions->contains($l)) {
            $this->doAddPostAction($l);

            if ($this->postActionsScheduledForDeletion and $this->postActionsScheduledForDeletion->contains($l)) {
                $this->postActionsScheduledForDeletion->remove($this->postActionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostAction $postAction The ChildPostAction object to add.
     */
    protected function doAddPostAction(ChildPostAction $postAction)
    {
        $this->collPostActions[]= $postAction;
        $postAction->setSection($this);
    }

    /**
     * @param  ChildPostAction $postAction The ChildPostAction object to remove.
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function removePostAction(ChildPostAction $postAction)
    {
        if ($this->getPostActions()->contains($postAction)) {
            $pos = $this->collPostActions->search($postAction);
            $this->collPostActions->remove($pos);
            if (null === $this->postActionsScheduledForDeletion) {
                $this->postActionsScheduledForDeletion = clone $this->collPostActions;
                $this->postActionsScheduledForDeletion->clear();
            }
            $this->postActionsScheduledForDeletion[]= $postAction;
            $postAction->setSection(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Section is new, it will return
     * an empty collection; or if this Section has previously
     * been saved, it will retrieve related PostActions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Section.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     */
    public function getPostActionsJoinPostRelatedByPostId(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostActionQuery::create(null, $criteria);
        $query->joinWith('PostRelatedByPostId', $joinBehavior);

        return $this->getPostActions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Section is new, it will return
     * an empty collection; or if this Section has previously
     * been saved, it will retrieve related PostActions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Section.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     */
    public function getPostActionsJoinPostRelatedByContentId(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostActionQuery::create(null, $criteria);
        $query->joinWith('PostRelatedByContentId', $joinBehavior);

        return $this->getPostActions($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Section is new, it will return
     * an empty collection; or if this Section has previously
     * been saved, it will retrieve related PostActions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Section.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     */
    public function getPostActionsJoinMedia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostActionQuery::create(null, $criteria);
        $query->joinWith('Media', $joinBehavior);

        return $this->getPostActions($query, $con);
    }

    /**
     * Clears out the collPushNotifications collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPushNotifications()
     */
    public function clearPushNotifications()
    {
        $this->collPushNotifications = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPushNotifications collection loaded partially.
     */
    public function resetPartialPushNotifications($v = true)
    {
        $this->collPushNotificationsPartial = $v;
    }

    /**
     * Initializes the collPushNotifications collection.
     *
     * By default this just sets the collPushNotifications collection to an empty array (like clearcollPushNotifications());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPushNotifications($overrideExisting = true)
    {
        if (null !== $this->collPushNotifications && !$overrideExisting) {
            return;
        }

        $collectionClassName = PushNotificationTableMap::getTableMap()->getCollectionClassName();

        $this->collPushNotifications = new $collectionClassName;
        $this->collPushNotifications->setModel('\Database\HubPlus\PushNotification');
    }

    /**
     * Gets an array of ChildPushNotification objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSection is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPushNotification[] List of ChildPushNotification objects
     * @throws PropelException
     */
    public function getPushNotifications(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPushNotificationsPartial && !$this->isNew();
        if (null === $this->collPushNotifications || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPushNotifications) {
                // return empty collection
                $this->initPushNotifications();
            } else {
                $collPushNotifications = ChildPushNotificationQuery::create(null, $criteria)
                    ->filterBySection($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPushNotificationsPartial && count($collPushNotifications)) {
                        $this->initPushNotifications(false);

                        foreach ($collPushNotifications as $obj) {
                            if (false == $this->collPushNotifications->contains($obj)) {
                                $this->collPushNotifications->append($obj);
                            }
                        }

                        $this->collPushNotificationsPartial = true;
                    }

                    return $collPushNotifications;
                }

                if ($partial && $this->collPushNotifications) {
                    foreach ($this->collPushNotifications as $obj) {
                        if ($obj->isNew()) {
                            $collPushNotifications[] = $obj;
                        }
                    }
                }

                $this->collPushNotifications = $collPushNotifications;
                $this->collPushNotificationsPartial = false;
            }
        }

        return $this->collPushNotifications;
    }

    /**
     * Sets a collection of ChildPushNotification objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $pushNotifications A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function setPushNotifications(Collection $pushNotifications, ConnectionInterface $con = null)
    {
        /** @var ChildPushNotification[] $pushNotificationsToDelete */
        $pushNotificationsToDelete = $this->getPushNotifications(new Criteria(), $con)->diff($pushNotifications);


        $this->pushNotificationsScheduledForDeletion = $pushNotificationsToDelete;

        foreach ($pushNotificationsToDelete as $pushNotificationRemoved) {
            $pushNotificationRemoved->setSection(null);
        }

        $this->collPushNotifications = null;
        foreach ($pushNotifications as $pushNotification) {
            $this->addPushNotification($pushNotification);
        }

        $this->collPushNotifications = $pushNotifications;
        $this->collPushNotificationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PushNotification objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PushNotification objects.
     * @throws PropelException
     */
    public function countPushNotifications(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPushNotificationsPartial && !$this->isNew();
        if (null === $this->collPushNotifications || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPushNotifications) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPushNotifications());
            }

            $query = ChildPushNotificationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySection($this)
                ->count($con);
        }

        return count($this->collPushNotifications);
    }

    /**
     * Method called to associate a ChildPushNotification object to this object
     * through the ChildPushNotification foreign key attribute.
     *
     * @param  ChildPushNotification $l ChildPushNotification
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function addPushNotification(ChildPushNotification $l)
    {
        if ($this->collPushNotifications === null) {
            $this->initPushNotifications();
            $this->collPushNotificationsPartial = true;
        }

        if (!$this->collPushNotifications->contains($l)) {
            $this->doAddPushNotification($l);

            if ($this->pushNotificationsScheduledForDeletion and $this->pushNotificationsScheduledForDeletion->contains($l)) {
                $this->pushNotificationsScheduledForDeletion->remove($this->pushNotificationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPushNotification $pushNotification The ChildPushNotification object to add.
     */
    protected function doAddPushNotification(ChildPushNotification $pushNotification)
    {
        $this->collPushNotifications[]= $pushNotification;
        $pushNotification->setSection($this);
    }

    /**
     * @param  ChildPushNotification $pushNotification The ChildPushNotification object to remove.
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function removePushNotification(ChildPushNotification $pushNotification)
    {
        if ($this->getPushNotifications()->contains($pushNotification)) {
            $pos = $this->collPushNotifications->search($pushNotification);
            $this->collPushNotifications->remove($pos);
            if (null === $this->pushNotificationsScheduledForDeletion) {
                $this->pushNotificationsScheduledForDeletion = clone $this->collPushNotifications;
                $this->pushNotificationsScheduledForDeletion->clear();
            }
            $this->pushNotificationsScheduledForDeletion[]= $pushNotification;
            $pushNotification->setSection(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Section is new, it will return
     * an empty collection; or if this Section has previously
     * been saved, it will retrieve related PushNotifications from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Section.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPushNotification[] List of ChildPushNotification objects
     */
    public function getPushNotificationsJoinPost(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPushNotificationQuery::create(null, $criteria);
        $query->joinWith('Post', $joinBehavior);

        return $this->getPushNotifications($query, $con);
    }

    /**
     * Clears out the collSectionConnectorsRelatedBySectionId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSectionConnectorsRelatedBySectionId()
     */
    public function clearSectionConnectorsRelatedBySectionId()
    {
        $this->collSectionConnectorsRelatedBySectionId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSectionConnectorsRelatedBySectionId collection loaded partially.
     */
    public function resetPartialSectionConnectorsRelatedBySectionId($v = true)
    {
        $this->collSectionConnectorsRelatedBySectionIdPartial = $v;
    }

    /**
     * Initializes the collSectionConnectorsRelatedBySectionId collection.
     *
     * By default this just sets the collSectionConnectorsRelatedBySectionId collection to an empty array (like clearcollSectionConnectorsRelatedBySectionId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSectionConnectorsRelatedBySectionId($overrideExisting = true)
    {
        if (null !== $this->collSectionConnectorsRelatedBySectionId && !$overrideExisting) {
            return;
        }

        $collectionClassName = SectionConnectorTableMap::getTableMap()->getCollectionClassName();

        $this->collSectionConnectorsRelatedBySectionId = new $collectionClassName;
        $this->collSectionConnectorsRelatedBySectionId->setModel('\Database\HubPlus\SectionConnector');
    }

    /**
     * Gets an array of ChildSectionConnector objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSection is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSectionConnector[] List of ChildSectionConnector objects
     * @throws PropelException
     */
    public function getSectionConnectorsRelatedBySectionId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionConnectorsRelatedBySectionIdPartial && !$this->isNew();
        if (null === $this->collSectionConnectorsRelatedBySectionId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSectionConnectorsRelatedBySectionId) {
                // return empty collection
                $this->initSectionConnectorsRelatedBySectionId();
            } else {
                $collSectionConnectorsRelatedBySectionId = ChildSectionConnectorQuery::create(null, $criteria)
                    ->filterBySectionRelatedBySectionId($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSectionConnectorsRelatedBySectionIdPartial && count($collSectionConnectorsRelatedBySectionId)) {
                        $this->initSectionConnectorsRelatedBySectionId(false);

                        foreach ($collSectionConnectorsRelatedBySectionId as $obj) {
                            if (false == $this->collSectionConnectorsRelatedBySectionId->contains($obj)) {
                                $this->collSectionConnectorsRelatedBySectionId->append($obj);
                            }
                        }

                        $this->collSectionConnectorsRelatedBySectionIdPartial = true;
                    }

                    return $collSectionConnectorsRelatedBySectionId;
                }

                if ($partial && $this->collSectionConnectorsRelatedBySectionId) {
                    foreach ($this->collSectionConnectorsRelatedBySectionId as $obj) {
                        if ($obj->isNew()) {
                            $collSectionConnectorsRelatedBySectionId[] = $obj;
                        }
                    }
                }

                $this->collSectionConnectorsRelatedBySectionId = $collSectionConnectorsRelatedBySectionId;
                $this->collSectionConnectorsRelatedBySectionIdPartial = false;
            }
        }

        return $this->collSectionConnectorsRelatedBySectionId;
    }

    /**
     * Sets a collection of ChildSectionConnector objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sectionConnectorsRelatedBySectionId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function setSectionConnectorsRelatedBySectionId(Collection $sectionConnectorsRelatedBySectionId, ConnectionInterface $con = null)
    {
        /** @var ChildSectionConnector[] $sectionConnectorsRelatedBySectionIdToDelete */
        $sectionConnectorsRelatedBySectionIdToDelete = $this->getSectionConnectorsRelatedBySectionId(new Criteria(), $con)->diff($sectionConnectorsRelatedBySectionId);


        $this->sectionConnectorsRelatedBySectionIdScheduledForDeletion = $sectionConnectorsRelatedBySectionIdToDelete;

        foreach ($sectionConnectorsRelatedBySectionIdToDelete as $sectionConnectorRelatedBySectionIdRemoved) {
            $sectionConnectorRelatedBySectionIdRemoved->setSectionRelatedBySectionId(null);
        }

        $this->collSectionConnectorsRelatedBySectionId = null;
        foreach ($sectionConnectorsRelatedBySectionId as $sectionConnectorRelatedBySectionId) {
            $this->addSectionConnectorRelatedBySectionId($sectionConnectorRelatedBySectionId);
        }

        $this->collSectionConnectorsRelatedBySectionId = $sectionConnectorsRelatedBySectionId;
        $this->collSectionConnectorsRelatedBySectionIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SectionConnector objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SectionConnector objects.
     * @throws PropelException
     */
    public function countSectionConnectorsRelatedBySectionId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionConnectorsRelatedBySectionIdPartial && !$this->isNew();
        if (null === $this->collSectionConnectorsRelatedBySectionId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSectionConnectorsRelatedBySectionId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSectionConnectorsRelatedBySectionId());
            }

            $query = ChildSectionConnectorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySectionRelatedBySectionId($this)
                ->count($con);
        }

        return count($this->collSectionConnectorsRelatedBySectionId);
    }

    /**
     * Method called to associate a ChildSectionConnector object to this object
     * through the ChildSectionConnector foreign key attribute.
     *
     * @param  ChildSectionConnector $l ChildSectionConnector
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function addSectionConnectorRelatedBySectionId(ChildSectionConnector $l)
    {
        if ($this->collSectionConnectorsRelatedBySectionId === null) {
            $this->initSectionConnectorsRelatedBySectionId();
            $this->collSectionConnectorsRelatedBySectionIdPartial = true;
        }

        if (!$this->collSectionConnectorsRelatedBySectionId->contains($l)) {
            $this->doAddSectionConnectorRelatedBySectionId($l);

            if ($this->sectionConnectorsRelatedBySectionIdScheduledForDeletion and $this->sectionConnectorsRelatedBySectionIdScheduledForDeletion->contains($l)) {
                $this->sectionConnectorsRelatedBySectionIdScheduledForDeletion->remove($this->sectionConnectorsRelatedBySectionIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSectionConnector $sectionConnectorRelatedBySectionId The ChildSectionConnector object to add.
     */
    protected function doAddSectionConnectorRelatedBySectionId(ChildSectionConnector $sectionConnectorRelatedBySectionId)
    {
        $this->collSectionConnectorsRelatedBySectionId[]= $sectionConnectorRelatedBySectionId;
        $sectionConnectorRelatedBySectionId->setSectionRelatedBySectionId($this);
    }

    /**
     * @param  ChildSectionConnector $sectionConnectorRelatedBySectionId The ChildSectionConnector object to remove.
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function removeSectionConnectorRelatedBySectionId(ChildSectionConnector $sectionConnectorRelatedBySectionId)
    {
        if ($this->getSectionConnectorsRelatedBySectionId()->contains($sectionConnectorRelatedBySectionId)) {
            $pos = $this->collSectionConnectorsRelatedBySectionId->search($sectionConnectorRelatedBySectionId);
            $this->collSectionConnectorsRelatedBySectionId->remove($pos);
            if (null === $this->sectionConnectorsRelatedBySectionIdScheduledForDeletion) {
                $this->sectionConnectorsRelatedBySectionIdScheduledForDeletion = clone $this->collSectionConnectorsRelatedBySectionId;
                $this->sectionConnectorsRelatedBySectionIdScheduledForDeletion->clear();
            }
            $this->sectionConnectorsRelatedBySectionIdScheduledForDeletion[]= clone $sectionConnectorRelatedBySectionId;
            $sectionConnectorRelatedBySectionId->setSectionRelatedBySectionId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Section is new, it will return
     * an empty collection; or if this Section has previously
     * been saved, it will retrieve related SectionConnectorsRelatedBySectionId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Section.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSectionConnector[] List of ChildSectionConnector objects
     */
    public function getSectionConnectorsRelatedBySectionIdJoinRemoteProvider(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSectionConnectorQuery::create(null, $criteria);
        $query->joinWith('RemoteProvider', $joinBehavior);

        return $this->getSectionConnectorsRelatedBySectionId($query, $con);
    }

    /**
     * Clears out the collSectionConnectorsRelatedByRemoteSectionId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSectionConnectorsRelatedByRemoteSectionId()
     */
    public function clearSectionConnectorsRelatedByRemoteSectionId()
    {
        $this->collSectionConnectorsRelatedByRemoteSectionId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSectionConnectorsRelatedByRemoteSectionId collection loaded partially.
     */
    public function resetPartialSectionConnectorsRelatedByRemoteSectionId($v = true)
    {
        $this->collSectionConnectorsRelatedByRemoteSectionIdPartial = $v;
    }

    /**
     * Initializes the collSectionConnectorsRelatedByRemoteSectionId collection.
     *
     * By default this just sets the collSectionConnectorsRelatedByRemoteSectionId collection to an empty array (like clearcollSectionConnectorsRelatedByRemoteSectionId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSectionConnectorsRelatedByRemoteSectionId($overrideExisting = true)
    {
        if (null !== $this->collSectionConnectorsRelatedByRemoteSectionId && !$overrideExisting) {
            return;
        }

        $collectionClassName = SectionConnectorTableMap::getTableMap()->getCollectionClassName();

        $this->collSectionConnectorsRelatedByRemoteSectionId = new $collectionClassName;
        $this->collSectionConnectorsRelatedByRemoteSectionId->setModel('\Database\HubPlus\SectionConnector');
    }

    /**
     * Gets an array of ChildSectionConnector objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSection is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSectionConnector[] List of ChildSectionConnector objects
     * @throws PropelException
     */
    public function getSectionConnectorsRelatedByRemoteSectionId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionConnectorsRelatedByRemoteSectionIdPartial && !$this->isNew();
        if (null === $this->collSectionConnectorsRelatedByRemoteSectionId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSectionConnectorsRelatedByRemoteSectionId) {
                // return empty collection
                $this->initSectionConnectorsRelatedByRemoteSectionId();
            } else {
                $collSectionConnectorsRelatedByRemoteSectionId = ChildSectionConnectorQuery::create(null, $criteria)
                    ->filterBySectionRelatedByRemoteSectionId($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSectionConnectorsRelatedByRemoteSectionIdPartial && count($collSectionConnectorsRelatedByRemoteSectionId)) {
                        $this->initSectionConnectorsRelatedByRemoteSectionId(false);

                        foreach ($collSectionConnectorsRelatedByRemoteSectionId as $obj) {
                            if (false == $this->collSectionConnectorsRelatedByRemoteSectionId->contains($obj)) {
                                $this->collSectionConnectorsRelatedByRemoteSectionId->append($obj);
                            }
                        }

                        $this->collSectionConnectorsRelatedByRemoteSectionIdPartial = true;
                    }

                    return $collSectionConnectorsRelatedByRemoteSectionId;
                }

                if ($partial && $this->collSectionConnectorsRelatedByRemoteSectionId) {
                    foreach ($this->collSectionConnectorsRelatedByRemoteSectionId as $obj) {
                        if ($obj->isNew()) {
                            $collSectionConnectorsRelatedByRemoteSectionId[] = $obj;
                        }
                    }
                }

                $this->collSectionConnectorsRelatedByRemoteSectionId = $collSectionConnectorsRelatedByRemoteSectionId;
                $this->collSectionConnectorsRelatedByRemoteSectionIdPartial = false;
            }
        }

        return $this->collSectionConnectorsRelatedByRemoteSectionId;
    }

    /**
     * Sets a collection of ChildSectionConnector objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sectionConnectorsRelatedByRemoteSectionId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function setSectionConnectorsRelatedByRemoteSectionId(Collection $sectionConnectorsRelatedByRemoteSectionId, ConnectionInterface $con = null)
    {
        /** @var ChildSectionConnector[] $sectionConnectorsRelatedByRemoteSectionIdToDelete */
        $sectionConnectorsRelatedByRemoteSectionIdToDelete = $this->getSectionConnectorsRelatedByRemoteSectionId(new Criteria(), $con)->diff($sectionConnectorsRelatedByRemoteSectionId);


        $this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion = $sectionConnectorsRelatedByRemoteSectionIdToDelete;

        foreach ($sectionConnectorsRelatedByRemoteSectionIdToDelete as $sectionConnectorRelatedByRemoteSectionIdRemoved) {
            $sectionConnectorRelatedByRemoteSectionIdRemoved->setSectionRelatedByRemoteSectionId(null);
        }

        $this->collSectionConnectorsRelatedByRemoteSectionId = null;
        foreach ($sectionConnectorsRelatedByRemoteSectionId as $sectionConnectorRelatedByRemoteSectionId) {
            $this->addSectionConnectorRelatedByRemoteSectionId($sectionConnectorRelatedByRemoteSectionId);
        }

        $this->collSectionConnectorsRelatedByRemoteSectionId = $sectionConnectorsRelatedByRemoteSectionId;
        $this->collSectionConnectorsRelatedByRemoteSectionIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SectionConnector objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SectionConnector objects.
     * @throws PropelException
     */
    public function countSectionConnectorsRelatedByRemoteSectionId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionConnectorsRelatedByRemoteSectionIdPartial && !$this->isNew();
        if (null === $this->collSectionConnectorsRelatedByRemoteSectionId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSectionConnectorsRelatedByRemoteSectionId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSectionConnectorsRelatedByRemoteSectionId());
            }

            $query = ChildSectionConnectorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySectionRelatedByRemoteSectionId($this)
                ->count($con);
        }

        return count($this->collSectionConnectorsRelatedByRemoteSectionId);
    }

    /**
     * Method called to associate a ChildSectionConnector object to this object
     * through the ChildSectionConnector foreign key attribute.
     *
     * @param  ChildSectionConnector $l ChildSectionConnector
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function addSectionConnectorRelatedByRemoteSectionId(ChildSectionConnector $l)
    {
        if ($this->collSectionConnectorsRelatedByRemoteSectionId === null) {
            $this->initSectionConnectorsRelatedByRemoteSectionId();
            $this->collSectionConnectorsRelatedByRemoteSectionIdPartial = true;
        }

        if (!$this->collSectionConnectorsRelatedByRemoteSectionId->contains($l)) {
            $this->doAddSectionConnectorRelatedByRemoteSectionId($l);

            if ($this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion and $this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion->contains($l)) {
                $this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion->remove($this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSectionConnector $sectionConnectorRelatedByRemoteSectionId The ChildSectionConnector object to add.
     */
    protected function doAddSectionConnectorRelatedByRemoteSectionId(ChildSectionConnector $sectionConnectorRelatedByRemoteSectionId)
    {
        $this->collSectionConnectorsRelatedByRemoteSectionId[]= $sectionConnectorRelatedByRemoteSectionId;
        $sectionConnectorRelatedByRemoteSectionId->setSectionRelatedByRemoteSectionId($this);
    }

    /**
     * @param  ChildSectionConnector $sectionConnectorRelatedByRemoteSectionId The ChildSectionConnector object to remove.
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function removeSectionConnectorRelatedByRemoteSectionId(ChildSectionConnector $sectionConnectorRelatedByRemoteSectionId)
    {
        if ($this->getSectionConnectorsRelatedByRemoteSectionId()->contains($sectionConnectorRelatedByRemoteSectionId)) {
            $pos = $this->collSectionConnectorsRelatedByRemoteSectionId->search($sectionConnectorRelatedByRemoteSectionId);
            $this->collSectionConnectorsRelatedByRemoteSectionId->remove($pos);
            if (null === $this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion) {
                $this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion = clone $this->collSectionConnectorsRelatedByRemoteSectionId;
                $this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion->clear();
            }
            $this->sectionConnectorsRelatedByRemoteSectionIdScheduledForDeletion[]= $sectionConnectorRelatedByRemoteSectionId;
            $sectionConnectorRelatedByRemoteSectionId->setSectionRelatedByRemoteSectionId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Section is new, it will return
     * an empty collection; or if this Section has previously
     * been saved, it will retrieve related SectionConnectorsRelatedByRemoteSectionId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Section.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSectionConnector[] List of ChildSectionConnector objects
     */
    public function getSectionConnectorsRelatedByRemoteSectionIdJoinRemoteProvider(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSectionConnectorQuery::create(null, $criteria);
        $query->joinWith('RemoteProvider', $joinBehavior);

        return $this->getSectionConnectorsRelatedByRemoteSectionId($query, $con);
    }

    /**
     * Clears out the collSectionRelatedTosRelatedByFromSectionId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSectionRelatedTosRelatedByFromSectionId()
     */
    public function clearSectionRelatedTosRelatedByFromSectionId()
    {
        $this->collSectionRelatedTosRelatedByFromSectionId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSectionRelatedTosRelatedByFromSectionId collection loaded partially.
     */
    public function resetPartialSectionRelatedTosRelatedByFromSectionId($v = true)
    {
        $this->collSectionRelatedTosRelatedByFromSectionIdPartial = $v;
    }

    /**
     * Initializes the collSectionRelatedTosRelatedByFromSectionId collection.
     *
     * By default this just sets the collSectionRelatedTosRelatedByFromSectionId collection to an empty array (like clearcollSectionRelatedTosRelatedByFromSectionId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSectionRelatedTosRelatedByFromSectionId($overrideExisting = true)
    {
        if (null !== $this->collSectionRelatedTosRelatedByFromSectionId && !$overrideExisting) {
            return;
        }

        $collectionClassName = SectionRelatedToTableMap::getTableMap()->getCollectionClassName();

        $this->collSectionRelatedTosRelatedByFromSectionId = new $collectionClassName;
        $this->collSectionRelatedTosRelatedByFromSectionId->setModel('\Database\HubPlus\SectionRelatedTo');
    }

    /**
     * Gets an array of ChildSectionRelatedTo objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSection is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSectionRelatedTo[] List of ChildSectionRelatedTo objects
     * @throws PropelException
     */
    public function getSectionRelatedTosRelatedByFromSectionId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionRelatedTosRelatedByFromSectionIdPartial && !$this->isNew();
        if (null === $this->collSectionRelatedTosRelatedByFromSectionId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSectionRelatedTosRelatedByFromSectionId) {
                // return empty collection
                $this->initSectionRelatedTosRelatedByFromSectionId();
            } else {
                $collSectionRelatedTosRelatedByFromSectionId = ChildSectionRelatedToQuery::create(null, $criteria)
                    ->filterBySectionRelatedByFromSectionId($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSectionRelatedTosRelatedByFromSectionIdPartial && count($collSectionRelatedTosRelatedByFromSectionId)) {
                        $this->initSectionRelatedTosRelatedByFromSectionId(false);

                        foreach ($collSectionRelatedTosRelatedByFromSectionId as $obj) {
                            if (false == $this->collSectionRelatedTosRelatedByFromSectionId->contains($obj)) {
                                $this->collSectionRelatedTosRelatedByFromSectionId->append($obj);
                            }
                        }

                        $this->collSectionRelatedTosRelatedByFromSectionIdPartial = true;
                    }

                    return $collSectionRelatedTosRelatedByFromSectionId;
                }

                if ($partial && $this->collSectionRelatedTosRelatedByFromSectionId) {
                    foreach ($this->collSectionRelatedTosRelatedByFromSectionId as $obj) {
                        if ($obj->isNew()) {
                            $collSectionRelatedTosRelatedByFromSectionId[] = $obj;
                        }
                    }
                }

                $this->collSectionRelatedTosRelatedByFromSectionId = $collSectionRelatedTosRelatedByFromSectionId;
                $this->collSectionRelatedTosRelatedByFromSectionIdPartial = false;
            }
        }

        return $this->collSectionRelatedTosRelatedByFromSectionId;
    }

    /**
     * Sets a collection of ChildSectionRelatedTo objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sectionRelatedTosRelatedByFromSectionId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function setSectionRelatedTosRelatedByFromSectionId(Collection $sectionRelatedTosRelatedByFromSectionId, ConnectionInterface $con = null)
    {
        /** @var ChildSectionRelatedTo[] $sectionRelatedTosRelatedByFromSectionIdToDelete */
        $sectionRelatedTosRelatedByFromSectionIdToDelete = $this->getSectionRelatedTosRelatedByFromSectionId(new Criteria(), $con)->diff($sectionRelatedTosRelatedByFromSectionId);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion = clone $sectionRelatedTosRelatedByFromSectionIdToDelete;

        foreach ($sectionRelatedTosRelatedByFromSectionIdToDelete as $sectionRelatedToRelatedByFromSectionIdRemoved) {
            $sectionRelatedToRelatedByFromSectionIdRemoved->setSectionRelatedByFromSectionId(null);
        }

        $this->collSectionRelatedTosRelatedByFromSectionId = null;
        foreach ($sectionRelatedTosRelatedByFromSectionId as $sectionRelatedToRelatedByFromSectionId) {
            $this->addSectionRelatedToRelatedByFromSectionId($sectionRelatedToRelatedByFromSectionId);
        }

        $this->collSectionRelatedTosRelatedByFromSectionId = $sectionRelatedTosRelatedByFromSectionId;
        $this->collSectionRelatedTosRelatedByFromSectionIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SectionRelatedTo objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SectionRelatedTo objects.
     * @throws PropelException
     */
    public function countSectionRelatedTosRelatedByFromSectionId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionRelatedTosRelatedByFromSectionIdPartial && !$this->isNew();
        if (null === $this->collSectionRelatedTosRelatedByFromSectionId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSectionRelatedTosRelatedByFromSectionId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSectionRelatedTosRelatedByFromSectionId());
            }

            $query = ChildSectionRelatedToQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySectionRelatedByFromSectionId($this)
                ->count($con);
        }

        return count($this->collSectionRelatedTosRelatedByFromSectionId);
    }

    /**
     * Method called to associate a ChildSectionRelatedTo object to this object
     * through the ChildSectionRelatedTo foreign key attribute.
     *
     * @param  ChildSectionRelatedTo $l ChildSectionRelatedTo
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function addSectionRelatedToRelatedByFromSectionId(ChildSectionRelatedTo $l)
    {
        if ($this->collSectionRelatedTosRelatedByFromSectionId === null) {
            $this->initSectionRelatedTosRelatedByFromSectionId();
            $this->collSectionRelatedTosRelatedByFromSectionIdPartial = true;
        }

        if (!$this->collSectionRelatedTosRelatedByFromSectionId->contains($l)) {
            $this->doAddSectionRelatedToRelatedByFromSectionId($l);

            if ($this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion and $this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion->contains($l)) {
                $this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion->remove($this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSectionRelatedTo $sectionRelatedToRelatedByFromSectionId The ChildSectionRelatedTo object to add.
     */
    protected function doAddSectionRelatedToRelatedByFromSectionId(ChildSectionRelatedTo $sectionRelatedToRelatedByFromSectionId)
    {
        $this->collSectionRelatedTosRelatedByFromSectionId[]= $sectionRelatedToRelatedByFromSectionId;
        $sectionRelatedToRelatedByFromSectionId->setSectionRelatedByFromSectionId($this);
    }

    /**
     * @param  ChildSectionRelatedTo $sectionRelatedToRelatedByFromSectionId The ChildSectionRelatedTo object to remove.
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function removeSectionRelatedToRelatedByFromSectionId(ChildSectionRelatedTo $sectionRelatedToRelatedByFromSectionId)
    {
        if ($this->getSectionRelatedTosRelatedByFromSectionId()->contains($sectionRelatedToRelatedByFromSectionId)) {
            $pos = $this->collSectionRelatedTosRelatedByFromSectionId->search($sectionRelatedToRelatedByFromSectionId);
            $this->collSectionRelatedTosRelatedByFromSectionId->remove($pos);
            if (null === $this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion) {
                $this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion = clone $this->collSectionRelatedTosRelatedByFromSectionId;
                $this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion->clear();
            }
            $this->sectionRelatedTosRelatedByFromSectionIdScheduledForDeletion[]= clone $sectionRelatedToRelatedByFromSectionId;
            $sectionRelatedToRelatedByFromSectionId->setSectionRelatedByFromSectionId(null);
        }

        return $this;
    }

    /**
     * Clears out the collSectionRelatedTosRelatedByToSectionId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSectionRelatedTosRelatedByToSectionId()
     */
    public function clearSectionRelatedTosRelatedByToSectionId()
    {
        $this->collSectionRelatedTosRelatedByToSectionId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSectionRelatedTosRelatedByToSectionId collection loaded partially.
     */
    public function resetPartialSectionRelatedTosRelatedByToSectionId($v = true)
    {
        $this->collSectionRelatedTosRelatedByToSectionIdPartial = $v;
    }

    /**
     * Initializes the collSectionRelatedTosRelatedByToSectionId collection.
     *
     * By default this just sets the collSectionRelatedTosRelatedByToSectionId collection to an empty array (like clearcollSectionRelatedTosRelatedByToSectionId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSectionRelatedTosRelatedByToSectionId($overrideExisting = true)
    {
        if (null !== $this->collSectionRelatedTosRelatedByToSectionId && !$overrideExisting) {
            return;
        }

        $collectionClassName = SectionRelatedToTableMap::getTableMap()->getCollectionClassName();

        $this->collSectionRelatedTosRelatedByToSectionId = new $collectionClassName;
        $this->collSectionRelatedTosRelatedByToSectionId->setModel('\Database\HubPlus\SectionRelatedTo');
    }

    /**
     * Gets an array of ChildSectionRelatedTo objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSection is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSectionRelatedTo[] List of ChildSectionRelatedTo objects
     * @throws PropelException
     */
    public function getSectionRelatedTosRelatedByToSectionId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionRelatedTosRelatedByToSectionIdPartial && !$this->isNew();
        if (null === $this->collSectionRelatedTosRelatedByToSectionId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSectionRelatedTosRelatedByToSectionId) {
                // return empty collection
                $this->initSectionRelatedTosRelatedByToSectionId();
            } else {
                $collSectionRelatedTosRelatedByToSectionId = ChildSectionRelatedToQuery::create(null, $criteria)
                    ->filterBySectionRelatedByToSectionId($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSectionRelatedTosRelatedByToSectionIdPartial && count($collSectionRelatedTosRelatedByToSectionId)) {
                        $this->initSectionRelatedTosRelatedByToSectionId(false);

                        foreach ($collSectionRelatedTosRelatedByToSectionId as $obj) {
                            if (false == $this->collSectionRelatedTosRelatedByToSectionId->contains($obj)) {
                                $this->collSectionRelatedTosRelatedByToSectionId->append($obj);
                            }
                        }

                        $this->collSectionRelatedTosRelatedByToSectionIdPartial = true;
                    }

                    return $collSectionRelatedTosRelatedByToSectionId;
                }

                if ($partial && $this->collSectionRelatedTosRelatedByToSectionId) {
                    foreach ($this->collSectionRelatedTosRelatedByToSectionId as $obj) {
                        if ($obj->isNew()) {
                            $collSectionRelatedTosRelatedByToSectionId[] = $obj;
                        }
                    }
                }

                $this->collSectionRelatedTosRelatedByToSectionId = $collSectionRelatedTosRelatedByToSectionId;
                $this->collSectionRelatedTosRelatedByToSectionIdPartial = false;
            }
        }

        return $this->collSectionRelatedTosRelatedByToSectionId;
    }

    /**
     * Sets a collection of ChildSectionRelatedTo objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sectionRelatedTosRelatedByToSectionId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function setSectionRelatedTosRelatedByToSectionId(Collection $sectionRelatedTosRelatedByToSectionId, ConnectionInterface $con = null)
    {
        /** @var ChildSectionRelatedTo[] $sectionRelatedTosRelatedByToSectionIdToDelete */
        $sectionRelatedTosRelatedByToSectionIdToDelete = $this->getSectionRelatedTosRelatedByToSectionId(new Criteria(), $con)->diff($sectionRelatedTosRelatedByToSectionId);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion = clone $sectionRelatedTosRelatedByToSectionIdToDelete;

        foreach ($sectionRelatedTosRelatedByToSectionIdToDelete as $sectionRelatedToRelatedByToSectionIdRemoved) {
            $sectionRelatedToRelatedByToSectionIdRemoved->setSectionRelatedByToSectionId(null);
        }

        $this->collSectionRelatedTosRelatedByToSectionId = null;
        foreach ($sectionRelatedTosRelatedByToSectionId as $sectionRelatedToRelatedByToSectionId) {
            $this->addSectionRelatedToRelatedByToSectionId($sectionRelatedToRelatedByToSectionId);
        }

        $this->collSectionRelatedTosRelatedByToSectionId = $sectionRelatedTosRelatedByToSectionId;
        $this->collSectionRelatedTosRelatedByToSectionIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SectionRelatedTo objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SectionRelatedTo objects.
     * @throws PropelException
     */
    public function countSectionRelatedTosRelatedByToSectionId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSectionRelatedTosRelatedByToSectionIdPartial && !$this->isNew();
        if (null === $this->collSectionRelatedTosRelatedByToSectionId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSectionRelatedTosRelatedByToSectionId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSectionRelatedTosRelatedByToSectionId());
            }

            $query = ChildSectionRelatedToQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySectionRelatedByToSectionId($this)
                ->count($con);
        }

        return count($this->collSectionRelatedTosRelatedByToSectionId);
    }

    /**
     * Method called to associate a ChildSectionRelatedTo object to this object
     * through the ChildSectionRelatedTo foreign key attribute.
     *
     * @param  ChildSectionRelatedTo $l ChildSectionRelatedTo
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function addSectionRelatedToRelatedByToSectionId(ChildSectionRelatedTo $l)
    {
        if ($this->collSectionRelatedTosRelatedByToSectionId === null) {
            $this->initSectionRelatedTosRelatedByToSectionId();
            $this->collSectionRelatedTosRelatedByToSectionIdPartial = true;
        }

        if (!$this->collSectionRelatedTosRelatedByToSectionId->contains($l)) {
            $this->doAddSectionRelatedToRelatedByToSectionId($l);

            if ($this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion and $this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion->contains($l)) {
                $this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion->remove($this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSectionRelatedTo $sectionRelatedToRelatedByToSectionId The ChildSectionRelatedTo object to add.
     */
    protected function doAddSectionRelatedToRelatedByToSectionId(ChildSectionRelatedTo $sectionRelatedToRelatedByToSectionId)
    {
        $this->collSectionRelatedTosRelatedByToSectionId[]= $sectionRelatedToRelatedByToSectionId;
        $sectionRelatedToRelatedByToSectionId->setSectionRelatedByToSectionId($this);
    }

    /**
     * @param  ChildSectionRelatedTo $sectionRelatedToRelatedByToSectionId The ChildSectionRelatedTo object to remove.
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function removeSectionRelatedToRelatedByToSectionId(ChildSectionRelatedTo $sectionRelatedToRelatedByToSectionId)
    {
        if ($this->getSectionRelatedTosRelatedByToSectionId()->contains($sectionRelatedToRelatedByToSectionId)) {
            $pos = $this->collSectionRelatedTosRelatedByToSectionId->search($sectionRelatedToRelatedByToSectionId);
            $this->collSectionRelatedTosRelatedByToSectionId->remove($pos);
            if (null === $this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion) {
                $this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion = clone $this->collSectionRelatedTosRelatedByToSectionId;
                $this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion->clear();
            }
            $this->sectionRelatedTosRelatedByToSectionIdScheduledForDeletion[]= clone $sectionRelatedToRelatedByToSectionId;
            $sectionRelatedToRelatedByToSectionId->setSectionRelatedByToSectionId(null);
        }

        return $this;
    }

    /**
     * Clears out the collUserApps collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUserApps()
     */
    public function clearUserApps()
    {
        $this->collUserApps = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUserApps collection loaded partially.
     */
    public function resetPartialUserApps($v = true)
    {
        $this->collUserAppsPartial = $v;
    }

    /**
     * Initializes the collUserApps collection.
     *
     * By default this just sets the collUserApps collection to an empty array (like clearcollUserApps());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserApps($overrideExisting = true)
    {
        if (null !== $this->collUserApps && !$overrideExisting) {
            return;
        }

        $collectionClassName = UserAppTableMap::getTableMap()->getCollectionClassName();

        $this->collUserApps = new $collectionClassName;
        $this->collUserApps->setModel('\Database\HubPlus\UserApp');
    }

    /**
     * Gets an array of ChildUserApp objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSection is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUserApp[] List of ChildUserApp objects
     * @throws PropelException
     */
    public function getUserApps(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUserAppsPartial && !$this->isNew();
        if (null === $this->collUserApps || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUserApps) {
                // return empty collection
                $this->initUserApps();
            } else {
                $collUserApps = ChildUserAppQuery::create(null, $criteria)
                    ->filterBySection($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUserAppsPartial && count($collUserApps)) {
                        $this->initUserApps(false);

                        foreach ($collUserApps as $obj) {
                            if (false == $this->collUserApps->contains($obj)) {
                                $this->collUserApps->append($obj);
                            }
                        }

                        $this->collUserAppsPartial = true;
                    }

                    return $collUserApps;
                }

                if ($partial && $this->collUserApps) {
                    foreach ($this->collUserApps as $obj) {
                        if ($obj->isNew()) {
                            $collUserApps[] = $obj;
                        }
                    }
                }

                $this->collUserApps = $collUserApps;
                $this->collUserAppsPartial = false;
            }
        }

        return $this->collUserApps;
    }

    /**
     * Sets a collection of ChildUserApp objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $userApps A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function setUserApps(Collection $userApps, ConnectionInterface $con = null)
    {
        /** @var ChildUserApp[] $userAppsToDelete */
        $userAppsToDelete = $this->getUserApps(new Criteria(), $con)->diff($userApps);


        $this->userAppsScheduledForDeletion = $userAppsToDelete;

        foreach ($userAppsToDelete as $userAppRemoved) {
            $userAppRemoved->setSection(null);
        }

        $this->collUserApps = null;
        foreach ($userApps as $userApp) {
            $this->addUserApp($userApp);
        }

        $this->collUserApps = $userApps;
        $this->collUserAppsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserApp objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UserApp objects.
     * @throws PropelException
     */
    public function countUserApps(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUserAppsPartial && !$this->isNew();
        if (null === $this->collUserApps || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserApps) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserApps());
            }

            $query = ChildUserAppQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySection($this)
                ->count($con);
        }

        return count($this->collUserApps);
    }

    /**
     * Method called to associate a ChildUserApp object to this object
     * through the ChildUserApp foreign key attribute.
     *
     * @param  ChildUserApp $l ChildUserApp
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function addUserApp(ChildUserApp $l)
    {
        if ($this->collUserApps === null) {
            $this->initUserApps();
            $this->collUserAppsPartial = true;
        }

        if (!$this->collUserApps->contains($l)) {
            $this->doAddUserApp($l);

            if ($this->userAppsScheduledForDeletion and $this->userAppsScheduledForDeletion->contains($l)) {
                $this->userAppsScheduledForDeletion->remove($this->userAppsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUserApp $userApp The ChildUserApp object to add.
     */
    protected function doAddUserApp(ChildUserApp $userApp)
    {
        $this->collUserApps[]= $userApp;
        $userApp->setSection($this);
    }

    /**
     * @param  ChildUserApp $userApp The ChildUserApp object to remove.
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function removeUserApp(ChildUserApp $userApp)
    {
        if ($this->getUserApps()->contains($userApp)) {
            $pos = $this->collUserApps->search($userApp);
            $this->collUserApps->remove($pos);
            if (null === $this->userAppsScheduledForDeletion) {
                $this->userAppsScheduledForDeletion = clone $this->collUserApps;
                $this->userAppsScheduledForDeletion->clear();
            }
            $this->userAppsScheduledForDeletion[]= $userApp;
            $userApp->setSection(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Section is new, it will return
     * an empty collection; or if this Section has previously
     * been saved, it will retrieve related UserApps from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Section.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUserApp[] List of ChildUserApp objects
     */
    public function getUserAppsJoinAddress(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUserAppQuery::create(null, $criteria);
        $query->joinWith('Address', $joinBehavior);

        return $this->getUserApps($query, $con);
    }

    /**
     * Clears out the collWebhooks collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addWebhooks()
     */
    public function clearWebhooks()
    {
        $this->collWebhooks = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collWebhooks collection loaded partially.
     */
    public function resetPartialWebhooks($v = true)
    {
        $this->collWebhooksPartial = $v;
    }

    /**
     * Initializes the collWebhooks collection.
     *
     * By default this just sets the collWebhooks collection to an empty array (like clearcollWebhooks());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWebhooks($overrideExisting = true)
    {
        if (null !== $this->collWebhooks && !$overrideExisting) {
            return;
        }

        $collectionClassName = WebhookTableMap::getTableMap()->getCollectionClassName();

        $this->collWebhooks = new $collectionClassName;
        $this->collWebhooks->setModel('\Database\HubPlus\Webhook');
    }

    /**
     * Gets an array of ChildWebhook objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSection is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildWebhook[] List of ChildWebhook objects
     * @throws PropelException
     */
    public function getWebhooks(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collWebhooksPartial && !$this->isNew();
        if (null === $this->collWebhooks || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWebhooks) {
                // return empty collection
                $this->initWebhooks();
            } else {
                $collWebhooks = ChildWebhookQuery::create(null, $criteria)
                    ->filterBySection($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collWebhooksPartial && count($collWebhooks)) {
                        $this->initWebhooks(false);

                        foreach ($collWebhooks as $obj) {
                            if (false == $this->collWebhooks->contains($obj)) {
                                $this->collWebhooks->append($obj);
                            }
                        }

                        $this->collWebhooksPartial = true;
                    }

                    return $collWebhooks;
                }

                if ($partial && $this->collWebhooks) {
                    foreach ($this->collWebhooks as $obj) {
                        if ($obj->isNew()) {
                            $collWebhooks[] = $obj;
                        }
                    }
                }

                $this->collWebhooks = $collWebhooks;
                $this->collWebhooksPartial = false;
            }
        }

        return $this->collWebhooks;
    }

    /**
     * Sets a collection of ChildWebhook objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $webhooks A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function setWebhooks(Collection $webhooks, ConnectionInterface $con = null)
    {
        /** @var ChildWebhook[] $webhooksToDelete */
        $webhooksToDelete = $this->getWebhooks(new Criteria(), $con)->diff($webhooks);


        $this->webhooksScheduledForDeletion = $webhooksToDelete;

        foreach ($webhooksToDelete as $webhookRemoved) {
            $webhookRemoved->setSection(null);
        }

        $this->collWebhooks = null;
        foreach ($webhooks as $webhook) {
            $this->addWebhook($webhook);
        }

        $this->collWebhooks = $webhooks;
        $this->collWebhooksPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Webhook objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Webhook objects.
     * @throws PropelException
     */
    public function countWebhooks(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collWebhooksPartial && !$this->isNew();
        if (null === $this->collWebhooks || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWebhooks) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getWebhooks());
            }

            $query = ChildWebhookQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySection($this)
                ->count($con);
        }

        return count($this->collWebhooks);
    }

    /**
     * Method called to associate a ChildWebhook object to this object
     * through the ChildWebhook foreign key attribute.
     *
     * @param  ChildWebhook $l ChildWebhook
     * @return $this|\Database\HubPlus\Section The current object (for fluent API support)
     */
    public function addWebhook(ChildWebhook $l)
    {
        if ($this->collWebhooks === null) {
            $this->initWebhooks();
            $this->collWebhooksPartial = true;
        }

        if (!$this->collWebhooks->contains($l)) {
            $this->doAddWebhook($l);

            if ($this->webhooksScheduledForDeletion and $this->webhooksScheduledForDeletion->contains($l)) {
                $this->webhooksScheduledForDeletion->remove($this->webhooksScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildWebhook $webhook The ChildWebhook object to add.
     */
    protected function doAddWebhook(ChildWebhook $webhook)
    {
        $this->collWebhooks[]= $webhook;
        $webhook->setSection($this);
    }

    /**
     * @param  ChildWebhook $webhook The ChildWebhook object to remove.
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function removeWebhook(ChildWebhook $webhook)
    {
        if ($this->getWebhooks()->contains($webhook)) {
            $pos = $this->collWebhooks->search($webhook);
            $this->collWebhooks->remove($pos);
            if (null === $this->webhooksScheduledForDeletion) {
                $this->webhooksScheduledForDeletion = clone $this->collWebhooks;
                $this->webhooksScheduledForDeletion->clear();
            }
            $this->webhooksScheduledForDeletion[]= $webhook;
            $webhook->setSection(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Section is new, it will return
     * an empty collection; or if this Section has previously
     * been saved, it will retrieve related Webhooks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Section.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildWebhook[] List of ChildWebhook objects
     */
    public function getWebhooksJoinRemoteProvider(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildWebhookQuery::create(null, $criteria);
        $query->joinWith('RemoteProvider', $joinBehavior);

        return $this->getWebhooks($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Section is new, it will return
     * an empty collection; or if this Section has previously
     * been saved, it will retrieve related Webhooks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Section.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildWebhook[] List of ChildWebhook objects
     */
    public function getWebhooksJoinPost(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildWebhookQuery::create(null, $criteria);
        $query->joinWith('Post', $joinBehavior);

        return $this->getWebhooks($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aTemplate) {
            $this->aTemplate->removeSection($this);
        }
        if (null !== $this->aMediaRelatedByIconId) {
            $this->aMediaRelatedByIconId->removeSectionRelatedByIconId($this);
        }
        if (null !== $this->aMediaRelatedByIconSelectedId) {
            $this->aMediaRelatedByIconSelectedId->removeSectionRelatedByIconSelectedId($this);
        }
        if (null !== $this->aUserBackend) {
            $this->aUserBackend->removeSection($this);
        }
        $this->id = null;
        $this->area = null;
        $this->type = null;
        $this->author_id = null;
        $this->title = null;
        $this->description = null;
        $this->slug = null;
        $this->icon_id = null;
        $this->icon_selected_id = null;
        $this->resource = null;
        $this->params = null;
        $this->template_id = null;
        $this->layout = null;
        $this->tags = null;
        $this->tags_user = null;
        $this->parsed_tags = null;
        $this->status = null;
        $this->weight = null;
        $this->deleted_at = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCustomFormExtraLogs) {
                foreach ($this->collCustomFormExtraLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCustomFormLogs) {
                foreach ($this->collCustomFormLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPostActions) {
                foreach ($this->collPostActions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPushNotifications) {
                foreach ($this->collPushNotifications as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSectionConnectorsRelatedBySectionId) {
                foreach ($this->collSectionConnectorsRelatedBySectionId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSectionConnectorsRelatedByRemoteSectionId) {
                foreach ($this->collSectionConnectorsRelatedByRemoteSectionId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSectionRelatedTosRelatedByFromSectionId) {
                foreach ($this->collSectionRelatedTosRelatedByFromSectionId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSectionRelatedTosRelatedByToSectionId) {
                foreach ($this->collSectionRelatedTosRelatedByToSectionId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUserApps) {
                foreach ($this->collUserApps as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWebhooks) {
                foreach ($this->collWebhooks as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCustomFormExtraLogs = null;
        $this->collCustomFormLogs = null;
        $this->collPostActions = null;
        $this->collPushNotifications = null;
        $this->collSectionConnectorsRelatedBySectionId = null;
        $this->collSectionConnectorsRelatedByRemoteSectionId = null;
        $this->collSectionRelatedTosRelatedByFromSectionId = null;
        $this->collSectionRelatedTosRelatedByToSectionId = null;
        $this->collUserApps = null;
        $this->collWebhooks = null;
        $this->aTemplate = null;
        $this->aMediaRelatedByIconId = null;
        $this->aMediaRelatedByIconSelectedId = null;
        $this->aUserBackend = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SectionTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildSection The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[SectionTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildSectionArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildSectionArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildSectionArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildSectionArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildSectionArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildSection The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setId($archive->getId());
        }
        $this->setArea($archive->getArea());
        $this->setType($archive->getType());
        $this->setAuthorId($archive->getAuthorId());
        $this->setTitle($archive->getTitle());
        $this->setDescription($archive->getDescription());
        $this->setSlug($archive->getSlug());
        $this->setIconId($archive->getIconId());
        $this->setIconSelectedId($archive->getIconSelectedId());
        $this->setResource($archive->getResource());
        $this->setParams($archive->getParams());
        $this->setTemplateId($archive->getTemplateId());
        $this->setLayout($archive->getLayout());
        $this->setTags($archive->getTags());
        $this->setTagsUser($archive->getTagsUser());
        $this->setParsedTags($archive->getParsedTags());
        $this->setStatus($archive->getStatus());
        $this->setWeight($archive->getWeight());
        $this->setDeletedAt($archive->getDeletedAt());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildSection The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
