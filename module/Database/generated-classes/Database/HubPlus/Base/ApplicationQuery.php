<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\Application as ChildApplication;
use Database\HubPlus\ApplicationArchive as ChildApplicationArchive;
use Database\HubPlus\ApplicationQuery as ChildApplicationQuery;
use Database\HubPlus\Map\ApplicationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'application' table.
 *
 *
 *
 * @method     ChildApplicationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildApplicationQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildApplicationQuery orderBySlug($order = Criteria::ASC) Order by the slug column
 * @method     ChildApplicationQuery orderByBundle($order = Criteria::ASC) Order by the bundle column
 * @method     ChildApplicationQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildApplicationQuery orderByAppKey($order = Criteria::ASC) Order by the app_key column
 * @method     ChildApplicationQuery orderByDsn($order = Criteria::ASC) Order by the dsn column
 * @method     ChildApplicationQuery orderByDbHost($order = Criteria::ASC) Order by the db_host column
 * @method     ChildApplicationQuery orderByDbName($order = Criteria::ASC) Order by the db_name column
 * @method     ChildApplicationQuery orderByDbUser($order = Criteria::ASC) Order by the db_user column
 * @method     ChildApplicationQuery orderByDbPwd($order = Criteria::ASC) Order by the db_pwd column
 * @method     ChildApplicationQuery orderByApiVersion($order = Criteria::ASC) Order by the api_version column
 * @method     ChildApplicationQuery orderByPackId($order = Criteria::ASC) Order by the pack_id column
 * @method     ChildApplicationQuery orderByCategoryId($order = Criteria::ASC) Order by the category_id column
 * @method     ChildApplicationQuery orderByTemplateId($order = Criteria::ASC) Order by the template_id column
 * @method     ChildApplicationQuery orderByPushNotification($order = Criteria::ASC) Order by the push_notification column
 * @method     ChildApplicationQuery orderByMaxNotification($order = Criteria::ASC) Order by the max_notification column
 * @method     ChildApplicationQuery orderByMaxAdviceHour($order = Criteria::ASC) Order by the max_advice_hour column
 * @method     ChildApplicationQuery orderByAdviceHourDone($order = Criteria::ASC) Order by the advice_hour_done column
 * @method     ChildApplicationQuery orderByMaxContentInsert($order = Criteria::ASC) Order by the max_content_insert column
 * @method     ChildApplicationQuery orderByIcon($order = Criteria::ASC) Order by the icon column
 * @method     ChildApplicationQuery orderBySplashScreen($order = Criteria::ASC) Order by the splash_screen column
 * @method     ChildApplicationQuery orderByBackgroundColor($order = Criteria::ASC) Order by the background_color column
 * @method     ChildApplicationQuery orderByAppLogo($order = Criteria::ASC) Order by the app_logo column
 * @method     ChildApplicationQuery orderByLayout($order = Criteria::ASC) Order by the layout column
 * @method     ChildApplicationQuery orderByWhiteLabel($order = Criteria::ASC) Order by the white_label column
 * @method     ChildApplicationQuery orderByDemo($order = Criteria::ASC) Order by the demo column
 * @method     ChildApplicationQuery orderByFacebookToken($order = Criteria::ASC) Order by the facebook_token column
 * @method     ChildApplicationQuery orderByAppleId($order = Criteria::ASC) Order by the apple_id column
 * @method     ChildApplicationQuery orderByAppleIdPassword($order = Criteria::ASC) Order by the apple_id_password column
 * @method     ChildApplicationQuery orderByAppleStoreAppLink($order = Criteria::ASC) Order by the apple_store_app_link column
 * @method     ChildApplicationQuery orderByPlayStoreId($order = Criteria::ASC) Order by the play_store_id column
 * @method     ChildApplicationQuery orderByPlayStoreIdPassword($order = Criteria::ASC) Order by the play_store_id_password column
 * @method     ChildApplicationQuery orderByPlayStoreAppLink($order = Criteria::ASC) Order by the play_store_app_link column
 * @method     ChildApplicationQuery orderByControllerUri($order = Criteria::ASC) Order by the controller_uri column
 * @method     ChildApplicationQuery orderByGoogleAnalyticsUa($order = Criteria::ASC) Order by the google_analytics_ua column
 * @method     ChildApplicationQuery orderByPublished($order = Criteria::ASC) Order by the published column
 * @method     ChildApplicationQuery orderByExpiredAt($order = Criteria::ASC) Order by the expired_at column
 * @method     ChildApplicationQuery orderByMainContentsLanguage($order = Criteria::ASC) Order by the main_contents_language column
 * @method     ChildApplicationQuery orderByOtherContentsLanguages($order = Criteria::ASC) Order by the other_contents_languages column
 * @method     ChildApplicationQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildApplicationQuery orderBySystemIcons($order = Criteria::ASC) Order by the system_icons column
 * @method     ChildApplicationQuery orderByToolbar($order = Criteria::ASC) Order by the toolbar column
 * @method     ChildApplicationQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildApplicationQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildApplicationQuery groupById() Group by the id column
 * @method     ChildApplicationQuery groupByTitle() Group by the title column
 * @method     ChildApplicationQuery groupBySlug() Group by the slug column
 * @method     ChildApplicationQuery groupByBundle() Group by the bundle column
 * @method     ChildApplicationQuery groupByDescription() Group by the description column
 * @method     ChildApplicationQuery groupByAppKey() Group by the app_key column
 * @method     ChildApplicationQuery groupByDsn() Group by the dsn column
 * @method     ChildApplicationQuery groupByDbHost() Group by the db_host column
 * @method     ChildApplicationQuery groupByDbName() Group by the db_name column
 * @method     ChildApplicationQuery groupByDbUser() Group by the db_user column
 * @method     ChildApplicationQuery groupByDbPwd() Group by the db_pwd column
 * @method     ChildApplicationQuery groupByApiVersion() Group by the api_version column
 * @method     ChildApplicationQuery groupByPackId() Group by the pack_id column
 * @method     ChildApplicationQuery groupByCategoryId() Group by the category_id column
 * @method     ChildApplicationQuery groupByTemplateId() Group by the template_id column
 * @method     ChildApplicationQuery groupByPushNotification() Group by the push_notification column
 * @method     ChildApplicationQuery groupByMaxNotification() Group by the max_notification column
 * @method     ChildApplicationQuery groupByMaxAdviceHour() Group by the max_advice_hour column
 * @method     ChildApplicationQuery groupByAdviceHourDone() Group by the advice_hour_done column
 * @method     ChildApplicationQuery groupByMaxContentInsert() Group by the max_content_insert column
 * @method     ChildApplicationQuery groupByIcon() Group by the icon column
 * @method     ChildApplicationQuery groupBySplashScreen() Group by the splash_screen column
 * @method     ChildApplicationQuery groupByBackgroundColor() Group by the background_color column
 * @method     ChildApplicationQuery groupByAppLogo() Group by the app_logo column
 * @method     ChildApplicationQuery groupByLayout() Group by the layout column
 * @method     ChildApplicationQuery groupByWhiteLabel() Group by the white_label column
 * @method     ChildApplicationQuery groupByDemo() Group by the demo column
 * @method     ChildApplicationQuery groupByFacebookToken() Group by the facebook_token column
 * @method     ChildApplicationQuery groupByAppleId() Group by the apple_id column
 * @method     ChildApplicationQuery groupByAppleIdPassword() Group by the apple_id_password column
 * @method     ChildApplicationQuery groupByAppleStoreAppLink() Group by the apple_store_app_link column
 * @method     ChildApplicationQuery groupByPlayStoreId() Group by the play_store_id column
 * @method     ChildApplicationQuery groupByPlayStoreIdPassword() Group by the play_store_id_password column
 * @method     ChildApplicationQuery groupByPlayStoreAppLink() Group by the play_store_app_link column
 * @method     ChildApplicationQuery groupByControllerUri() Group by the controller_uri column
 * @method     ChildApplicationQuery groupByGoogleAnalyticsUa() Group by the google_analytics_ua column
 * @method     ChildApplicationQuery groupByPublished() Group by the published column
 * @method     ChildApplicationQuery groupByExpiredAt() Group by the expired_at column
 * @method     ChildApplicationQuery groupByMainContentsLanguage() Group by the main_contents_language column
 * @method     ChildApplicationQuery groupByOtherContentsLanguages() Group by the other_contents_languages column
 * @method     ChildApplicationQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildApplicationQuery groupBySystemIcons() Group by the system_icons column
 * @method     ChildApplicationQuery groupByToolbar() Group by the toolbar column
 * @method     ChildApplicationQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildApplicationQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildApplicationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildApplicationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildApplicationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildApplicationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildApplicationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildApplicationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildApplicationQuery leftJoinCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the Category relation
 * @method     ChildApplicationQuery rightJoinCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Category relation
 * @method     ChildApplicationQuery innerJoinCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the Category relation
 *
 * @method     ChildApplicationQuery joinWithCategory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Category relation
 *
 * @method     ChildApplicationQuery leftJoinWithCategory() Adds a LEFT JOIN clause and with to the query using the Category relation
 * @method     ChildApplicationQuery rightJoinWithCategory() Adds a RIGHT JOIN clause and with to the query using the Category relation
 * @method     ChildApplicationQuery innerJoinWithCategory() Adds a INNER JOIN clause and with to the query using the Category relation
 *
 * @method     ChildApplicationQuery leftJoinTemplate($relationAlias = null) Adds a LEFT JOIN clause to the query using the Template relation
 * @method     ChildApplicationQuery rightJoinTemplate($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Template relation
 * @method     ChildApplicationQuery innerJoinTemplate($relationAlias = null) Adds a INNER JOIN clause to the query using the Template relation
 *
 * @method     ChildApplicationQuery joinWithTemplate($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Template relation
 *
 * @method     ChildApplicationQuery leftJoinWithTemplate() Adds a LEFT JOIN clause and with to the query using the Template relation
 * @method     ChildApplicationQuery rightJoinWithTemplate() Adds a RIGHT JOIN clause and with to the query using the Template relation
 * @method     ChildApplicationQuery innerJoinWithTemplate() Adds a INNER JOIN clause and with to the query using the Template relation
 *
 * @method     ChildApplicationQuery leftJoinPack($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pack relation
 * @method     ChildApplicationQuery rightJoinPack($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pack relation
 * @method     ChildApplicationQuery innerJoinPack($relationAlias = null) Adds a INNER JOIN clause to the query using the Pack relation
 *
 * @method     ChildApplicationQuery joinWithPack($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pack relation
 *
 * @method     ChildApplicationQuery leftJoinWithPack() Adds a LEFT JOIN clause and with to the query using the Pack relation
 * @method     ChildApplicationQuery rightJoinWithPack() Adds a RIGHT JOIN clause and with to the query using the Pack relation
 * @method     ChildApplicationQuery innerJoinWithPack() Adds a INNER JOIN clause and with to the query using the Pack relation
 *
 * @method     ChildApplicationQuery leftJoinUserApplication($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserApplication relation
 * @method     ChildApplicationQuery rightJoinUserApplication($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserApplication relation
 * @method     ChildApplicationQuery innerJoinUserApplication($relationAlias = null) Adds a INNER JOIN clause to the query using the UserApplication relation
 *
 * @method     ChildApplicationQuery joinWithUserApplication($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserApplication relation
 *
 * @method     ChildApplicationQuery leftJoinWithUserApplication() Adds a LEFT JOIN clause and with to the query using the UserApplication relation
 * @method     ChildApplicationQuery rightJoinWithUserApplication() Adds a RIGHT JOIN clause and with to the query using the UserApplication relation
 * @method     ChildApplicationQuery innerJoinWithUserApplication() Adds a INNER JOIN clause and with to the query using the UserApplication relation
 *
 * @method     \Database\HubPlus\CategoryQuery|\Database\HubPlus\TemplateQuery|\Database\HubPlus\PackQuery|\Database\HubPlus\UserApplicationQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApplication findOne(ConnectionInterface $con = null) Return the first ChildApplication matching the query
 * @method     ChildApplication findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApplication matching the query, or a new ChildApplication object populated from the query conditions when no match is found
 *
 * @method     ChildApplication findOneById(int $id) Return the first ChildApplication filtered by the id column
 * @method     ChildApplication findOneByTitle(string $title) Return the first ChildApplication filtered by the title column
 * @method     ChildApplication findOneBySlug(string $slug) Return the first ChildApplication filtered by the slug column
 * @method     ChildApplication findOneByBundle(string $bundle) Return the first ChildApplication filtered by the bundle column
 * @method     ChildApplication findOneByDescription(string $description) Return the first ChildApplication filtered by the description column
 * @method     ChildApplication findOneByAppKey(string $app_key) Return the first ChildApplication filtered by the app_key column
 * @method     ChildApplication findOneByDsn(string $dsn) Return the first ChildApplication filtered by the dsn column
 * @method     ChildApplication findOneByDbHost(string $db_host) Return the first ChildApplication filtered by the db_host column
 * @method     ChildApplication findOneByDbName(string $db_name) Return the first ChildApplication filtered by the db_name column
 * @method     ChildApplication findOneByDbUser(string $db_user) Return the first ChildApplication filtered by the db_user column
 * @method     ChildApplication findOneByDbPwd(string $db_pwd) Return the first ChildApplication filtered by the db_pwd column
 * @method     ChildApplication findOneByApiVersion(string $api_version) Return the first ChildApplication filtered by the api_version column
 * @method     ChildApplication findOneByPackId(int $pack_id) Return the first ChildApplication filtered by the pack_id column
 * @method     ChildApplication findOneByCategoryId(int $category_id) Return the first ChildApplication filtered by the category_id column
 * @method     ChildApplication findOneByTemplateId(int $template_id) Return the first ChildApplication filtered by the template_id column
 * @method     ChildApplication findOneByPushNotification(string $push_notification) Return the first ChildApplication filtered by the push_notification column
 * @method     ChildApplication findOneByMaxNotification(int $max_notification) Return the first ChildApplication filtered by the max_notification column
 * @method     ChildApplication findOneByMaxAdviceHour(int $max_advice_hour) Return the first ChildApplication filtered by the max_advice_hour column
 * @method     ChildApplication findOneByAdviceHourDone(int $advice_hour_done) Return the first ChildApplication filtered by the advice_hour_done column
 * @method     ChildApplication findOneByMaxContentInsert(int $max_content_insert) Return the first ChildApplication filtered by the max_content_insert column
 * @method     ChildApplication findOneByIcon(string $icon) Return the first ChildApplication filtered by the icon column
 * @method     ChildApplication findOneBySplashScreen(string $splash_screen) Return the first ChildApplication filtered by the splash_screen column
 * @method     ChildApplication findOneByBackgroundColor(string $background_color) Return the first ChildApplication filtered by the background_color column
 * @method     ChildApplication findOneByAppLogo(string $app_logo) Return the first ChildApplication filtered by the app_logo column
 * @method     ChildApplication findOneByLayout(string $layout) Return the first ChildApplication filtered by the layout column
 * @method     ChildApplication findOneByWhiteLabel(boolean $white_label) Return the first ChildApplication filtered by the white_label column
 * @method     ChildApplication findOneByDemo(boolean $demo) Return the first ChildApplication filtered by the demo column
 * @method     ChildApplication findOneByFacebookToken(string $facebook_token) Return the first ChildApplication filtered by the facebook_token column
 * @method     ChildApplication findOneByAppleId(string $apple_id) Return the first ChildApplication filtered by the apple_id column
 * @method     ChildApplication findOneByAppleIdPassword(string $apple_id_password) Return the first ChildApplication filtered by the apple_id_password column
 * @method     ChildApplication findOneByAppleStoreAppLink(string $apple_store_app_link) Return the first ChildApplication filtered by the apple_store_app_link column
 * @method     ChildApplication findOneByPlayStoreId(string $play_store_id) Return the first ChildApplication filtered by the play_store_id column
 * @method     ChildApplication findOneByPlayStoreIdPassword(string $play_store_id_password) Return the first ChildApplication filtered by the play_store_id_password column
 * @method     ChildApplication findOneByPlayStoreAppLink(string $play_store_app_link) Return the first ChildApplication filtered by the play_store_app_link column
 * @method     ChildApplication findOneByControllerUri(string $controller_uri) Return the first ChildApplication filtered by the controller_uri column
 * @method     ChildApplication findOneByGoogleAnalyticsUa(string $google_analytics_ua) Return the first ChildApplication filtered by the google_analytics_ua column
 * @method     ChildApplication findOneByPublished(int $published) Return the first ChildApplication filtered by the published column
 * @method     ChildApplication findOneByExpiredAt(string $expired_at) Return the first ChildApplication filtered by the expired_at column
 * @method     ChildApplication findOneByMainContentsLanguage(string $main_contents_language) Return the first ChildApplication filtered by the main_contents_language column
 * @method     ChildApplication findOneByOtherContentsLanguages(string $other_contents_languages) Return the first ChildApplication filtered by the other_contents_languages column
 * @method     ChildApplication findOneByDeletedAt(string $deleted_at) Return the first ChildApplication filtered by the deleted_at column
 * @method     ChildApplication findOneBySystemIcons(string $system_icons) Return the first ChildApplication filtered by the system_icons column
 * @method     ChildApplication findOneByToolbar(string $toolbar) Return the first ChildApplication filtered by the toolbar column
 * @method     ChildApplication findOneByCreatedAt(string $created_at) Return the first ChildApplication filtered by the created_at column
 * @method     ChildApplication findOneByUpdatedAt(string $updated_at) Return the first ChildApplication filtered by the updated_at column *

 * @method     ChildApplication requirePk($key, ConnectionInterface $con = null) Return the ChildApplication by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOne(ConnectionInterface $con = null) Return the first ChildApplication matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApplication requireOneById(int $id) Return the first ChildApplication filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByTitle(string $title) Return the first ChildApplication filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneBySlug(string $slug) Return the first ChildApplication filtered by the slug column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByBundle(string $bundle) Return the first ChildApplication filtered by the bundle column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByDescription(string $description) Return the first ChildApplication filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByAppKey(string $app_key) Return the first ChildApplication filtered by the app_key column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByDsn(string $dsn) Return the first ChildApplication filtered by the dsn column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByDbHost(string $db_host) Return the first ChildApplication filtered by the db_host column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByDbName(string $db_name) Return the first ChildApplication filtered by the db_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByDbUser(string $db_user) Return the first ChildApplication filtered by the db_user column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByDbPwd(string $db_pwd) Return the first ChildApplication filtered by the db_pwd column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByApiVersion(string $api_version) Return the first ChildApplication filtered by the api_version column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByPackId(int $pack_id) Return the first ChildApplication filtered by the pack_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByCategoryId(int $category_id) Return the first ChildApplication filtered by the category_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByTemplateId(int $template_id) Return the first ChildApplication filtered by the template_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByPushNotification(string $push_notification) Return the first ChildApplication filtered by the push_notification column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByMaxNotification(int $max_notification) Return the first ChildApplication filtered by the max_notification column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByMaxAdviceHour(int $max_advice_hour) Return the first ChildApplication filtered by the max_advice_hour column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByAdviceHourDone(int $advice_hour_done) Return the first ChildApplication filtered by the advice_hour_done column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByMaxContentInsert(int $max_content_insert) Return the first ChildApplication filtered by the max_content_insert column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByIcon(string $icon) Return the first ChildApplication filtered by the icon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneBySplashScreen(string $splash_screen) Return the first ChildApplication filtered by the splash_screen column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByBackgroundColor(string $background_color) Return the first ChildApplication filtered by the background_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByAppLogo(string $app_logo) Return the first ChildApplication filtered by the app_logo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByLayout(string $layout) Return the first ChildApplication filtered by the layout column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByWhiteLabel(boolean $white_label) Return the first ChildApplication filtered by the white_label column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByDemo(boolean $demo) Return the first ChildApplication filtered by the demo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByFacebookToken(string $facebook_token) Return the first ChildApplication filtered by the facebook_token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByAppleId(string $apple_id) Return the first ChildApplication filtered by the apple_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByAppleIdPassword(string $apple_id_password) Return the first ChildApplication filtered by the apple_id_password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByAppleStoreAppLink(string $apple_store_app_link) Return the first ChildApplication filtered by the apple_store_app_link column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByPlayStoreId(string $play_store_id) Return the first ChildApplication filtered by the play_store_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByPlayStoreIdPassword(string $play_store_id_password) Return the first ChildApplication filtered by the play_store_id_password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByPlayStoreAppLink(string $play_store_app_link) Return the first ChildApplication filtered by the play_store_app_link column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByControllerUri(string $controller_uri) Return the first ChildApplication filtered by the controller_uri column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByGoogleAnalyticsUa(string $google_analytics_ua) Return the first ChildApplication filtered by the google_analytics_ua column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByPublished(int $published) Return the first ChildApplication filtered by the published column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByExpiredAt(string $expired_at) Return the first ChildApplication filtered by the expired_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByMainContentsLanguage(string $main_contents_language) Return the first ChildApplication filtered by the main_contents_language column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByOtherContentsLanguages(string $other_contents_languages) Return the first ChildApplication filtered by the other_contents_languages column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByDeletedAt(string $deleted_at) Return the first ChildApplication filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneBySystemIcons(string $system_icons) Return the first ChildApplication filtered by the system_icons column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByToolbar(string $toolbar) Return the first ChildApplication filtered by the toolbar column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByCreatedAt(string $created_at) Return the first ChildApplication filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApplication requireOneByUpdatedAt(string $updated_at) Return the first ChildApplication filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApplication[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApplication objects based on current ModelCriteria
 * @method     ChildApplication[]|ObjectCollection findById(int $id) Return ChildApplication objects filtered by the id column
 * @method     ChildApplication[]|ObjectCollection findByTitle(string $title) Return ChildApplication objects filtered by the title column
 * @method     ChildApplication[]|ObjectCollection findBySlug(string $slug) Return ChildApplication objects filtered by the slug column
 * @method     ChildApplication[]|ObjectCollection findByBundle(string $bundle) Return ChildApplication objects filtered by the bundle column
 * @method     ChildApplication[]|ObjectCollection findByDescription(string $description) Return ChildApplication objects filtered by the description column
 * @method     ChildApplication[]|ObjectCollection findByAppKey(string $app_key) Return ChildApplication objects filtered by the app_key column
 * @method     ChildApplication[]|ObjectCollection findByDsn(string $dsn) Return ChildApplication objects filtered by the dsn column
 * @method     ChildApplication[]|ObjectCollection findByDbHost(string $db_host) Return ChildApplication objects filtered by the db_host column
 * @method     ChildApplication[]|ObjectCollection findByDbName(string $db_name) Return ChildApplication objects filtered by the db_name column
 * @method     ChildApplication[]|ObjectCollection findByDbUser(string $db_user) Return ChildApplication objects filtered by the db_user column
 * @method     ChildApplication[]|ObjectCollection findByDbPwd(string $db_pwd) Return ChildApplication objects filtered by the db_pwd column
 * @method     ChildApplication[]|ObjectCollection findByApiVersion(string $api_version) Return ChildApplication objects filtered by the api_version column
 * @method     ChildApplication[]|ObjectCollection findByPackId(int $pack_id) Return ChildApplication objects filtered by the pack_id column
 * @method     ChildApplication[]|ObjectCollection findByCategoryId(int $category_id) Return ChildApplication objects filtered by the category_id column
 * @method     ChildApplication[]|ObjectCollection findByTemplateId(int $template_id) Return ChildApplication objects filtered by the template_id column
 * @method     ChildApplication[]|ObjectCollection findByPushNotification(string $push_notification) Return ChildApplication objects filtered by the push_notification column
 * @method     ChildApplication[]|ObjectCollection findByMaxNotification(int $max_notification) Return ChildApplication objects filtered by the max_notification column
 * @method     ChildApplication[]|ObjectCollection findByMaxAdviceHour(int $max_advice_hour) Return ChildApplication objects filtered by the max_advice_hour column
 * @method     ChildApplication[]|ObjectCollection findByAdviceHourDone(int $advice_hour_done) Return ChildApplication objects filtered by the advice_hour_done column
 * @method     ChildApplication[]|ObjectCollection findByMaxContentInsert(int $max_content_insert) Return ChildApplication objects filtered by the max_content_insert column
 * @method     ChildApplication[]|ObjectCollection findByIcon(string $icon) Return ChildApplication objects filtered by the icon column
 * @method     ChildApplication[]|ObjectCollection findBySplashScreen(string $splash_screen) Return ChildApplication objects filtered by the splash_screen column
 * @method     ChildApplication[]|ObjectCollection findByBackgroundColor(string $background_color) Return ChildApplication objects filtered by the background_color column
 * @method     ChildApplication[]|ObjectCollection findByAppLogo(string $app_logo) Return ChildApplication objects filtered by the app_logo column
 * @method     ChildApplication[]|ObjectCollection findByLayout(string $layout) Return ChildApplication objects filtered by the layout column
 * @method     ChildApplication[]|ObjectCollection findByWhiteLabel(boolean $white_label) Return ChildApplication objects filtered by the white_label column
 * @method     ChildApplication[]|ObjectCollection findByDemo(boolean $demo) Return ChildApplication objects filtered by the demo column
 * @method     ChildApplication[]|ObjectCollection findByFacebookToken(string $facebook_token) Return ChildApplication objects filtered by the facebook_token column
 * @method     ChildApplication[]|ObjectCollection findByAppleId(string $apple_id) Return ChildApplication objects filtered by the apple_id column
 * @method     ChildApplication[]|ObjectCollection findByAppleIdPassword(string $apple_id_password) Return ChildApplication objects filtered by the apple_id_password column
 * @method     ChildApplication[]|ObjectCollection findByAppleStoreAppLink(string $apple_store_app_link) Return ChildApplication objects filtered by the apple_store_app_link column
 * @method     ChildApplication[]|ObjectCollection findByPlayStoreId(string $play_store_id) Return ChildApplication objects filtered by the play_store_id column
 * @method     ChildApplication[]|ObjectCollection findByPlayStoreIdPassword(string $play_store_id_password) Return ChildApplication objects filtered by the play_store_id_password column
 * @method     ChildApplication[]|ObjectCollection findByPlayStoreAppLink(string $play_store_app_link) Return ChildApplication objects filtered by the play_store_app_link column
 * @method     ChildApplication[]|ObjectCollection findByControllerUri(string $controller_uri) Return ChildApplication objects filtered by the controller_uri column
 * @method     ChildApplication[]|ObjectCollection findByGoogleAnalyticsUa(string $google_analytics_ua) Return ChildApplication objects filtered by the google_analytics_ua column
 * @method     ChildApplication[]|ObjectCollection findByPublished(int $published) Return ChildApplication objects filtered by the published column
 * @method     ChildApplication[]|ObjectCollection findByExpiredAt(string $expired_at) Return ChildApplication objects filtered by the expired_at column
 * @method     ChildApplication[]|ObjectCollection findByMainContentsLanguage(string $main_contents_language) Return ChildApplication objects filtered by the main_contents_language column
 * @method     ChildApplication[]|ObjectCollection findByOtherContentsLanguages(string $other_contents_languages) Return ChildApplication objects filtered by the other_contents_languages column
 * @method     ChildApplication[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildApplication objects filtered by the deleted_at column
 * @method     ChildApplication[]|ObjectCollection findBySystemIcons(string $system_icons) Return ChildApplication objects filtered by the system_icons column
 * @method     ChildApplication[]|ObjectCollection findByToolbar(string $toolbar) Return ChildApplication objects filtered by the toolbar column
 * @method     ChildApplication[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildApplication objects filtered by the created_at column
 * @method     ChildApplication[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildApplication objects filtered by the updated_at column
 * @method     ChildApplication[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ApplicationQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\ApplicationQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\Application', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildApplicationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildApplicationQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildApplicationQuery) {
            return $criteria;
        }
        $query = new ChildApplicationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApplication|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ApplicationTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ApplicationTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApplication A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, title, slug, bundle, description, app_key, dsn, db_host, db_name, db_user, db_pwd, api_version, pack_id, category_id, template_id, push_notification, max_notification, max_advice_hour, advice_hour_done, max_content_insert, icon, splash_screen, background_color, app_logo, layout, white_label, demo, facebook_token, apple_id, apple_id_password, apple_store_app_link, play_store_id, play_store_id_password, play_store_app_link, controller_uri, google_analytics_ua, published, expired_at, main_contents_language, other_contents_languages, deleted_at, system_icons, toolbar, created_at, updated_at FROM application WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApplication $obj */
            $obj = new ChildApplication();
            $obj->hydrate($row);
            ApplicationTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApplication|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApplicationTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApplicationTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the slug column
     *
     * Example usage:
     * <code>
     * $query->filterBySlug('fooValue');   // WHERE slug = 'fooValue'
     * $query->filterBySlug('%fooValue%', Criteria::LIKE); // WHERE slug LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slug The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterBySlug($slug = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slug)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_SLUG, $slug, $comparison);
    }

    /**
     * Filter the query on the bundle column
     *
     * Example usage:
     * <code>
     * $query->filterByBundle('fooValue');   // WHERE bundle = 'fooValue'
     * $query->filterByBundle('%fooValue%', Criteria::LIKE); // WHERE bundle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bundle The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByBundle($bundle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bundle)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_BUNDLE, $bundle, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the app_key column
     *
     * Example usage:
     * <code>
     * $query->filterByAppKey('fooValue');   // WHERE app_key = 'fooValue'
     * $query->filterByAppKey('%fooValue%', Criteria::LIKE); // WHERE app_key LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appKey The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByAppKey($appKey = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appKey)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_APP_KEY, $appKey, $comparison);
    }

    /**
     * Filter the query on the dsn column
     *
     * Example usage:
     * <code>
     * $query->filterByDsn('fooValue');   // WHERE dsn = 'fooValue'
     * $query->filterByDsn('%fooValue%', Criteria::LIKE); // WHERE dsn LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dsn The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByDsn($dsn = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dsn)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_DSN, $dsn, $comparison);
    }

    /**
     * Filter the query on the db_host column
     *
     * Example usage:
     * <code>
     * $query->filterByDbHost('fooValue');   // WHERE db_host = 'fooValue'
     * $query->filterByDbHost('%fooValue%', Criteria::LIKE); // WHERE db_host LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dbHost The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByDbHost($dbHost = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dbHost)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_DB_HOST, $dbHost, $comparison);
    }

    /**
     * Filter the query on the db_name column
     *
     * Example usage:
     * <code>
     * $query->filterByDbName('fooValue');   // WHERE db_name = 'fooValue'
     * $query->filterByDbName('%fooValue%', Criteria::LIKE); // WHERE db_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dbName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByDbName($dbName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dbName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_DB_NAME, $dbName, $comparison);
    }

    /**
     * Filter the query on the db_user column
     *
     * Example usage:
     * <code>
     * $query->filterByDbUser('fooValue');   // WHERE db_user = 'fooValue'
     * $query->filterByDbUser('%fooValue%', Criteria::LIKE); // WHERE db_user LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dbUser The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByDbUser($dbUser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dbUser)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_DB_USER, $dbUser, $comparison);
    }

    /**
     * Filter the query on the db_pwd column
     *
     * Example usage:
     * <code>
     * $query->filterByDbPwd('fooValue');   // WHERE db_pwd = 'fooValue'
     * $query->filterByDbPwd('%fooValue%', Criteria::LIKE); // WHERE db_pwd LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dbPwd The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByDbPwd($dbPwd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dbPwd)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_DB_PWD, $dbPwd, $comparison);
    }

    /**
     * Filter the query on the api_version column
     *
     * Example usage:
     * <code>
     * $query->filterByApiVersion('fooValue');   // WHERE api_version = 'fooValue'
     * $query->filterByApiVersion('%fooValue%', Criteria::LIKE); // WHERE api_version LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiVersion The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByApiVersion($apiVersion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiVersion)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_API_VERSION, $apiVersion, $comparison);
    }

    /**
     * Filter the query on the pack_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPackId(1234); // WHERE pack_id = 1234
     * $query->filterByPackId(array(12, 34)); // WHERE pack_id IN (12, 34)
     * $query->filterByPackId(array('min' => 12)); // WHERE pack_id > 12
     * </code>
     *
     * @see       filterByPack()
     *
     * @param     mixed $packId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByPackId($packId = null, $comparison = null)
    {
        if (is_array($packId)) {
            $useMinMax = false;
            if (isset($packId['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_PACK_ID, $packId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($packId['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_PACK_ID, $packId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_PACK_ID, $packId, $comparison);
    }

    /**
     * Filter the query on the category_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryId(1234); // WHERE category_id = 1234
     * $query->filterByCategoryId(array(12, 34)); // WHERE category_id IN (12, 34)
     * $query->filterByCategoryId(array('min' => 12)); // WHERE category_id > 12
     * </code>
     *
     * @see       filterByCategory()
     *
     * @param     mixed $categoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByCategoryId($categoryId = null, $comparison = null)
    {
        if (is_array($categoryId)) {
            $useMinMax = false;
            if (isset($categoryId['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_CATEGORY_ID, $categoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categoryId['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_CATEGORY_ID, $categoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_CATEGORY_ID, $categoryId, $comparison);
    }

    /**
     * Filter the query on the template_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTemplateId(1234); // WHERE template_id = 1234
     * $query->filterByTemplateId(array(12, 34)); // WHERE template_id IN (12, 34)
     * $query->filterByTemplateId(array('min' => 12)); // WHERE template_id > 12
     * </code>
     *
     * @see       filterByTemplate()
     *
     * @param     mixed $templateId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByTemplateId($templateId = null, $comparison = null)
    {
        if (is_array($templateId)) {
            $useMinMax = false;
            if (isset($templateId['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_TEMPLATE_ID, $templateId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($templateId['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_TEMPLATE_ID, $templateId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_TEMPLATE_ID, $templateId, $comparison);
    }

    /**
     * Filter the query on the push_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByPushNotification('fooValue');   // WHERE push_notification = 'fooValue'
     * $query->filterByPushNotification('%fooValue%', Criteria::LIKE); // WHERE push_notification LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pushNotification The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByPushNotification($pushNotification = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pushNotification)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_PUSH_NOTIFICATION, $pushNotification, $comparison);
    }

    /**
     * Filter the query on the max_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxNotification(1234); // WHERE max_notification = 1234
     * $query->filterByMaxNotification(array(12, 34)); // WHERE max_notification IN (12, 34)
     * $query->filterByMaxNotification(array('min' => 12)); // WHERE max_notification > 12
     * </code>
     *
     * @param     mixed $maxNotification The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByMaxNotification($maxNotification = null, $comparison = null)
    {
        if (is_array($maxNotification)) {
            $useMinMax = false;
            if (isset($maxNotification['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_MAX_NOTIFICATION, $maxNotification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxNotification['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_MAX_NOTIFICATION, $maxNotification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_MAX_NOTIFICATION, $maxNotification, $comparison);
    }

    /**
     * Filter the query on the max_advice_hour column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxAdviceHour(1234); // WHERE max_advice_hour = 1234
     * $query->filterByMaxAdviceHour(array(12, 34)); // WHERE max_advice_hour IN (12, 34)
     * $query->filterByMaxAdviceHour(array('min' => 12)); // WHERE max_advice_hour > 12
     * </code>
     *
     * @param     mixed $maxAdviceHour The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByMaxAdviceHour($maxAdviceHour = null, $comparison = null)
    {
        if (is_array($maxAdviceHour)) {
            $useMinMax = false;
            if (isset($maxAdviceHour['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_MAX_ADVICE_HOUR, $maxAdviceHour['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxAdviceHour['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_MAX_ADVICE_HOUR, $maxAdviceHour['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_MAX_ADVICE_HOUR, $maxAdviceHour, $comparison);
    }

    /**
     * Filter the query on the advice_hour_done column
     *
     * Example usage:
     * <code>
     * $query->filterByAdviceHourDone(1234); // WHERE advice_hour_done = 1234
     * $query->filterByAdviceHourDone(array(12, 34)); // WHERE advice_hour_done IN (12, 34)
     * $query->filterByAdviceHourDone(array('min' => 12)); // WHERE advice_hour_done > 12
     * </code>
     *
     * @param     mixed $adviceHourDone The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByAdviceHourDone($adviceHourDone = null, $comparison = null)
    {
        if (is_array($adviceHourDone)) {
            $useMinMax = false;
            if (isset($adviceHourDone['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_ADVICE_HOUR_DONE, $adviceHourDone['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($adviceHourDone['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_ADVICE_HOUR_DONE, $adviceHourDone['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_ADVICE_HOUR_DONE, $adviceHourDone, $comparison);
    }

    /**
     * Filter the query on the max_content_insert column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxContentInsert(1234); // WHERE max_content_insert = 1234
     * $query->filterByMaxContentInsert(array(12, 34)); // WHERE max_content_insert IN (12, 34)
     * $query->filterByMaxContentInsert(array('min' => 12)); // WHERE max_content_insert > 12
     * </code>
     *
     * @param     mixed $maxContentInsert The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByMaxContentInsert($maxContentInsert = null, $comparison = null)
    {
        if (is_array($maxContentInsert)) {
            $useMinMax = false;
            if (isset($maxContentInsert['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_MAX_CONTENT_INSERT, $maxContentInsert['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxContentInsert['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_MAX_CONTENT_INSERT, $maxContentInsert['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_MAX_CONTENT_INSERT, $maxContentInsert, $comparison);
    }

    /**
     * Filter the query on the icon column
     *
     * Example usage:
     * <code>
     * $query->filterByIcon('fooValue');   // WHERE icon = 'fooValue'
     * $query->filterByIcon('%fooValue%', Criteria::LIKE); // WHERE icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $icon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByIcon($icon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($icon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_ICON, $icon, $comparison);
    }

    /**
     * Filter the query on the splash_screen column
     *
     * Example usage:
     * <code>
     * $query->filterBySplashScreen('fooValue');   // WHERE splash_screen = 'fooValue'
     * $query->filterBySplashScreen('%fooValue%', Criteria::LIKE); // WHERE splash_screen LIKE '%fooValue%'
     * </code>
     *
     * @param     string $splashScreen The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterBySplashScreen($splashScreen = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($splashScreen)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_SPLASH_SCREEN, $splashScreen, $comparison);
    }

    /**
     * Filter the query on the background_color column
     *
     * Example usage:
     * <code>
     * $query->filterByBackgroundColor('fooValue');   // WHERE background_color = 'fooValue'
     * $query->filterByBackgroundColor('%fooValue%', Criteria::LIKE); // WHERE background_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $backgroundColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByBackgroundColor($backgroundColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($backgroundColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_BACKGROUND_COLOR, $backgroundColor, $comparison);
    }

    /**
     * Filter the query on the app_logo column
     *
     * Example usage:
     * <code>
     * $query->filterByAppLogo('fooValue');   // WHERE app_logo = 'fooValue'
     * $query->filterByAppLogo('%fooValue%', Criteria::LIKE); // WHERE app_logo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appLogo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByAppLogo($appLogo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appLogo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_APP_LOGO, $appLogo, $comparison);
    }

    /**
     * Filter the query on the layout column
     *
     * Example usage:
     * <code>
     * $query->filterByLayout('fooValue');   // WHERE layout = 'fooValue'
     * $query->filterByLayout('%fooValue%', Criteria::LIKE); // WHERE layout LIKE '%fooValue%'
     * </code>
     *
     * @param     string $layout The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByLayout($layout = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($layout)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_LAYOUT, $layout, $comparison);
    }

    /**
     * Filter the query on the white_label column
     *
     * Example usage:
     * <code>
     * $query->filterByWhiteLabel(true); // WHERE white_label = true
     * $query->filterByWhiteLabel('yes'); // WHERE white_label = true
     * </code>
     *
     * @param     boolean|string $whiteLabel The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByWhiteLabel($whiteLabel = null, $comparison = null)
    {
        if (is_string($whiteLabel)) {
            $whiteLabel = in_array(strtolower($whiteLabel), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_WHITE_LABEL, $whiteLabel, $comparison);
    }

    /**
     * Filter the query on the demo column
     *
     * Example usage:
     * <code>
     * $query->filterByDemo(true); // WHERE demo = true
     * $query->filterByDemo('yes'); // WHERE demo = true
     * </code>
     *
     * @param     boolean|string $demo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByDemo($demo = null, $comparison = null)
    {
        if (is_string($demo)) {
            $demo = in_array(strtolower($demo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_DEMO, $demo, $comparison);
    }

    /**
     * Filter the query on the facebook_token column
     *
     * Example usage:
     * <code>
     * $query->filterByFacebookToken('fooValue');   // WHERE facebook_token = 'fooValue'
     * $query->filterByFacebookToken('%fooValue%', Criteria::LIKE); // WHERE facebook_token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $facebookToken The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByFacebookToken($facebookToken = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($facebookToken)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_FACEBOOK_TOKEN, $facebookToken, $comparison);
    }

    /**
     * Filter the query on the apple_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAppleId('fooValue');   // WHERE apple_id = 'fooValue'
     * $query->filterByAppleId('%fooValue%', Criteria::LIKE); // WHERE apple_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appleId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByAppleId($appleId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appleId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_APPLE_ID, $appleId, $comparison);
    }

    /**
     * Filter the query on the apple_id_password column
     *
     * Example usage:
     * <code>
     * $query->filterByAppleIdPassword('fooValue');   // WHERE apple_id_password = 'fooValue'
     * $query->filterByAppleIdPassword('%fooValue%', Criteria::LIKE); // WHERE apple_id_password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appleIdPassword The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByAppleIdPassword($appleIdPassword = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appleIdPassword)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_APPLE_ID_PASSWORD, $appleIdPassword, $comparison);
    }

    /**
     * Filter the query on the apple_store_app_link column
     *
     * Example usage:
     * <code>
     * $query->filterByAppleStoreAppLink('fooValue');   // WHERE apple_store_app_link = 'fooValue'
     * $query->filterByAppleStoreAppLink('%fooValue%', Criteria::LIKE); // WHERE apple_store_app_link LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appleStoreAppLink The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByAppleStoreAppLink($appleStoreAppLink = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appleStoreAppLink)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_APPLE_STORE_APP_LINK, $appleStoreAppLink, $comparison);
    }

    /**
     * Filter the query on the play_store_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayStoreId('fooValue');   // WHERE play_store_id = 'fooValue'
     * $query->filterByPlayStoreId('%fooValue%', Criteria::LIKE); // WHERE play_store_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $playStoreId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByPlayStoreId($playStoreId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($playStoreId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_PLAY_STORE_ID, $playStoreId, $comparison);
    }

    /**
     * Filter the query on the play_store_id_password column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayStoreIdPassword('fooValue');   // WHERE play_store_id_password = 'fooValue'
     * $query->filterByPlayStoreIdPassword('%fooValue%', Criteria::LIKE); // WHERE play_store_id_password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $playStoreIdPassword The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByPlayStoreIdPassword($playStoreIdPassword = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($playStoreIdPassword)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_PLAY_STORE_ID_PASSWORD, $playStoreIdPassword, $comparison);
    }

    /**
     * Filter the query on the play_store_app_link column
     *
     * Example usage:
     * <code>
     * $query->filterByPlayStoreAppLink('fooValue');   // WHERE play_store_app_link = 'fooValue'
     * $query->filterByPlayStoreAppLink('%fooValue%', Criteria::LIKE); // WHERE play_store_app_link LIKE '%fooValue%'
     * </code>
     *
     * @param     string $playStoreAppLink The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByPlayStoreAppLink($playStoreAppLink = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($playStoreAppLink)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_PLAY_STORE_APP_LINK, $playStoreAppLink, $comparison);
    }

    /**
     * Filter the query on the controller_uri column
     *
     * Example usage:
     * <code>
     * $query->filterByControllerUri('fooValue');   // WHERE controller_uri = 'fooValue'
     * $query->filterByControllerUri('%fooValue%', Criteria::LIKE); // WHERE controller_uri LIKE '%fooValue%'
     * </code>
     *
     * @param     string $controllerUri The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByControllerUri($controllerUri = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($controllerUri)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_CONTROLLER_URI, $controllerUri, $comparison);
    }

    /**
     * Filter the query on the google_analytics_ua column
     *
     * Example usage:
     * <code>
     * $query->filterByGoogleAnalyticsUa('fooValue');   // WHERE google_analytics_ua = 'fooValue'
     * $query->filterByGoogleAnalyticsUa('%fooValue%', Criteria::LIKE); // WHERE google_analytics_ua LIKE '%fooValue%'
     * </code>
     *
     * @param     string $googleAnalyticsUa The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByGoogleAnalyticsUa($googleAnalyticsUa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($googleAnalyticsUa)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_GOOGLE_ANALYTICS_UA, $googleAnalyticsUa, $comparison);
    }

    /**
     * Filter the query on the published column
     *
     * Example usage:
     * <code>
     * $query->filterByPublished(1234); // WHERE published = 1234
     * $query->filterByPublished(array(12, 34)); // WHERE published IN (12, 34)
     * $query->filterByPublished(array('min' => 12)); // WHERE published > 12
     * </code>
     *
     * @param     mixed $published The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByPublished($published = null, $comparison = null)
    {
        if (is_array($published)) {
            $useMinMax = false;
            if (isset($published['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_PUBLISHED, $published['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($published['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_PUBLISHED, $published['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_PUBLISHED, $published, $comparison);
    }

    /**
     * Filter the query on the expired_at column
     *
     * Example usage:
     * <code>
     * $query->filterByExpiredAt('2011-03-14'); // WHERE expired_at = '2011-03-14'
     * $query->filterByExpiredAt('now'); // WHERE expired_at = '2011-03-14'
     * $query->filterByExpiredAt(array('max' => 'yesterday')); // WHERE expired_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $expiredAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByExpiredAt($expiredAt = null, $comparison = null)
    {
        if (is_array($expiredAt)) {
            $useMinMax = false;
            if (isset($expiredAt['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_EXPIRED_AT, $expiredAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($expiredAt['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_EXPIRED_AT, $expiredAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_EXPIRED_AT, $expiredAt, $comparison);
    }

    /**
     * Filter the query on the main_contents_language column
     *
     * Example usage:
     * <code>
     * $query->filterByMainContentsLanguage('fooValue');   // WHERE main_contents_language = 'fooValue'
     * $query->filterByMainContentsLanguage('%fooValue%', Criteria::LIKE); // WHERE main_contents_language LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mainContentsLanguage The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByMainContentsLanguage($mainContentsLanguage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mainContentsLanguage)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_MAIN_CONTENTS_LANGUAGE, $mainContentsLanguage, $comparison);
    }

    /**
     * Filter the query on the other_contents_languages column
     *
     * Example usage:
     * <code>
     * $query->filterByOtherContentsLanguages('fooValue');   // WHERE other_contents_languages = 'fooValue'
     * $query->filterByOtherContentsLanguages('%fooValue%', Criteria::LIKE); // WHERE other_contents_languages LIKE '%fooValue%'
     * </code>
     *
     * @param     string $otherContentsLanguages The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByOtherContentsLanguages($otherContentsLanguages = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($otherContentsLanguages)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_OTHER_CONTENTS_LANGUAGES, $otherContentsLanguages, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the system_icons column
     *
     * Example usage:
     * <code>
     * $query->filterBySystemIcons('fooValue');   // WHERE system_icons = 'fooValue'
     * $query->filterBySystemIcons('%fooValue%', Criteria::LIKE); // WHERE system_icons LIKE '%fooValue%'
     * </code>
     *
     * @param     string $systemIcons The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterBySystemIcons($systemIcons = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($systemIcons)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_SYSTEM_ICONS, $systemIcons, $comparison);
    }

    /**
     * Filter the query on the toolbar column
     *
     * Example usage:
     * <code>
     * $query->filterByToolbar('fooValue');   // WHERE toolbar = 'fooValue'
     * $query->filterByToolbar('%fooValue%', Criteria::LIKE); // WHERE toolbar LIKE '%fooValue%'
     * </code>
     *
     * @param     string $toolbar The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByToolbar($toolbar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($toolbar)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_TOOLBAR, $toolbar, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ApplicationTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApplicationTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Category object
     *
     * @param \Database\HubPlus\Category|ObjectCollection $category The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByCategory($category, $comparison = null)
    {
        if ($category instanceof \Database\HubPlus\Category) {
            return $this
                ->addUsingAlias(ApplicationTableMap::COL_CATEGORY_ID, $category->getId(), $comparison);
        } elseif ($category instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApplicationTableMap::COL_CATEGORY_ID, $category->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCategory() only accepts arguments of type \Database\HubPlus\Category or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Category relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function joinCategory($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Category');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Category');
        }

        return $this;
    }

    /**
     * Use the Category relation Category object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\CategoryQuery A secondary query class using the current class as primary query
     */
    public function useCategoryQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Category', '\Database\HubPlus\CategoryQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Template object
     *
     * @param \Database\HubPlus\Template|ObjectCollection $template The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByTemplate($template, $comparison = null)
    {
        if ($template instanceof \Database\HubPlus\Template) {
            return $this
                ->addUsingAlias(ApplicationTableMap::COL_TEMPLATE_ID, $template->getId(), $comparison);
        } elseif ($template instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApplicationTableMap::COL_TEMPLATE_ID, $template->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTemplate() only accepts arguments of type \Database\HubPlus\Template or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Template relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function joinTemplate($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Template');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Template');
        }

        return $this;
    }

    /**
     * Use the Template relation Template object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\TemplateQuery A secondary query class using the current class as primary query
     */
    public function useTemplateQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTemplate($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Template', '\Database\HubPlus\TemplateQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Pack object
     *
     * @param \Database\HubPlus\Pack|ObjectCollection $pack The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByPack($pack, $comparison = null)
    {
        if ($pack instanceof \Database\HubPlus\Pack) {
            return $this
                ->addUsingAlias(ApplicationTableMap::COL_PACK_ID, $pack->getId(), $comparison);
        } elseif ($pack instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ApplicationTableMap::COL_PACK_ID, $pack->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPack() only accepts arguments of type \Database\HubPlus\Pack or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pack relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function joinPack($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pack');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pack');
        }

        return $this;
    }

    /**
     * Use the Pack relation Pack object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PackQuery A secondary query class using the current class as primary query
     */
    public function usePackQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPack($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pack', '\Database\HubPlus\PackQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\UserApplication object
     *
     * @param \Database\HubPlus\UserApplication|ObjectCollection $userApplication the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildApplicationQuery The current query, for fluid interface
     */
    public function filterByUserApplication($userApplication, $comparison = null)
    {
        if ($userApplication instanceof \Database\HubPlus\UserApplication) {
            return $this
                ->addUsingAlias(ApplicationTableMap::COL_ID, $userApplication->getAppId(), $comparison);
        } elseif ($userApplication instanceof ObjectCollection) {
            return $this
                ->useUserApplicationQuery()
                ->filterByPrimaryKeys($userApplication->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserApplication() only accepts arguments of type \Database\HubPlus\UserApplication or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserApplication relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function joinUserApplication($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserApplication');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserApplication');
        }

        return $this;
    }

    /**
     * Use the UserApplication relation UserApplication object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\UserApplicationQuery A secondary query class using the current class as primary query
     */
    public function useUserApplicationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserApplication($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserApplication', '\Database\HubPlus\UserApplicationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApplication $application Object to remove from the list of results
     *
     * @return $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function prune($application = null)
    {
        if ($application) {
            $this->addUsingAlias(ApplicationTableMap::COL_ID, $application->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the application table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ApplicationTableMap::clearInstancePool();
            ApplicationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ApplicationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ApplicationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ApplicationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ApplicationTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ApplicationTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ApplicationTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ApplicationTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ApplicationTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildApplicationQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ApplicationTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildApplicationArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicationTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // ApplicationQuery
