<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\SectionRelatedTo as ChildSectionRelatedTo;
use Database\HubPlus\SectionRelatedToQuery as ChildSectionRelatedToQuery;
use Database\HubPlus\Map\SectionRelatedToTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'section_related_to' table.
 *
 *
 *
 * @method     ChildSectionRelatedToQuery orderByFromSectionId($order = Criteria::ASC) Order by the from_section_id column
 * @method     ChildSectionRelatedToQuery orderByToSectionId($order = Criteria::ASC) Order by the to_section_id column
 *
 * @method     ChildSectionRelatedToQuery groupByFromSectionId() Group by the from_section_id column
 * @method     ChildSectionRelatedToQuery groupByToSectionId() Group by the to_section_id column
 *
 * @method     ChildSectionRelatedToQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSectionRelatedToQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSectionRelatedToQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSectionRelatedToQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSectionRelatedToQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSectionRelatedToQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSectionRelatedToQuery leftJoinSectionRelatedByFromSectionId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionRelatedByFromSectionId relation
 * @method     ChildSectionRelatedToQuery rightJoinSectionRelatedByFromSectionId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionRelatedByFromSectionId relation
 * @method     ChildSectionRelatedToQuery innerJoinSectionRelatedByFromSectionId($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionRelatedByFromSectionId relation
 *
 * @method     ChildSectionRelatedToQuery joinWithSectionRelatedByFromSectionId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionRelatedByFromSectionId relation
 *
 * @method     ChildSectionRelatedToQuery leftJoinWithSectionRelatedByFromSectionId() Adds a LEFT JOIN clause and with to the query using the SectionRelatedByFromSectionId relation
 * @method     ChildSectionRelatedToQuery rightJoinWithSectionRelatedByFromSectionId() Adds a RIGHT JOIN clause and with to the query using the SectionRelatedByFromSectionId relation
 * @method     ChildSectionRelatedToQuery innerJoinWithSectionRelatedByFromSectionId() Adds a INNER JOIN clause and with to the query using the SectionRelatedByFromSectionId relation
 *
 * @method     ChildSectionRelatedToQuery leftJoinSectionRelatedByToSectionId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionRelatedByToSectionId relation
 * @method     ChildSectionRelatedToQuery rightJoinSectionRelatedByToSectionId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionRelatedByToSectionId relation
 * @method     ChildSectionRelatedToQuery innerJoinSectionRelatedByToSectionId($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionRelatedByToSectionId relation
 *
 * @method     ChildSectionRelatedToQuery joinWithSectionRelatedByToSectionId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionRelatedByToSectionId relation
 *
 * @method     ChildSectionRelatedToQuery leftJoinWithSectionRelatedByToSectionId() Adds a LEFT JOIN clause and with to the query using the SectionRelatedByToSectionId relation
 * @method     ChildSectionRelatedToQuery rightJoinWithSectionRelatedByToSectionId() Adds a RIGHT JOIN clause and with to the query using the SectionRelatedByToSectionId relation
 * @method     ChildSectionRelatedToQuery innerJoinWithSectionRelatedByToSectionId() Adds a INNER JOIN clause and with to the query using the SectionRelatedByToSectionId relation
 *
 * @method     \Database\HubPlus\SectionQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSectionRelatedTo findOne(ConnectionInterface $con = null) Return the first ChildSectionRelatedTo matching the query
 * @method     ChildSectionRelatedTo findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSectionRelatedTo matching the query, or a new ChildSectionRelatedTo object populated from the query conditions when no match is found
 *
 * @method     ChildSectionRelatedTo findOneByFromSectionId(int $from_section_id) Return the first ChildSectionRelatedTo filtered by the from_section_id column
 * @method     ChildSectionRelatedTo findOneByToSectionId(int $to_section_id) Return the first ChildSectionRelatedTo filtered by the to_section_id column *

 * @method     ChildSectionRelatedTo requirePk($key, ConnectionInterface $con = null) Return the ChildSectionRelatedTo by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionRelatedTo requireOne(ConnectionInterface $con = null) Return the first ChildSectionRelatedTo matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSectionRelatedTo requireOneByFromSectionId(int $from_section_id) Return the first ChildSectionRelatedTo filtered by the from_section_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionRelatedTo requireOneByToSectionId(int $to_section_id) Return the first ChildSectionRelatedTo filtered by the to_section_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSectionRelatedTo[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSectionRelatedTo objects based on current ModelCriteria
 * @method     ChildSectionRelatedTo[]|ObjectCollection findByFromSectionId(int $from_section_id) Return ChildSectionRelatedTo objects filtered by the from_section_id column
 * @method     ChildSectionRelatedTo[]|ObjectCollection findByToSectionId(int $to_section_id) Return ChildSectionRelatedTo objects filtered by the to_section_id column
 * @method     ChildSectionRelatedTo[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SectionRelatedToQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\SectionRelatedToQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\SectionRelatedTo', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSectionRelatedToQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSectionRelatedToQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSectionRelatedToQuery) {
            return $criteria;
        }
        $query = new ChildSectionRelatedToQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$from_section_id, $to_section_id] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSectionRelatedTo|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SectionRelatedToTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SectionRelatedToTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSectionRelatedTo A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT from_section_id, to_section_id FROM section_related_to WHERE from_section_id = :p0 AND to_section_id = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSectionRelatedTo $obj */
            $obj = new ChildSectionRelatedTo();
            $obj->hydrate($row);
            SectionRelatedToTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSectionRelatedTo|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSectionRelatedToQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(SectionRelatedToTableMap::COL_FROM_SECTION_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(SectionRelatedToTableMap::COL_TO_SECTION_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSectionRelatedToQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(SectionRelatedToTableMap::COL_FROM_SECTION_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(SectionRelatedToTableMap::COL_TO_SECTION_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the from_section_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFromSectionId(1234); // WHERE from_section_id = 1234
     * $query->filterByFromSectionId(array(12, 34)); // WHERE from_section_id IN (12, 34)
     * $query->filterByFromSectionId(array('min' => 12)); // WHERE from_section_id > 12
     * </code>
     *
     * @see       filterBySectionRelatedByFromSectionId()
     *
     * @param     mixed $fromSectionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionRelatedToQuery The current query, for fluid interface
     */
    public function filterByFromSectionId($fromSectionId = null, $comparison = null)
    {
        if (is_array($fromSectionId)) {
            $useMinMax = false;
            if (isset($fromSectionId['min'])) {
                $this->addUsingAlias(SectionRelatedToTableMap::COL_FROM_SECTION_ID, $fromSectionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fromSectionId['max'])) {
                $this->addUsingAlias(SectionRelatedToTableMap::COL_FROM_SECTION_ID, $fromSectionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionRelatedToTableMap::COL_FROM_SECTION_ID, $fromSectionId, $comparison);
    }

    /**
     * Filter the query on the to_section_id column
     *
     * Example usage:
     * <code>
     * $query->filterByToSectionId(1234); // WHERE to_section_id = 1234
     * $query->filterByToSectionId(array(12, 34)); // WHERE to_section_id IN (12, 34)
     * $query->filterByToSectionId(array('min' => 12)); // WHERE to_section_id > 12
     * </code>
     *
     * @see       filterBySectionRelatedByToSectionId()
     *
     * @param     mixed $toSectionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionRelatedToQuery The current query, for fluid interface
     */
    public function filterByToSectionId($toSectionId = null, $comparison = null)
    {
        if (is_array($toSectionId)) {
            $useMinMax = false;
            if (isset($toSectionId['min'])) {
                $this->addUsingAlias(SectionRelatedToTableMap::COL_TO_SECTION_ID, $toSectionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($toSectionId['max'])) {
                $this->addUsingAlias(SectionRelatedToTableMap::COL_TO_SECTION_ID, $toSectionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionRelatedToTableMap::COL_TO_SECTION_ID, $toSectionId, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Section object
     *
     * @param \Database\HubPlus\Section|ObjectCollection $section The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSectionRelatedToQuery The current query, for fluid interface
     */
    public function filterBySectionRelatedByFromSectionId($section, $comparison = null)
    {
        if ($section instanceof \Database\HubPlus\Section) {
            return $this
                ->addUsingAlias(SectionRelatedToTableMap::COL_FROM_SECTION_ID, $section->getId(), $comparison);
        } elseif ($section instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SectionRelatedToTableMap::COL_FROM_SECTION_ID, $section->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySectionRelatedByFromSectionId() only accepts arguments of type \Database\HubPlus\Section or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionRelatedByFromSectionId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionRelatedToQuery The current query, for fluid interface
     */
    public function joinSectionRelatedByFromSectionId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionRelatedByFromSectionId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionRelatedByFromSectionId');
        }

        return $this;
    }

    /**
     * Use the SectionRelatedByFromSectionId relation Section object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionQuery A secondary query class using the current class as primary query
     */
    public function useSectionRelatedByFromSectionIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSectionRelatedByFromSectionId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionRelatedByFromSectionId', '\Database\HubPlus\SectionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Section object
     *
     * @param \Database\HubPlus\Section|ObjectCollection $section The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSectionRelatedToQuery The current query, for fluid interface
     */
    public function filterBySectionRelatedByToSectionId($section, $comparison = null)
    {
        if ($section instanceof \Database\HubPlus\Section) {
            return $this
                ->addUsingAlias(SectionRelatedToTableMap::COL_TO_SECTION_ID, $section->getId(), $comparison);
        } elseif ($section instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SectionRelatedToTableMap::COL_TO_SECTION_ID, $section->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySectionRelatedByToSectionId() only accepts arguments of type \Database\HubPlus\Section or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionRelatedByToSectionId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionRelatedToQuery The current query, for fluid interface
     */
    public function joinSectionRelatedByToSectionId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionRelatedByToSectionId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionRelatedByToSectionId');
        }

        return $this;
    }

    /**
     * Use the SectionRelatedByToSectionId relation Section object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionQuery A secondary query class using the current class as primary query
     */
    public function useSectionRelatedByToSectionIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSectionRelatedByToSectionId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionRelatedByToSectionId', '\Database\HubPlus\SectionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSectionRelatedTo $sectionRelatedTo Object to remove from the list of results
     *
     * @return $this|ChildSectionRelatedToQuery The current query, for fluid interface
     */
    public function prune($sectionRelatedTo = null)
    {
        if ($sectionRelatedTo) {
            $this->addCond('pruneCond0', $this->getAliasedColName(SectionRelatedToTableMap::COL_FROM_SECTION_ID), $sectionRelatedTo->getFromSectionId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(SectionRelatedToTableMap::COL_TO_SECTION_ID), $sectionRelatedTo->getToSectionId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the section_related_to table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionRelatedToTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SectionRelatedToTableMap::clearInstancePool();
            SectionRelatedToTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionRelatedToTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SectionRelatedToTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SectionRelatedToTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SectionRelatedToTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SectionRelatedToQuery
