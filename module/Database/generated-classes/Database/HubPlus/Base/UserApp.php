<?php

namespace Database\HubPlus\Base;

use \DateTime;
use \Exception;
use \PDO;
use Database\HubPlus\Address as ChildAddress;
use Database\HubPlus\AddressQuery as ChildAddressQuery;
use Database\HubPlus\CustomFormLog as ChildCustomFormLog;
use Database\HubPlus\CustomFormLogQuery as ChildCustomFormLogQuery;
use Database\HubPlus\Device as ChildDevice;
use Database\HubPlus\DeviceQuery as ChildDeviceQuery;
use Database\HubPlus\MediaLog as ChildMediaLog;
use Database\HubPlus\MediaLogQuery as ChildMediaLogQuery;
use Database\HubPlus\PostLog as ChildPostLog;
use Database\HubPlus\PostLogQuery as ChildPostLogQuery;
use Database\HubPlus\RemoteLog as ChildRemoteLog;
use Database\HubPlus\RemoteLogQuery as ChildRemoteLogQuery;
use Database\HubPlus\Section as ChildSection;
use Database\HubPlus\SectionQuery as ChildSectionQuery;
use Database\HubPlus\UserApp as ChildUserApp;
use Database\HubPlus\UserAppArchive as ChildUserAppArchive;
use Database\HubPlus\UserAppArchiveQuery as ChildUserAppArchiveQuery;
use Database\HubPlus\UserAppQuery as ChildUserAppQuery;
use Database\HubPlus\Map\CustomFormLogTableMap;
use Database\HubPlus\Map\DeviceTableMap;
use Database\HubPlus\Map\MediaLogTableMap;
use Database\HubPlus\Map\PostLogTableMap;
use Database\HubPlus\Map\RemoteLogTableMap;
use Database\HubPlus\Map\UserAppTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'user_app' table.
 *
 *
 *
 * @package    propel.generator.Database.HubPlus.Base
 */
abstract class UserApp implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Database\\HubPlus\\Map\\UserAppTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the username field.
     *
     * @var        string
     */
    protected $username;

    /**
     * The value for the username_canonical field.
     *
     * @var        string
     */
    protected $username_canonical;

    /**
     * The value for the email field.
     *
     * @var        string
     */
    protected $email;

    /**
     * The value for the email_canonical field.
     *
     * @var        string
     */
    protected $email_canonical;

    /**
     * The value for the enabled field.
     *
     * @var        boolean
     */
    protected $enabled;

    /**
     * The value for the privacy field.
     *
     * @var        boolean
     */
    protected $privacy;

    /**
     * The value for the terms field.
     *
     * @var        boolean
     */
    protected $terms;

    /**
     * The value for the salt field.
     *
     * @var        string
     */
    protected $salt;

    /**
     * The value for the password field.
     *
     * @var        string
     */
    protected $password;

    /**
     * The value for the last_login field.
     *
     * @var        DateTime
     */
    protected $last_login;

    /**
     * The value for the locked field.
     *
     * @var        boolean
     */
    protected $locked;

    /**
     * The value for the expired field.
     *
     * @var        boolean
     */
    protected $expired;

    /**
     * The value for the expires_at field.
     *
     * @var        DateTime
     */
    protected $expires_at;

    /**
     * The value for the confirmation_token field.
     *
     * @var        string
     */
    protected $confirmation_token;

    /**
     * The value for the password_requested_at field.
     *
     * @var        DateTime
     */
    protected $password_requested_at;

    /**
     * The value for the roles field.
     *
     * @var        string
     */
    protected $roles;

    /**
     * The value for the credentials_expired field.
     *
     * @var        boolean
     */
    protected $credentials_expired;

    /**
     * The value for the credentials_expire_at field.
     *
     * @var        DateTime
     */
    protected $credentials_expire_at;

    /**
     * The value for the firstname field.
     *
     * @var        string
     */
    protected $firstname;

    /**
     * The value for the lastname field.
     *
     * @var        string
     */
    protected $lastname;

    /**
     * The value for the phone field.
     *
     * @var        string
     */
    protected $phone;

    /**
     * The value for the imagepath field.
     *
     * @var        string
     */
    protected $imagepath;

    /**
     * The value for the address_id field.
     *
     * @var        int
     */
    protected $address_id;

    /**
     * The value for the tags field.
     *
     * @var        string
     */
    protected $tags;

    /**
     * The value for the section_id field.
     *
     * @var        int
     */
    protected $section_id;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildAddress
     */
    protected $aAddress;

    /**
     * @var        ChildSection
     */
    protected $aSection;

    /**
     * @var        ObjectCollection|ChildCustomFormLog[] Collection to store aggregation of ChildCustomFormLog objects.
     */
    protected $collCustomFormLogs;
    protected $collCustomFormLogsPartial;

    /**
     * @var        ObjectCollection|ChildDevice[] Collection to store aggregation of ChildDevice objects.
     */
    protected $collDevices;
    protected $collDevicesPartial;

    /**
     * @var        ObjectCollection|ChildMediaLog[] Collection to store aggregation of ChildMediaLog objects.
     */
    protected $collMediaLogs;
    protected $collMediaLogsPartial;

    /**
     * @var        ObjectCollection|ChildPostLog[] Collection to store aggregation of ChildPostLog objects.
     */
    protected $collPostLogs;
    protected $collPostLogsPartial;

    /**
     * @var        ObjectCollection|ChildRemoteLog[] Collection to store aggregation of ChildRemoteLog objects.
     */
    protected $collRemoteLogs;
    protected $collRemoteLogsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCustomFormLog[]
     */
    protected $customFormLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDevice[]
     */
    protected $devicesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMediaLog[]
     */
    protected $mediaLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostLog[]
     */
    protected $postLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildRemoteLog[]
     */
    protected $remoteLogsScheduledForDeletion = null;

    /**
     * Initializes internal state of Database\HubPlus\Base\UserApp object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>UserApp</code> instance.  If
     * <code>obj</code> is an instance of <code>UserApp</code>, delegates to
     * <code>equals(UserApp)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|UserApp The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [username] column value.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the [username_canonical] column value.
     *
     * @return string
     */
    public function getUsernameCanonical()
    {
        return $this->username_canonical;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [email_canonical] column value.
     *
     * @return string
     */
    public function getEmailCanonical()
    {
        return $this->email_canonical;
    }

    /**
     * Get the [enabled] column value.
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get the [enabled] column value.
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->getEnabled();
    }

    /**
     * Get the [privacy] column value.
     *
     * @return boolean
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * Get the [privacy] column value.
     *
     * @return boolean
     */
    public function isPrivacy()
    {
        return $this->getPrivacy();
    }

    /**
     * Get the [terms] column value.
     *
     * @return boolean
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Get the [terms] column value.
     *
     * @return boolean
     */
    public function isTerms()
    {
        return $this->getTerms();
    }

    /**
     * Get the [salt] column value.
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [optionally formatted] temporal [last_login] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastLogin($format = NULL)
    {
        if ($format === null) {
            return $this->last_login;
        } else {
            return $this->last_login instanceof \DateTimeInterface ? $this->last_login->format($format) : null;
        }
    }

    /**
     * Get the [locked] column value.
     *
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Get the [locked] column value.
     *
     * @return boolean
     */
    public function isLocked()
    {
        return $this->getLocked();
    }

    /**
     * Get the [expired] column value.
     *
     * @return boolean
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * Get the [expired] column value.
     *
     * @return boolean
     */
    public function isExpired()
    {
        return $this->getExpired();
    }

    /**
     * Get the [optionally formatted] temporal [expires_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getExpiresAt($format = NULL)
    {
        if ($format === null) {
            return $this->expires_at;
        } else {
            return $this->expires_at instanceof \DateTimeInterface ? $this->expires_at->format($format) : null;
        }
    }

    /**
     * Get the [confirmation_token] column value.
     *
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmation_token;
    }

    /**
     * Get the [optionally formatted] temporal [password_requested_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPasswordRequestedAt($format = NULL)
    {
        if ($format === null) {
            return $this->password_requested_at;
        } else {
            return $this->password_requested_at instanceof \DateTimeInterface ? $this->password_requested_at->format($format) : null;
        }
    }

    /**
     * Get the [roles] column value.
     *
     * @return string
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Get the [credentials_expired] column value.
     *
     * @return boolean
     */
    public function getCredentialsExpired()
    {
        return $this->credentials_expired;
    }

    /**
     * Get the [credentials_expired] column value.
     *
     * @return boolean
     */
    public function isCredentialsExpired()
    {
        return $this->getCredentialsExpired();
    }

    /**
     * Get the [optionally formatted] temporal [credentials_expire_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCredentialsExpireAt($format = NULL)
    {
        if ($format === null) {
            return $this->credentials_expire_at;
        } else {
            return $this->credentials_expire_at instanceof \DateTimeInterface ? $this->credentials_expire_at->format($format) : null;
        }
    }

    /**
     * Get the [firstname] column value.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Get the [lastname] column value.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Get the [phone] column value.
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get the [imagepath] column value.
     *
     * @return string
     */
    public function getImagepath()
    {
        return $this->imagepath;
    }

    /**
     * Get the [address_id] column value.
     *
     * @return int
     */
    public function getAddressId()
    {
        return $this->address_id;
    }

    /**
     * Get the [tags] column value.
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get the [section_id] column value.
     *
     * @return int
     */
    public function getSectionId()
    {
        return $this->section_id;
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UserAppTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [username] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[UserAppTableMap::COL_USERNAME] = true;
        }

        return $this;
    } // setUsername()

    /**
     * Set the value of [username_canonical] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setUsernameCanonical($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->username_canonical !== $v) {
            $this->username_canonical = $v;
            $this->modifiedColumns[UserAppTableMap::COL_USERNAME_CANONICAL] = true;
        }

        return $this;
    } // setUsernameCanonical()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[UserAppTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [email_canonical] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setEmailCanonical($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email_canonical !== $v) {
            $this->email_canonical = $v;
            $this->modifiedColumns[UserAppTableMap::COL_EMAIL_CANONICAL] = true;
        }

        return $this;
    } // setEmailCanonical()

    /**
     * Sets the value of the [enabled] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setEnabled($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->enabled !== $v) {
            $this->enabled = $v;
            $this->modifiedColumns[UserAppTableMap::COL_ENABLED] = true;
        }

        return $this;
    } // setEnabled()

    /**
     * Sets the value of the [privacy] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setPrivacy($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->privacy !== $v) {
            $this->privacy = $v;
            $this->modifiedColumns[UserAppTableMap::COL_PRIVACY] = true;
        }

        return $this;
    } // setPrivacy()

    /**
     * Sets the value of the [terms] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setTerms($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->terms !== $v) {
            $this->terms = $v;
            $this->modifiedColumns[UserAppTableMap::COL_TERMS] = true;
        }

        return $this;
    } // setTerms()

    /**
     * Set the value of [salt] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setSalt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->salt !== $v) {
            $this->salt = $v;
            $this->modifiedColumns[UserAppTableMap::COL_SALT] = true;
        }

        return $this;
    } // setSalt()

    /**
     * Set the value of [password] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[UserAppTableMap::COL_PASSWORD] = true;
        }

        return $this;
    } // setPassword()

    /**
     * Sets the value of [last_login] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setLastLogin($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_login !== null || $dt !== null) {
            if ($this->last_login === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->last_login->format("Y-m-d H:i:s.u")) {
                $this->last_login = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserAppTableMap::COL_LAST_LOGIN] = true;
            }
        } // if either are not null

        return $this;
    } // setLastLogin()

    /**
     * Sets the value of the [locked] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setLocked($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->locked !== $v) {
            $this->locked = $v;
            $this->modifiedColumns[UserAppTableMap::COL_LOCKED] = true;
        }

        return $this;
    } // setLocked()

    /**
     * Sets the value of the [expired] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setExpired($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->expired !== $v) {
            $this->expired = $v;
            $this->modifiedColumns[UserAppTableMap::COL_EXPIRED] = true;
        }

        return $this;
    } // setExpired()

    /**
     * Sets the value of [expires_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setExpiresAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->expires_at !== null || $dt !== null) {
            if ($this->expires_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->expires_at->format("Y-m-d H:i:s.u")) {
                $this->expires_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserAppTableMap::COL_EXPIRES_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setExpiresAt()

    /**
     * Set the value of [confirmation_token] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setConfirmationToken($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->confirmation_token !== $v) {
            $this->confirmation_token = $v;
            $this->modifiedColumns[UserAppTableMap::COL_CONFIRMATION_TOKEN] = true;
        }

        return $this;
    } // setConfirmationToken()

    /**
     * Sets the value of [password_requested_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setPasswordRequestedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->password_requested_at !== null || $dt !== null) {
            if ($this->password_requested_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->password_requested_at->format("Y-m-d H:i:s.u")) {
                $this->password_requested_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserAppTableMap::COL_PASSWORD_REQUESTED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setPasswordRequestedAt()

    /**
     * Set the value of [roles] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setRoles($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->roles !== $v) {
            $this->roles = $v;
            $this->modifiedColumns[UserAppTableMap::COL_ROLES] = true;
        }

        return $this;
    } // setRoles()

    /**
     * Sets the value of the [credentials_expired] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setCredentialsExpired($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->credentials_expired !== $v) {
            $this->credentials_expired = $v;
            $this->modifiedColumns[UserAppTableMap::COL_CREDENTIALS_EXPIRED] = true;
        }

        return $this;
    } // setCredentialsExpired()

    /**
     * Sets the value of [credentials_expire_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setCredentialsExpireAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->credentials_expire_at !== null || $dt !== null) {
            if ($this->credentials_expire_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->credentials_expire_at->format("Y-m-d H:i:s.u")) {
                $this->credentials_expire_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserAppTableMap::COL_CREDENTIALS_EXPIRE_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCredentialsExpireAt()

    /**
     * Set the value of [firstname] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setFirstname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->firstname !== $v) {
            $this->firstname = $v;
            $this->modifiedColumns[UserAppTableMap::COL_FIRSTNAME] = true;
        }

        return $this;
    } // setFirstname()

    /**
     * Set the value of [lastname] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setLastname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->lastname !== $v) {
            $this->lastname = $v;
            $this->modifiedColumns[UserAppTableMap::COL_LASTNAME] = true;
        }

        return $this;
    } // setLastname()

    /**
     * Set the value of [phone] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[UserAppTableMap::COL_PHONE] = true;
        }

        return $this;
    } // setPhone()

    /**
     * Set the value of [imagepath] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setImagepath($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->imagepath !== $v) {
            $this->imagepath = $v;
            $this->modifiedColumns[UserAppTableMap::COL_IMAGEPATH] = true;
        }

        return $this;
    } // setImagepath()

    /**
     * Set the value of [address_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setAddressId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->address_id !== $v) {
            $this->address_id = $v;
            $this->modifiedColumns[UserAppTableMap::COL_ADDRESS_ID] = true;
        }

        if ($this->aAddress !== null && $this->aAddress->getId() !== $v) {
            $this->aAddress = null;
        }

        return $this;
    } // setAddressId()

    /**
     * Set the value of [tags] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setTags($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tags !== $v) {
            $this->tags = $v;
            $this->modifiedColumns[UserAppTableMap::COL_TAGS] = true;
        }

        return $this;
    } // setTags()

    /**
     * Set the value of [section_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setSectionId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->section_id !== $v) {
            $this->section_id = $v;
            $this->modifiedColumns[UserAppTableMap::COL_SECTION_ID] = true;
        }

        if ($this->aSection !== null && $this->aSection->getId() !== $v) {
            $this->aSection = null;
        }

        return $this;
    } // setSectionId()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserAppTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserAppTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserAppTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UserAppTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UserAppTableMap::translateFieldName('Username', TableMap::TYPE_PHPNAME, $indexType)];
            $this->username = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UserAppTableMap::translateFieldName('UsernameCanonical', TableMap::TYPE_PHPNAME, $indexType)];
            $this->username_canonical = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UserAppTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UserAppTableMap::translateFieldName('EmailCanonical', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email_canonical = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UserAppTableMap::translateFieldName('Enabled', TableMap::TYPE_PHPNAME, $indexType)];
            $this->enabled = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UserAppTableMap::translateFieldName('Privacy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->privacy = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UserAppTableMap::translateFieldName('Terms', TableMap::TYPE_PHPNAME, $indexType)];
            $this->terms = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UserAppTableMap::translateFieldName('Salt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->salt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : UserAppTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : UserAppTableMap::translateFieldName('LastLogin', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->last_login = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : UserAppTableMap::translateFieldName('Locked', TableMap::TYPE_PHPNAME, $indexType)];
            $this->locked = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : UserAppTableMap::translateFieldName('Expired', TableMap::TYPE_PHPNAME, $indexType)];
            $this->expired = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : UserAppTableMap::translateFieldName('ExpiresAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->expires_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : UserAppTableMap::translateFieldName('ConfirmationToken', TableMap::TYPE_PHPNAME, $indexType)];
            $this->confirmation_token = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : UserAppTableMap::translateFieldName('PasswordRequestedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->password_requested_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : UserAppTableMap::translateFieldName('Roles', TableMap::TYPE_PHPNAME, $indexType)];
            $this->roles = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : UserAppTableMap::translateFieldName('CredentialsExpired', TableMap::TYPE_PHPNAME, $indexType)];
            $this->credentials_expired = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : UserAppTableMap::translateFieldName('CredentialsExpireAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->credentials_expire_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : UserAppTableMap::translateFieldName('Firstname', TableMap::TYPE_PHPNAME, $indexType)];
            $this->firstname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : UserAppTableMap::translateFieldName('Lastname', TableMap::TYPE_PHPNAME, $indexType)];
            $this->lastname = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : UserAppTableMap::translateFieldName('Phone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->phone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : UserAppTableMap::translateFieldName('Imagepath', TableMap::TYPE_PHPNAME, $indexType)];
            $this->imagepath = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : UserAppTableMap::translateFieldName('AddressId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->address_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : UserAppTableMap::translateFieldName('Tags', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tags = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : UserAppTableMap::translateFieldName('SectionId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->section_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : UserAppTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : UserAppTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : UserAppTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 29; // 29 = UserAppTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Database\\HubPlus\\UserApp'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aAddress !== null && $this->address_id !== $this->aAddress->getId()) {
            $this->aAddress = null;
        }
        if ($this->aSection !== null && $this->section_id !== $this->aSection->getId()) {
            $this->aSection = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserAppTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUserAppQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aAddress = null;
            $this->aSection = null;
            $this->collCustomFormLogs = null;

            $this->collDevices = null;

            $this->collMediaLogs = null;

            $this->collPostLogs = null;

            $this->collRemoteLogs = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see UserApp::setDeleted()
     * @see UserApp::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAppTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUserAppQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildUserAppQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAppTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(UserAppTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(UserAppTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(UserAppTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UserAppTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aAddress !== null) {
                if ($this->aAddress->isModified() || $this->aAddress->isNew()) {
                    $affectedRows += $this->aAddress->save($con);
                }
                $this->setAddress($this->aAddress);
            }

            if ($this->aSection !== null) {
                if ($this->aSection->isModified() || $this->aSection->isNew()) {
                    $affectedRows += $this->aSection->save($con);
                }
                $this->setSection($this->aSection);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->customFormLogsScheduledForDeletion !== null) {
                if (!$this->customFormLogsScheduledForDeletion->isEmpty()) {
                    foreach ($this->customFormLogsScheduledForDeletion as $customFormLog) {
                        // need to save related object because we set the relation to null
                        $customFormLog->save($con);
                    }
                    $this->customFormLogsScheduledForDeletion = null;
                }
            }

            if ($this->collCustomFormLogs !== null) {
                foreach ($this->collCustomFormLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->devicesScheduledForDeletion !== null) {
                if (!$this->devicesScheduledForDeletion->isEmpty()) {
                    foreach ($this->devicesScheduledForDeletion as $device) {
                        // need to save related object because we set the relation to null
                        $device->save($con);
                    }
                    $this->devicesScheduledForDeletion = null;
                }
            }

            if ($this->collDevices !== null) {
                foreach ($this->collDevices as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->mediaLogsScheduledForDeletion !== null) {
                if (!$this->mediaLogsScheduledForDeletion->isEmpty()) {
                    foreach ($this->mediaLogsScheduledForDeletion as $mediaLog) {
                        // need to save related object because we set the relation to null
                        $mediaLog->save($con);
                    }
                    $this->mediaLogsScheduledForDeletion = null;
                }
            }

            if ($this->collMediaLogs !== null) {
                foreach ($this->collMediaLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postLogsScheduledForDeletion !== null) {
                if (!$this->postLogsScheduledForDeletion->isEmpty()) {
                    foreach ($this->postLogsScheduledForDeletion as $postLog) {
                        // need to save related object because we set the relation to null
                        $postLog->save($con);
                    }
                    $this->postLogsScheduledForDeletion = null;
                }
            }

            if ($this->collPostLogs !== null) {
                foreach ($this->collPostLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->remoteLogsScheduledForDeletion !== null) {
                if (!$this->remoteLogsScheduledForDeletion->isEmpty()) {
                    foreach ($this->remoteLogsScheduledForDeletion as $remoteLog) {
                        // need to save related object because we set the relation to null
                        $remoteLog->save($con);
                    }
                    $this->remoteLogsScheduledForDeletion = null;
                }
            }

            if ($this->collRemoteLogs !== null) {
                foreach ($this->collRemoteLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UserAppTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UserAppTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UserAppTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_USERNAME)) {
            $modifiedColumns[':p' . $index++]  = 'username';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_USERNAME_CANONICAL)) {
            $modifiedColumns[':p' . $index++]  = 'username_canonical';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'email';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_EMAIL_CANONICAL)) {
            $modifiedColumns[':p' . $index++]  = 'email_canonical';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_ENABLED)) {
            $modifiedColumns[':p' . $index++]  = 'enabled';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_PRIVACY)) {
            $modifiedColumns[':p' . $index++]  = 'privacy';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_TERMS)) {
            $modifiedColumns[':p' . $index++]  = 'terms';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_SALT)) {
            $modifiedColumns[':p' . $index++]  = 'salt';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'password';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_LAST_LOGIN)) {
            $modifiedColumns[':p' . $index++]  = 'last_login';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_LOCKED)) {
            $modifiedColumns[':p' . $index++]  = 'locked';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_EXPIRED)) {
            $modifiedColumns[':p' . $index++]  = 'expired';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_EXPIRES_AT)) {
            $modifiedColumns[':p' . $index++]  = 'expires_at';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_CONFIRMATION_TOKEN)) {
            $modifiedColumns[':p' . $index++]  = 'confirmation_token';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_PASSWORD_REQUESTED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'password_requested_at';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_ROLES)) {
            $modifiedColumns[':p' . $index++]  = 'roles';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_CREDENTIALS_EXPIRED)) {
            $modifiedColumns[':p' . $index++]  = 'credentials_expired';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_CREDENTIALS_EXPIRE_AT)) {
            $modifiedColumns[':p' . $index++]  = 'credentials_expire_at';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_FIRSTNAME)) {
            $modifiedColumns[':p' . $index++]  = 'firstname';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_LASTNAME)) {
            $modifiedColumns[':p' . $index++]  = 'lastname';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_PHONE)) {
            $modifiedColumns[':p' . $index++]  = 'phone';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_IMAGEPATH)) {
            $modifiedColumns[':p' . $index++]  = 'imagePath';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_ADDRESS_ID)) {
            $modifiedColumns[':p' . $index++]  = 'address_id';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_TAGS)) {
            $modifiedColumns[':p' . $index++]  = 'tags';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_SECTION_ID)) {
            $modifiedColumns[':p' . $index++]  = 'section_id';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(UserAppTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO user_app (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'username':
                        $stmt->bindValue($identifier, $this->username, PDO::PARAM_STR);
                        break;
                    case 'username_canonical':
                        $stmt->bindValue($identifier, $this->username_canonical, PDO::PARAM_STR);
                        break;
                    case 'email':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case 'email_canonical':
                        $stmt->bindValue($identifier, $this->email_canonical, PDO::PARAM_STR);
                        break;
                    case 'enabled':
                        $stmt->bindValue($identifier, (int) $this->enabled, PDO::PARAM_INT);
                        break;
                    case 'privacy':
                        $stmt->bindValue($identifier, (int) $this->privacy, PDO::PARAM_INT);
                        break;
                    case 'terms':
                        $stmt->bindValue($identifier, (int) $this->terms, PDO::PARAM_INT);
                        break;
                    case 'salt':
                        $stmt->bindValue($identifier, $this->salt, PDO::PARAM_STR);
                        break;
                    case 'password':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case 'last_login':
                        $stmt->bindValue($identifier, $this->last_login ? $this->last_login->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'locked':
                        $stmt->bindValue($identifier, (int) $this->locked, PDO::PARAM_INT);
                        break;
                    case 'expired':
                        $stmt->bindValue($identifier, (int) $this->expired, PDO::PARAM_INT);
                        break;
                    case 'expires_at':
                        $stmt->bindValue($identifier, $this->expires_at ? $this->expires_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'confirmation_token':
                        $stmt->bindValue($identifier, $this->confirmation_token, PDO::PARAM_STR);
                        break;
                    case 'password_requested_at':
                        $stmt->bindValue($identifier, $this->password_requested_at ? $this->password_requested_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'roles':
                        $stmt->bindValue($identifier, $this->roles, PDO::PARAM_STR);
                        break;
                    case 'credentials_expired':
                        $stmt->bindValue($identifier, (int) $this->credentials_expired, PDO::PARAM_INT);
                        break;
                    case 'credentials_expire_at':
                        $stmt->bindValue($identifier, $this->credentials_expire_at ? $this->credentials_expire_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'firstname':
                        $stmt->bindValue($identifier, $this->firstname, PDO::PARAM_STR);
                        break;
                    case 'lastname':
                        $stmt->bindValue($identifier, $this->lastname, PDO::PARAM_STR);
                        break;
                    case 'phone':
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case 'imagePath':
                        $stmt->bindValue($identifier, $this->imagepath, PDO::PARAM_STR);
                        break;
                    case 'address_id':
                        $stmt->bindValue($identifier, $this->address_id, PDO::PARAM_INT);
                        break;
                    case 'tags':
                        $stmt->bindValue($identifier, $this->tags, PDO::PARAM_STR);
                        break;
                    case 'section_id':
                        $stmt->bindValue($identifier, $this->section_id, PDO::PARAM_INT);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserAppTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUsername();
                break;
            case 2:
                return $this->getUsernameCanonical();
                break;
            case 3:
                return $this->getEmail();
                break;
            case 4:
                return $this->getEmailCanonical();
                break;
            case 5:
                return $this->getEnabled();
                break;
            case 6:
                return $this->getPrivacy();
                break;
            case 7:
                return $this->getTerms();
                break;
            case 8:
                return $this->getSalt();
                break;
            case 9:
                return $this->getPassword();
                break;
            case 10:
                return $this->getLastLogin();
                break;
            case 11:
                return $this->getLocked();
                break;
            case 12:
                return $this->getExpired();
                break;
            case 13:
                return $this->getExpiresAt();
                break;
            case 14:
                return $this->getConfirmationToken();
                break;
            case 15:
                return $this->getPasswordRequestedAt();
                break;
            case 16:
                return $this->getRoles();
                break;
            case 17:
                return $this->getCredentialsExpired();
                break;
            case 18:
                return $this->getCredentialsExpireAt();
                break;
            case 19:
                return $this->getFirstname();
                break;
            case 20:
                return $this->getLastname();
                break;
            case 21:
                return $this->getPhone();
                break;
            case 22:
                return $this->getImagepath();
                break;
            case 23:
                return $this->getAddressId();
                break;
            case 24:
                return $this->getTags();
                break;
            case 25:
                return $this->getSectionId();
                break;
            case 26:
                return $this->getDeletedAt();
                break;
            case 27:
                return $this->getCreatedAt();
                break;
            case 28:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['UserApp'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['UserApp'][$this->hashCode()] = true;
        $keys = UserAppTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUsername(),
            $keys[2] => $this->getUsernameCanonical(),
            $keys[3] => $this->getEmail(),
            $keys[4] => $this->getEmailCanonical(),
            $keys[5] => $this->getEnabled(),
            $keys[6] => $this->getPrivacy(),
            $keys[7] => $this->getTerms(),
            $keys[8] => $this->getSalt(),
            $keys[9] => $this->getPassword(),
            $keys[10] => $this->getLastLogin(),
            $keys[11] => $this->getLocked(),
            $keys[12] => $this->getExpired(),
            $keys[13] => $this->getExpiresAt(),
            $keys[14] => $this->getConfirmationToken(),
            $keys[15] => $this->getPasswordRequestedAt(),
            $keys[16] => $this->getRoles(),
            $keys[17] => $this->getCredentialsExpired(),
            $keys[18] => $this->getCredentialsExpireAt(),
            $keys[19] => $this->getFirstname(),
            $keys[20] => $this->getLastname(),
            $keys[21] => $this->getPhone(),
            $keys[22] => $this->getImagepath(),
            $keys[23] => $this->getAddressId(),
            $keys[24] => $this->getTags(),
            $keys[25] => $this->getSectionId(),
            $keys[26] => $this->getDeletedAt(),
            $keys[27] => $this->getCreatedAt(),
            $keys[28] => $this->getUpdatedAt(),
        );
        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTimeInterface) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        if ($result[$keys[15]] instanceof \DateTimeInterface) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTimeInterface) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        if ($result[$keys[26]] instanceof \DateTimeInterface) {
            $result[$keys[26]] = $result[$keys[26]]->format('c');
        }

        if ($result[$keys[27]] instanceof \DateTimeInterface) {
            $result[$keys[27]] = $result[$keys[27]]->format('c');
        }

        if ($result[$keys[28]] instanceof \DateTimeInterface) {
            $result[$keys[28]] = $result[$keys[28]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aAddress) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'address';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'address';
                        break;
                    default:
                        $key = 'Address';
                }

                $result[$key] = $this->aAddress->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSection) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'section';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'section';
                        break;
                    default:
                        $key = 'Section';
                }

                $result[$key] = $this->aSection->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCustomFormLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customFormLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cf_logs';
                        break;
                    default:
                        $key = 'CustomFormLogs';
                }

                $result[$key] = $this->collCustomFormLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDevices) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'devices';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'devices';
                        break;
                    default:
                        $key = 'Devices';
                }

                $result[$key] = $this->collDevices->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMediaLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mediaLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'media_logs';
                        break;
                    default:
                        $key = 'MediaLogs';
                }

                $result[$key] = $this->collMediaLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPostLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_logs';
                        break;
                    default:
                        $key = 'PostLogs';
                }

                $result[$key] = $this->collPostLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRemoteLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'remoteLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'remote_logs';
                        break;
                    default:
                        $key = 'RemoteLogs';
                }

                $result[$key] = $this->collRemoteLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Database\HubPlus\UserApp
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserAppTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Database\HubPlus\UserApp
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUsername($value);
                break;
            case 2:
                $this->setUsernameCanonical($value);
                break;
            case 3:
                $this->setEmail($value);
                break;
            case 4:
                $this->setEmailCanonical($value);
                break;
            case 5:
                $this->setEnabled($value);
                break;
            case 6:
                $this->setPrivacy($value);
                break;
            case 7:
                $this->setTerms($value);
                break;
            case 8:
                $this->setSalt($value);
                break;
            case 9:
                $this->setPassword($value);
                break;
            case 10:
                $this->setLastLogin($value);
                break;
            case 11:
                $this->setLocked($value);
                break;
            case 12:
                $this->setExpired($value);
                break;
            case 13:
                $this->setExpiresAt($value);
                break;
            case 14:
                $this->setConfirmationToken($value);
                break;
            case 15:
                $this->setPasswordRequestedAt($value);
                break;
            case 16:
                $this->setRoles($value);
                break;
            case 17:
                $this->setCredentialsExpired($value);
                break;
            case 18:
                $this->setCredentialsExpireAt($value);
                break;
            case 19:
                $this->setFirstname($value);
                break;
            case 20:
                $this->setLastname($value);
                break;
            case 21:
                $this->setPhone($value);
                break;
            case 22:
                $this->setImagepath($value);
                break;
            case 23:
                $this->setAddressId($value);
                break;
            case 24:
                $this->setTags($value);
                break;
            case 25:
                $this->setSectionId($value);
                break;
            case 26:
                $this->setDeletedAt($value);
                break;
            case 27:
                $this->setCreatedAt($value);
                break;
            case 28:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UserAppTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUsername($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setUsernameCanonical($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setEmail($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setEmailCanonical($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setEnabled($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setPrivacy($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setTerms($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setSalt($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setPassword($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setLastLogin($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setLocked($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setExpired($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setExpiresAt($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setConfirmationToken($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setPasswordRequestedAt($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setRoles($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setCredentialsExpired($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setCredentialsExpireAt($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setFirstname($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setLastname($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setPhone($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setImagepath($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setAddressId($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setTags($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setSectionId($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setDeletedAt($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setCreatedAt($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setUpdatedAt($arr[$keys[28]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Database\HubPlus\UserApp The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UserAppTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UserAppTableMap::COL_ID)) {
            $criteria->add(UserAppTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_USERNAME)) {
            $criteria->add(UserAppTableMap::COL_USERNAME, $this->username);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_USERNAME_CANONICAL)) {
            $criteria->add(UserAppTableMap::COL_USERNAME_CANONICAL, $this->username_canonical);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_EMAIL)) {
            $criteria->add(UserAppTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_EMAIL_CANONICAL)) {
            $criteria->add(UserAppTableMap::COL_EMAIL_CANONICAL, $this->email_canonical);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_ENABLED)) {
            $criteria->add(UserAppTableMap::COL_ENABLED, $this->enabled);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_PRIVACY)) {
            $criteria->add(UserAppTableMap::COL_PRIVACY, $this->privacy);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_TERMS)) {
            $criteria->add(UserAppTableMap::COL_TERMS, $this->terms);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_SALT)) {
            $criteria->add(UserAppTableMap::COL_SALT, $this->salt);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_PASSWORD)) {
            $criteria->add(UserAppTableMap::COL_PASSWORD, $this->password);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_LAST_LOGIN)) {
            $criteria->add(UserAppTableMap::COL_LAST_LOGIN, $this->last_login);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_LOCKED)) {
            $criteria->add(UserAppTableMap::COL_LOCKED, $this->locked);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_EXPIRED)) {
            $criteria->add(UserAppTableMap::COL_EXPIRED, $this->expired);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_EXPIRES_AT)) {
            $criteria->add(UserAppTableMap::COL_EXPIRES_AT, $this->expires_at);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_CONFIRMATION_TOKEN)) {
            $criteria->add(UserAppTableMap::COL_CONFIRMATION_TOKEN, $this->confirmation_token);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_PASSWORD_REQUESTED_AT)) {
            $criteria->add(UserAppTableMap::COL_PASSWORD_REQUESTED_AT, $this->password_requested_at);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_ROLES)) {
            $criteria->add(UserAppTableMap::COL_ROLES, $this->roles);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_CREDENTIALS_EXPIRED)) {
            $criteria->add(UserAppTableMap::COL_CREDENTIALS_EXPIRED, $this->credentials_expired);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_CREDENTIALS_EXPIRE_AT)) {
            $criteria->add(UserAppTableMap::COL_CREDENTIALS_EXPIRE_AT, $this->credentials_expire_at);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_FIRSTNAME)) {
            $criteria->add(UserAppTableMap::COL_FIRSTNAME, $this->firstname);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_LASTNAME)) {
            $criteria->add(UserAppTableMap::COL_LASTNAME, $this->lastname);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_PHONE)) {
            $criteria->add(UserAppTableMap::COL_PHONE, $this->phone);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_IMAGEPATH)) {
            $criteria->add(UserAppTableMap::COL_IMAGEPATH, $this->imagepath);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_ADDRESS_ID)) {
            $criteria->add(UserAppTableMap::COL_ADDRESS_ID, $this->address_id);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_TAGS)) {
            $criteria->add(UserAppTableMap::COL_TAGS, $this->tags);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_SECTION_ID)) {
            $criteria->add(UserAppTableMap::COL_SECTION_ID, $this->section_id);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_DELETED_AT)) {
            $criteria->add(UserAppTableMap::COL_DELETED_AT, $this->deleted_at);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_CREATED_AT)) {
            $criteria->add(UserAppTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(UserAppTableMap::COL_UPDATED_AT)) {
            $criteria->add(UserAppTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUserAppQuery::create();
        $criteria->add(UserAppTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Database\HubPlus\UserApp (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUsername($this->getUsername());
        $copyObj->setUsernameCanonical($this->getUsernameCanonical());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setEmailCanonical($this->getEmailCanonical());
        $copyObj->setEnabled($this->getEnabled());
        $copyObj->setPrivacy($this->getPrivacy());
        $copyObj->setTerms($this->getTerms());
        $copyObj->setSalt($this->getSalt());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setLastLogin($this->getLastLogin());
        $copyObj->setLocked($this->getLocked());
        $copyObj->setExpired($this->getExpired());
        $copyObj->setExpiresAt($this->getExpiresAt());
        $copyObj->setConfirmationToken($this->getConfirmationToken());
        $copyObj->setPasswordRequestedAt($this->getPasswordRequestedAt());
        $copyObj->setRoles($this->getRoles());
        $copyObj->setCredentialsExpired($this->getCredentialsExpired());
        $copyObj->setCredentialsExpireAt($this->getCredentialsExpireAt());
        $copyObj->setFirstname($this->getFirstname());
        $copyObj->setLastname($this->getLastname());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setImagepath($this->getImagepath());
        $copyObj->setAddressId($this->getAddressId());
        $copyObj->setTags($this->getTags());
        $copyObj->setSectionId($this->getSectionId());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCustomFormLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomFormLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDevices() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDevice($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMediaLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMediaLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPostLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRemoteLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRemoteLog($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Database\HubPlus\UserApp Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildAddress object.
     *
     * @param  ChildAddress $v
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAddress(ChildAddress $v = null)
    {
        if ($v === null) {
            $this->setAddressId(NULL);
        } else {
            $this->setAddressId($v->getId());
        }

        $this->aAddress = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildAddress object, it will not be re-added.
        if ($v !== null) {
            $v->addUserApp($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildAddress object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildAddress The associated ChildAddress object.
     * @throws PropelException
     */
    public function getAddress(ConnectionInterface $con = null)
    {
        if ($this->aAddress === null && ($this->address_id != 0)) {
            $this->aAddress = ChildAddressQuery::create()->findPk($this->address_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAddress->addUserApps($this);
             */
        }

        return $this->aAddress;
    }

    /**
     * Declares an association between this object and a ChildSection object.
     *
     * @param  ChildSection $v
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSection(ChildSection $v = null)
    {
        if ($v === null) {
            $this->setSectionId(NULL);
        } else {
            $this->setSectionId($v->getId());
        }

        $this->aSection = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSection object, it will not be re-added.
        if ($v !== null) {
            $v->addUserApp($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSection object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSection The associated ChildSection object.
     * @throws PropelException
     */
    public function getSection(ConnectionInterface $con = null)
    {
        if ($this->aSection === null && ($this->section_id != 0)) {
            $this->aSection = ChildSectionQuery::create()->findPk($this->section_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSection->addUserApps($this);
             */
        }

        return $this->aSection;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CustomFormLog' == $relationName) {
            $this->initCustomFormLogs();
            return;
        }
        if ('Device' == $relationName) {
            $this->initDevices();
            return;
        }
        if ('MediaLog' == $relationName) {
            $this->initMediaLogs();
            return;
        }
        if ('PostLog' == $relationName) {
            $this->initPostLogs();
            return;
        }
        if ('RemoteLog' == $relationName) {
            $this->initRemoteLogs();
            return;
        }
    }

    /**
     * Clears out the collCustomFormLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomFormLogs()
     */
    public function clearCustomFormLogs()
    {
        $this->collCustomFormLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomFormLogs collection loaded partially.
     */
    public function resetPartialCustomFormLogs($v = true)
    {
        $this->collCustomFormLogsPartial = $v;
    }

    /**
     * Initializes the collCustomFormLogs collection.
     *
     * By default this just sets the collCustomFormLogs collection to an empty array (like clearcollCustomFormLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomFormLogs($overrideExisting = true)
    {
        if (null !== $this->collCustomFormLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomFormLogTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomFormLogs = new $collectionClassName;
        $this->collCustomFormLogs->setModel('\Database\HubPlus\CustomFormLog');
    }

    /**
     * Gets an array of ChildCustomFormLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUserApp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCustomFormLog[] List of ChildCustomFormLog objects
     * @throws PropelException
     */
    public function getCustomFormLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomFormLogsPartial && !$this->isNew();
        if (null === $this->collCustomFormLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCustomFormLogs) {
                // return empty collection
                $this->initCustomFormLogs();
            } else {
                $collCustomFormLogs = ChildCustomFormLogQuery::create(null, $criteria)
                    ->filterByUserApp($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomFormLogsPartial && count($collCustomFormLogs)) {
                        $this->initCustomFormLogs(false);

                        foreach ($collCustomFormLogs as $obj) {
                            if (false == $this->collCustomFormLogs->contains($obj)) {
                                $this->collCustomFormLogs->append($obj);
                            }
                        }

                        $this->collCustomFormLogsPartial = true;
                    }

                    return $collCustomFormLogs;
                }

                if ($partial && $this->collCustomFormLogs) {
                    foreach ($this->collCustomFormLogs as $obj) {
                        if ($obj->isNew()) {
                            $collCustomFormLogs[] = $obj;
                        }
                    }
                }

                $this->collCustomFormLogs = $collCustomFormLogs;
                $this->collCustomFormLogsPartial = false;
            }
        }

        return $this->collCustomFormLogs;
    }

    /**
     * Sets a collection of ChildCustomFormLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customFormLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUserApp The current object (for fluent API support)
     */
    public function setCustomFormLogs(Collection $customFormLogs, ConnectionInterface $con = null)
    {
        /** @var ChildCustomFormLog[] $customFormLogsToDelete */
        $customFormLogsToDelete = $this->getCustomFormLogs(new Criteria(), $con)->diff($customFormLogs);


        $this->customFormLogsScheduledForDeletion = $customFormLogsToDelete;

        foreach ($customFormLogsToDelete as $customFormLogRemoved) {
            $customFormLogRemoved->setUserApp(null);
        }

        $this->collCustomFormLogs = null;
        foreach ($customFormLogs as $customFormLog) {
            $this->addCustomFormLog($customFormLog);
        }

        $this->collCustomFormLogs = $customFormLogs;
        $this->collCustomFormLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CustomFormLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CustomFormLog objects.
     * @throws PropelException
     */
    public function countCustomFormLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomFormLogsPartial && !$this->isNew();
        if (null === $this->collCustomFormLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomFormLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomFormLogs());
            }

            $query = ChildCustomFormLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUserApp($this)
                ->count($con);
        }

        return count($this->collCustomFormLogs);
    }

    /**
     * Method called to associate a ChildCustomFormLog object to this object
     * through the ChildCustomFormLog foreign key attribute.
     *
     * @param  ChildCustomFormLog $l ChildCustomFormLog
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function addCustomFormLog(ChildCustomFormLog $l)
    {
        if ($this->collCustomFormLogs === null) {
            $this->initCustomFormLogs();
            $this->collCustomFormLogsPartial = true;
        }

        if (!$this->collCustomFormLogs->contains($l)) {
            $this->doAddCustomFormLog($l);

            if ($this->customFormLogsScheduledForDeletion and $this->customFormLogsScheduledForDeletion->contains($l)) {
                $this->customFormLogsScheduledForDeletion->remove($this->customFormLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCustomFormLog $customFormLog The ChildCustomFormLog object to add.
     */
    protected function doAddCustomFormLog(ChildCustomFormLog $customFormLog)
    {
        $this->collCustomFormLogs[]= $customFormLog;
        $customFormLog->setUserApp($this);
    }

    /**
     * @param  ChildCustomFormLog $customFormLog The ChildCustomFormLog object to remove.
     * @return $this|ChildUserApp The current object (for fluent API support)
     */
    public function removeCustomFormLog(ChildCustomFormLog $customFormLog)
    {
        if ($this->getCustomFormLogs()->contains($customFormLog)) {
            $pos = $this->collCustomFormLogs->search($customFormLog);
            $this->collCustomFormLogs->remove($pos);
            if (null === $this->customFormLogsScheduledForDeletion) {
                $this->customFormLogsScheduledForDeletion = clone $this->collCustomFormLogs;
                $this->customFormLogsScheduledForDeletion->clear();
            }
            $this->customFormLogsScheduledForDeletion[]= $customFormLog;
            $customFormLog->setUserApp(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserApp is new, it will return
     * an empty collection; or if this UserApp has previously
     * been saved, it will retrieve related CustomFormLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserApp.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCustomFormLog[] List of ChildCustomFormLog objects
     */
    public function getCustomFormLogsJoinSection(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCustomFormLogQuery::create(null, $criteria);
        $query->joinWith('Section', $joinBehavior);

        return $this->getCustomFormLogs($query, $con);
    }

    /**
     * Clears out the collDevices collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addDevices()
     */
    public function clearDevices()
    {
        $this->collDevices = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collDevices collection loaded partially.
     */
    public function resetPartialDevices($v = true)
    {
        $this->collDevicesPartial = $v;
    }

    /**
     * Initializes the collDevices collection.
     *
     * By default this just sets the collDevices collection to an empty array (like clearcollDevices());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDevices($overrideExisting = true)
    {
        if (null !== $this->collDevices && !$overrideExisting) {
            return;
        }

        $collectionClassName = DeviceTableMap::getTableMap()->getCollectionClassName();

        $this->collDevices = new $collectionClassName;
        $this->collDevices->setModel('\Database\HubPlus\Device');
    }

    /**
     * Gets an array of ChildDevice objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUserApp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDevice[] List of ChildDevice objects
     * @throws PropelException
     */
    public function getDevices(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collDevicesPartial && !$this->isNew();
        if (null === $this->collDevices || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDevices) {
                // return empty collection
                $this->initDevices();
            } else {
                $collDevices = ChildDeviceQuery::create(null, $criteria)
                    ->filterByUserApp($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDevicesPartial && count($collDevices)) {
                        $this->initDevices(false);

                        foreach ($collDevices as $obj) {
                            if (false == $this->collDevices->contains($obj)) {
                                $this->collDevices->append($obj);
                            }
                        }

                        $this->collDevicesPartial = true;
                    }

                    return $collDevices;
                }

                if ($partial && $this->collDevices) {
                    foreach ($this->collDevices as $obj) {
                        if ($obj->isNew()) {
                            $collDevices[] = $obj;
                        }
                    }
                }

                $this->collDevices = $collDevices;
                $this->collDevicesPartial = false;
            }
        }

        return $this->collDevices;
    }

    /**
     * Sets a collection of ChildDevice objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $devices A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUserApp The current object (for fluent API support)
     */
    public function setDevices(Collection $devices, ConnectionInterface $con = null)
    {
        /** @var ChildDevice[] $devicesToDelete */
        $devicesToDelete = $this->getDevices(new Criteria(), $con)->diff($devices);


        $this->devicesScheduledForDeletion = $devicesToDelete;

        foreach ($devicesToDelete as $deviceRemoved) {
            $deviceRemoved->setUserApp(null);
        }

        $this->collDevices = null;
        foreach ($devices as $device) {
            $this->addDevice($device);
        }

        $this->collDevices = $devices;
        $this->collDevicesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Device objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Device objects.
     * @throws PropelException
     */
    public function countDevices(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collDevicesPartial && !$this->isNew();
        if (null === $this->collDevices || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDevices) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDevices());
            }

            $query = ChildDeviceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUserApp($this)
                ->count($con);
        }

        return count($this->collDevices);
    }

    /**
     * Method called to associate a ChildDevice object to this object
     * through the ChildDevice foreign key attribute.
     *
     * @param  ChildDevice $l ChildDevice
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function addDevice(ChildDevice $l)
    {
        if ($this->collDevices === null) {
            $this->initDevices();
            $this->collDevicesPartial = true;
        }

        if (!$this->collDevices->contains($l)) {
            $this->doAddDevice($l);

            if ($this->devicesScheduledForDeletion and $this->devicesScheduledForDeletion->contains($l)) {
                $this->devicesScheduledForDeletion->remove($this->devicesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDevice $device The ChildDevice object to add.
     */
    protected function doAddDevice(ChildDevice $device)
    {
        $this->collDevices[]= $device;
        $device->setUserApp($this);
    }

    /**
     * @param  ChildDevice $device The ChildDevice object to remove.
     * @return $this|ChildUserApp The current object (for fluent API support)
     */
    public function removeDevice(ChildDevice $device)
    {
        if ($this->getDevices()->contains($device)) {
            $pos = $this->collDevices->search($device);
            $this->collDevices->remove($pos);
            if (null === $this->devicesScheduledForDeletion) {
                $this->devicesScheduledForDeletion = clone $this->collDevices;
                $this->devicesScheduledForDeletion->clear();
            }
            $this->devicesScheduledForDeletion[]= $device;
            $device->setUserApp(null);
        }

        return $this;
    }

    /**
     * Clears out the collMediaLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMediaLogs()
     */
    public function clearMediaLogs()
    {
        $this->collMediaLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMediaLogs collection loaded partially.
     */
    public function resetPartialMediaLogs($v = true)
    {
        $this->collMediaLogsPartial = $v;
    }

    /**
     * Initializes the collMediaLogs collection.
     *
     * By default this just sets the collMediaLogs collection to an empty array (like clearcollMediaLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMediaLogs($overrideExisting = true)
    {
        if (null !== $this->collMediaLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = MediaLogTableMap::getTableMap()->getCollectionClassName();

        $this->collMediaLogs = new $collectionClassName;
        $this->collMediaLogs->setModel('\Database\HubPlus\MediaLog');
    }

    /**
     * Gets an array of ChildMediaLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUserApp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMediaLog[] List of ChildMediaLog objects
     * @throws PropelException
     */
    public function getMediaLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMediaLogsPartial && !$this->isNew();
        if (null === $this->collMediaLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMediaLogs) {
                // return empty collection
                $this->initMediaLogs();
            } else {
                $collMediaLogs = ChildMediaLogQuery::create(null, $criteria)
                    ->filterByUserApp($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMediaLogsPartial && count($collMediaLogs)) {
                        $this->initMediaLogs(false);

                        foreach ($collMediaLogs as $obj) {
                            if (false == $this->collMediaLogs->contains($obj)) {
                                $this->collMediaLogs->append($obj);
                            }
                        }

                        $this->collMediaLogsPartial = true;
                    }

                    return $collMediaLogs;
                }

                if ($partial && $this->collMediaLogs) {
                    foreach ($this->collMediaLogs as $obj) {
                        if ($obj->isNew()) {
                            $collMediaLogs[] = $obj;
                        }
                    }
                }

                $this->collMediaLogs = $collMediaLogs;
                $this->collMediaLogsPartial = false;
            }
        }

        return $this->collMediaLogs;
    }

    /**
     * Sets a collection of ChildMediaLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $mediaLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUserApp The current object (for fluent API support)
     */
    public function setMediaLogs(Collection $mediaLogs, ConnectionInterface $con = null)
    {
        /** @var ChildMediaLog[] $mediaLogsToDelete */
        $mediaLogsToDelete = $this->getMediaLogs(new Criteria(), $con)->diff($mediaLogs);


        $this->mediaLogsScheduledForDeletion = $mediaLogsToDelete;

        foreach ($mediaLogsToDelete as $mediaLogRemoved) {
            $mediaLogRemoved->setUserApp(null);
        }

        $this->collMediaLogs = null;
        foreach ($mediaLogs as $mediaLog) {
            $this->addMediaLog($mediaLog);
        }

        $this->collMediaLogs = $mediaLogs;
        $this->collMediaLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MediaLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MediaLog objects.
     * @throws PropelException
     */
    public function countMediaLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMediaLogsPartial && !$this->isNew();
        if (null === $this->collMediaLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMediaLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMediaLogs());
            }

            $query = ChildMediaLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUserApp($this)
                ->count($con);
        }

        return count($this->collMediaLogs);
    }

    /**
     * Method called to associate a ChildMediaLog object to this object
     * through the ChildMediaLog foreign key attribute.
     *
     * @param  ChildMediaLog $l ChildMediaLog
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function addMediaLog(ChildMediaLog $l)
    {
        if ($this->collMediaLogs === null) {
            $this->initMediaLogs();
            $this->collMediaLogsPartial = true;
        }

        if (!$this->collMediaLogs->contains($l)) {
            $this->doAddMediaLog($l);

            if ($this->mediaLogsScheduledForDeletion and $this->mediaLogsScheduledForDeletion->contains($l)) {
                $this->mediaLogsScheduledForDeletion->remove($this->mediaLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMediaLog $mediaLog The ChildMediaLog object to add.
     */
    protected function doAddMediaLog(ChildMediaLog $mediaLog)
    {
        $this->collMediaLogs[]= $mediaLog;
        $mediaLog->setUserApp($this);
    }

    /**
     * @param  ChildMediaLog $mediaLog The ChildMediaLog object to remove.
     * @return $this|ChildUserApp The current object (for fluent API support)
     */
    public function removeMediaLog(ChildMediaLog $mediaLog)
    {
        if ($this->getMediaLogs()->contains($mediaLog)) {
            $pos = $this->collMediaLogs->search($mediaLog);
            $this->collMediaLogs->remove($pos);
            if (null === $this->mediaLogsScheduledForDeletion) {
                $this->mediaLogsScheduledForDeletion = clone $this->collMediaLogs;
                $this->mediaLogsScheduledForDeletion->clear();
            }
            $this->mediaLogsScheduledForDeletion[]= $mediaLog;
            $mediaLog->setUserApp(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserApp is new, it will return
     * an empty collection; or if this UserApp has previously
     * been saved, it will retrieve related MediaLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserApp.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMediaLog[] List of ChildMediaLog objects
     */
    public function getMediaLogsJoinMedia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMediaLogQuery::create(null, $criteria);
        $query->joinWith('Media', $joinBehavior);

        return $this->getMediaLogs($query, $con);
    }

    /**
     * Clears out the collPostLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostLogs()
     */
    public function clearPostLogs()
    {
        $this->collPostLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostLogs collection loaded partially.
     */
    public function resetPartialPostLogs($v = true)
    {
        $this->collPostLogsPartial = $v;
    }

    /**
     * Initializes the collPostLogs collection.
     *
     * By default this just sets the collPostLogs collection to an empty array (like clearcollPostLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostLogs($overrideExisting = true)
    {
        if (null !== $this->collPostLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostLogTableMap::getTableMap()->getCollectionClassName();

        $this->collPostLogs = new $collectionClassName;
        $this->collPostLogs->setModel('\Database\HubPlus\PostLog');
    }

    /**
     * Gets an array of ChildPostLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUserApp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostLog[] List of ChildPostLog objects
     * @throws PropelException
     */
    public function getPostLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostLogsPartial && !$this->isNew();
        if (null === $this->collPostLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostLogs) {
                // return empty collection
                $this->initPostLogs();
            } else {
                $collPostLogs = ChildPostLogQuery::create(null, $criteria)
                    ->filterByUserApp($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostLogsPartial && count($collPostLogs)) {
                        $this->initPostLogs(false);

                        foreach ($collPostLogs as $obj) {
                            if (false == $this->collPostLogs->contains($obj)) {
                                $this->collPostLogs->append($obj);
                            }
                        }

                        $this->collPostLogsPartial = true;
                    }

                    return $collPostLogs;
                }

                if ($partial && $this->collPostLogs) {
                    foreach ($this->collPostLogs as $obj) {
                        if ($obj->isNew()) {
                            $collPostLogs[] = $obj;
                        }
                    }
                }

                $this->collPostLogs = $collPostLogs;
                $this->collPostLogsPartial = false;
            }
        }

        return $this->collPostLogs;
    }

    /**
     * Sets a collection of ChildPostLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUserApp The current object (for fluent API support)
     */
    public function setPostLogs(Collection $postLogs, ConnectionInterface $con = null)
    {
        /** @var ChildPostLog[] $postLogsToDelete */
        $postLogsToDelete = $this->getPostLogs(new Criteria(), $con)->diff($postLogs);


        $this->postLogsScheduledForDeletion = $postLogsToDelete;

        foreach ($postLogsToDelete as $postLogRemoved) {
            $postLogRemoved->setUserApp(null);
        }

        $this->collPostLogs = null;
        foreach ($postLogs as $postLog) {
            $this->addPostLog($postLog);
        }

        $this->collPostLogs = $postLogs;
        $this->collPostLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostLog objects.
     * @throws PropelException
     */
    public function countPostLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostLogsPartial && !$this->isNew();
        if (null === $this->collPostLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostLogs());
            }

            $query = ChildPostLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUserApp($this)
                ->count($con);
        }

        return count($this->collPostLogs);
    }

    /**
     * Method called to associate a ChildPostLog object to this object
     * through the ChildPostLog foreign key attribute.
     *
     * @param  ChildPostLog $l ChildPostLog
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function addPostLog(ChildPostLog $l)
    {
        if ($this->collPostLogs === null) {
            $this->initPostLogs();
            $this->collPostLogsPartial = true;
        }

        if (!$this->collPostLogs->contains($l)) {
            $this->doAddPostLog($l);

            if ($this->postLogsScheduledForDeletion and $this->postLogsScheduledForDeletion->contains($l)) {
                $this->postLogsScheduledForDeletion->remove($this->postLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostLog $postLog The ChildPostLog object to add.
     */
    protected function doAddPostLog(ChildPostLog $postLog)
    {
        $this->collPostLogs[]= $postLog;
        $postLog->setUserApp($this);
    }

    /**
     * @param  ChildPostLog $postLog The ChildPostLog object to remove.
     * @return $this|ChildUserApp The current object (for fluent API support)
     */
    public function removePostLog(ChildPostLog $postLog)
    {
        if ($this->getPostLogs()->contains($postLog)) {
            $pos = $this->collPostLogs->search($postLog);
            $this->collPostLogs->remove($pos);
            if (null === $this->postLogsScheduledForDeletion) {
                $this->postLogsScheduledForDeletion = clone $this->collPostLogs;
                $this->postLogsScheduledForDeletion->clear();
            }
            $this->postLogsScheduledForDeletion[]= $postLog;
            $postLog->setUserApp(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserApp is new, it will return
     * an empty collection; or if this UserApp has previously
     * been saved, it will retrieve related PostLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserApp.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostLog[] List of ChildPostLog objects
     */
    public function getPostLogsJoinPost(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostLogQuery::create(null, $criteria);
        $query->joinWith('Post', $joinBehavior);

        return $this->getPostLogs($query, $con);
    }

    /**
     * Clears out the collRemoteLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRemoteLogs()
     */
    public function clearRemoteLogs()
    {
        $this->collRemoteLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRemoteLogs collection loaded partially.
     */
    public function resetPartialRemoteLogs($v = true)
    {
        $this->collRemoteLogsPartial = $v;
    }

    /**
     * Initializes the collRemoteLogs collection.
     *
     * By default this just sets the collRemoteLogs collection to an empty array (like clearcollRemoteLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRemoteLogs($overrideExisting = true)
    {
        if (null !== $this->collRemoteLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = RemoteLogTableMap::getTableMap()->getCollectionClassName();

        $this->collRemoteLogs = new $collectionClassName;
        $this->collRemoteLogs->setModel('\Database\HubPlus\RemoteLog');
    }

    /**
     * Gets an array of ChildRemoteLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUserApp is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildRemoteLog[] List of ChildRemoteLog objects
     * @throws PropelException
     */
    public function getRemoteLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRemoteLogsPartial && !$this->isNew();
        if (null === $this->collRemoteLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRemoteLogs) {
                // return empty collection
                $this->initRemoteLogs();
            } else {
                $collRemoteLogs = ChildRemoteLogQuery::create(null, $criteria)
                    ->filterByUserApp($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRemoteLogsPartial && count($collRemoteLogs)) {
                        $this->initRemoteLogs(false);

                        foreach ($collRemoteLogs as $obj) {
                            if (false == $this->collRemoteLogs->contains($obj)) {
                                $this->collRemoteLogs->append($obj);
                            }
                        }

                        $this->collRemoteLogsPartial = true;
                    }

                    return $collRemoteLogs;
                }

                if ($partial && $this->collRemoteLogs) {
                    foreach ($this->collRemoteLogs as $obj) {
                        if ($obj->isNew()) {
                            $collRemoteLogs[] = $obj;
                        }
                    }
                }

                $this->collRemoteLogs = $collRemoteLogs;
                $this->collRemoteLogsPartial = false;
            }
        }

        return $this->collRemoteLogs;
    }

    /**
     * Sets a collection of ChildRemoteLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $remoteLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUserApp The current object (for fluent API support)
     */
    public function setRemoteLogs(Collection $remoteLogs, ConnectionInterface $con = null)
    {
        /** @var ChildRemoteLog[] $remoteLogsToDelete */
        $remoteLogsToDelete = $this->getRemoteLogs(new Criteria(), $con)->diff($remoteLogs);


        $this->remoteLogsScheduledForDeletion = $remoteLogsToDelete;

        foreach ($remoteLogsToDelete as $remoteLogRemoved) {
            $remoteLogRemoved->setUserApp(null);
        }

        $this->collRemoteLogs = null;
        foreach ($remoteLogs as $remoteLog) {
            $this->addRemoteLog($remoteLog);
        }

        $this->collRemoteLogs = $remoteLogs;
        $this->collRemoteLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RemoteLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related RemoteLog objects.
     * @throws PropelException
     */
    public function countRemoteLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRemoteLogsPartial && !$this->isNew();
        if (null === $this->collRemoteLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRemoteLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRemoteLogs());
            }

            $query = ChildRemoteLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUserApp($this)
                ->count($con);
        }

        return count($this->collRemoteLogs);
    }

    /**
     * Method called to associate a ChildRemoteLog object to this object
     * through the ChildRemoteLog foreign key attribute.
     *
     * @param  ChildRemoteLog $l ChildRemoteLog
     * @return $this|\Database\HubPlus\UserApp The current object (for fluent API support)
     */
    public function addRemoteLog(ChildRemoteLog $l)
    {
        if ($this->collRemoteLogs === null) {
            $this->initRemoteLogs();
            $this->collRemoteLogsPartial = true;
        }

        if (!$this->collRemoteLogs->contains($l)) {
            $this->doAddRemoteLog($l);

            if ($this->remoteLogsScheduledForDeletion and $this->remoteLogsScheduledForDeletion->contains($l)) {
                $this->remoteLogsScheduledForDeletion->remove($this->remoteLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildRemoteLog $remoteLog The ChildRemoteLog object to add.
     */
    protected function doAddRemoteLog(ChildRemoteLog $remoteLog)
    {
        $this->collRemoteLogs[]= $remoteLog;
        $remoteLog->setUserApp($this);
    }

    /**
     * @param  ChildRemoteLog $remoteLog The ChildRemoteLog object to remove.
     * @return $this|ChildUserApp The current object (for fluent API support)
     */
    public function removeRemoteLog(ChildRemoteLog $remoteLog)
    {
        if ($this->getRemoteLogs()->contains($remoteLog)) {
            $pos = $this->collRemoteLogs->search($remoteLog);
            $this->collRemoteLogs->remove($pos);
            if (null === $this->remoteLogsScheduledForDeletion) {
                $this->remoteLogsScheduledForDeletion = clone $this->collRemoteLogs;
                $this->remoteLogsScheduledForDeletion->clear();
            }
            $this->remoteLogsScheduledForDeletion[]= $remoteLog;
            $remoteLog->setUserApp(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UserApp is new, it will return
     * an empty collection; or if this UserApp has previously
     * been saved, it will retrieve related RemoteLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UserApp.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildRemoteLog[] List of ChildRemoteLog objects
     */
    public function getRemoteLogsJoinSectionConnector(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildRemoteLogQuery::create(null, $criteria);
        $query->joinWith('SectionConnector', $joinBehavior);

        return $this->getRemoteLogs($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aAddress) {
            $this->aAddress->removeUserApp($this);
        }
        if (null !== $this->aSection) {
            $this->aSection->removeUserApp($this);
        }
        $this->id = null;
        $this->username = null;
        $this->username_canonical = null;
        $this->email = null;
        $this->email_canonical = null;
        $this->enabled = null;
        $this->privacy = null;
        $this->terms = null;
        $this->salt = null;
        $this->password = null;
        $this->last_login = null;
        $this->locked = null;
        $this->expired = null;
        $this->expires_at = null;
        $this->confirmation_token = null;
        $this->password_requested_at = null;
        $this->roles = null;
        $this->credentials_expired = null;
        $this->credentials_expire_at = null;
        $this->firstname = null;
        $this->lastname = null;
        $this->phone = null;
        $this->imagepath = null;
        $this->address_id = null;
        $this->tags = null;
        $this->section_id = null;
        $this->deleted_at = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCustomFormLogs) {
                foreach ($this->collCustomFormLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDevices) {
                foreach ($this->collDevices as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMediaLogs) {
                foreach ($this->collMediaLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPostLogs) {
                foreach ($this->collPostLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRemoteLogs) {
                foreach ($this->collRemoteLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCustomFormLogs = null;
        $this->collDevices = null;
        $this->collMediaLogs = null;
        $this->collPostLogs = null;
        $this->collRemoteLogs = null;
        $this->aAddress = null;
        $this->aSection = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UserAppTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildUserApp The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[UserAppTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildUserAppArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildUserAppArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildUserAppArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildUserAppArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildUserApp The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildUserAppArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildUserApp The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setId($archive->getId());
        }
        $this->setUsername($archive->getUsername());
        $this->setUsernameCanonical($archive->getUsernameCanonical());
        $this->setEmail($archive->getEmail());
        $this->setEmailCanonical($archive->getEmailCanonical());
        $this->setEnabled($archive->getEnabled());
        $this->setPrivacy($archive->getPrivacy());
        $this->setTerms($archive->getTerms());
        $this->setSalt($archive->getSalt());
        $this->setPassword($archive->getPassword());
        $this->setLastLogin($archive->getLastLogin());
        $this->setLocked($archive->getLocked());
        $this->setExpired($archive->getExpired());
        $this->setExpiresAt($archive->getExpiresAt());
        $this->setConfirmationToken($archive->getConfirmationToken());
        $this->setPasswordRequestedAt($archive->getPasswordRequestedAt());
        $this->setRoles($archive->getRoles());
        $this->setCredentialsExpired($archive->getCredentialsExpired());
        $this->setCredentialsExpireAt($archive->getCredentialsExpireAt());
        $this->setFirstname($archive->getFirstname());
        $this->setLastname($archive->getLastname());
        $this->setPhone($archive->getPhone());
        $this->setImagepath($archive->getImagepath());
        $this->setAddressId($archive->getAddressId());
        $this->setTags($archive->getTags());
        $this->setSectionId($archive->getSectionId());
        $this->setDeletedAt($archive->getDeletedAt());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildUserApp The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
