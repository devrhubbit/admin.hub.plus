<?php

namespace Database\HubPlus\Base;

use \DateTime;
use \Exception;
use \PDO;
use Database\HubPlus\Webhook as ChildWebhook;
use Database\HubPlus\WebhookQuery as ChildWebhookQuery;
use Database\HubPlus\WebhookQueue as ChildWebhookQueue;
use Database\HubPlus\WebhookQueueQuery as ChildWebhookQueueQuery;
use Database\HubPlus\Map\WebhookQueueTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'webhook_queue' table.
 *
 *
 *
 * @package    propel.generator.Database.HubPlus.Base
 */
abstract class WebhookQueue implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Database\\HubPlus\\Map\\WebhookQueueTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the webhook_id field.
     *
     * @var        int
     */
    protected $webhook_id;

    /**
     * The value for the status field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $status;

    /**
     * The value for the failed_attempts field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $failed_attempts;

    /**
     * The value for the max_attempts field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $max_attempts;

    /**
     * The value for the method field.
     *
     * Note: this column has a database default value of: 'POST'
     * @var        string
     */
    protected $method;

    /**
     * The value for the url field.
     *
     * @var        string
     */
    protected $url;

    /**
     * The value for the bundle field.
     *
     * @var        string
     */
    protected $bundle;

    /**
     * The value for the payload field.
     *
     * @var        string
     */
    protected $payload;

    /**
     * The value for the response_status field.
     *
     * @var        int
     */
    protected $response_status;

    /**
     * The value for the response_log field.
     *
     * @var        string
     */
    protected $response_log;

    /**
     * The value for the to_sync_at field.
     *
     * @var        DateTime
     */
    protected $to_sync_at;

    /**
     * The value for the synced_at field.
     *
     * @var        DateTime
     */
    protected $synced_at;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildWebhook
     */
    protected $aWebhook;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->status = 0;
        $this->failed_attempts = 0;
        $this->max_attempts = 1;
        $this->method = 'POST';
    }

    /**
     * Initializes internal state of Database\HubPlus\Base\WebhookQueue object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>WebhookQueue</code> instance.  If
     * <code>obj</code> is an instance of <code>WebhookQueue</code>, delegates to
     * <code>equals(WebhookQueue)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|WebhookQueue The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [webhook_id] column value.
     *
     * @return int
     */
    public function getWebhookId()
    {
        return $this->webhook_id;
    }

    /**
     * Get the [status] column value.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [failed_attempts] column value.
     *
     * @return int
     */
    public function getFailedAttempts()
    {
        return $this->failed_attempts;
    }

    /**
     * Get the [max_attempts] column value.
     *
     * @return int
     */
    public function getMaxAttempts()
    {
        return $this->max_attempts;
    }

    /**
     * Get the [method] column value.
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Get the [url] column value.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the [bundle] column value.
     *
     * @return string
     */
    public function getBundle()
    {
        return $this->bundle;
    }

    /**
     * Get the [payload] column value.
     *
     * @return string
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * Get the [response_status] column value.
     *
     * @return int
     */
    public function getResponseStatus()
    {
        return $this->response_status;
    }

    /**
     * Get the [response_log] column value.
     *
     * @return string
     */
    public function getResponseLog()
    {
        return $this->response_log;
    }

    /**
     * Get the [optionally formatted] temporal [to_sync_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getToSyncAt($format = NULL)
    {
        if ($format === null) {
            return $this->to_sync_at;
        } else {
            return $this->to_sync_at instanceof \DateTimeInterface ? $this->to_sync_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [synced_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getSyncedAt($format = NULL)
    {
        if ($format === null) {
            return $this->synced_at;
        } else {
            return $this->synced_at instanceof \DateTimeInterface ? $this->synced_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[WebhookQueueTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [webhook_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setWebhookId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->webhook_id !== $v) {
            $this->webhook_id = $v;
            $this->modifiedColumns[WebhookQueueTableMap::COL_WEBHOOK_ID] = true;
        }

        if ($this->aWebhook !== null && $this->aWebhook->getId() !== $v) {
            $this->aWebhook = null;
        }

        return $this;
    } // setWebhookId()

    /**
     * Set the value of [status] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[WebhookQueueTableMap::COL_STATUS] = true;
        }

        return $this;
    } // setStatus()

    /**
     * Set the value of [failed_attempts] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setFailedAttempts($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->failed_attempts !== $v) {
            $this->failed_attempts = $v;
            $this->modifiedColumns[WebhookQueueTableMap::COL_FAILED_ATTEMPTS] = true;
        }

        return $this;
    } // setFailedAttempts()

    /**
     * Set the value of [max_attempts] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setMaxAttempts($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->max_attempts !== $v) {
            $this->max_attempts = $v;
            $this->modifiedColumns[WebhookQueueTableMap::COL_MAX_ATTEMPTS] = true;
        }

        return $this;
    } // setMaxAttempts()

    /**
     * Set the value of [method] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setMethod($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->method !== $v) {
            $this->method = $v;
            $this->modifiedColumns[WebhookQueueTableMap::COL_METHOD] = true;
        }

        return $this;
    } // setMethod()

    /**
     * Set the value of [url] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[WebhookQueueTableMap::COL_URL] = true;
        }

        return $this;
    } // setUrl()

    /**
     * Set the value of [bundle] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setBundle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bundle !== $v) {
            $this->bundle = $v;
            $this->modifiedColumns[WebhookQueueTableMap::COL_BUNDLE] = true;
        }

        return $this;
    } // setBundle()

    /**
     * Set the value of [payload] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setPayload($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->payload !== $v) {
            $this->payload = $v;
            $this->modifiedColumns[WebhookQueueTableMap::COL_PAYLOAD] = true;
        }

        return $this;
    } // setPayload()

    /**
     * Set the value of [response_status] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setResponseStatus($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->response_status !== $v) {
            $this->response_status = $v;
            $this->modifiedColumns[WebhookQueueTableMap::COL_RESPONSE_STATUS] = true;
        }

        return $this;
    } // setResponseStatus()

    /**
     * Set the value of [response_log] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setResponseLog($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->response_log !== $v) {
            $this->response_log = $v;
            $this->modifiedColumns[WebhookQueueTableMap::COL_RESPONSE_LOG] = true;
        }

        return $this;
    } // setResponseLog()

    /**
     * Sets the value of [to_sync_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setToSyncAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->to_sync_at !== null || $dt !== null) {
            if ($this->to_sync_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->to_sync_at->format("Y-m-d H:i:s.u")) {
                $this->to_sync_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[WebhookQueueTableMap::COL_TO_SYNC_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setToSyncAt()

    /**
     * Sets the value of [synced_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setSyncedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->synced_at !== null || $dt !== null) {
            if ($this->synced_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->synced_at->format("Y-m-d H:i:s.u")) {
                $this->synced_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[WebhookQueueTableMap::COL_SYNCED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setSyncedAt()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[WebhookQueueTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[WebhookQueueTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->status !== 0) {
                return false;
            }

            if ($this->failed_attempts !== 0) {
                return false;
            }

            if ($this->max_attempts !== 1) {
                return false;
            }

            if ($this->method !== 'POST') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : WebhookQueueTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : WebhookQueueTableMap::translateFieldName('WebhookId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->webhook_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : WebhookQueueTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : WebhookQueueTableMap::translateFieldName('FailedAttempts', TableMap::TYPE_PHPNAME, $indexType)];
            $this->failed_attempts = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : WebhookQueueTableMap::translateFieldName('MaxAttempts', TableMap::TYPE_PHPNAME, $indexType)];
            $this->max_attempts = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : WebhookQueueTableMap::translateFieldName('Method', TableMap::TYPE_PHPNAME, $indexType)];
            $this->method = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : WebhookQueueTableMap::translateFieldName('Url', TableMap::TYPE_PHPNAME, $indexType)];
            $this->url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : WebhookQueueTableMap::translateFieldName('Bundle', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bundle = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : WebhookQueueTableMap::translateFieldName('Payload', TableMap::TYPE_PHPNAME, $indexType)];
            $this->payload = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : WebhookQueueTableMap::translateFieldName('ResponseStatus', TableMap::TYPE_PHPNAME, $indexType)];
            $this->response_status = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : WebhookQueueTableMap::translateFieldName('ResponseLog', TableMap::TYPE_PHPNAME, $indexType)];
            $this->response_log = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : WebhookQueueTableMap::translateFieldName('ToSyncAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->to_sync_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : WebhookQueueTableMap::translateFieldName('SyncedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->synced_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : WebhookQueueTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : WebhookQueueTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 15; // 15 = WebhookQueueTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Database\\HubPlus\\WebhookQueue'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aWebhook !== null && $this->webhook_id !== $this->aWebhook->getId()) {
            $this->aWebhook = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(WebhookQueueTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildWebhookQueueQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aWebhook = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see WebhookQueue::setDeleted()
     * @see WebhookQueue::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(WebhookQueueTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildWebhookQueueQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(WebhookQueueTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(WebhookQueueTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(WebhookQueueTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(WebhookQueueTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                WebhookQueueTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aWebhook !== null) {
                if ($this->aWebhook->isModified() || $this->aWebhook->isNew()) {
                    $affectedRows += $this->aWebhook->save($con);
                }
                $this->setWebhook($this->aWebhook);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[WebhookQueueTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . WebhookQueueTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(WebhookQueueTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_WEBHOOK_ID)) {
            $modifiedColumns[':p' . $index++]  = 'webhook_id';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'status';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_FAILED_ATTEMPTS)) {
            $modifiedColumns[':p' . $index++]  = 'failed_attempts';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_MAX_ATTEMPTS)) {
            $modifiedColumns[':p' . $index++]  = 'max_attempts';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_METHOD)) {
            $modifiedColumns[':p' . $index++]  = 'method';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'url';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_BUNDLE)) {
            $modifiedColumns[':p' . $index++]  = 'bundle';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_PAYLOAD)) {
            $modifiedColumns[':p' . $index++]  = 'payload';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_RESPONSE_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'response_status';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_RESPONSE_LOG)) {
            $modifiedColumns[':p' . $index++]  = 'response_log';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_TO_SYNC_AT)) {
            $modifiedColumns[':p' . $index++]  = 'to_sync_at';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_SYNCED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'synced_at';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO webhook_queue (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'webhook_id':
                        $stmt->bindValue($identifier, $this->webhook_id, PDO::PARAM_INT);
                        break;
                    case 'status':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_INT);
                        break;
                    case 'failed_attempts':
                        $stmt->bindValue($identifier, $this->failed_attempts, PDO::PARAM_INT);
                        break;
                    case 'max_attempts':
                        $stmt->bindValue($identifier, $this->max_attempts, PDO::PARAM_INT);
                        break;
                    case 'method':
                        $stmt->bindValue($identifier, $this->method, PDO::PARAM_STR);
                        break;
                    case 'url':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case 'bundle':
                        $stmt->bindValue($identifier, $this->bundle, PDO::PARAM_STR);
                        break;
                    case 'payload':
                        $stmt->bindValue($identifier, $this->payload, PDO::PARAM_STR);
                        break;
                    case 'response_status':
                        $stmt->bindValue($identifier, $this->response_status, PDO::PARAM_INT);
                        break;
                    case 'response_log':
                        $stmt->bindValue($identifier, $this->response_log, PDO::PARAM_STR);
                        break;
                    case 'to_sync_at':
                        $stmt->bindValue($identifier, $this->to_sync_at ? $this->to_sync_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'synced_at':
                        $stmt->bindValue($identifier, $this->synced_at ? $this->synced_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = WebhookQueueTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getWebhookId();
                break;
            case 2:
                return $this->getStatus();
                break;
            case 3:
                return $this->getFailedAttempts();
                break;
            case 4:
                return $this->getMaxAttempts();
                break;
            case 5:
                return $this->getMethod();
                break;
            case 6:
                return $this->getUrl();
                break;
            case 7:
                return $this->getBundle();
                break;
            case 8:
                return $this->getPayload();
                break;
            case 9:
                return $this->getResponseStatus();
                break;
            case 10:
                return $this->getResponseLog();
                break;
            case 11:
                return $this->getToSyncAt();
                break;
            case 12:
                return $this->getSyncedAt();
                break;
            case 13:
                return $this->getCreatedAt();
                break;
            case 14:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['WebhookQueue'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['WebhookQueue'][$this->hashCode()] = true;
        $keys = WebhookQueueTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getWebhookId(),
            $keys[2] => $this->getStatus(),
            $keys[3] => $this->getFailedAttempts(),
            $keys[4] => $this->getMaxAttempts(),
            $keys[5] => $this->getMethod(),
            $keys[6] => $this->getUrl(),
            $keys[7] => $this->getBundle(),
            $keys[8] => $this->getPayload(),
            $keys[9] => $this->getResponseStatus(),
            $keys[10] => $this->getResponseLog(),
            $keys[11] => $this->getToSyncAt(),
            $keys[12] => $this->getSyncedAt(),
            $keys[13] => $this->getCreatedAt(),
            $keys[14] => $this->getUpdatedAt(),
        );
        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        if ($result[$keys[12]] instanceof \DateTimeInterface) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTimeInterface) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        if ($result[$keys[14]] instanceof \DateTimeInterface) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aWebhook) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'webhook';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'webhook';
                        break;
                    default:
                        $key = 'Webhook';
                }

                $result[$key] = $this->aWebhook->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Database\HubPlus\WebhookQueue
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = WebhookQueueTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Database\HubPlus\WebhookQueue
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setWebhookId($value);
                break;
            case 2:
                $this->setStatus($value);
                break;
            case 3:
                $this->setFailedAttempts($value);
                break;
            case 4:
                $this->setMaxAttempts($value);
                break;
            case 5:
                $this->setMethod($value);
                break;
            case 6:
                $this->setUrl($value);
                break;
            case 7:
                $this->setBundle($value);
                break;
            case 8:
                $this->setPayload($value);
                break;
            case 9:
                $this->setResponseStatus($value);
                break;
            case 10:
                $this->setResponseLog($value);
                break;
            case 11:
                $this->setToSyncAt($value);
                break;
            case 12:
                $this->setSyncedAt($value);
                break;
            case 13:
                $this->setCreatedAt($value);
                break;
            case 14:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = WebhookQueueTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setWebhookId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setStatus($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setFailedAttempts($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setMaxAttempts($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setMethod($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUrl($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setBundle($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setPayload($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setResponseStatus($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setResponseLog($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setToSyncAt($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setSyncedAt($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setCreatedAt($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setUpdatedAt($arr[$keys[14]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Database\HubPlus\WebhookQueue The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(WebhookQueueTableMap::DATABASE_NAME);

        if ($this->isColumnModified(WebhookQueueTableMap::COL_ID)) {
            $criteria->add(WebhookQueueTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_WEBHOOK_ID)) {
            $criteria->add(WebhookQueueTableMap::COL_WEBHOOK_ID, $this->webhook_id);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_STATUS)) {
            $criteria->add(WebhookQueueTableMap::COL_STATUS, $this->status);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_FAILED_ATTEMPTS)) {
            $criteria->add(WebhookQueueTableMap::COL_FAILED_ATTEMPTS, $this->failed_attempts);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_MAX_ATTEMPTS)) {
            $criteria->add(WebhookQueueTableMap::COL_MAX_ATTEMPTS, $this->max_attempts);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_METHOD)) {
            $criteria->add(WebhookQueueTableMap::COL_METHOD, $this->method);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_URL)) {
            $criteria->add(WebhookQueueTableMap::COL_URL, $this->url);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_BUNDLE)) {
            $criteria->add(WebhookQueueTableMap::COL_BUNDLE, $this->bundle);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_PAYLOAD)) {
            $criteria->add(WebhookQueueTableMap::COL_PAYLOAD, $this->payload);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_RESPONSE_STATUS)) {
            $criteria->add(WebhookQueueTableMap::COL_RESPONSE_STATUS, $this->response_status);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_RESPONSE_LOG)) {
            $criteria->add(WebhookQueueTableMap::COL_RESPONSE_LOG, $this->response_log);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_TO_SYNC_AT)) {
            $criteria->add(WebhookQueueTableMap::COL_TO_SYNC_AT, $this->to_sync_at);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_SYNCED_AT)) {
            $criteria->add(WebhookQueueTableMap::COL_SYNCED_AT, $this->synced_at);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_CREATED_AT)) {
            $criteria->add(WebhookQueueTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(WebhookQueueTableMap::COL_UPDATED_AT)) {
            $criteria->add(WebhookQueueTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildWebhookQueueQuery::create();
        $criteria->add(WebhookQueueTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Database\HubPlus\WebhookQueue (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setWebhookId($this->getWebhookId());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setFailedAttempts($this->getFailedAttempts());
        $copyObj->setMaxAttempts($this->getMaxAttempts());
        $copyObj->setMethod($this->getMethod());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setBundle($this->getBundle());
        $copyObj->setPayload($this->getPayload());
        $copyObj->setResponseStatus($this->getResponseStatus());
        $copyObj->setResponseLog($this->getResponseLog());
        $copyObj->setToSyncAt($this->getToSyncAt());
        $copyObj->setSyncedAt($this->getSyncedAt());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Database\HubPlus\WebhookQueue Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildWebhook object.
     *
     * @param  ChildWebhook $v
     * @return $this|\Database\HubPlus\WebhookQueue The current object (for fluent API support)
     * @throws PropelException
     */
    public function setWebhook(ChildWebhook $v = null)
    {
        if ($v === null) {
            $this->setWebhookId(NULL);
        } else {
            $this->setWebhookId($v->getId());
        }

        $this->aWebhook = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildWebhook object, it will not be re-added.
        if ($v !== null) {
            $v->addWebhookQueue($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildWebhook object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildWebhook The associated ChildWebhook object.
     * @throws PropelException
     */
    public function getWebhook(ConnectionInterface $con = null)
    {
        if ($this->aWebhook === null && ($this->webhook_id != 0)) {
            $this->aWebhook = ChildWebhookQuery::create()->findPk($this->webhook_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aWebhook->addWebhookQueues($this);
             */
        }

        return $this->aWebhook;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aWebhook) {
            $this->aWebhook->removeWebhookQueue($this);
        }
        $this->id = null;
        $this->webhook_id = null;
        $this->status = null;
        $this->failed_attempts = null;
        $this->max_attempts = null;
        $this->method = null;
        $this->url = null;
        $this->bundle = null;
        $this->payload = null;
        $this->response_status = null;
        $this->response_log = null;
        $this->to_sync_at = null;
        $this->synced_at = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aWebhook = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(WebhookQueueTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildWebhookQueue The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[WebhookQueueTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
