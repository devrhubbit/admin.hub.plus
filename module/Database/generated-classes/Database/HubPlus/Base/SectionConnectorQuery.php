<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\SectionConnector as ChildSectionConnector;
use Database\HubPlus\SectionConnectorArchive as ChildSectionConnectorArchive;
use Database\HubPlus\SectionConnectorQuery as ChildSectionConnectorQuery;
use Database\HubPlus\Map\SectionConnectorTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'section_connector' table.
 *
 *
 *
 * @method     ChildSectionConnectorQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSectionConnectorQuery orderBySectionId($order = Criteria::ASC) Order by the section_id column
 * @method     ChildSectionConnectorQuery orderByProviderId($order = Criteria::ASC) Order by the provider_id column
 * @method     ChildSectionConnectorQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildSectionConnectorQuery orderByBaseurl($order = Criteria::ASC) Order by the baseurl column
 * @method     ChildSectionConnectorQuery orderByRemoteSectionId($order = Criteria::ASC) Order by the remote_section_id column
 * @method     ChildSectionConnectorQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildSectionConnectorQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildSectionConnectorQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildSectionConnectorQuery groupById() Group by the id column
 * @method     ChildSectionConnectorQuery groupBySectionId() Group by the section_id column
 * @method     ChildSectionConnectorQuery groupByProviderId() Group by the provider_id column
 * @method     ChildSectionConnectorQuery groupByType() Group by the type column
 * @method     ChildSectionConnectorQuery groupByBaseurl() Group by the baseurl column
 * @method     ChildSectionConnectorQuery groupByRemoteSectionId() Group by the remote_section_id column
 * @method     ChildSectionConnectorQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildSectionConnectorQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildSectionConnectorQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildSectionConnectorQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSectionConnectorQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSectionConnectorQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSectionConnectorQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSectionConnectorQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSectionConnectorQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSectionConnectorQuery leftJoinRemoteProvider($relationAlias = null) Adds a LEFT JOIN clause to the query using the RemoteProvider relation
 * @method     ChildSectionConnectorQuery rightJoinRemoteProvider($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RemoteProvider relation
 * @method     ChildSectionConnectorQuery innerJoinRemoteProvider($relationAlias = null) Adds a INNER JOIN clause to the query using the RemoteProvider relation
 *
 * @method     ChildSectionConnectorQuery joinWithRemoteProvider($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RemoteProvider relation
 *
 * @method     ChildSectionConnectorQuery leftJoinWithRemoteProvider() Adds a LEFT JOIN clause and with to the query using the RemoteProvider relation
 * @method     ChildSectionConnectorQuery rightJoinWithRemoteProvider() Adds a RIGHT JOIN clause and with to the query using the RemoteProvider relation
 * @method     ChildSectionConnectorQuery innerJoinWithRemoteProvider() Adds a INNER JOIN clause and with to the query using the RemoteProvider relation
 *
 * @method     ChildSectionConnectorQuery leftJoinSectionRelatedBySectionId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionRelatedBySectionId relation
 * @method     ChildSectionConnectorQuery rightJoinSectionRelatedBySectionId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionRelatedBySectionId relation
 * @method     ChildSectionConnectorQuery innerJoinSectionRelatedBySectionId($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionRelatedBySectionId relation
 *
 * @method     ChildSectionConnectorQuery joinWithSectionRelatedBySectionId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionRelatedBySectionId relation
 *
 * @method     ChildSectionConnectorQuery leftJoinWithSectionRelatedBySectionId() Adds a LEFT JOIN clause and with to the query using the SectionRelatedBySectionId relation
 * @method     ChildSectionConnectorQuery rightJoinWithSectionRelatedBySectionId() Adds a RIGHT JOIN clause and with to the query using the SectionRelatedBySectionId relation
 * @method     ChildSectionConnectorQuery innerJoinWithSectionRelatedBySectionId() Adds a INNER JOIN clause and with to the query using the SectionRelatedBySectionId relation
 *
 * @method     ChildSectionConnectorQuery leftJoinSectionRelatedByRemoteSectionId($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionRelatedByRemoteSectionId relation
 * @method     ChildSectionConnectorQuery rightJoinSectionRelatedByRemoteSectionId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionRelatedByRemoteSectionId relation
 * @method     ChildSectionConnectorQuery innerJoinSectionRelatedByRemoteSectionId($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionRelatedByRemoteSectionId relation
 *
 * @method     ChildSectionConnectorQuery joinWithSectionRelatedByRemoteSectionId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionRelatedByRemoteSectionId relation
 *
 * @method     ChildSectionConnectorQuery leftJoinWithSectionRelatedByRemoteSectionId() Adds a LEFT JOIN clause and with to the query using the SectionRelatedByRemoteSectionId relation
 * @method     ChildSectionConnectorQuery rightJoinWithSectionRelatedByRemoteSectionId() Adds a RIGHT JOIN clause and with to the query using the SectionRelatedByRemoteSectionId relation
 * @method     ChildSectionConnectorQuery innerJoinWithSectionRelatedByRemoteSectionId() Adds a INNER JOIN clause and with to the query using the SectionRelatedByRemoteSectionId relation
 *
 * @method     ChildSectionConnectorQuery leftJoinRemoteExtraLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the RemoteExtraLog relation
 * @method     ChildSectionConnectorQuery rightJoinRemoteExtraLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RemoteExtraLog relation
 * @method     ChildSectionConnectorQuery innerJoinRemoteExtraLog($relationAlias = null) Adds a INNER JOIN clause to the query using the RemoteExtraLog relation
 *
 * @method     ChildSectionConnectorQuery joinWithRemoteExtraLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RemoteExtraLog relation
 *
 * @method     ChildSectionConnectorQuery leftJoinWithRemoteExtraLog() Adds a LEFT JOIN clause and with to the query using the RemoteExtraLog relation
 * @method     ChildSectionConnectorQuery rightJoinWithRemoteExtraLog() Adds a RIGHT JOIN clause and with to the query using the RemoteExtraLog relation
 * @method     ChildSectionConnectorQuery innerJoinWithRemoteExtraLog() Adds a INNER JOIN clause and with to the query using the RemoteExtraLog relation
 *
 * @method     ChildSectionConnectorQuery leftJoinRemoteLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the RemoteLog relation
 * @method     ChildSectionConnectorQuery rightJoinRemoteLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RemoteLog relation
 * @method     ChildSectionConnectorQuery innerJoinRemoteLog($relationAlias = null) Adds a INNER JOIN clause to the query using the RemoteLog relation
 *
 * @method     ChildSectionConnectorQuery joinWithRemoteLog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RemoteLog relation
 *
 * @method     ChildSectionConnectorQuery leftJoinWithRemoteLog() Adds a LEFT JOIN clause and with to the query using the RemoteLog relation
 * @method     ChildSectionConnectorQuery rightJoinWithRemoteLog() Adds a RIGHT JOIN clause and with to the query using the RemoteLog relation
 * @method     ChildSectionConnectorQuery innerJoinWithRemoteLog() Adds a INNER JOIN clause and with to the query using the RemoteLog relation
 *
 * @method     \Database\HubPlus\RemoteProviderQuery|\Database\HubPlus\SectionQuery|\Database\HubPlus\RemoteExtraLogQuery|\Database\HubPlus\RemoteLogQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSectionConnector findOne(ConnectionInterface $con = null) Return the first ChildSectionConnector matching the query
 * @method     ChildSectionConnector findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSectionConnector matching the query, or a new ChildSectionConnector object populated from the query conditions when no match is found
 *
 * @method     ChildSectionConnector findOneById(int $id) Return the first ChildSectionConnector filtered by the id column
 * @method     ChildSectionConnector findOneBySectionId(int $section_id) Return the first ChildSectionConnector filtered by the section_id column
 * @method     ChildSectionConnector findOneByProviderId(int $provider_id) Return the first ChildSectionConnector filtered by the provider_id column
 * @method     ChildSectionConnector findOneByType(string $type) Return the first ChildSectionConnector filtered by the type column
 * @method     ChildSectionConnector findOneByBaseurl(string $baseurl) Return the first ChildSectionConnector filtered by the baseurl column
 * @method     ChildSectionConnector findOneByRemoteSectionId(int $remote_section_id) Return the first ChildSectionConnector filtered by the remote_section_id column
 * @method     ChildSectionConnector findOneByDeletedAt(string $deleted_at) Return the first ChildSectionConnector filtered by the deleted_at column
 * @method     ChildSectionConnector findOneByCreatedAt(string $created_at) Return the first ChildSectionConnector filtered by the created_at column
 * @method     ChildSectionConnector findOneByUpdatedAt(string $updated_at) Return the first ChildSectionConnector filtered by the updated_at column *

 * @method     ChildSectionConnector requirePk($key, ConnectionInterface $con = null) Return the ChildSectionConnector by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionConnector requireOne(ConnectionInterface $con = null) Return the first ChildSectionConnector matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSectionConnector requireOneById(int $id) Return the first ChildSectionConnector filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionConnector requireOneBySectionId(int $section_id) Return the first ChildSectionConnector filtered by the section_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionConnector requireOneByProviderId(int $provider_id) Return the first ChildSectionConnector filtered by the provider_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionConnector requireOneByType(string $type) Return the first ChildSectionConnector filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionConnector requireOneByBaseurl(string $baseurl) Return the first ChildSectionConnector filtered by the baseurl column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionConnector requireOneByRemoteSectionId(int $remote_section_id) Return the first ChildSectionConnector filtered by the remote_section_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionConnector requireOneByDeletedAt(string $deleted_at) Return the first ChildSectionConnector filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionConnector requireOneByCreatedAt(string $created_at) Return the first ChildSectionConnector filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSectionConnector requireOneByUpdatedAt(string $updated_at) Return the first ChildSectionConnector filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSectionConnector[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSectionConnector objects based on current ModelCriteria
 * @method     ChildSectionConnector[]|ObjectCollection findById(int $id) Return ChildSectionConnector objects filtered by the id column
 * @method     ChildSectionConnector[]|ObjectCollection findBySectionId(int $section_id) Return ChildSectionConnector objects filtered by the section_id column
 * @method     ChildSectionConnector[]|ObjectCollection findByProviderId(int $provider_id) Return ChildSectionConnector objects filtered by the provider_id column
 * @method     ChildSectionConnector[]|ObjectCollection findByType(string $type) Return ChildSectionConnector objects filtered by the type column
 * @method     ChildSectionConnector[]|ObjectCollection findByBaseurl(string $baseurl) Return ChildSectionConnector objects filtered by the baseurl column
 * @method     ChildSectionConnector[]|ObjectCollection findByRemoteSectionId(int $remote_section_id) Return ChildSectionConnector objects filtered by the remote_section_id column
 * @method     ChildSectionConnector[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildSectionConnector objects filtered by the deleted_at column
 * @method     ChildSectionConnector[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildSectionConnector objects filtered by the created_at column
 * @method     ChildSectionConnector[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildSectionConnector objects filtered by the updated_at column
 * @method     ChildSectionConnector[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SectionConnectorQuery extends ModelCriteria
{

    // archivable behavior
    protected $archiveOnDelete = true;
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\SectionConnectorQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\SectionConnector', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSectionConnectorQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSectionConnectorQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSectionConnectorQuery) {
            return $criteria;
        }
        $query = new ChildSectionConnectorQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSectionConnector|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SectionConnectorTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SectionConnectorTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSectionConnector A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, section_id, provider_id, type, baseurl, remote_section_id, deleted_at, created_at, updated_at FROM section_connector WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSectionConnector $obj */
            $obj = new ChildSectionConnector();
            $obj->hydrate($row);
            SectionConnectorTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSectionConnector|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SectionConnectorTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SectionConnectorTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionConnectorTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the section_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySectionId(1234); // WHERE section_id = 1234
     * $query->filterBySectionId(array(12, 34)); // WHERE section_id IN (12, 34)
     * $query->filterBySectionId(array('min' => 12)); // WHERE section_id > 12
     * </code>
     *
     * @see       filterBySectionRelatedBySectionId()
     *
     * @param     mixed $sectionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterBySectionId($sectionId = null, $comparison = null)
    {
        if (is_array($sectionId)) {
            $useMinMax = false;
            if (isset($sectionId['min'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_SECTION_ID, $sectionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sectionId['max'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_SECTION_ID, $sectionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionConnectorTableMap::COL_SECTION_ID, $sectionId, $comparison);
    }

    /**
     * Filter the query on the provider_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProviderId(1234); // WHERE provider_id = 1234
     * $query->filterByProviderId(array(12, 34)); // WHERE provider_id IN (12, 34)
     * $query->filterByProviderId(array('min' => 12)); // WHERE provider_id > 12
     * </code>
     *
     * @see       filterByRemoteProvider()
     *
     * @param     mixed $providerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterByProviderId($providerId = null, $comparison = null)
    {
        if (is_array($providerId)) {
            $useMinMax = false;
            if (isset($providerId['min'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_PROVIDER_ID, $providerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($providerId['max'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_PROVIDER_ID, $providerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionConnectorTableMap::COL_PROVIDER_ID, $providerId, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionConnectorTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the baseurl column
     *
     * Example usage:
     * <code>
     * $query->filterByBaseurl('fooValue');   // WHERE baseurl = 'fooValue'
     * $query->filterByBaseurl('%fooValue%', Criteria::LIKE); // WHERE baseurl LIKE '%fooValue%'
     * </code>
     *
     * @param     string $baseurl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterByBaseurl($baseurl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($baseurl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionConnectorTableMap::COL_BASEURL, $baseurl, $comparison);
    }

    /**
     * Filter the query on the remote_section_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRemoteSectionId(1234); // WHERE remote_section_id = 1234
     * $query->filterByRemoteSectionId(array(12, 34)); // WHERE remote_section_id IN (12, 34)
     * $query->filterByRemoteSectionId(array('min' => 12)); // WHERE remote_section_id > 12
     * </code>
     *
     * @see       filterBySectionRelatedByRemoteSectionId()
     *
     * @param     mixed $remoteSectionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterByRemoteSectionId($remoteSectionId = null, $comparison = null)
    {
        if (is_array($remoteSectionId)) {
            $useMinMax = false;
            if (isset($remoteSectionId['min'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_REMOTE_SECTION_ID, $remoteSectionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($remoteSectionId['max'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_REMOTE_SECTION_ID, $remoteSectionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionConnectorTableMap::COL_REMOTE_SECTION_ID, $remoteSectionId, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionConnectorTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionConnectorTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(SectionConnectorTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SectionConnectorTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\RemoteProvider object
     *
     * @param \Database\HubPlus\RemoteProvider|ObjectCollection $remoteProvider The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterByRemoteProvider($remoteProvider, $comparison = null)
    {
        if ($remoteProvider instanceof \Database\HubPlus\RemoteProvider) {
            return $this
                ->addUsingAlias(SectionConnectorTableMap::COL_PROVIDER_ID, $remoteProvider->getId(), $comparison);
        } elseif ($remoteProvider instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SectionConnectorTableMap::COL_PROVIDER_ID, $remoteProvider->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByRemoteProvider() only accepts arguments of type \Database\HubPlus\RemoteProvider or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RemoteProvider relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function joinRemoteProvider($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RemoteProvider');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RemoteProvider');
        }

        return $this;
    }

    /**
     * Use the RemoteProvider relation RemoteProvider object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\RemoteProviderQuery A secondary query class using the current class as primary query
     */
    public function useRemoteProviderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRemoteProvider($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RemoteProvider', '\Database\HubPlus\RemoteProviderQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Section object
     *
     * @param \Database\HubPlus\Section|ObjectCollection $section The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterBySectionRelatedBySectionId($section, $comparison = null)
    {
        if ($section instanceof \Database\HubPlus\Section) {
            return $this
                ->addUsingAlias(SectionConnectorTableMap::COL_SECTION_ID, $section->getId(), $comparison);
        } elseif ($section instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SectionConnectorTableMap::COL_SECTION_ID, $section->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySectionRelatedBySectionId() only accepts arguments of type \Database\HubPlus\Section or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionRelatedBySectionId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function joinSectionRelatedBySectionId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionRelatedBySectionId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionRelatedBySectionId');
        }

        return $this;
    }

    /**
     * Use the SectionRelatedBySectionId relation Section object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionQuery A secondary query class using the current class as primary query
     */
    public function useSectionRelatedBySectionIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSectionRelatedBySectionId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionRelatedBySectionId', '\Database\HubPlus\SectionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Section object
     *
     * @param \Database\HubPlus\Section|ObjectCollection $section The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterBySectionRelatedByRemoteSectionId($section, $comparison = null)
    {
        if ($section instanceof \Database\HubPlus\Section) {
            return $this
                ->addUsingAlias(SectionConnectorTableMap::COL_REMOTE_SECTION_ID, $section->getId(), $comparison);
        } elseif ($section instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SectionConnectorTableMap::COL_REMOTE_SECTION_ID, $section->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySectionRelatedByRemoteSectionId() only accepts arguments of type \Database\HubPlus\Section or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionRelatedByRemoteSectionId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function joinSectionRelatedByRemoteSectionId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionRelatedByRemoteSectionId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionRelatedByRemoteSectionId');
        }

        return $this;
    }

    /**
     * Use the SectionRelatedByRemoteSectionId relation Section object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionQuery A secondary query class using the current class as primary query
     */
    public function useSectionRelatedByRemoteSectionIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSectionRelatedByRemoteSectionId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionRelatedByRemoteSectionId', '\Database\HubPlus\SectionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\RemoteExtraLog object
     *
     * @param \Database\HubPlus\RemoteExtraLog|ObjectCollection $remoteExtraLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterByRemoteExtraLog($remoteExtraLog, $comparison = null)
    {
        if ($remoteExtraLog instanceof \Database\HubPlus\RemoteExtraLog) {
            return $this
                ->addUsingAlias(SectionConnectorTableMap::COL_ID, $remoteExtraLog->getConnectorId(), $comparison);
        } elseif ($remoteExtraLog instanceof ObjectCollection) {
            return $this
                ->useRemoteExtraLogQuery()
                ->filterByPrimaryKeys($remoteExtraLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRemoteExtraLog() only accepts arguments of type \Database\HubPlus\RemoteExtraLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RemoteExtraLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function joinRemoteExtraLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RemoteExtraLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RemoteExtraLog');
        }

        return $this;
    }

    /**
     * Use the RemoteExtraLog relation RemoteExtraLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\RemoteExtraLogQuery A secondary query class using the current class as primary query
     */
    public function useRemoteExtraLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRemoteExtraLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RemoteExtraLog', '\Database\HubPlus\RemoteExtraLogQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\RemoteLog object
     *
     * @param \Database\HubPlus\RemoteLog|ObjectCollection $remoteLog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function filterByRemoteLog($remoteLog, $comparison = null)
    {
        if ($remoteLog instanceof \Database\HubPlus\RemoteLog) {
            return $this
                ->addUsingAlias(SectionConnectorTableMap::COL_ID, $remoteLog->getConnectorId(), $comparison);
        } elseif ($remoteLog instanceof ObjectCollection) {
            return $this
                ->useRemoteLogQuery()
                ->filterByPrimaryKeys($remoteLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRemoteLog() only accepts arguments of type \Database\HubPlus\RemoteLog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RemoteLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function joinRemoteLog($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RemoteLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RemoteLog');
        }

        return $this;
    }

    /**
     * Use the RemoteLog relation RemoteLog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\RemoteLogQuery A secondary query class using the current class as primary query
     */
    public function useRemoteLogQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRemoteLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RemoteLog', '\Database\HubPlus\RemoteLogQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSectionConnector $sectionConnector Object to remove from the list of results
     *
     * @return $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function prune($sectionConnector = null)
    {
        if ($sectionConnector) {
            $this->addUsingAlias(SectionConnectorTableMap::COL_ID, $sectionConnector->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Code to execute before every DELETE statement
     *
     * @param     ConnectionInterface $con The connection object used by the query
     */
    protected function basePreDelete(ConnectionInterface $con)
    {
        // archivable behavior

        if ($this->archiveOnDelete) {
            $this->archive($con);
        } else {
            $this->archiveOnDelete = true;
        }


        return $this->preDelete($con);
    }

    /**
     * Deletes all rows from the section_connector table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionConnectorTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SectionConnectorTableMap::clearInstancePool();
            SectionConnectorTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionConnectorTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SectionConnectorTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SectionConnectorTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SectionConnectorTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(SectionConnectorTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(SectionConnectorTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(SectionConnectorTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(SectionConnectorTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(SectionConnectorTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildSectionConnectorQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(SectionConnectorTableMap::COL_CREATED_AT);
    }

    // archivable behavior

    /**
     * Copy the data of the objects satisfying the query into ChildSectionConnectorArchive archive objects.
     * The archived objects are then saved.
     * If any of the objects has already been archived, the archived object
     * is updated and not duplicated.
     * Warning: This termination methods issues 2n+1 queries.
     *
     * @param      ConnectionInterface $con    Connection to use.
     * @param      Boolean $useLittleMemory    Whether or not to use OnDemandFormatter to retrieve objects.
     *               Set to false if the identity map matters.
     *               Set to true (default) to use less memory.
     *
     * @return     int the number of archived objects
     */
    public function archive($con = null, $useLittleMemory = true)
    {
        $criteria = clone $this;
        // prepare the query
        $criteria->setWith(array());
        if ($useLittleMemory) {
            $criteria->setFormatter(ModelCriteria::FORMAT_ON_DEMAND);
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionConnectorTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con, $criteria) {
            $totalArchivedObjects = 0;

            // archive all results one by one
            foreach ($criteria->find($con) as $object) {
                $object->archive($con);
                $totalArchivedObjects++;
            }

            return $totalArchivedObjects;
        });
    }

    /**
     * Enable/disable auto-archiving on delete for the next query.
     *
     * @param boolean True if the query must archive deleted objects, false otherwise.
     */
    public function setArchiveOnDelete($archiveOnDelete)
    {
        $this->archiveOnDelete = $archiveOnDelete;
    }

    /**
     * Delete records matching the current query without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Delete all records without archiving them.
     *
     * @param      ConnectionInterface $con    Connection to use.
     *
     * @return integer the number of deleted rows
     */
    public function deleteAllWithoutArchive($con = null)
    {
        $this->archiveOnDelete = false;

        return $this->deleteAll($con);
    }

} // SectionConnectorQuery
