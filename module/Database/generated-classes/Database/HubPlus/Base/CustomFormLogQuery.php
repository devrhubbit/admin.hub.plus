<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\CustomFormLog as ChildCustomFormLog;
use Database\HubPlus\CustomFormLogQuery as ChildCustomFormLogQuery;
use Database\HubPlus\Map\CustomFormLogTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'cf_log' table.
 *
 *
 *
 * @method     ChildCustomFormLogQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCustomFormLogQuery orderByFormId($order = Criteria::ASC) Order by the form_id column
 * @method     ChildCustomFormLogQuery orderByRowId($order = Criteria::ASC) Order by the row_id column
 * @method     ChildCustomFormLogQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildCustomFormLogQuery orderByLikedAt($order = Criteria::ASC) Order by the liked_at column
 * @method     ChildCustomFormLogQuery orderBySharedAt($order = Criteria::ASC) Order by the shared_at column
 * @method     ChildCustomFormLogQuery orderByLikeCount($order = Criteria::ASC) Order by the like_count column
 * @method     ChildCustomFormLogQuery orderByShareCount($order = Criteria::ASC) Order by the share_count column
 * @method     ChildCustomFormLogQuery orderByViewCount($order = Criteria::ASC) Order by the view_count column
 *
 * @method     ChildCustomFormLogQuery groupById() Group by the id column
 * @method     ChildCustomFormLogQuery groupByFormId() Group by the form_id column
 * @method     ChildCustomFormLogQuery groupByRowId() Group by the row_id column
 * @method     ChildCustomFormLogQuery groupByUserId() Group by the user_id column
 * @method     ChildCustomFormLogQuery groupByLikedAt() Group by the liked_at column
 * @method     ChildCustomFormLogQuery groupBySharedAt() Group by the shared_at column
 * @method     ChildCustomFormLogQuery groupByLikeCount() Group by the like_count column
 * @method     ChildCustomFormLogQuery groupByShareCount() Group by the share_count column
 * @method     ChildCustomFormLogQuery groupByViewCount() Group by the view_count column
 *
 * @method     ChildCustomFormLogQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCustomFormLogQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCustomFormLogQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCustomFormLogQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCustomFormLogQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCustomFormLogQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCustomFormLogQuery leftJoinSection($relationAlias = null) Adds a LEFT JOIN clause to the query using the Section relation
 * @method     ChildCustomFormLogQuery rightJoinSection($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Section relation
 * @method     ChildCustomFormLogQuery innerJoinSection($relationAlias = null) Adds a INNER JOIN clause to the query using the Section relation
 *
 * @method     ChildCustomFormLogQuery joinWithSection($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Section relation
 *
 * @method     ChildCustomFormLogQuery leftJoinWithSection() Adds a LEFT JOIN clause and with to the query using the Section relation
 * @method     ChildCustomFormLogQuery rightJoinWithSection() Adds a RIGHT JOIN clause and with to the query using the Section relation
 * @method     ChildCustomFormLogQuery innerJoinWithSection() Adds a INNER JOIN clause and with to the query using the Section relation
 *
 * @method     ChildCustomFormLogQuery leftJoinUserApp($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserApp relation
 * @method     ChildCustomFormLogQuery rightJoinUserApp($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserApp relation
 * @method     ChildCustomFormLogQuery innerJoinUserApp($relationAlias = null) Adds a INNER JOIN clause to the query using the UserApp relation
 *
 * @method     ChildCustomFormLogQuery joinWithUserApp($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserApp relation
 *
 * @method     ChildCustomFormLogQuery leftJoinWithUserApp() Adds a LEFT JOIN clause and with to the query using the UserApp relation
 * @method     ChildCustomFormLogQuery rightJoinWithUserApp() Adds a RIGHT JOIN clause and with to the query using the UserApp relation
 * @method     ChildCustomFormLogQuery innerJoinWithUserApp() Adds a INNER JOIN clause and with to the query using the UserApp relation
 *
 * @method     \Database\HubPlus\SectionQuery|\Database\HubPlus\UserAppQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCustomFormLog findOne(ConnectionInterface $con = null) Return the first ChildCustomFormLog matching the query
 * @method     ChildCustomFormLog findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCustomFormLog matching the query, or a new ChildCustomFormLog object populated from the query conditions when no match is found
 *
 * @method     ChildCustomFormLog findOneById(int $id) Return the first ChildCustomFormLog filtered by the id column
 * @method     ChildCustomFormLog findOneByFormId(int $form_id) Return the first ChildCustomFormLog filtered by the form_id column
 * @method     ChildCustomFormLog findOneByRowId(int $row_id) Return the first ChildCustomFormLog filtered by the row_id column
 * @method     ChildCustomFormLog findOneByUserId(int $user_id) Return the first ChildCustomFormLog filtered by the user_id column
 * @method     ChildCustomFormLog findOneByLikedAt(string $liked_at) Return the first ChildCustomFormLog filtered by the liked_at column
 * @method     ChildCustomFormLog findOneBySharedAt(string $shared_at) Return the first ChildCustomFormLog filtered by the shared_at column
 * @method     ChildCustomFormLog findOneByLikeCount(int $like_count) Return the first ChildCustomFormLog filtered by the like_count column
 * @method     ChildCustomFormLog findOneByShareCount(int $share_count) Return the first ChildCustomFormLog filtered by the share_count column
 * @method     ChildCustomFormLog findOneByViewCount(int $view_count) Return the first ChildCustomFormLog filtered by the view_count column *

 * @method     ChildCustomFormLog requirePk($key, ConnectionInterface $con = null) Return the ChildCustomFormLog by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormLog requireOne(ConnectionInterface $con = null) Return the first ChildCustomFormLog matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomFormLog requireOneById(int $id) Return the first ChildCustomFormLog filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormLog requireOneByFormId(int $form_id) Return the first ChildCustomFormLog filtered by the form_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormLog requireOneByRowId(int $row_id) Return the first ChildCustomFormLog filtered by the row_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormLog requireOneByUserId(int $user_id) Return the first ChildCustomFormLog filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormLog requireOneByLikedAt(string $liked_at) Return the first ChildCustomFormLog filtered by the liked_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormLog requireOneBySharedAt(string $shared_at) Return the first ChildCustomFormLog filtered by the shared_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormLog requireOneByLikeCount(int $like_count) Return the first ChildCustomFormLog filtered by the like_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormLog requireOneByShareCount(int $share_count) Return the first ChildCustomFormLog filtered by the share_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomFormLog requireOneByViewCount(int $view_count) Return the first ChildCustomFormLog filtered by the view_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomFormLog[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCustomFormLog objects based on current ModelCriteria
 * @method     ChildCustomFormLog[]|ObjectCollection findById(int $id) Return ChildCustomFormLog objects filtered by the id column
 * @method     ChildCustomFormLog[]|ObjectCollection findByFormId(int $form_id) Return ChildCustomFormLog objects filtered by the form_id column
 * @method     ChildCustomFormLog[]|ObjectCollection findByRowId(int $row_id) Return ChildCustomFormLog objects filtered by the row_id column
 * @method     ChildCustomFormLog[]|ObjectCollection findByUserId(int $user_id) Return ChildCustomFormLog objects filtered by the user_id column
 * @method     ChildCustomFormLog[]|ObjectCollection findByLikedAt(string $liked_at) Return ChildCustomFormLog objects filtered by the liked_at column
 * @method     ChildCustomFormLog[]|ObjectCollection findBySharedAt(string $shared_at) Return ChildCustomFormLog objects filtered by the shared_at column
 * @method     ChildCustomFormLog[]|ObjectCollection findByLikeCount(int $like_count) Return ChildCustomFormLog objects filtered by the like_count column
 * @method     ChildCustomFormLog[]|ObjectCollection findByShareCount(int $share_count) Return ChildCustomFormLog objects filtered by the share_count column
 * @method     ChildCustomFormLog[]|ObjectCollection findByViewCount(int $view_count) Return ChildCustomFormLog objects filtered by the view_count column
 * @method     ChildCustomFormLog[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CustomFormLogQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\CustomFormLogQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\CustomFormLog', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCustomFormLogQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCustomFormLogQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCustomFormLogQuery) {
            return $criteria;
        }
        $query = new ChildCustomFormLogQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCustomFormLog|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CustomFormLogTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CustomFormLogTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomFormLog A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, form_id, row_id, user_id, liked_at, shared_at, like_count, share_count, view_count FROM cf_log WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCustomFormLog $obj */
            $obj = new ChildCustomFormLog();
            $obj->hydrate($row);
            CustomFormLogTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCustomFormLog|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CustomFormLogTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CustomFormLogTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormLogTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the form_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFormId(1234); // WHERE form_id = 1234
     * $query->filterByFormId(array(12, 34)); // WHERE form_id IN (12, 34)
     * $query->filterByFormId(array('min' => 12)); // WHERE form_id > 12
     * </code>
     *
     * @see       filterBySection()
     *
     * @param     mixed $formId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterByFormId($formId = null, $comparison = null)
    {
        if (is_array($formId)) {
            $useMinMax = false;
            if (isset($formId['min'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_FORM_ID, $formId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($formId['max'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_FORM_ID, $formId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormLogTableMap::COL_FORM_ID, $formId, $comparison);
    }

    /**
     * Filter the query on the row_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRowId(1234); // WHERE row_id = 1234
     * $query->filterByRowId(array(12, 34)); // WHERE row_id IN (12, 34)
     * $query->filterByRowId(array('min' => 12)); // WHERE row_id > 12
     * </code>
     *
     * @param     mixed $rowId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterByRowId($rowId = null, $comparison = null)
    {
        if (is_array($rowId)) {
            $useMinMax = false;
            if (isset($rowId['min'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_ROW_ID, $rowId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rowId['max'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_ROW_ID, $rowId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormLogTableMap::COL_ROW_ID, $rowId, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @see       filterByUserApp()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormLogTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the liked_at column
     *
     * Example usage:
     * <code>
     * $query->filterByLikedAt('2011-03-14'); // WHERE liked_at = '2011-03-14'
     * $query->filterByLikedAt('now'); // WHERE liked_at = '2011-03-14'
     * $query->filterByLikedAt(array('max' => 'yesterday')); // WHERE liked_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $likedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterByLikedAt($likedAt = null, $comparison = null)
    {
        if (is_array($likedAt)) {
            $useMinMax = false;
            if (isset($likedAt['min'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_LIKED_AT, $likedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($likedAt['max'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_LIKED_AT, $likedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormLogTableMap::COL_LIKED_AT, $likedAt, $comparison);
    }

    /**
     * Filter the query on the shared_at column
     *
     * Example usage:
     * <code>
     * $query->filterBySharedAt('2011-03-14'); // WHERE shared_at = '2011-03-14'
     * $query->filterBySharedAt('now'); // WHERE shared_at = '2011-03-14'
     * $query->filterBySharedAt(array('max' => 'yesterday')); // WHERE shared_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $sharedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterBySharedAt($sharedAt = null, $comparison = null)
    {
        if (is_array($sharedAt)) {
            $useMinMax = false;
            if (isset($sharedAt['min'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_SHARED_AT, $sharedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sharedAt['max'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_SHARED_AT, $sharedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormLogTableMap::COL_SHARED_AT, $sharedAt, $comparison);
    }

    /**
     * Filter the query on the like_count column
     *
     * Example usage:
     * <code>
     * $query->filterByLikeCount(1234); // WHERE like_count = 1234
     * $query->filterByLikeCount(array(12, 34)); // WHERE like_count IN (12, 34)
     * $query->filterByLikeCount(array('min' => 12)); // WHERE like_count > 12
     * </code>
     *
     * @param     mixed $likeCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterByLikeCount($likeCount = null, $comparison = null)
    {
        if (is_array($likeCount)) {
            $useMinMax = false;
            if (isset($likeCount['min'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_LIKE_COUNT, $likeCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($likeCount['max'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_LIKE_COUNT, $likeCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormLogTableMap::COL_LIKE_COUNT, $likeCount, $comparison);
    }

    /**
     * Filter the query on the share_count column
     *
     * Example usage:
     * <code>
     * $query->filterByShareCount(1234); // WHERE share_count = 1234
     * $query->filterByShareCount(array(12, 34)); // WHERE share_count IN (12, 34)
     * $query->filterByShareCount(array('min' => 12)); // WHERE share_count > 12
     * </code>
     *
     * @param     mixed $shareCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterByShareCount($shareCount = null, $comparison = null)
    {
        if (is_array($shareCount)) {
            $useMinMax = false;
            if (isset($shareCount['min'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_SHARE_COUNT, $shareCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shareCount['max'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_SHARE_COUNT, $shareCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormLogTableMap::COL_SHARE_COUNT, $shareCount, $comparison);
    }

    /**
     * Filter the query on the view_count column
     *
     * Example usage:
     * <code>
     * $query->filterByViewCount(1234); // WHERE view_count = 1234
     * $query->filterByViewCount(array(12, 34)); // WHERE view_count IN (12, 34)
     * $query->filterByViewCount(array('min' => 12)); // WHERE view_count > 12
     * </code>
     *
     * @param     mixed $viewCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterByViewCount($viewCount = null, $comparison = null)
    {
        if (is_array($viewCount)) {
            $useMinMax = false;
            if (isset($viewCount['min'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_VIEW_COUNT, $viewCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($viewCount['max'])) {
                $this->addUsingAlias(CustomFormLogTableMap::COL_VIEW_COUNT, $viewCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomFormLogTableMap::COL_VIEW_COUNT, $viewCount, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Section object
     *
     * @param \Database\HubPlus\Section|ObjectCollection $section The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterBySection($section, $comparison = null)
    {
        if ($section instanceof \Database\HubPlus\Section) {
            return $this
                ->addUsingAlias(CustomFormLogTableMap::COL_FORM_ID, $section->getId(), $comparison);
        } elseif ($section instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomFormLogTableMap::COL_FORM_ID, $section->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySection() only accepts arguments of type \Database\HubPlus\Section or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Section relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function joinSection($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Section');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Section');
        }

        return $this;
    }

    /**
     * Use the Section relation Section object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionQuery A secondary query class using the current class as primary query
     */
    public function useSectionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSection($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Section', '\Database\HubPlus\SectionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\UserApp object
     *
     * @param \Database\HubPlus\UserApp|ObjectCollection $userApp The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function filterByUserApp($userApp, $comparison = null)
    {
        if ($userApp instanceof \Database\HubPlus\UserApp) {
            return $this
                ->addUsingAlias(CustomFormLogTableMap::COL_USER_ID, $userApp->getId(), $comparison);
        } elseif ($userApp instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomFormLogTableMap::COL_USER_ID, $userApp->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUserApp() only accepts arguments of type \Database\HubPlus\UserApp or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserApp relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function joinUserApp($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserApp');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserApp');
        }

        return $this;
    }

    /**
     * Use the UserApp relation UserApp object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\UserAppQuery A secondary query class using the current class as primary query
     */
    public function useUserAppQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserApp($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserApp', '\Database\HubPlus\UserAppQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCustomFormLog $customFormLog Object to remove from the list of results
     *
     * @return $this|ChildCustomFormLogQuery The current query, for fluid interface
     */
    public function prune($customFormLog = null)
    {
        if ($customFormLog) {
            $this->addUsingAlias(CustomFormLogTableMap::COL_ID, $customFormLog->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the cf_log table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomFormLogTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CustomFormLogTableMap::clearInstancePool();
            CustomFormLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomFormLogTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CustomFormLogTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CustomFormLogTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CustomFormLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CustomFormLogQuery
