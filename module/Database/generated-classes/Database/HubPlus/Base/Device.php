<?php

namespace Database\HubPlus\Base;

use \DateTime;
use \Exception;
use \PDO;
use Database\HubPlus\CustomFormExtraLog as ChildCustomFormExtraLog;
use Database\HubPlus\CustomFormExtraLogQuery as ChildCustomFormExtraLogQuery;
use Database\HubPlus\Device as ChildDevice;
use Database\HubPlus\DeviceArchive as ChildDeviceArchive;
use Database\HubPlus\DeviceArchiveQuery as ChildDeviceArchiveQuery;
use Database\HubPlus\DeviceQuery as ChildDeviceQuery;
use Database\HubPlus\Media as ChildMedia;
use Database\HubPlus\MediaExtraLog as ChildMediaExtraLog;
use Database\HubPlus\MediaExtraLogQuery as ChildMediaExtraLogQuery;
use Database\HubPlus\MediaQuery as ChildMediaQuery;
use Database\HubPlus\PostExtraLog as ChildPostExtraLog;
use Database\HubPlus\PostExtraLogQuery as ChildPostExtraLogQuery;
use Database\HubPlus\PushNotificationDevice as ChildPushNotificationDevice;
use Database\HubPlus\PushNotificationDeviceQuery as ChildPushNotificationDeviceQuery;
use Database\HubPlus\RemoteExtraLog as ChildRemoteExtraLog;
use Database\HubPlus\RemoteExtraLogQuery as ChildRemoteExtraLogQuery;
use Database\HubPlus\UserApp as ChildUserApp;
use Database\HubPlus\UserAppQuery as ChildUserAppQuery;
use Database\HubPlus\Map\CustomFormExtraLogTableMap;
use Database\HubPlus\Map\DeviceTableMap;
use Database\HubPlus\Map\MediaExtraLogTableMap;
use Database\HubPlus\Map\MediaTableMap;
use Database\HubPlus\Map\PostExtraLogTableMap;
use Database\HubPlus\Map\PushNotificationDeviceTableMap;
use Database\HubPlus\Map\RemoteExtraLogTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'device' table.
 *
 *
 *
 * @package    propel.generator.Database.HubPlus.Base
 */
abstract class Device implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Database\\HubPlus\\Map\\DeviceTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the user_id field.
     *
     * @var        int
     */
    protected $user_id;

    /**
     * The value for the status field.
     *
     * @var        string
     */
    protected $status;

    /**
     * The value for the type field.
     *
     * @var        string
     */
    protected $type;

    /**
     * The value for the os field.
     *
     * @var        string
     */
    protected $os;

    /**
     * The value for the os_version field.
     *
     * @var        string
     */
    protected $os_version;

    /**
     * The value for the enable_notification field.
     *
     * @var        boolean
     */
    protected $enable_notification;

    /**
     * The value for the api_version field.
     *
     * @var        string
     */
    protected $api_version;

    /**
     * The value for the language field.
     *
     * @var        string
     */
    protected $language;

    /**
     * The value for the demo field.
     *
     * @var        boolean
     */
    protected $demo;

    /**
     * The value for the token field.
     *
     * @var        string
     */
    protected $token;

    /**
     * The value for the api_key field.
     *
     * @var        string
     */
    protected $api_key;

    /**
     * The value for the ip_address field.
     *
     * @var        string
     */
    protected $ip_address;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildUserApp
     */
    protected $aUserApp;

    /**
     * @var        ObjectCollection|ChildCustomFormExtraLog[] Collection to store aggregation of ChildCustomFormExtraLog objects.
     */
    protected $collCustomFormExtraLogs;
    protected $collCustomFormExtraLogsPartial;

    /**
     * @var        ObjectCollection|ChildMedia[] Collection to store aggregation of ChildMedia objects.
     */
    protected $collMedias;
    protected $collMediasPartial;

    /**
     * @var        ObjectCollection|ChildMediaExtraLog[] Collection to store aggregation of ChildMediaExtraLog objects.
     */
    protected $collMediaExtraLogs;
    protected $collMediaExtraLogsPartial;

    /**
     * @var        ObjectCollection|ChildPostExtraLog[] Collection to store aggregation of ChildPostExtraLog objects.
     */
    protected $collPostExtraLogs;
    protected $collPostExtraLogsPartial;

    /**
     * @var        ObjectCollection|ChildPushNotificationDevice[] Collection to store aggregation of ChildPushNotificationDevice objects.
     */
    protected $collPushNotificationDevices;
    protected $collPushNotificationDevicesPartial;

    /**
     * @var        ObjectCollection|ChildRemoteExtraLog[] Collection to store aggregation of ChildRemoteExtraLog objects.
     */
    protected $collRemoteExtraLogs;
    protected $collRemoteExtraLogsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCustomFormExtraLog[]
     */
    protected $customFormExtraLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMedia[]
     */
    protected $mediasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMediaExtraLog[]
     */
    protected $mediaExtraLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostExtraLog[]
     */
    protected $postExtraLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPushNotificationDevice[]
     */
    protected $pushNotificationDevicesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildRemoteExtraLog[]
     */
    protected $remoteExtraLogsScheduledForDeletion = null;

    /**
     * Initializes internal state of Database\HubPlus\Base\Device object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Device</code> instance.  If
     * <code>obj</code> is an instance of <code>Device</code>, delegates to
     * <code>equals(Device)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Device The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [user_id] column value.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Get the [status] column value.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [type] column value.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the [os] column value.
     *
     * @return string
     */
    public function getOs()
    {
        return $this->os;
    }

    /**
     * Get the [os_version] column value.
     *
     * @return string
     */
    public function getOsVersion()
    {
        return $this->os_version;
    }

    /**
     * Get the [enable_notification] column value.
     *
     * @return boolean
     */
    public function getEnableNotification()
    {
        return $this->enable_notification;
    }

    /**
     * Get the [enable_notification] column value.
     *
     * @return boolean
     */
    public function isEnableNotification()
    {
        return $this->getEnableNotification();
    }

    /**
     * Get the [api_version] column value.
     *
     * @return string
     */
    public function getApiVersion()
    {
        return $this->api_version;
    }

    /**
     * Get the [language] column value.
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Get the [demo] column value.
     *
     * @return boolean
     */
    public function getDemo()
    {
        return $this->demo;
    }

    /**
     * Get the [demo] column value.
     *
     * @return boolean
     */
    public function isDemo()
    {
        return $this->getDemo();
    }

    /**
     * Get the [token] column value.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Get the [api_key] column value.
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->api_key;
    }

    /**
     * Get the [ip_address] column value.
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[DeviceTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [user_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->user_id !== $v) {
            $this->user_id = $v;
            $this->modifiedColumns[DeviceTableMap::COL_USER_ID] = true;
        }

        if ($this->aUserApp !== null && $this->aUserApp->getId() !== $v) {
            $this->aUserApp = null;
        }

        return $this;
    } // setUserId()

    /**
     * Set the value of [status] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[DeviceTableMap::COL_STATUS] = true;
        }

        return $this;
    } // setStatus()

    /**
     * Set the value of [type] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[DeviceTableMap::COL_TYPE] = true;
        }

        return $this;
    } // setType()

    /**
     * Set the value of [os] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setOs($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->os !== $v) {
            $this->os = $v;
            $this->modifiedColumns[DeviceTableMap::COL_OS] = true;
        }

        return $this;
    } // setOs()

    /**
     * Set the value of [os_version] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setOsVersion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->os_version !== $v) {
            $this->os_version = $v;
            $this->modifiedColumns[DeviceTableMap::COL_OS_VERSION] = true;
        }

        return $this;
    } // setOsVersion()

    /**
     * Sets the value of the [enable_notification] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setEnableNotification($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->enable_notification !== $v) {
            $this->enable_notification = $v;
            $this->modifiedColumns[DeviceTableMap::COL_ENABLE_NOTIFICATION] = true;
        }

        return $this;
    } // setEnableNotification()

    /**
     * Set the value of [api_version] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setApiVersion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->api_version !== $v) {
            $this->api_version = $v;
            $this->modifiedColumns[DeviceTableMap::COL_API_VERSION] = true;
        }

        return $this;
    } // setApiVersion()

    /**
     * Set the value of [language] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setLanguage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->language !== $v) {
            $this->language = $v;
            $this->modifiedColumns[DeviceTableMap::COL_LANGUAGE] = true;
        }

        return $this;
    } // setLanguage()

    /**
     * Sets the value of the [demo] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setDemo($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->demo !== $v) {
            $this->demo = $v;
            $this->modifiedColumns[DeviceTableMap::COL_DEMO] = true;
        }

        return $this;
    } // setDemo()

    /**
     * Set the value of [token] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setToken($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->token !== $v) {
            $this->token = $v;
            $this->modifiedColumns[DeviceTableMap::COL_TOKEN] = true;
        }

        return $this;
    } // setToken()

    /**
     * Set the value of [api_key] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setApiKey($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->api_key !== $v) {
            $this->api_key = $v;
            $this->modifiedColumns[DeviceTableMap::COL_API_KEY] = true;
        }

        return $this;
    } // setApiKey()

    /**
     * Set the value of [ip_address] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setIpAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ip_address !== $v) {
            $this->ip_address = $v;
            $this->modifiedColumns[DeviceTableMap::COL_IP_ADDRESS] = true;
        }

        return $this;
    } // setIpAddress()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[DeviceTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[DeviceTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[DeviceTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : DeviceTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : DeviceTableMap::translateFieldName('UserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : DeviceTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : DeviceTableMap::translateFieldName('Type', TableMap::TYPE_PHPNAME, $indexType)];
            $this->type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : DeviceTableMap::translateFieldName('Os', TableMap::TYPE_PHPNAME, $indexType)];
            $this->os = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : DeviceTableMap::translateFieldName('OsVersion', TableMap::TYPE_PHPNAME, $indexType)];
            $this->os_version = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : DeviceTableMap::translateFieldName('EnableNotification', TableMap::TYPE_PHPNAME, $indexType)];
            $this->enable_notification = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : DeviceTableMap::translateFieldName('ApiVersion', TableMap::TYPE_PHPNAME, $indexType)];
            $this->api_version = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : DeviceTableMap::translateFieldName('Language', TableMap::TYPE_PHPNAME, $indexType)];
            $this->language = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : DeviceTableMap::translateFieldName('Demo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->demo = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : DeviceTableMap::translateFieldName('Token', TableMap::TYPE_PHPNAME, $indexType)];
            $this->token = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : DeviceTableMap::translateFieldName('ApiKey', TableMap::TYPE_PHPNAME, $indexType)];
            $this->api_key = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : DeviceTableMap::translateFieldName('IpAddress', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ip_address = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : DeviceTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : DeviceTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : DeviceTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 16; // 16 = DeviceTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Database\\HubPlus\\Device'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aUserApp !== null && $this->user_id !== $this->aUserApp->getId()) {
            $this->aUserApp = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DeviceTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildDeviceQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aUserApp = null;
            $this->collCustomFormExtraLogs = null;

            $this->collMedias = null;

            $this->collMediaExtraLogs = null;

            $this->collPostExtraLogs = null;

            $this->collPushNotificationDevices = null;

            $this->collRemoteExtraLogs = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Device::setDeleted()
     * @see Device::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildDeviceQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildDeviceQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(DeviceTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(DeviceTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(DeviceTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                DeviceTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aUserApp !== null) {
                if ($this->aUserApp->isModified() || $this->aUserApp->isNew()) {
                    $affectedRows += $this->aUserApp->save($con);
                }
                $this->setUserApp($this->aUserApp);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->customFormExtraLogsScheduledForDeletion !== null) {
                if (!$this->customFormExtraLogsScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\CustomFormExtraLogQuery::create()
                        ->filterByPrimaryKeys($this->customFormExtraLogsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->customFormExtraLogsScheduledForDeletion = null;
                }
            }

            if ($this->collCustomFormExtraLogs !== null) {
                foreach ($this->collCustomFormExtraLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->mediasScheduledForDeletion !== null) {
                if (!$this->mediasScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\MediaQuery::create()
                        ->filterByPrimaryKeys($this->mediasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->mediasScheduledForDeletion = null;
                }
            }

            if ($this->collMedias !== null) {
                foreach ($this->collMedias as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->mediaExtraLogsScheduledForDeletion !== null) {
                if (!$this->mediaExtraLogsScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\MediaExtraLogQuery::create()
                        ->filterByPrimaryKeys($this->mediaExtraLogsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->mediaExtraLogsScheduledForDeletion = null;
                }
            }

            if ($this->collMediaExtraLogs !== null) {
                foreach ($this->collMediaExtraLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postExtraLogsScheduledForDeletion !== null) {
                if (!$this->postExtraLogsScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\PostExtraLogQuery::create()
                        ->filterByPrimaryKeys($this->postExtraLogsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->postExtraLogsScheduledForDeletion = null;
                }
            }

            if ($this->collPostExtraLogs !== null) {
                foreach ($this->collPostExtraLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pushNotificationDevicesScheduledForDeletion !== null) {
                if (!$this->pushNotificationDevicesScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\PushNotificationDeviceQuery::create()
                        ->filterByPrimaryKeys($this->pushNotificationDevicesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->pushNotificationDevicesScheduledForDeletion = null;
                }
            }

            if ($this->collPushNotificationDevices !== null) {
                foreach ($this->collPushNotificationDevices as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->remoteExtraLogsScheduledForDeletion !== null) {
                if (!$this->remoteExtraLogsScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\RemoteExtraLogQuery::create()
                        ->filterByPrimaryKeys($this->remoteExtraLogsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->remoteExtraLogsScheduledForDeletion = null;
                }
            }

            if ($this->collRemoteExtraLogs !== null) {
                foreach ($this->collRemoteExtraLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[DeviceTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . DeviceTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(DeviceTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'user_id';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'status';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'type';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_OS)) {
            $modifiedColumns[':p' . $index++]  = 'os';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_OS_VERSION)) {
            $modifiedColumns[':p' . $index++]  = 'os_version';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_ENABLE_NOTIFICATION)) {
            $modifiedColumns[':p' . $index++]  = 'enable_notification';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_API_VERSION)) {
            $modifiedColumns[':p' . $index++]  = 'api_version';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_LANGUAGE)) {
            $modifiedColumns[':p' . $index++]  = 'language';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_DEMO)) {
            $modifiedColumns[':p' . $index++]  = 'demo';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_TOKEN)) {
            $modifiedColumns[':p' . $index++]  = 'token';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_API_KEY)) {
            $modifiedColumns[':p' . $index++]  = 'api_key';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_IP_ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = 'ip_address';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(DeviceTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO device (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'user_id':
                        $stmt->bindValue($identifier, $this->user_id, PDO::PARAM_INT);
                        break;
                    case 'status':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_STR);
                        break;
                    case 'type':
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_STR);
                        break;
                    case 'os':
                        $stmt->bindValue($identifier, $this->os, PDO::PARAM_STR);
                        break;
                    case 'os_version':
                        $stmt->bindValue($identifier, $this->os_version, PDO::PARAM_STR);
                        break;
                    case 'enable_notification':
                        $stmt->bindValue($identifier, (int) $this->enable_notification, PDO::PARAM_INT);
                        break;
                    case 'api_version':
                        $stmt->bindValue($identifier, $this->api_version, PDO::PARAM_STR);
                        break;
                    case 'language':
                        $stmt->bindValue($identifier, $this->language, PDO::PARAM_STR);
                        break;
                    case 'demo':
                        $stmt->bindValue($identifier, (int) $this->demo, PDO::PARAM_INT);
                        break;
                    case 'token':
                        $stmt->bindValue($identifier, $this->token, PDO::PARAM_STR);
                        break;
                    case 'api_key':
                        $stmt->bindValue($identifier, $this->api_key, PDO::PARAM_STR);
                        break;
                    case 'ip_address':
                        $stmt->bindValue($identifier, $this->ip_address, PDO::PARAM_STR);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = DeviceTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUserId();
                break;
            case 2:
                return $this->getStatus();
                break;
            case 3:
                return $this->getType();
                break;
            case 4:
                return $this->getOs();
                break;
            case 5:
                return $this->getOsVersion();
                break;
            case 6:
                return $this->getEnableNotification();
                break;
            case 7:
                return $this->getApiVersion();
                break;
            case 8:
                return $this->getLanguage();
                break;
            case 9:
                return $this->getDemo();
                break;
            case 10:
                return $this->getToken();
                break;
            case 11:
                return $this->getApiKey();
                break;
            case 12:
                return $this->getIpAddress();
                break;
            case 13:
                return $this->getDeletedAt();
                break;
            case 14:
                return $this->getCreatedAt();
                break;
            case 15:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Device'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Device'][$this->hashCode()] = true;
        $keys = DeviceTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUserId(),
            $keys[2] => $this->getStatus(),
            $keys[3] => $this->getType(),
            $keys[4] => $this->getOs(),
            $keys[5] => $this->getOsVersion(),
            $keys[6] => $this->getEnableNotification(),
            $keys[7] => $this->getApiVersion(),
            $keys[8] => $this->getLanguage(),
            $keys[9] => $this->getDemo(),
            $keys[10] => $this->getToken(),
            $keys[11] => $this->getApiKey(),
            $keys[12] => $this->getIpAddress(),
            $keys[13] => $this->getDeletedAt(),
            $keys[14] => $this->getCreatedAt(),
            $keys[15] => $this->getUpdatedAt(),
        );
        if ($result[$keys[13]] instanceof \DateTimeInterface) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        if ($result[$keys[14]] instanceof \DateTimeInterface) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[15]] instanceof \DateTimeInterface) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aUserApp) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userApp';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_app';
                        break;
                    default:
                        $key = 'UserApp';
                }

                $result[$key] = $this->aUserApp->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCustomFormExtraLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customFormExtraLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cf_extra_logs';
                        break;
                    default:
                        $key = 'CustomFormExtraLogs';
                }

                $result[$key] = $this->collCustomFormExtraLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMedias) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'medias';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'medias';
                        break;
                    default:
                        $key = 'Medias';
                }

                $result[$key] = $this->collMedias->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMediaExtraLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mediaExtraLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'media_extra_logs';
                        break;
                    default:
                        $key = 'MediaExtraLogs';
                }

                $result[$key] = $this->collMediaExtraLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPostExtraLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postExtraLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_extra_logs';
                        break;
                    default:
                        $key = 'PostExtraLogs';
                }

                $result[$key] = $this->collPostExtraLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPushNotificationDevices) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pushNotificationDevices';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'push_notification_devices';
                        break;
                    default:
                        $key = 'PushNotificationDevices';
                }

                $result[$key] = $this->collPushNotificationDevices->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRemoteExtraLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'remoteExtraLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'remote_extra_logs';
                        break;
                    default:
                        $key = 'RemoteExtraLogs';
                }

                $result[$key] = $this->collRemoteExtraLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Database\HubPlus\Device
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = DeviceTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Database\HubPlus\Device
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUserId($value);
                break;
            case 2:
                $this->setStatus($value);
                break;
            case 3:
                $this->setType($value);
                break;
            case 4:
                $this->setOs($value);
                break;
            case 5:
                $this->setOsVersion($value);
                break;
            case 6:
                $this->setEnableNotification($value);
                break;
            case 7:
                $this->setApiVersion($value);
                break;
            case 8:
                $this->setLanguage($value);
                break;
            case 9:
                $this->setDemo($value);
                break;
            case 10:
                $this->setToken($value);
                break;
            case 11:
                $this->setApiKey($value);
                break;
            case 12:
                $this->setIpAddress($value);
                break;
            case 13:
                $this->setDeletedAt($value);
                break;
            case 14:
                $this->setCreatedAt($value);
                break;
            case 15:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = DeviceTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUserId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setStatus($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setType($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setOs($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setOsVersion($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setEnableNotification($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setApiVersion($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setLanguage($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setDemo($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setToken($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setApiKey($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setIpAddress($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setDeletedAt($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setCreatedAt($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setUpdatedAt($arr[$keys[15]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Database\HubPlus\Device The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(DeviceTableMap::DATABASE_NAME);

        if ($this->isColumnModified(DeviceTableMap::COL_ID)) {
            $criteria->add(DeviceTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_USER_ID)) {
            $criteria->add(DeviceTableMap::COL_USER_ID, $this->user_id);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_STATUS)) {
            $criteria->add(DeviceTableMap::COL_STATUS, $this->status);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_TYPE)) {
            $criteria->add(DeviceTableMap::COL_TYPE, $this->type);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_OS)) {
            $criteria->add(DeviceTableMap::COL_OS, $this->os);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_OS_VERSION)) {
            $criteria->add(DeviceTableMap::COL_OS_VERSION, $this->os_version);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_ENABLE_NOTIFICATION)) {
            $criteria->add(DeviceTableMap::COL_ENABLE_NOTIFICATION, $this->enable_notification);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_API_VERSION)) {
            $criteria->add(DeviceTableMap::COL_API_VERSION, $this->api_version);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_LANGUAGE)) {
            $criteria->add(DeviceTableMap::COL_LANGUAGE, $this->language);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_DEMO)) {
            $criteria->add(DeviceTableMap::COL_DEMO, $this->demo);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_TOKEN)) {
            $criteria->add(DeviceTableMap::COL_TOKEN, $this->token);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_API_KEY)) {
            $criteria->add(DeviceTableMap::COL_API_KEY, $this->api_key);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_IP_ADDRESS)) {
            $criteria->add(DeviceTableMap::COL_IP_ADDRESS, $this->ip_address);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_DELETED_AT)) {
            $criteria->add(DeviceTableMap::COL_DELETED_AT, $this->deleted_at);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_CREATED_AT)) {
            $criteria->add(DeviceTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(DeviceTableMap::COL_UPDATED_AT)) {
            $criteria->add(DeviceTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildDeviceQuery::create();
        $criteria->add(DeviceTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Database\HubPlus\Device (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUserId($this->getUserId());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setType($this->getType());
        $copyObj->setOs($this->getOs());
        $copyObj->setOsVersion($this->getOsVersion());
        $copyObj->setEnableNotification($this->getEnableNotification());
        $copyObj->setApiVersion($this->getApiVersion());
        $copyObj->setLanguage($this->getLanguage());
        $copyObj->setDemo($this->getDemo());
        $copyObj->setToken($this->getToken());
        $copyObj->setApiKey($this->getApiKey());
        $copyObj->setIpAddress($this->getIpAddress());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCustomFormExtraLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomFormExtraLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMedias() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMedia($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMediaExtraLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMediaExtraLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPostExtraLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostExtraLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPushNotificationDevices() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPushNotificationDevice($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRemoteExtraLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRemoteExtraLog($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Database\HubPlus\Device Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildUserApp object.
     *
     * @param  ChildUserApp $v
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUserApp(ChildUserApp $v = null)
    {
        if ($v === null) {
            $this->setUserId(NULL);
        } else {
            $this->setUserId($v->getId());
        }

        $this->aUserApp = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUserApp object, it will not be re-added.
        if ($v !== null) {
            $v->addDevice($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUserApp object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUserApp The associated ChildUserApp object.
     * @throws PropelException
     */
    public function getUserApp(ConnectionInterface $con = null)
    {
        if ($this->aUserApp === null && ($this->user_id != 0)) {
            $this->aUserApp = ChildUserAppQuery::create()->findPk($this->user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUserApp->addDevices($this);
             */
        }

        return $this->aUserApp;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CustomFormExtraLog' == $relationName) {
            $this->initCustomFormExtraLogs();
            return;
        }
        if ('Media' == $relationName) {
            $this->initMedias();
            return;
        }
        if ('MediaExtraLog' == $relationName) {
            $this->initMediaExtraLogs();
            return;
        }
        if ('PostExtraLog' == $relationName) {
            $this->initPostExtraLogs();
            return;
        }
        if ('PushNotificationDevice' == $relationName) {
            $this->initPushNotificationDevices();
            return;
        }
        if ('RemoteExtraLog' == $relationName) {
            $this->initRemoteExtraLogs();
            return;
        }
    }

    /**
     * Clears out the collCustomFormExtraLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomFormExtraLogs()
     */
    public function clearCustomFormExtraLogs()
    {
        $this->collCustomFormExtraLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomFormExtraLogs collection loaded partially.
     */
    public function resetPartialCustomFormExtraLogs($v = true)
    {
        $this->collCustomFormExtraLogsPartial = $v;
    }

    /**
     * Initializes the collCustomFormExtraLogs collection.
     *
     * By default this just sets the collCustomFormExtraLogs collection to an empty array (like clearcollCustomFormExtraLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomFormExtraLogs($overrideExisting = true)
    {
        if (null !== $this->collCustomFormExtraLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomFormExtraLogTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomFormExtraLogs = new $collectionClassName;
        $this->collCustomFormExtraLogs->setModel('\Database\HubPlus\CustomFormExtraLog');
    }

    /**
     * Gets an array of ChildCustomFormExtraLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDevice is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCustomFormExtraLog[] List of ChildCustomFormExtraLog objects
     * @throws PropelException
     */
    public function getCustomFormExtraLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomFormExtraLogsPartial && !$this->isNew();
        if (null === $this->collCustomFormExtraLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCustomFormExtraLogs) {
                // return empty collection
                $this->initCustomFormExtraLogs();
            } else {
                $collCustomFormExtraLogs = ChildCustomFormExtraLogQuery::create(null, $criteria)
                    ->filterByDevice($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomFormExtraLogsPartial && count($collCustomFormExtraLogs)) {
                        $this->initCustomFormExtraLogs(false);

                        foreach ($collCustomFormExtraLogs as $obj) {
                            if (false == $this->collCustomFormExtraLogs->contains($obj)) {
                                $this->collCustomFormExtraLogs->append($obj);
                            }
                        }

                        $this->collCustomFormExtraLogsPartial = true;
                    }

                    return $collCustomFormExtraLogs;
                }

                if ($partial && $this->collCustomFormExtraLogs) {
                    foreach ($this->collCustomFormExtraLogs as $obj) {
                        if ($obj->isNew()) {
                            $collCustomFormExtraLogs[] = $obj;
                        }
                    }
                }

                $this->collCustomFormExtraLogs = $collCustomFormExtraLogs;
                $this->collCustomFormExtraLogsPartial = false;
            }
        }

        return $this->collCustomFormExtraLogs;
    }

    /**
     * Sets a collection of ChildCustomFormExtraLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customFormExtraLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function setCustomFormExtraLogs(Collection $customFormExtraLogs, ConnectionInterface $con = null)
    {
        /** @var ChildCustomFormExtraLog[] $customFormExtraLogsToDelete */
        $customFormExtraLogsToDelete = $this->getCustomFormExtraLogs(new Criteria(), $con)->diff($customFormExtraLogs);


        $this->customFormExtraLogsScheduledForDeletion = $customFormExtraLogsToDelete;

        foreach ($customFormExtraLogsToDelete as $customFormExtraLogRemoved) {
            $customFormExtraLogRemoved->setDevice(null);
        }

        $this->collCustomFormExtraLogs = null;
        foreach ($customFormExtraLogs as $customFormExtraLog) {
            $this->addCustomFormExtraLog($customFormExtraLog);
        }

        $this->collCustomFormExtraLogs = $customFormExtraLogs;
        $this->collCustomFormExtraLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CustomFormExtraLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CustomFormExtraLog objects.
     * @throws PropelException
     */
    public function countCustomFormExtraLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomFormExtraLogsPartial && !$this->isNew();
        if (null === $this->collCustomFormExtraLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomFormExtraLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomFormExtraLogs());
            }

            $query = ChildCustomFormExtraLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDevice($this)
                ->count($con);
        }

        return count($this->collCustomFormExtraLogs);
    }

    /**
     * Method called to associate a ChildCustomFormExtraLog object to this object
     * through the ChildCustomFormExtraLog foreign key attribute.
     *
     * @param  ChildCustomFormExtraLog $l ChildCustomFormExtraLog
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function addCustomFormExtraLog(ChildCustomFormExtraLog $l)
    {
        if ($this->collCustomFormExtraLogs === null) {
            $this->initCustomFormExtraLogs();
            $this->collCustomFormExtraLogsPartial = true;
        }

        if (!$this->collCustomFormExtraLogs->contains($l)) {
            $this->doAddCustomFormExtraLog($l);

            if ($this->customFormExtraLogsScheduledForDeletion and $this->customFormExtraLogsScheduledForDeletion->contains($l)) {
                $this->customFormExtraLogsScheduledForDeletion->remove($this->customFormExtraLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCustomFormExtraLog $customFormExtraLog The ChildCustomFormExtraLog object to add.
     */
    protected function doAddCustomFormExtraLog(ChildCustomFormExtraLog $customFormExtraLog)
    {
        $this->collCustomFormExtraLogs[]= $customFormExtraLog;
        $customFormExtraLog->setDevice($this);
    }

    /**
     * @param  ChildCustomFormExtraLog $customFormExtraLog The ChildCustomFormExtraLog object to remove.
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function removeCustomFormExtraLog(ChildCustomFormExtraLog $customFormExtraLog)
    {
        if ($this->getCustomFormExtraLogs()->contains($customFormExtraLog)) {
            $pos = $this->collCustomFormExtraLogs->search($customFormExtraLog);
            $this->collCustomFormExtraLogs->remove($pos);
            if (null === $this->customFormExtraLogsScheduledForDeletion) {
                $this->customFormExtraLogsScheduledForDeletion = clone $this->collCustomFormExtraLogs;
                $this->customFormExtraLogsScheduledForDeletion->clear();
            }
            $this->customFormExtraLogsScheduledForDeletion[]= $customFormExtraLog;
            $customFormExtraLog->setDevice(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Device is new, it will return
     * an empty collection; or if this Device has previously
     * been saved, it will retrieve related CustomFormExtraLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Device.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCustomFormExtraLog[] List of ChildCustomFormExtraLog objects
     */
    public function getCustomFormExtraLogsJoinSection(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCustomFormExtraLogQuery::create(null, $criteria);
        $query->joinWith('Section', $joinBehavior);

        return $this->getCustomFormExtraLogs($query, $con);
    }

    /**
     * Clears out the collMedias collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMedias()
     */
    public function clearMedias()
    {
        $this->collMedias = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMedias collection loaded partially.
     */
    public function resetPartialMedias($v = true)
    {
        $this->collMediasPartial = $v;
    }

    /**
     * Initializes the collMedias collection.
     *
     * By default this just sets the collMedias collection to an empty array (like clearcollMedias());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMedias($overrideExisting = true)
    {
        if (null !== $this->collMedias && !$overrideExisting) {
            return;
        }

        $collectionClassName = MediaTableMap::getTableMap()->getCollectionClassName();

        $this->collMedias = new $collectionClassName;
        $this->collMedias->setModel('\Database\HubPlus\Media');
    }

    /**
     * Gets an array of ChildMedia objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDevice is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMedia[] List of ChildMedia objects
     * @throws PropelException
     */
    public function getMedias(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMediasPartial && !$this->isNew();
        if (null === $this->collMedias || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMedias) {
                // return empty collection
                $this->initMedias();
            } else {
                $collMedias = ChildMediaQuery::create(null, $criteria)
                    ->filterByDevice($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMediasPartial && count($collMedias)) {
                        $this->initMedias(false);

                        foreach ($collMedias as $obj) {
                            if (false == $this->collMedias->contains($obj)) {
                                $this->collMedias->append($obj);
                            }
                        }

                        $this->collMediasPartial = true;
                    }

                    return $collMedias;
                }

                if ($partial && $this->collMedias) {
                    foreach ($this->collMedias as $obj) {
                        if ($obj->isNew()) {
                            $collMedias[] = $obj;
                        }
                    }
                }

                $this->collMedias = $collMedias;
                $this->collMediasPartial = false;
            }
        }

        return $this->collMedias;
    }

    /**
     * Sets a collection of ChildMedia objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $medias A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function setMedias(Collection $medias, ConnectionInterface $con = null)
    {
        /** @var ChildMedia[] $mediasToDelete */
        $mediasToDelete = $this->getMedias(new Criteria(), $con)->diff($medias);


        $this->mediasScheduledForDeletion = $mediasToDelete;

        foreach ($mediasToDelete as $mediaRemoved) {
            $mediaRemoved->setDevice(null);
        }

        $this->collMedias = null;
        foreach ($medias as $media) {
            $this->addMedia($media);
        }

        $this->collMedias = $medias;
        $this->collMediasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Media objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Media objects.
     * @throws PropelException
     */
    public function countMedias(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMediasPartial && !$this->isNew();
        if (null === $this->collMedias || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMedias) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMedias());
            }

            $query = ChildMediaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDevice($this)
                ->count($con);
        }

        return count($this->collMedias);
    }

    /**
     * Method called to associate a ChildMedia object to this object
     * through the ChildMedia foreign key attribute.
     *
     * @param  ChildMedia $l ChildMedia
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function addMedia(ChildMedia $l)
    {
        if ($this->collMedias === null) {
            $this->initMedias();
            $this->collMediasPartial = true;
        }

        if (!$this->collMedias->contains($l)) {
            $this->doAddMedia($l);

            if ($this->mediasScheduledForDeletion and $this->mediasScheduledForDeletion->contains($l)) {
                $this->mediasScheduledForDeletion->remove($this->mediasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMedia $media The ChildMedia object to add.
     */
    protected function doAddMedia(ChildMedia $media)
    {
        $this->collMedias[]= $media;
        $media->setDevice($this);
    }

    /**
     * @param  ChildMedia $media The ChildMedia object to remove.
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function removeMedia(ChildMedia $media)
    {
        if ($this->getMedias()->contains($media)) {
            $pos = $this->collMedias->search($media);
            $this->collMedias->remove($pos);
            if (null === $this->mediasScheduledForDeletion) {
                $this->mediasScheduledForDeletion = clone $this->collMedias;
                $this->mediasScheduledForDeletion->clear();
            }
            $this->mediasScheduledForDeletion[]= $media;
            $media->setDevice(null);
        }

        return $this;
    }

    /**
     * Clears out the collMediaExtraLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMediaExtraLogs()
     */
    public function clearMediaExtraLogs()
    {
        $this->collMediaExtraLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMediaExtraLogs collection loaded partially.
     */
    public function resetPartialMediaExtraLogs($v = true)
    {
        $this->collMediaExtraLogsPartial = $v;
    }

    /**
     * Initializes the collMediaExtraLogs collection.
     *
     * By default this just sets the collMediaExtraLogs collection to an empty array (like clearcollMediaExtraLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMediaExtraLogs($overrideExisting = true)
    {
        if (null !== $this->collMediaExtraLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = MediaExtraLogTableMap::getTableMap()->getCollectionClassName();

        $this->collMediaExtraLogs = new $collectionClassName;
        $this->collMediaExtraLogs->setModel('\Database\HubPlus\MediaExtraLog');
    }

    /**
     * Gets an array of ChildMediaExtraLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDevice is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMediaExtraLog[] List of ChildMediaExtraLog objects
     * @throws PropelException
     */
    public function getMediaExtraLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMediaExtraLogsPartial && !$this->isNew();
        if (null === $this->collMediaExtraLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collMediaExtraLogs) {
                // return empty collection
                $this->initMediaExtraLogs();
            } else {
                $collMediaExtraLogs = ChildMediaExtraLogQuery::create(null, $criteria)
                    ->filterByDevice($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMediaExtraLogsPartial && count($collMediaExtraLogs)) {
                        $this->initMediaExtraLogs(false);

                        foreach ($collMediaExtraLogs as $obj) {
                            if (false == $this->collMediaExtraLogs->contains($obj)) {
                                $this->collMediaExtraLogs->append($obj);
                            }
                        }

                        $this->collMediaExtraLogsPartial = true;
                    }

                    return $collMediaExtraLogs;
                }

                if ($partial && $this->collMediaExtraLogs) {
                    foreach ($this->collMediaExtraLogs as $obj) {
                        if ($obj->isNew()) {
                            $collMediaExtraLogs[] = $obj;
                        }
                    }
                }

                $this->collMediaExtraLogs = $collMediaExtraLogs;
                $this->collMediaExtraLogsPartial = false;
            }
        }

        return $this->collMediaExtraLogs;
    }

    /**
     * Sets a collection of ChildMediaExtraLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $mediaExtraLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function setMediaExtraLogs(Collection $mediaExtraLogs, ConnectionInterface $con = null)
    {
        /** @var ChildMediaExtraLog[] $mediaExtraLogsToDelete */
        $mediaExtraLogsToDelete = $this->getMediaExtraLogs(new Criteria(), $con)->diff($mediaExtraLogs);


        $this->mediaExtraLogsScheduledForDeletion = $mediaExtraLogsToDelete;

        foreach ($mediaExtraLogsToDelete as $mediaExtraLogRemoved) {
            $mediaExtraLogRemoved->setDevice(null);
        }

        $this->collMediaExtraLogs = null;
        foreach ($mediaExtraLogs as $mediaExtraLog) {
            $this->addMediaExtraLog($mediaExtraLog);
        }

        $this->collMediaExtraLogs = $mediaExtraLogs;
        $this->collMediaExtraLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MediaExtraLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MediaExtraLog objects.
     * @throws PropelException
     */
    public function countMediaExtraLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMediaExtraLogsPartial && !$this->isNew();
        if (null === $this->collMediaExtraLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMediaExtraLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMediaExtraLogs());
            }

            $query = ChildMediaExtraLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDevice($this)
                ->count($con);
        }

        return count($this->collMediaExtraLogs);
    }

    /**
     * Method called to associate a ChildMediaExtraLog object to this object
     * through the ChildMediaExtraLog foreign key attribute.
     *
     * @param  ChildMediaExtraLog $l ChildMediaExtraLog
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function addMediaExtraLog(ChildMediaExtraLog $l)
    {
        if ($this->collMediaExtraLogs === null) {
            $this->initMediaExtraLogs();
            $this->collMediaExtraLogsPartial = true;
        }

        if (!$this->collMediaExtraLogs->contains($l)) {
            $this->doAddMediaExtraLog($l);

            if ($this->mediaExtraLogsScheduledForDeletion and $this->mediaExtraLogsScheduledForDeletion->contains($l)) {
                $this->mediaExtraLogsScheduledForDeletion->remove($this->mediaExtraLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMediaExtraLog $mediaExtraLog The ChildMediaExtraLog object to add.
     */
    protected function doAddMediaExtraLog(ChildMediaExtraLog $mediaExtraLog)
    {
        $this->collMediaExtraLogs[]= $mediaExtraLog;
        $mediaExtraLog->setDevice($this);
    }

    /**
     * @param  ChildMediaExtraLog $mediaExtraLog The ChildMediaExtraLog object to remove.
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function removeMediaExtraLog(ChildMediaExtraLog $mediaExtraLog)
    {
        if ($this->getMediaExtraLogs()->contains($mediaExtraLog)) {
            $pos = $this->collMediaExtraLogs->search($mediaExtraLog);
            $this->collMediaExtraLogs->remove($pos);
            if (null === $this->mediaExtraLogsScheduledForDeletion) {
                $this->mediaExtraLogsScheduledForDeletion = clone $this->collMediaExtraLogs;
                $this->mediaExtraLogsScheduledForDeletion->clear();
            }
            $this->mediaExtraLogsScheduledForDeletion[]= $mediaExtraLog;
            $mediaExtraLog->setDevice(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Device is new, it will return
     * an empty collection; or if this Device has previously
     * been saved, it will retrieve related MediaExtraLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Device.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildMediaExtraLog[] List of ChildMediaExtraLog objects
     */
    public function getMediaExtraLogsJoinMedia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildMediaExtraLogQuery::create(null, $criteria);
        $query->joinWith('Media', $joinBehavior);

        return $this->getMediaExtraLogs($query, $con);
    }

    /**
     * Clears out the collPostExtraLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostExtraLogs()
     */
    public function clearPostExtraLogs()
    {
        $this->collPostExtraLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostExtraLogs collection loaded partially.
     */
    public function resetPartialPostExtraLogs($v = true)
    {
        $this->collPostExtraLogsPartial = $v;
    }

    /**
     * Initializes the collPostExtraLogs collection.
     *
     * By default this just sets the collPostExtraLogs collection to an empty array (like clearcollPostExtraLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostExtraLogs($overrideExisting = true)
    {
        if (null !== $this->collPostExtraLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostExtraLogTableMap::getTableMap()->getCollectionClassName();

        $this->collPostExtraLogs = new $collectionClassName;
        $this->collPostExtraLogs->setModel('\Database\HubPlus\PostExtraLog');
    }

    /**
     * Gets an array of ChildPostExtraLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDevice is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostExtraLog[] List of ChildPostExtraLog objects
     * @throws PropelException
     */
    public function getPostExtraLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostExtraLogsPartial && !$this->isNew();
        if (null === $this->collPostExtraLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostExtraLogs) {
                // return empty collection
                $this->initPostExtraLogs();
            } else {
                $collPostExtraLogs = ChildPostExtraLogQuery::create(null, $criteria)
                    ->filterByDevice($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostExtraLogsPartial && count($collPostExtraLogs)) {
                        $this->initPostExtraLogs(false);

                        foreach ($collPostExtraLogs as $obj) {
                            if (false == $this->collPostExtraLogs->contains($obj)) {
                                $this->collPostExtraLogs->append($obj);
                            }
                        }

                        $this->collPostExtraLogsPartial = true;
                    }

                    return $collPostExtraLogs;
                }

                if ($partial && $this->collPostExtraLogs) {
                    foreach ($this->collPostExtraLogs as $obj) {
                        if ($obj->isNew()) {
                            $collPostExtraLogs[] = $obj;
                        }
                    }
                }

                $this->collPostExtraLogs = $collPostExtraLogs;
                $this->collPostExtraLogsPartial = false;
            }
        }

        return $this->collPostExtraLogs;
    }

    /**
     * Sets a collection of ChildPostExtraLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postExtraLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function setPostExtraLogs(Collection $postExtraLogs, ConnectionInterface $con = null)
    {
        /** @var ChildPostExtraLog[] $postExtraLogsToDelete */
        $postExtraLogsToDelete = $this->getPostExtraLogs(new Criteria(), $con)->diff($postExtraLogs);


        $this->postExtraLogsScheduledForDeletion = $postExtraLogsToDelete;

        foreach ($postExtraLogsToDelete as $postExtraLogRemoved) {
            $postExtraLogRemoved->setDevice(null);
        }

        $this->collPostExtraLogs = null;
        foreach ($postExtraLogs as $postExtraLog) {
            $this->addPostExtraLog($postExtraLog);
        }

        $this->collPostExtraLogs = $postExtraLogs;
        $this->collPostExtraLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostExtraLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostExtraLog objects.
     * @throws PropelException
     */
    public function countPostExtraLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostExtraLogsPartial && !$this->isNew();
        if (null === $this->collPostExtraLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostExtraLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostExtraLogs());
            }

            $query = ChildPostExtraLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDevice($this)
                ->count($con);
        }

        return count($this->collPostExtraLogs);
    }

    /**
     * Method called to associate a ChildPostExtraLog object to this object
     * through the ChildPostExtraLog foreign key attribute.
     *
     * @param  ChildPostExtraLog $l ChildPostExtraLog
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function addPostExtraLog(ChildPostExtraLog $l)
    {
        if ($this->collPostExtraLogs === null) {
            $this->initPostExtraLogs();
            $this->collPostExtraLogsPartial = true;
        }

        if (!$this->collPostExtraLogs->contains($l)) {
            $this->doAddPostExtraLog($l);

            if ($this->postExtraLogsScheduledForDeletion and $this->postExtraLogsScheduledForDeletion->contains($l)) {
                $this->postExtraLogsScheduledForDeletion->remove($this->postExtraLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostExtraLog $postExtraLog The ChildPostExtraLog object to add.
     */
    protected function doAddPostExtraLog(ChildPostExtraLog $postExtraLog)
    {
        $this->collPostExtraLogs[]= $postExtraLog;
        $postExtraLog->setDevice($this);
    }

    /**
     * @param  ChildPostExtraLog $postExtraLog The ChildPostExtraLog object to remove.
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function removePostExtraLog(ChildPostExtraLog $postExtraLog)
    {
        if ($this->getPostExtraLogs()->contains($postExtraLog)) {
            $pos = $this->collPostExtraLogs->search($postExtraLog);
            $this->collPostExtraLogs->remove($pos);
            if (null === $this->postExtraLogsScheduledForDeletion) {
                $this->postExtraLogsScheduledForDeletion = clone $this->collPostExtraLogs;
                $this->postExtraLogsScheduledForDeletion->clear();
            }
            $this->postExtraLogsScheduledForDeletion[]= $postExtraLog;
            $postExtraLog->setDevice(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Device is new, it will return
     * an empty collection; or if this Device has previously
     * been saved, it will retrieve related PostExtraLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Device.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostExtraLog[] List of ChildPostExtraLog objects
     */
    public function getPostExtraLogsJoinPost(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostExtraLogQuery::create(null, $criteria);
        $query->joinWith('Post', $joinBehavior);

        return $this->getPostExtraLogs($query, $con);
    }

    /**
     * Clears out the collPushNotificationDevices collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPushNotificationDevices()
     */
    public function clearPushNotificationDevices()
    {
        $this->collPushNotificationDevices = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPushNotificationDevices collection loaded partially.
     */
    public function resetPartialPushNotificationDevices($v = true)
    {
        $this->collPushNotificationDevicesPartial = $v;
    }

    /**
     * Initializes the collPushNotificationDevices collection.
     *
     * By default this just sets the collPushNotificationDevices collection to an empty array (like clearcollPushNotificationDevices());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPushNotificationDevices($overrideExisting = true)
    {
        if (null !== $this->collPushNotificationDevices && !$overrideExisting) {
            return;
        }

        $collectionClassName = PushNotificationDeviceTableMap::getTableMap()->getCollectionClassName();

        $this->collPushNotificationDevices = new $collectionClassName;
        $this->collPushNotificationDevices->setModel('\Database\HubPlus\PushNotificationDevice');
    }

    /**
     * Gets an array of ChildPushNotificationDevice objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDevice is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPushNotificationDevice[] List of ChildPushNotificationDevice objects
     * @throws PropelException
     */
    public function getPushNotificationDevices(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPushNotificationDevicesPartial && !$this->isNew();
        if (null === $this->collPushNotificationDevices || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPushNotificationDevices) {
                // return empty collection
                $this->initPushNotificationDevices();
            } else {
                $collPushNotificationDevices = ChildPushNotificationDeviceQuery::create(null, $criteria)
                    ->filterByDevice($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPushNotificationDevicesPartial && count($collPushNotificationDevices)) {
                        $this->initPushNotificationDevices(false);

                        foreach ($collPushNotificationDevices as $obj) {
                            if (false == $this->collPushNotificationDevices->contains($obj)) {
                                $this->collPushNotificationDevices->append($obj);
                            }
                        }

                        $this->collPushNotificationDevicesPartial = true;
                    }

                    return $collPushNotificationDevices;
                }

                if ($partial && $this->collPushNotificationDevices) {
                    foreach ($this->collPushNotificationDevices as $obj) {
                        if ($obj->isNew()) {
                            $collPushNotificationDevices[] = $obj;
                        }
                    }
                }

                $this->collPushNotificationDevices = $collPushNotificationDevices;
                $this->collPushNotificationDevicesPartial = false;
            }
        }

        return $this->collPushNotificationDevices;
    }

    /**
     * Sets a collection of ChildPushNotificationDevice objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $pushNotificationDevices A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function setPushNotificationDevices(Collection $pushNotificationDevices, ConnectionInterface $con = null)
    {
        /** @var ChildPushNotificationDevice[] $pushNotificationDevicesToDelete */
        $pushNotificationDevicesToDelete = $this->getPushNotificationDevices(new Criteria(), $con)->diff($pushNotificationDevices);


        $this->pushNotificationDevicesScheduledForDeletion = $pushNotificationDevicesToDelete;

        foreach ($pushNotificationDevicesToDelete as $pushNotificationDeviceRemoved) {
            $pushNotificationDeviceRemoved->setDevice(null);
        }

        $this->collPushNotificationDevices = null;
        foreach ($pushNotificationDevices as $pushNotificationDevice) {
            $this->addPushNotificationDevice($pushNotificationDevice);
        }

        $this->collPushNotificationDevices = $pushNotificationDevices;
        $this->collPushNotificationDevicesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PushNotificationDevice objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PushNotificationDevice objects.
     * @throws PropelException
     */
    public function countPushNotificationDevices(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPushNotificationDevicesPartial && !$this->isNew();
        if (null === $this->collPushNotificationDevices || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPushNotificationDevices) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPushNotificationDevices());
            }

            $query = ChildPushNotificationDeviceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDevice($this)
                ->count($con);
        }

        return count($this->collPushNotificationDevices);
    }

    /**
     * Method called to associate a ChildPushNotificationDevice object to this object
     * through the ChildPushNotificationDevice foreign key attribute.
     *
     * @param  ChildPushNotificationDevice $l ChildPushNotificationDevice
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function addPushNotificationDevice(ChildPushNotificationDevice $l)
    {
        if ($this->collPushNotificationDevices === null) {
            $this->initPushNotificationDevices();
            $this->collPushNotificationDevicesPartial = true;
        }

        if (!$this->collPushNotificationDevices->contains($l)) {
            $this->doAddPushNotificationDevice($l);

            if ($this->pushNotificationDevicesScheduledForDeletion and $this->pushNotificationDevicesScheduledForDeletion->contains($l)) {
                $this->pushNotificationDevicesScheduledForDeletion->remove($this->pushNotificationDevicesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPushNotificationDevice $pushNotificationDevice The ChildPushNotificationDevice object to add.
     */
    protected function doAddPushNotificationDevice(ChildPushNotificationDevice $pushNotificationDevice)
    {
        $this->collPushNotificationDevices[]= $pushNotificationDevice;
        $pushNotificationDevice->setDevice($this);
    }

    /**
     * @param  ChildPushNotificationDevice $pushNotificationDevice The ChildPushNotificationDevice object to remove.
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function removePushNotificationDevice(ChildPushNotificationDevice $pushNotificationDevice)
    {
        if ($this->getPushNotificationDevices()->contains($pushNotificationDevice)) {
            $pos = $this->collPushNotificationDevices->search($pushNotificationDevice);
            $this->collPushNotificationDevices->remove($pos);
            if (null === $this->pushNotificationDevicesScheduledForDeletion) {
                $this->pushNotificationDevicesScheduledForDeletion = clone $this->collPushNotificationDevices;
                $this->pushNotificationDevicesScheduledForDeletion->clear();
            }
            $this->pushNotificationDevicesScheduledForDeletion[]= clone $pushNotificationDevice;
            $pushNotificationDevice->setDevice(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Device is new, it will return
     * an empty collection; or if this Device has previously
     * been saved, it will retrieve related PushNotificationDevices from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Device.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPushNotificationDevice[] List of ChildPushNotificationDevice objects
     */
    public function getPushNotificationDevicesJoinPushNotification(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPushNotificationDeviceQuery::create(null, $criteria);
        $query->joinWith('PushNotification', $joinBehavior);

        return $this->getPushNotificationDevices($query, $con);
    }

    /**
     * Clears out the collRemoteExtraLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRemoteExtraLogs()
     */
    public function clearRemoteExtraLogs()
    {
        $this->collRemoteExtraLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRemoteExtraLogs collection loaded partially.
     */
    public function resetPartialRemoteExtraLogs($v = true)
    {
        $this->collRemoteExtraLogsPartial = $v;
    }

    /**
     * Initializes the collRemoteExtraLogs collection.
     *
     * By default this just sets the collRemoteExtraLogs collection to an empty array (like clearcollRemoteExtraLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRemoteExtraLogs($overrideExisting = true)
    {
        if (null !== $this->collRemoteExtraLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = RemoteExtraLogTableMap::getTableMap()->getCollectionClassName();

        $this->collRemoteExtraLogs = new $collectionClassName;
        $this->collRemoteExtraLogs->setModel('\Database\HubPlus\RemoteExtraLog');
    }

    /**
     * Gets an array of ChildRemoteExtraLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDevice is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildRemoteExtraLog[] List of ChildRemoteExtraLog objects
     * @throws PropelException
     */
    public function getRemoteExtraLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRemoteExtraLogsPartial && !$this->isNew();
        if (null === $this->collRemoteExtraLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRemoteExtraLogs) {
                // return empty collection
                $this->initRemoteExtraLogs();
            } else {
                $collRemoteExtraLogs = ChildRemoteExtraLogQuery::create(null, $criteria)
                    ->filterByDevice($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRemoteExtraLogsPartial && count($collRemoteExtraLogs)) {
                        $this->initRemoteExtraLogs(false);

                        foreach ($collRemoteExtraLogs as $obj) {
                            if (false == $this->collRemoteExtraLogs->contains($obj)) {
                                $this->collRemoteExtraLogs->append($obj);
                            }
                        }

                        $this->collRemoteExtraLogsPartial = true;
                    }

                    return $collRemoteExtraLogs;
                }

                if ($partial && $this->collRemoteExtraLogs) {
                    foreach ($this->collRemoteExtraLogs as $obj) {
                        if ($obj->isNew()) {
                            $collRemoteExtraLogs[] = $obj;
                        }
                    }
                }

                $this->collRemoteExtraLogs = $collRemoteExtraLogs;
                $this->collRemoteExtraLogsPartial = false;
            }
        }

        return $this->collRemoteExtraLogs;
    }

    /**
     * Sets a collection of ChildRemoteExtraLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $remoteExtraLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function setRemoteExtraLogs(Collection $remoteExtraLogs, ConnectionInterface $con = null)
    {
        /** @var ChildRemoteExtraLog[] $remoteExtraLogsToDelete */
        $remoteExtraLogsToDelete = $this->getRemoteExtraLogs(new Criteria(), $con)->diff($remoteExtraLogs);


        $this->remoteExtraLogsScheduledForDeletion = $remoteExtraLogsToDelete;

        foreach ($remoteExtraLogsToDelete as $remoteExtraLogRemoved) {
            $remoteExtraLogRemoved->setDevice(null);
        }

        $this->collRemoteExtraLogs = null;
        foreach ($remoteExtraLogs as $remoteExtraLog) {
            $this->addRemoteExtraLog($remoteExtraLog);
        }

        $this->collRemoteExtraLogs = $remoteExtraLogs;
        $this->collRemoteExtraLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RemoteExtraLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related RemoteExtraLog objects.
     * @throws PropelException
     */
    public function countRemoteExtraLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRemoteExtraLogsPartial && !$this->isNew();
        if (null === $this->collRemoteExtraLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRemoteExtraLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRemoteExtraLogs());
            }

            $query = ChildRemoteExtraLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDevice($this)
                ->count($con);
        }

        return count($this->collRemoteExtraLogs);
    }

    /**
     * Method called to associate a ChildRemoteExtraLog object to this object
     * through the ChildRemoteExtraLog foreign key attribute.
     *
     * @param  ChildRemoteExtraLog $l ChildRemoteExtraLog
     * @return $this|\Database\HubPlus\Device The current object (for fluent API support)
     */
    public function addRemoteExtraLog(ChildRemoteExtraLog $l)
    {
        if ($this->collRemoteExtraLogs === null) {
            $this->initRemoteExtraLogs();
            $this->collRemoteExtraLogsPartial = true;
        }

        if (!$this->collRemoteExtraLogs->contains($l)) {
            $this->doAddRemoteExtraLog($l);

            if ($this->remoteExtraLogsScheduledForDeletion and $this->remoteExtraLogsScheduledForDeletion->contains($l)) {
                $this->remoteExtraLogsScheduledForDeletion->remove($this->remoteExtraLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildRemoteExtraLog $remoteExtraLog The ChildRemoteExtraLog object to add.
     */
    protected function doAddRemoteExtraLog(ChildRemoteExtraLog $remoteExtraLog)
    {
        $this->collRemoteExtraLogs[]= $remoteExtraLog;
        $remoteExtraLog->setDevice($this);
    }

    /**
     * @param  ChildRemoteExtraLog $remoteExtraLog The ChildRemoteExtraLog object to remove.
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function removeRemoteExtraLog(ChildRemoteExtraLog $remoteExtraLog)
    {
        if ($this->getRemoteExtraLogs()->contains($remoteExtraLog)) {
            $pos = $this->collRemoteExtraLogs->search($remoteExtraLog);
            $this->collRemoteExtraLogs->remove($pos);
            if (null === $this->remoteExtraLogsScheduledForDeletion) {
                $this->remoteExtraLogsScheduledForDeletion = clone $this->collRemoteExtraLogs;
                $this->remoteExtraLogsScheduledForDeletion->clear();
            }
            $this->remoteExtraLogsScheduledForDeletion[]= $remoteExtraLog;
            $remoteExtraLog->setDevice(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Device is new, it will return
     * an empty collection; or if this Device has previously
     * been saved, it will retrieve related RemoteExtraLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Device.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildRemoteExtraLog[] List of ChildRemoteExtraLog objects
     */
    public function getRemoteExtraLogsJoinSectionConnector(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildRemoteExtraLogQuery::create(null, $criteria);
        $query->joinWith('SectionConnector', $joinBehavior);

        return $this->getRemoteExtraLogs($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aUserApp) {
            $this->aUserApp->removeDevice($this);
        }
        $this->id = null;
        $this->user_id = null;
        $this->status = null;
        $this->type = null;
        $this->os = null;
        $this->os_version = null;
        $this->enable_notification = null;
        $this->api_version = null;
        $this->language = null;
        $this->demo = null;
        $this->token = null;
        $this->api_key = null;
        $this->ip_address = null;
        $this->deleted_at = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCustomFormExtraLogs) {
                foreach ($this->collCustomFormExtraLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMedias) {
                foreach ($this->collMedias as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMediaExtraLogs) {
                foreach ($this->collMediaExtraLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPostExtraLogs) {
                foreach ($this->collPostExtraLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPushNotificationDevices) {
                foreach ($this->collPushNotificationDevices as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRemoteExtraLogs) {
                foreach ($this->collRemoteExtraLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCustomFormExtraLogs = null;
        $this->collMedias = null;
        $this->collMediaExtraLogs = null;
        $this->collPostExtraLogs = null;
        $this->collPushNotificationDevices = null;
        $this->collRemoteExtraLogs = null;
        $this->aUserApp = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(DeviceTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildDevice The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[DeviceTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildDeviceArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildDeviceArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildDeviceArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildDeviceArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildDeviceArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildDevice The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setId($archive->getId());
        }
        $this->setUserId($archive->getUserId());
        $this->setStatus($archive->getStatus());
        $this->setType($archive->getType());
        $this->setOs($archive->getOs());
        $this->setOsVersion($archive->getOsVersion());
        $this->setEnableNotification($archive->getEnableNotification());
        $this->setApiVersion($archive->getApiVersion());
        $this->setLanguage($archive->getLanguage());
        $this->setDemo($archive->getDemo());
        $this->setToken($archive->getToken());
        $this->setApiKey($archive->getApiKey());
        $this->setIpAddress($archive->getIpAddress());
        $this->setDeletedAt($archive->getDeletedAt());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildDevice The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
