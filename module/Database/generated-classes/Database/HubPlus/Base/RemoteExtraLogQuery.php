<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\RemoteExtraLog as ChildRemoteExtraLog;
use Database\HubPlus\RemoteExtraLogQuery as ChildRemoteExtraLogQuery;
use Database\HubPlus\Map\RemoteExtraLogTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'remote_extra_log' table.
 *
 *
 *
 * @method     ChildRemoteExtraLogQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildRemoteExtraLogQuery orderByConnectorId($order = Criteria::ASC) Order by the connector_id column
 * @method     ChildRemoteExtraLogQuery orderByRemoteId($order = Criteria::ASC) Order by the remote_id column
 * @method     ChildRemoteExtraLogQuery orderByDeviceId($order = Criteria::ASC) Order by the device_id column
 * @method     ChildRemoteExtraLogQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildRemoteExtraLogQuery orderByContentType($order = Criteria::ASC) Order by the content_type column
 * @method     ChildRemoteExtraLogQuery orderByTimestamp($order = Criteria::ASC) Order by the timestamp column
 *
 * @method     ChildRemoteExtraLogQuery groupById() Group by the id column
 * @method     ChildRemoteExtraLogQuery groupByConnectorId() Group by the connector_id column
 * @method     ChildRemoteExtraLogQuery groupByRemoteId() Group by the remote_id column
 * @method     ChildRemoteExtraLogQuery groupByDeviceId() Group by the device_id column
 * @method     ChildRemoteExtraLogQuery groupByType() Group by the type column
 * @method     ChildRemoteExtraLogQuery groupByContentType() Group by the content_type column
 * @method     ChildRemoteExtraLogQuery groupByTimestamp() Group by the timestamp column
 *
 * @method     ChildRemoteExtraLogQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRemoteExtraLogQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRemoteExtraLogQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRemoteExtraLogQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRemoteExtraLogQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRemoteExtraLogQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRemoteExtraLogQuery leftJoinDevice($relationAlias = null) Adds a LEFT JOIN clause to the query using the Device relation
 * @method     ChildRemoteExtraLogQuery rightJoinDevice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Device relation
 * @method     ChildRemoteExtraLogQuery innerJoinDevice($relationAlias = null) Adds a INNER JOIN clause to the query using the Device relation
 *
 * @method     ChildRemoteExtraLogQuery joinWithDevice($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Device relation
 *
 * @method     ChildRemoteExtraLogQuery leftJoinWithDevice() Adds a LEFT JOIN clause and with to the query using the Device relation
 * @method     ChildRemoteExtraLogQuery rightJoinWithDevice() Adds a RIGHT JOIN clause and with to the query using the Device relation
 * @method     ChildRemoteExtraLogQuery innerJoinWithDevice() Adds a INNER JOIN clause and with to the query using the Device relation
 *
 * @method     ChildRemoteExtraLogQuery leftJoinSectionConnector($relationAlias = null) Adds a LEFT JOIN clause to the query using the SectionConnector relation
 * @method     ChildRemoteExtraLogQuery rightJoinSectionConnector($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SectionConnector relation
 * @method     ChildRemoteExtraLogQuery innerJoinSectionConnector($relationAlias = null) Adds a INNER JOIN clause to the query using the SectionConnector relation
 *
 * @method     ChildRemoteExtraLogQuery joinWithSectionConnector($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SectionConnector relation
 *
 * @method     ChildRemoteExtraLogQuery leftJoinWithSectionConnector() Adds a LEFT JOIN clause and with to the query using the SectionConnector relation
 * @method     ChildRemoteExtraLogQuery rightJoinWithSectionConnector() Adds a RIGHT JOIN clause and with to the query using the SectionConnector relation
 * @method     ChildRemoteExtraLogQuery innerJoinWithSectionConnector() Adds a INNER JOIN clause and with to the query using the SectionConnector relation
 *
 * @method     \Database\HubPlus\DeviceQuery|\Database\HubPlus\SectionConnectorQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRemoteExtraLog findOne(ConnectionInterface $con = null) Return the first ChildRemoteExtraLog matching the query
 * @method     ChildRemoteExtraLog findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRemoteExtraLog matching the query, or a new ChildRemoteExtraLog object populated from the query conditions when no match is found
 *
 * @method     ChildRemoteExtraLog findOneById(int $id) Return the first ChildRemoteExtraLog filtered by the id column
 * @method     ChildRemoteExtraLog findOneByConnectorId(int $connector_id) Return the first ChildRemoteExtraLog filtered by the connector_id column
 * @method     ChildRemoteExtraLog findOneByRemoteId(string $remote_id) Return the first ChildRemoteExtraLog filtered by the remote_id column
 * @method     ChildRemoteExtraLog findOneByDeviceId(int $device_id) Return the first ChildRemoteExtraLog filtered by the device_id column
 * @method     ChildRemoteExtraLog findOneByType(string $type) Return the first ChildRemoteExtraLog filtered by the type column
 * @method     ChildRemoteExtraLog findOneByContentType(string $content_type) Return the first ChildRemoteExtraLog filtered by the content_type column
 * @method     ChildRemoteExtraLog findOneByTimestamp(string $timestamp) Return the first ChildRemoteExtraLog filtered by the timestamp column *

 * @method     ChildRemoteExtraLog requirePk($key, ConnectionInterface $con = null) Return the ChildRemoteExtraLog by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteExtraLog requireOne(ConnectionInterface $con = null) Return the first ChildRemoteExtraLog matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRemoteExtraLog requireOneById(int $id) Return the first ChildRemoteExtraLog filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteExtraLog requireOneByConnectorId(int $connector_id) Return the first ChildRemoteExtraLog filtered by the connector_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteExtraLog requireOneByRemoteId(string $remote_id) Return the first ChildRemoteExtraLog filtered by the remote_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteExtraLog requireOneByDeviceId(int $device_id) Return the first ChildRemoteExtraLog filtered by the device_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteExtraLog requireOneByType(string $type) Return the first ChildRemoteExtraLog filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteExtraLog requireOneByContentType(string $content_type) Return the first ChildRemoteExtraLog filtered by the content_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRemoteExtraLog requireOneByTimestamp(string $timestamp) Return the first ChildRemoteExtraLog filtered by the timestamp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRemoteExtraLog[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRemoteExtraLog objects based on current ModelCriteria
 * @method     ChildRemoteExtraLog[]|ObjectCollection findById(int $id) Return ChildRemoteExtraLog objects filtered by the id column
 * @method     ChildRemoteExtraLog[]|ObjectCollection findByConnectorId(int $connector_id) Return ChildRemoteExtraLog objects filtered by the connector_id column
 * @method     ChildRemoteExtraLog[]|ObjectCollection findByRemoteId(string $remote_id) Return ChildRemoteExtraLog objects filtered by the remote_id column
 * @method     ChildRemoteExtraLog[]|ObjectCollection findByDeviceId(int $device_id) Return ChildRemoteExtraLog objects filtered by the device_id column
 * @method     ChildRemoteExtraLog[]|ObjectCollection findByType(string $type) Return ChildRemoteExtraLog objects filtered by the type column
 * @method     ChildRemoteExtraLog[]|ObjectCollection findByContentType(string $content_type) Return ChildRemoteExtraLog objects filtered by the content_type column
 * @method     ChildRemoteExtraLog[]|ObjectCollection findByTimestamp(string $timestamp) Return ChildRemoteExtraLog objects filtered by the timestamp column
 * @method     ChildRemoteExtraLog[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RemoteExtraLogQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\RemoteExtraLogQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\RemoteExtraLog', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRemoteExtraLogQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRemoteExtraLogQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRemoteExtraLogQuery) {
            return $criteria;
        }
        $query = new ChildRemoteExtraLogQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRemoteExtraLog|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RemoteExtraLogTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RemoteExtraLogTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRemoteExtraLog A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, connector_id, remote_id, device_id, type, content_type, timestamp FROM remote_extra_log WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRemoteExtraLog $obj */
            $obj = new ChildRemoteExtraLog();
            $obj->hydrate($row);
            RemoteExtraLogTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRemoteExtraLog|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RemoteExtraLogTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RemoteExtraLogTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RemoteExtraLogTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RemoteExtraLogTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteExtraLogTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the connector_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConnectorId(1234); // WHERE connector_id = 1234
     * $query->filterByConnectorId(array(12, 34)); // WHERE connector_id IN (12, 34)
     * $query->filterByConnectorId(array('min' => 12)); // WHERE connector_id > 12
     * </code>
     *
     * @see       filterBySectionConnector()
     *
     * @param     mixed $connectorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function filterByConnectorId($connectorId = null, $comparison = null)
    {
        if (is_array($connectorId)) {
            $useMinMax = false;
            if (isset($connectorId['min'])) {
                $this->addUsingAlias(RemoteExtraLogTableMap::COL_CONNECTOR_ID, $connectorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($connectorId['max'])) {
                $this->addUsingAlias(RemoteExtraLogTableMap::COL_CONNECTOR_ID, $connectorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteExtraLogTableMap::COL_CONNECTOR_ID, $connectorId, $comparison);
    }

    /**
     * Filter the query on the remote_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRemoteId('fooValue');   // WHERE remote_id = 'fooValue'
     * $query->filterByRemoteId('%fooValue%', Criteria::LIKE); // WHERE remote_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remoteId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function filterByRemoteId($remoteId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remoteId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteExtraLogTableMap::COL_REMOTE_ID, $remoteId, $comparison);
    }

    /**
     * Filter the query on the device_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDeviceId(1234); // WHERE device_id = 1234
     * $query->filterByDeviceId(array(12, 34)); // WHERE device_id IN (12, 34)
     * $query->filterByDeviceId(array('min' => 12)); // WHERE device_id > 12
     * </code>
     *
     * @see       filterByDevice()
     *
     * @param     mixed $deviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function filterByDeviceId($deviceId = null, $comparison = null)
    {
        if (is_array($deviceId)) {
            $useMinMax = false;
            if (isset($deviceId['min'])) {
                $this->addUsingAlias(RemoteExtraLogTableMap::COL_DEVICE_ID, $deviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deviceId['max'])) {
                $this->addUsingAlias(RemoteExtraLogTableMap::COL_DEVICE_ID, $deviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteExtraLogTableMap::COL_DEVICE_ID, $deviceId, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteExtraLogTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the content_type column
     *
     * Example usage:
     * <code>
     * $query->filterByContentType('fooValue');   // WHERE content_type = 'fooValue'
     * $query->filterByContentType('%fooValue%', Criteria::LIKE); // WHERE content_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contentType The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function filterByContentType($contentType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contentType)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteExtraLogTableMap::COL_CONTENT_TYPE, $contentType, $comparison);
    }

    /**
     * Filter the query on the timestamp column
     *
     * Example usage:
     * <code>
     * $query->filterByTimestamp('2011-03-14'); // WHERE timestamp = '2011-03-14'
     * $query->filterByTimestamp('now'); // WHERE timestamp = '2011-03-14'
     * $query->filterByTimestamp(array('max' => 'yesterday')); // WHERE timestamp > '2011-03-13'
     * </code>
     *
     * @param     mixed $timestamp The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function filterByTimestamp($timestamp = null, $comparison = null)
    {
        if (is_array($timestamp)) {
            $useMinMax = false;
            if (isset($timestamp['min'])) {
                $this->addUsingAlias(RemoteExtraLogTableMap::COL_TIMESTAMP, $timestamp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($timestamp['max'])) {
                $this->addUsingAlias(RemoteExtraLogTableMap::COL_TIMESTAMP, $timestamp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RemoteExtraLogTableMap::COL_TIMESTAMP, $timestamp, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\Device object
     *
     * @param \Database\HubPlus\Device|ObjectCollection $device The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function filterByDevice($device, $comparison = null)
    {
        if ($device instanceof \Database\HubPlus\Device) {
            return $this
                ->addUsingAlias(RemoteExtraLogTableMap::COL_DEVICE_ID, $device->getId(), $comparison);
        } elseif ($device instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RemoteExtraLogTableMap::COL_DEVICE_ID, $device->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDevice() only accepts arguments of type \Database\HubPlus\Device or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Device relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function joinDevice($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Device');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Device');
        }

        return $this;
    }

    /**
     * Use the Device relation Device object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\DeviceQuery A secondary query class using the current class as primary query
     */
    public function useDeviceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinDevice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Device', '\Database\HubPlus\DeviceQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\SectionConnector object
     *
     * @param \Database\HubPlus\SectionConnector|ObjectCollection $sectionConnector The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function filterBySectionConnector($sectionConnector, $comparison = null)
    {
        if ($sectionConnector instanceof \Database\HubPlus\SectionConnector) {
            return $this
                ->addUsingAlias(RemoteExtraLogTableMap::COL_CONNECTOR_ID, $sectionConnector->getId(), $comparison);
        } elseif ($sectionConnector instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RemoteExtraLogTableMap::COL_CONNECTOR_ID, $sectionConnector->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySectionConnector() only accepts arguments of type \Database\HubPlus\SectionConnector or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SectionConnector relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function joinSectionConnector($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SectionConnector');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SectionConnector');
        }

        return $this;
    }

    /**
     * Use the SectionConnector relation SectionConnector object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionConnectorQuery A secondary query class using the current class as primary query
     */
    public function useSectionConnectorQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSectionConnector($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SectionConnector', '\Database\HubPlus\SectionConnectorQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRemoteExtraLog $remoteExtraLog Object to remove from the list of results
     *
     * @return $this|ChildRemoteExtraLogQuery The current query, for fluid interface
     */
    public function prune($remoteExtraLog = null)
    {
        if ($remoteExtraLog) {
            $this->addUsingAlias(RemoteExtraLogTableMap::COL_ID, $remoteExtraLog->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the remote_extra_log table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteExtraLogTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RemoteExtraLogTableMap::clearInstancePool();
            RemoteExtraLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteExtraLogTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RemoteExtraLogTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RemoteExtraLogTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RemoteExtraLogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RemoteExtraLogQuery
