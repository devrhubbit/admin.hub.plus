<?php

namespace Database\HubPlus\Base;

use \DateTime;
use \Exception;
use \PDO;
use Database\HubPlus\Gallery as ChildGallery;
use Database\HubPlus\GalleryQuery as ChildGalleryQuery;
use Database\HubPlus\Media as ChildMedia;
use Database\HubPlus\MediaQuery as ChildMediaQuery;
use Database\HubPlus\Post as ChildPost;
use Database\HubPlus\PostAction as ChildPostAction;
use Database\HubPlus\PostActionQuery as ChildPostActionQuery;
use Database\HubPlus\PostArchive as ChildPostArchive;
use Database\HubPlus\PostArchiveQuery as ChildPostArchiveQuery;
use Database\HubPlus\PostConnector as ChildPostConnector;
use Database\HubPlus\PostConnectorQuery as ChildPostConnectorQuery;
use Database\HubPlus\PostEvent as ChildPostEvent;
use Database\HubPlus\PostEventQuery as ChildPostEventQuery;
use Database\HubPlus\PostExtraLog as ChildPostExtraLog;
use Database\HubPlus\PostExtraLogQuery as ChildPostExtraLogQuery;
use Database\HubPlus\PostLog as ChildPostLog;
use Database\HubPlus\PostLogQuery as ChildPostLogQuery;
use Database\HubPlus\PostPerson as ChildPostPerson;
use Database\HubPlus\PostPersonQuery as ChildPostPersonQuery;
use Database\HubPlus\PostPoi as ChildPostPoi;
use Database\HubPlus\PostPoiQuery as ChildPostPoiQuery;
use Database\HubPlus\PostQuery as ChildPostQuery;
use Database\HubPlus\PushNotification as ChildPushNotification;
use Database\HubPlus\PushNotificationQuery as ChildPushNotificationQuery;
use Database\HubPlus\Template as ChildTemplate;
use Database\HubPlus\TemplateQuery as ChildTemplateQuery;
use Database\HubPlus\UserBackend as ChildUserBackend;
use Database\HubPlus\UserBackendQuery as ChildUserBackendQuery;
use Database\HubPlus\Webhook as ChildWebhook;
use Database\HubPlus\WebhookQuery as ChildWebhookQuery;
use Database\HubPlus\Map\GalleryTableMap;
use Database\HubPlus\Map\PostActionTableMap;
use Database\HubPlus\Map\PostConnectorTableMap;
use Database\HubPlus\Map\PostEventTableMap;
use Database\HubPlus\Map\PostExtraLogTableMap;
use Database\HubPlus\Map\PostLogTableMap;
use Database\HubPlus\Map\PostPersonTableMap;
use Database\HubPlus\Map\PostPoiTableMap;
use Database\HubPlus\Map\PostTableMap;
use Database\HubPlus\Map\PushNotificationTableMap;
use Database\HubPlus\Map\WebhookTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'post' table.
 *
 *
 *
 * @package    propel.generator.Database.HubPlus.Base
 */
abstract class Post implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Database\\HubPlus\\Map\\PostTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the type field.
     *
     * @var        string
     */
    protected $type;

    /**
     * The value for the status field.
     *
     * @var        int
     */
    protected $status;

    /**
     * The value for the author_id field.
     *
     * @var        int
     */
    protected $author_id;

    /**
     * The value for the cover_id field.
     *
     * @var        int
     */
    protected $cover_id;

    /**
     * The value for the slug field.
     *
     * @var        string
     */
    protected $slug;

    /**
     * The value for the title field.
     *
     * @var        string
     */
    protected $title;

    /**
     * The value for the subtitle field.
     *
     * @var        string
     */
    protected $subtitle;

    /**
     * The value for the description field.
     *
     * @var        string
     */
    protected $description;

    /**
     * The value for the body field.
     *
     * @var        string
     */
    protected $body;

    /**
     * The value for the search field.
     *
     * @var        string
     */
    protected $search;

    /**
     * The value for the template_id field.
     *
     * @var        int
     */
    protected $template_id;

    /**
     * The value for the layout field.
     *
     * @var        string
     */
    protected $layout;

    /**
     * The value for the tags field.
     *
     * @var        string
     */
    protected $tags;

    /**
     * The value for the tags_user field.
     *
     * @var        string
     */
    protected $tags_user;

    /**
     * The value for the row_data field.
     *
     * @var        string
     */
    protected $row_data;

    /**
     * The value for the count_like field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $count_like;

    /**
     * The value for the count_share field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $count_share;

    /**
     * The value for the deleted_at field.
     *
     * @var        DateTime
     */
    protected $deleted_at;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime
     */
    protected $updated_at;

    /**
     * @var        ChildTemplate
     */
    protected $aTemplate;

    /**
     * @var        ChildMedia
     */
    protected $aMedia;

    /**
     * @var        ChildUserBackend
     */
    protected $aUserBackend;

    /**
     * @var        ObjectCollection|ChildGallery[] Collection to store aggregation of ChildGallery objects.
     */
    protected $collGalleries;
    protected $collGalleriesPartial;

    /**
     * @var        ObjectCollection|ChildPostAction[] Collection to store aggregation of ChildPostAction objects.
     */
    protected $collPostActionsRelatedByPostId;
    protected $collPostActionsRelatedByPostIdPartial;

    /**
     * @var        ObjectCollection|ChildPostAction[] Collection to store aggregation of ChildPostAction objects.
     */
    protected $collPostActionsRelatedByContentId;
    protected $collPostActionsRelatedByContentIdPartial;

    /**
     * @var        ObjectCollection|ChildPostConnector[] Collection to store aggregation of ChildPostConnector objects.
     */
    protected $collPostConnectors;
    protected $collPostConnectorsPartial;

    /**
     * @var        ObjectCollection|ChildPostEvent[] Collection to store aggregation of ChildPostEvent objects.
     */
    protected $collPostEvents;
    protected $collPostEventsPartial;

    /**
     * @var        ObjectCollection|ChildPostExtraLog[] Collection to store aggregation of ChildPostExtraLog objects.
     */
    protected $collPostExtraLogs;
    protected $collPostExtraLogsPartial;

    /**
     * @var        ObjectCollection|ChildPostLog[] Collection to store aggregation of ChildPostLog objects.
     */
    protected $collPostLogs;
    protected $collPostLogsPartial;

    /**
     * @var        ObjectCollection|ChildPostPerson[] Collection to store aggregation of ChildPostPerson objects.
     */
    protected $collPostpeople;
    protected $collPostpeoplePartial;

    /**
     * @var        ObjectCollection|ChildPostPoi[] Collection to store aggregation of ChildPostPoi objects.
     */
    protected $collPostPois;
    protected $collPostPoisPartial;

    /**
     * @var        ObjectCollection|ChildPushNotification[] Collection to store aggregation of ChildPushNotification objects.
     */
    protected $collPushNotifications;
    protected $collPushNotificationsPartial;

    /**
     * @var        ObjectCollection|ChildWebhook[] Collection to store aggregation of ChildWebhook objects.
     */
    protected $collWebhooks;
    protected $collWebhooksPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    // archivable behavior
    protected $archiveOnDelete = true;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildGallery[]
     */
    protected $galleriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostAction[]
     */
    protected $postActionsRelatedByPostIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostAction[]
     */
    protected $postActionsRelatedByContentIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostConnector[]
     */
    protected $postConnectorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostEvent[]
     */
    protected $postEventsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostExtraLog[]
     */
    protected $postExtraLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostLog[]
     */
    protected $postLogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostPerson[]
     */
    protected $postpeopleScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPostPoi[]
     */
    protected $postPoisScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPushNotification[]
     */
    protected $pushNotificationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildWebhook[]
     */
    protected $webhooksScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->count_like = 0;
        $this->count_share = 0;
    }

    /**
     * Initializes internal state of Database\HubPlus\Base\Post object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Post</code> instance.  If
     * <code>obj</code> is an instance of <code>Post</code>, delegates to
     * <code>equals(Post)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Post The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [type] column value.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the [status] column value.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [author_id] column value.
     *
     * @return int
     */
    public function getAuthorId()
    {
        return $this->author_id;
    }

    /**
     * Get the [cover_id] column value.
     *
     * @return int
     */
    public function getCoverId()
    {
        return $this->cover_id;
    }

    /**
     * Get the [slug] column value.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [subtitle] column value.
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [body] column value.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Get the [search] column value.
     *
     * @return string
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * Get the [template_id] column value.
     *
     * @return int
     */
    public function getTemplateId()
    {
        return $this->template_id;
    }

    /**
     * Get the [layout] column value.
     *
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Get the [tags] column value.
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get the [tags_user] column value.
     *
     * @return string
     */
    public function getTagsUser()
    {
        return $this->tags_user;
    }

    /**
     * Get the [row_data] column value.
     *
     * @return string
     */
    public function getRowData()
    {
        return $this->row_data;
    }

    /**
     * Get the [count_like] column value.
     *
     * @return int
     */
    public function getCountLike()
    {
        return $this->count_like;
    }

    /**
     * Get the [count_share] column value.
     *
     * @return int
     */
    public function getCountShare()
    {
        return $this->count_share;
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = NULL)
    {
        if ($format === null) {
            return $this->deleted_at;
        } else {
            return $this->deleted_at instanceof \DateTimeInterface ? $this->deleted_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[PostTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [type] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->type !== $v) {
            $this->type = $v;
            $this->modifiedColumns[PostTableMap::COL_TYPE] = true;
        }

        return $this;
    } // setType()

    /**
     * Set the value of [status] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[PostTableMap::COL_STATUS] = true;
        }

        return $this;
    } // setStatus()

    /**
     * Set the value of [author_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setAuthorId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->author_id !== $v) {
            $this->author_id = $v;
            $this->modifiedColumns[PostTableMap::COL_AUTHOR_ID] = true;
        }

        if ($this->aUserBackend !== null && $this->aUserBackend->getId() !== $v) {
            $this->aUserBackend = null;
        }

        return $this;
    } // setAuthorId()

    /**
     * Set the value of [cover_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setCoverId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cover_id !== $v) {
            $this->cover_id = $v;
            $this->modifiedColumns[PostTableMap::COL_COVER_ID] = true;
        }

        if ($this->aMedia !== null && $this->aMedia->getId() !== $v) {
            $this->aMedia = null;
        }

        return $this;
    } // setCoverId()

    /**
     * Set the value of [slug] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->slug !== $v) {
            $this->slug = $v;
            $this->modifiedColumns[PostTableMap::COL_SLUG] = true;
        }

        return $this;
    } // setSlug()

    /**
     * Set the value of [title] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[PostTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [subtitle] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setSubtitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->subtitle !== $v) {
            $this->subtitle = $v;
            $this->modifiedColumns[PostTableMap::COL_SUBTITLE] = true;
        }

        return $this;
    } // setSubtitle()

    /**
     * Set the value of [description] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[PostTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [body] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setBody($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->body !== $v) {
            $this->body = $v;
            $this->modifiedColumns[PostTableMap::COL_BODY] = true;
        }

        return $this;
    } // setBody()

    /**
     * Set the value of [search] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setSearch($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->search !== $v) {
            $this->search = $v;
            $this->modifiedColumns[PostTableMap::COL_SEARCH] = true;
        }

        return $this;
    } // setSearch()

    /**
     * Set the value of [template_id] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setTemplateId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->template_id !== $v) {
            $this->template_id = $v;
            $this->modifiedColumns[PostTableMap::COL_TEMPLATE_ID] = true;
        }

        if ($this->aTemplate !== null && $this->aTemplate->getId() !== $v) {
            $this->aTemplate = null;
        }

        return $this;
    } // setTemplateId()

    /**
     * Set the value of [layout] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setLayout($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->layout !== $v) {
            $this->layout = $v;
            $this->modifiedColumns[PostTableMap::COL_LAYOUT] = true;
        }

        return $this;
    } // setLayout()

    /**
     * Set the value of [tags] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setTags($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tags !== $v) {
            $this->tags = $v;
            $this->modifiedColumns[PostTableMap::COL_TAGS] = true;
        }

        return $this;
    } // setTags()

    /**
     * Set the value of [tags_user] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setTagsUser($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tags_user !== $v) {
            $this->tags_user = $v;
            $this->modifiedColumns[PostTableMap::COL_TAGS_USER] = true;
        }

        return $this;
    } // setTagsUser()

    /**
     * Set the value of [row_data] column.
     *
     * @param string $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setRowData($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->row_data !== $v) {
            $this->row_data = $v;
            $this->modifiedColumns[PostTableMap::COL_ROW_DATA] = true;
        }

        return $this;
    } // setRowData()

    /**
     * Set the value of [count_like] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setCountLike($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->count_like !== $v) {
            $this->count_like = $v;
            $this->modifiedColumns[PostTableMap::COL_COUNT_LIKE] = true;
        }

        return $this;
    } // setCountLike()

    /**
     * Set the value of [count_share] column.
     *
     * @param int $v new value
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setCountShare($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->count_share !== $v) {
            $this->count_share = $v;
            $this->modifiedColumns[PostTableMap::COL_COUNT_SHARE] = true;
        }

        return $this;
    } // setCountShare()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            if ($this->deleted_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_at->format("Y-m-d H:i:s.u")) {
                $this->deleted_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PostTableMap::COL_DELETED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedAt()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PostTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PostTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->count_like !== 0) {
                return false;
            }

            if ($this->count_share !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PostTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PostTableMap::translateFieldName('Type', TableMap::TYPE_PHPNAME, $indexType)];
            $this->type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PostTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PostTableMap::translateFieldName('AuthorId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->author_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PostTableMap::translateFieldName('CoverId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cover_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PostTableMap::translateFieldName('Slug', TableMap::TYPE_PHPNAME, $indexType)];
            $this->slug = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PostTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PostTableMap::translateFieldName('Subtitle', TableMap::TYPE_PHPNAME, $indexType)];
            $this->subtitle = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : PostTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : PostTableMap::translateFieldName('Body', TableMap::TYPE_PHPNAME, $indexType)];
            $this->body = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : PostTableMap::translateFieldName('Search', TableMap::TYPE_PHPNAME, $indexType)];
            $this->search = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : PostTableMap::translateFieldName('TemplateId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->template_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : PostTableMap::translateFieldName('Layout', TableMap::TYPE_PHPNAME, $indexType)];
            $this->layout = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : PostTableMap::translateFieldName('Tags', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tags = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : PostTableMap::translateFieldName('TagsUser', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tags_user = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : PostTableMap::translateFieldName('RowData', TableMap::TYPE_PHPNAME, $indexType)];
            $this->row_data = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : PostTableMap::translateFieldName('CountLike', TableMap::TYPE_PHPNAME, $indexType)];
            $this->count_like = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : PostTableMap::translateFieldName('CountShare', TableMap::TYPE_PHPNAME, $indexType)];
            $this->count_share = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : PostTableMap::translateFieldName('DeletedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->deleted_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : PostTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : PostTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 21; // 21 = PostTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Database\\HubPlus\\Post'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aUserBackend !== null && $this->author_id !== $this->aUserBackend->getId()) {
            $this->aUserBackend = null;
        }
        if ($this->aMedia !== null && $this->cover_id !== $this->aMedia->getId()) {
            $this->aMedia = null;
        }
        if ($this->aTemplate !== null && $this->template_id !== $this->aTemplate->getId()) {
            $this->aTemplate = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PostTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPostQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aTemplate = null;
            $this->aMedia = null;
            $this->aUserBackend = null;
            $this->collGalleries = null;

            $this->collPostActionsRelatedByPostId = null;

            $this->collPostActionsRelatedByContentId = null;

            $this->collPostConnectors = null;

            $this->collPostEvents = null;

            $this->collPostExtraLogs = null;

            $this->collPostLogs = null;

            $this->collPostpeople = null;

            $this->collPostPois = null;

            $this->collPushNotifications = null;

            $this->collWebhooks = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Post::setDeleted()
     * @see Post::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPostQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            // archivable behavior
            if ($ret) {
                if ($this->archiveOnDelete) {
                    // do nothing yet. The object will be archived later when calling ChildPostQuery::delete().
                } else {
                    $deleteQuery->setArchiveOnDelete(false);
                    $this->archiveOnDelete = true;
                }
            }

            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(PostTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(PostTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(PostTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PostTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aTemplate !== null) {
                if ($this->aTemplate->isModified() || $this->aTemplate->isNew()) {
                    $affectedRows += $this->aTemplate->save($con);
                }
                $this->setTemplate($this->aTemplate);
            }

            if ($this->aMedia !== null) {
                if ($this->aMedia->isModified() || $this->aMedia->isNew()) {
                    $affectedRows += $this->aMedia->save($con);
                }
                $this->setMedia($this->aMedia);
            }

            if ($this->aUserBackend !== null) {
                if ($this->aUserBackend->isModified() || $this->aUserBackend->isNew()) {
                    $affectedRows += $this->aUserBackend->save($con);
                }
                $this->setUserBackend($this->aUserBackend);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->galleriesScheduledForDeletion !== null) {
                if (!$this->galleriesScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\GalleryQuery::create()
                        ->filterByPrimaryKeys($this->galleriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->galleriesScheduledForDeletion = null;
                }
            }

            if ($this->collGalleries !== null) {
                foreach ($this->collGalleries as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postActionsRelatedByPostIdScheduledForDeletion !== null) {
                if (!$this->postActionsRelatedByPostIdScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\PostActionQuery::create()
                        ->filterByPrimaryKeys($this->postActionsRelatedByPostIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->postActionsRelatedByPostIdScheduledForDeletion = null;
                }
            }

            if ($this->collPostActionsRelatedByPostId !== null) {
                foreach ($this->collPostActionsRelatedByPostId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postActionsRelatedByContentIdScheduledForDeletion !== null) {
                if (!$this->postActionsRelatedByContentIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->postActionsRelatedByContentIdScheduledForDeletion as $postActionRelatedByContentId) {
                        // need to save related object because we set the relation to null
                        $postActionRelatedByContentId->save($con);
                    }
                    $this->postActionsRelatedByContentIdScheduledForDeletion = null;
                }
            }

            if ($this->collPostActionsRelatedByContentId !== null) {
                foreach ($this->collPostActionsRelatedByContentId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postConnectorsScheduledForDeletion !== null) {
                if (!$this->postConnectorsScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\PostConnectorQuery::create()
                        ->filterByPrimaryKeys($this->postConnectorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->postConnectorsScheduledForDeletion = null;
                }
            }

            if ($this->collPostConnectors !== null) {
                foreach ($this->collPostConnectors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postEventsScheduledForDeletion !== null) {
                if (!$this->postEventsScheduledForDeletion->isEmpty()) {
                    foreach ($this->postEventsScheduledForDeletion as $postEvent) {
                        // need to save related object because we set the relation to null
                        $postEvent->save($con);
                    }
                    $this->postEventsScheduledForDeletion = null;
                }
            }

            if ($this->collPostEvents !== null) {
                foreach ($this->collPostEvents as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postExtraLogsScheduledForDeletion !== null) {
                if (!$this->postExtraLogsScheduledForDeletion->isEmpty()) {
                    foreach ($this->postExtraLogsScheduledForDeletion as $postExtraLog) {
                        // need to save related object because we set the relation to null
                        $postExtraLog->save($con);
                    }
                    $this->postExtraLogsScheduledForDeletion = null;
                }
            }

            if ($this->collPostExtraLogs !== null) {
                foreach ($this->collPostExtraLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postLogsScheduledForDeletion !== null) {
                if (!$this->postLogsScheduledForDeletion->isEmpty()) {
                    foreach ($this->postLogsScheduledForDeletion as $postLog) {
                        // need to save related object because we set the relation to null
                        $postLog->save($con);
                    }
                    $this->postLogsScheduledForDeletion = null;
                }
            }

            if ($this->collPostLogs !== null) {
                foreach ($this->collPostLogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postpeopleScheduledForDeletion !== null) {
                if (!$this->postpeopleScheduledForDeletion->isEmpty()) {
                    foreach ($this->postpeopleScheduledForDeletion as $postPerson) {
                        // need to save related object because we set the relation to null
                        $postPerson->save($con);
                    }
                    $this->postpeopleScheduledForDeletion = null;
                }
            }

            if ($this->collPostpeople !== null) {
                foreach ($this->collPostpeople as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->postPoisScheduledForDeletion !== null) {
                if (!$this->postPoisScheduledForDeletion->isEmpty()) {
                    foreach ($this->postPoisScheduledForDeletion as $postPoi) {
                        // need to save related object because we set the relation to null
                        $postPoi->save($con);
                    }
                    $this->postPoisScheduledForDeletion = null;
                }
            }

            if ($this->collPostPois !== null) {
                foreach ($this->collPostPois as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->pushNotificationsScheduledForDeletion !== null) {
                if (!$this->pushNotificationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->pushNotificationsScheduledForDeletion as $pushNotification) {
                        // need to save related object because we set the relation to null
                        $pushNotification->save($con);
                    }
                    $this->pushNotificationsScheduledForDeletion = null;
                }
            }

            if ($this->collPushNotifications !== null) {
                foreach ($this->collPushNotifications as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->webhooksScheduledForDeletion !== null) {
                if (!$this->webhooksScheduledForDeletion->isEmpty()) {
                    \Database\HubPlus\WebhookQuery::create()
                        ->filterByPrimaryKeys($this->webhooksScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->webhooksScheduledForDeletion = null;
                }
            }

            if ($this->collWebhooks !== null) {
                foreach ($this->collWebhooks as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[PostTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PostTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PostTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(PostTableMap::COL_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'type';
        }
        if ($this->isColumnModified(PostTableMap::COL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'status';
        }
        if ($this->isColumnModified(PostTableMap::COL_AUTHOR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'author_id';
        }
        if ($this->isColumnModified(PostTableMap::COL_COVER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cover_id';
        }
        if ($this->isColumnModified(PostTableMap::COL_SLUG)) {
            $modifiedColumns[':p' . $index++]  = 'slug';
        }
        if ($this->isColumnModified(PostTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(PostTableMap::COL_SUBTITLE)) {
            $modifiedColumns[':p' . $index++]  = 'subtitle';
        }
        if ($this->isColumnModified(PostTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(PostTableMap::COL_BODY)) {
            $modifiedColumns[':p' . $index++]  = 'body';
        }
        if ($this->isColumnModified(PostTableMap::COL_SEARCH)) {
            $modifiedColumns[':p' . $index++]  = 'search';
        }
        if ($this->isColumnModified(PostTableMap::COL_TEMPLATE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'template_id';
        }
        if ($this->isColumnModified(PostTableMap::COL_LAYOUT)) {
            $modifiedColumns[':p' . $index++]  = 'layout';
        }
        if ($this->isColumnModified(PostTableMap::COL_TAGS)) {
            $modifiedColumns[':p' . $index++]  = 'tags';
        }
        if ($this->isColumnModified(PostTableMap::COL_TAGS_USER)) {
            $modifiedColumns[':p' . $index++]  = 'tags_user';
        }
        if ($this->isColumnModified(PostTableMap::COL_ROW_DATA)) {
            $modifiedColumns[':p' . $index++]  = 'row_data';
        }
        if ($this->isColumnModified(PostTableMap::COL_COUNT_LIKE)) {
            $modifiedColumns[':p' . $index++]  = 'count_like';
        }
        if ($this->isColumnModified(PostTableMap::COL_COUNT_SHARE)) {
            $modifiedColumns[':p' . $index++]  = 'count_share';
        }
        if ($this->isColumnModified(PostTableMap::COL_DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_at';
        }
        if ($this->isColumnModified(PostTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(PostTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO post (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'type':
                        $stmt->bindValue($identifier, $this->type, PDO::PARAM_STR);
                        break;
                    case 'status':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_INT);
                        break;
                    case 'author_id':
                        $stmt->bindValue($identifier, $this->author_id, PDO::PARAM_INT);
                        break;
                    case 'cover_id':
                        $stmt->bindValue($identifier, $this->cover_id, PDO::PARAM_INT);
                        break;
                    case 'slug':
                        $stmt->bindValue($identifier, $this->slug, PDO::PARAM_STR);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'subtitle':
                        $stmt->bindValue($identifier, $this->subtitle, PDO::PARAM_STR);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'body':
                        $stmt->bindValue($identifier, $this->body, PDO::PARAM_STR);
                        break;
                    case 'search':
                        $stmt->bindValue($identifier, $this->search, PDO::PARAM_STR);
                        break;
                    case 'template_id':
                        $stmt->bindValue($identifier, $this->template_id, PDO::PARAM_INT);
                        break;
                    case 'layout':
                        $stmt->bindValue($identifier, $this->layout, PDO::PARAM_STR);
                        break;
                    case 'tags':
                        $stmt->bindValue($identifier, $this->tags, PDO::PARAM_STR);
                        break;
                    case 'tags_user':
                        $stmt->bindValue($identifier, $this->tags_user, PDO::PARAM_STR);
                        break;
                    case 'row_data':
                        $stmt->bindValue($identifier, $this->row_data, PDO::PARAM_STR);
                        break;
                    case 'count_like':
                        $stmt->bindValue($identifier, $this->count_like, PDO::PARAM_INT);
                        break;
                    case 'count_share':
                        $stmt->bindValue($identifier, $this->count_share, PDO::PARAM_INT);
                        break;
                    case 'deleted_at':
                        $stmt->bindValue($identifier, $this->deleted_at ? $this->deleted_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PostTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getType();
                break;
            case 2:
                return $this->getStatus();
                break;
            case 3:
                return $this->getAuthorId();
                break;
            case 4:
                return $this->getCoverId();
                break;
            case 5:
                return $this->getSlug();
                break;
            case 6:
                return $this->getTitle();
                break;
            case 7:
                return $this->getSubtitle();
                break;
            case 8:
                return $this->getDescription();
                break;
            case 9:
                return $this->getBody();
                break;
            case 10:
                return $this->getSearch();
                break;
            case 11:
                return $this->getTemplateId();
                break;
            case 12:
                return $this->getLayout();
                break;
            case 13:
                return $this->getTags();
                break;
            case 14:
                return $this->getTagsUser();
                break;
            case 15:
                return $this->getRowData();
                break;
            case 16:
                return $this->getCountLike();
                break;
            case 17:
                return $this->getCountShare();
                break;
            case 18:
                return $this->getDeletedAt();
                break;
            case 19:
                return $this->getCreatedAt();
                break;
            case 20:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Post'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Post'][$this->hashCode()] = true;
        $keys = PostTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getType(),
            $keys[2] => $this->getStatus(),
            $keys[3] => $this->getAuthorId(),
            $keys[4] => $this->getCoverId(),
            $keys[5] => $this->getSlug(),
            $keys[6] => $this->getTitle(),
            $keys[7] => $this->getSubtitle(),
            $keys[8] => $this->getDescription(),
            $keys[9] => $this->getBody(),
            $keys[10] => $this->getSearch(),
            $keys[11] => $this->getTemplateId(),
            $keys[12] => $this->getLayout(),
            $keys[13] => $this->getTags(),
            $keys[14] => $this->getTagsUser(),
            $keys[15] => $this->getRowData(),
            $keys[16] => $this->getCountLike(),
            $keys[17] => $this->getCountShare(),
            $keys[18] => $this->getDeletedAt(),
            $keys[19] => $this->getCreatedAt(),
            $keys[20] => $this->getUpdatedAt(),
        );
        if ($result[$keys[18]] instanceof \DateTimeInterface) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        if ($result[$keys[19]] instanceof \DateTimeInterface) {
            $result[$keys[19]] = $result[$keys[19]]->format('c');
        }

        if ($result[$keys[20]] instanceof \DateTimeInterface) {
            $result[$keys[20]] = $result[$keys[20]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aTemplate) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'template';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'template';
                        break;
                    default:
                        $key = 'Template';
                }

                $result[$key] = $this->aTemplate->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMedia) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'media';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'media';
                        break;
                    default:
                        $key = 'Media';
                }

                $result[$key] = $this->aMedia->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUserBackend) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userBackend';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_backend';
                        break;
                    default:
                        $key = 'UserBackend';
                }

                $result[$key] = $this->aUserBackend->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collGalleries) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'galleries';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'galleries';
                        break;
                    default:
                        $key = 'Galleries';
                }

                $result[$key] = $this->collGalleries->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPostActionsRelatedByPostId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postActions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_actions';
                        break;
                    default:
                        $key = 'PostActions';
                }

                $result[$key] = $this->collPostActionsRelatedByPostId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPostActionsRelatedByContentId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postActions';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_actions';
                        break;
                    default:
                        $key = 'PostActions';
                }

                $result[$key] = $this->collPostActionsRelatedByContentId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPostConnectors) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postConnectors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_connectors';
                        break;
                    default:
                        $key = 'PostConnectors';
                }

                $result[$key] = $this->collPostConnectors->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPostEvents) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postEvents';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_events';
                        break;
                    default:
                        $key = 'PostEvents';
                }

                $result[$key] = $this->collPostEvents->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPostExtraLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postExtraLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_extra_logs';
                        break;
                    default:
                        $key = 'PostExtraLogs';
                }

                $result[$key] = $this->collPostExtraLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPostLogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postLogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_logs';
                        break;
                    default:
                        $key = 'PostLogs';
                }

                $result[$key] = $this->collPostLogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPostpeople) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postpeople';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_people';
                        break;
                    default:
                        $key = 'Postpeople';
                }

                $result[$key] = $this->collPostpeople->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPostPois) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'postPois';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'post_pois';
                        break;
                    default:
                        $key = 'PostPois';
                }

                $result[$key] = $this->collPostPois->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPushNotifications) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pushNotifications';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'push_notifications';
                        break;
                    default:
                        $key = 'PushNotifications';
                }

                $result[$key] = $this->collPushNotifications->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collWebhooks) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'webhooks';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'webhooks';
                        break;
                    default:
                        $key = 'Webhooks';
                }

                $result[$key] = $this->collWebhooks->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Database\HubPlus\Post
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PostTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Database\HubPlus\Post
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setType($value);
                break;
            case 2:
                $this->setStatus($value);
                break;
            case 3:
                $this->setAuthorId($value);
                break;
            case 4:
                $this->setCoverId($value);
                break;
            case 5:
                $this->setSlug($value);
                break;
            case 6:
                $this->setTitle($value);
                break;
            case 7:
                $this->setSubtitle($value);
                break;
            case 8:
                $this->setDescription($value);
                break;
            case 9:
                $this->setBody($value);
                break;
            case 10:
                $this->setSearch($value);
                break;
            case 11:
                $this->setTemplateId($value);
                break;
            case 12:
                $this->setLayout($value);
                break;
            case 13:
                $this->setTags($value);
                break;
            case 14:
                $this->setTagsUser($value);
                break;
            case 15:
                $this->setRowData($value);
                break;
            case 16:
                $this->setCountLike($value);
                break;
            case 17:
                $this->setCountShare($value);
                break;
            case 18:
                $this->setDeletedAt($value);
                break;
            case 19:
                $this->setCreatedAt($value);
                break;
            case 20:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PostTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setType($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setStatus($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setAuthorId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCoverId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setSlug($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setTitle($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setSubtitle($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setDescription($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setBody($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setSearch($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setTemplateId($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setLayout($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setTags($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setTagsUser($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setRowData($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setCountLike($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setCountShare($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setDeletedAt($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setCreatedAt($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setUpdatedAt($arr[$keys[20]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Database\HubPlus\Post The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PostTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PostTableMap::COL_ID)) {
            $criteria->add(PostTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(PostTableMap::COL_TYPE)) {
            $criteria->add(PostTableMap::COL_TYPE, $this->type);
        }
        if ($this->isColumnModified(PostTableMap::COL_STATUS)) {
            $criteria->add(PostTableMap::COL_STATUS, $this->status);
        }
        if ($this->isColumnModified(PostTableMap::COL_AUTHOR_ID)) {
            $criteria->add(PostTableMap::COL_AUTHOR_ID, $this->author_id);
        }
        if ($this->isColumnModified(PostTableMap::COL_COVER_ID)) {
            $criteria->add(PostTableMap::COL_COVER_ID, $this->cover_id);
        }
        if ($this->isColumnModified(PostTableMap::COL_SLUG)) {
            $criteria->add(PostTableMap::COL_SLUG, $this->slug);
        }
        if ($this->isColumnModified(PostTableMap::COL_TITLE)) {
            $criteria->add(PostTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(PostTableMap::COL_SUBTITLE)) {
            $criteria->add(PostTableMap::COL_SUBTITLE, $this->subtitle);
        }
        if ($this->isColumnModified(PostTableMap::COL_DESCRIPTION)) {
            $criteria->add(PostTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(PostTableMap::COL_BODY)) {
            $criteria->add(PostTableMap::COL_BODY, $this->body);
        }
        if ($this->isColumnModified(PostTableMap::COL_SEARCH)) {
            $criteria->add(PostTableMap::COL_SEARCH, $this->search);
        }
        if ($this->isColumnModified(PostTableMap::COL_TEMPLATE_ID)) {
            $criteria->add(PostTableMap::COL_TEMPLATE_ID, $this->template_id);
        }
        if ($this->isColumnModified(PostTableMap::COL_LAYOUT)) {
            $criteria->add(PostTableMap::COL_LAYOUT, $this->layout);
        }
        if ($this->isColumnModified(PostTableMap::COL_TAGS)) {
            $criteria->add(PostTableMap::COL_TAGS, $this->tags);
        }
        if ($this->isColumnModified(PostTableMap::COL_TAGS_USER)) {
            $criteria->add(PostTableMap::COL_TAGS_USER, $this->tags_user);
        }
        if ($this->isColumnModified(PostTableMap::COL_ROW_DATA)) {
            $criteria->add(PostTableMap::COL_ROW_DATA, $this->row_data);
        }
        if ($this->isColumnModified(PostTableMap::COL_COUNT_LIKE)) {
            $criteria->add(PostTableMap::COL_COUNT_LIKE, $this->count_like);
        }
        if ($this->isColumnModified(PostTableMap::COL_COUNT_SHARE)) {
            $criteria->add(PostTableMap::COL_COUNT_SHARE, $this->count_share);
        }
        if ($this->isColumnModified(PostTableMap::COL_DELETED_AT)) {
            $criteria->add(PostTableMap::COL_DELETED_AT, $this->deleted_at);
        }
        if ($this->isColumnModified(PostTableMap::COL_CREATED_AT)) {
            $criteria->add(PostTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(PostTableMap::COL_UPDATED_AT)) {
            $criteria->add(PostTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPostQuery::create();
        $criteria->add(PostTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Database\HubPlus\Post (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setType($this->getType());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setAuthorId($this->getAuthorId());
        $copyObj->setCoverId($this->getCoverId());
        $copyObj->setSlug($this->getSlug());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setSubtitle($this->getSubtitle());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setBody($this->getBody());
        $copyObj->setSearch($this->getSearch());
        $copyObj->setTemplateId($this->getTemplateId());
        $copyObj->setLayout($this->getLayout());
        $copyObj->setTags($this->getTags());
        $copyObj->setTagsUser($this->getTagsUser());
        $copyObj->setRowData($this->getRowData());
        $copyObj->setCountLike($this->getCountLike());
        $copyObj->setCountShare($this->getCountShare());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getGalleries() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addGallery($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPostActionsRelatedByPostId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostActionRelatedByPostId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPostActionsRelatedByContentId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostActionRelatedByContentId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPostConnectors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostConnector($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPostEvents() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostEvent($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPostExtraLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostExtraLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPostLogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostLog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPostpeople() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostPerson($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPostPois() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPostPoi($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPushNotifications() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPushNotification($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getWebhooks() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addWebhook($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Database\HubPlus\Post Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildTemplate object.
     *
     * @param  ChildTemplate $v
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTemplate(ChildTemplate $v = null)
    {
        if ($v === null) {
            $this->setTemplateId(NULL);
        } else {
            $this->setTemplateId($v->getId());
        }

        $this->aTemplate = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildTemplate object, it will not be re-added.
        if ($v !== null) {
            $v->addPost($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildTemplate object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildTemplate The associated ChildTemplate object.
     * @throws PropelException
     */
    public function getTemplate(ConnectionInterface $con = null)
    {
        if ($this->aTemplate === null && ($this->template_id != 0)) {
            $this->aTemplate = ChildTemplateQuery::create()->findPk($this->template_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTemplate->addPosts($this);
             */
        }

        return $this->aTemplate;
    }

    /**
     * Declares an association between this object and a ChildMedia object.
     *
     * @param  ChildMedia $v
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMedia(ChildMedia $v = null)
    {
        if ($v === null) {
            $this->setCoverId(NULL);
        } else {
            $this->setCoverId($v->getId());
        }

        $this->aMedia = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMedia object, it will not be re-added.
        if ($v !== null) {
            $v->addPost($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMedia object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildMedia The associated ChildMedia object.
     * @throws PropelException
     */
    public function getMedia(ConnectionInterface $con = null)
    {
        if ($this->aMedia === null && ($this->cover_id != 0)) {
            $this->aMedia = ChildMediaQuery::create()->findPk($this->cover_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMedia->addPosts($this);
             */
        }

        return $this->aMedia;
    }

    /**
     * Declares an association between this object and a ChildUserBackend object.
     *
     * @param  ChildUserBackend $v
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUserBackend(ChildUserBackend $v = null)
    {
        if ($v === null) {
            $this->setAuthorId(NULL);
        } else {
            $this->setAuthorId($v->getId());
        }

        $this->aUserBackend = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUserBackend object, it will not be re-added.
        if ($v !== null) {
            $v->addPost($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUserBackend object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUserBackend The associated ChildUserBackend object.
     * @throws PropelException
     */
    public function getUserBackend(ConnectionInterface $con = null)
    {
        if ($this->aUserBackend === null && ($this->author_id != 0)) {
            $this->aUserBackend = ChildUserBackendQuery::create()->findPk($this->author_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUserBackend->addPosts($this);
             */
        }

        return $this->aUserBackend;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Gallery' == $relationName) {
            $this->initGalleries();
            return;
        }
        if ('PostActionRelatedByPostId' == $relationName) {
            $this->initPostActionsRelatedByPostId();
            return;
        }
        if ('PostActionRelatedByContentId' == $relationName) {
            $this->initPostActionsRelatedByContentId();
            return;
        }
        if ('PostConnector' == $relationName) {
            $this->initPostConnectors();
            return;
        }
        if ('PostEvent' == $relationName) {
            $this->initPostEvents();
            return;
        }
        if ('PostExtraLog' == $relationName) {
            $this->initPostExtraLogs();
            return;
        }
        if ('PostLog' == $relationName) {
            $this->initPostLogs();
            return;
        }
        if ('PostPerson' == $relationName) {
            $this->initPostpeople();
            return;
        }
        if ('PostPoi' == $relationName) {
            $this->initPostPois();
            return;
        }
        if ('PushNotification' == $relationName) {
            $this->initPushNotifications();
            return;
        }
        if ('Webhook' == $relationName) {
            $this->initWebhooks();
            return;
        }
    }

    /**
     * Clears out the collGalleries collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addGalleries()
     */
    public function clearGalleries()
    {
        $this->collGalleries = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collGalleries collection loaded partially.
     */
    public function resetPartialGalleries($v = true)
    {
        $this->collGalleriesPartial = $v;
    }

    /**
     * Initializes the collGalleries collection.
     *
     * By default this just sets the collGalleries collection to an empty array (like clearcollGalleries());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initGalleries($overrideExisting = true)
    {
        if (null !== $this->collGalleries && !$overrideExisting) {
            return;
        }

        $collectionClassName = GalleryTableMap::getTableMap()->getCollectionClassName();

        $this->collGalleries = new $collectionClassName;
        $this->collGalleries->setModel('\Database\HubPlus\Gallery');
    }

    /**
     * Gets an array of ChildGallery objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPost is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildGallery[] List of ChildGallery objects
     * @throws PropelException
     */
    public function getGalleries(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collGalleriesPartial && !$this->isNew();
        if (null === $this->collGalleries || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collGalleries) {
                // return empty collection
                $this->initGalleries();
            } else {
                $collGalleries = ChildGalleryQuery::create(null, $criteria)
                    ->filterByPost($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collGalleriesPartial && count($collGalleries)) {
                        $this->initGalleries(false);

                        foreach ($collGalleries as $obj) {
                            if (false == $this->collGalleries->contains($obj)) {
                                $this->collGalleries->append($obj);
                            }
                        }

                        $this->collGalleriesPartial = true;
                    }

                    return $collGalleries;
                }

                if ($partial && $this->collGalleries) {
                    foreach ($this->collGalleries as $obj) {
                        if ($obj->isNew()) {
                            $collGalleries[] = $obj;
                        }
                    }
                }

                $this->collGalleries = $collGalleries;
                $this->collGalleriesPartial = false;
            }
        }

        return $this->collGalleries;
    }

    /**
     * Sets a collection of ChildGallery objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $galleries A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function setGalleries(Collection $galleries, ConnectionInterface $con = null)
    {
        /** @var ChildGallery[] $galleriesToDelete */
        $galleriesToDelete = $this->getGalleries(new Criteria(), $con)->diff($galleries);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->galleriesScheduledForDeletion = clone $galleriesToDelete;

        foreach ($galleriesToDelete as $galleryRemoved) {
            $galleryRemoved->setPost(null);
        }

        $this->collGalleries = null;
        foreach ($galleries as $gallery) {
            $this->addGallery($gallery);
        }

        $this->collGalleries = $galleries;
        $this->collGalleriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Gallery objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Gallery objects.
     * @throws PropelException
     */
    public function countGalleries(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collGalleriesPartial && !$this->isNew();
        if (null === $this->collGalleries || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collGalleries) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getGalleries());
            }

            $query = ChildGalleryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPost($this)
                ->count($con);
        }

        return count($this->collGalleries);
    }

    /**
     * Method called to associate a ChildGallery object to this object
     * through the ChildGallery foreign key attribute.
     *
     * @param  ChildGallery $l ChildGallery
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function addGallery(ChildGallery $l)
    {
        if ($this->collGalleries === null) {
            $this->initGalleries();
            $this->collGalleriesPartial = true;
        }

        if (!$this->collGalleries->contains($l)) {
            $this->doAddGallery($l);

            if ($this->galleriesScheduledForDeletion and $this->galleriesScheduledForDeletion->contains($l)) {
                $this->galleriesScheduledForDeletion->remove($this->galleriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildGallery $gallery The ChildGallery object to add.
     */
    protected function doAddGallery(ChildGallery $gallery)
    {
        $this->collGalleries[]= $gallery;
        $gallery->setPost($this);
    }

    /**
     * @param  ChildGallery $gallery The ChildGallery object to remove.
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function removeGallery(ChildGallery $gallery)
    {
        if ($this->getGalleries()->contains($gallery)) {
            $pos = $this->collGalleries->search($gallery);
            $this->collGalleries->remove($pos);
            if (null === $this->galleriesScheduledForDeletion) {
                $this->galleriesScheduledForDeletion = clone $this->collGalleries;
                $this->galleriesScheduledForDeletion->clear();
            }
            $this->galleriesScheduledForDeletion[]= clone $gallery;
            $gallery->setPost(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Post is new, it will return
     * an empty collection; or if this Post has previously
     * been saved, it will retrieve related Galleries from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Post.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildGallery[] List of ChildGallery objects
     */
    public function getGalleriesJoinMedia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildGalleryQuery::create(null, $criteria);
        $query->joinWith('Media', $joinBehavior);

        return $this->getGalleries($query, $con);
    }

    /**
     * Clears out the collPostActionsRelatedByPostId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostActionsRelatedByPostId()
     */
    public function clearPostActionsRelatedByPostId()
    {
        $this->collPostActionsRelatedByPostId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostActionsRelatedByPostId collection loaded partially.
     */
    public function resetPartialPostActionsRelatedByPostId($v = true)
    {
        $this->collPostActionsRelatedByPostIdPartial = $v;
    }

    /**
     * Initializes the collPostActionsRelatedByPostId collection.
     *
     * By default this just sets the collPostActionsRelatedByPostId collection to an empty array (like clearcollPostActionsRelatedByPostId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostActionsRelatedByPostId($overrideExisting = true)
    {
        if (null !== $this->collPostActionsRelatedByPostId && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostActionTableMap::getTableMap()->getCollectionClassName();

        $this->collPostActionsRelatedByPostId = new $collectionClassName;
        $this->collPostActionsRelatedByPostId->setModel('\Database\HubPlus\PostAction');
    }

    /**
     * Gets an array of ChildPostAction objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPost is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     * @throws PropelException
     */
    public function getPostActionsRelatedByPostId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostActionsRelatedByPostIdPartial && !$this->isNew();
        if (null === $this->collPostActionsRelatedByPostId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostActionsRelatedByPostId) {
                // return empty collection
                $this->initPostActionsRelatedByPostId();
            } else {
                $collPostActionsRelatedByPostId = ChildPostActionQuery::create(null, $criteria)
                    ->filterByPostRelatedByPostId($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostActionsRelatedByPostIdPartial && count($collPostActionsRelatedByPostId)) {
                        $this->initPostActionsRelatedByPostId(false);

                        foreach ($collPostActionsRelatedByPostId as $obj) {
                            if (false == $this->collPostActionsRelatedByPostId->contains($obj)) {
                                $this->collPostActionsRelatedByPostId->append($obj);
                            }
                        }

                        $this->collPostActionsRelatedByPostIdPartial = true;
                    }

                    return $collPostActionsRelatedByPostId;
                }

                if ($partial && $this->collPostActionsRelatedByPostId) {
                    foreach ($this->collPostActionsRelatedByPostId as $obj) {
                        if ($obj->isNew()) {
                            $collPostActionsRelatedByPostId[] = $obj;
                        }
                    }
                }

                $this->collPostActionsRelatedByPostId = $collPostActionsRelatedByPostId;
                $this->collPostActionsRelatedByPostIdPartial = false;
            }
        }

        return $this->collPostActionsRelatedByPostId;
    }

    /**
     * Sets a collection of ChildPostAction objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postActionsRelatedByPostId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function setPostActionsRelatedByPostId(Collection $postActionsRelatedByPostId, ConnectionInterface $con = null)
    {
        /** @var ChildPostAction[] $postActionsRelatedByPostIdToDelete */
        $postActionsRelatedByPostIdToDelete = $this->getPostActionsRelatedByPostId(new Criteria(), $con)->diff($postActionsRelatedByPostId);


        $this->postActionsRelatedByPostIdScheduledForDeletion = $postActionsRelatedByPostIdToDelete;

        foreach ($postActionsRelatedByPostIdToDelete as $postActionRelatedByPostIdRemoved) {
            $postActionRelatedByPostIdRemoved->setPostRelatedByPostId(null);
        }

        $this->collPostActionsRelatedByPostId = null;
        foreach ($postActionsRelatedByPostId as $postActionRelatedByPostId) {
            $this->addPostActionRelatedByPostId($postActionRelatedByPostId);
        }

        $this->collPostActionsRelatedByPostId = $postActionsRelatedByPostId;
        $this->collPostActionsRelatedByPostIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostAction objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostAction objects.
     * @throws PropelException
     */
    public function countPostActionsRelatedByPostId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostActionsRelatedByPostIdPartial && !$this->isNew();
        if (null === $this->collPostActionsRelatedByPostId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostActionsRelatedByPostId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostActionsRelatedByPostId());
            }

            $query = ChildPostActionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPostRelatedByPostId($this)
                ->count($con);
        }

        return count($this->collPostActionsRelatedByPostId);
    }

    /**
     * Method called to associate a ChildPostAction object to this object
     * through the ChildPostAction foreign key attribute.
     *
     * @param  ChildPostAction $l ChildPostAction
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function addPostActionRelatedByPostId(ChildPostAction $l)
    {
        if ($this->collPostActionsRelatedByPostId === null) {
            $this->initPostActionsRelatedByPostId();
            $this->collPostActionsRelatedByPostIdPartial = true;
        }

        if (!$this->collPostActionsRelatedByPostId->contains($l)) {
            $this->doAddPostActionRelatedByPostId($l);

            if ($this->postActionsRelatedByPostIdScheduledForDeletion and $this->postActionsRelatedByPostIdScheduledForDeletion->contains($l)) {
                $this->postActionsRelatedByPostIdScheduledForDeletion->remove($this->postActionsRelatedByPostIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostAction $postActionRelatedByPostId The ChildPostAction object to add.
     */
    protected function doAddPostActionRelatedByPostId(ChildPostAction $postActionRelatedByPostId)
    {
        $this->collPostActionsRelatedByPostId[]= $postActionRelatedByPostId;
        $postActionRelatedByPostId->setPostRelatedByPostId($this);
    }

    /**
     * @param  ChildPostAction $postActionRelatedByPostId The ChildPostAction object to remove.
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function removePostActionRelatedByPostId(ChildPostAction $postActionRelatedByPostId)
    {
        if ($this->getPostActionsRelatedByPostId()->contains($postActionRelatedByPostId)) {
            $pos = $this->collPostActionsRelatedByPostId->search($postActionRelatedByPostId);
            $this->collPostActionsRelatedByPostId->remove($pos);
            if (null === $this->postActionsRelatedByPostIdScheduledForDeletion) {
                $this->postActionsRelatedByPostIdScheduledForDeletion = clone $this->collPostActionsRelatedByPostId;
                $this->postActionsRelatedByPostIdScheduledForDeletion->clear();
            }
            $this->postActionsRelatedByPostIdScheduledForDeletion[]= clone $postActionRelatedByPostId;
            $postActionRelatedByPostId->setPostRelatedByPostId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Post is new, it will return
     * an empty collection; or if this Post has previously
     * been saved, it will retrieve related PostActionsRelatedByPostId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Post.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     */
    public function getPostActionsRelatedByPostIdJoinSection(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostActionQuery::create(null, $criteria);
        $query->joinWith('Section', $joinBehavior);

        return $this->getPostActionsRelatedByPostId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Post is new, it will return
     * an empty collection; or if this Post has previously
     * been saved, it will retrieve related PostActionsRelatedByPostId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Post.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     */
    public function getPostActionsRelatedByPostIdJoinMedia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostActionQuery::create(null, $criteria);
        $query->joinWith('Media', $joinBehavior);

        return $this->getPostActionsRelatedByPostId($query, $con);
    }

    /**
     * Clears out the collPostActionsRelatedByContentId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostActionsRelatedByContentId()
     */
    public function clearPostActionsRelatedByContentId()
    {
        $this->collPostActionsRelatedByContentId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostActionsRelatedByContentId collection loaded partially.
     */
    public function resetPartialPostActionsRelatedByContentId($v = true)
    {
        $this->collPostActionsRelatedByContentIdPartial = $v;
    }

    /**
     * Initializes the collPostActionsRelatedByContentId collection.
     *
     * By default this just sets the collPostActionsRelatedByContentId collection to an empty array (like clearcollPostActionsRelatedByContentId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostActionsRelatedByContentId($overrideExisting = true)
    {
        if (null !== $this->collPostActionsRelatedByContentId && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostActionTableMap::getTableMap()->getCollectionClassName();

        $this->collPostActionsRelatedByContentId = new $collectionClassName;
        $this->collPostActionsRelatedByContentId->setModel('\Database\HubPlus\PostAction');
    }

    /**
     * Gets an array of ChildPostAction objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPost is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     * @throws PropelException
     */
    public function getPostActionsRelatedByContentId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostActionsRelatedByContentIdPartial && !$this->isNew();
        if (null === $this->collPostActionsRelatedByContentId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostActionsRelatedByContentId) {
                // return empty collection
                $this->initPostActionsRelatedByContentId();
            } else {
                $collPostActionsRelatedByContentId = ChildPostActionQuery::create(null, $criteria)
                    ->filterByPostRelatedByContentId($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostActionsRelatedByContentIdPartial && count($collPostActionsRelatedByContentId)) {
                        $this->initPostActionsRelatedByContentId(false);

                        foreach ($collPostActionsRelatedByContentId as $obj) {
                            if (false == $this->collPostActionsRelatedByContentId->contains($obj)) {
                                $this->collPostActionsRelatedByContentId->append($obj);
                            }
                        }

                        $this->collPostActionsRelatedByContentIdPartial = true;
                    }

                    return $collPostActionsRelatedByContentId;
                }

                if ($partial && $this->collPostActionsRelatedByContentId) {
                    foreach ($this->collPostActionsRelatedByContentId as $obj) {
                        if ($obj->isNew()) {
                            $collPostActionsRelatedByContentId[] = $obj;
                        }
                    }
                }

                $this->collPostActionsRelatedByContentId = $collPostActionsRelatedByContentId;
                $this->collPostActionsRelatedByContentIdPartial = false;
            }
        }

        return $this->collPostActionsRelatedByContentId;
    }

    /**
     * Sets a collection of ChildPostAction objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postActionsRelatedByContentId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function setPostActionsRelatedByContentId(Collection $postActionsRelatedByContentId, ConnectionInterface $con = null)
    {
        /** @var ChildPostAction[] $postActionsRelatedByContentIdToDelete */
        $postActionsRelatedByContentIdToDelete = $this->getPostActionsRelatedByContentId(new Criteria(), $con)->diff($postActionsRelatedByContentId);


        $this->postActionsRelatedByContentIdScheduledForDeletion = $postActionsRelatedByContentIdToDelete;

        foreach ($postActionsRelatedByContentIdToDelete as $postActionRelatedByContentIdRemoved) {
            $postActionRelatedByContentIdRemoved->setPostRelatedByContentId(null);
        }

        $this->collPostActionsRelatedByContentId = null;
        foreach ($postActionsRelatedByContentId as $postActionRelatedByContentId) {
            $this->addPostActionRelatedByContentId($postActionRelatedByContentId);
        }

        $this->collPostActionsRelatedByContentId = $postActionsRelatedByContentId;
        $this->collPostActionsRelatedByContentIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostAction objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostAction objects.
     * @throws PropelException
     */
    public function countPostActionsRelatedByContentId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostActionsRelatedByContentIdPartial && !$this->isNew();
        if (null === $this->collPostActionsRelatedByContentId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostActionsRelatedByContentId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostActionsRelatedByContentId());
            }

            $query = ChildPostActionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPostRelatedByContentId($this)
                ->count($con);
        }

        return count($this->collPostActionsRelatedByContentId);
    }

    /**
     * Method called to associate a ChildPostAction object to this object
     * through the ChildPostAction foreign key attribute.
     *
     * @param  ChildPostAction $l ChildPostAction
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function addPostActionRelatedByContentId(ChildPostAction $l)
    {
        if ($this->collPostActionsRelatedByContentId === null) {
            $this->initPostActionsRelatedByContentId();
            $this->collPostActionsRelatedByContentIdPartial = true;
        }

        if (!$this->collPostActionsRelatedByContentId->contains($l)) {
            $this->doAddPostActionRelatedByContentId($l);

            if ($this->postActionsRelatedByContentIdScheduledForDeletion and $this->postActionsRelatedByContentIdScheduledForDeletion->contains($l)) {
                $this->postActionsRelatedByContentIdScheduledForDeletion->remove($this->postActionsRelatedByContentIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostAction $postActionRelatedByContentId The ChildPostAction object to add.
     */
    protected function doAddPostActionRelatedByContentId(ChildPostAction $postActionRelatedByContentId)
    {
        $this->collPostActionsRelatedByContentId[]= $postActionRelatedByContentId;
        $postActionRelatedByContentId->setPostRelatedByContentId($this);
    }

    /**
     * @param  ChildPostAction $postActionRelatedByContentId The ChildPostAction object to remove.
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function removePostActionRelatedByContentId(ChildPostAction $postActionRelatedByContentId)
    {
        if ($this->getPostActionsRelatedByContentId()->contains($postActionRelatedByContentId)) {
            $pos = $this->collPostActionsRelatedByContentId->search($postActionRelatedByContentId);
            $this->collPostActionsRelatedByContentId->remove($pos);
            if (null === $this->postActionsRelatedByContentIdScheduledForDeletion) {
                $this->postActionsRelatedByContentIdScheduledForDeletion = clone $this->collPostActionsRelatedByContentId;
                $this->postActionsRelatedByContentIdScheduledForDeletion->clear();
            }
            $this->postActionsRelatedByContentIdScheduledForDeletion[]= $postActionRelatedByContentId;
            $postActionRelatedByContentId->setPostRelatedByContentId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Post is new, it will return
     * an empty collection; or if this Post has previously
     * been saved, it will retrieve related PostActionsRelatedByContentId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Post.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     */
    public function getPostActionsRelatedByContentIdJoinSection(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostActionQuery::create(null, $criteria);
        $query->joinWith('Section', $joinBehavior);

        return $this->getPostActionsRelatedByContentId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Post is new, it will return
     * an empty collection; or if this Post has previously
     * been saved, it will retrieve related PostActionsRelatedByContentId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Post.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostAction[] List of ChildPostAction objects
     */
    public function getPostActionsRelatedByContentIdJoinMedia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostActionQuery::create(null, $criteria);
        $query->joinWith('Media', $joinBehavior);

        return $this->getPostActionsRelatedByContentId($query, $con);
    }

    /**
     * Clears out the collPostConnectors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostConnectors()
     */
    public function clearPostConnectors()
    {
        $this->collPostConnectors = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostConnectors collection loaded partially.
     */
    public function resetPartialPostConnectors($v = true)
    {
        $this->collPostConnectorsPartial = $v;
    }

    /**
     * Initializes the collPostConnectors collection.
     *
     * By default this just sets the collPostConnectors collection to an empty array (like clearcollPostConnectors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostConnectors($overrideExisting = true)
    {
        if (null !== $this->collPostConnectors && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostConnectorTableMap::getTableMap()->getCollectionClassName();

        $this->collPostConnectors = new $collectionClassName;
        $this->collPostConnectors->setModel('\Database\HubPlus\PostConnector');
    }

    /**
     * Gets an array of ChildPostConnector objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPost is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostConnector[] List of ChildPostConnector objects
     * @throws PropelException
     */
    public function getPostConnectors(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostConnectorsPartial && !$this->isNew();
        if (null === $this->collPostConnectors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostConnectors) {
                // return empty collection
                $this->initPostConnectors();
            } else {
                $collPostConnectors = ChildPostConnectorQuery::create(null, $criteria)
                    ->filterByPost($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostConnectorsPartial && count($collPostConnectors)) {
                        $this->initPostConnectors(false);

                        foreach ($collPostConnectors as $obj) {
                            if (false == $this->collPostConnectors->contains($obj)) {
                                $this->collPostConnectors->append($obj);
                            }
                        }

                        $this->collPostConnectorsPartial = true;
                    }

                    return $collPostConnectors;
                }

                if ($partial && $this->collPostConnectors) {
                    foreach ($this->collPostConnectors as $obj) {
                        if ($obj->isNew()) {
                            $collPostConnectors[] = $obj;
                        }
                    }
                }

                $this->collPostConnectors = $collPostConnectors;
                $this->collPostConnectorsPartial = false;
            }
        }

        return $this->collPostConnectors;
    }

    /**
     * Sets a collection of ChildPostConnector objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postConnectors A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function setPostConnectors(Collection $postConnectors, ConnectionInterface $con = null)
    {
        /** @var ChildPostConnector[] $postConnectorsToDelete */
        $postConnectorsToDelete = $this->getPostConnectors(new Criteria(), $con)->diff($postConnectors);


        $this->postConnectorsScheduledForDeletion = $postConnectorsToDelete;

        foreach ($postConnectorsToDelete as $postConnectorRemoved) {
            $postConnectorRemoved->setPost(null);
        }

        $this->collPostConnectors = null;
        foreach ($postConnectors as $postConnector) {
            $this->addPostConnector($postConnector);
        }

        $this->collPostConnectors = $postConnectors;
        $this->collPostConnectorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostConnector objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostConnector objects.
     * @throws PropelException
     */
    public function countPostConnectors(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostConnectorsPartial && !$this->isNew();
        if (null === $this->collPostConnectors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostConnectors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostConnectors());
            }

            $query = ChildPostConnectorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPost($this)
                ->count($con);
        }

        return count($this->collPostConnectors);
    }

    /**
     * Method called to associate a ChildPostConnector object to this object
     * through the ChildPostConnector foreign key attribute.
     *
     * @param  ChildPostConnector $l ChildPostConnector
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function addPostConnector(ChildPostConnector $l)
    {
        if ($this->collPostConnectors === null) {
            $this->initPostConnectors();
            $this->collPostConnectorsPartial = true;
        }

        if (!$this->collPostConnectors->contains($l)) {
            $this->doAddPostConnector($l);

            if ($this->postConnectorsScheduledForDeletion and $this->postConnectorsScheduledForDeletion->contains($l)) {
                $this->postConnectorsScheduledForDeletion->remove($this->postConnectorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostConnector $postConnector The ChildPostConnector object to add.
     */
    protected function doAddPostConnector(ChildPostConnector $postConnector)
    {
        $this->collPostConnectors[]= $postConnector;
        $postConnector->setPost($this);
    }

    /**
     * @param  ChildPostConnector $postConnector The ChildPostConnector object to remove.
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function removePostConnector(ChildPostConnector $postConnector)
    {
        if ($this->getPostConnectors()->contains($postConnector)) {
            $pos = $this->collPostConnectors->search($postConnector);
            $this->collPostConnectors->remove($pos);
            if (null === $this->postConnectorsScheduledForDeletion) {
                $this->postConnectorsScheduledForDeletion = clone $this->collPostConnectors;
                $this->postConnectorsScheduledForDeletion->clear();
            }
            $this->postConnectorsScheduledForDeletion[]= clone $postConnector;
            $postConnector->setPost(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Post is new, it will return
     * an empty collection; or if this Post has previously
     * been saved, it will retrieve related PostConnectors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Post.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostConnector[] List of ChildPostConnector objects
     */
    public function getPostConnectorsJoinRemoteProvider(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostConnectorQuery::create(null, $criteria);
        $query->joinWith('RemoteProvider', $joinBehavior);

        return $this->getPostConnectors($query, $con);
    }

    /**
     * Clears out the collPostEvents collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostEvents()
     */
    public function clearPostEvents()
    {
        $this->collPostEvents = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostEvents collection loaded partially.
     */
    public function resetPartialPostEvents($v = true)
    {
        $this->collPostEventsPartial = $v;
    }

    /**
     * Initializes the collPostEvents collection.
     *
     * By default this just sets the collPostEvents collection to an empty array (like clearcollPostEvents());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostEvents($overrideExisting = true)
    {
        if (null !== $this->collPostEvents && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostEventTableMap::getTableMap()->getCollectionClassName();

        $this->collPostEvents = new $collectionClassName;
        $this->collPostEvents->setModel('\Database\HubPlus\PostEvent');
    }

    /**
     * Gets an array of ChildPostEvent objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPost is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostEvent[] List of ChildPostEvent objects
     * @throws PropelException
     */
    public function getPostEvents(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostEventsPartial && !$this->isNew();
        if (null === $this->collPostEvents || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostEvents) {
                // return empty collection
                $this->initPostEvents();
            } else {
                $collPostEvents = ChildPostEventQuery::create(null, $criteria)
                    ->filterByPost($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostEventsPartial && count($collPostEvents)) {
                        $this->initPostEvents(false);

                        foreach ($collPostEvents as $obj) {
                            if (false == $this->collPostEvents->contains($obj)) {
                                $this->collPostEvents->append($obj);
                            }
                        }

                        $this->collPostEventsPartial = true;
                    }

                    return $collPostEvents;
                }

                if ($partial && $this->collPostEvents) {
                    foreach ($this->collPostEvents as $obj) {
                        if ($obj->isNew()) {
                            $collPostEvents[] = $obj;
                        }
                    }
                }

                $this->collPostEvents = $collPostEvents;
                $this->collPostEventsPartial = false;
            }
        }

        return $this->collPostEvents;
    }

    /**
     * Sets a collection of ChildPostEvent objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postEvents A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function setPostEvents(Collection $postEvents, ConnectionInterface $con = null)
    {
        /** @var ChildPostEvent[] $postEventsToDelete */
        $postEventsToDelete = $this->getPostEvents(new Criteria(), $con)->diff($postEvents);


        $this->postEventsScheduledForDeletion = $postEventsToDelete;

        foreach ($postEventsToDelete as $postEventRemoved) {
            $postEventRemoved->setPost(null);
        }

        $this->collPostEvents = null;
        foreach ($postEvents as $postEvent) {
            $this->addPostEvent($postEvent);
        }

        $this->collPostEvents = $postEvents;
        $this->collPostEventsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostEvent objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostEvent objects.
     * @throws PropelException
     */
    public function countPostEvents(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostEventsPartial && !$this->isNew();
        if (null === $this->collPostEvents || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostEvents) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostEvents());
            }

            $query = ChildPostEventQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPost($this)
                ->count($con);
        }

        return count($this->collPostEvents);
    }

    /**
     * Method called to associate a ChildPostEvent object to this object
     * through the ChildPostEvent foreign key attribute.
     *
     * @param  ChildPostEvent $l ChildPostEvent
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function addPostEvent(ChildPostEvent $l)
    {
        if ($this->collPostEvents === null) {
            $this->initPostEvents();
            $this->collPostEventsPartial = true;
        }

        if (!$this->collPostEvents->contains($l)) {
            $this->doAddPostEvent($l);

            if ($this->postEventsScheduledForDeletion and $this->postEventsScheduledForDeletion->contains($l)) {
                $this->postEventsScheduledForDeletion->remove($this->postEventsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostEvent $postEvent The ChildPostEvent object to add.
     */
    protected function doAddPostEvent(ChildPostEvent $postEvent)
    {
        $this->collPostEvents[]= $postEvent;
        $postEvent->setPost($this);
    }

    /**
     * @param  ChildPostEvent $postEvent The ChildPostEvent object to remove.
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function removePostEvent(ChildPostEvent $postEvent)
    {
        if ($this->getPostEvents()->contains($postEvent)) {
            $pos = $this->collPostEvents->search($postEvent);
            $this->collPostEvents->remove($pos);
            if (null === $this->postEventsScheduledForDeletion) {
                $this->postEventsScheduledForDeletion = clone $this->collPostEvents;
                $this->postEventsScheduledForDeletion->clear();
            }
            $this->postEventsScheduledForDeletion[]= $postEvent;
            $postEvent->setPost(null);
        }

        return $this;
    }

    /**
     * Clears out the collPostExtraLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostExtraLogs()
     */
    public function clearPostExtraLogs()
    {
        $this->collPostExtraLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostExtraLogs collection loaded partially.
     */
    public function resetPartialPostExtraLogs($v = true)
    {
        $this->collPostExtraLogsPartial = $v;
    }

    /**
     * Initializes the collPostExtraLogs collection.
     *
     * By default this just sets the collPostExtraLogs collection to an empty array (like clearcollPostExtraLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostExtraLogs($overrideExisting = true)
    {
        if (null !== $this->collPostExtraLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostExtraLogTableMap::getTableMap()->getCollectionClassName();

        $this->collPostExtraLogs = new $collectionClassName;
        $this->collPostExtraLogs->setModel('\Database\HubPlus\PostExtraLog');
    }

    /**
     * Gets an array of ChildPostExtraLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPost is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostExtraLog[] List of ChildPostExtraLog objects
     * @throws PropelException
     */
    public function getPostExtraLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostExtraLogsPartial && !$this->isNew();
        if (null === $this->collPostExtraLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostExtraLogs) {
                // return empty collection
                $this->initPostExtraLogs();
            } else {
                $collPostExtraLogs = ChildPostExtraLogQuery::create(null, $criteria)
                    ->filterByPost($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostExtraLogsPartial && count($collPostExtraLogs)) {
                        $this->initPostExtraLogs(false);

                        foreach ($collPostExtraLogs as $obj) {
                            if (false == $this->collPostExtraLogs->contains($obj)) {
                                $this->collPostExtraLogs->append($obj);
                            }
                        }

                        $this->collPostExtraLogsPartial = true;
                    }

                    return $collPostExtraLogs;
                }

                if ($partial && $this->collPostExtraLogs) {
                    foreach ($this->collPostExtraLogs as $obj) {
                        if ($obj->isNew()) {
                            $collPostExtraLogs[] = $obj;
                        }
                    }
                }

                $this->collPostExtraLogs = $collPostExtraLogs;
                $this->collPostExtraLogsPartial = false;
            }
        }

        return $this->collPostExtraLogs;
    }

    /**
     * Sets a collection of ChildPostExtraLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postExtraLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function setPostExtraLogs(Collection $postExtraLogs, ConnectionInterface $con = null)
    {
        /** @var ChildPostExtraLog[] $postExtraLogsToDelete */
        $postExtraLogsToDelete = $this->getPostExtraLogs(new Criteria(), $con)->diff($postExtraLogs);


        $this->postExtraLogsScheduledForDeletion = $postExtraLogsToDelete;

        foreach ($postExtraLogsToDelete as $postExtraLogRemoved) {
            $postExtraLogRemoved->setPost(null);
        }

        $this->collPostExtraLogs = null;
        foreach ($postExtraLogs as $postExtraLog) {
            $this->addPostExtraLog($postExtraLog);
        }

        $this->collPostExtraLogs = $postExtraLogs;
        $this->collPostExtraLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostExtraLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostExtraLog objects.
     * @throws PropelException
     */
    public function countPostExtraLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostExtraLogsPartial && !$this->isNew();
        if (null === $this->collPostExtraLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostExtraLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostExtraLogs());
            }

            $query = ChildPostExtraLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPost($this)
                ->count($con);
        }

        return count($this->collPostExtraLogs);
    }

    /**
     * Method called to associate a ChildPostExtraLog object to this object
     * through the ChildPostExtraLog foreign key attribute.
     *
     * @param  ChildPostExtraLog $l ChildPostExtraLog
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function addPostExtraLog(ChildPostExtraLog $l)
    {
        if ($this->collPostExtraLogs === null) {
            $this->initPostExtraLogs();
            $this->collPostExtraLogsPartial = true;
        }

        if (!$this->collPostExtraLogs->contains($l)) {
            $this->doAddPostExtraLog($l);

            if ($this->postExtraLogsScheduledForDeletion and $this->postExtraLogsScheduledForDeletion->contains($l)) {
                $this->postExtraLogsScheduledForDeletion->remove($this->postExtraLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostExtraLog $postExtraLog The ChildPostExtraLog object to add.
     */
    protected function doAddPostExtraLog(ChildPostExtraLog $postExtraLog)
    {
        $this->collPostExtraLogs[]= $postExtraLog;
        $postExtraLog->setPost($this);
    }

    /**
     * @param  ChildPostExtraLog $postExtraLog The ChildPostExtraLog object to remove.
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function removePostExtraLog(ChildPostExtraLog $postExtraLog)
    {
        if ($this->getPostExtraLogs()->contains($postExtraLog)) {
            $pos = $this->collPostExtraLogs->search($postExtraLog);
            $this->collPostExtraLogs->remove($pos);
            if (null === $this->postExtraLogsScheduledForDeletion) {
                $this->postExtraLogsScheduledForDeletion = clone $this->collPostExtraLogs;
                $this->postExtraLogsScheduledForDeletion->clear();
            }
            $this->postExtraLogsScheduledForDeletion[]= $postExtraLog;
            $postExtraLog->setPost(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Post is new, it will return
     * an empty collection; or if this Post has previously
     * been saved, it will retrieve related PostExtraLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Post.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostExtraLog[] List of ChildPostExtraLog objects
     */
    public function getPostExtraLogsJoinDevice(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostExtraLogQuery::create(null, $criteria);
        $query->joinWith('Device', $joinBehavior);

        return $this->getPostExtraLogs($query, $con);
    }

    /**
     * Clears out the collPostLogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostLogs()
     */
    public function clearPostLogs()
    {
        $this->collPostLogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostLogs collection loaded partially.
     */
    public function resetPartialPostLogs($v = true)
    {
        $this->collPostLogsPartial = $v;
    }

    /**
     * Initializes the collPostLogs collection.
     *
     * By default this just sets the collPostLogs collection to an empty array (like clearcollPostLogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostLogs($overrideExisting = true)
    {
        if (null !== $this->collPostLogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostLogTableMap::getTableMap()->getCollectionClassName();

        $this->collPostLogs = new $collectionClassName;
        $this->collPostLogs->setModel('\Database\HubPlus\PostLog');
    }

    /**
     * Gets an array of ChildPostLog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPost is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostLog[] List of ChildPostLog objects
     * @throws PropelException
     */
    public function getPostLogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostLogsPartial && !$this->isNew();
        if (null === $this->collPostLogs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostLogs) {
                // return empty collection
                $this->initPostLogs();
            } else {
                $collPostLogs = ChildPostLogQuery::create(null, $criteria)
                    ->filterByPost($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostLogsPartial && count($collPostLogs)) {
                        $this->initPostLogs(false);

                        foreach ($collPostLogs as $obj) {
                            if (false == $this->collPostLogs->contains($obj)) {
                                $this->collPostLogs->append($obj);
                            }
                        }

                        $this->collPostLogsPartial = true;
                    }

                    return $collPostLogs;
                }

                if ($partial && $this->collPostLogs) {
                    foreach ($this->collPostLogs as $obj) {
                        if ($obj->isNew()) {
                            $collPostLogs[] = $obj;
                        }
                    }
                }

                $this->collPostLogs = $collPostLogs;
                $this->collPostLogsPartial = false;
            }
        }

        return $this->collPostLogs;
    }

    /**
     * Sets a collection of ChildPostLog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postLogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function setPostLogs(Collection $postLogs, ConnectionInterface $con = null)
    {
        /** @var ChildPostLog[] $postLogsToDelete */
        $postLogsToDelete = $this->getPostLogs(new Criteria(), $con)->diff($postLogs);


        $this->postLogsScheduledForDeletion = $postLogsToDelete;

        foreach ($postLogsToDelete as $postLogRemoved) {
            $postLogRemoved->setPost(null);
        }

        $this->collPostLogs = null;
        foreach ($postLogs as $postLog) {
            $this->addPostLog($postLog);
        }

        $this->collPostLogs = $postLogs;
        $this->collPostLogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostLog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostLog objects.
     * @throws PropelException
     */
    public function countPostLogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostLogsPartial && !$this->isNew();
        if (null === $this->collPostLogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostLogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostLogs());
            }

            $query = ChildPostLogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPost($this)
                ->count($con);
        }

        return count($this->collPostLogs);
    }

    /**
     * Method called to associate a ChildPostLog object to this object
     * through the ChildPostLog foreign key attribute.
     *
     * @param  ChildPostLog $l ChildPostLog
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function addPostLog(ChildPostLog $l)
    {
        if ($this->collPostLogs === null) {
            $this->initPostLogs();
            $this->collPostLogsPartial = true;
        }

        if (!$this->collPostLogs->contains($l)) {
            $this->doAddPostLog($l);

            if ($this->postLogsScheduledForDeletion and $this->postLogsScheduledForDeletion->contains($l)) {
                $this->postLogsScheduledForDeletion->remove($this->postLogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostLog $postLog The ChildPostLog object to add.
     */
    protected function doAddPostLog(ChildPostLog $postLog)
    {
        $this->collPostLogs[]= $postLog;
        $postLog->setPost($this);
    }

    /**
     * @param  ChildPostLog $postLog The ChildPostLog object to remove.
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function removePostLog(ChildPostLog $postLog)
    {
        if ($this->getPostLogs()->contains($postLog)) {
            $pos = $this->collPostLogs->search($postLog);
            $this->collPostLogs->remove($pos);
            if (null === $this->postLogsScheduledForDeletion) {
                $this->postLogsScheduledForDeletion = clone $this->collPostLogs;
                $this->postLogsScheduledForDeletion->clear();
            }
            $this->postLogsScheduledForDeletion[]= $postLog;
            $postLog->setPost(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Post is new, it will return
     * an empty collection; or if this Post has previously
     * been saved, it will retrieve related PostLogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Post.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPostLog[] List of ChildPostLog objects
     */
    public function getPostLogsJoinUserApp(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPostLogQuery::create(null, $criteria);
        $query->joinWith('UserApp', $joinBehavior);

        return $this->getPostLogs($query, $con);
    }

    /**
     * Clears out the collPostpeople collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostpeople()
     */
    public function clearPostpeople()
    {
        $this->collPostpeople = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostpeople collection loaded partially.
     */
    public function resetPartialPostpeople($v = true)
    {
        $this->collPostpeoplePartial = $v;
    }

    /**
     * Initializes the collPostpeople collection.
     *
     * By default this just sets the collPostpeople collection to an empty array (like clearcollPostpeople());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostpeople($overrideExisting = true)
    {
        if (null !== $this->collPostpeople && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostPersonTableMap::getTableMap()->getCollectionClassName();

        $this->collPostpeople = new $collectionClassName;
        $this->collPostpeople->setModel('\Database\HubPlus\PostPerson');
    }

    /**
     * Gets an array of ChildPostPerson objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPost is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostPerson[] List of ChildPostPerson objects
     * @throws PropelException
     */
    public function getPostpeople(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostpeoplePartial && !$this->isNew();
        if (null === $this->collPostpeople || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostpeople) {
                // return empty collection
                $this->initPostpeople();
            } else {
                $collPostpeople = ChildPostPersonQuery::create(null, $criteria)
                    ->filterByPost($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostpeoplePartial && count($collPostpeople)) {
                        $this->initPostpeople(false);

                        foreach ($collPostpeople as $obj) {
                            if (false == $this->collPostpeople->contains($obj)) {
                                $this->collPostpeople->append($obj);
                            }
                        }

                        $this->collPostpeoplePartial = true;
                    }

                    return $collPostpeople;
                }

                if ($partial && $this->collPostpeople) {
                    foreach ($this->collPostpeople as $obj) {
                        if ($obj->isNew()) {
                            $collPostpeople[] = $obj;
                        }
                    }
                }

                $this->collPostpeople = $collPostpeople;
                $this->collPostpeoplePartial = false;
            }
        }

        return $this->collPostpeople;
    }

    /**
     * Sets a collection of ChildPostPerson objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postpeople A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function setPostpeople(Collection $postpeople, ConnectionInterface $con = null)
    {
        /** @var ChildPostPerson[] $postpeopleToDelete */
        $postpeopleToDelete = $this->getPostpeople(new Criteria(), $con)->diff($postpeople);


        $this->postpeopleScheduledForDeletion = $postpeopleToDelete;

        foreach ($postpeopleToDelete as $postPersonRemoved) {
            $postPersonRemoved->setPost(null);
        }

        $this->collPostpeople = null;
        foreach ($postpeople as $postPerson) {
            $this->addPostPerson($postPerson);
        }

        $this->collPostpeople = $postpeople;
        $this->collPostpeoplePartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostPerson objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostPerson objects.
     * @throws PropelException
     */
    public function countPostpeople(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostpeoplePartial && !$this->isNew();
        if (null === $this->collPostpeople || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostpeople) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostpeople());
            }

            $query = ChildPostPersonQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPost($this)
                ->count($con);
        }

        return count($this->collPostpeople);
    }

    /**
     * Method called to associate a ChildPostPerson object to this object
     * through the ChildPostPerson foreign key attribute.
     *
     * @param  ChildPostPerson $l ChildPostPerson
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function addPostPerson(ChildPostPerson $l)
    {
        if ($this->collPostpeople === null) {
            $this->initPostpeople();
            $this->collPostpeoplePartial = true;
        }

        if (!$this->collPostpeople->contains($l)) {
            $this->doAddPostPerson($l);

            if ($this->postpeopleScheduledForDeletion and $this->postpeopleScheduledForDeletion->contains($l)) {
                $this->postpeopleScheduledForDeletion->remove($this->postpeopleScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostPerson $postPerson The ChildPostPerson object to add.
     */
    protected function doAddPostPerson(ChildPostPerson $postPerson)
    {
        $this->collPostpeople[]= $postPerson;
        $postPerson->setPost($this);
    }

    /**
     * @param  ChildPostPerson $postPerson The ChildPostPerson object to remove.
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function removePostPerson(ChildPostPerson $postPerson)
    {
        if ($this->getPostpeople()->contains($postPerson)) {
            $pos = $this->collPostpeople->search($postPerson);
            $this->collPostpeople->remove($pos);
            if (null === $this->postpeopleScheduledForDeletion) {
                $this->postpeopleScheduledForDeletion = clone $this->collPostpeople;
                $this->postpeopleScheduledForDeletion->clear();
            }
            $this->postpeopleScheduledForDeletion[]= $postPerson;
            $postPerson->setPost(null);
        }

        return $this;
    }

    /**
     * Clears out the collPostPois collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPostPois()
     */
    public function clearPostPois()
    {
        $this->collPostPois = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPostPois collection loaded partially.
     */
    public function resetPartialPostPois($v = true)
    {
        $this->collPostPoisPartial = $v;
    }

    /**
     * Initializes the collPostPois collection.
     *
     * By default this just sets the collPostPois collection to an empty array (like clearcollPostPois());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPostPois($overrideExisting = true)
    {
        if (null !== $this->collPostPois && !$overrideExisting) {
            return;
        }

        $collectionClassName = PostPoiTableMap::getTableMap()->getCollectionClassName();

        $this->collPostPois = new $collectionClassName;
        $this->collPostPois->setModel('\Database\HubPlus\PostPoi');
    }

    /**
     * Gets an array of ChildPostPoi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPost is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPostPoi[] List of ChildPostPoi objects
     * @throws PropelException
     */
    public function getPostPois(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPostPoisPartial && !$this->isNew();
        if (null === $this->collPostPois || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPostPois) {
                // return empty collection
                $this->initPostPois();
            } else {
                $collPostPois = ChildPostPoiQuery::create(null, $criteria)
                    ->filterByPost($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPostPoisPartial && count($collPostPois)) {
                        $this->initPostPois(false);

                        foreach ($collPostPois as $obj) {
                            if (false == $this->collPostPois->contains($obj)) {
                                $this->collPostPois->append($obj);
                            }
                        }

                        $this->collPostPoisPartial = true;
                    }

                    return $collPostPois;
                }

                if ($partial && $this->collPostPois) {
                    foreach ($this->collPostPois as $obj) {
                        if ($obj->isNew()) {
                            $collPostPois[] = $obj;
                        }
                    }
                }

                $this->collPostPois = $collPostPois;
                $this->collPostPoisPartial = false;
            }
        }

        return $this->collPostPois;
    }

    /**
     * Sets a collection of ChildPostPoi objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $postPois A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function setPostPois(Collection $postPois, ConnectionInterface $con = null)
    {
        /** @var ChildPostPoi[] $postPoisToDelete */
        $postPoisToDelete = $this->getPostPois(new Criteria(), $con)->diff($postPois);


        $this->postPoisScheduledForDeletion = $postPoisToDelete;

        foreach ($postPoisToDelete as $postPoiRemoved) {
            $postPoiRemoved->setPost(null);
        }

        $this->collPostPois = null;
        foreach ($postPois as $postPoi) {
            $this->addPostPoi($postPoi);
        }

        $this->collPostPois = $postPois;
        $this->collPostPoisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PostPoi objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PostPoi objects.
     * @throws PropelException
     */
    public function countPostPois(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPostPoisPartial && !$this->isNew();
        if (null === $this->collPostPois || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPostPois) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPostPois());
            }

            $query = ChildPostPoiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPost($this)
                ->count($con);
        }

        return count($this->collPostPois);
    }

    /**
     * Method called to associate a ChildPostPoi object to this object
     * through the ChildPostPoi foreign key attribute.
     *
     * @param  ChildPostPoi $l ChildPostPoi
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function addPostPoi(ChildPostPoi $l)
    {
        if ($this->collPostPois === null) {
            $this->initPostPois();
            $this->collPostPoisPartial = true;
        }

        if (!$this->collPostPois->contains($l)) {
            $this->doAddPostPoi($l);

            if ($this->postPoisScheduledForDeletion and $this->postPoisScheduledForDeletion->contains($l)) {
                $this->postPoisScheduledForDeletion->remove($this->postPoisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPostPoi $postPoi The ChildPostPoi object to add.
     */
    protected function doAddPostPoi(ChildPostPoi $postPoi)
    {
        $this->collPostPois[]= $postPoi;
        $postPoi->setPost($this);
    }

    /**
     * @param  ChildPostPoi $postPoi The ChildPostPoi object to remove.
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function removePostPoi(ChildPostPoi $postPoi)
    {
        if ($this->getPostPois()->contains($postPoi)) {
            $pos = $this->collPostPois->search($postPoi);
            $this->collPostPois->remove($pos);
            if (null === $this->postPoisScheduledForDeletion) {
                $this->postPoisScheduledForDeletion = clone $this->collPostPois;
                $this->postPoisScheduledForDeletion->clear();
            }
            $this->postPoisScheduledForDeletion[]= $postPoi;
            $postPoi->setPost(null);
        }

        return $this;
    }

    /**
     * Clears out the collPushNotifications collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPushNotifications()
     */
    public function clearPushNotifications()
    {
        $this->collPushNotifications = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPushNotifications collection loaded partially.
     */
    public function resetPartialPushNotifications($v = true)
    {
        $this->collPushNotificationsPartial = $v;
    }

    /**
     * Initializes the collPushNotifications collection.
     *
     * By default this just sets the collPushNotifications collection to an empty array (like clearcollPushNotifications());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPushNotifications($overrideExisting = true)
    {
        if (null !== $this->collPushNotifications && !$overrideExisting) {
            return;
        }

        $collectionClassName = PushNotificationTableMap::getTableMap()->getCollectionClassName();

        $this->collPushNotifications = new $collectionClassName;
        $this->collPushNotifications->setModel('\Database\HubPlus\PushNotification');
    }

    /**
     * Gets an array of ChildPushNotification objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPost is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPushNotification[] List of ChildPushNotification objects
     * @throws PropelException
     */
    public function getPushNotifications(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPushNotificationsPartial && !$this->isNew();
        if (null === $this->collPushNotifications || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPushNotifications) {
                // return empty collection
                $this->initPushNotifications();
            } else {
                $collPushNotifications = ChildPushNotificationQuery::create(null, $criteria)
                    ->filterByPost($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPushNotificationsPartial && count($collPushNotifications)) {
                        $this->initPushNotifications(false);

                        foreach ($collPushNotifications as $obj) {
                            if (false == $this->collPushNotifications->contains($obj)) {
                                $this->collPushNotifications->append($obj);
                            }
                        }

                        $this->collPushNotificationsPartial = true;
                    }

                    return $collPushNotifications;
                }

                if ($partial && $this->collPushNotifications) {
                    foreach ($this->collPushNotifications as $obj) {
                        if ($obj->isNew()) {
                            $collPushNotifications[] = $obj;
                        }
                    }
                }

                $this->collPushNotifications = $collPushNotifications;
                $this->collPushNotificationsPartial = false;
            }
        }

        return $this->collPushNotifications;
    }

    /**
     * Sets a collection of ChildPushNotification objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $pushNotifications A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function setPushNotifications(Collection $pushNotifications, ConnectionInterface $con = null)
    {
        /** @var ChildPushNotification[] $pushNotificationsToDelete */
        $pushNotificationsToDelete = $this->getPushNotifications(new Criteria(), $con)->diff($pushNotifications);


        $this->pushNotificationsScheduledForDeletion = $pushNotificationsToDelete;

        foreach ($pushNotificationsToDelete as $pushNotificationRemoved) {
            $pushNotificationRemoved->setPost(null);
        }

        $this->collPushNotifications = null;
        foreach ($pushNotifications as $pushNotification) {
            $this->addPushNotification($pushNotification);
        }

        $this->collPushNotifications = $pushNotifications;
        $this->collPushNotificationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PushNotification objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PushNotification objects.
     * @throws PropelException
     */
    public function countPushNotifications(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPushNotificationsPartial && !$this->isNew();
        if (null === $this->collPushNotifications || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPushNotifications) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPushNotifications());
            }

            $query = ChildPushNotificationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPost($this)
                ->count($con);
        }

        return count($this->collPushNotifications);
    }

    /**
     * Method called to associate a ChildPushNotification object to this object
     * through the ChildPushNotification foreign key attribute.
     *
     * @param  ChildPushNotification $l ChildPushNotification
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function addPushNotification(ChildPushNotification $l)
    {
        if ($this->collPushNotifications === null) {
            $this->initPushNotifications();
            $this->collPushNotificationsPartial = true;
        }

        if (!$this->collPushNotifications->contains($l)) {
            $this->doAddPushNotification($l);

            if ($this->pushNotificationsScheduledForDeletion and $this->pushNotificationsScheduledForDeletion->contains($l)) {
                $this->pushNotificationsScheduledForDeletion->remove($this->pushNotificationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPushNotification $pushNotification The ChildPushNotification object to add.
     */
    protected function doAddPushNotification(ChildPushNotification $pushNotification)
    {
        $this->collPushNotifications[]= $pushNotification;
        $pushNotification->setPost($this);
    }

    /**
     * @param  ChildPushNotification $pushNotification The ChildPushNotification object to remove.
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function removePushNotification(ChildPushNotification $pushNotification)
    {
        if ($this->getPushNotifications()->contains($pushNotification)) {
            $pos = $this->collPushNotifications->search($pushNotification);
            $this->collPushNotifications->remove($pos);
            if (null === $this->pushNotificationsScheduledForDeletion) {
                $this->pushNotificationsScheduledForDeletion = clone $this->collPushNotifications;
                $this->pushNotificationsScheduledForDeletion->clear();
            }
            $this->pushNotificationsScheduledForDeletion[]= $pushNotification;
            $pushNotification->setPost(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Post is new, it will return
     * an empty collection; or if this Post has previously
     * been saved, it will retrieve related PushNotifications from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Post.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPushNotification[] List of ChildPushNotification objects
     */
    public function getPushNotificationsJoinSection(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPushNotificationQuery::create(null, $criteria);
        $query->joinWith('Section', $joinBehavior);

        return $this->getPushNotifications($query, $con);
    }

    /**
     * Clears out the collWebhooks collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addWebhooks()
     */
    public function clearWebhooks()
    {
        $this->collWebhooks = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collWebhooks collection loaded partially.
     */
    public function resetPartialWebhooks($v = true)
    {
        $this->collWebhooksPartial = $v;
    }

    /**
     * Initializes the collWebhooks collection.
     *
     * By default this just sets the collWebhooks collection to an empty array (like clearcollWebhooks());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initWebhooks($overrideExisting = true)
    {
        if (null !== $this->collWebhooks && !$overrideExisting) {
            return;
        }

        $collectionClassName = WebhookTableMap::getTableMap()->getCollectionClassName();

        $this->collWebhooks = new $collectionClassName;
        $this->collWebhooks->setModel('\Database\HubPlus\Webhook');
    }

    /**
     * Gets an array of ChildWebhook objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPost is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildWebhook[] List of ChildWebhook objects
     * @throws PropelException
     */
    public function getWebhooks(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collWebhooksPartial && !$this->isNew();
        if (null === $this->collWebhooks || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collWebhooks) {
                // return empty collection
                $this->initWebhooks();
            } else {
                $collWebhooks = ChildWebhookQuery::create(null, $criteria)
                    ->filterByPost($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collWebhooksPartial && count($collWebhooks)) {
                        $this->initWebhooks(false);

                        foreach ($collWebhooks as $obj) {
                            if (false == $this->collWebhooks->contains($obj)) {
                                $this->collWebhooks->append($obj);
                            }
                        }

                        $this->collWebhooksPartial = true;
                    }

                    return $collWebhooks;
                }

                if ($partial && $this->collWebhooks) {
                    foreach ($this->collWebhooks as $obj) {
                        if ($obj->isNew()) {
                            $collWebhooks[] = $obj;
                        }
                    }
                }

                $this->collWebhooks = $collWebhooks;
                $this->collWebhooksPartial = false;
            }
        }

        return $this->collWebhooks;
    }

    /**
     * Sets a collection of ChildWebhook objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $webhooks A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function setWebhooks(Collection $webhooks, ConnectionInterface $con = null)
    {
        /** @var ChildWebhook[] $webhooksToDelete */
        $webhooksToDelete = $this->getWebhooks(new Criteria(), $con)->diff($webhooks);


        $this->webhooksScheduledForDeletion = $webhooksToDelete;

        foreach ($webhooksToDelete as $webhookRemoved) {
            $webhookRemoved->setPost(null);
        }

        $this->collWebhooks = null;
        foreach ($webhooks as $webhook) {
            $this->addWebhook($webhook);
        }

        $this->collWebhooks = $webhooks;
        $this->collWebhooksPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Webhook objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Webhook objects.
     * @throws PropelException
     */
    public function countWebhooks(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collWebhooksPartial && !$this->isNew();
        if (null === $this->collWebhooks || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collWebhooks) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getWebhooks());
            }

            $query = ChildWebhookQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPost($this)
                ->count($con);
        }

        return count($this->collWebhooks);
    }

    /**
     * Method called to associate a ChildWebhook object to this object
     * through the ChildWebhook foreign key attribute.
     *
     * @param  ChildWebhook $l ChildWebhook
     * @return $this|\Database\HubPlus\Post The current object (for fluent API support)
     */
    public function addWebhook(ChildWebhook $l)
    {
        if ($this->collWebhooks === null) {
            $this->initWebhooks();
            $this->collWebhooksPartial = true;
        }

        if (!$this->collWebhooks->contains($l)) {
            $this->doAddWebhook($l);

            if ($this->webhooksScheduledForDeletion and $this->webhooksScheduledForDeletion->contains($l)) {
                $this->webhooksScheduledForDeletion->remove($this->webhooksScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildWebhook $webhook The ChildWebhook object to add.
     */
    protected function doAddWebhook(ChildWebhook $webhook)
    {
        $this->collWebhooks[]= $webhook;
        $webhook->setPost($this);
    }

    /**
     * @param  ChildWebhook $webhook The ChildWebhook object to remove.
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function removeWebhook(ChildWebhook $webhook)
    {
        if ($this->getWebhooks()->contains($webhook)) {
            $pos = $this->collWebhooks->search($webhook);
            $this->collWebhooks->remove($pos);
            if (null === $this->webhooksScheduledForDeletion) {
                $this->webhooksScheduledForDeletion = clone $this->collWebhooks;
                $this->webhooksScheduledForDeletion->clear();
            }
            $this->webhooksScheduledForDeletion[]= $webhook;
            $webhook->setPost(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Post is new, it will return
     * an empty collection; or if this Post has previously
     * been saved, it will retrieve related Webhooks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Post.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildWebhook[] List of ChildWebhook objects
     */
    public function getWebhooksJoinRemoteProvider(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildWebhookQuery::create(null, $criteria);
        $query->joinWith('RemoteProvider', $joinBehavior);

        return $this->getWebhooks($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Post is new, it will return
     * an empty collection; or if this Post has previously
     * been saved, it will retrieve related Webhooks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Post.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildWebhook[] List of ChildWebhook objects
     */
    public function getWebhooksJoinSection(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildWebhookQuery::create(null, $criteria);
        $query->joinWith('Section', $joinBehavior);

        return $this->getWebhooks($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aTemplate) {
            $this->aTemplate->removePost($this);
        }
        if (null !== $this->aMedia) {
            $this->aMedia->removePost($this);
        }
        if (null !== $this->aUserBackend) {
            $this->aUserBackend->removePost($this);
        }
        $this->id = null;
        $this->type = null;
        $this->status = null;
        $this->author_id = null;
        $this->cover_id = null;
        $this->slug = null;
        $this->title = null;
        $this->subtitle = null;
        $this->description = null;
        $this->body = null;
        $this->search = null;
        $this->template_id = null;
        $this->layout = null;
        $this->tags = null;
        $this->tags_user = null;
        $this->row_data = null;
        $this->count_like = null;
        $this->count_share = null;
        $this->deleted_at = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collGalleries) {
                foreach ($this->collGalleries as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPostActionsRelatedByPostId) {
                foreach ($this->collPostActionsRelatedByPostId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPostActionsRelatedByContentId) {
                foreach ($this->collPostActionsRelatedByContentId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPostConnectors) {
                foreach ($this->collPostConnectors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPostEvents) {
                foreach ($this->collPostEvents as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPostExtraLogs) {
                foreach ($this->collPostExtraLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPostLogs) {
                foreach ($this->collPostLogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPostpeople) {
                foreach ($this->collPostpeople as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPostPois) {
                foreach ($this->collPostPois as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPushNotifications) {
                foreach ($this->collPushNotifications as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collWebhooks) {
                foreach ($this->collWebhooks as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collGalleries = null;
        $this->collPostActionsRelatedByPostId = null;
        $this->collPostActionsRelatedByContentId = null;
        $this->collPostConnectors = null;
        $this->collPostEvents = null;
        $this->collPostExtraLogs = null;
        $this->collPostLogs = null;
        $this->collPostpeople = null;
        $this->collPostPois = null;
        $this->collPushNotifications = null;
        $this->collWebhooks = null;
        $this->aTemplate = null;
        $this->aMedia = null;
        $this->aUserBackend = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PostTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildPost The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[PostTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    // archivable behavior

    /**
     * Get an archived version of the current object.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return     ChildPostArchive An archive object, or null if the current object was never archived
     */
    public function getArchive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            return null;
        }
        $archive = ChildPostArchiveQuery::create()
            ->filterByPrimaryKey($this->getPrimaryKey())
            ->findOne($con);

        return $archive;
    }
    /**
     * Copy the data of the current object into a $archiveTablePhpName archive object.
     * The archived object is then saved.
     * If the current object has already been archived, the archived object
     * is updated and not duplicated.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object is new
     *
     * @return     ChildPostArchive The archive object based on this object
     */
    public function archive(ConnectionInterface $con = null)
    {
        if ($this->isNew()) {
            throw new PropelException('New objects cannot be archived. You must save the current object before calling archive().');
        }
        $archive = $this->getArchive($con);
        if (!$archive) {
            $archive = new ChildPostArchive();
            $archive->setPrimaryKey($this->getPrimaryKey());
        }
        $this->copyInto($archive, $deepCopy = false, $makeNew = false);
        $archive->setArchivedAt(time());
        $archive->save($con);

        return $archive;
    }

    /**
     * Revert the the current object to the state it had when it was last archived.
     * The object must be saved afterwards if the changes must persist.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @throws PropelException If the object has no corresponding archive.
     *
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function restoreFromArchive(ConnectionInterface $con = null)
    {
        $archive = $this->getArchive($con);
        if (!$archive) {
            throw new PropelException('The current object has never been archived and cannot be restored');
        }
        $this->populateFromArchive($archive);

        return $this;
    }

    /**
     * Populates the the current object based on a $archiveTablePhpName archive object.
     *
     * @param      ChildPostArchive $archive An archived object based on the same class
      * @param      Boolean $populateAutoIncrementPrimaryKeys
     *               If true, autoincrement columns are copied from the archive object.
     *               If false, autoincrement columns are left intact.
      *
     * @return     ChildPost The current object (for fluent API support)
     */
    public function populateFromArchive($archive, $populateAutoIncrementPrimaryKeys = false) {
        if ($populateAutoIncrementPrimaryKeys) {
            $this->setId($archive->getId());
        }
        $this->setType($archive->getType());
        $this->setStatus($archive->getStatus());
        $this->setAuthorId($archive->getAuthorId());
        $this->setCoverId($archive->getCoverId());
        $this->setSlug($archive->getSlug());
        $this->setTitle($archive->getTitle());
        $this->setSubtitle($archive->getSubtitle());
        $this->setDescription($archive->getDescription());
        $this->setBody($archive->getBody());
        $this->setSearch($archive->getSearch());
        $this->setTemplateId($archive->getTemplateId());
        $this->setLayout($archive->getLayout());
        $this->setTags($archive->getTags());
        $this->setTagsUser($archive->getTagsUser());
        $this->setRowData($archive->getRowData());
        $this->setCountLike($archive->getCountLike());
        $this->setCountShare($archive->getCountShare());
        $this->setDeletedAt($archive->getDeletedAt());
        $this->setCreatedAt($archive->getCreatedAt());
        $this->setUpdatedAt($archive->getUpdatedAt());

        return $this;
    }

    /**
     * Removes the object from the database without archiving it.
     *
     * @param ConnectionInterface $con Optional connection object
     *
     * @return $this|ChildPost The current object (for fluent API support)
     */
    public function deleteWithoutArchive(ConnectionInterface $con = null)
    {
        $this->archiveOnDelete = false;

        return $this->delete($con);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
