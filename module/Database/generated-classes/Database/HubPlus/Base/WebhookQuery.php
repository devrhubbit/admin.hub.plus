<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\Webhook as ChildWebhook;
use Database\HubPlus\WebhookQuery as ChildWebhookQuery;
use Database\HubPlus\Map\WebhookTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'webhook' table.
 *
 *
 *
 * @method     ChildWebhookQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildWebhookQuery orderByBindTo($order = Criteria::ASC) Order by the bind_to column
 * @method     ChildWebhookQuery orderByHashId($order = Criteria::ASC) Order by the hash_id column
 * @method     ChildWebhookQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method     ChildWebhookQuery orderByProviderId($order = Criteria::ASC) Order by the provider_id column
 * @method     ChildWebhookQuery orderBySectionId($order = Criteria::ASC) Order by the section_id column
 * @method     ChildWebhookQuery orderByPostId($order = Criteria::ASC) Order by the post_id column
 * @method     ChildWebhookQuery orderBySecondsDelay($order = Criteria::ASC) Order by the seconds_delay column
 * @method     ChildWebhookQuery orderByMaxAttempts($order = Criteria::ASC) Order by the max_attempts column
 * @method     ChildWebhookQuery orderByEnabled($order = Criteria::ASC) Order by the enabled column
 * @method     ChildWebhookQuery orderByLastTriggerAt($order = Criteria::ASC) Order by the last_trigger_at column
 * @method     ChildWebhookQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildWebhookQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildWebhookQuery groupById() Group by the id column
 * @method     ChildWebhookQuery groupByBindTo() Group by the bind_to column
 * @method     ChildWebhookQuery groupByHashId() Group by the hash_id column
 * @method     ChildWebhookQuery groupByUrl() Group by the url column
 * @method     ChildWebhookQuery groupByProviderId() Group by the provider_id column
 * @method     ChildWebhookQuery groupBySectionId() Group by the section_id column
 * @method     ChildWebhookQuery groupByPostId() Group by the post_id column
 * @method     ChildWebhookQuery groupBySecondsDelay() Group by the seconds_delay column
 * @method     ChildWebhookQuery groupByMaxAttempts() Group by the max_attempts column
 * @method     ChildWebhookQuery groupByEnabled() Group by the enabled column
 * @method     ChildWebhookQuery groupByLastTriggerAt() Group by the last_trigger_at column
 * @method     ChildWebhookQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildWebhookQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildWebhookQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildWebhookQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildWebhookQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildWebhookQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildWebhookQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildWebhookQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildWebhookQuery leftJoinRemoteProvider($relationAlias = null) Adds a LEFT JOIN clause to the query using the RemoteProvider relation
 * @method     ChildWebhookQuery rightJoinRemoteProvider($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RemoteProvider relation
 * @method     ChildWebhookQuery innerJoinRemoteProvider($relationAlias = null) Adds a INNER JOIN clause to the query using the RemoteProvider relation
 *
 * @method     ChildWebhookQuery joinWithRemoteProvider($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RemoteProvider relation
 *
 * @method     ChildWebhookQuery leftJoinWithRemoteProvider() Adds a LEFT JOIN clause and with to the query using the RemoteProvider relation
 * @method     ChildWebhookQuery rightJoinWithRemoteProvider() Adds a RIGHT JOIN clause and with to the query using the RemoteProvider relation
 * @method     ChildWebhookQuery innerJoinWithRemoteProvider() Adds a INNER JOIN clause and with to the query using the RemoteProvider relation
 *
 * @method     ChildWebhookQuery leftJoinSection($relationAlias = null) Adds a LEFT JOIN clause to the query using the Section relation
 * @method     ChildWebhookQuery rightJoinSection($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Section relation
 * @method     ChildWebhookQuery innerJoinSection($relationAlias = null) Adds a INNER JOIN clause to the query using the Section relation
 *
 * @method     ChildWebhookQuery joinWithSection($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Section relation
 *
 * @method     ChildWebhookQuery leftJoinWithSection() Adds a LEFT JOIN clause and with to the query using the Section relation
 * @method     ChildWebhookQuery rightJoinWithSection() Adds a RIGHT JOIN clause and with to the query using the Section relation
 * @method     ChildWebhookQuery innerJoinWithSection() Adds a INNER JOIN clause and with to the query using the Section relation
 *
 * @method     ChildWebhookQuery leftJoinPost($relationAlias = null) Adds a LEFT JOIN clause to the query using the Post relation
 * @method     ChildWebhookQuery rightJoinPost($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Post relation
 * @method     ChildWebhookQuery innerJoinPost($relationAlias = null) Adds a INNER JOIN clause to the query using the Post relation
 *
 * @method     ChildWebhookQuery joinWithPost($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Post relation
 *
 * @method     ChildWebhookQuery leftJoinWithPost() Adds a LEFT JOIN clause and with to the query using the Post relation
 * @method     ChildWebhookQuery rightJoinWithPost() Adds a RIGHT JOIN clause and with to the query using the Post relation
 * @method     ChildWebhookQuery innerJoinWithPost() Adds a INNER JOIN clause and with to the query using the Post relation
 *
 * @method     ChildWebhookQuery leftJoinWebhookQueue($relationAlias = null) Adds a LEFT JOIN clause to the query using the WebhookQueue relation
 * @method     ChildWebhookQuery rightJoinWebhookQueue($relationAlias = null) Adds a RIGHT JOIN clause to the query using the WebhookQueue relation
 * @method     ChildWebhookQuery innerJoinWebhookQueue($relationAlias = null) Adds a INNER JOIN clause to the query using the WebhookQueue relation
 *
 * @method     ChildWebhookQuery joinWithWebhookQueue($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the WebhookQueue relation
 *
 * @method     ChildWebhookQuery leftJoinWithWebhookQueue() Adds a LEFT JOIN clause and with to the query using the WebhookQueue relation
 * @method     ChildWebhookQuery rightJoinWithWebhookQueue() Adds a RIGHT JOIN clause and with to the query using the WebhookQueue relation
 * @method     ChildWebhookQuery innerJoinWithWebhookQueue() Adds a INNER JOIN clause and with to the query using the WebhookQueue relation
 *
 * @method     \Database\HubPlus\RemoteProviderQuery|\Database\HubPlus\SectionQuery|\Database\HubPlus\PostQuery|\Database\HubPlus\WebhookQueueQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildWebhook findOne(ConnectionInterface $con = null) Return the first ChildWebhook matching the query
 * @method     ChildWebhook findOneOrCreate(ConnectionInterface $con = null) Return the first ChildWebhook matching the query, or a new ChildWebhook object populated from the query conditions when no match is found
 *
 * @method     ChildWebhook findOneById(int $id) Return the first ChildWebhook filtered by the id column
 * @method     ChildWebhook findOneByBindTo(string $bind_to) Return the first ChildWebhook filtered by the bind_to column
 * @method     ChildWebhook findOneByHashId(string $hash_id) Return the first ChildWebhook filtered by the hash_id column
 * @method     ChildWebhook findOneByUrl(string $url) Return the first ChildWebhook filtered by the url column
 * @method     ChildWebhook findOneByProviderId(int $provider_id) Return the first ChildWebhook filtered by the provider_id column
 * @method     ChildWebhook findOneBySectionId(int $section_id) Return the first ChildWebhook filtered by the section_id column
 * @method     ChildWebhook findOneByPostId(int $post_id) Return the first ChildWebhook filtered by the post_id column
 * @method     ChildWebhook findOneBySecondsDelay(int $seconds_delay) Return the first ChildWebhook filtered by the seconds_delay column
 * @method     ChildWebhook findOneByMaxAttempts(int $max_attempts) Return the first ChildWebhook filtered by the max_attempts column
 * @method     ChildWebhook findOneByEnabled(int $enabled) Return the first ChildWebhook filtered by the enabled column
 * @method     ChildWebhook findOneByLastTriggerAt(string $last_trigger_at) Return the first ChildWebhook filtered by the last_trigger_at column
 * @method     ChildWebhook findOneByCreatedAt(string $created_at) Return the first ChildWebhook filtered by the created_at column
 * @method     ChildWebhook findOneByUpdatedAt(string $updated_at) Return the first ChildWebhook filtered by the updated_at column *

 * @method     ChildWebhook requirePk($key, ConnectionInterface $con = null) Return the ChildWebhook by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOne(ConnectionInterface $con = null) Return the first ChildWebhook matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildWebhook requireOneById(int $id) Return the first ChildWebhook filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOneByBindTo(string $bind_to) Return the first ChildWebhook filtered by the bind_to column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOneByHashId(string $hash_id) Return the first ChildWebhook filtered by the hash_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOneByUrl(string $url) Return the first ChildWebhook filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOneByProviderId(int $provider_id) Return the first ChildWebhook filtered by the provider_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOneBySectionId(int $section_id) Return the first ChildWebhook filtered by the section_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOneByPostId(int $post_id) Return the first ChildWebhook filtered by the post_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOneBySecondsDelay(int $seconds_delay) Return the first ChildWebhook filtered by the seconds_delay column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOneByMaxAttempts(int $max_attempts) Return the first ChildWebhook filtered by the max_attempts column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOneByEnabled(int $enabled) Return the first ChildWebhook filtered by the enabled column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOneByLastTriggerAt(string $last_trigger_at) Return the first ChildWebhook filtered by the last_trigger_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOneByCreatedAt(string $created_at) Return the first ChildWebhook filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildWebhook requireOneByUpdatedAt(string $updated_at) Return the first ChildWebhook filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildWebhook[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildWebhook objects based on current ModelCriteria
 * @method     ChildWebhook[]|ObjectCollection findById(int $id) Return ChildWebhook objects filtered by the id column
 * @method     ChildWebhook[]|ObjectCollection findByBindTo(string $bind_to) Return ChildWebhook objects filtered by the bind_to column
 * @method     ChildWebhook[]|ObjectCollection findByHashId(string $hash_id) Return ChildWebhook objects filtered by the hash_id column
 * @method     ChildWebhook[]|ObjectCollection findByUrl(string $url) Return ChildWebhook objects filtered by the url column
 * @method     ChildWebhook[]|ObjectCollection findByProviderId(int $provider_id) Return ChildWebhook objects filtered by the provider_id column
 * @method     ChildWebhook[]|ObjectCollection findBySectionId(int $section_id) Return ChildWebhook objects filtered by the section_id column
 * @method     ChildWebhook[]|ObjectCollection findByPostId(int $post_id) Return ChildWebhook objects filtered by the post_id column
 * @method     ChildWebhook[]|ObjectCollection findBySecondsDelay(int $seconds_delay) Return ChildWebhook objects filtered by the seconds_delay column
 * @method     ChildWebhook[]|ObjectCollection findByMaxAttempts(int $max_attempts) Return ChildWebhook objects filtered by the max_attempts column
 * @method     ChildWebhook[]|ObjectCollection findByEnabled(int $enabled) Return ChildWebhook objects filtered by the enabled column
 * @method     ChildWebhook[]|ObjectCollection findByLastTriggerAt(string $last_trigger_at) Return ChildWebhook objects filtered by the last_trigger_at column
 * @method     ChildWebhook[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildWebhook objects filtered by the created_at column
 * @method     ChildWebhook[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildWebhook objects filtered by the updated_at column
 * @method     ChildWebhook[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class WebhookQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\WebhookQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\Webhook', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildWebhookQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildWebhookQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildWebhookQuery) {
            return $criteria;
        }
        $query = new ChildWebhookQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildWebhook|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(WebhookTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = WebhookTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWebhook A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, bind_to, hash_id, url, provider_id, section_id, post_id, seconds_delay, max_attempts, enabled, last_trigger_at, created_at, updated_at FROM webhook WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildWebhook $obj */
            $obj = new ChildWebhook();
            $obj->hydrate($row);
            WebhookTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildWebhook|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(WebhookTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(WebhookTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(WebhookTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(WebhookTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the bind_to column
     *
     * Example usage:
     * <code>
     * $query->filterByBindTo('fooValue');   // WHERE bind_to = 'fooValue'
     * $query->filterByBindTo('%fooValue%', Criteria::LIKE); // WHERE bind_to LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bindTo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByBindTo($bindTo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bindTo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_BIND_TO, $bindTo, $comparison);
    }

    /**
     * Filter the query on the hash_id column
     *
     * Example usage:
     * <code>
     * $query->filterByHashId('fooValue');   // WHERE hash_id = 'fooValue'
     * $query->filterByHashId('%fooValue%', Criteria::LIKE); // WHERE hash_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hashId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByHashId($hashId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hashId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_HASH_ID, $hashId, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the provider_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProviderId(1234); // WHERE provider_id = 1234
     * $query->filterByProviderId(array(12, 34)); // WHERE provider_id IN (12, 34)
     * $query->filterByProviderId(array('min' => 12)); // WHERE provider_id > 12
     * </code>
     *
     * @see       filterByRemoteProvider()
     *
     * @param     mixed $providerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByProviderId($providerId = null, $comparison = null)
    {
        if (is_array($providerId)) {
            $useMinMax = false;
            if (isset($providerId['min'])) {
                $this->addUsingAlias(WebhookTableMap::COL_PROVIDER_ID, $providerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($providerId['max'])) {
                $this->addUsingAlias(WebhookTableMap::COL_PROVIDER_ID, $providerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_PROVIDER_ID, $providerId, $comparison);
    }

    /**
     * Filter the query on the section_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySectionId(1234); // WHERE section_id = 1234
     * $query->filterBySectionId(array(12, 34)); // WHERE section_id IN (12, 34)
     * $query->filterBySectionId(array('min' => 12)); // WHERE section_id > 12
     * </code>
     *
     * @see       filterBySection()
     *
     * @param     mixed $sectionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterBySectionId($sectionId = null, $comparison = null)
    {
        if (is_array($sectionId)) {
            $useMinMax = false;
            if (isset($sectionId['min'])) {
                $this->addUsingAlias(WebhookTableMap::COL_SECTION_ID, $sectionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sectionId['max'])) {
                $this->addUsingAlias(WebhookTableMap::COL_SECTION_ID, $sectionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_SECTION_ID, $sectionId, $comparison);
    }

    /**
     * Filter the query on the post_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPostId(1234); // WHERE post_id = 1234
     * $query->filterByPostId(array(12, 34)); // WHERE post_id IN (12, 34)
     * $query->filterByPostId(array('min' => 12)); // WHERE post_id > 12
     * </code>
     *
     * @see       filterByPost()
     *
     * @param     mixed $postId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByPostId($postId = null, $comparison = null)
    {
        if (is_array($postId)) {
            $useMinMax = false;
            if (isset($postId['min'])) {
                $this->addUsingAlias(WebhookTableMap::COL_POST_ID, $postId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($postId['max'])) {
                $this->addUsingAlias(WebhookTableMap::COL_POST_ID, $postId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_POST_ID, $postId, $comparison);
    }

    /**
     * Filter the query on the seconds_delay column
     *
     * Example usage:
     * <code>
     * $query->filterBySecondsDelay(1234); // WHERE seconds_delay = 1234
     * $query->filterBySecondsDelay(array(12, 34)); // WHERE seconds_delay IN (12, 34)
     * $query->filterBySecondsDelay(array('min' => 12)); // WHERE seconds_delay > 12
     * </code>
     *
     * @param     mixed $secondsDelay The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterBySecondsDelay($secondsDelay = null, $comparison = null)
    {
        if (is_array($secondsDelay)) {
            $useMinMax = false;
            if (isset($secondsDelay['min'])) {
                $this->addUsingAlias(WebhookTableMap::COL_SECONDS_DELAY, $secondsDelay['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($secondsDelay['max'])) {
                $this->addUsingAlias(WebhookTableMap::COL_SECONDS_DELAY, $secondsDelay['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_SECONDS_DELAY, $secondsDelay, $comparison);
    }

    /**
     * Filter the query on the max_attempts column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxAttempts(1234); // WHERE max_attempts = 1234
     * $query->filterByMaxAttempts(array(12, 34)); // WHERE max_attempts IN (12, 34)
     * $query->filterByMaxAttempts(array('min' => 12)); // WHERE max_attempts > 12
     * </code>
     *
     * @param     mixed $maxAttempts The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByMaxAttempts($maxAttempts = null, $comparison = null)
    {
        if (is_array($maxAttempts)) {
            $useMinMax = false;
            if (isset($maxAttempts['min'])) {
                $this->addUsingAlias(WebhookTableMap::COL_MAX_ATTEMPTS, $maxAttempts['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxAttempts['max'])) {
                $this->addUsingAlias(WebhookTableMap::COL_MAX_ATTEMPTS, $maxAttempts['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_MAX_ATTEMPTS, $maxAttempts, $comparison);
    }

    /**
     * Filter the query on the enabled column
     *
     * Example usage:
     * <code>
     * $query->filterByEnabled(1234); // WHERE enabled = 1234
     * $query->filterByEnabled(array(12, 34)); // WHERE enabled IN (12, 34)
     * $query->filterByEnabled(array('min' => 12)); // WHERE enabled > 12
     * </code>
     *
     * @param     mixed $enabled The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByEnabled($enabled = null, $comparison = null)
    {
        if (is_array($enabled)) {
            $useMinMax = false;
            if (isset($enabled['min'])) {
                $this->addUsingAlias(WebhookTableMap::COL_ENABLED, $enabled['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($enabled['max'])) {
                $this->addUsingAlias(WebhookTableMap::COL_ENABLED, $enabled['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_ENABLED, $enabled, $comparison);
    }

    /**
     * Filter the query on the last_trigger_at column
     *
     * Example usage:
     * <code>
     * $query->filterByLastTriggerAt('2011-03-14'); // WHERE last_trigger_at = '2011-03-14'
     * $query->filterByLastTriggerAt('now'); // WHERE last_trigger_at = '2011-03-14'
     * $query->filterByLastTriggerAt(array('max' => 'yesterday')); // WHERE last_trigger_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastTriggerAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByLastTriggerAt($lastTriggerAt = null, $comparison = null)
    {
        if (is_array($lastTriggerAt)) {
            $useMinMax = false;
            if (isset($lastTriggerAt['min'])) {
                $this->addUsingAlias(WebhookTableMap::COL_LAST_TRIGGER_AT, $lastTriggerAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastTriggerAt['max'])) {
                $this->addUsingAlias(WebhookTableMap::COL_LAST_TRIGGER_AT, $lastTriggerAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_LAST_TRIGGER_AT, $lastTriggerAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(WebhookTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(WebhookTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(WebhookTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(WebhookTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WebhookTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Database\HubPlus\RemoteProvider object
     *
     * @param \Database\HubPlus\RemoteProvider|ObjectCollection $remoteProvider The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByRemoteProvider($remoteProvider, $comparison = null)
    {
        if ($remoteProvider instanceof \Database\HubPlus\RemoteProvider) {
            return $this
                ->addUsingAlias(WebhookTableMap::COL_PROVIDER_ID, $remoteProvider->getId(), $comparison);
        } elseif ($remoteProvider instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WebhookTableMap::COL_PROVIDER_ID, $remoteProvider->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByRemoteProvider() only accepts arguments of type \Database\HubPlus\RemoteProvider or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RemoteProvider relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function joinRemoteProvider($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RemoteProvider');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RemoteProvider');
        }

        return $this;
    }

    /**
     * Use the RemoteProvider relation RemoteProvider object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\RemoteProviderQuery A secondary query class using the current class as primary query
     */
    public function useRemoteProviderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRemoteProvider($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RemoteProvider', '\Database\HubPlus\RemoteProviderQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Section object
     *
     * @param \Database\HubPlus\Section|ObjectCollection $section The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWebhookQuery The current query, for fluid interface
     */
    public function filterBySection($section, $comparison = null)
    {
        if ($section instanceof \Database\HubPlus\Section) {
            return $this
                ->addUsingAlias(WebhookTableMap::COL_SECTION_ID, $section->getId(), $comparison);
        } elseif ($section instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WebhookTableMap::COL_SECTION_ID, $section->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySection() only accepts arguments of type \Database\HubPlus\Section or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Section relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function joinSection($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Section');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Section');
        }

        return $this;
    }

    /**
     * Use the Section relation Section object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\SectionQuery A secondary query class using the current class as primary query
     */
    public function useSectionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSection($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Section', '\Database\HubPlus\SectionQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\Post object
     *
     * @param \Database\HubPlus\Post|ObjectCollection $post The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByPost($post, $comparison = null)
    {
        if ($post instanceof \Database\HubPlus\Post) {
            return $this
                ->addUsingAlias(WebhookTableMap::COL_POST_ID, $post->getId(), $comparison);
        } elseif ($post instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(WebhookTableMap::COL_POST_ID, $post->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPost() only accepts arguments of type \Database\HubPlus\Post or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Post relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function joinPost($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Post');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Post');
        }

        return $this;
    }

    /**
     * Use the Post relation Post object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\PostQuery A secondary query class using the current class as primary query
     */
    public function usePostQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPost($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Post', '\Database\HubPlus\PostQuery');
    }

    /**
     * Filter the query by a related \Database\HubPlus\WebhookQueue object
     *
     * @param \Database\HubPlus\WebhookQueue|ObjectCollection $webhookQueue the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildWebhookQuery The current query, for fluid interface
     */
    public function filterByWebhookQueue($webhookQueue, $comparison = null)
    {
        if ($webhookQueue instanceof \Database\HubPlus\WebhookQueue) {
            return $this
                ->addUsingAlias(WebhookTableMap::COL_ID, $webhookQueue->getWebhookId(), $comparison);
        } elseif ($webhookQueue instanceof ObjectCollection) {
            return $this
                ->useWebhookQueueQuery()
                ->filterByPrimaryKeys($webhookQueue->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByWebhookQueue() only accepts arguments of type \Database\HubPlus\WebhookQueue or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the WebhookQueue relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function joinWebhookQueue($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('WebhookQueue');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'WebhookQueue');
        }

        return $this;
    }

    /**
     * Use the WebhookQueue relation WebhookQueue object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Database\HubPlus\WebhookQueueQuery A secondary query class using the current class as primary query
     */
    public function useWebhookQueueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinWebhookQueue($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'WebhookQueue', '\Database\HubPlus\WebhookQueueQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildWebhook $webhook Object to remove from the list of results
     *
     * @return $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function prune($webhook = null)
    {
        if ($webhook) {
            $this->addUsingAlias(WebhookTableMap::COL_ID, $webhook->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the webhook table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WebhookTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            WebhookTableMap::clearInstancePool();
            WebhookTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WebhookTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(WebhookTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            WebhookTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            WebhookTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(WebhookTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(WebhookTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(WebhookTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(WebhookTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(WebhookTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildWebhookQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(WebhookTableMap::COL_CREATED_AT);
    }

} // WebhookQuery
