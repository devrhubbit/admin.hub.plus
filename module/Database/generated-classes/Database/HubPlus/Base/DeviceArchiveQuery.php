<?php

namespace Database\HubPlus\Base;

use \Exception;
use \PDO;
use Database\HubPlus\DeviceArchive as ChildDeviceArchive;
use Database\HubPlus\DeviceArchiveQuery as ChildDeviceArchiveQuery;
use Database\HubPlus\Map\DeviceArchiveTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'device_archive' table.
 *
 *
 *
 * @method     ChildDeviceArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildDeviceArchiveQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildDeviceArchiveQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildDeviceArchiveQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildDeviceArchiveQuery orderByOs($order = Criteria::ASC) Order by the os column
 * @method     ChildDeviceArchiveQuery orderByOsVersion($order = Criteria::ASC) Order by the os_version column
 * @method     ChildDeviceArchiveQuery orderByEnableNotification($order = Criteria::ASC) Order by the enable_notification column
 * @method     ChildDeviceArchiveQuery orderByApiVersion($order = Criteria::ASC) Order by the api_version column
 * @method     ChildDeviceArchiveQuery orderByLanguage($order = Criteria::ASC) Order by the language column
 * @method     ChildDeviceArchiveQuery orderByDemo($order = Criteria::ASC) Order by the demo column
 * @method     ChildDeviceArchiveQuery orderByToken($order = Criteria::ASC) Order by the token column
 * @method     ChildDeviceArchiveQuery orderByApiKey($order = Criteria::ASC) Order by the api_key column
 * @method     ChildDeviceArchiveQuery orderByIpAddress($order = Criteria::ASC) Order by the ip_address column
 * @method     ChildDeviceArchiveQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method     ChildDeviceArchiveQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildDeviceArchiveQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildDeviceArchiveQuery orderByArchivedAt($order = Criteria::ASC) Order by the archived_at column
 *
 * @method     ChildDeviceArchiveQuery groupById() Group by the id column
 * @method     ChildDeviceArchiveQuery groupByUserId() Group by the user_id column
 * @method     ChildDeviceArchiveQuery groupByStatus() Group by the status column
 * @method     ChildDeviceArchiveQuery groupByType() Group by the type column
 * @method     ChildDeviceArchiveQuery groupByOs() Group by the os column
 * @method     ChildDeviceArchiveQuery groupByOsVersion() Group by the os_version column
 * @method     ChildDeviceArchiveQuery groupByEnableNotification() Group by the enable_notification column
 * @method     ChildDeviceArchiveQuery groupByApiVersion() Group by the api_version column
 * @method     ChildDeviceArchiveQuery groupByLanguage() Group by the language column
 * @method     ChildDeviceArchiveQuery groupByDemo() Group by the demo column
 * @method     ChildDeviceArchiveQuery groupByToken() Group by the token column
 * @method     ChildDeviceArchiveQuery groupByApiKey() Group by the api_key column
 * @method     ChildDeviceArchiveQuery groupByIpAddress() Group by the ip_address column
 * @method     ChildDeviceArchiveQuery groupByDeletedAt() Group by the deleted_at column
 * @method     ChildDeviceArchiveQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildDeviceArchiveQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildDeviceArchiveQuery groupByArchivedAt() Group by the archived_at column
 *
 * @method     ChildDeviceArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildDeviceArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildDeviceArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildDeviceArchiveQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildDeviceArchiveQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildDeviceArchiveQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildDeviceArchive findOne(ConnectionInterface $con = null) Return the first ChildDeviceArchive matching the query
 * @method     ChildDeviceArchive findOneOrCreate(ConnectionInterface $con = null) Return the first ChildDeviceArchive matching the query, or a new ChildDeviceArchive object populated from the query conditions when no match is found
 *
 * @method     ChildDeviceArchive findOneById(int $id) Return the first ChildDeviceArchive filtered by the id column
 * @method     ChildDeviceArchive findOneByUserId(int $user_id) Return the first ChildDeviceArchive filtered by the user_id column
 * @method     ChildDeviceArchive findOneByStatus(string $status) Return the first ChildDeviceArchive filtered by the status column
 * @method     ChildDeviceArchive findOneByType(string $type) Return the first ChildDeviceArchive filtered by the type column
 * @method     ChildDeviceArchive findOneByOs(string $os) Return the first ChildDeviceArchive filtered by the os column
 * @method     ChildDeviceArchive findOneByOsVersion(string $os_version) Return the first ChildDeviceArchive filtered by the os_version column
 * @method     ChildDeviceArchive findOneByEnableNotification(boolean $enable_notification) Return the first ChildDeviceArchive filtered by the enable_notification column
 * @method     ChildDeviceArchive findOneByApiVersion(string $api_version) Return the first ChildDeviceArchive filtered by the api_version column
 * @method     ChildDeviceArchive findOneByLanguage(string $language) Return the first ChildDeviceArchive filtered by the language column
 * @method     ChildDeviceArchive findOneByDemo(boolean $demo) Return the first ChildDeviceArchive filtered by the demo column
 * @method     ChildDeviceArchive findOneByToken(string $token) Return the first ChildDeviceArchive filtered by the token column
 * @method     ChildDeviceArchive findOneByApiKey(string $api_key) Return the first ChildDeviceArchive filtered by the api_key column
 * @method     ChildDeviceArchive findOneByIpAddress(string $ip_address) Return the first ChildDeviceArchive filtered by the ip_address column
 * @method     ChildDeviceArchive findOneByDeletedAt(string $deleted_at) Return the first ChildDeviceArchive filtered by the deleted_at column
 * @method     ChildDeviceArchive findOneByCreatedAt(string $created_at) Return the first ChildDeviceArchive filtered by the created_at column
 * @method     ChildDeviceArchive findOneByUpdatedAt(string $updated_at) Return the first ChildDeviceArchive filtered by the updated_at column
 * @method     ChildDeviceArchive findOneByArchivedAt(string $archived_at) Return the first ChildDeviceArchive filtered by the archived_at column *

 * @method     ChildDeviceArchive requirePk($key, ConnectionInterface $con = null) Return the ChildDeviceArchive by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOne(ConnectionInterface $con = null) Return the first ChildDeviceArchive matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDeviceArchive requireOneById(int $id) Return the first ChildDeviceArchive filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByUserId(int $user_id) Return the first ChildDeviceArchive filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByStatus(string $status) Return the first ChildDeviceArchive filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByType(string $type) Return the first ChildDeviceArchive filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByOs(string $os) Return the first ChildDeviceArchive filtered by the os column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByOsVersion(string $os_version) Return the first ChildDeviceArchive filtered by the os_version column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByEnableNotification(boolean $enable_notification) Return the first ChildDeviceArchive filtered by the enable_notification column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByApiVersion(string $api_version) Return the first ChildDeviceArchive filtered by the api_version column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByLanguage(string $language) Return the first ChildDeviceArchive filtered by the language column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByDemo(boolean $demo) Return the first ChildDeviceArchive filtered by the demo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByToken(string $token) Return the first ChildDeviceArchive filtered by the token column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByApiKey(string $api_key) Return the first ChildDeviceArchive filtered by the api_key column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByIpAddress(string $ip_address) Return the first ChildDeviceArchive filtered by the ip_address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByDeletedAt(string $deleted_at) Return the first ChildDeviceArchive filtered by the deleted_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByCreatedAt(string $created_at) Return the first ChildDeviceArchive filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByUpdatedAt(string $updated_at) Return the first ChildDeviceArchive filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDeviceArchive requireOneByArchivedAt(string $archived_at) Return the first ChildDeviceArchive filtered by the archived_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDeviceArchive[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildDeviceArchive objects based on current ModelCriteria
 * @method     ChildDeviceArchive[]|ObjectCollection findById(int $id) Return ChildDeviceArchive objects filtered by the id column
 * @method     ChildDeviceArchive[]|ObjectCollection findByUserId(int $user_id) Return ChildDeviceArchive objects filtered by the user_id column
 * @method     ChildDeviceArchive[]|ObjectCollection findByStatus(string $status) Return ChildDeviceArchive objects filtered by the status column
 * @method     ChildDeviceArchive[]|ObjectCollection findByType(string $type) Return ChildDeviceArchive objects filtered by the type column
 * @method     ChildDeviceArchive[]|ObjectCollection findByOs(string $os) Return ChildDeviceArchive objects filtered by the os column
 * @method     ChildDeviceArchive[]|ObjectCollection findByOsVersion(string $os_version) Return ChildDeviceArchive objects filtered by the os_version column
 * @method     ChildDeviceArchive[]|ObjectCollection findByEnableNotification(boolean $enable_notification) Return ChildDeviceArchive objects filtered by the enable_notification column
 * @method     ChildDeviceArchive[]|ObjectCollection findByApiVersion(string $api_version) Return ChildDeviceArchive objects filtered by the api_version column
 * @method     ChildDeviceArchive[]|ObjectCollection findByLanguage(string $language) Return ChildDeviceArchive objects filtered by the language column
 * @method     ChildDeviceArchive[]|ObjectCollection findByDemo(boolean $demo) Return ChildDeviceArchive objects filtered by the demo column
 * @method     ChildDeviceArchive[]|ObjectCollection findByToken(string $token) Return ChildDeviceArchive objects filtered by the token column
 * @method     ChildDeviceArchive[]|ObjectCollection findByApiKey(string $api_key) Return ChildDeviceArchive objects filtered by the api_key column
 * @method     ChildDeviceArchive[]|ObjectCollection findByIpAddress(string $ip_address) Return ChildDeviceArchive objects filtered by the ip_address column
 * @method     ChildDeviceArchive[]|ObjectCollection findByDeletedAt(string $deleted_at) Return ChildDeviceArchive objects filtered by the deleted_at column
 * @method     ChildDeviceArchive[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildDeviceArchive objects filtered by the created_at column
 * @method     ChildDeviceArchive[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildDeviceArchive objects filtered by the updated_at column
 * @method     ChildDeviceArchive[]|ObjectCollection findByArchivedAt(string $archived_at) Return ChildDeviceArchive objects filtered by the archived_at column
 * @method     ChildDeviceArchive[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class DeviceArchiveQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Database\HubPlus\Base\DeviceArchiveQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hp_core', $modelName = '\\Database\\HubPlus\\DeviceArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildDeviceArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildDeviceArchiveQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildDeviceArchiveQuery) {
            return $criteria;
        }
        $query = new ChildDeviceArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildDeviceArchive|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DeviceArchiveTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = DeviceArchiveTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDeviceArchive A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, status, type, os, os_version, enable_notification, api_version, language, demo, token, api_key, ip_address, deleted_at, created_at, updated_at, archived_at FROM device_archive WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildDeviceArchive $obj */
            $obj = new ChildDeviceArchive();
            $obj->hydrate($row);
            DeviceArchiveTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildDeviceArchive|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DeviceArchiveTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DeviceArchiveTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(DeviceArchiveTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(DeviceArchiveTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%', Criteria::LIKE); // WHERE status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the os column
     *
     * Example usage:
     * <code>
     * $query->filterByOs('fooValue');   // WHERE os = 'fooValue'
     * $query->filterByOs('%fooValue%', Criteria::LIKE); // WHERE os LIKE '%fooValue%'
     * </code>
     *
     * @param     string $os The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByOs($os = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($os)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_OS, $os, $comparison);
    }

    /**
     * Filter the query on the os_version column
     *
     * Example usage:
     * <code>
     * $query->filterByOsVersion('fooValue');   // WHERE os_version = 'fooValue'
     * $query->filterByOsVersion('%fooValue%', Criteria::LIKE); // WHERE os_version LIKE '%fooValue%'
     * </code>
     *
     * @param     string $osVersion The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByOsVersion($osVersion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($osVersion)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_OS_VERSION, $osVersion, $comparison);
    }

    /**
     * Filter the query on the enable_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByEnableNotification(true); // WHERE enable_notification = true
     * $query->filterByEnableNotification('yes'); // WHERE enable_notification = true
     * </code>
     *
     * @param     boolean|string $enableNotification The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByEnableNotification($enableNotification = null, $comparison = null)
    {
        if (is_string($enableNotification)) {
            $enableNotification = in_array(strtolower($enableNotification), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_ENABLE_NOTIFICATION, $enableNotification, $comparison);
    }

    /**
     * Filter the query on the api_version column
     *
     * Example usage:
     * <code>
     * $query->filterByApiVersion('fooValue');   // WHERE api_version = 'fooValue'
     * $query->filterByApiVersion('%fooValue%', Criteria::LIKE); // WHERE api_version LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiVersion The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByApiVersion($apiVersion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiVersion)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_API_VERSION, $apiVersion, $comparison);
    }

    /**
     * Filter the query on the language column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguage('fooValue');   // WHERE language = 'fooValue'
     * $query->filterByLanguage('%fooValue%', Criteria::LIKE); // WHERE language LIKE '%fooValue%'
     * </code>
     *
     * @param     string $language The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByLanguage($language = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($language)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_LANGUAGE, $language, $comparison);
    }

    /**
     * Filter the query on the demo column
     *
     * Example usage:
     * <code>
     * $query->filterByDemo(true); // WHERE demo = true
     * $query->filterByDemo('yes'); // WHERE demo = true
     * </code>
     *
     * @param     boolean|string $demo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByDemo($demo = null, $comparison = null)
    {
        if (is_string($demo)) {
            $demo = in_array(strtolower($demo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_DEMO, $demo, $comparison);
    }

    /**
     * Filter the query on the token column
     *
     * Example usage:
     * <code>
     * $query->filterByToken('fooValue');   // WHERE token = 'fooValue'
     * $query->filterByToken('%fooValue%', Criteria::LIKE); // WHERE token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $token The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByToken($token = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($token)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_TOKEN, $token, $comparison);
    }

    /**
     * Filter the query on the api_key column
     *
     * Example usage:
     * <code>
     * $query->filterByApiKey('fooValue');   // WHERE api_key = 'fooValue'
     * $query->filterByApiKey('%fooValue%', Criteria::LIKE); // WHERE api_key LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiKey The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByApiKey($apiKey = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiKey)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_API_KEY, $apiKey, $comparison);
    }

    /**
     * Filter the query on the ip_address column
     *
     * Example usage:
     * <code>
     * $query->filterByIpAddress('fooValue');   // WHERE ip_address = 'fooValue'
     * $query->filterByIpAddress('%fooValue%', Criteria::LIKE); // WHERE ip_address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ipAddress The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByIpAddress($ipAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ipAddress)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_IP_ADDRESS, $ipAddress, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(DeviceArchiveTableMap::COL_DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(DeviceArchiveTableMap::COL_DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(DeviceArchiveTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(DeviceArchiveTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(DeviceArchiveTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(DeviceArchiveTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the archived_at column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivedAt('2011-03-14'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt('now'); // WHERE archived_at = '2011-03-14'
     * $query->filterByArchivedAt(array('max' => 'yesterday')); // WHERE archived_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $archivedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function filterByArchivedAt($archivedAt = null, $comparison = null)
    {
        if (is_array($archivedAt)) {
            $useMinMax = false;
            if (isset($archivedAt['min'])) {
                $this->addUsingAlias(DeviceArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($archivedAt['max'])) {
                $this->addUsingAlias(DeviceArchiveTableMap::COL_ARCHIVED_AT, $archivedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DeviceArchiveTableMap::COL_ARCHIVED_AT, $archivedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildDeviceArchive $deviceArchive Object to remove from the list of results
     *
     * @return $this|ChildDeviceArchiveQuery The current query, for fluid interface
     */
    public function prune($deviceArchive = null)
    {
        if ($deviceArchive) {
            $this->addUsingAlias(DeviceArchiveTableMap::COL_ID, $deviceArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the device_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceArchiveTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            DeviceArchiveTableMap::clearInstancePool();
            DeviceArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceArchiveTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(DeviceArchiveTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            DeviceArchiveTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            DeviceArchiveTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // DeviceArchiveQuery
