<?php

namespace Database\HubPlus;

use Database\HubPlus\Base\GroupArchiveQuery as BaseGroupArchiveQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'group_archive' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class GroupArchiveQuery extends BaseGroupArchiveQuery
{

}
