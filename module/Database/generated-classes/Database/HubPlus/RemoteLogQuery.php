<?php

namespace Database\HubPlus;

use Database\HubPlus\Base\RemoteLogQuery as BaseRemoteLogQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'remote_log' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RemoteLogQuery extends BaseRemoteLogQuery
{

}
