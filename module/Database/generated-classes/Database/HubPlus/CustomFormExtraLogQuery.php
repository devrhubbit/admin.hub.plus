<?php

namespace Database\HubPlus;

use Database\HubPlus\Base\CustomFormExtraLogQuery as BaseCustomFormExtraLogQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'custom_form_extra_log' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomFormExtraLogQuery extends BaseCustomFormExtraLogQuery
{

}
