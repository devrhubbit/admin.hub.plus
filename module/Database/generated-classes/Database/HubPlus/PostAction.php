<?php

namespace Database\HubPlus;

use Database\HubPlus\Base\PostAction as BasePostAction;

/**
 * Skeleton subclass for representing a row from the 'post_action' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PostAction extends BasePostAction
{

}
