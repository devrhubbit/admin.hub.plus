<?php

namespace Database\HubPlus;

use Database\HubPlus\Base\PostActionArchive as BasePostActionArchive;

/**
 * Skeleton subclass for representing a row from the 'post_action_archive' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PostActionArchive extends BasePostActionArchive
{

}
