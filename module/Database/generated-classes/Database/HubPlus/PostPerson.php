<?php

namespace Database\HubPlus;

use Database\HubPlus\Base\PostPerson as BasePostPerson;

/**
 * Skeleton subclass for representing a row from the 'post_person' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PostPerson extends BasePostPerson
{

}
