<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\TemplateArchive;
use Database\HubPlus\TemplateArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'template_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class TemplateArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.TemplateArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'template_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\TemplateArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.TemplateArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id field
     */
    const COL_ID = 'template_archive.id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'template_archive.title';

    /**
     * the column name for the subtitle field
     */
    const COL_SUBTITLE = 'template_archive.subtitle';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'template_archive.description';

    /**
     * the column name for the icon field
     */
    const COL_ICON = 'template_archive.icon';

    /**
     * the column name for the cover field
     */
    const COL_COVER = 'template_archive.cover';

    /**
     * the column name for the json_views field
     */
    const COL_JSON_VIEWS = 'template_archive.json_views';

    /**
     * the column name for the layout field
     */
    const COL_LAYOUT = 'template_archive.layout';

    /**
     * the column name for the enabled field
     */
    const COL_ENABLED = 'template_archive.enabled';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'template_archive.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'template_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'template_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'template_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Title', 'Subtitle', 'Description', 'Icon', 'Cover', 'JsonViews', 'Layout', 'Enabled', 'DeletedAt', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'title', 'subtitle', 'description', 'icon', 'cover', 'jsonViews', 'layout', 'enabled', 'deletedAt', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(TemplateArchiveTableMap::COL_ID, TemplateArchiveTableMap::COL_TITLE, TemplateArchiveTableMap::COL_SUBTITLE, TemplateArchiveTableMap::COL_DESCRIPTION, TemplateArchiveTableMap::COL_ICON, TemplateArchiveTableMap::COL_COVER, TemplateArchiveTableMap::COL_JSON_VIEWS, TemplateArchiveTableMap::COL_LAYOUT, TemplateArchiveTableMap::COL_ENABLED, TemplateArchiveTableMap::COL_DELETED_AT, TemplateArchiveTableMap::COL_CREATED_AT, TemplateArchiveTableMap::COL_UPDATED_AT, TemplateArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'title', 'subtitle', 'description', 'icon', 'cover', 'json_views', 'layout', 'enabled', 'deleted_at', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Title' => 1, 'Subtitle' => 2, 'Description' => 3, 'Icon' => 4, 'Cover' => 5, 'JsonViews' => 6, 'Layout' => 7, 'Enabled' => 8, 'DeletedAt' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, 'ArchivedAt' => 12, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'title' => 1, 'subtitle' => 2, 'description' => 3, 'icon' => 4, 'cover' => 5, 'jsonViews' => 6, 'layout' => 7, 'enabled' => 8, 'deletedAt' => 9, 'createdAt' => 10, 'updatedAt' => 11, 'archivedAt' => 12, ),
        self::TYPE_COLNAME       => array(TemplateArchiveTableMap::COL_ID => 0, TemplateArchiveTableMap::COL_TITLE => 1, TemplateArchiveTableMap::COL_SUBTITLE => 2, TemplateArchiveTableMap::COL_DESCRIPTION => 3, TemplateArchiveTableMap::COL_ICON => 4, TemplateArchiveTableMap::COL_COVER => 5, TemplateArchiveTableMap::COL_JSON_VIEWS => 6, TemplateArchiveTableMap::COL_LAYOUT => 7, TemplateArchiveTableMap::COL_ENABLED => 8, TemplateArchiveTableMap::COL_DELETED_AT => 9, TemplateArchiveTableMap::COL_CREATED_AT => 10, TemplateArchiveTableMap::COL_UPDATED_AT => 11, TemplateArchiveTableMap::COL_ARCHIVED_AT => 12, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'title' => 1, 'subtitle' => 2, 'description' => 3, 'icon' => 4, 'cover' => 5, 'json_views' => 6, 'layout' => 7, 'enabled' => 8, 'deleted_at' => 9, 'created_at' => 10, 'updated_at' => 11, 'archived_at' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('template_archive');
        $this->setPhpName('TemplateArchive');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\TemplateArchive');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 255, null);
        $this->addColumn('subtitle', 'Subtitle', 'VARCHAR', false, 255, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('icon', 'Icon', 'VARCHAR', false, 255, null);
        $this->addColumn('cover', 'Cover', 'VARCHAR', false, 255, null);
        $this->addColumn('json_views', 'JsonViews', 'LONGVARCHAR', false, null, null);
        $this->addColumn('layout', 'Layout', 'LONGVARCHAR', false, null, null);
        $this->addColumn('enabled', 'Enabled', 'BOOLEAN', false, 1, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? TemplateArchiveTableMap::CLASS_DEFAULT : TemplateArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (TemplateArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = TemplateArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = TemplateArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + TemplateArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = TemplateArchiveTableMap::OM_CLASS;
            /** @var TemplateArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            TemplateArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = TemplateArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = TemplateArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var TemplateArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                TemplateArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_ID);
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_TITLE);
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_SUBTITLE);
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_ICON);
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_COVER);
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_JSON_VIEWS);
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_LAYOUT);
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_ENABLED);
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(TemplateArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.subtitle');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.icon');
            $criteria->addSelectColumn($alias . '.cover');
            $criteria->addSelectColumn($alias . '.json_views');
            $criteria->addSelectColumn($alias . '.layout');
            $criteria->addSelectColumn($alias . '.enabled');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(TemplateArchiveTableMap::DATABASE_NAME)->getTable(TemplateArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(TemplateArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(TemplateArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new TemplateArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a TemplateArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or TemplateArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TemplateArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\TemplateArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(TemplateArchiveTableMap::DATABASE_NAME);
            $criteria->add(TemplateArchiveTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = TemplateArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            TemplateArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                TemplateArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the template_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return TemplateArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a TemplateArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or TemplateArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TemplateArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from TemplateArchive object
        }


        // Set the correct dbName
        $query = TemplateArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // TemplateArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
TemplateArchiveTableMap::buildTableMap();
