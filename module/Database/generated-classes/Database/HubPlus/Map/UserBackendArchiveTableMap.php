<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\UserBackendArchive;
use Database\HubPlus\UserBackendArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'user_backend_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UserBackendArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.UserBackendArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'user_backend_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\UserBackendArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.UserBackendArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 22;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 22;

    /**
     * the column name for the id field
     */
    const COL_ID = 'user_backend_archive.id';

    /**
     * the column name for the imagePath field
     */
    const COL_IMAGEPATH = 'user_backend_archive.imagePath';

    /**
     * the column name for the firstname field
     */
    const COL_FIRSTNAME = 'user_backend_archive.firstname';

    /**
     * the column name for the lastname field
     */
    const COL_LASTNAME = 'user_backend_archive.lastname';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'user_backend_archive.email';

    /**
     * the column name for the password field
     */
    const COL_PASSWORD = 'user_backend_archive.password';

    /**
     * the column name for the password_requested_at field
     */
    const COL_PASSWORD_REQUESTED_AT = 'user_backend_archive.password_requested_at';

    /**
     * the column name for the confirmation_token field
     */
    const COL_CONFIRMATION_TOKEN = 'user_backend_archive.confirmation_token';

    /**
     * the column name for the last_login field
     */
    const COL_LAST_LOGIN = 'user_backend_archive.last_login';

    /**
     * the column name for the locked field
     */
    const COL_LOCKED = 'user_backend_archive.locked';

    /**
     * the column name for the locked_at field
     */
    const COL_LOCKED_AT = 'user_backend_archive.locked_at';

    /**
     * the column name for the expired field
     */
    const COL_EXPIRED = 'user_backend_archive.expired';

    /**
     * the column name for the expired_at field
     */
    const COL_EXPIRED_AT = 'user_backend_archive.expired_at';

    /**
     * the column name for the role_id field
     */
    const COL_ROLE_ID = 'user_backend_archive.role_id';

    /**
     * the column name for the privacy field
     */
    const COL_PRIVACY = 'user_backend_archive.privacy';

    /**
     * the column name for the privacy_at field
     */
    const COL_PRIVACY_AT = 'user_backend_archive.privacy_at';

    /**
     * the column name for the terms field
     */
    const COL_TERMS = 'user_backend_archive.terms';

    /**
     * the column name for the terms_at field
     */
    const COL_TERMS_AT = 'user_backend_archive.terms_at';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'user_backend_archive.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'user_backend_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'user_backend_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'user_backend_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Imagepath', 'Firstname', 'Lastname', 'Email', 'Password', 'PasswordRequestedAt', 'ConfirmationToken', 'LastLogin', 'Locked', 'LockedAt', 'Expired', 'ExpiredAt', 'RoleId', 'Privacy', 'PrivacyAt', 'Terms', 'TermsAt', 'DeletedAt', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'imagepath', 'firstname', 'lastname', 'email', 'password', 'passwordRequestedAt', 'confirmationToken', 'lastLogin', 'locked', 'lockedAt', 'expired', 'expiredAt', 'roleId', 'privacy', 'privacyAt', 'terms', 'termsAt', 'deletedAt', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(UserBackendArchiveTableMap::COL_ID, UserBackendArchiveTableMap::COL_IMAGEPATH, UserBackendArchiveTableMap::COL_FIRSTNAME, UserBackendArchiveTableMap::COL_LASTNAME, UserBackendArchiveTableMap::COL_EMAIL, UserBackendArchiveTableMap::COL_PASSWORD, UserBackendArchiveTableMap::COL_PASSWORD_REQUESTED_AT, UserBackendArchiveTableMap::COL_CONFIRMATION_TOKEN, UserBackendArchiveTableMap::COL_LAST_LOGIN, UserBackendArchiveTableMap::COL_LOCKED, UserBackendArchiveTableMap::COL_LOCKED_AT, UserBackendArchiveTableMap::COL_EXPIRED, UserBackendArchiveTableMap::COL_EXPIRED_AT, UserBackendArchiveTableMap::COL_ROLE_ID, UserBackendArchiveTableMap::COL_PRIVACY, UserBackendArchiveTableMap::COL_PRIVACY_AT, UserBackendArchiveTableMap::COL_TERMS, UserBackendArchiveTableMap::COL_TERMS_AT, UserBackendArchiveTableMap::COL_DELETED_AT, UserBackendArchiveTableMap::COL_CREATED_AT, UserBackendArchiveTableMap::COL_UPDATED_AT, UserBackendArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'imagePath', 'firstname', 'lastname', 'email', 'password', 'password_requested_at', 'confirmation_token', 'last_login', 'locked', 'locked_at', 'expired', 'expired_at', 'role_id', 'privacy', 'privacy_at', 'terms', 'terms_at', 'deleted_at', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Imagepath' => 1, 'Firstname' => 2, 'Lastname' => 3, 'Email' => 4, 'Password' => 5, 'PasswordRequestedAt' => 6, 'ConfirmationToken' => 7, 'LastLogin' => 8, 'Locked' => 9, 'LockedAt' => 10, 'Expired' => 11, 'ExpiredAt' => 12, 'RoleId' => 13, 'Privacy' => 14, 'PrivacyAt' => 15, 'Terms' => 16, 'TermsAt' => 17, 'DeletedAt' => 18, 'CreatedAt' => 19, 'UpdatedAt' => 20, 'ArchivedAt' => 21, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'imagepath' => 1, 'firstname' => 2, 'lastname' => 3, 'email' => 4, 'password' => 5, 'passwordRequestedAt' => 6, 'confirmationToken' => 7, 'lastLogin' => 8, 'locked' => 9, 'lockedAt' => 10, 'expired' => 11, 'expiredAt' => 12, 'roleId' => 13, 'privacy' => 14, 'privacyAt' => 15, 'terms' => 16, 'termsAt' => 17, 'deletedAt' => 18, 'createdAt' => 19, 'updatedAt' => 20, 'archivedAt' => 21, ),
        self::TYPE_COLNAME       => array(UserBackendArchiveTableMap::COL_ID => 0, UserBackendArchiveTableMap::COL_IMAGEPATH => 1, UserBackendArchiveTableMap::COL_FIRSTNAME => 2, UserBackendArchiveTableMap::COL_LASTNAME => 3, UserBackendArchiveTableMap::COL_EMAIL => 4, UserBackendArchiveTableMap::COL_PASSWORD => 5, UserBackendArchiveTableMap::COL_PASSWORD_REQUESTED_AT => 6, UserBackendArchiveTableMap::COL_CONFIRMATION_TOKEN => 7, UserBackendArchiveTableMap::COL_LAST_LOGIN => 8, UserBackendArchiveTableMap::COL_LOCKED => 9, UserBackendArchiveTableMap::COL_LOCKED_AT => 10, UserBackendArchiveTableMap::COL_EXPIRED => 11, UserBackendArchiveTableMap::COL_EXPIRED_AT => 12, UserBackendArchiveTableMap::COL_ROLE_ID => 13, UserBackendArchiveTableMap::COL_PRIVACY => 14, UserBackendArchiveTableMap::COL_PRIVACY_AT => 15, UserBackendArchiveTableMap::COL_TERMS => 16, UserBackendArchiveTableMap::COL_TERMS_AT => 17, UserBackendArchiveTableMap::COL_DELETED_AT => 18, UserBackendArchiveTableMap::COL_CREATED_AT => 19, UserBackendArchiveTableMap::COL_UPDATED_AT => 20, UserBackendArchiveTableMap::COL_ARCHIVED_AT => 21, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'imagePath' => 1, 'firstname' => 2, 'lastname' => 3, 'email' => 4, 'password' => 5, 'password_requested_at' => 6, 'confirmation_token' => 7, 'last_login' => 8, 'locked' => 9, 'locked_at' => 10, 'expired' => 11, 'expired_at' => 12, 'role_id' => 13, 'privacy' => 14, 'privacy_at' => 15, 'terms' => 16, 'terms_at' => 17, 'deleted_at' => 18, 'created_at' => 19, 'updated_at' => 20, 'archived_at' => 21, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user_backend_archive');
        $this->setPhpName('UserBackendArchive');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\UserBackendArchive');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('imagePath', 'Imagepath', 'VARCHAR', false, 255, null);
        $this->addColumn('firstname', 'Firstname', 'VARCHAR', false, 255, null);
        $this->addColumn('lastname', 'Lastname', 'VARCHAR', false, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 255, null);
        $this->addColumn('password', 'Password', 'VARCHAR', false, 255, null);
        $this->addColumn('password_requested_at', 'PasswordRequestedAt', 'INTEGER', false, null, null);
        $this->addColumn('confirmation_token', 'ConfirmationToken', 'VARCHAR', false, 255, null);
        $this->addColumn('last_login', 'LastLogin', 'TIMESTAMP', false, null, null);
        $this->addColumn('locked', 'Locked', 'BOOLEAN', false, 1, false);
        $this->addColumn('locked_at', 'LockedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('expired', 'Expired', 'BOOLEAN', false, 1, false);
        $this->addColumn('expired_at', 'ExpiredAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('role_id', 'RoleId', 'INTEGER', false, null, null);
        $this->addColumn('privacy', 'Privacy', 'BOOLEAN', false, 1, null);
        $this->addColumn('privacy_at', 'PrivacyAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('terms', 'Terms', 'BOOLEAN', false, 1, null);
        $this->addColumn('terms_at', 'TermsAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UserBackendArchiveTableMap::CLASS_DEFAULT : UserBackendArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UserBackendArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UserBackendArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UserBackendArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UserBackendArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UserBackendArchiveTableMap::OM_CLASS;
            /** @var UserBackendArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UserBackendArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UserBackendArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UserBackendArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UserBackendArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UserBackendArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_ID);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_IMAGEPATH);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_FIRSTNAME);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_LASTNAME);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_EMAIL);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_PASSWORD);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_PASSWORD_REQUESTED_AT);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_CONFIRMATION_TOKEN);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_LAST_LOGIN);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_LOCKED);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_LOCKED_AT);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_EXPIRED);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_EXPIRED_AT);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_ROLE_ID);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_PRIVACY);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_PRIVACY_AT);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_TERMS);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_TERMS_AT);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(UserBackendArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.imagePath');
            $criteria->addSelectColumn($alias . '.firstname');
            $criteria->addSelectColumn($alias . '.lastname');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.password_requested_at');
            $criteria->addSelectColumn($alias . '.confirmation_token');
            $criteria->addSelectColumn($alias . '.last_login');
            $criteria->addSelectColumn($alias . '.locked');
            $criteria->addSelectColumn($alias . '.locked_at');
            $criteria->addSelectColumn($alias . '.expired');
            $criteria->addSelectColumn($alias . '.expired_at');
            $criteria->addSelectColumn($alias . '.role_id');
            $criteria->addSelectColumn($alias . '.privacy');
            $criteria->addSelectColumn($alias . '.privacy_at');
            $criteria->addSelectColumn($alias . '.terms');
            $criteria->addSelectColumn($alias . '.terms_at');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UserBackendArchiveTableMap::DATABASE_NAME)->getTable(UserBackendArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UserBackendArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UserBackendArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UserBackendArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UserBackendArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UserBackendArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserBackendArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\UserBackendArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UserBackendArchiveTableMap::DATABASE_NAME);
            $criteria->add(UserBackendArchiveTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = UserBackendArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UserBackendArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UserBackendArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the user_backend_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UserBackendArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UserBackendArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or UserBackendArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserBackendArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UserBackendArchive object
        }


        // Set the correct dbName
        $query = UserBackendArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UserBackendArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UserBackendArchiveTableMap::buildTableMap();
