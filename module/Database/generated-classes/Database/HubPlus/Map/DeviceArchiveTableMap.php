<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\DeviceArchive;
use Database\HubPlus\DeviceArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'device_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class DeviceArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.DeviceArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'device_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\DeviceArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.DeviceArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 17;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 17;

    /**
     * the column name for the id field
     */
    const COL_ID = 'device_archive.id';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'device_archive.user_id';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'device_archive.status';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'device_archive.type';

    /**
     * the column name for the os field
     */
    const COL_OS = 'device_archive.os';

    /**
     * the column name for the os_version field
     */
    const COL_OS_VERSION = 'device_archive.os_version';

    /**
     * the column name for the enable_notification field
     */
    const COL_ENABLE_NOTIFICATION = 'device_archive.enable_notification';

    /**
     * the column name for the api_version field
     */
    const COL_API_VERSION = 'device_archive.api_version';

    /**
     * the column name for the language field
     */
    const COL_LANGUAGE = 'device_archive.language';

    /**
     * the column name for the demo field
     */
    const COL_DEMO = 'device_archive.demo';

    /**
     * the column name for the token field
     */
    const COL_TOKEN = 'device_archive.token';

    /**
     * the column name for the api_key field
     */
    const COL_API_KEY = 'device_archive.api_key';

    /**
     * the column name for the ip_address field
     */
    const COL_IP_ADDRESS = 'device_archive.ip_address';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'device_archive.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'device_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'device_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'device_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'UserId', 'Status', 'Type', 'Os', 'OsVersion', 'EnableNotification', 'ApiVersion', 'Language', 'Demo', 'Token', 'ApiKey', 'IpAddress', 'DeletedAt', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'userId', 'status', 'type', 'os', 'osVersion', 'enableNotification', 'apiVersion', 'language', 'demo', 'token', 'apiKey', 'ipAddress', 'deletedAt', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(DeviceArchiveTableMap::COL_ID, DeviceArchiveTableMap::COL_USER_ID, DeviceArchiveTableMap::COL_STATUS, DeviceArchiveTableMap::COL_TYPE, DeviceArchiveTableMap::COL_OS, DeviceArchiveTableMap::COL_OS_VERSION, DeviceArchiveTableMap::COL_ENABLE_NOTIFICATION, DeviceArchiveTableMap::COL_API_VERSION, DeviceArchiveTableMap::COL_LANGUAGE, DeviceArchiveTableMap::COL_DEMO, DeviceArchiveTableMap::COL_TOKEN, DeviceArchiveTableMap::COL_API_KEY, DeviceArchiveTableMap::COL_IP_ADDRESS, DeviceArchiveTableMap::COL_DELETED_AT, DeviceArchiveTableMap::COL_CREATED_AT, DeviceArchiveTableMap::COL_UPDATED_AT, DeviceArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'user_id', 'status', 'type', 'os', 'os_version', 'enable_notification', 'api_version', 'language', 'demo', 'token', 'api_key', 'ip_address', 'deleted_at', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'UserId' => 1, 'Status' => 2, 'Type' => 3, 'Os' => 4, 'OsVersion' => 5, 'EnableNotification' => 6, 'ApiVersion' => 7, 'Language' => 8, 'Demo' => 9, 'Token' => 10, 'ApiKey' => 11, 'IpAddress' => 12, 'DeletedAt' => 13, 'CreatedAt' => 14, 'UpdatedAt' => 15, 'ArchivedAt' => 16, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'userId' => 1, 'status' => 2, 'type' => 3, 'os' => 4, 'osVersion' => 5, 'enableNotification' => 6, 'apiVersion' => 7, 'language' => 8, 'demo' => 9, 'token' => 10, 'apiKey' => 11, 'ipAddress' => 12, 'deletedAt' => 13, 'createdAt' => 14, 'updatedAt' => 15, 'archivedAt' => 16, ),
        self::TYPE_COLNAME       => array(DeviceArchiveTableMap::COL_ID => 0, DeviceArchiveTableMap::COL_USER_ID => 1, DeviceArchiveTableMap::COL_STATUS => 2, DeviceArchiveTableMap::COL_TYPE => 3, DeviceArchiveTableMap::COL_OS => 4, DeviceArchiveTableMap::COL_OS_VERSION => 5, DeviceArchiveTableMap::COL_ENABLE_NOTIFICATION => 6, DeviceArchiveTableMap::COL_API_VERSION => 7, DeviceArchiveTableMap::COL_LANGUAGE => 8, DeviceArchiveTableMap::COL_DEMO => 9, DeviceArchiveTableMap::COL_TOKEN => 10, DeviceArchiveTableMap::COL_API_KEY => 11, DeviceArchiveTableMap::COL_IP_ADDRESS => 12, DeviceArchiveTableMap::COL_DELETED_AT => 13, DeviceArchiveTableMap::COL_CREATED_AT => 14, DeviceArchiveTableMap::COL_UPDATED_AT => 15, DeviceArchiveTableMap::COL_ARCHIVED_AT => 16, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'user_id' => 1, 'status' => 2, 'type' => 3, 'os' => 4, 'os_version' => 5, 'enable_notification' => 6, 'api_version' => 7, 'language' => 8, 'demo' => 9, 'token' => 10, 'api_key' => 11, 'ip_address' => 12, 'deleted_at' => 13, 'created_at' => 14, 'updated_at' => 15, 'archived_at' => 16, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('device_archive');
        $this->setPhpName('DeviceArchive');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\DeviceArchive');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('user_id', 'UserId', 'INTEGER', false, null, null);
        $this->addColumn('status', 'Status', 'VARCHAR', true, 20, null);
        $this->addColumn('type', 'Type', 'VARCHAR', true, 20, null);
        $this->addColumn('os', 'Os', 'CLOB', true, null, null);
        $this->addColumn('os_version', 'OsVersion', 'CLOB', true, null, null);
        $this->addColumn('enable_notification', 'EnableNotification', 'BOOLEAN', false, 1, null);
        $this->addColumn('api_version', 'ApiVersion', 'VARCHAR', false, 50, null);
        $this->addColumn('language', 'Language', 'VARCHAR', false, 10, null);
        $this->addColumn('demo', 'Demo', 'BOOLEAN', false, 1, null);
        $this->addColumn('token', 'Token', 'CLOB', true, null, null);
        $this->addColumn('api_key', 'ApiKey', 'VARCHAR', false, 255, null);
        $this->addColumn('ip_address', 'IpAddress', 'VARCHAR', false, 50, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? DeviceArchiveTableMap::CLASS_DEFAULT : DeviceArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (DeviceArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = DeviceArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = DeviceArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + DeviceArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = DeviceArchiveTableMap::OM_CLASS;
            /** @var DeviceArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            DeviceArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = DeviceArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = DeviceArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var DeviceArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                DeviceArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_ID);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_USER_ID);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_STATUS);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_TYPE);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_OS);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_OS_VERSION);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_ENABLE_NOTIFICATION);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_API_VERSION);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_LANGUAGE);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_DEMO);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_TOKEN);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_API_KEY);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_IP_ADDRESS);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(DeviceArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.os');
            $criteria->addSelectColumn($alias . '.os_version');
            $criteria->addSelectColumn($alias . '.enable_notification');
            $criteria->addSelectColumn($alias . '.api_version');
            $criteria->addSelectColumn($alias . '.language');
            $criteria->addSelectColumn($alias . '.demo');
            $criteria->addSelectColumn($alias . '.token');
            $criteria->addSelectColumn($alias . '.api_key');
            $criteria->addSelectColumn($alias . '.ip_address');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(DeviceArchiveTableMap::DATABASE_NAME)->getTable(DeviceArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(DeviceArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(DeviceArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new DeviceArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a DeviceArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or DeviceArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\DeviceArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(DeviceArchiveTableMap::DATABASE_NAME);
            $criteria->add(DeviceArchiveTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = DeviceArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            DeviceArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                DeviceArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the device_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return DeviceArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a DeviceArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or DeviceArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from DeviceArchive object
        }


        // Set the correct dbName
        $query = DeviceArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // DeviceArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
DeviceArchiveTableMap::buildTableMap();
