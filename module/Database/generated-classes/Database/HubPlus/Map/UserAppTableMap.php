<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\UserApp;
use Database\HubPlus\UserAppQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'user_app' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UserAppTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.UserAppTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'user_app';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\UserApp';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.UserApp';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 29;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 29;

    /**
     * the column name for the id field
     */
    const COL_ID = 'user_app.id';

    /**
     * the column name for the username field
     */
    const COL_USERNAME = 'user_app.username';

    /**
     * the column name for the username_canonical field
     */
    const COL_USERNAME_CANONICAL = 'user_app.username_canonical';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'user_app.email';

    /**
     * the column name for the email_canonical field
     */
    const COL_EMAIL_CANONICAL = 'user_app.email_canonical';

    /**
     * the column name for the enabled field
     */
    const COL_ENABLED = 'user_app.enabled';

    /**
     * the column name for the privacy field
     */
    const COL_PRIVACY = 'user_app.privacy';

    /**
     * the column name for the terms field
     */
    const COL_TERMS = 'user_app.terms';

    /**
     * the column name for the salt field
     */
    const COL_SALT = 'user_app.salt';

    /**
     * the column name for the password field
     */
    const COL_PASSWORD = 'user_app.password';

    /**
     * the column name for the last_login field
     */
    const COL_LAST_LOGIN = 'user_app.last_login';

    /**
     * the column name for the locked field
     */
    const COL_LOCKED = 'user_app.locked';

    /**
     * the column name for the expired field
     */
    const COL_EXPIRED = 'user_app.expired';

    /**
     * the column name for the expires_at field
     */
    const COL_EXPIRES_AT = 'user_app.expires_at';

    /**
     * the column name for the confirmation_token field
     */
    const COL_CONFIRMATION_TOKEN = 'user_app.confirmation_token';

    /**
     * the column name for the password_requested_at field
     */
    const COL_PASSWORD_REQUESTED_AT = 'user_app.password_requested_at';

    /**
     * the column name for the roles field
     */
    const COL_ROLES = 'user_app.roles';

    /**
     * the column name for the credentials_expired field
     */
    const COL_CREDENTIALS_EXPIRED = 'user_app.credentials_expired';

    /**
     * the column name for the credentials_expire_at field
     */
    const COL_CREDENTIALS_EXPIRE_AT = 'user_app.credentials_expire_at';

    /**
     * the column name for the firstname field
     */
    const COL_FIRSTNAME = 'user_app.firstname';

    /**
     * the column name for the lastname field
     */
    const COL_LASTNAME = 'user_app.lastname';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'user_app.phone';

    /**
     * the column name for the imagePath field
     */
    const COL_IMAGEPATH = 'user_app.imagePath';

    /**
     * the column name for the address_id field
     */
    const COL_ADDRESS_ID = 'user_app.address_id';

    /**
     * the column name for the tags field
     */
    const COL_TAGS = 'user_app.tags';

    /**
     * the column name for the section_id field
     */
    const COL_SECTION_ID = 'user_app.section_id';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'user_app.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'user_app.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'user_app.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Username', 'UsernameCanonical', 'Email', 'EmailCanonical', 'Enabled', 'Privacy', 'Terms', 'Salt', 'Password', 'LastLogin', 'Locked', 'Expired', 'ExpiresAt', 'ConfirmationToken', 'PasswordRequestedAt', 'Roles', 'CredentialsExpired', 'CredentialsExpireAt', 'Firstname', 'Lastname', 'Phone', 'Imagepath', 'AddressId', 'Tags', 'SectionId', 'DeletedAt', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'username', 'usernameCanonical', 'email', 'emailCanonical', 'enabled', 'privacy', 'terms', 'salt', 'password', 'lastLogin', 'locked', 'expired', 'expiresAt', 'confirmationToken', 'passwordRequestedAt', 'roles', 'credentialsExpired', 'credentialsExpireAt', 'firstname', 'lastname', 'phone', 'imagepath', 'addressId', 'tags', 'sectionId', 'deletedAt', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(UserAppTableMap::COL_ID, UserAppTableMap::COL_USERNAME, UserAppTableMap::COL_USERNAME_CANONICAL, UserAppTableMap::COL_EMAIL, UserAppTableMap::COL_EMAIL_CANONICAL, UserAppTableMap::COL_ENABLED, UserAppTableMap::COL_PRIVACY, UserAppTableMap::COL_TERMS, UserAppTableMap::COL_SALT, UserAppTableMap::COL_PASSWORD, UserAppTableMap::COL_LAST_LOGIN, UserAppTableMap::COL_LOCKED, UserAppTableMap::COL_EXPIRED, UserAppTableMap::COL_EXPIRES_AT, UserAppTableMap::COL_CONFIRMATION_TOKEN, UserAppTableMap::COL_PASSWORD_REQUESTED_AT, UserAppTableMap::COL_ROLES, UserAppTableMap::COL_CREDENTIALS_EXPIRED, UserAppTableMap::COL_CREDENTIALS_EXPIRE_AT, UserAppTableMap::COL_FIRSTNAME, UserAppTableMap::COL_LASTNAME, UserAppTableMap::COL_PHONE, UserAppTableMap::COL_IMAGEPATH, UserAppTableMap::COL_ADDRESS_ID, UserAppTableMap::COL_TAGS, UserAppTableMap::COL_SECTION_ID, UserAppTableMap::COL_DELETED_AT, UserAppTableMap::COL_CREATED_AT, UserAppTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'username', 'username_canonical', 'email', 'email_canonical', 'enabled', 'privacy', 'terms', 'salt', 'password', 'last_login', 'locked', 'expired', 'expires_at', 'confirmation_token', 'password_requested_at', 'roles', 'credentials_expired', 'credentials_expire_at', 'firstname', 'lastname', 'phone', 'imagePath', 'address_id', 'tags', 'section_id', 'deleted_at', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Username' => 1, 'UsernameCanonical' => 2, 'Email' => 3, 'EmailCanonical' => 4, 'Enabled' => 5, 'Privacy' => 6, 'Terms' => 7, 'Salt' => 8, 'Password' => 9, 'LastLogin' => 10, 'Locked' => 11, 'Expired' => 12, 'ExpiresAt' => 13, 'ConfirmationToken' => 14, 'PasswordRequestedAt' => 15, 'Roles' => 16, 'CredentialsExpired' => 17, 'CredentialsExpireAt' => 18, 'Firstname' => 19, 'Lastname' => 20, 'Phone' => 21, 'Imagepath' => 22, 'AddressId' => 23, 'Tags' => 24, 'SectionId' => 25, 'DeletedAt' => 26, 'CreatedAt' => 27, 'UpdatedAt' => 28, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'username' => 1, 'usernameCanonical' => 2, 'email' => 3, 'emailCanonical' => 4, 'enabled' => 5, 'privacy' => 6, 'terms' => 7, 'salt' => 8, 'password' => 9, 'lastLogin' => 10, 'locked' => 11, 'expired' => 12, 'expiresAt' => 13, 'confirmationToken' => 14, 'passwordRequestedAt' => 15, 'roles' => 16, 'credentialsExpired' => 17, 'credentialsExpireAt' => 18, 'firstname' => 19, 'lastname' => 20, 'phone' => 21, 'imagepath' => 22, 'addressId' => 23, 'tags' => 24, 'sectionId' => 25, 'deletedAt' => 26, 'createdAt' => 27, 'updatedAt' => 28, ),
        self::TYPE_COLNAME       => array(UserAppTableMap::COL_ID => 0, UserAppTableMap::COL_USERNAME => 1, UserAppTableMap::COL_USERNAME_CANONICAL => 2, UserAppTableMap::COL_EMAIL => 3, UserAppTableMap::COL_EMAIL_CANONICAL => 4, UserAppTableMap::COL_ENABLED => 5, UserAppTableMap::COL_PRIVACY => 6, UserAppTableMap::COL_TERMS => 7, UserAppTableMap::COL_SALT => 8, UserAppTableMap::COL_PASSWORD => 9, UserAppTableMap::COL_LAST_LOGIN => 10, UserAppTableMap::COL_LOCKED => 11, UserAppTableMap::COL_EXPIRED => 12, UserAppTableMap::COL_EXPIRES_AT => 13, UserAppTableMap::COL_CONFIRMATION_TOKEN => 14, UserAppTableMap::COL_PASSWORD_REQUESTED_AT => 15, UserAppTableMap::COL_ROLES => 16, UserAppTableMap::COL_CREDENTIALS_EXPIRED => 17, UserAppTableMap::COL_CREDENTIALS_EXPIRE_AT => 18, UserAppTableMap::COL_FIRSTNAME => 19, UserAppTableMap::COL_LASTNAME => 20, UserAppTableMap::COL_PHONE => 21, UserAppTableMap::COL_IMAGEPATH => 22, UserAppTableMap::COL_ADDRESS_ID => 23, UserAppTableMap::COL_TAGS => 24, UserAppTableMap::COL_SECTION_ID => 25, UserAppTableMap::COL_DELETED_AT => 26, UserAppTableMap::COL_CREATED_AT => 27, UserAppTableMap::COL_UPDATED_AT => 28, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'username' => 1, 'username_canonical' => 2, 'email' => 3, 'email_canonical' => 4, 'enabled' => 5, 'privacy' => 6, 'terms' => 7, 'salt' => 8, 'password' => 9, 'last_login' => 10, 'locked' => 11, 'expired' => 12, 'expires_at' => 13, 'confirmation_token' => 14, 'password_requested_at' => 15, 'roles' => 16, 'credentials_expired' => 17, 'credentials_expire_at' => 18, 'firstname' => 19, 'lastname' => 20, 'phone' => 21, 'imagePath' => 22, 'address_id' => 23, 'tags' => 24, 'section_id' => 25, 'deleted_at' => 26, 'created_at' => 27, 'updated_at' => 28, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user_app');
        $this->setPhpName('UserApp');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\UserApp');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 255, null);
        $this->addColumn('username_canonical', 'UsernameCanonical', 'VARCHAR', true, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 255, null);
        $this->addColumn('email_canonical', 'EmailCanonical', 'VARCHAR', true, 255, null);
        $this->addColumn('enabled', 'Enabled', 'BOOLEAN', true, 1, null);
        $this->addColumn('privacy', 'Privacy', 'BOOLEAN', false, 1, null);
        $this->addColumn('terms', 'Terms', 'BOOLEAN', false, 1, null);
        $this->addColumn('salt', 'Salt', 'VARCHAR', true, 255, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 255, null);
        $this->addColumn('last_login', 'LastLogin', 'TIMESTAMP', false, null, null);
        $this->addColumn('locked', 'Locked', 'BOOLEAN', true, 1, null);
        $this->addColumn('expired', 'Expired', 'BOOLEAN', true, 1, null);
        $this->addColumn('expires_at', 'ExpiresAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('confirmation_token', 'ConfirmationToken', 'VARCHAR', false, 255, null);
        $this->addColumn('password_requested_at', 'PasswordRequestedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('roles', 'Roles', 'CLOB', true, null, null);
        $this->addColumn('credentials_expired', 'CredentialsExpired', 'BOOLEAN', true, 1, null);
        $this->addColumn('credentials_expire_at', 'CredentialsExpireAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('firstname', 'Firstname', 'VARCHAR', false, 255, null);
        $this->addColumn('lastname', 'Lastname', 'VARCHAR', false, 255, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', false, 255, null);
        $this->addColumn('imagePath', 'Imagepath', 'VARCHAR', false, 255, null);
        $this->addForeignKey('address_id', 'AddressId', 'INTEGER', 'address', 'id', false, null, null);
        $this->addColumn('tags', 'Tags', 'CLOB', false, null, null);
        $this->addForeignKey('section_id', 'SectionId', 'INTEGER', 'section', 'id', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Address', '\\Database\\HubPlus\\Address', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':address_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Section', '\\Database\\HubPlus\\Section', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':section_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('CustomFormLog', '\\Database\\HubPlus\\CustomFormLog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, 'CustomFormLogs', false);
        $this->addRelation('Device', '\\Database\\HubPlus\\Device', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, 'Devices', false);
        $this->addRelation('MediaLog', '\\Database\\HubPlus\\MediaLog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, 'MediaLogs', false);
        $this->addRelation('PostLog', '\\Database\\HubPlus\\PostLog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, 'PostLogs', false);
        $this->addRelation('RemoteLog', '\\Database\\HubPlus\\RemoteLog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, 'RemoteLogs', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UserAppTableMap::CLASS_DEFAULT : UserAppTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UserApp object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UserAppTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UserAppTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UserAppTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UserAppTableMap::OM_CLASS;
            /** @var UserApp $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UserAppTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UserAppTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UserAppTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UserApp $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UserAppTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UserAppTableMap::COL_ID);
            $criteria->addSelectColumn(UserAppTableMap::COL_USERNAME);
            $criteria->addSelectColumn(UserAppTableMap::COL_USERNAME_CANONICAL);
            $criteria->addSelectColumn(UserAppTableMap::COL_EMAIL);
            $criteria->addSelectColumn(UserAppTableMap::COL_EMAIL_CANONICAL);
            $criteria->addSelectColumn(UserAppTableMap::COL_ENABLED);
            $criteria->addSelectColumn(UserAppTableMap::COL_PRIVACY);
            $criteria->addSelectColumn(UserAppTableMap::COL_TERMS);
            $criteria->addSelectColumn(UserAppTableMap::COL_SALT);
            $criteria->addSelectColumn(UserAppTableMap::COL_PASSWORD);
            $criteria->addSelectColumn(UserAppTableMap::COL_LAST_LOGIN);
            $criteria->addSelectColumn(UserAppTableMap::COL_LOCKED);
            $criteria->addSelectColumn(UserAppTableMap::COL_EXPIRED);
            $criteria->addSelectColumn(UserAppTableMap::COL_EXPIRES_AT);
            $criteria->addSelectColumn(UserAppTableMap::COL_CONFIRMATION_TOKEN);
            $criteria->addSelectColumn(UserAppTableMap::COL_PASSWORD_REQUESTED_AT);
            $criteria->addSelectColumn(UserAppTableMap::COL_ROLES);
            $criteria->addSelectColumn(UserAppTableMap::COL_CREDENTIALS_EXPIRED);
            $criteria->addSelectColumn(UserAppTableMap::COL_CREDENTIALS_EXPIRE_AT);
            $criteria->addSelectColumn(UserAppTableMap::COL_FIRSTNAME);
            $criteria->addSelectColumn(UserAppTableMap::COL_LASTNAME);
            $criteria->addSelectColumn(UserAppTableMap::COL_PHONE);
            $criteria->addSelectColumn(UserAppTableMap::COL_IMAGEPATH);
            $criteria->addSelectColumn(UserAppTableMap::COL_ADDRESS_ID);
            $criteria->addSelectColumn(UserAppTableMap::COL_TAGS);
            $criteria->addSelectColumn(UserAppTableMap::COL_SECTION_ID);
            $criteria->addSelectColumn(UserAppTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(UserAppTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(UserAppTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.username');
            $criteria->addSelectColumn($alias . '.username_canonical');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.email_canonical');
            $criteria->addSelectColumn($alias . '.enabled');
            $criteria->addSelectColumn($alias . '.privacy');
            $criteria->addSelectColumn($alias . '.terms');
            $criteria->addSelectColumn($alias . '.salt');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.last_login');
            $criteria->addSelectColumn($alias . '.locked');
            $criteria->addSelectColumn($alias . '.expired');
            $criteria->addSelectColumn($alias . '.expires_at');
            $criteria->addSelectColumn($alias . '.confirmation_token');
            $criteria->addSelectColumn($alias . '.password_requested_at');
            $criteria->addSelectColumn($alias . '.roles');
            $criteria->addSelectColumn($alias . '.credentials_expired');
            $criteria->addSelectColumn($alias . '.credentials_expire_at');
            $criteria->addSelectColumn($alias . '.firstname');
            $criteria->addSelectColumn($alias . '.lastname');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.imagePath');
            $criteria->addSelectColumn($alias . '.address_id');
            $criteria->addSelectColumn($alias . '.tags');
            $criteria->addSelectColumn($alias . '.section_id');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UserAppTableMap::DATABASE_NAME)->getTable(UserAppTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UserAppTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UserAppTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UserAppTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UserApp or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UserApp object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAppTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\UserApp) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UserAppTableMap::DATABASE_NAME);
            $criteria->add(UserAppTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = UserAppQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UserAppTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UserAppTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the user_app table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UserAppQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UserApp or Criteria object.
     *
     * @param mixed               $criteria Criteria or UserApp object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAppTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UserApp object
        }

        if ($criteria->containsKey(UserAppTableMap::COL_ID) && $criteria->keyContainsValue(UserAppTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UserAppTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = UserAppQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UserAppTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UserAppTableMap::buildTableMap();
