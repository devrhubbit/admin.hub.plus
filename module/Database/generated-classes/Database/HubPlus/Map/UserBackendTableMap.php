<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\UserBackend;
use Database\HubPlus\UserBackendQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'user_backend' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UserBackendTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.UserBackendTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'user_backend';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\UserBackend';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.UserBackend';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 21;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 21;

    /**
     * the column name for the id field
     */
    const COL_ID = 'user_backend.id';

    /**
     * the column name for the imagePath field
     */
    const COL_IMAGEPATH = 'user_backend.imagePath';

    /**
     * the column name for the firstname field
     */
    const COL_FIRSTNAME = 'user_backend.firstname';

    /**
     * the column name for the lastname field
     */
    const COL_LASTNAME = 'user_backend.lastname';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'user_backend.email';

    /**
     * the column name for the password field
     */
    const COL_PASSWORD = 'user_backend.password';

    /**
     * the column name for the password_requested_at field
     */
    const COL_PASSWORD_REQUESTED_AT = 'user_backend.password_requested_at';

    /**
     * the column name for the confirmation_token field
     */
    const COL_CONFIRMATION_TOKEN = 'user_backend.confirmation_token';

    /**
     * the column name for the last_login field
     */
    const COL_LAST_LOGIN = 'user_backend.last_login';

    /**
     * the column name for the locked field
     */
    const COL_LOCKED = 'user_backend.locked';

    /**
     * the column name for the locked_at field
     */
    const COL_LOCKED_AT = 'user_backend.locked_at';

    /**
     * the column name for the expired field
     */
    const COL_EXPIRED = 'user_backend.expired';

    /**
     * the column name for the expired_at field
     */
    const COL_EXPIRED_AT = 'user_backend.expired_at';

    /**
     * the column name for the role_id field
     */
    const COL_ROLE_ID = 'user_backend.role_id';

    /**
     * the column name for the privacy field
     */
    const COL_PRIVACY = 'user_backend.privacy';

    /**
     * the column name for the privacy_at field
     */
    const COL_PRIVACY_AT = 'user_backend.privacy_at';

    /**
     * the column name for the terms field
     */
    const COL_TERMS = 'user_backend.terms';

    /**
     * the column name for the terms_at field
     */
    const COL_TERMS_AT = 'user_backend.terms_at';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'user_backend.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'user_backend.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'user_backend.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Imagepath', 'Firstname', 'Lastname', 'Email', 'Password', 'PasswordRequestedAt', 'ConfirmationToken', 'LastLogin', 'Locked', 'LockedAt', 'Expired', 'ExpiredAt', 'RoleId', 'Privacy', 'PrivacyAt', 'Terms', 'TermsAt', 'DeletedAt', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'imagepath', 'firstname', 'lastname', 'email', 'password', 'passwordRequestedAt', 'confirmationToken', 'lastLogin', 'locked', 'lockedAt', 'expired', 'expiredAt', 'roleId', 'privacy', 'privacyAt', 'terms', 'termsAt', 'deletedAt', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(UserBackendTableMap::COL_ID, UserBackendTableMap::COL_IMAGEPATH, UserBackendTableMap::COL_FIRSTNAME, UserBackendTableMap::COL_LASTNAME, UserBackendTableMap::COL_EMAIL, UserBackendTableMap::COL_PASSWORD, UserBackendTableMap::COL_PASSWORD_REQUESTED_AT, UserBackendTableMap::COL_CONFIRMATION_TOKEN, UserBackendTableMap::COL_LAST_LOGIN, UserBackendTableMap::COL_LOCKED, UserBackendTableMap::COL_LOCKED_AT, UserBackendTableMap::COL_EXPIRED, UserBackendTableMap::COL_EXPIRED_AT, UserBackendTableMap::COL_ROLE_ID, UserBackendTableMap::COL_PRIVACY, UserBackendTableMap::COL_PRIVACY_AT, UserBackendTableMap::COL_TERMS, UserBackendTableMap::COL_TERMS_AT, UserBackendTableMap::COL_DELETED_AT, UserBackendTableMap::COL_CREATED_AT, UserBackendTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'imagePath', 'firstname', 'lastname', 'email', 'password', 'password_requested_at', 'confirmation_token', 'last_login', 'locked', 'locked_at', 'expired', 'expired_at', 'role_id', 'privacy', 'privacy_at', 'terms', 'terms_at', 'deleted_at', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Imagepath' => 1, 'Firstname' => 2, 'Lastname' => 3, 'Email' => 4, 'Password' => 5, 'PasswordRequestedAt' => 6, 'ConfirmationToken' => 7, 'LastLogin' => 8, 'Locked' => 9, 'LockedAt' => 10, 'Expired' => 11, 'ExpiredAt' => 12, 'RoleId' => 13, 'Privacy' => 14, 'PrivacyAt' => 15, 'Terms' => 16, 'TermsAt' => 17, 'DeletedAt' => 18, 'CreatedAt' => 19, 'UpdatedAt' => 20, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'imagepath' => 1, 'firstname' => 2, 'lastname' => 3, 'email' => 4, 'password' => 5, 'passwordRequestedAt' => 6, 'confirmationToken' => 7, 'lastLogin' => 8, 'locked' => 9, 'lockedAt' => 10, 'expired' => 11, 'expiredAt' => 12, 'roleId' => 13, 'privacy' => 14, 'privacyAt' => 15, 'terms' => 16, 'termsAt' => 17, 'deletedAt' => 18, 'createdAt' => 19, 'updatedAt' => 20, ),
        self::TYPE_COLNAME       => array(UserBackendTableMap::COL_ID => 0, UserBackendTableMap::COL_IMAGEPATH => 1, UserBackendTableMap::COL_FIRSTNAME => 2, UserBackendTableMap::COL_LASTNAME => 3, UserBackendTableMap::COL_EMAIL => 4, UserBackendTableMap::COL_PASSWORD => 5, UserBackendTableMap::COL_PASSWORD_REQUESTED_AT => 6, UserBackendTableMap::COL_CONFIRMATION_TOKEN => 7, UserBackendTableMap::COL_LAST_LOGIN => 8, UserBackendTableMap::COL_LOCKED => 9, UserBackendTableMap::COL_LOCKED_AT => 10, UserBackendTableMap::COL_EXPIRED => 11, UserBackendTableMap::COL_EXPIRED_AT => 12, UserBackendTableMap::COL_ROLE_ID => 13, UserBackendTableMap::COL_PRIVACY => 14, UserBackendTableMap::COL_PRIVACY_AT => 15, UserBackendTableMap::COL_TERMS => 16, UserBackendTableMap::COL_TERMS_AT => 17, UserBackendTableMap::COL_DELETED_AT => 18, UserBackendTableMap::COL_CREATED_AT => 19, UserBackendTableMap::COL_UPDATED_AT => 20, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'imagePath' => 1, 'firstname' => 2, 'lastname' => 3, 'email' => 4, 'password' => 5, 'password_requested_at' => 6, 'confirmation_token' => 7, 'last_login' => 8, 'locked' => 9, 'locked_at' => 10, 'expired' => 11, 'expired_at' => 12, 'role_id' => 13, 'privacy' => 14, 'privacy_at' => 15, 'terms' => 16, 'terms_at' => 17, 'deleted_at' => 18, 'created_at' => 19, 'updated_at' => 20, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user_backend');
        $this->setPhpName('UserBackend');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\UserBackend');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('imagePath', 'Imagepath', 'VARCHAR', false, 255, null);
        $this->addColumn('firstname', 'Firstname', 'VARCHAR', false, 255, null);
        $this->addColumn('lastname', 'Lastname', 'VARCHAR', false, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 255, null);
        $this->addColumn('password', 'Password', 'VARCHAR', false, 255, null);
        $this->addColumn('password_requested_at', 'PasswordRequestedAt', 'INTEGER', false, null, null);
        $this->addColumn('confirmation_token', 'ConfirmationToken', 'VARCHAR', false, 255, null);
        $this->addColumn('last_login', 'LastLogin', 'TIMESTAMP', false, null, null);
        $this->addColumn('locked', 'Locked', 'BOOLEAN', false, 1, false);
        $this->addColumn('locked_at', 'LockedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('expired', 'Expired', 'BOOLEAN', false, 1, false);
        $this->addColumn('expired_at', 'ExpiredAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('role_id', 'RoleId', 'INTEGER', false, null, null);
        $this->addColumn('privacy', 'Privacy', 'BOOLEAN', false, 1, null);
        $this->addColumn('privacy_at', 'PrivacyAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('terms', 'Terms', 'BOOLEAN', false, 1, null);
        $this->addColumn('terms_at', 'TermsAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Post', '\\Database\\HubPlus\\Post', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':author_id',
    1 => ':id',
  ),
), null, null, 'Posts', false);
        $this->addRelation('Section', '\\Database\\HubPlus\\Section', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':author_id',
    1 => ':id',
  ),
), null, null, 'Sections', false);
        $this->addRelation('UserApplication', '\\Database\\HubPlus\\UserApplication', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'UserApplications', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to user_backend     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        UserApplicationTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UserBackendTableMap::CLASS_DEFAULT : UserBackendTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UserBackend object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UserBackendTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UserBackendTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UserBackendTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UserBackendTableMap::OM_CLASS;
            /** @var UserBackend $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UserBackendTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UserBackendTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UserBackendTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UserBackend $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UserBackendTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UserBackendTableMap::COL_ID);
            $criteria->addSelectColumn(UserBackendTableMap::COL_IMAGEPATH);
            $criteria->addSelectColumn(UserBackendTableMap::COL_FIRSTNAME);
            $criteria->addSelectColumn(UserBackendTableMap::COL_LASTNAME);
            $criteria->addSelectColumn(UserBackendTableMap::COL_EMAIL);
            $criteria->addSelectColumn(UserBackendTableMap::COL_PASSWORD);
            $criteria->addSelectColumn(UserBackendTableMap::COL_PASSWORD_REQUESTED_AT);
            $criteria->addSelectColumn(UserBackendTableMap::COL_CONFIRMATION_TOKEN);
            $criteria->addSelectColumn(UserBackendTableMap::COL_LAST_LOGIN);
            $criteria->addSelectColumn(UserBackendTableMap::COL_LOCKED);
            $criteria->addSelectColumn(UserBackendTableMap::COL_LOCKED_AT);
            $criteria->addSelectColumn(UserBackendTableMap::COL_EXPIRED);
            $criteria->addSelectColumn(UserBackendTableMap::COL_EXPIRED_AT);
            $criteria->addSelectColumn(UserBackendTableMap::COL_ROLE_ID);
            $criteria->addSelectColumn(UserBackendTableMap::COL_PRIVACY);
            $criteria->addSelectColumn(UserBackendTableMap::COL_PRIVACY_AT);
            $criteria->addSelectColumn(UserBackendTableMap::COL_TERMS);
            $criteria->addSelectColumn(UserBackendTableMap::COL_TERMS_AT);
            $criteria->addSelectColumn(UserBackendTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(UserBackendTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(UserBackendTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.imagePath');
            $criteria->addSelectColumn($alias . '.firstname');
            $criteria->addSelectColumn($alias . '.lastname');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.password_requested_at');
            $criteria->addSelectColumn($alias . '.confirmation_token');
            $criteria->addSelectColumn($alias . '.last_login');
            $criteria->addSelectColumn($alias . '.locked');
            $criteria->addSelectColumn($alias . '.locked_at');
            $criteria->addSelectColumn($alias . '.expired');
            $criteria->addSelectColumn($alias . '.expired_at');
            $criteria->addSelectColumn($alias . '.role_id');
            $criteria->addSelectColumn($alias . '.privacy');
            $criteria->addSelectColumn($alias . '.privacy_at');
            $criteria->addSelectColumn($alias . '.terms');
            $criteria->addSelectColumn($alias . '.terms_at');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UserBackendTableMap::DATABASE_NAME)->getTable(UserBackendTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UserBackendTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UserBackendTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UserBackendTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UserBackend or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UserBackend object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserBackendTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\UserBackend) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UserBackendTableMap::DATABASE_NAME);
            $criteria->add(UserBackendTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = UserBackendQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UserBackendTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UserBackendTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the user_backend table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UserBackendQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UserBackend or Criteria object.
     *
     * @param mixed               $criteria Criteria or UserBackend object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserBackendTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UserBackend object
        }

        if ($criteria->containsKey(UserBackendTableMap::COL_ID) && $criteria->keyContainsValue(UserBackendTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UserBackendTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = UserBackendQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UserBackendTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UserBackendTableMap::buildTableMap();
