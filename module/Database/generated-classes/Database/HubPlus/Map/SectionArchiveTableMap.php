<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\SectionArchive;
use Database\HubPlus\SectionArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'section_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SectionArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.SectionArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'section_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\SectionArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.SectionArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 22;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 22;

    /**
     * the column name for the id field
     */
    const COL_ID = 'section_archive.id';

    /**
     * the column name for the area field
     */
    const COL_AREA = 'section_archive.area';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'section_archive.type';

    /**
     * the column name for the author_id field
     */
    const COL_AUTHOR_ID = 'section_archive.author_id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'section_archive.title';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'section_archive.description';

    /**
     * the column name for the slug field
     */
    const COL_SLUG = 'section_archive.slug';

    /**
     * the column name for the icon_id field
     */
    const COL_ICON_ID = 'section_archive.icon_id';

    /**
     * the column name for the icon_selected_id field
     */
    const COL_ICON_SELECTED_ID = 'section_archive.icon_selected_id';

    /**
     * the column name for the resource field
     */
    const COL_RESOURCE = 'section_archive.resource';

    /**
     * the column name for the params field
     */
    const COL_PARAMS = 'section_archive.params';

    /**
     * the column name for the template_id field
     */
    const COL_TEMPLATE_ID = 'section_archive.template_id';

    /**
     * the column name for the layout field
     */
    const COL_LAYOUT = 'section_archive.layout';

    /**
     * the column name for the tags field
     */
    const COL_TAGS = 'section_archive.tags';

    /**
     * the column name for the tags_user field
     */
    const COL_TAGS_USER = 'section_archive.tags_user';

    /**
     * the column name for the parsed_tags field
     */
    const COL_PARSED_TAGS = 'section_archive.parsed_tags';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'section_archive.status';

    /**
     * the column name for the weight field
     */
    const COL_WEIGHT = 'section_archive.weight';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'section_archive.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'section_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'section_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'section_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Area', 'Type', 'AuthorId', 'Title', 'Description', 'Slug', 'IconId', 'IconSelectedId', 'Resource', 'Params', 'TemplateId', 'Layout', 'Tags', 'TagsUser', 'ParsedTags', 'Status', 'Weight', 'DeletedAt', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'area', 'type', 'authorId', 'title', 'description', 'slug', 'iconId', 'iconSelectedId', 'resource', 'params', 'templateId', 'layout', 'tags', 'tagsUser', 'parsedTags', 'status', 'weight', 'deletedAt', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(SectionArchiveTableMap::COL_ID, SectionArchiveTableMap::COL_AREA, SectionArchiveTableMap::COL_TYPE, SectionArchiveTableMap::COL_AUTHOR_ID, SectionArchiveTableMap::COL_TITLE, SectionArchiveTableMap::COL_DESCRIPTION, SectionArchiveTableMap::COL_SLUG, SectionArchiveTableMap::COL_ICON_ID, SectionArchiveTableMap::COL_ICON_SELECTED_ID, SectionArchiveTableMap::COL_RESOURCE, SectionArchiveTableMap::COL_PARAMS, SectionArchiveTableMap::COL_TEMPLATE_ID, SectionArchiveTableMap::COL_LAYOUT, SectionArchiveTableMap::COL_TAGS, SectionArchiveTableMap::COL_TAGS_USER, SectionArchiveTableMap::COL_PARSED_TAGS, SectionArchiveTableMap::COL_STATUS, SectionArchiveTableMap::COL_WEIGHT, SectionArchiveTableMap::COL_DELETED_AT, SectionArchiveTableMap::COL_CREATED_AT, SectionArchiveTableMap::COL_UPDATED_AT, SectionArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'area', 'type', 'author_id', 'title', 'description', 'slug', 'icon_id', 'icon_selected_id', 'resource', 'params', 'template_id', 'layout', 'tags', 'tags_user', 'parsed_tags', 'status', 'weight', 'deleted_at', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Area' => 1, 'Type' => 2, 'AuthorId' => 3, 'Title' => 4, 'Description' => 5, 'Slug' => 6, 'IconId' => 7, 'IconSelectedId' => 8, 'Resource' => 9, 'Params' => 10, 'TemplateId' => 11, 'Layout' => 12, 'Tags' => 13, 'TagsUser' => 14, 'ParsedTags' => 15, 'Status' => 16, 'Weight' => 17, 'DeletedAt' => 18, 'CreatedAt' => 19, 'UpdatedAt' => 20, 'ArchivedAt' => 21, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'area' => 1, 'type' => 2, 'authorId' => 3, 'title' => 4, 'description' => 5, 'slug' => 6, 'iconId' => 7, 'iconSelectedId' => 8, 'resource' => 9, 'params' => 10, 'templateId' => 11, 'layout' => 12, 'tags' => 13, 'tagsUser' => 14, 'parsedTags' => 15, 'status' => 16, 'weight' => 17, 'deletedAt' => 18, 'createdAt' => 19, 'updatedAt' => 20, 'archivedAt' => 21, ),
        self::TYPE_COLNAME       => array(SectionArchiveTableMap::COL_ID => 0, SectionArchiveTableMap::COL_AREA => 1, SectionArchiveTableMap::COL_TYPE => 2, SectionArchiveTableMap::COL_AUTHOR_ID => 3, SectionArchiveTableMap::COL_TITLE => 4, SectionArchiveTableMap::COL_DESCRIPTION => 5, SectionArchiveTableMap::COL_SLUG => 6, SectionArchiveTableMap::COL_ICON_ID => 7, SectionArchiveTableMap::COL_ICON_SELECTED_ID => 8, SectionArchiveTableMap::COL_RESOURCE => 9, SectionArchiveTableMap::COL_PARAMS => 10, SectionArchiveTableMap::COL_TEMPLATE_ID => 11, SectionArchiveTableMap::COL_LAYOUT => 12, SectionArchiveTableMap::COL_TAGS => 13, SectionArchiveTableMap::COL_TAGS_USER => 14, SectionArchiveTableMap::COL_PARSED_TAGS => 15, SectionArchiveTableMap::COL_STATUS => 16, SectionArchiveTableMap::COL_WEIGHT => 17, SectionArchiveTableMap::COL_DELETED_AT => 18, SectionArchiveTableMap::COL_CREATED_AT => 19, SectionArchiveTableMap::COL_UPDATED_AT => 20, SectionArchiveTableMap::COL_ARCHIVED_AT => 21, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'area' => 1, 'type' => 2, 'author_id' => 3, 'title' => 4, 'description' => 5, 'slug' => 6, 'icon_id' => 7, 'icon_selected_id' => 8, 'resource' => 9, 'params' => 10, 'template_id' => 11, 'layout' => 12, 'tags' => 13, 'tags_user' => 14, 'parsed_tags' => 15, 'status' => 16, 'weight' => 17, 'deleted_at' => 18, 'created_at' => 19, 'updated_at' => 20, 'archived_at' => 21, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('section_archive');
        $this->setPhpName('SectionArchive');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\SectionArchive');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('area', 'Area', 'VARCHAR', false, 50, null);
        $this->addColumn('type', 'Type', 'VARCHAR', false, 50, null);
        $this->addColumn('author_id', 'AuthorId', 'INTEGER', false, null, null);
        $this->addColumn('title', 'Title', 'LONGVARCHAR', false, null, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('slug', 'Slug', 'VARCHAR', false, 255, null);
        $this->addColumn('icon_id', 'IconId', 'INTEGER', false, null, null);
        $this->addColumn('icon_selected_id', 'IconSelectedId', 'INTEGER', false, null, null);
        $this->addColumn('resource', 'Resource', 'VARCHAR', false, 2048, null);
        $this->addColumn('params', 'Params', 'LONGVARCHAR', false, null, null);
        $this->addColumn('template_id', 'TemplateId', 'INTEGER', true, null, null);
        $this->addColumn('layout', 'Layout', 'LONGVARCHAR', false, null, null);
        $this->addColumn('tags', 'Tags', 'LONGVARCHAR', false, null, null);
        $this->addColumn('tags_user', 'TagsUser', 'LONGVARCHAR', false, null, null);
        $this->addColumn('parsed_tags', 'ParsedTags', 'CLOB', false, null, null);
        $this->addColumn('status', 'Status', 'BOOLEAN', false, 1, null);
        $this->addColumn('weight', 'Weight', 'INTEGER', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SectionArchiveTableMap::CLASS_DEFAULT : SectionArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SectionArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SectionArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SectionArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SectionArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SectionArchiveTableMap::OM_CLASS;
            /** @var SectionArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SectionArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SectionArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SectionArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SectionArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SectionArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_ID);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_AREA);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_TYPE);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_AUTHOR_ID);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_TITLE);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_SLUG);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_ICON_ID);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_ICON_SELECTED_ID);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_RESOURCE);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_PARAMS);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_TEMPLATE_ID);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_LAYOUT);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_TAGS);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_TAGS_USER);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_PARSED_TAGS);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_STATUS);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_WEIGHT);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(SectionArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.area');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.author_id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.slug');
            $criteria->addSelectColumn($alias . '.icon_id');
            $criteria->addSelectColumn($alias . '.icon_selected_id');
            $criteria->addSelectColumn($alias . '.resource');
            $criteria->addSelectColumn($alias . '.params');
            $criteria->addSelectColumn($alias . '.template_id');
            $criteria->addSelectColumn($alias . '.layout');
            $criteria->addSelectColumn($alias . '.tags');
            $criteria->addSelectColumn($alias . '.tags_user');
            $criteria->addSelectColumn($alias . '.parsed_tags');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.weight');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SectionArchiveTableMap::DATABASE_NAME)->getTable(SectionArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SectionArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SectionArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SectionArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SectionArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SectionArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\SectionArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SectionArchiveTableMap::DATABASE_NAME);
            $criteria->add(SectionArchiveTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SectionArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SectionArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SectionArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the section_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SectionArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SectionArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or SectionArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SectionArchive object
        }


        // Set the correct dbName
        $query = SectionArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SectionArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SectionArchiveTableMap::buildTableMap();
