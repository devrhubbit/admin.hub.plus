<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\PostAction;
use Database\HubPlus\PostActionQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'post_action' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PostActionTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.PostActionTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'post_action';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\PostAction';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.PostAction';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'post_action.id';

    /**
     * the column name for the post_id field
     */
    const COL_POST_ID = 'post_action.post_id';

    /**
     * the column name for the area field
     */
    const COL_AREA = 'post_action.area';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'post_action.type';

    /**
     * the column name for the label field
     */
    const COL_LABEL = 'post_action.label';

    /**
     * the column name for the cover_id field
     */
    const COL_COVER_ID = 'post_action.cover_id';

    /**
     * the column name for the params field
     */
    const COL_PARAMS = 'post_action.params';

    /**
     * the column name for the weight field
     */
    const COL_WEIGHT = 'post_action.weight';

    /**
     * the column name for the section_id field
     */
    const COL_SECTION_ID = 'post_action.section_id';

    /**
     * the column name for the content_id field
     */
    const COL_CONTENT_ID = 'post_action.content_id';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'post_action.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'post_action.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'PostId', 'Area', 'Type', 'Label', 'CoverId', 'Params', 'Weight', 'SectionId', 'ContentId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'postId', 'area', 'type', 'label', 'coverId', 'params', 'weight', 'sectionId', 'contentId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(PostActionTableMap::COL_ID, PostActionTableMap::COL_POST_ID, PostActionTableMap::COL_AREA, PostActionTableMap::COL_TYPE, PostActionTableMap::COL_LABEL, PostActionTableMap::COL_COVER_ID, PostActionTableMap::COL_PARAMS, PostActionTableMap::COL_WEIGHT, PostActionTableMap::COL_SECTION_ID, PostActionTableMap::COL_CONTENT_ID, PostActionTableMap::COL_CREATED_AT, PostActionTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'post_id', 'area', 'type', 'label', 'cover_id', 'params', 'weight', 'section_id', 'content_id', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'PostId' => 1, 'Area' => 2, 'Type' => 3, 'Label' => 4, 'CoverId' => 5, 'Params' => 6, 'Weight' => 7, 'SectionId' => 8, 'ContentId' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'postId' => 1, 'area' => 2, 'type' => 3, 'label' => 4, 'coverId' => 5, 'params' => 6, 'weight' => 7, 'sectionId' => 8, 'contentId' => 9, 'createdAt' => 10, 'updatedAt' => 11, ),
        self::TYPE_COLNAME       => array(PostActionTableMap::COL_ID => 0, PostActionTableMap::COL_POST_ID => 1, PostActionTableMap::COL_AREA => 2, PostActionTableMap::COL_TYPE => 3, PostActionTableMap::COL_LABEL => 4, PostActionTableMap::COL_COVER_ID => 5, PostActionTableMap::COL_PARAMS => 6, PostActionTableMap::COL_WEIGHT => 7, PostActionTableMap::COL_SECTION_ID => 8, PostActionTableMap::COL_CONTENT_ID => 9, PostActionTableMap::COL_CREATED_AT => 10, PostActionTableMap::COL_UPDATED_AT => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'post_id' => 1, 'area' => 2, 'type' => 3, 'label' => 4, 'cover_id' => 5, 'params' => 6, 'weight' => 7, 'section_id' => 8, 'content_id' => 9, 'created_at' => 10, 'updated_at' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('post_action');
        $this->setPhpName('PostAction');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\PostAction');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('post_id', 'PostId', 'INTEGER', 'post', 'id', true, null, null);
        $this->addColumn('area', 'Area', 'VARCHAR', false, 50, null);
        $this->addColumn('type', 'Type', 'VARCHAR', true, 10, null);
        $this->addColumn('label', 'Label', 'VARCHAR', true, 255, null);
        $this->addForeignKey('cover_id', 'CoverId', 'INTEGER', 'media', 'id', false, null, null);
        $this->addColumn('params', 'Params', 'CLOB', false, null, null);
        $this->addColumn('weight', 'Weight', 'INTEGER', false, null, null);
        $this->addForeignKey('section_id', 'SectionId', 'INTEGER', 'section', 'id', false, null, null);
        $this->addForeignKey('content_id', 'ContentId', 'INTEGER', 'post', 'id', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PostRelatedByPostId', '\\Database\\HubPlus\\Post', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':post_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Section', '\\Database\\HubPlus\\Section', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':section_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('PostRelatedByContentId', '\\Database\\HubPlus\\Post', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':content_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Media', '\\Database\\HubPlus\\Media', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':cover_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PostActionTableMap::CLASS_DEFAULT : PostActionTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PostAction object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PostActionTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PostActionTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PostActionTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PostActionTableMap::OM_CLASS;
            /** @var PostAction $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PostActionTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PostActionTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PostActionTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PostAction $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PostActionTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PostActionTableMap::COL_ID);
            $criteria->addSelectColumn(PostActionTableMap::COL_POST_ID);
            $criteria->addSelectColumn(PostActionTableMap::COL_AREA);
            $criteria->addSelectColumn(PostActionTableMap::COL_TYPE);
            $criteria->addSelectColumn(PostActionTableMap::COL_LABEL);
            $criteria->addSelectColumn(PostActionTableMap::COL_COVER_ID);
            $criteria->addSelectColumn(PostActionTableMap::COL_PARAMS);
            $criteria->addSelectColumn(PostActionTableMap::COL_WEIGHT);
            $criteria->addSelectColumn(PostActionTableMap::COL_SECTION_ID);
            $criteria->addSelectColumn(PostActionTableMap::COL_CONTENT_ID);
            $criteria->addSelectColumn(PostActionTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PostActionTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.post_id');
            $criteria->addSelectColumn($alias . '.area');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.label');
            $criteria->addSelectColumn($alias . '.cover_id');
            $criteria->addSelectColumn($alias . '.params');
            $criteria->addSelectColumn($alias . '.weight');
            $criteria->addSelectColumn($alias . '.section_id');
            $criteria->addSelectColumn($alias . '.content_id');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PostActionTableMap::DATABASE_NAME)->getTable(PostActionTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PostActionTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PostActionTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PostActionTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PostAction or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PostAction object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostActionTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\PostAction) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PostActionTableMap::DATABASE_NAME);
            $criteria->add(PostActionTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PostActionQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PostActionTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PostActionTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the post_action table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PostActionQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PostAction or Criteria object.
     *
     * @param mixed               $criteria Criteria or PostAction object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostActionTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PostAction object
        }

        if ($criteria->containsKey(PostActionTableMap::COL_ID) && $criteria->keyContainsValue(PostActionTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PostActionTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PostActionQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PostActionTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PostActionTableMap::buildTableMap();
