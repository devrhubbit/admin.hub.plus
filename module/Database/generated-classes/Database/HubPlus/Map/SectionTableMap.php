<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\Section;
use Database\HubPlus\SectionQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'section' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SectionTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.SectionTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'section';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\Section';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.Section';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 21;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 21;

    /**
     * the column name for the id field
     */
    const COL_ID = 'section.id';

    /**
     * the column name for the area field
     */
    const COL_AREA = 'section.area';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'section.type';

    /**
     * the column name for the author_id field
     */
    const COL_AUTHOR_ID = 'section.author_id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'section.title';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'section.description';

    /**
     * the column name for the slug field
     */
    const COL_SLUG = 'section.slug';

    /**
     * the column name for the icon_id field
     */
    const COL_ICON_ID = 'section.icon_id';

    /**
     * the column name for the icon_selected_id field
     */
    const COL_ICON_SELECTED_ID = 'section.icon_selected_id';

    /**
     * the column name for the resource field
     */
    const COL_RESOURCE = 'section.resource';

    /**
     * the column name for the params field
     */
    const COL_PARAMS = 'section.params';

    /**
     * the column name for the template_id field
     */
    const COL_TEMPLATE_ID = 'section.template_id';

    /**
     * the column name for the layout field
     */
    const COL_LAYOUT = 'section.layout';

    /**
     * the column name for the tags field
     */
    const COL_TAGS = 'section.tags';

    /**
     * the column name for the tags_user field
     */
    const COL_TAGS_USER = 'section.tags_user';

    /**
     * the column name for the parsed_tags field
     */
    const COL_PARSED_TAGS = 'section.parsed_tags';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'section.status';

    /**
     * the column name for the weight field
     */
    const COL_WEIGHT = 'section.weight';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'section.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'section.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'section.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Area', 'Type', 'AuthorId', 'Title', 'Description', 'Slug', 'IconId', 'IconSelectedId', 'Resource', 'Params', 'TemplateId', 'Layout', 'Tags', 'TagsUser', 'ParsedTags', 'Status', 'Weight', 'DeletedAt', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'area', 'type', 'authorId', 'title', 'description', 'slug', 'iconId', 'iconSelectedId', 'resource', 'params', 'templateId', 'layout', 'tags', 'tagsUser', 'parsedTags', 'status', 'weight', 'deletedAt', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(SectionTableMap::COL_ID, SectionTableMap::COL_AREA, SectionTableMap::COL_TYPE, SectionTableMap::COL_AUTHOR_ID, SectionTableMap::COL_TITLE, SectionTableMap::COL_DESCRIPTION, SectionTableMap::COL_SLUG, SectionTableMap::COL_ICON_ID, SectionTableMap::COL_ICON_SELECTED_ID, SectionTableMap::COL_RESOURCE, SectionTableMap::COL_PARAMS, SectionTableMap::COL_TEMPLATE_ID, SectionTableMap::COL_LAYOUT, SectionTableMap::COL_TAGS, SectionTableMap::COL_TAGS_USER, SectionTableMap::COL_PARSED_TAGS, SectionTableMap::COL_STATUS, SectionTableMap::COL_WEIGHT, SectionTableMap::COL_DELETED_AT, SectionTableMap::COL_CREATED_AT, SectionTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'area', 'type', 'author_id', 'title', 'description', 'slug', 'icon_id', 'icon_selected_id', 'resource', 'params', 'template_id', 'layout', 'tags', 'tags_user', 'parsed_tags', 'status', 'weight', 'deleted_at', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Area' => 1, 'Type' => 2, 'AuthorId' => 3, 'Title' => 4, 'Description' => 5, 'Slug' => 6, 'IconId' => 7, 'IconSelectedId' => 8, 'Resource' => 9, 'Params' => 10, 'TemplateId' => 11, 'Layout' => 12, 'Tags' => 13, 'TagsUser' => 14, 'ParsedTags' => 15, 'Status' => 16, 'Weight' => 17, 'DeletedAt' => 18, 'CreatedAt' => 19, 'UpdatedAt' => 20, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'area' => 1, 'type' => 2, 'authorId' => 3, 'title' => 4, 'description' => 5, 'slug' => 6, 'iconId' => 7, 'iconSelectedId' => 8, 'resource' => 9, 'params' => 10, 'templateId' => 11, 'layout' => 12, 'tags' => 13, 'tagsUser' => 14, 'parsedTags' => 15, 'status' => 16, 'weight' => 17, 'deletedAt' => 18, 'createdAt' => 19, 'updatedAt' => 20, ),
        self::TYPE_COLNAME       => array(SectionTableMap::COL_ID => 0, SectionTableMap::COL_AREA => 1, SectionTableMap::COL_TYPE => 2, SectionTableMap::COL_AUTHOR_ID => 3, SectionTableMap::COL_TITLE => 4, SectionTableMap::COL_DESCRIPTION => 5, SectionTableMap::COL_SLUG => 6, SectionTableMap::COL_ICON_ID => 7, SectionTableMap::COL_ICON_SELECTED_ID => 8, SectionTableMap::COL_RESOURCE => 9, SectionTableMap::COL_PARAMS => 10, SectionTableMap::COL_TEMPLATE_ID => 11, SectionTableMap::COL_LAYOUT => 12, SectionTableMap::COL_TAGS => 13, SectionTableMap::COL_TAGS_USER => 14, SectionTableMap::COL_PARSED_TAGS => 15, SectionTableMap::COL_STATUS => 16, SectionTableMap::COL_WEIGHT => 17, SectionTableMap::COL_DELETED_AT => 18, SectionTableMap::COL_CREATED_AT => 19, SectionTableMap::COL_UPDATED_AT => 20, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'area' => 1, 'type' => 2, 'author_id' => 3, 'title' => 4, 'description' => 5, 'slug' => 6, 'icon_id' => 7, 'icon_selected_id' => 8, 'resource' => 9, 'params' => 10, 'template_id' => 11, 'layout' => 12, 'tags' => 13, 'tags_user' => 14, 'parsed_tags' => 15, 'status' => 16, 'weight' => 17, 'deleted_at' => 18, 'created_at' => 19, 'updated_at' => 20, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('section');
        $this->setPhpName('Section');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\Section');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('area', 'Area', 'VARCHAR', false, 50, null);
        $this->addColumn('type', 'Type', 'VARCHAR', false, 50, null);
        $this->addForeignKey('author_id', 'AuthorId', 'INTEGER', 'user_backend', 'id', false, null, null);
        $this->addColumn('title', 'Title', 'LONGVARCHAR', false, null, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('slug', 'Slug', 'VARCHAR', false, 255, null);
        $this->addForeignKey('icon_id', 'IconId', 'INTEGER', 'media', 'id', false, null, null);
        $this->addForeignKey('icon_selected_id', 'IconSelectedId', 'INTEGER', 'media', 'id', false, null, null);
        $this->addColumn('resource', 'Resource', 'VARCHAR', false, 2048, null);
        $this->addColumn('params', 'Params', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('template_id', 'TemplateId', 'INTEGER', 'template', 'id', true, null, null);
        $this->addColumn('layout', 'Layout', 'LONGVARCHAR', false, null, null);
        $this->addColumn('tags', 'Tags', 'LONGVARCHAR', false, null, null);
        $this->addColumn('tags_user', 'TagsUser', 'LONGVARCHAR', false, null, null);
        $this->addColumn('parsed_tags', 'ParsedTags', 'CLOB', false, null, null);
        $this->addColumn('status', 'Status', 'BOOLEAN', false, 1, null);
        $this->addColumn('weight', 'Weight', 'INTEGER', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Template', '\\Database\\HubPlus\\Template', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':template_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('MediaRelatedByIconId', '\\Database\\HubPlus\\Media', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':icon_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('MediaRelatedByIconSelectedId', '\\Database\\HubPlus\\Media', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':icon_selected_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('UserBackend', '\\Database\\HubPlus\\UserBackend', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':author_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('CustomFormExtraLog', '\\Database\\HubPlus\\CustomFormExtraLog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':form_id',
    1 => ':id',
  ),
), null, null, 'CustomFormExtraLogs', false);
        $this->addRelation('CustomFormLog', '\\Database\\HubPlus\\CustomFormLog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':form_id',
    1 => ':id',
  ),
), null, null, 'CustomFormLogs', false);
        $this->addRelation('PostAction', '\\Database\\HubPlus\\PostAction', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':section_id',
    1 => ':id',
  ),
), null, null, 'PostActions', false);
        $this->addRelation('PushNotification', '\\Database\\HubPlus\\PushNotification', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':section_id',
    1 => ':id',
  ),
), null, null, 'PushNotifications', false);
        $this->addRelation('SectionConnectorRelatedBySectionId', '\\Database\\HubPlus\\SectionConnector', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':section_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'SectionConnectorsRelatedBySectionId', false);
        $this->addRelation('SectionConnectorRelatedByRemoteSectionId', '\\Database\\HubPlus\\SectionConnector', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':remote_section_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'SectionConnectorsRelatedByRemoteSectionId', false);
        $this->addRelation('SectionRelatedToRelatedByFromSectionId', '\\Database\\HubPlus\\SectionRelatedTo', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':from_section_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'SectionRelatedTosRelatedByFromSectionId', false);
        $this->addRelation('SectionRelatedToRelatedByToSectionId', '\\Database\\HubPlus\\SectionRelatedTo', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':to_section_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'SectionRelatedTosRelatedByToSectionId', false);
        $this->addRelation('UserApp', '\\Database\\HubPlus\\UserApp', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':section_id',
    1 => ':id',
  ),
), null, null, 'UserApps', false);
        $this->addRelation('Webhook', '\\Database\\HubPlus\\Webhook', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':section_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'Webhooks', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to section     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        SectionConnectorTableMap::clearInstancePool();
        SectionRelatedToTableMap::clearInstancePool();
        WebhookTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SectionTableMap::CLASS_DEFAULT : SectionTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Section object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SectionTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SectionTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SectionTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SectionTableMap::OM_CLASS;
            /** @var Section $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SectionTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SectionTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SectionTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Section $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SectionTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SectionTableMap::COL_ID);
            $criteria->addSelectColumn(SectionTableMap::COL_AREA);
            $criteria->addSelectColumn(SectionTableMap::COL_TYPE);
            $criteria->addSelectColumn(SectionTableMap::COL_AUTHOR_ID);
            $criteria->addSelectColumn(SectionTableMap::COL_TITLE);
            $criteria->addSelectColumn(SectionTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(SectionTableMap::COL_SLUG);
            $criteria->addSelectColumn(SectionTableMap::COL_ICON_ID);
            $criteria->addSelectColumn(SectionTableMap::COL_ICON_SELECTED_ID);
            $criteria->addSelectColumn(SectionTableMap::COL_RESOURCE);
            $criteria->addSelectColumn(SectionTableMap::COL_PARAMS);
            $criteria->addSelectColumn(SectionTableMap::COL_TEMPLATE_ID);
            $criteria->addSelectColumn(SectionTableMap::COL_LAYOUT);
            $criteria->addSelectColumn(SectionTableMap::COL_TAGS);
            $criteria->addSelectColumn(SectionTableMap::COL_TAGS_USER);
            $criteria->addSelectColumn(SectionTableMap::COL_PARSED_TAGS);
            $criteria->addSelectColumn(SectionTableMap::COL_STATUS);
            $criteria->addSelectColumn(SectionTableMap::COL_WEIGHT);
            $criteria->addSelectColumn(SectionTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(SectionTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(SectionTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.area');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.author_id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.slug');
            $criteria->addSelectColumn($alias . '.icon_id');
            $criteria->addSelectColumn($alias . '.icon_selected_id');
            $criteria->addSelectColumn($alias . '.resource');
            $criteria->addSelectColumn($alias . '.params');
            $criteria->addSelectColumn($alias . '.template_id');
            $criteria->addSelectColumn($alias . '.layout');
            $criteria->addSelectColumn($alias . '.tags');
            $criteria->addSelectColumn($alias . '.tags_user');
            $criteria->addSelectColumn($alias . '.parsed_tags');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.weight');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SectionTableMap::DATABASE_NAME)->getTable(SectionTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SectionTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SectionTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SectionTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Section or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Section object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\Section) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SectionTableMap::DATABASE_NAME);
            $criteria->add(SectionTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SectionQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SectionTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SectionTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the section table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SectionQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Section or Criteria object.
     *
     * @param mixed               $criteria Criteria or Section object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Section object
        }

        if ($criteria->containsKey(SectionTableMap::COL_ID) && $criteria->keyContainsValue(SectionTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SectionTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SectionQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SectionTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SectionTableMap::buildTableMap();
