<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\PostArchive;
use Database\HubPlus\PostArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'post_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PostArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.PostArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'post_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\PostArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.PostArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 22;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 22;

    /**
     * the column name for the id field
     */
    const COL_ID = 'post_archive.id';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'post_archive.type';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'post_archive.status';

    /**
     * the column name for the author_id field
     */
    const COL_AUTHOR_ID = 'post_archive.author_id';

    /**
     * the column name for the cover_id field
     */
    const COL_COVER_ID = 'post_archive.cover_id';

    /**
     * the column name for the slug field
     */
    const COL_SLUG = 'post_archive.slug';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'post_archive.title';

    /**
     * the column name for the subtitle field
     */
    const COL_SUBTITLE = 'post_archive.subtitle';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'post_archive.description';

    /**
     * the column name for the body field
     */
    const COL_BODY = 'post_archive.body';

    /**
     * the column name for the search field
     */
    const COL_SEARCH = 'post_archive.search';

    /**
     * the column name for the template_id field
     */
    const COL_TEMPLATE_ID = 'post_archive.template_id';

    /**
     * the column name for the layout field
     */
    const COL_LAYOUT = 'post_archive.layout';

    /**
     * the column name for the tags field
     */
    const COL_TAGS = 'post_archive.tags';

    /**
     * the column name for the tags_user field
     */
    const COL_TAGS_USER = 'post_archive.tags_user';

    /**
     * the column name for the row_data field
     */
    const COL_ROW_DATA = 'post_archive.row_data';

    /**
     * the column name for the count_like field
     */
    const COL_COUNT_LIKE = 'post_archive.count_like';

    /**
     * the column name for the count_share field
     */
    const COL_COUNT_SHARE = 'post_archive.count_share';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'post_archive.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'post_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'post_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'post_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Type', 'Status', 'AuthorId', 'CoverId', 'Slug', 'Title', 'Subtitle', 'Description', 'Body', 'Search', 'TemplateId', 'Layout', 'Tags', 'TagsUser', 'RowData', 'CountLike', 'CountShare', 'DeletedAt', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'type', 'status', 'authorId', 'coverId', 'slug', 'title', 'subtitle', 'description', 'body', 'search', 'templateId', 'layout', 'tags', 'tagsUser', 'rowData', 'countLike', 'countShare', 'deletedAt', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(PostArchiveTableMap::COL_ID, PostArchiveTableMap::COL_TYPE, PostArchiveTableMap::COL_STATUS, PostArchiveTableMap::COL_AUTHOR_ID, PostArchiveTableMap::COL_COVER_ID, PostArchiveTableMap::COL_SLUG, PostArchiveTableMap::COL_TITLE, PostArchiveTableMap::COL_SUBTITLE, PostArchiveTableMap::COL_DESCRIPTION, PostArchiveTableMap::COL_BODY, PostArchiveTableMap::COL_SEARCH, PostArchiveTableMap::COL_TEMPLATE_ID, PostArchiveTableMap::COL_LAYOUT, PostArchiveTableMap::COL_TAGS, PostArchiveTableMap::COL_TAGS_USER, PostArchiveTableMap::COL_ROW_DATA, PostArchiveTableMap::COL_COUNT_LIKE, PostArchiveTableMap::COL_COUNT_SHARE, PostArchiveTableMap::COL_DELETED_AT, PostArchiveTableMap::COL_CREATED_AT, PostArchiveTableMap::COL_UPDATED_AT, PostArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'type', 'status', 'author_id', 'cover_id', 'slug', 'title', 'subtitle', 'description', 'body', 'search', 'template_id', 'layout', 'tags', 'tags_user', 'row_data', 'count_like', 'count_share', 'deleted_at', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Type' => 1, 'Status' => 2, 'AuthorId' => 3, 'CoverId' => 4, 'Slug' => 5, 'Title' => 6, 'Subtitle' => 7, 'Description' => 8, 'Body' => 9, 'Search' => 10, 'TemplateId' => 11, 'Layout' => 12, 'Tags' => 13, 'TagsUser' => 14, 'RowData' => 15, 'CountLike' => 16, 'CountShare' => 17, 'DeletedAt' => 18, 'CreatedAt' => 19, 'UpdatedAt' => 20, 'ArchivedAt' => 21, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'type' => 1, 'status' => 2, 'authorId' => 3, 'coverId' => 4, 'slug' => 5, 'title' => 6, 'subtitle' => 7, 'description' => 8, 'body' => 9, 'search' => 10, 'templateId' => 11, 'layout' => 12, 'tags' => 13, 'tagsUser' => 14, 'rowData' => 15, 'countLike' => 16, 'countShare' => 17, 'deletedAt' => 18, 'createdAt' => 19, 'updatedAt' => 20, 'archivedAt' => 21, ),
        self::TYPE_COLNAME       => array(PostArchiveTableMap::COL_ID => 0, PostArchiveTableMap::COL_TYPE => 1, PostArchiveTableMap::COL_STATUS => 2, PostArchiveTableMap::COL_AUTHOR_ID => 3, PostArchiveTableMap::COL_COVER_ID => 4, PostArchiveTableMap::COL_SLUG => 5, PostArchiveTableMap::COL_TITLE => 6, PostArchiveTableMap::COL_SUBTITLE => 7, PostArchiveTableMap::COL_DESCRIPTION => 8, PostArchiveTableMap::COL_BODY => 9, PostArchiveTableMap::COL_SEARCH => 10, PostArchiveTableMap::COL_TEMPLATE_ID => 11, PostArchiveTableMap::COL_LAYOUT => 12, PostArchiveTableMap::COL_TAGS => 13, PostArchiveTableMap::COL_TAGS_USER => 14, PostArchiveTableMap::COL_ROW_DATA => 15, PostArchiveTableMap::COL_COUNT_LIKE => 16, PostArchiveTableMap::COL_COUNT_SHARE => 17, PostArchiveTableMap::COL_DELETED_AT => 18, PostArchiveTableMap::COL_CREATED_AT => 19, PostArchiveTableMap::COL_UPDATED_AT => 20, PostArchiveTableMap::COL_ARCHIVED_AT => 21, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'type' => 1, 'status' => 2, 'author_id' => 3, 'cover_id' => 4, 'slug' => 5, 'title' => 6, 'subtitle' => 7, 'description' => 8, 'body' => 9, 'search' => 10, 'template_id' => 11, 'layout' => 12, 'tags' => 13, 'tags_user' => 14, 'row_data' => 15, 'count_like' => 16, 'count_share' => 17, 'deleted_at' => 18, 'created_at' => 19, 'updated_at' => 20, 'archived_at' => 21, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('post_archive');
        $this->setPhpName('PostArchive');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\PostArchive');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('type', 'Type', 'VARCHAR', false, 20, null);
        $this->addColumn('status', 'Status', 'TINYINT', false, 3, null);
        $this->addColumn('author_id', 'AuthorId', 'INTEGER', false, null, null);
        $this->addColumn('cover_id', 'CoverId', 'INTEGER', false, null, null);
        $this->addColumn('slug', 'Slug', 'VARCHAR', false, 255, null);
        $this->addColumn('title', 'Title', 'LONGVARCHAR', false, null, null);
        $this->addColumn('subtitle', 'Subtitle', 'LONGVARCHAR', false, null, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('body', 'Body', 'LONGVARCHAR', false, null, null);
        $this->addColumn('search', 'Search', 'LONGVARCHAR', false, null, null);
        $this->addColumn('template_id', 'TemplateId', 'INTEGER', true, null, null);
        $this->addColumn('layout', 'Layout', 'LONGVARCHAR', false, null, null);
        $this->addColumn('tags', 'Tags', 'LONGVARCHAR', false, null, null);
        $this->addColumn('tags_user', 'TagsUser', 'LONGVARCHAR', false, null, null);
        $this->addColumn('row_data', 'RowData', 'LONGVARCHAR', false, null, null);
        $this->addColumn('count_like', 'CountLike', 'INTEGER', false, null, 0);
        $this->addColumn('count_share', 'CountShare', 'INTEGER', false, null, 0);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PostArchiveTableMap::CLASS_DEFAULT : PostArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PostArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PostArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PostArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PostArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PostArchiveTableMap::OM_CLASS;
            /** @var PostArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PostArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PostArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PostArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PostArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PostArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PostArchiveTableMap::COL_ID);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_TYPE);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_STATUS);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_AUTHOR_ID);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_COVER_ID);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_SLUG);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_TITLE);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_SUBTITLE);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_BODY);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_SEARCH);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_TEMPLATE_ID);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_LAYOUT);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_TAGS);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_TAGS_USER);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_ROW_DATA);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_COUNT_LIKE);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_COUNT_SHARE);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(PostArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.author_id');
            $criteria->addSelectColumn($alias . '.cover_id');
            $criteria->addSelectColumn($alias . '.slug');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.subtitle');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.body');
            $criteria->addSelectColumn($alias . '.search');
            $criteria->addSelectColumn($alias . '.template_id');
            $criteria->addSelectColumn($alias . '.layout');
            $criteria->addSelectColumn($alias . '.tags');
            $criteria->addSelectColumn($alias . '.tags_user');
            $criteria->addSelectColumn($alias . '.row_data');
            $criteria->addSelectColumn($alias . '.count_like');
            $criteria->addSelectColumn($alias . '.count_share');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PostArchiveTableMap::DATABASE_NAME)->getTable(PostArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PostArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PostArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PostArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PostArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PostArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\PostArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PostArchiveTableMap::DATABASE_NAME);
            $criteria->add(PostArchiveTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PostArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PostArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PostArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the post_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PostArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PostArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or PostArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PostArchive object
        }


        // Set the correct dbName
        $query = PostArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PostArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PostArchiveTableMap::buildTableMap();
