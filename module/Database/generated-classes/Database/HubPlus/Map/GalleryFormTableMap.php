<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\GalleryForm;
use Database\HubPlus\GalleryFormQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'gallery_form' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class GalleryFormTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.GalleryFormTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'gallery_form';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\GalleryForm';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.GalleryForm';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the media_id field
     */
    const COL_MEDIA_ID = 'gallery_form.media_id';

    /**
     * the column name for the form_id field
     */
    const COL_FORM_ID = 'gallery_form.form_id';

    /**
     * the column name for the row_id field
     */
    const COL_ROW_ID = 'gallery_form.row_id';

    /**
     * the column name for the field_name field
     */
    const COL_FIELD_NAME = 'gallery_form.field_name';

    /**
     * the column name for the weight field
     */
    const COL_WEIGHT = 'gallery_form.weight';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('MediaId', 'FormId', 'RowId', 'FieldName', 'Weight', ),
        self::TYPE_CAMELNAME     => array('mediaId', 'formId', 'rowId', 'fieldName', 'weight', ),
        self::TYPE_COLNAME       => array(GalleryFormTableMap::COL_MEDIA_ID, GalleryFormTableMap::COL_FORM_ID, GalleryFormTableMap::COL_ROW_ID, GalleryFormTableMap::COL_FIELD_NAME, GalleryFormTableMap::COL_WEIGHT, ),
        self::TYPE_FIELDNAME     => array('media_id', 'form_id', 'row_id', 'field_name', 'weight', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('MediaId' => 0, 'FormId' => 1, 'RowId' => 2, 'FieldName' => 3, 'Weight' => 4, ),
        self::TYPE_CAMELNAME     => array('mediaId' => 0, 'formId' => 1, 'rowId' => 2, 'fieldName' => 3, 'weight' => 4, ),
        self::TYPE_COLNAME       => array(GalleryFormTableMap::COL_MEDIA_ID => 0, GalleryFormTableMap::COL_FORM_ID => 1, GalleryFormTableMap::COL_ROW_ID => 2, GalleryFormTableMap::COL_FIELD_NAME => 3, GalleryFormTableMap::COL_WEIGHT => 4, ),
        self::TYPE_FIELDNAME     => array('media_id' => 0, 'form_id' => 1, 'row_id' => 2, 'field_name' => 3, 'weight' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('gallery_form');
        $this->setPhpName('GalleryForm');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\GalleryForm');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignKey('media_id', 'MediaId', 'INTEGER', 'media', 'id', false, null, null);
        $this->addColumn('form_id', 'FormId', 'INTEGER', false, null, null);
        $this->addColumn('row_id', 'RowId', 'INTEGER', false, null, null);
        $this->addColumn('field_name', 'FieldName', 'VARCHAR', false, 1024, null);
        $this->addColumn('weight', 'Weight', 'INTEGER', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Media', '\\Database\\HubPlus\\Media', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':media_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return null;
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return '';
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? GalleryFormTableMap::CLASS_DEFAULT : GalleryFormTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (GalleryForm object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = GalleryFormTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = GalleryFormTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + GalleryFormTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = GalleryFormTableMap::OM_CLASS;
            /** @var GalleryForm $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            GalleryFormTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = GalleryFormTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = GalleryFormTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var GalleryForm $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                GalleryFormTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(GalleryFormTableMap::COL_MEDIA_ID);
            $criteria->addSelectColumn(GalleryFormTableMap::COL_FORM_ID);
            $criteria->addSelectColumn(GalleryFormTableMap::COL_ROW_ID);
            $criteria->addSelectColumn(GalleryFormTableMap::COL_FIELD_NAME);
            $criteria->addSelectColumn(GalleryFormTableMap::COL_WEIGHT);
        } else {
            $criteria->addSelectColumn($alias . '.media_id');
            $criteria->addSelectColumn($alias . '.form_id');
            $criteria->addSelectColumn($alias . '.row_id');
            $criteria->addSelectColumn($alias . '.field_name');
            $criteria->addSelectColumn($alias . '.weight');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(GalleryFormTableMap::DATABASE_NAME)->getTable(GalleryFormTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(GalleryFormTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(GalleryFormTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new GalleryFormTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a GalleryForm or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or GalleryForm object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GalleryFormTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\GalleryForm) { // it's a model object
            // create criteria based on pk value
            $criteria = $values->buildCriteria();
        } else { // it's a primary key, or an array of pks
            throw new LogicException('The GalleryForm object has no primary key');
        }

        $query = GalleryFormQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            GalleryFormTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                GalleryFormTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the gallery_form table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return GalleryFormQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a GalleryForm or Criteria object.
     *
     * @param mixed               $criteria Criteria or GalleryForm object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(GalleryFormTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from GalleryForm object
        }


        // Set the correct dbName
        $query = GalleryFormQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // GalleryFormTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
GalleryFormTableMap::buildTableMap();
