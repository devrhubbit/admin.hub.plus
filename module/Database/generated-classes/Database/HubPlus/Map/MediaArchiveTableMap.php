<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\MediaArchive;
use Database\HubPlus\MediaArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'media_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class MediaArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.MediaArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'media_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\MediaArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.MediaArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 20;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 20;

    /**
     * the column name for the id field
     */
    const COL_ID = 'media_archive.id';

    /**
     * the column name for the device_id field
     */
    const COL_DEVICE_ID = 'media_archive.device_id';

    /**
     * the column name for the is_public field
     */
    const COL_IS_PUBLIC = 'media_archive.is_public';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'media_archive.type';

    /**
     * the column name for the uri field
     */
    const COL_URI = 'media_archive.uri';

    /**
     * the column name for the uri_thumb field
     */
    const COL_URI_THUMB = 'media_archive.uri_thumb';

    /**
     * the column name for the extra_params field
     */
    const COL_EXTRA_PARAMS = 'media_archive.extra_params';

    /**
     * the column name for the weight field
     */
    const COL_WEIGHT = 'media_archive.weight';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'media_archive.title';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'media_archive.description';

    /**
     * the column name for the extension field
     */
    const COL_EXTENSION = 'media_archive.extension';

    /**
     * the column name for the mime_type field
     */
    const COL_MIME_TYPE = 'media_archive.mime_type';

    /**
     * the column name for the size field
     */
    const COL_SIZE = 'media_archive.size';

    /**
     * the column name for the count_like field
     */
    const COL_COUNT_LIKE = 'media_archive.count_like';

    /**
     * the column name for the count_share field
     */
    const COL_COUNT_SHARE = 'media_archive.count_share';

    /**
     * the column name for the format field
     */
    const COL_FORMAT = 'media_archive.format';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'media_archive.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'media_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'media_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'media_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'DeviceId', 'IsPublic', 'Type', 'Uri', 'UriThumb', 'ExtraParams', 'Weight', 'Title', 'Description', 'Extension', 'MimeType', 'Size', 'CountLike', 'CountShare', 'Format', 'DeletedAt', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'deviceId', 'isPublic', 'type', 'uri', 'uriThumb', 'extraParams', 'weight', 'title', 'description', 'extension', 'mimeType', 'size', 'countLike', 'countShare', 'format', 'deletedAt', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(MediaArchiveTableMap::COL_ID, MediaArchiveTableMap::COL_DEVICE_ID, MediaArchiveTableMap::COL_IS_PUBLIC, MediaArchiveTableMap::COL_TYPE, MediaArchiveTableMap::COL_URI, MediaArchiveTableMap::COL_URI_THUMB, MediaArchiveTableMap::COL_EXTRA_PARAMS, MediaArchiveTableMap::COL_WEIGHT, MediaArchiveTableMap::COL_TITLE, MediaArchiveTableMap::COL_DESCRIPTION, MediaArchiveTableMap::COL_EXTENSION, MediaArchiveTableMap::COL_MIME_TYPE, MediaArchiveTableMap::COL_SIZE, MediaArchiveTableMap::COL_COUNT_LIKE, MediaArchiveTableMap::COL_COUNT_SHARE, MediaArchiveTableMap::COL_FORMAT, MediaArchiveTableMap::COL_DELETED_AT, MediaArchiveTableMap::COL_CREATED_AT, MediaArchiveTableMap::COL_UPDATED_AT, MediaArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'device_id', 'is_public', 'type', 'uri', 'uri_thumb', 'extra_params', 'weight', 'title', 'description', 'extension', 'mime_type', 'size', 'count_like', 'count_share', 'format', 'deleted_at', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'DeviceId' => 1, 'IsPublic' => 2, 'Type' => 3, 'Uri' => 4, 'UriThumb' => 5, 'ExtraParams' => 6, 'Weight' => 7, 'Title' => 8, 'Description' => 9, 'Extension' => 10, 'MimeType' => 11, 'Size' => 12, 'CountLike' => 13, 'CountShare' => 14, 'Format' => 15, 'DeletedAt' => 16, 'CreatedAt' => 17, 'UpdatedAt' => 18, 'ArchivedAt' => 19, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'deviceId' => 1, 'isPublic' => 2, 'type' => 3, 'uri' => 4, 'uriThumb' => 5, 'extraParams' => 6, 'weight' => 7, 'title' => 8, 'description' => 9, 'extension' => 10, 'mimeType' => 11, 'size' => 12, 'countLike' => 13, 'countShare' => 14, 'format' => 15, 'deletedAt' => 16, 'createdAt' => 17, 'updatedAt' => 18, 'archivedAt' => 19, ),
        self::TYPE_COLNAME       => array(MediaArchiveTableMap::COL_ID => 0, MediaArchiveTableMap::COL_DEVICE_ID => 1, MediaArchiveTableMap::COL_IS_PUBLIC => 2, MediaArchiveTableMap::COL_TYPE => 3, MediaArchiveTableMap::COL_URI => 4, MediaArchiveTableMap::COL_URI_THUMB => 5, MediaArchiveTableMap::COL_EXTRA_PARAMS => 6, MediaArchiveTableMap::COL_WEIGHT => 7, MediaArchiveTableMap::COL_TITLE => 8, MediaArchiveTableMap::COL_DESCRIPTION => 9, MediaArchiveTableMap::COL_EXTENSION => 10, MediaArchiveTableMap::COL_MIME_TYPE => 11, MediaArchiveTableMap::COL_SIZE => 12, MediaArchiveTableMap::COL_COUNT_LIKE => 13, MediaArchiveTableMap::COL_COUNT_SHARE => 14, MediaArchiveTableMap::COL_FORMAT => 15, MediaArchiveTableMap::COL_DELETED_AT => 16, MediaArchiveTableMap::COL_CREATED_AT => 17, MediaArchiveTableMap::COL_UPDATED_AT => 18, MediaArchiveTableMap::COL_ARCHIVED_AT => 19, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'device_id' => 1, 'is_public' => 2, 'type' => 3, 'uri' => 4, 'uri_thumb' => 5, 'extra_params' => 6, 'weight' => 7, 'title' => 8, 'description' => 9, 'extension' => 10, 'mime_type' => 11, 'size' => 12, 'count_like' => 13, 'count_share' => 14, 'format' => 15, 'deleted_at' => 16, 'created_at' => 17, 'updated_at' => 18, 'archived_at' => 19, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('media_archive');
        $this->setPhpName('MediaArchive');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\MediaArchive');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('device_id', 'DeviceId', 'INTEGER', false, null, null);
        $this->addColumn('is_public', 'IsPublic', 'TINYINT', true, 1, 1);
        $this->addColumn('type', 'Type', 'VARCHAR', true, 20, null);
        $this->addColumn('uri', 'Uri', 'VARCHAR', false, 255, null);
        $this->addColumn('uri_thumb', 'UriThumb', 'VARCHAR', false, 255, null);
        $this->addColumn('extra_params', 'ExtraParams', 'LONGVARCHAR', false, null, null);
        $this->addColumn('weight', 'Weight', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 255, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('extension', 'Extension', 'VARCHAR', false, 20, null);
        $this->addColumn('mime_type', 'MimeType', 'VARCHAR', false, 20, null);
        $this->addColumn('size', 'Size', 'INTEGER', false, null, null);
        $this->addColumn('count_like', 'CountLike', 'INTEGER', false, null, 0);
        $this->addColumn('count_share', 'CountShare', 'INTEGER', false, null, 0);
        $this->addColumn('format', 'Format', 'LONGVARCHAR', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? MediaArchiveTableMap::CLASS_DEFAULT : MediaArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (MediaArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = MediaArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = MediaArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + MediaArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = MediaArchiveTableMap::OM_CLASS;
            /** @var MediaArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            MediaArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = MediaArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = MediaArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var MediaArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                MediaArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_ID);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_DEVICE_ID);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_IS_PUBLIC);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_TYPE);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_URI);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_URI_THUMB);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_EXTRA_PARAMS);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_WEIGHT);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_TITLE);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_EXTENSION);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_MIME_TYPE);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_SIZE);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_COUNT_LIKE);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_COUNT_SHARE);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_FORMAT);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(MediaArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.device_id');
            $criteria->addSelectColumn($alias . '.is_public');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.uri');
            $criteria->addSelectColumn($alias . '.uri_thumb');
            $criteria->addSelectColumn($alias . '.extra_params');
            $criteria->addSelectColumn($alias . '.weight');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.extension');
            $criteria->addSelectColumn($alias . '.mime_type');
            $criteria->addSelectColumn($alias . '.size');
            $criteria->addSelectColumn($alias . '.count_like');
            $criteria->addSelectColumn($alias . '.count_share');
            $criteria->addSelectColumn($alias . '.format');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(MediaArchiveTableMap::DATABASE_NAME)->getTable(MediaArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(MediaArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(MediaArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new MediaArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a MediaArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or MediaArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MediaArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\MediaArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(MediaArchiveTableMap::DATABASE_NAME);
            $criteria->add(MediaArchiveTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = MediaArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            MediaArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                MediaArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the media_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return MediaArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a MediaArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or MediaArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MediaArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from MediaArchive object
        }


        // Set the correct dbName
        $query = MediaArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // MediaArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
MediaArchiveTableMap::buildTableMap();
