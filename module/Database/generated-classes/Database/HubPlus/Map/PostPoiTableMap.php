<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\PostPoi;
use Database\HubPlus\PostPoiQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'post_poi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PostPoiTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.PostPoiTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'post_poi';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\PostPoi';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.PostPoi';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the id field
     */
    const COL_ID = 'post_poi.id';

    /**
     * the column name for the post_id field
     */
    const COL_POST_ID = 'post_poi.post_id';

    /**
     * the column name for the identifier field
     */
    const COL_IDENTIFIER = 'post_poi.identifier';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'post_poi.type';

    /**
     * the column name for the lat field
     */
    const COL_LAT = 'post_poi.lat';

    /**
     * the column name for the lng field
     */
    const COL_LNG = 'post_poi.lng';

    /**
     * the column name for the radius field
     */
    const COL_RADIUS = 'post_poi.radius';

    /**
     * the column name for the uuid field
     */
    const COL_UUID = 'post_poi.uuid';

    /**
     * the column name for the major field
     */
    const COL_MAJOR = 'post_poi.major';

    /**
     * the column name for the minor field
     */
    const COL_MINOR = 'post_poi.minor';

    /**
     * the column name for the distance field
     */
    const COL_DISTANCE = 'post_poi.distance';

    /**
     * the column name for the pin field
     */
    const COL_PIN = 'post_poi.pin';

    /**
     * the column name for the poi_notification field
     */
    const COL_POI_NOTIFICATION = 'post_poi.poi_notification';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'post_poi.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'post_poi.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'post_poi.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'PostId', 'Identifier', 'Type', 'Lat', 'Lng', 'Radius', 'Uuid', 'Major', 'Minor', 'Distance', 'Pin', 'PoiNotification', 'DeletedAt', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'postId', 'identifier', 'type', 'lat', 'lng', 'radius', 'uuid', 'major', 'minor', 'distance', 'pin', 'poiNotification', 'deletedAt', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(PostPoiTableMap::COL_ID, PostPoiTableMap::COL_POST_ID, PostPoiTableMap::COL_IDENTIFIER, PostPoiTableMap::COL_TYPE, PostPoiTableMap::COL_LAT, PostPoiTableMap::COL_LNG, PostPoiTableMap::COL_RADIUS, PostPoiTableMap::COL_UUID, PostPoiTableMap::COL_MAJOR, PostPoiTableMap::COL_MINOR, PostPoiTableMap::COL_DISTANCE, PostPoiTableMap::COL_PIN, PostPoiTableMap::COL_POI_NOTIFICATION, PostPoiTableMap::COL_DELETED_AT, PostPoiTableMap::COL_CREATED_AT, PostPoiTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'post_id', 'identifier', 'type', 'lat', 'lng', 'radius', 'uuid', 'major', 'minor', 'distance', 'pin', 'poi_notification', 'deleted_at', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'PostId' => 1, 'Identifier' => 2, 'Type' => 3, 'Lat' => 4, 'Lng' => 5, 'Radius' => 6, 'Uuid' => 7, 'Major' => 8, 'Minor' => 9, 'Distance' => 10, 'Pin' => 11, 'PoiNotification' => 12, 'DeletedAt' => 13, 'CreatedAt' => 14, 'UpdatedAt' => 15, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'postId' => 1, 'identifier' => 2, 'type' => 3, 'lat' => 4, 'lng' => 5, 'radius' => 6, 'uuid' => 7, 'major' => 8, 'minor' => 9, 'distance' => 10, 'pin' => 11, 'poiNotification' => 12, 'deletedAt' => 13, 'createdAt' => 14, 'updatedAt' => 15, ),
        self::TYPE_COLNAME       => array(PostPoiTableMap::COL_ID => 0, PostPoiTableMap::COL_POST_ID => 1, PostPoiTableMap::COL_IDENTIFIER => 2, PostPoiTableMap::COL_TYPE => 3, PostPoiTableMap::COL_LAT => 4, PostPoiTableMap::COL_LNG => 5, PostPoiTableMap::COL_RADIUS => 6, PostPoiTableMap::COL_UUID => 7, PostPoiTableMap::COL_MAJOR => 8, PostPoiTableMap::COL_MINOR => 9, PostPoiTableMap::COL_DISTANCE => 10, PostPoiTableMap::COL_PIN => 11, PostPoiTableMap::COL_POI_NOTIFICATION => 12, PostPoiTableMap::COL_DELETED_AT => 13, PostPoiTableMap::COL_CREATED_AT => 14, PostPoiTableMap::COL_UPDATED_AT => 15, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'post_id' => 1, 'identifier' => 2, 'type' => 3, 'lat' => 4, 'lng' => 5, 'radius' => 6, 'uuid' => 7, 'major' => 8, 'minor' => 9, 'distance' => 10, 'pin' => 11, 'poi_notification' => 12, 'deleted_at' => 13, 'created_at' => 14, 'updated_at' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('post_poi');
        $this->setPhpName('PostPoi');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\PostPoi');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('post_id', 'PostId', 'INTEGER', 'post', 'id', false, null, null);
        $this->addColumn('identifier', 'Identifier', 'VARCHAR', true, 20, null);
        $this->addColumn('type', 'Type', 'VARCHAR', false, 20, null);
        $this->addColumn('lat', 'Lat', 'FLOAT', false, 10, null);
        $this->addColumn('lng', 'Lng', 'FLOAT', false, 10, null);
        $this->addColumn('radius', 'Radius', 'INTEGER', false, null, null);
        $this->addColumn('uuid', 'Uuid', 'VARCHAR', false, 255, null);
        $this->addColumn('major', 'Major', 'SMALLINT', false, 1, null);
        $this->addColumn('minor', 'Minor', 'SMALLINT', false, 1, null);
        $this->addColumn('distance', 'Distance', 'VARCHAR', false, 10, null);
        $this->addColumn('pin', 'Pin', 'VARCHAR', false, 255, null);
        $this->addColumn('poi_notification', 'PoiNotification', 'BOOLEAN', false, 1, false);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Post', '\\Database\\HubPlus\\Post', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':post_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PostPoiTableMap::CLASS_DEFAULT : PostPoiTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PostPoi object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PostPoiTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PostPoiTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PostPoiTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PostPoiTableMap::OM_CLASS;
            /** @var PostPoi $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PostPoiTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PostPoiTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PostPoiTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PostPoi $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PostPoiTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PostPoiTableMap::COL_ID);
            $criteria->addSelectColumn(PostPoiTableMap::COL_POST_ID);
            $criteria->addSelectColumn(PostPoiTableMap::COL_IDENTIFIER);
            $criteria->addSelectColumn(PostPoiTableMap::COL_TYPE);
            $criteria->addSelectColumn(PostPoiTableMap::COL_LAT);
            $criteria->addSelectColumn(PostPoiTableMap::COL_LNG);
            $criteria->addSelectColumn(PostPoiTableMap::COL_RADIUS);
            $criteria->addSelectColumn(PostPoiTableMap::COL_UUID);
            $criteria->addSelectColumn(PostPoiTableMap::COL_MAJOR);
            $criteria->addSelectColumn(PostPoiTableMap::COL_MINOR);
            $criteria->addSelectColumn(PostPoiTableMap::COL_DISTANCE);
            $criteria->addSelectColumn(PostPoiTableMap::COL_PIN);
            $criteria->addSelectColumn(PostPoiTableMap::COL_POI_NOTIFICATION);
            $criteria->addSelectColumn(PostPoiTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(PostPoiTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(PostPoiTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.post_id');
            $criteria->addSelectColumn($alias . '.identifier');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.lat');
            $criteria->addSelectColumn($alias . '.lng');
            $criteria->addSelectColumn($alias . '.radius');
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.major');
            $criteria->addSelectColumn($alias . '.minor');
            $criteria->addSelectColumn($alias . '.distance');
            $criteria->addSelectColumn($alias . '.pin');
            $criteria->addSelectColumn($alias . '.poi_notification');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PostPoiTableMap::DATABASE_NAME)->getTable(PostPoiTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PostPoiTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PostPoiTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PostPoiTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PostPoi or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PostPoi object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostPoiTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\PostPoi) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PostPoiTableMap::DATABASE_NAME);
            $criteria->add(PostPoiTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PostPoiQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PostPoiTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PostPoiTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the post_poi table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PostPoiQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PostPoi or Criteria object.
     *
     * @param mixed               $criteria Criteria or PostPoi object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PostPoiTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PostPoi object
        }

        if ($criteria->containsKey(PostPoiTableMap::COL_ID) && $criteria->keyContainsValue(PostPoiTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PostPoiTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PostPoiQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PostPoiTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PostPoiTableMap::buildTableMap();
