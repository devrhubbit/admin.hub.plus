<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\RemoteProviderArchive;
use Database\HubPlus\RemoteProviderArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'remote_provider_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class RemoteProviderArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.RemoteProviderArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'remote_provider_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\RemoteProviderArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.RemoteProviderArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'remote_provider_archive.id';

    /**
     * the column name for the uuid field
     */
    const COL_UUID = 'remote_provider_archive.uuid';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'remote_provider_archive.email';

    /**
     * the column name for the email_canonical field
     */
    const COL_EMAIL_CANONICAL = 'remote_provider_archive.email_canonical';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'remote_provider_archive.name';

    /**
     * the column name for the slug field
     */
    const COL_SLUG = 'remote_provider_archive.slug';

    /**
     * the column name for the icon field
     */
    const COL_ICON = 'remote_provider_archive.icon';

    /**
     * the column name for the site field
     */
    const COL_SITE = 'remote_provider_archive.site';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'remote_provider_archive.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'remote_provider_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'remote_provider_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'remote_provider_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Uuid', 'Email', 'EmailCanonical', 'Name', 'Slug', 'Icon', 'Site', 'DeletedAt', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'uuid', 'email', 'emailCanonical', 'name', 'slug', 'icon', 'site', 'deletedAt', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(RemoteProviderArchiveTableMap::COL_ID, RemoteProviderArchiveTableMap::COL_UUID, RemoteProviderArchiveTableMap::COL_EMAIL, RemoteProviderArchiveTableMap::COL_EMAIL_CANONICAL, RemoteProviderArchiveTableMap::COL_NAME, RemoteProviderArchiveTableMap::COL_SLUG, RemoteProviderArchiveTableMap::COL_ICON, RemoteProviderArchiveTableMap::COL_SITE, RemoteProviderArchiveTableMap::COL_DELETED_AT, RemoteProviderArchiveTableMap::COL_CREATED_AT, RemoteProviderArchiveTableMap::COL_UPDATED_AT, RemoteProviderArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'uuid', 'email', 'email_canonical', 'name', 'slug', 'icon', 'site', 'deleted_at', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Uuid' => 1, 'Email' => 2, 'EmailCanonical' => 3, 'Name' => 4, 'Slug' => 5, 'Icon' => 6, 'Site' => 7, 'DeletedAt' => 8, 'CreatedAt' => 9, 'UpdatedAt' => 10, 'ArchivedAt' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'uuid' => 1, 'email' => 2, 'emailCanonical' => 3, 'name' => 4, 'slug' => 5, 'icon' => 6, 'site' => 7, 'deletedAt' => 8, 'createdAt' => 9, 'updatedAt' => 10, 'archivedAt' => 11, ),
        self::TYPE_COLNAME       => array(RemoteProviderArchiveTableMap::COL_ID => 0, RemoteProviderArchiveTableMap::COL_UUID => 1, RemoteProviderArchiveTableMap::COL_EMAIL => 2, RemoteProviderArchiveTableMap::COL_EMAIL_CANONICAL => 3, RemoteProviderArchiveTableMap::COL_NAME => 4, RemoteProviderArchiveTableMap::COL_SLUG => 5, RemoteProviderArchiveTableMap::COL_ICON => 6, RemoteProviderArchiveTableMap::COL_SITE => 7, RemoteProviderArchiveTableMap::COL_DELETED_AT => 8, RemoteProviderArchiveTableMap::COL_CREATED_AT => 9, RemoteProviderArchiveTableMap::COL_UPDATED_AT => 10, RemoteProviderArchiveTableMap::COL_ARCHIVED_AT => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'uuid' => 1, 'email' => 2, 'email_canonical' => 3, 'name' => 4, 'slug' => 5, 'icon' => 6, 'site' => 7, 'deleted_at' => 8, 'created_at' => 9, 'updated_at' => 10, 'archived_at' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('remote_provider_archive');
        $this->setPhpName('RemoteProviderArchive');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\RemoteProviderArchive');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('uuid', 'Uuid', 'VARCHAR', true, 255, '');
        $this->addColumn('email', 'Email', 'VARCHAR', true, 255, '');
        $this->addColumn('email_canonical', 'EmailCanonical', 'VARCHAR', true, 255, '');
        $this->addColumn('name', 'Name', 'VARCHAR', true, 255, '');
        $this->addColumn('slug', 'Slug', 'VARCHAR', true, 255, '');
        $this->addColumn('icon', 'Icon', 'VARCHAR', false, 255, null);
        $this->addColumn('site', 'Site', 'VARCHAR', false, 255, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? RemoteProviderArchiveTableMap::CLASS_DEFAULT : RemoteProviderArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (RemoteProviderArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = RemoteProviderArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = RemoteProviderArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + RemoteProviderArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = RemoteProviderArchiveTableMap::OM_CLASS;
            /** @var RemoteProviderArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            RemoteProviderArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = RemoteProviderArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = RemoteProviderArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var RemoteProviderArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                RemoteProviderArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(RemoteProviderArchiveTableMap::COL_ID);
            $criteria->addSelectColumn(RemoteProviderArchiveTableMap::COL_UUID);
            $criteria->addSelectColumn(RemoteProviderArchiveTableMap::COL_EMAIL);
            $criteria->addSelectColumn(RemoteProviderArchiveTableMap::COL_EMAIL_CANONICAL);
            $criteria->addSelectColumn(RemoteProviderArchiveTableMap::COL_NAME);
            $criteria->addSelectColumn(RemoteProviderArchiveTableMap::COL_SLUG);
            $criteria->addSelectColumn(RemoteProviderArchiveTableMap::COL_ICON);
            $criteria->addSelectColumn(RemoteProviderArchiveTableMap::COL_SITE);
            $criteria->addSelectColumn(RemoteProviderArchiveTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(RemoteProviderArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(RemoteProviderArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(RemoteProviderArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.email_canonical');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.slug');
            $criteria->addSelectColumn($alias . '.icon');
            $criteria->addSelectColumn($alias . '.site');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(RemoteProviderArchiveTableMap::DATABASE_NAME)->getTable(RemoteProviderArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(RemoteProviderArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(RemoteProviderArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new RemoteProviderArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a RemoteProviderArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or RemoteProviderArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteProviderArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\RemoteProviderArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(RemoteProviderArchiveTableMap::DATABASE_NAME);
            $criteria->add(RemoteProviderArchiveTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = RemoteProviderArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            RemoteProviderArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                RemoteProviderArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the remote_provider_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return RemoteProviderArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a RemoteProviderArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or RemoteProviderArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteProviderArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from RemoteProviderArchive object
        }


        // Set the correct dbName
        $query = RemoteProviderArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // RemoteProviderArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
RemoteProviderArchiveTableMap::buildTableMap();
