<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\Device;
use Database\HubPlus\DeviceQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'device' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class DeviceTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.DeviceTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'device';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\Device';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.Device';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the id field
     */
    const COL_ID = 'device.id';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'device.user_id';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'device.status';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'device.type';

    /**
     * the column name for the os field
     */
    const COL_OS = 'device.os';

    /**
     * the column name for the os_version field
     */
    const COL_OS_VERSION = 'device.os_version';

    /**
     * the column name for the enable_notification field
     */
    const COL_ENABLE_NOTIFICATION = 'device.enable_notification';

    /**
     * the column name for the api_version field
     */
    const COL_API_VERSION = 'device.api_version';

    /**
     * the column name for the language field
     */
    const COL_LANGUAGE = 'device.language';

    /**
     * the column name for the demo field
     */
    const COL_DEMO = 'device.demo';

    /**
     * the column name for the token field
     */
    const COL_TOKEN = 'device.token';

    /**
     * the column name for the api_key field
     */
    const COL_API_KEY = 'device.api_key';

    /**
     * the column name for the ip_address field
     */
    const COL_IP_ADDRESS = 'device.ip_address';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'device.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'device.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'device.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'UserId', 'Status', 'Type', 'Os', 'OsVersion', 'EnableNotification', 'ApiVersion', 'Language', 'Demo', 'Token', 'ApiKey', 'IpAddress', 'DeletedAt', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'userId', 'status', 'type', 'os', 'osVersion', 'enableNotification', 'apiVersion', 'language', 'demo', 'token', 'apiKey', 'ipAddress', 'deletedAt', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(DeviceTableMap::COL_ID, DeviceTableMap::COL_USER_ID, DeviceTableMap::COL_STATUS, DeviceTableMap::COL_TYPE, DeviceTableMap::COL_OS, DeviceTableMap::COL_OS_VERSION, DeviceTableMap::COL_ENABLE_NOTIFICATION, DeviceTableMap::COL_API_VERSION, DeviceTableMap::COL_LANGUAGE, DeviceTableMap::COL_DEMO, DeviceTableMap::COL_TOKEN, DeviceTableMap::COL_API_KEY, DeviceTableMap::COL_IP_ADDRESS, DeviceTableMap::COL_DELETED_AT, DeviceTableMap::COL_CREATED_AT, DeviceTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'user_id', 'status', 'type', 'os', 'os_version', 'enable_notification', 'api_version', 'language', 'demo', 'token', 'api_key', 'ip_address', 'deleted_at', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'UserId' => 1, 'Status' => 2, 'Type' => 3, 'Os' => 4, 'OsVersion' => 5, 'EnableNotification' => 6, 'ApiVersion' => 7, 'Language' => 8, 'Demo' => 9, 'Token' => 10, 'ApiKey' => 11, 'IpAddress' => 12, 'DeletedAt' => 13, 'CreatedAt' => 14, 'UpdatedAt' => 15, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'userId' => 1, 'status' => 2, 'type' => 3, 'os' => 4, 'osVersion' => 5, 'enableNotification' => 6, 'apiVersion' => 7, 'language' => 8, 'demo' => 9, 'token' => 10, 'apiKey' => 11, 'ipAddress' => 12, 'deletedAt' => 13, 'createdAt' => 14, 'updatedAt' => 15, ),
        self::TYPE_COLNAME       => array(DeviceTableMap::COL_ID => 0, DeviceTableMap::COL_USER_ID => 1, DeviceTableMap::COL_STATUS => 2, DeviceTableMap::COL_TYPE => 3, DeviceTableMap::COL_OS => 4, DeviceTableMap::COL_OS_VERSION => 5, DeviceTableMap::COL_ENABLE_NOTIFICATION => 6, DeviceTableMap::COL_API_VERSION => 7, DeviceTableMap::COL_LANGUAGE => 8, DeviceTableMap::COL_DEMO => 9, DeviceTableMap::COL_TOKEN => 10, DeviceTableMap::COL_API_KEY => 11, DeviceTableMap::COL_IP_ADDRESS => 12, DeviceTableMap::COL_DELETED_AT => 13, DeviceTableMap::COL_CREATED_AT => 14, DeviceTableMap::COL_UPDATED_AT => 15, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'user_id' => 1, 'status' => 2, 'type' => 3, 'os' => 4, 'os_version' => 5, 'enable_notification' => 6, 'api_version' => 7, 'language' => 8, 'demo' => 9, 'token' => 10, 'api_key' => 11, 'ip_address' => 12, 'deleted_at' => 13, 'created_at' => 14, 'updated_at' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('device');
        $this->setPhpName('Device');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\Device');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'user_app', 'id', false, null, null);
        $this->addColumn('status', 'Status', 'VARCHAR', true, 20, null);
        $this->addColumn('type', 'Type', 'VARCHAR', true, 20, null);
        $this->addColumn('os', 'Os', 'CLOB', true, null, null);
        $this->addColumn('os_version', 'OsVersion', 'CLOB', true, null, null);
        $this->addColumn('enable_notification', 'EnableNotification', 'BOOLEAN', false, 1, null);
        $this->addColumn('api_version', 'ApiVersion', 'VARCHAR', false, 50, null);
        $this->addColumn('language', 'Language', 'VARCHAR', false, 10, null);
        $this->addColumn('demo', 'Demo', 'BOOLEAN', false, 1, null);
        $this->addColumn('token', 'Token', 'CLOB', true, null, null);
        $this->addColumn('api_key', 'ApiKey', 'VARCHAR', false, 255, null);
        $this->addColumn('ip_address', 'IpAddress', 'VARCHAR', false, 50, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('UserApp', '\\Database\\HubPlus\\UserApp', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('CustomFormExtraLog', '\\Database\\HubPlus\\CustomFormExtraLog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':device_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'CustomFormExtraLogs', false);
        $this->addRelation('Media', '\\Database\\HubPlus\\Media', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':device_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'Medias', false);
        $this->addRelation('MediaExtraLog', '\\Database\\HubPlus\\MediaExtraLog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':device_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'MediaExtraLogs', false);
        $this->addRelation('PostExtraLog', '\\Database\\HubPlus\\PostExtraLog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':device_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'PostExtraLogs', false);
        $this->addRelation('PushNotificationDevice', '\\Database\\HubPlus\\PushNotificationDevice', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':device_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'PushNotificationDevices', false);
        $this->addRelation('RemoteExtraLog', '\\Database\\HubPlus\\RemoteExtraLog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':device_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'RemoteExtraLogs', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to device     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CustomFormExtraLogTableMap::clearInstancePool();
        MediaTableMap::clearInstancePool();
        MediaExtraLogTableMap::clearInstancePool();
        PostExtraLogTableMap::clearInstancePool();
        PushNotificationDeviceTableMap::clearInstancePool();
        RemoteExtraLogTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? DeviceTableMap::CLASS_DEFAULT : DeviceTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Device object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = DeviceTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = DeviceTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + DeviceTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = DeviceTableMap::OM_CLASS;
            /** @var Device $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            DeviceTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = DeviceTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = DeviceTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Device $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                DeviceTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(DeviceTableMap::COL_ID);
            $criteria->addSelectColumn(DeviceTableMap::COL_USER_ID);
            $criteria->addSelectColumn(DeviceTableMap::COL_STATUS);
            $criteria->addSelectColumn(DeviceTableMap::COL_TYPE);
            $criteria->addSelectColumn(DeviceTableMap::COL_OS);
            $criteria->addSelectColumn(DeviceTableMap::COL_OS_VERSION);
            $criteria->addSelectColumn(DeviceTableMap::COL_ENABLE_NOTIFICATION);
            $criteria->addSelectColumn(DeviceTableMap::COL_API_VERSION);
            $criteria->addSelectColumn(DeviceTableMap::COL_LANGUAGE);
            $criteria->addSelectColumn(DeviceTableMap::COL_DEMO);
            $criteria->addSelectColumn(DeviceTableMap::COL_TOKEN);
            $criteria->addSelectColumn(DeviceTableMap::COL_API_KEY);
            $criteria->addSelectColumn(DeviceTableMap::COL_IP_ADDRESS);
            $criteria->addSelectColumn(DeviceTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(DeviceTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(DeviceTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.os');
            $criteria->addSelectColumn($alias . '.os_version');
            $criteria->addSelectColumn($alias . '.enable_notification');
            $criteria->addSelectColumn($alias . '.api_version');
            $criteria->addSelectColumn($alias . '.language');
            $criteria->addSelectColumn($alias . '.demo');
            $criteria->addSelectColumn($alias . '.token');
            $criteria->addSelectColumn($alias . '.api_key');
            $criteria->addSelectColumn($alias . '.ip_address');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(DeviceTableMap::DATABASE_NAME)->getTable(DeviceTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(DeviceTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(DeviceTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new DeviceTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Device or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Device object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\Device) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(DeviceTableMap::DATABASE_NAME);
            $criteria->add(DeviceTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = DeviceQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            DeviceTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                DeviceTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the device table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return DeviceQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Device or Criteria object.
     *
     * @param mixed               $criteria Criteria or Device object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DeviceTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Device object
        }

        if ($criteria->containsKey(DeviceTableMap::COL_ID) && $criteria->keyContainsValue(DeviceTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.DeviceTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = DeviceQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // DeviceTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
DeviceTableMap::buildTableMap();
