<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\Webhook;
use Database\HubPlus\WebhookQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'webhook' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class WebhookTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.WebhookTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'webhook';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\Webhook';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.Webhook';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id field
     */
    const COL_ID = 'webhook.id';

    /**
     * the column name for the bind_to field
     */
    const COL_BIND_TO = 'webhook.bind_to';

    /**
     * the column name for the hash_id field
     */
    const COL_HASH_ID = 'webhook.hash_id';

    /**
     * the column name for the url field
     */
    const COL_URL = 'webhook.url';

    /**
     * the column name for the provider_id field
     */
    const COL_PROVIDER_ID = 'webhook.provider_id';

    /**
     * the column name for the section_id field
     */
    const COL_SECTION_ID = 'webhook.section_id';

    /**
     * the column name for the post_id field
     */
    const COL_POST_ID = 'webhook.post_id';

    /**
     * the column name for the seconds_delay field
     */
    const COL_SECONDS_DELAY = 'webhook.seconds_delay';

    /**
     * the column name for the max_attempts field
     */
    const COL_MAX_ATTEMPTS = 'webhook.max_attempts';

    /**
     * the column name for the enabled field
     */
    const COL_ENABLED = 'webhook.enabled';

    /**
     * the column name for the last_trigger_at field
     */
    const COL_LAST_TRIGGER_AT = 'webhook.last_trigger_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'webhook.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'webhook.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'BindTo', 'HashId', 'Url', 'ProviderId', 'SectionId', 'PostId', 'SecondsDelay', 'MaxAttempts', 'Enabled', 'LastTriggerAt', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'bindTo', 'hashId', 'url', 'providerId', 'sectionId', 'postId', 'secondsDelay', 'maxAttempts', 'enabled', 'lastTriggerAt', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(WebhookTableMap::COL_ID, WebhookTableMap::COL_BIND_TO, WebhookTableMap::COL_HASH_ID, WebhookTableMap::COL_URL, WebhookTableMap::COL_PROVIDER_ID, WebhookTableMap::COL_SECTION_ID, WebhookTableMap::COL_POST_ID, WebhookTableMap::COL_SECONDS_DELAY, WebhookTableMap::COL_MAX_ATTEMPTS, WebhookTableMap::COL_ENABLED, WebhookTableMap::COL_LAST_TRIGGER_AT, WebhookTableMap::COL_CREATED_AT, WebhookTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'bind_to', 'hash_id', 'url', 'provider_id', 'section_id', 'post_id', 'seconds_delay', 'max_attempts', 'enabled', 'last_trigger_at', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'BindTo' => 1, 'HashId' => 2, 'Url' => 3, 'ProviderId' => 4, 'SectionId' => 5, 'PostId' => 6, 'SecondsDelay' => 7, 'MaxAttempts' => 8, 'Enabled' => 9, 'LastTriggerAt' => 10, 'CreatedAt' => 11, 'UpdatedAt' => 12, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'bindTo' => 1, 'hashId' => 2, 'url' => 3, 'providerId' => 4, 'sectionId' => 5, 'postId' => 6, 'secondsDelay' => 7, 'maxAttempts' => 8, 'enabled' => 9, 'lastTriggerAt' => 10, 'createdAt' => 11, 'updatedAt' => 12, ),
        self::TYPE_COLNAME       => array(WebhookTableMap::COL_ID => 0, WebhookTableMap::COL_BIND_TO => 1, WebhookTableMap::COL_HASH_ID => 2, WebhookTableMap::COL_URL => 3, WebhookTableMap::COL_PROVIDER_ID => 4, WebhookTableMap::COL_SECTION_ID => 5, WebhookTableMap::COL_POST_ID => 6, WebhookTableMap::COL_SECONDS_DELAY => 7, WebhookTableMap::COL_MAX_ATTEMPTS => 8, WebhookTableMap::COL_ENABLED => 9, WebhookTableMap::COL_LAST_TRIGGER_AT => 10, WebhookTableMap::COL_CREATED_AT => 11, WebhookTableMap::COL_UPDATED_AT => 12, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'bind_to' => 1, 'hash_id' => 2, 'url' => 3, 'provider_id' => 4, 'section_id' => 5, 'post_id' => 6, 'seconds_delay' => 7, 'max_attempts' => 8, 'enabled' => 9, 'last_trigger_at' => 10, 'created_at' => 11, 'updated_at' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('webhook');
        $this->setPhpName('Webhook');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\Webhook');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('bind_to', 'BindTo', 'LONGVARCHAR', true, null, null);
        $this->addColumn('hash_id', 'HashId', 'VARCHAR', true, 255, null);
        $this->addColumn('url', 'Url', 'VARCHAR', true, 2024, null);
        $this->addForeignKey('provider_id', 'ProviderId', 'INTEGER', 'remote_provider', 'id', true, null, null);
        $this->addForeignKey('section_id', 'SectionId', 'INTEGER', 'section', 'id', false, null, null);
        $this->addForeignKey('post_id', 'PostId', 'INTEGER', 'post', 'id', false, null, null);
        $this->addColumn('seconds_delay', 'SecondsDelay', 'INTEGER', true, null, 0);
        $this->addColumn('max_attempts', 'MaxAttempts', 'TINYINT', true, 1, 1);
        $this->addColumn('enabled', 'Enabled', 'TINYINT', true, 1, 1);
        $this->addColumn('last_trigger_at', 'LastTriggerAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RemoteProvider', '\\Database\\HubPlus\\RemoteProvider', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':provider_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Section', '\\Database\\HubPlus\\Section', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':section_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Post', '\\Database\\HubPlus\\Post', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':post_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('WebhookQueue', '\\Database\\HubPlus\\WebhookQueue', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':webhook_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'WebhookQueues', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to webhook     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        WebhookQueueTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? WebhookTableMap::CLASS_DEFAULT : WebhookTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Webhook object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = WebhookTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = WebhookTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + WebhookTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = WebhookTableMap::OM_CLASS;
            /** @var Webhook $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            WebhookTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = WebhookTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = WebhookTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Webhook $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                WebhookTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(WebhookTableMap::COL_ID);
            $criteria->addSelectColumn(WebhookTableMap::COL_BIND_TO);
            $criteria->addSelectColumn(WebhookTableMap::COL_HASH_ID);
            $criteria->addSelectColumn(WebhookTableMap::COL_URL);
            $criteria->addSelectColumn(WebhookTableMap::COL_PROVIDER_ID);
            $criteria->addSelectColumn(WebhookTableMap::COL_SECTION_ID);
            $criteria->addSelectColumn(WebhookTableMap::COL_POST_ID);
            $criteria->addSelectColumn(WebhookTableMap::COL_SECONDS_DELAY);
            $criteria->addSelectColumn(WebhookTableMap::COL_MAX_ATTEMPTS);
            $criteria->addSelectColumn(WebhookTableMap::COL_ENABLED);
            $criteria->addSelectColumn(WebhookTableMap::COL_LAST_TRIGGER_AT);
            $criteria->addSelectColumn(WebhookTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(WebhookTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.bind_to');
            $criteria->addSelectColumn($alias . '.hash_id');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.provider_id');
            $criteria->addSelectColumn($alias . '.section_id');
            $criteria->addSelectColumn($alias . '.post_id');
            $criteria->addSelectColumn($alias . '.seconds_delay');
            $criteria->addSelectColumn($alias . '.max_attempts');
            $criteria->addSelectColumn($alias . '.enabled');
            $criteria->addSelectColumn($alias . '.last_trigger_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(WebhookTableMap::DATABASE_NAME)->getTable(WebhookTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(WebhookTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(WebhookTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new WebhookTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Webhook or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Webhook object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WebhookTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\Webhook) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(WebhookTableMap::DATABASE_NAME);
            $criteria->add(WebhookTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = WebhookQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            WebhookTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                WebhookTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the webhook table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return WebhookQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Webhook or Criteria object.
     *
     * @param mixed               $criteria Criteria or Webhook object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WebhookTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Webhook object
        }

        if ($criteria->containsKey(WebhookTableMap::COL_ID) && $criteria->keyContainsValue(WebhookTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.WebhookTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = WebhookQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // WebhookTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
WebhookTableMap::buildTableMap();
