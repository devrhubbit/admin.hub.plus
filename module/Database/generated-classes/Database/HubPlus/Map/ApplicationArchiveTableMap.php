<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\ApplicationArchive;
use Database\HubPlus\ApplicationArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'application_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApplicationArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.ApplicationArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'application_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\ApplicationArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.ApplicationArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 46;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 46;

    /**
     * the column name for the id field
     */
    const COL_ID = 'application_archive.id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'application_archive.title';

    /**
     * the column name for the slug field
     */
    const COL_SLUG = 'application_archive.slug';

    /**
     * the column name for the bundle field
     */
    const COL_BUNDLE = 'application_archive.bundle';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'application_archive.description';

    /**
     * the column name for the app_key field
     */
    const COL_APP_KEY = 'application_archive.app_key';

    /**
     * the column name for the dsn field
     */
    const COL_DSN = 'application_archive.dsn';

    /**
     * the column name for the db_host field
     */
    const COL_DB_HOST = 'application_archive.db_host';

    /**
     * the column name for the db_name field
     */
    const COL_DB_NAME = 'application_archive.db_name';

    /**
     * the column name for the db_user field
     */
    const COL_DB_USER = 'application_archive.db_user';

    /**
     * the column name for the db_pwd field
     */
    const COL_DB_PWD = 'application_archive.db_pwd';

    /**
     * the column name for the api_version field
     */
    const COL_API_VERSION = 'application_archive.api_version';

    /**
     * the column name for the pack_id field
     */
    const COL_PACK_ID = 'application_archive.pack_id';

    /**
     * the column name for the category_id field
     */
    const COL_CATEGORY_ID = 'application_archive.category_id';

    /**
     * the column name for the template_id field
     */
    const COL_TEMPLATE_ID = 'application_archive.template_id';

    /**
     * the column name for the push_notification field
     */
    const COL_PUSH_NOTIFICATION = 'application_archive.push_notification';

    /**
     * the column name for the max_notification field
     */
    const COL_MAX_NOTIFICATION = 'application_archive.max_notification';

    /**
     * the column name for the max_advice_hour field
     */
    const COL_MAX_ADVICE_HOUR = 'application_archive.max_advice_hour';

    /**
     * the column name for the advice_hour_done field
     */
    const COL_ADVICE_HOUR_DONE = 'application_archive.advice_hour_done';

    /**
     * the column name for the max_content_insert field
     */
    const COL_MAX_CONTENT_INSERT = 'application_archive.max_content_insert';

    /**
     * the column name for the icon field
     */
    const COL_ICON = 'application_archive.icon';

    /**
     * the column name for the splash_screen field
     */
    const COL_SPLASH_SCREEN = 'application_archive.splash_screen';

    /**
     * the column name for the background_color field
     */
    const COL_BACKGROUND_COLOR = 'application_archive.background_color';

    /**
     * the column name for the app_logo field
     */
    const COL_APP_LOGO = 'application_archive.app_logo';

    /**
     * the column name for the layout field
     */
    const COL_LAYOUT = 'application_archive.layout';

    /**
     * the column name for the white_label field
     */
    const COL_WHITE_LABEL = 'application_archive.white_label';

    /**
     * the column name for the demo field
     */
    const COL_DEMO = 'application_archive.demo';

    /**
     * the column name for the facebook_token field
     */
    const COL_FACEBOOK_TOKEN = 'application_archive.facebook_token';

    /**
     * the column name for the apple_id field
     */
    const COL_APPLE_ID = 'application_archive.apple_id';

    /**
     * the column name for the apple_id_password field
     */
    const COL_APPLE_ID_PASSWORD = 'application_archive.apple_id_password';

    /**
     * the column name for the apple_store_app_link field
     */
    const COL_APPLE_STORE_APP_LINK = 'application_archive.apple_store_app_link';

    /**
     * the column name for the play_store_id field
     */
    const COL_PLAY_STORE_ID = 'application_archive.play_store_id';

    /**
     * the column name for the play_store_id_password field
     */
    const COL_PLAY_STORE_ID_PASSWORD = 'application_archive.play_store_id_password';

    /**
     * the column name for the play_store_app_link field
     */
    const COL_PLAY_STORE_APP_LINK = 'application_archive.play_store_app_link';

    /**
     * the column name for the controller_uri field
     */
    const COL_CONTROLLER_URI = 'application_archive.controller_uri';

    /**
     * the column name for the google_analytics_ua field
     */
    const COL_GOOGLE_ANALYTICS_UA = 'application_archive.google_analytics_ua';

    /**
     * the column name for the published field
     */
    const COL_PUBLISHED = 'application_archive.published';

    /**
     * the column name for the expired_at field
     */
    const COL_EXPIRED_AT = 'application_archive.expired_at';

    /**
     * the column name for the main_contents_language field
     */
    const COL_MAIN_CONTENTS_LANGUAGE = 'application_archive.main_contents_language';

    /**
     * the column name for the other_contents_languages field
     */
    const COL_OTHER_CONTENTS_LANGUAGES = 'application_archive.other_contents_languages';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'application_archive.deleted_at';

    /**
     * the column name for the system_icons field
     */
    const COL_SYSTEM_ICONS = 'application_archive.system_icons';

    /**
     * the column name for the toolbar field
     */
    const COL_TOOLBAR = 'application_archive.toolbar';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'application_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'application_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'application_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Title', 'Slug', 'Bundle', 'Description', 'AppKey', 'Dsn', 'DbHost', 'DbName', 'DbUser', 'DbPwd', 'ApiVersion', 'PackId', 'CategoryId', 'TemplateId', 'PushNotification', 'MaxNotification', 'MaxAdviceHour', 'AdviceHourDone', 'MaxContentInsert', 'Icon', 'SplashScreen', 'BackgroundColor', 'AppLogo', 'Layout', 'WhiteLabel', 'Demo', 'FacebookToken', 'AppleId', 'AppleIdPassword', 'AppleStoreAppLink', 'PlayStoreId', 'PlayStoreIdPassword', 'PlayStoreAppLink', 'ControllerUri', 'GoogleAnalyticsUa', 'Published', 'ExpiredAt', 'MainContentsLanguage', 'OtherContentsLanguages', 'DeletedAt', 'SystemIcons', 'Toolbar', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'title', 'slug', 'bundle', 'description', 'appKey', 'dsn', 'dbHost', 'dbName', 'dbUser', 'dbPwd', 'apiVersion', 'packId', 'categoryId', 'templateId', 'pushNotification', 'maxNotification', 'maxAdviceHour', 'adviceHourDone', 'maxContentInsert', 'icon', 'splashScreen', 'backgroundColor', 'appLogo', 'layout', 'whiteLabel', 'demo', 'facebookToken', 'appleId', 'appleIdPassword', 'appleStoreAppLink', 'playStoreId', 'playStoreIdPassword', 'playStoreAppLink', 'controllerUri', 'googleAnalyticsUa', 'published', 'expiredAt', 'mainContentsLanguage', 'otherContentsLanguages', 'deletedAt', 'systemIcons', 'toolbar', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(ApplicationArchiveTableMap::COL_ID, ApplicationArchiveTableMap::COL_TITLE, ApplicationArchiveTableMap::COL_SLUG, ApplicationArchiveTableMap::COL_BUNDLE, ApplicationArchiveTableMap::COL_DESCRIPTION, ApplicationArchiveTableMap::COL_APP_KEY, ApplicationArchiveTableMap::COL_DSN, ApplicationArchiveTableMap::COL_DB_HOST, ApplicationArchiveTableMap::COL_DB_NAME, ApplicationArchiveTableMap::COL_DB_USER, ApplicationArchiveTableMap::COL_DB_PWD, ApplicationArchiveTableMap::COL_API_VERSION, ApplicationArchiveTableMap::COL_PACK_ID, ApplicationArchiveTableMap::COL_CATEGORY_ID, ApplicationArchiveTableMap::COL_TEMPLATE_ID, ApplicationArchiveTableMap::COL_PUSH_NOTIFICATION, ApplicationArchiveTableMap::COL_MAX_NOTIFICATION, ApplicationArchiveTableMap::COL_MAX_ADVICE_HOUR, ApplicationArchiveTableMap::COL_ADVICE_HOUR_DONE, ApplicationArchiveTableMap::COL_MAX_CONTENT_INSERT, ApplicationArchiveTableMap::COL_ICON, ApplicationArchiveTableMap::COL_SPLASH_SCREEN, ApplicationArchiveTableMap::COL_BACKGROUND_COLOR, ApplicationArchiveTableMap::COL_APP_LOGO, ApplicationArchiveTableMap::COL_LAYOUT, ApplicationArchiveTableMap::COL_WHITE_LABEL, ApplicationArchiveTableMap::COL_DEMO, ApplicationArchiveTableMap::COL_FACEBOOK_TOKEN, ApplicationArchiveTableMap::COL_APPLE_ID, ApplicationArchiveTableMap::COL_APPLE_ID_PASSWORD, ApplicationArchiveTableMap::COL_APPLE_STORE_APP_LINK, ApplicationArchiveTableMap::COL_PLAY_STORE_ID, ApplicationArchiveTableMap::COL_PLAY_STORE_ID_PASSWORD, ApplicationArchiveTableMap::COL_PLAY_STORE_APP_LINK, ApplicationArchiveTableMap::COL_CONTROLLER_URI, ApplicationArchiveTableMap::COL_GOOGLE_ANALYTICS_UA, ApplicationArchiveTableMap::COL_PUBLISHED, ApplicationArchiveTableMap::COL_EXPIRED_AT, ApplicationArchiveTableMap::COL_MAIN_CONTENTS_LANGUAGE, ApplicationArchiveTableMap::COL_OTHER_CONTENTS_LANGUAGES, ApplicationArchiveTableMap::COL_DELETED_AT, ApplicationArchiveTableMap::COL_SYSTEM_ICONS, ApplicationArchiveTableMap::COL_TOOLBAR, ApplicationArchiveTableMap::COL_CREATED_AT, ApplicationArchiveTableMap::COL_UPDATED_AT, ApplicationArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'title', 'slug', 'bundle', 'description', 'app_key', 'dsn', 'db_host', 'db_name', 'db_user', 'db_pwd', 'api_version', 'pack_id', 'category_id', 'template_id', 'push_notification', 'max_notification', 'max_advice_hour', 'advice_hour_done', 'max_content_insert', 'icon', 'splash_screen', 'background_color', 'app_logo', 'layout', 'white_label', 'demo', 'facebook_token', 'apple_id', 'apple_id_password', 'apple_store_app_link', 'play_store_id', 'play_store_id_password', 'play_store_app_link', 'controller_uri', 'google_analytics_ua', 'published', 'expired_at', 'main_contents_language', 'other_contents_languages', 'deleted_at', 'system_icons', 'toolbar', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Title' => 1, 'Slug' => 2, 'Bundle' => 3, 'Description' => 4, 'AppKey' => 5, 'Dsn' => 6, 'DbHost' => 7, 'DbName' => 8, 'DbUser' => 9, 'DbPwd' => 10, 'ApiVersion' => 11, 'PackId' => 12, 'CategoryId' => 13, 'TemplateId' => 14, 'PushNotification' => 15, 'MaxNotification' => 16, 'MaxAdviceHour' => 17, 'AdviceHourDone' => 18, 'MaxContentInsert' => 19, 'Icon' => 20, 'SplashScreen' => 21, 'BackgroundColor' => 22, 'AppLogo' => 23, 'Layout' => 24, 'WhiteLabel' => 25, 'Demo' => 26, 'FacebookToken' => 27, 'AppleId' => 28, 'AppleIdPassword' => 29, 'AppleStoreAppLink' => 30, 'PlayStoreId' => 31, 'PlayStoreIdPassword' => 32, 'PlayStoreAppLink' => 33, 'ControllerUri' => 34, 'GoogleAnalyticsUa' => 35, 'Published' => 36, 'ExpiredAt' => 37, 'MainContentsLanguage' => 38, 'OtherContentsLanguages' => 39, 'DeletedAt' => 40, 'SystemIcons' => 41, 'Toolbar' => 42, 'CreatedAt' => 43, 'UpdatedAt' => 44, 'ArchivedAt' => 45, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'title' => 1, 'slug' => 2, 'bundle' => 3, 'description' => 4, 'appKey' => 5, 'dsn' => 6, 'dbHost' => 7, 'dbName' => 8, 'dbUser' => 9, 'dbPwd' => 10, 'apiVersion' => 11, 'packId' => 12, 'categoryId' => 13, 'templateId' => 14, 'pushNotification' => 15, 'maxNotification' => 16, 'maxAdviceHour' => 17, 'adviceHourDone' => 18, 'maxContentInsert' => 19, 'icon' => 20, 'splashScreen' => 21, 'backgroundColor' => 22, 'appLogo' => 23, 'layout' => 24, 'whiteLabel' => 25, 'demo' => 26, 'facebookToken' => 27, 'appleId' => 28, 'appleIdPassword' => 29, 'appleStoreAppLink' => 30, 'playStoreId' => 31, 'playStoreIdPassword' => 32, 'playStoreAppLink' => 33, 'controllerUri' => 34, 'googleAnalyticsUa' => 35, 'published' => 36, 'expiredAt' => 37, 'mainContentsLanguage' => 38, 'otherContentsLanguages' => 39, 'deletedAt' => 40, 'systemIcons' => 41, 'toolbar' => 42, 'createdAt' => 43, 'updatedAt' => 44, 'archivedAt' => 45, ),
        self::TYPE_COLNAME       => array(ApplicationArchiveTableMap::COL_ID => 0, ApplicationArchiveTableMap::COL_TITLE => 1, ApplicationArchiveTableMap::COL_SLUG => 2, ApplicationArchiveTableMap::COL_BUNDLE => 3, ApplicationArchiveTableMap::COL_DESCRIPTION => 4, ApplicationArchiveTableMap::COL_APP_KEY => 5, ApplicationArchiveTableMap::COL_DSN => 6, ApplicationArchiveTableMap::COL_DB_HOST => 7, ApplicationArchiveTableMap::COL_DB_NAME => 8, ApplicationArchiveTableMap::COL_DB_USER => 9, ApplicationArchiveTableMap::COL_DB_PWD => 10, ApplicationArchiveTableMap::COL_API_VERSION => 11, ApplicationArchiveTableMap::COL_PACK_ID => 12, ApplicationArchiveTableMap::COL_CATEGORY_ID => 13, ApplicationArchiveTableMap::COL_TEMPLATE_ID => 14, ApplicationArchiveTableMap::COL_PUSH_NOTIFICATION => 15, ApplicationArchiveTableMap::COL_MAX_NOTIFICATION => 16, ApplicationArchiveTableMap::COL_MAX_ADVICE_HOUR => 17, ApplicationArchiveTableMap::COL_ADVICE_HOUR_DONE => 18, ApplicationArchiveTableMap::COL_MAX_CONTENT_INSERT => 19, ApplicationArchiveTableMap::COL_ICON => 20, ApplicationArchiveTableMap::COL_SPLASH_SCREEN => 21, ApplicationArchiveTableMap::COL_BACKGROUND_COLOR => 22, ApplicationArchiveTableMap::COL_APP_LOGO => 23, ApplicationArchiveTableMap::COL_LAYOUT => 24, ApplicationArchiveTableMap::COL_WHITE_LABEL => 25, ApplicationArchiveTableMap::COL_DEMO => 26, ApplicationArchiveTableMap::COL_FACEBOOK_TOKEN => 27, ApplicationArchiveTableMap::COL_APPLE_ID => 28, ApplicationArchiveTableMap::COL_APPLE_ID_PASSWORD => 29, ApplicationArchiveTableMap::COL_APPLE_STORE_APP_LINK => 30, ApplicationArchiveTableMap::COL_PLAY_STORE_ID => 31, ApplicationArchiveTableMap::COL_PLAY_STORE_ID_PASSWORD => 32, ApplicationArchiveTableMap::COL_PLAY_STORE_APP_LINK => 33, ApplicationArchiveTableMap::COL_CONTROLLER_URI => 34, ApplicationArchiveTableMap::COL_GOOGLE_ANALYTICS_UA => 35, ApplicationArchiveTableMap::COL_PUBLISHED => 36, ApplicationArchiveTableMap::COL_EXPIRED_AT => 37, ApplicationArchiveTableMap::COL_MAIN_CONTENTS_LANGUAGE => 38, ApplicationArchiveTableMap::COL_OTHER_CONTENTS_LANGUAGES => 39, ApplicationArchiveTableMap::COL_DELETED_AT => 40, ApplicationArchiveTableMap::COL_SYSTEM_ICONS => 41, ApplicationArchiveTableMap::COL_TOOLBAR => 42, ApplicationArchiveTableMap::COL_CREATED_AT => 43, ApplicationArchiveTableMap::COL_UPDATED_AT => 44, ApplicationArchiveTableMap::COL_ARCHIVED_AT => 45, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'title' => 1, 'slug' => 2, 'bundle' => 3, 'description' => 4, 'app_key' => 5, 'dsn' => 6, 'db_host' => 7, 'db_name' => 8, 'db_user' => 9, 'db_pwd' => 10, 'api_version' => 11, 'pack_id' => 12, 'category_id' => 13, 'template_id' => 14, 'push_notification' => 15, 'max_notification' => 16, 'max_advice_hour' => 17, 'advice_hour_done' => 18, 'max_content_insert' => 19, 'icon' => 20, 'splash_screen' => 21, 'background_color' => 22, 'app_logo' => 23, 'layout' => 24, 'white_label' => 25, 'demo' => 26, 'facebook_token' => 27, 'apple_id' => 28, 'apple_id_password' => 29, 'apple_store_app_link' => 30, 'play_store_id' => 31, 'play_store_id_password' => 32, 'play_store_app_link' => 33, 'controller_uri' => 34, 'google_analytics_ua' => 35, 'published' => 36, 'expired_at' => 37, 'main_contents_language' => 38, 'other_contents_languages' => 39, 'deleted_at' => 40, 'system_icons' => 41, 'toolbar' => 42, 'created_at' => 43, 'updated_at' => 44, 'archived_at' => 45, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('application_archive');
        $this->setPhpName('ApplicationArchive');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\ApplicationArchive');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 255, null);
        $this->addColumn('slug', 'Slug', 'VARCHAR', false, 255, null);
        $this->addColumn('bundle', 'Bundle', 'VARCHAR', false, 255, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('app_key', 'AppKey', 'VARCHAR', false, 255, null);
        $this->addColumn('dsn', 'Dsn', 'VARCHAR', false, 255, null);
        $this->addColumn('db_host', 'DbHost', 'VARCHAR', false, 255, null);
        $this->addColumn('db_name', 'DbName', 'VARCHAR', false, 255, null);
        $this->addColumn('db_user', 'DbUser', 'VARCHAR', false, 255, null);
        $this->addColumn('db_pwd', 'DbPwd', 'VARCHAR', false, 255, null);
        $this->addColumn('api_version', 'ApiVersion', 'VARCHAR', false, 10, null);
        $this->addColumn('pack_id', 'PackId', 'INTEGER', false, null, null);
        $this->addColumn('category_id', 'CategoryId', 'INTEGER', false, null, null);
        $this->addColumn('template_id', 'TemplateId', 'INTEGER', false, null, null);
        $this->addColumn('push_notification', 'PushNotification', 'LONGVARCHAR', false, null, null);
        $this->addColumn('max_notification', 'MaxNotification', 'INTEGER', false, null, 0);
        $this->addColumn('max_advice_hour', 'MaxAdviceHour', 'INTEGER', false, null, 0);
        $this->addColumn('advice_hour_done', 'AdviceHourDone', 'INTEGER', false, null, 0);
        $this->addColumn('max_content_insert', 'MaxContentInsert', 'INTEGER', false, null, 0);
        $this->addColumn('icon', 'Icon', 'VARCHAR', false, 255, null);
        $this->addColumn('splash_screen', 'SplashScreen', 'VARCHAR', false, 255, null);
        $this->addColumn('background_color', 'BackgroundColor', 'VARCHAR', false, 7, null);
        $this->addColumn('app_logo', 'AppLogo', 'VARCHAR', false, 255, null);
        $this->addColumn('layout', 'Layout', 'LONGVARCHAR', false, null, null);
        $this->addColumn('white_label', 'WhiteLabel', 'BOOLEAN', false, 1, null);
        $this->addColumn('demo', 'Demo', 'BOOLEAN', false, 1, null);
        $this->addColumn('facebook_token', 'FacebookToken', 'VARCHAR', false, 255, null);
        $this->addColumn('apple_id', 'AppleId', 'VARCHAR', false, 255, null);
        $this->addColumn('apple_id_password', 'AppleIdPassword', 'VARCHAR', false, 255, null);
        $this->addColumn('apple_store_app_link', 'AppleStoreAppLink', 'VARCHAR', false, 255, null);
        $this->addColumn('play_store_id', 'PlayStoreId', 'VARCHAR', false, 255, null);
        $this->addColumn('play_store_id_password', 'PlayStoreIdPassword', 'VARCHAR', false, 255, null);
        $this->addColumn('play_store_app_link', 'PlayStoreAppLink', 'VARCHAR', false, 255, null);
        $this->addColumn('controller_uri', 'ControllerUri', 'LONGVARCHAR', false, null, null);
        $this->addColumn('google_analytics_ua', 'GoogleAnalyticsUa', 'LONGVARCHAR', false, null, null);
        $this->addColumn('published', 'Published', 'TINYINT', true, 1, 0);
        $this->addColumn('expired_at', 'ExpiredAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('main_contents_language', 'MainContentsLanguage', 'VARCHAR', false, 2, null);
        $this->addColumn('other_contents_languages', 'OtherContentsLanguages', 'VARCHAR', false, 255, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('system_icons', 'SystemIcons', 'LONGVARCHAR', true, null, null);
        $this->addColumn('toolbar', 'Toolbar', 'CHAR', true, null, 'SLIDESHOW');
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApplicationArchiveTableMap::CLASS_DEFAULT : ApplicationArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ApplicationArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApplicationArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApplicationArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApplicationArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApplicationArchiveTableMap::OM_CLASS;
            /** @var ApplicationArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApplicationArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApplicationArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApplicationArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ApplicationArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApplicationArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_ID);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_TITLE);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_SLUG);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_BUNDLE);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_APP_KEY);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_DSN);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_DB_HOST);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_DB_NAME);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_DB_USER);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_DB_PWD);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_API_VERSION);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_PACK_ID);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_CATEGORY_ID);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_TEMPLATE_ID);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_PUSH_NOTIFICATION);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_MAX_NOTIFICATION);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_MAX_ADVICE_HOUR);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_ADVICE_HOUR_DONE);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_MAX_CONTENT_INSERT);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_ICON);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_SPLASH_SCREEN);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_BACKGROUND_COLOR);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_APP_LOGO);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_LAYOUT);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_WHITE_LABEL);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_DEMO);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_FACEBOOK_TOKEN);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_APPLE_ID);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_APPLE_ID_PASSWORD);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_APPLE_STORE_APP_LINK);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_PLAY_STORE_ID);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_PLAY_STORE_ID_PASSWORD);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_PLAY_STORE_APP_LINK);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_CONTROLLER_URI);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_GOOGLE_ANALYTICS_UA);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_PUBLISHED);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_EXPIRED_AT);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_MAIN_CONTENTS_LANGUAGE);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_OTHER_CONTENTS_LANGUAGES);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_SYSTEM_ICONS);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_TOOLBAR);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(ApplicationArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.slug');
            $criteria->addSelectColumn($alias . '.bundle');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.app_key');
            $criteria->addSelectColumn($alias . '.dsn');
            $criteria->addSelectColumn($alias . '.db_host');
            $criteria->addSelectColumn($alias . '.db_name');
            $criteria->addSelectColumn($alias . '.db_user');
            $criteria->addSelectColumn($alias . '.db_pwd');
            $criteria->addSelectColumn($alias . '.api_version');
            $criteria->addSelectColumn($alias . '.pack_id');
            $criteria->addSelectColumn($alias . '.category_id');
            $criteria->addSelectColumn($alias . '.template_id');
            $criteria->addSelectColumn($alias . '.push_notification');
            $criteria->addSelectColumn($alias . '.max_notification');
            $criteria->addSelectColumn($alias . '.max_advice_hour');
            $criteria->addSelectColumn($alias . '.advice_hour_done');
            $criteria->addSelectColumn($alias . '.max_content_insert');
            $criteria->addSelectColumn($alias . '.icon');
            $criteria->addSelectColumn($alias . '.splash_screen');
            $criteria->addSelectColumn($alias . '.background_color');
            $criteria->addSelectColumn($alias . '.app_logo');
            $criteria->addSelectColumn($alias . '.layout');
            $criteria->addSelectColumn($alias . '.white_label');
            $criteria->addSelectColumn($alias . '.demo');
            $criteria->addSelectColumn($alias . '.facebook_token');
            $criteria->addSelectColumn($alias . '.apple_id');
            $criteria->addSelectColumn($alias . '.apple_id_password');
            $criteria->addSelectColumn($alias . '.apple_store_app_link');
            $criteria->addSelectColumn($alias . '.play_store_id');
            $criteria->addSelectColumn($alias . '.play_store_id_password');
            $criteria->addSelectColumn($alias . '.play_store_app_link');
            $criteria->addSelectColumn($alias . '.controller_uri');
            $criteria->addSelectColumn($alias . '.google_analytics_ua');
            $criteria->addSelectColumn($alias . '.published');
            $criteria->addSelectColumn($alias . '.expired_at');
            $criteria->addSelectColumn($alias . '.main_contents_language');
            $criteria->addSelectColumn($alias . '.other_contents_languages');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.system_icons');
            $criteria->addSelectColumn($alias . '.toolbar');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApplicationArchiveTableMap::DATABASE_NAME)->getTable(ApplicationArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApplicationArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApplicationArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApplicationArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ApplicationArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ApplicationArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicationArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\ApplicationArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApplicationArchiveTableMap::DATABASE_NAME);
            $criteria->add(ApplicationArchiveTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ApplicationArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApplicationArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApplicationArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the application_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApplicationArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ApplicationArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or ApplicationArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicationArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ApplicationArchive object
        }


        // Set the correct dbName
        $query = ApplicationArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApplicationArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApplicationArchiveTableMap::buildTableMap();
