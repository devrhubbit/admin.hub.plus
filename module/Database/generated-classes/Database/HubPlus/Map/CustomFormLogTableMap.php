<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\CustomFormLog;
use Database\HubPlus\CustomFormLogQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'cf_log' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class CustomFormLogTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.CustomFormLogTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'cf_log';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\CustomFormLog';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.CustomFormLog';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'cf_log.id';

    /**
     * the column name for the form_id field
     */
    const COL_FORM_ID = 'cf_log.form_id';

    /**
     * the column name for the row_id field
     */
    const COL_ROW_ID = 'cf_log.row_id';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'cf_log.user_id';

    /**
     * the column name for the liked_at field
     */
    const COL_LIKED_AT = 'cf_log.liked_at';

    /**
     * the column name for the shared_at field
     */
    const COL_SHARED_AT = 'cf_log.shared_at';

    /**
     * the column name for the like_count field
     */
    const COL_LIKE_COUNT = 'cf_log.like_count';

    /**
     * the column name for the share_count field
     */
    const COL_SHARE_COUNT = 'cf_log.share_count';

    /**
     * the column name for the view_count field
     */
    const COL_VIEW_COUNT = 'cf_log.view_count';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'FormId', 'RowId', 'UserId', 'LikedAt', 'SharedAt', 'LikeCount', 'ShareCount', 'ViewCount', ),
        self::TYPE_CAMELNAME     => array('id', 'formId', 'rowId', 'userId', 'likedAt', 'sharedAt', 'likeCount', 'shareCount', 'viewCount', ),
        self::TYPE_COLNAME       => array(CustomFormLogTableMap::COL_ID, CustomFormLogTableMap::COL_FORM_ID, CustomFormLogTableMap::COL_ROW_ID, CustomFormLogTableMap::COL_USER_ID, CustomFormLogTableMap::COL_LIKED_AT, CustomFormLogTableMap::COL_SHARED_AT, CustomFormLogTableMap::COL_LIKE_COUNT, CustomFormLogTableMap::COL_SHARE_COUNT, CustomFormLogTableMap::COL_VIEW_COUNT, ),
        self::TYPE_FIELDNAME     => array('id', 'form_id', 'row_id', 'user_id', 'liked_at', 'shared_at', 'like_count', 'share_count', 'view_count', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'FormId' => 1, 'RowId' => 2, 'UserId' => 3, 'LikedAt' => 4, 'SharedAt' => 5, 'LikeCount' => 6, 'ShareCount' => 7, 'ViewCount' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'formId' => 1, 'rowId' => 2, 'userId' => 3, 'likedAt' => 4, 'sharedAt' => 5, 'likeCount' => 6, 'shareCount' => 7, 'viewCount' => 8, ),
        self::TYPE_COLNAME       => array(CustomFormLogTableMap::COL_ID => 0, CustomFormLogTableMap::COL_FORM_ID => 1, CustomFormLogTableMap::COL_ROW_ID => 2, CustomFormLogTableMap::COL_USER_ID => 3, CustomFormLogTableMap::COL_LIKED_AT => 4, CustomFormLogTableMap::COL_SHARED_AT => 5, CustomFormLogTableMap::COL_LIKE_COUNT => 6, CustomFormLogTableMap::COL_SHARE_COUNT => 7, CustomFormLogTableMap::COL_VIEW_COUNT => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'form_id' => 1, 'row_id' => 2, 'user_id' => 3, 'liked_at' => 4, 'shared_at' => 5, 'like_count' => 6, 'share_count' => 7, 'view_count' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cf_log');
        $this->setPhpName('CustomFormLog');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\CustomFormLog');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('form_id', 'FormId', 'INTEGER', 'section', 'id', false, null, null);
        $this->addColumn('row_id', 'RowId', 'INTEGER', false, null, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'user_app', 'id', false, null, null);
        $this->addColumn('liked_at', 'LikedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('shared_at', 'SharedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('like_count', 'LikeCount', 'INTEGER', false, null, 0);
        $this->addColumn('share_count', 'ShareCount', 'INTEGER', false, null, 0);
        $this->addColumn('view_count', 'ViewCount', 'INTEGER', false, null, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Section', '\\Database\\HubPlus\\Section', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':form_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('UserApp', '\\Database\\HubPlus\\UserApp', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CustomFormLogTableMap::CLASS_DEFAULT : CustomFormLogTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CustomFormLog object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CustomFormLogTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CustomFormLogTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CustomFormLogTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CustomFormLogTableMap::OM_CLASS;
            /** @var CustomFormLog $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CustomFormLogTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CustomFormLogTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CustomFormLogTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CustomFormLog $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CustomFormLogTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CustomFormLogTableMap::COL_ID);
            $criteria->addSelectColumn(CustomFormLogTableMap::COL_FORM_ID);
            $criteria->addSelectColumn(CustomFormLogTableMap::COL_ROW_ID);
            $criteria->addSelectColumn(CustomFormLogTableMap::COL_USER_ID);
            $criteria->addSelectColumn(CustomFormLogTableMap::COL_LIKED_AT);
            $criteria->addSelectColumn(CustomFormLogTableMap::COL_SHARED_AT);
            $criteria->addSelectColumn(CustomFormLogTableMap::COL_LIKE_COUNT);
            $criteria->addSelectColumn(CustomFormLogTableMap::COL_SHARE_COUNT);
            $criteria->addSelectColumn(CustomFormLogTableMap::COL_VIEW_COUNT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.form_id');
            $criteria->addSelectColumn($alias . '.row_id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.liked_at');
            $criteria->addSelectColumn($alias . '.shared_at');
            $criteria->addSelectColumn($alias . '.like_count');
            $criteria->addSelectColumn($alias . '.share_count');
            $criteria->addSelectColumn($alias . '.view_count');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CustomFormLogTableMap::DATABASE_NAME)->getTable(CustomFormLogTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CustomFormLogTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CustomFormLogTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CustomFormLogTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CustomFormLog or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CustomFormLog object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomFormLogTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\CustomFormLog) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CustomFormLogTableMap::DATABASE_NAME);
            $criteria->add(CustomFormLogTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CustomFormLogQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CustomFormLogTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CustomFormLogTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the cf_log table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CustomFormLogQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CustomFormLog or Criteria object.
     *
     * @param mixed               $criteria Criteria or CustomFormLog object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomFormLogTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CustomFormLog object
        }

        if ($criteria->containsKey(CustomFormLogTableMap::COL_ID) && $criteria->keyContainsValue(CustomFormLogTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CustomFormLogTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CustomFormLogQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CustomFormLogTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CustomFormLogTableMap::buildTableMap();
