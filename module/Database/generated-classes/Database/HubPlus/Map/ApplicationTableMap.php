<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\Application;
use Database\HubPlus\ApplicationQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'application' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ApplicationTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.ApplicationTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'application';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\Application';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.Application';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 45;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 45;

    /**
     * the column name for the id field
     */
    const COL_ID = 'application.id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'application.title';

    /**
     * the column name for the slug field
     */
    const COL_SLUG = 'application.slug';

    /**
     * the column name for the bundle field
     */
    const COL_BUNDLE = 'application.bundle';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'application.description';

    /**
     * the column name for the app_key field
     */
    const COL_APP_KEY = 'application.app_key';

    /**
     * the column name for the dsn field
     */
    const COL_DSN = 'application.dsn';

    /**
     * the column name for the db_host field
     */
    const COL_DB_HOST = 'application.db_host';

    /**
     * the column name for the db_name field
     */
    const COL_DB_NAME = 'application.db_name';

    /**
     * the column name for the db_user field
     */
    const COL_DB_USER = 'application.db_user';

    /**
     * the column name for the db_pwd field
     */
    const COL_DB_PWD = 'application.db_pwd';

    /**
     * the column name for the api_version field
     */
    const COL_API_VERSION = 'application.api_version';

    /**
     * the column name for the pack_id field
     */
    const COL_PACK_ID = 'application.pack_id';

    /**
     * the column name for the category_id field
     */
    const COL_CATEGORY_ID = 'application.category_id';

    /**
     * the column name for the template_id field
     */
    const COL_TEMPLATE_ID = 'application.template_id';

    /**
     * the column name for the push_notification field
     */
    const COL_PUSH_NOTIFICATION = 'application.push_notification';

    /**
     * the column name for the max_notification field
     */
    const COL_MAX_NOTIFICATION = 'application.max_notification';

    /**
     * the column name for the max_advice_hour field
     */
    const COL_MAX_ADVICE_HOUR = 'application.max_advice_hour';

    /**
     * the column name for the advice_hour_done field
     */
    const COL_ADVICE_HOUR_DONE = 'application.advice_hour_done';

    /**
     * the column name for the max_content_insert field
     */
    const COL_MAX_CONTENT_INSERT = 'application.max_content_insert';

    /**
     * the column name for the icon field
     */
    const COL_ICON = 'application.icon';

    /**
     * the column name for the splash_screen field
     */
    const COL_SPLASH_SCREEN = 'application.splash_screen';

    /**
     * the column name for the background_color field
     */
    const COL_BACKGROUND_COLOR = 'application.background_color';

    /**
     * the column name for the app_logo field
     */
    const COL_APP_LOGO = 'application.app_logo';

    /**
     * the column name for the layout field
     */
    const COL_LAYOUT = 'application.layout';

    /**
     * the column name for the white_label field
     */
    const COL_WHITE_LABEL = 'application.white_label';

    /**
     * the column name for the demo field
     */
    const COL_DEMO = 'application.demo';

    /**
     * the column name for the facebook_token field
     */
    const COL_FACEBOOK_TOKEN = 'application.facebook_token';

    /**
     * the column name for the apple_id field
     */
    const COL_APPLE_ID = 'application.apple_id';

    /**
     * the column name for the apple_id_password field
     */
    const COL_APPLE_ID_PASSWORD = 'application.apple_id_password';

    /**
     * the column name for the apple_store_app_link field
     */
    const COL_APPLE_STORE_APP_LINK = 'application.apple_store_app_link';

    /**
     * the column name for the play_store_id field
     */
    const COL_PLAY_STORE_ID = 'application.play_store_id';

    /**
     * the column name for the play_store_id_password field
     */
    const COL_PLAY_STORE_ID_PASSWORD = 'application.play_store_id_password';

    /**
     * the column name for the play_store_app_link field
     */
    const COL_PLAY_STORE_APP_LINK = 'application.play_store_app_link';

    /**
     * the column name for the controller_uri field
     */
    const COL_CONTROLLER_URI = 'application.controller_uri';

    /**
     * the column name for the google_analytics_ua field
     */
    const COL_GOOGLE_ANALYTICS_UA = 'application.google_analytics_ua';

    /**
     * the column name for the published field
     */
    const COL_PUBLISHED = 'application.published';

    /**
     * the column name for the expired_at field
     */
    const COL_EXPIRED_AT = 'application.expired_at';

    /**
     * the column name for the main_contents_language field
     */
    const COL_MAIN_CONTENTS_LANGUAGE = 'application.main_contents_language';

    /**
     * the column name for the other_contents_languages field
     */
    const COL_OTHER_CONTENTS_LANGUAGES = 'application.other_contents_languages';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'application.deleted_at';

    /**
     * the column name for the system_icons field
     */
    const COL_SYSTEM_ICONS = 'application.system_icons';

    /**
     * the column name for the toolbar field
     */
    const COL_TOOLBAR = 'application.toolbar';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'application.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'application.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Title', 'Slug', 'Bundle', 'Description', 'AppKey', 'Dsn', 'DbHost', 'DbName', 'DbUser', 'DbPwd', 'ApiVersion', 'PackId', 'CategoryId', 'TemplateId', 'PushNotification', 'MaxNotification', 'MaxAdviceHour', 'AdviceHourDone', 'MaxContentInsert', 'Icon', 'SplashScreen', 'BackgroundColor', 'AppLogo', 'Layout', 'WhiteLabel', 'Demo', 'FacebookToken', 'AppleId', 'AppleIdPassword', 'AppleStoreAppLink', 'PlayStoreId', 'PlayStoreIdPassword', 'PlayStoreAppLink', 'ControllerUri', 'GoogleAnalyticsUa', 'Published', 'ExpiredAt', 'MainContentsLanguage', 'OtherContentsLanguages', 'DeletedAt', 'SystemIcons', 'Toolbar', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'title', 'slug', 'bundle', 'description', 'appKey', 'dsn', 'dbHost', 'dbName', 'dbUser', 'dbPwd', 'apiVersion', 'packId', 'categoryId', 'templateId', 'pushNotification', 'maxNotification', 'maxAdviceHour', 'adviceHourDone', 'maxContentInsert', 'icon', 'splashScreen', 'backgroundColor', 'appLogo', 'layout', 'whiteLabel', 'demo', 'facebookToken', 'appleId', 'appleIdPassword', 'appleStoreAppLink', 'playStoreId', 'playStoreIdPassword', 'playStoreAppLink', 'controllerUri', 'googleAnalyticsUa', 'published', 'expiredAt', 'mainContentsLanguage', 'otherContentsLanguages', 'deletedAt', 'systemIcons', 'toolbar', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ApplicationTableMap::COL_ID, ApplicationTableMap::COL_TITLE, ApplicationTableMap::COL_SLUG, ApplicationTableMap::COL_BUNDLE, ApplicationTableMap::COL_DESCRIPTION, ApplicationTableMap::COL_APP_KEY, ApplicationTableMap::COL_DSN, ApplicationTableMap::COL_DB_HOST, ApplicationTableMap::COL_DB_NAME, ApplicationTableMap::COL_DB_USER, ApplicationTableMap::COL_DB_PWD, ApplicationTableMap::COL_API_VERSION, ApplicationTableMap::COL_PACK_ID, ApplicationTableMap::COL_CATEGORY_ID, ApplicationTableMap::COL_TEMPLATE_ID, ApplicationTableMap::COL_PUSH_NOTIFICATION, ApplicationTableMap::COL_MAX_NOTIFICATION, ApplicationTableMap::COL_MAX_ADVICE_HOUR, ApplicationTableMap::COL_ADVICE_HOUR_DONE, ApplicationTableMap::COL_MAX_CONTENT_INSERT, ApplicationTableMap::COL_ICON, ApplicationTableMap::COL_SPLASH_SCREEN, ApplicationTableMap::COL_BACKGROUND_COLOR, ApplicationTableMap::COL_APP_LOGO, ApplicationTableMap::COL_LAYOUT, ApplicationTableMap::COL_WHITE_LABEL, ApplicationTableMap::COL_DEMO, ApplicationTableMap::COL_FACEBOOK_TOKEN, ApplicationTableMap::COL_APPLE_ID, ApplicationTableMap::COL_APPLE_ID_PASSWORD, ApplicationTableMap::COL_APPLE_STORE_APP_LINK, ApplicationTableMap::COL_PLAY_STORE_ID, ApplicationTableMap::COL_PLAY_STORE_ID_PASSWORD, ApplicationTableMap::COL_PLAY_STORE_APP_LINK, ApplicationTableMap::COL_CONTROLLER_URI, ApplicationTableMap::COL_GOOGLE_ANALYTICS_UA, ApplicationTableMap::COL_PUBLISHED, ApplicationTableMap::COL_EXPIRED_AT, ApplicationTableMap::COL_MAIN_CONTENTS_LANGUAGE, ApplicationTableMap::COL_OTHER_CONTENTS_LANGUAGES, ApplicationTableMap::COL_DELETED_AT, ApplicationTableMap::COL_SYSTEM_ICONS, ApplicationTableMap::COL_TOOLBAR, ApplicationTableMap::COL_CREATED_AT, ApplicationTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'title', 'slug', 'bundle', 'description', 'app_key', 'dsn', 'db_host', 'db_name', 'db_user', 'db_pwd', 'api_version', 'pack_id', 'category_id', 'template_id', 'push_notification', 'max_notification', 'max_advice_hour', 'advice_hour_done', 'max_content_insert', 'icon', 'splash_screen', 'background_color', 'app_logo', 'layout', 'white_label', 'demo', 'facebook_token', 'apple_id', 'apple_id_password', 'apple_store_app_link', 'play_store_id', 'play_store_id_password', 'play_store_app_link', 'controller_uri', 'google_analytics_ua', 'published', 'expired_at', 'main_contents_language', 'other_contents_languages', 'deleted_at', 'system_icons', 'toolbar', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Title' => 1, 'Slug' => 2, 'Bundle' => 3, 'Description' => 4, 'AppKey' => 5, 'Dsn' => 6, 'DbHost' => 7, 'DbName' => 8, 'DbUser' => 9, 'DbPwd' => 10, 'ApiVersion' => 11, 'PackId' => 12, 'CategoryId' => 13, 'TemplateId' => 14, 'PushNotification' => 15, 'MaxNotification' => 16, 'MaxAdviceHour' => 17, 'AdviceHourDone' => 18, 'MaxContentInsert' => 19, 'Icon' => 20, 'SplashScreen' => 21, 'BackgroundColor' => 22, 'AppLogo' => 23, 'Layout' => 24, 'WhiteLabel' => 25, 'Demo' => 26, 'FacebookToken' => 27, 'AppleId' => 28, 'AppleIdPassword' => 29, 'AppleStoreAppLink' => 30, 'PlayStoreId' => 31, 'PlayStoreIdPassword' => 32, 'PlayStoreAppLink' => 33, 'ControllerUri' => 34, 'GoogleAnalyticsUa' => 35, 'Published' => 36, 'ExpiredAt' => 37, 'MainContentsLanguage' => 38, 'OtherContentsLanguages' => 39, 'DeletedAt' => 40, 'SystemIcons' => 41, 'Toolbar' => 42, 'CreatedAt' => 43, 'UpdatedAt' => 44, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'title' => 1, 'slug' => 2, 'bundle' => 3, 'description' => 4, 'appKey' => 5, 'dsn' => 6, 'dbHost' => 7, 'dbName' => 8, 'dbUser' => 9, 'dbPwd' => 10, 'apiVersion' => 11, 'packId' => 12, 'categoryId' => 13, 'templateId' => 14, 'pushNotification' => 15, 'maxNotification' => 16, 'maxAdviceHour' => 17, 'adviceHourDone' => 18, 'maxContentInsert' => 19, 'icon' => 20, 'splashScreen' => 21, 'backgroundColor' => 22, 'appLogo' => 23, 'layout' => 24, 'whiteLabel' => 25, 'demo' => 26, 'facebookToken' => 27, 'appleId' => 28, 'appleIdPassword' => 29, 'appleStoreAppLink' => 30, 'playStoreId' => 31, 'playStoreIdPassword' => 32, 'playStoreAppLink' => 33, 'controllerUri' => 34, 'googleAnalyticsUa' => 35, 'published' => 36, 'expiredAt' => 37, 'mainContentsLanguage' => 38, 'otherContentsLanguages' => 39, 'deletedAt' => 40, 'systemIcons' => 41, 'toolbar' => 42, 'createdAt' => 43, 'updatedAt' => 44, ),
        self::TYPE_COLNAME       => array(ApplicationTableMap::COL_ID => 0, ApplicationTableMap::COL_TITLE => 1, ApplicationTableMap::COL_SLUG => 2, ApplicationTableMap::COL_BUNDLE => 3, ApplicationTableMap::COL_DESCRIPTION => 4, ApplicationTableMap::COL_APP_KEY => 5, ApplicationTableMap::COL_DSN => 6, ApplicationTableMap::COL_DB_HOST => 7, ApplicationTableMap::COL_DB_NAME => 8, ApplicationTableMap::COL_DB_USER => 9, ApplicationTableMap::COL_DB_PWD => 10, ApplicationTableMap::COL_API_VERSION => 11, ApplicationTableMap::COL_PACK_ID => 12, ApplicationTableMap::COL_CATEGORY_ID => 13, ApplicationTableMap::COL_TEMPLATE_ID => 14, ApplicationTableMap::COL_PUSH_NOTIFICATION => 15, ApplicationTableMap::COL_MAX_NOTIFICATION => 16, ApplicationTableMap::COL_MAX_ADVICE_HOUR => 17, ApplicationTableMap::COL_ADVICE_HOUR_DONE => 18, ApplicationTableMap::COL_MAX_CONTENT_INSERT => 19, ApplicationTableMap::COL_ICON => 20, ApplicationTableMap::COL_SPLASH_SCREEN => 21, ApplicationTableMap::COL_BACKGROUND_COLOR => 22, ApplicationTableMap::COL_APP_LOGO => 23, ApplicationTableMap::COL_LAYOUT => 24, ApplicationTableMap::COL_WHITE_LABEL => 25, ApplicationTableMap::COL_DEMO => 26, ApplicationTableMap::COL_FACEBOOK_TOKEN => 27, ApplicationTableMap::COL_APPLE_ID => 28, ApplicationTableMap::COL_APPLE_ID_PASSWORD => 29, ApplicationTableMap::COL_APPLE_STORE_APP_LINK => 30, ApplicationTableMap::COL_PLAY_STORE_ID => 31, ApplicationTableMap::COL_PLAY_STORE_ID_PASSWORD => 32, ApplicationTableMap::COL_PLAY_STORE_APP_LINK => 33, ApplicationTableMap::COL_CONTROLLER_URI => 34, ApplicationTableMap::COL_GOOGLE_ANALYTICS_UA => 35, ApplicationTableMap::COL_PUBLISHED => 36, ApplicationTableMap::COL_EXPIRED_AT => 37, ApplicationTableMap::COL_MAIN_CONTENTS_LANGUAGE => 38, ApplicationTableMap::COL_OTHER_CONTENTS_LANGUAGES => 39, ApplicationTableMap::COL_DELETED_AT => 40, ApplicationTableMap::COL_SYSTEM_ICONS => 41, ApplicationTableMap::COL_TOOLBAR => 42, ApplicationTableMap::COL_CREATED_AT => 43, ApplicationTableMap::COL_UPDATED_AT => 44, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'title' => 1, 'slug' => 2, 'bundle' => 3, 'description' => 4, 'app_key' => 5, 'dsn' => 6, 'db_host' => 7, 'db_name' => 8, 'db_user' => 9, 'db_pwd' => 10, 'api_version' => 11, 'pack_id' => 12, 'category_id' => 13, 'template_id' => 14, 'push_notification' => 15, 'max_notification' => 16, 'max_advice_hour' => 17, 'advice_hour_done' => 18, 'max_content_insert' => 19, 'icon' => 20, 'splash_screen' => 21, 'background_color' => 22, 'app_logo' => 23, 'layout' => 24, 'white_label' => 25, 'demo' => 26, 'facebook_token' => 27, 'apple_id' => 28, 'apple_id_password' => 29, 'apple_store_app_link' => 30, 'play_store_id' => 31, 'play_store_id_password' => 32, 'play_store_app_link' => 33, 'controller_uri' => 34, 'google_analytics_ua' => 35, 'published' => 36, 'expired_at' => 37, 'main_contents_language' => 38, 'other_contents_languages' => 39, 'deleted_at' => 40, 'system_icons' => 41, 'toolbar' => 42, 'created_at' => 43, 'updated_at' => 44, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('application');
        $this->setPhpName('Application');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\Application');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 255, null);
        $this->addColumn('slug', 'Slug', 'VARCHAR', false, 255, null);
        $this->addColumn('bundle', 'Bundle', 'VARCHAR', false, 255, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('app_key', 'AppKey', 'VARCHAR', false, 255, null);
        $this->addColumn('dsn', 'Dsn', 'VARCHAR', false, 255, null);
        $this->addColumn('db_host', 'DbHost', 'VARCHAR', false, 255, null);
        $this->addColumn('db_name', 'DbName', 'VARCHAR', false, 255, null);
        $this->addColumn('db_user', 'DbUser', 'VARCHAR', false, 255, null);
        $this->addColumn('db_pwd', 'DbPwd', 'VARCHAR', false, 255, null);
        $this->addColumn('api_version', 'ApiVersion', 'VARCHAR', false, 10, null);
        $this->addForeignKey('pack_id', 'PackId', 'INTEGER', 'pack', 'id', false, null, null);
        $this->addForeignKey('category_id', 'CategoryId', 'INTEGER', 'category', 'id', false, null, null);
        $this->addForeignKey('template_id', 'TemplateId', 'INTEGER', 'template', 'id', false, null, null);
        $this->addColumn('push_notification', 'PushNotification', 'LONGVARCHAR', false, null, null);
        $this->addColumn('max_notification', 'MaxNotification', 'INTEGER', false, null, 0);
        $this->addColumn('max_advice_hour', 'MaxAdviceHour', 'INTEGER', false, null, 0);
        $this->addColumn('advice_hour_done', 'AdviceHourDone', 'INTEGER', false, null, 0);
        $this->addColumn('max_content_insert', 'MaxContentInsert', 'INTEGER', false, null, 0);
        $this->addColumn('icon', 'Icon', 'VARCHAR', false, 255, null);
        $this->addColumn('splash_screen', 'SplashScreen', 'VARCHAR', false, 255, null);
        $this->addColumn('background_color', 'BackgroundColor', 'VARCHAR', false, 7, null);
        $this->addColumn('app_logo', 'AppLogo', 'VARCHAR', false, 255, null);
        $this->addColumn('layout', 'Layout', 'LONGVARCHAR', false, null, null);
        $this->addColumn('white_label', 'WhiteLabel', 'BOOLEAN', false, 1, null);
        $this->addColumn('demo', 'Demo', 'BOOLEAN', false, 1, null);
        $this->addColumn('facebook_token', 'FacebookToken', 'VARCHAR', false, 255, null);
        $this->addColumn('apple_id', 'AppleId', 'VARCHAR', false, 255, null);
        $this->addColumn('apple_id_password', 'AppleIdPassword', 'VARCHAR', false, 255, null);
        $this->addColumn('apple_store_app_link', 'AppleStoreAppLink', 'VARCHAR', false, 255, null);
        $this->addColumn('play_store_id', 'PlayStoreId', 'VARCHAR', false, 255, null);
        $this->addColumn('play_store_id_password', 'PlayStoreIdPassword', 'VARCHAR', false, 255, null);
        $this->addColumn('play_store_app_link', 'PlayStoreAppLink', 'VARCHAR', false, 255, null);
        $this->addColumn('controller_uri', 'ControllerUri', 'LONGVARCHAR', false, null, null);
        $this->addColumn('google_analytics_ua', 'GoogleAnalyticsUa', 'LONGVARCHAR', false, null, null);
        $this->addColumn('published', 'Published', 'TINYINT', true, 1, 0);
        $this->addColumn('expired_at', 'ExpiredAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('main_contents_language', 'MainContentsLanguage', 'VARCHAR', false, 2, null);
        $this->addColumn('other_contents_languages', 'OtherContentsLanguages', 'VARCHAR', false, 255, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('system_icons', 'SystemIcons', 'LONGVARCHAR', true, null, null);
        $this->addColumn('toolbar', 'Toolbar', 'CHAR', true, null, 'SLIDESHOW');
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Category', '\\Database\\HubPlus\\Category', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':category_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Template', '\\Database\\HubPlus\\Template', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':template_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Pack', '\\Database\\HubPlus\\Pack', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':pack_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('UserApplication', '\\Database\\HubPlus\\UserApplication', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':app_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'UserApplications', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to application     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        UserApplicationTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ApplicationTableMap::CLASS_DEFAULT : ApplicationTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Application object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ApplicationTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ApplicationTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ApplicationTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ApplicationTableMap::OM_CLASS;
            /** @var Application $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ApplicationTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ApplicationTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ApplicationTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Application $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ApplicationTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ApplicationTableMap::COL_ID);
            $criteria->addSelectColumn(ApplicationTableMap::COL_TITLE);
            $criteria->addSelectColumn(ApplicationTableMap::COL_SLUG);
            $criteria->addSelectColumn(ApplicationTableMap::COL_BUNDLE);
            $criteria->addSelectColumn(ApplicationTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(ApplicationTableMap::COL_APP_KEY);
            $criteria->addSelectColumn(ApplicationTableMap::COL_DSN);
            $criteria->addSelectColumn(ApplicationTableMap::COL_DB_HOST);
            $criteria->addSelectColumn(ApplicationTableMap::COL_DB_NAME);
            $criteria->addSelectColumn(ApplicationTableMap::COL_DB_USER);
            $criteria->addSelectColumn(ApplicationTableMap::COL_DB_PWD);
            $criteria->addSelectColumn(ApplicationTableMap::COL_API_VERSION);
            $criteria->addSelectColumn(ApplicationTableMap::COL_PACK_ID);
            $criteria->addSelectColumn(ApplicationTableMap::COL_CATEGORY_ID);
            $criteria->addSelectColumn(ApplicationTableMap::COL_TEMPLATE_ID);
            $criteria->addSelectColumn(ApplicationTableMap::COL_PUSH_NOTIFICATION);
            $criteria->addSelectColumn(ApplicationTableMap::COL_MAX_NOTIFICATION);
            $criteria->addSelectColumn(ApplicationTableMap::COL_MAX_ADVICE_HOUR);
            $criteria->addSelectColumn(ApplicationTableMap::COL_ADVICE_HOUR_DONE);
            $criteria->addSelectColumn(ApplicationTableMap::COL_MAX_CONTENT_INSERT);
            $criteria->addSelectColumn(ApplicationTableMap::COL_ICON);
            $criteria->addSelectColumn(ApplicationTableMap::COL_SPLASH_SCREEN);
            $criteria->addSelectColumn(ApplicationTableMap::COL_BACKGROUND_COLOR);
            $criteria->addSelectColumn(ApplicationTableMap::COL_APP_LOGO);
            $criteria->addSelectColumn(ApplicationTableMap::COL_LAYOUT);
            $criteria->addSelectColumn(ApplicationTableMap::COL_WHITE_LABEL);
            $criteria->addSelectColumn(ApplicationTableMap::COL_DEMO);
            $criteria->addSelectColumn(ApplicationTableMap::COL_FACEBOOK_TOKEN);
            $criteria->addSelectColumn(ApplicationTableMap::COL_APPLE_ID);
            $criteria->addSelectColumn(ApplicationTableMap::COL_APPLE_ID_PASSWORD);
            $criteria->addSelectColumn(ApplicationTableMap::COL_APPLE_STORE_APP_LINK);
            $criteria->addSelectColumn(ApplicationTableMap::COL_PLAY_STORE_ID);
            $criteria->addSelectColumn(ApplicationTableMap::COL_PLAY_STORE_ID_PASSWORD);
            $criteria->addSelectColumn(ApplicationTableMap::COL_PLAY_STORE_APP_LINK);
            $criteria->addSelectColumn(ApplicationTableMap::COL_CONTROLLER_URI);
            $criteria->addSelectColumn(ApplicationTableMap::COL_GOOGLE_ANALYTICS_UA);
            $criteria->addSelectColumn(ApplicationTableMap::COL_PUBLISHED);
            $criteria->addSelectColumn(ApplicationTableMap::COL_EXPIRED_AT);
            $criteria->addSelectColumn(ApplicationTableMap::COL_MAIN_CONTENTS_LANGUAGE);
            $criteria->addSelectColumn(ApplicationTableMap::COL_OTHER_CONTENTS_LANGUAGES);
            $criteria->addSelectColumn(ApplicationTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(ApplicationTableMap::COL_SYSTEM_ICONS);
            $criteria->addSelectColumn(ApplicationTableMap::COL_TOOLBAR);
            $criteria->addSelectColumn(ApplicationTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(ApplicationTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.slug');
            $criteria->addSelectColumn($alias . '.bundle');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.app_key');
            $criteria->addSelectColumn($alias . '.dsn');
            $criteria->addSelectColumn($alias . '.db_host');
            $criteria->addSelectColumn($alias . '.db_name');
            $criteria->addSelectColumn($alias . '.db_user');
            $criteria->addSelectColumn($alias . '.db_pwd');
            $criteria->addSelectColumn($alias . '.api_version');
            $criteria->addSelectColumn($alias . '.pack_id');
            $criteria->addSelectColumn($alias . '.category_id');
            $criteria->addSelectColumn($alias . '.template_id');
            $criteria->addSelectColumn($alias . '.push_notification');
            $criteria->addSelectColumn($alias . '.max_notification');
            $criteria->addSelectColumn($alias . '.max_advice_hour');
            $criteria->addSelectColumn($alias . '.advice_hour_done');
            $criteria->addSelectColumn($alias . '.max_content_insert');
            $criteria->addSelectColumn($alias . '.icon');
            $criteria->addSelectColumn($alias . '.splash_screen');
            $criteria->addSelectColumn($alias . '.background_color');
            $criteria->addSelectColumn($alias . '.app_logo');
            $criteria->addSelectColumn($alias . '.layout');
            $criteria->addSelectColumn($alias . '.white_label');
            $criteria->addSelectColumn($alias . '.demo');
            $criteria->addSelectColumn($alias . '.facebook_token');
            $criteria->addSelectColumn($alias . '.apple_id');
            $criteria->addSelectColumn($alias . '.apple_id_password');
            $criteria->addSelectColumn($alias . '.apple_store_app_link');
            $criteria->addSelectColumn($alias . '.play_store_id');
            $criteria->addSelectColumn($alias . '.play_store_id_password');
            $criteria->addSelectColumn($alias . '.play_store_app_link');
            $criteria->addSelectColumn($alias . '.controller_uri');
            $criteria->addSelectColumn($alias . '.google_analytics_ua');
            $criteria->addSelectColumn($alias . '.published');
            $criteria->addSelectColumn($alias . '.expired_at');
            $criteria->addSelectColumn($alias . '.main_contents_language');
            $criteria->addSelectColumn($alias . '.other_contents_languages');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.system_icons');
            $criteria->addSelectColumn($alias . '.toolbar');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ApplicationTableMap::DATABASE_NAME)->getTable(ApplicationTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ApplicationTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ApplicationTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ApplicationTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Application or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Application object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicationTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\Application) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ApplicationTableMap::DATABASE_NAME);
            $criteria->add(ApplicationTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ApplicationQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ApplicationTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ApplicationTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the application table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ApplicationQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Application or Criteria object.
     *
     * @param mixed               $criteria Criteria or Application object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ApplicationTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Application object
        }

        if ($criteria->containsKey(ApplicationTableMap::COL_ID) && $criteria->keyContainsValue(ApplicationTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ApplicationTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ApplicationQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ApplicationTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ApplicationTableMap::buildTableMap();
