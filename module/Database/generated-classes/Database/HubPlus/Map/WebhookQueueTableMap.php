<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\WebhookQueue;
use Database\HubPlus\WebhookQueueQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'webhook_queue' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class WebhookQueueTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.WebhookQueueTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'webhook_queue';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\WebhookQueue';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.WebhookQueue';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 15;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 15;

    /**
     * the column name for the id field
     */
    const COL_ID = 'webhook_queue.id';

    /**
     * the column name for the webhook_id field
     */
    const COL_WEBHOOK_ID = 'webhook_queue.webhook_id';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'webhook_queue.status';

    /**
     * the column name for the failed_attempts field
     */
    const COL_FAILED_ATTEMPTS = 'webhook_queue.failed_attempts';

    /**
     * the column name for the max_attempts field
     */
    const COL_MAX_ATTEMPTS = 'webhook_queue.max_attempts';

    /**
     * the column name for the method field
     */
    const COL_METHOD = 'webhook_queue.method';

    /**
     * the column name for the url field
     */
    const COL_URL = 'webhook_queue.url';

    /**
     * the column name for the bundle field
     */
    const COL_BUNDLE = 'webhook_queue.bundle';

    /**
     * the column name for the payload field
     */
    const COL_PAYLOAD = 'webhook_queue.payload';

    /**
     * the column name for the response_status field
     */
    const COL_RESPONSE_STATUS = 'webhook_queue.response_status';

    /**
     * the column name for the response_log field
     */
    const COL_RESPONSE_LOG = 'webhook_queue.response_log';

    /**
     * the column name for the to_sync_at field
     */
    const COL_TO_SYNC_AT = 'webhook_queue.to_sync_at';

    /**
     * the column name for the synced_at field
     */
    const COL_SYNCED_AT = 'webhook_queue.synced_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'webhook_queue.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'webhook_queue.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'WebhookId', 'Status', 'FailedAttempts', 'MaxAttempts', 'Method', 'Url', 'Bundle', 'Payload', 'ResponseStatus', 'ResponseLog', 'ToSyncAt', 'SyncedAt', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'webhookId', 'status', 'failedAttempts', 'maxAttempts', 'method', 'url', 'bundle', 'payload', 'responseStatus', 'responseLog', 'toSyncAt', 'syncedAt', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(WebhookQueueTableMap::COL_ID, WebhookQueueTableMap::COL_WEBHOOK_ID, WebhookQueueTableMap::COL_STATUS, WebhookQueueTableMap::COL_FAILED_ATTEMPTS, WebhookQueueTableMap::COL_MAX_ATTEMPTS, WebhookQueueTableMap::COL_METHOD, WebhookQueueTableMap::COL_URL, WebhookQueueTableMap::COL_BUNDLE, WebhookQueueTableMap::COL_PAYLOAD, WebhookQueueTableMap::COL_RESPONSE_STATUS, WebhookQueueTableMap::COL_RESPONSE_LOG, WebhookQueueTableMap::COL_TO_SYNC_AT, WebhookQueueTableMap::COL_SYNCED_AT, WebhookQueueTableMap::COL_CREATED_AT, WebhookQueueTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'webhook_id', 'status', 'failed_attempts', 'max_attempts', 'method', 'url', 'bundle', 'payload', 'response_status', 'response_log', 'to_sync_at', 'synced_at', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'WebhookId' => 1, 'Status' => 2, 'FailedAttempts' => 3, 'MaxAttempts' => 4, 'Method' => 5, 'Url' => 6, 'Bundle' => 7, 'Payload' => 8, 'ResponseStatus' => 9, 'ResponseLog' => 10, 'ToSyncAt' => 11, 'SyncedAt' => 12, 'CreatedAt' => 13, 'UpdatedAt' => 14, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'webhookId' => 1, 'status' => 2, 'failedAttempts' => 3, 'maxAttempts' => 4, 'method' => 5, 'url' => 6, 'bundle' => 7, 'payload' => 8, 'responseStatus' => 9, 'responseLog' => 10, 'toSyncAt' => 11, 'syncedAt' => 12, 'createdAt' => 13, 'updatedAt' => 14, ),
        self::TYPE_COLNAME       => array(WebhookQueueTableMap::COL_ID => 0, WebhookQueueTableMap::COL_WEBHOOK_ID => 1, WebhookQueueTableMap::COL_STATUS => 2, WebhookQueueTableMap::COL_FAILED_ATTEMPTS => 3, WebhookQueueTableMap::COL_MAX_ATTEMPTS => 4, WebhookQueueTableMap::COL_METHOD => 5, WebhookQueueTableMap::COL_URL => 6, WebhookQueueTableMap::COL_BUNDLE => 7, WebhookQueueTableMap::COL_PAYLOAD => 8, WebhookQueueTableMap::COL_RESPONSE_STATUS => 9, WebhookQueueTableMap::COL_RESPONSE_LOG => 10, WebhookQueueTableMap::COL_TO_SYNC_AT => 11, WebhookQueueTableMap::COL_SYNCED_AT => 12, WebhookQueueTableMap::COL_CREATED_AT => 13, WebhookQueueTableMap::COL_UPDATED_AT => 14, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'webhook_id' => 1, 'status' => 2, 'failed_attempts' => 3, 'max_attempts' => 4, 'method' => 5, 'url' => 6, 'bundle' => 7, 'payload' => 8, 'response_status' => 9, 'response_log' => 10, 'to_sync_at' => 11, 'synced_at' => 12, 'created_at' => 13, 'updated_at' => 14, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('webhook_queue');
        $this->setPhpName('WebhookQueue');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\WebhookQueue');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('webhook_id', 'WebhookId', 'INTEGER', 'webhook', 'id', true, null, null);
        $this->addColumn('status', 'Status', 'SMALLINT', true, 2, 0);
        $this->addColumn('failed_attempts', 'FailedAttempts', 'TINYINT', true, 1, 0);
        $this->addColumn('max_attempts', 'MaxAttempts', 'TINYINT', true, 1, 1);
        $this->addColumn('method', 'Method', 'CHAR', true, null, 'POST');
        $this->addColumn('url', 'Url', 'VARCHAR', true, 2024, null);
        $this->addColumn('bundle', 'Bundle', 'VARCHAR', true, 255, null);
        $this->addColumn('payload', 'Payload', 'LONGVARCHAR', true, null, null);
        $this->addColumn('response_status', 'ResponseStatus', 'SMALLINT', false, 2, null);
        $this->addColumn('response_log', 'ResponseLog', 'LONGVARCHAR', false, null, null);
        $this->addColumn('to_sync_at', 'ToSyncAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('synced_at', 'SyncedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Webhook', '\\Database\\HubPlus\\Webhook', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':webhook_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? WebhookQueueTableMap::CLASS_DEFAULT : WebhookQueueTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (WebhookQueue object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = WebhookQueueTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = WebhookQueueTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + WebhookQueueTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = WebhookQueueTableMap::OM_CLASS;
            /** @var WebhookQueue $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            WebhookQueueTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = WebhookQueueTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = WebhookQueueTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var WebhookQueue $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                WebhookQueueTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_ID);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_WEBHOOK_ID);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_STATUS);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_FAILED_ATTEMPTS);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_MAX_ATTEMPTS);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_METHOD);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_URL);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_BUNDLE);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_PAYLOAD);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_RESPONSE_STATUS);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_RESPONSE_LOG);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_TO_SYNC_AT);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_SYNCED_AT);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(WebhookQueueTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.webhook_id');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.failed_attempts');
            $criteria->addSelectColumn($alias . '.max_attempts');
            $criteria->addSelectColumn($alias . '.method');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.bundle');
            $criteria->addSelectColumn($alias . '.payload');
            $criteria->addSelectColumn($alias . '.response_status');
            $criteria->addSelectColumn($alias . '.response_log');
            $criteria->addSelectColumn($alias . '.to_sync_at');
            $criteria->addSelectColumn($alias . '.synced_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(WebhookQueueTableMap::DATABASE_NAME)->getTable(WebhookQueueTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(WebhookQueueTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(WebhookQueueTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new WebhookQueueTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a WebhookQueue or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or WebhookQueue object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WebhookQueueTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\WebhookQueue) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(WebhookQueueTableMap::DATABASE_NAME);
            $criteria->add(WebhookQueueTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = WebhookQueueQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            WebhookQueueTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                WebhookQueueTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the webhook_queue table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return WebhookQueueQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a WebhookQueue or Criteria object.
     *
     * @param mixed               $criteria Criteria or WebhookQueue object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(WebhookQueueTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from WebhookQueue object
        }

        if ($criteria->containsKey(WebhookQueueTableMap::COL_ID) && $criteria->keyContainsValue(WebhookQueueTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.WebhookQueueTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = WebhookQueueQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // WebhookQueueTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
WebhookQueueTableMap::buildTableMap();
