<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\SectionConnector;
use Database\HubPlus\SectionConnectorQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'section_connector' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SectionConnectorTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.SectionConnectorTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'section_connector';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\SectionConnector';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.SectionConnector';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'section_connector.id';

    /**
     * the column name for the section_id field
     */
    const COL_SECTION_ID = 'section_connector.section_id';

    /**
     * the column name for the provider_id field
     */
    const COL_PROVIDER_ID = 'section_connector.provider_id';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'section_connector.type';

    /**
     * the column name for the baseurl field
     */
    const COL_BASEURL = 'section_connector.baseurl';

    /**
     * the column name for the remote_section_id field
     */
    const COL_REMOTE_SECTION_ID = 'section_connector.remote_section_id';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'section_connector.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'section_connector.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'section_connector.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'SectionId', 'ProviderId', 'Type', 'Baseurl', 'RemoteSectionId', 'DeletedAt', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'sectionId', 'providerId', 'type', 'baseurl', 'remoteSectionId', 'deletedAt', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(SectionConnectorTableMap::COL_ID, SectionConnectorTableMap::COL_SECTION_ID, SectionConnectorTableMap::COL_PROVIDER_ID, SectionConnectorTableMap::COL_TYPE, SectionConnectorTableMap::COL_BASEURL, SectionConnectorTableMap::COL_REMOTE_SECTION_ID, SectionConnectorTableMap::COL_DELETED_AT, SectionConnectorTableMap::COL_CREATED_AT, SectionConnectorTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'section_id', 'provider_id', 'type', 'baseurl', 'remote_section_id', 'deleted_at', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'SectionId' => 1, 'ProviderId' => 2, 'Type' => 3, 'Baseurl' => 4, 'RemoteSectionId' => 5, 'DeletedAt' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'sectionId' => 1, 'providerId' => 2, 'type' => 3, 'baseurl' => 4, 'remoteSectionId' => 5, 'deletedAt' => 6, 'createdAt' => 7, 'updatedAt' => 8, ),
        self::TYPE_COLNAME       => array(SectionConnectorTableMap::COL_ID => 0, SectionConnectorTableMap::COL_SECTION_ID => 1, SectionConnectorTableMap::COL_PROVIDER_ID => 2, SectionConnectorTableMap::COL_TYPE => 3, SectionConnectorTableMap::COL_BASEURL => 4, SectionConnectorTableMap::COL_REMOTE_SECTION_ID => 5, SectionConnectorTableMap::COL_DELETED_AT => 6, SectionConnectorTableMap::COL_CREATED_AT => 7, SectionConnectorTableMap::COL_UPDATED_AT => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'section_id' => 1, 'provider_id' => 2, 'type' => 3, 'baseurl' => 4, 'remote_section_id' => 5, 'deleted_at' => 6, 'created_at' => 7, 'updated_at' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('section_connector');
        $this->setPhpName('SectionConnector');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\SectionConnector');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('section_id', 'SectionId', 'INTEGER', 'section', 'id', true, null, null);
        $this->addForeignKey('provider_id', 'ProviderId', 'INTEGER', 'remote_provider', 'id', true, null, null);
        $this->addColumn('type', 'Type', 'VARCHAR', true, 20, '');
        $this->addColumn('baseurl', 'Baseurl', 'VARCHAR', true, 255, '');
        $this->addForeignKey('remote_section_id', 'RemoteSectionId', 'INTEGER', 'section', 'id', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RemoteProvider', '\\Database\\HubPlus\\RemoteProvider', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':provider_id',
    1 => ':id',
  ),
), null, 'CASCADE', null, false);
        $this->addRelation('SectionRelatedBySectionId', '\\Database\\HubPlus\\Section', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':section_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('SectionRelatedByRemoteSectionId', '\\Database\\HubPlus\\Section', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':remote_section_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('RemoteExtraLog', '\\Database\\HubPlus\\RemoteExtraLog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':connector_id',
    1 => ':id',
  ),
), null, null, 'RemoteExtraLogs', false);
        $this->addRelation('RemoteLog', '\\Database\\HubPlus\\RemoteLog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':connector_id',
    1 => ':id',
  ),
), null, null, 'RemoteLogs', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
            'archivable' => array('archive_table' => '', 'archive_phpname' => '', 'archive_class' => '', 'log_archived_at' => 'true', 'archived_at_column' => 'archived_at', 'archive_on_insert' => 'false', 'archive_on_update' => 'false', 'archive_on_delete' => 'true', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SectionConnectorTableMap::CLASS_DEFAULT : SectionConnectorTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SectionConnector object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SectionConnectorTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SectionConnectorTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SectionConnectorTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SectionConnectorTableMap::OM_CLASS;
            /** @var SectionConnector $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SectionConnectorTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SectionConnectorTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SectionConnectorTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SectionConnector $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SectionConnectorTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SectionConnectorTableMap::COL_ID);
            $criteria->addSelectColumn(SectionConnectorTableMap::COL_SECTION_ID);
            $criteria->addSelectColumn(SectionConnectorTableMap::COL_PROVIDER_ID);
            $criteria->addSelectColumn(SectionConnectorTableMap::COL_TYPE);
            $criteria->addSelectColumn(SectionConnectorTableMap::COL_BASEURL);
            $criteria->addSelectColumn(SectionConnectorTableMap::COL_REMOTE_SECTION_ID);
            $criteria->addSelectColumn(SectionConnectorTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(SectionConnectorTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(SectionConnectorTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.section_id');
            $criteria->addSelectColumn($alias . '.provider_id');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.baseurl');
            $criteria->addSelectColumn($alias . '.remote_section_id');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SectionConnectorTableMap::DATABASE_NAME)->getTable(SectionConnectorTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SectionConnectorTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SectionConnectorTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SectionConnectorTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SectionConnector or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SectionConnector object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionConnectorTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\SectionConnector) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SectionConnectorTableMap::DATABASE_NAME);
            $criteria->add(SectionConnectorTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SectionConnectorQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SectionConnectorTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SectionConnectorTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the section_connector table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SectionConnectorQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SectionConnector or Criteria object.
     *
     * @param mixed               $criteria Criteria or SectionConnector object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SectionConnectorTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SectionConnector object
        }

        if ($criteria->containsKey(SectionConnectorTableMap::COL_ID) && $criteria->keyContainsValue(SectionConnectorTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SectionConnectorTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SectionConnectorQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SectionConnectorTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SectionConnectorTableMap::buildTableMap();
