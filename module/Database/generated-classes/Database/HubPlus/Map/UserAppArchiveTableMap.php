<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\UserAppArchive;
use Database\HubPlus\UserAppArchiveQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'user_app_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UserAppArchiveTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.UserAppArchiveTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'user_app_archive';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\UserAppArchive';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.UserAppArchive';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 30;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 30;

    /**
     * the column name for the id field
     */
    const COL_ID = 'user_app_archive.id';

    /**
     * the column name for the username field
     */
    const COL_USERNAME = 'user_app_archive.username';

    /**
     * the column name for the username_canonical field
     */
    const COL_USERNAME_CANONICAL = 'user_app_archive.username_canonical';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'user_app_archive.email';

    /**
     * the column name for the email_canonical field
     */
    const COL_EMAIL_CANONICAL = 'user_app_archive.email_canonical';

    /**
     * the column name for the enabled field
     */
    const COL_ENABLED = 'user_app_archive.enabled';

    /**
     * the column name for the privacy field
     */
    const COL_PRIVACY = 'user_app_archive.privacy';

    /**
     * the column name for the terms field
     */
    const COL_TERMS = 'user_app_archive.terms';

    /**
     * the column name for the salt field
     */
    const COL_SALT = 'user_app_archive.salt';

    /**
     * the column name for the password field
     */
    const COL_PASSWORD = 'user_app_archive.password';

    /**
     * the column name for the last_login field
     */
    const COL_LAST_LOGIN = 'user_app_archive.last_login';

    /**
     * the column name for the locked field
     */
    const COL_LOCKED = 'user_app_archive.locked';

    /**
     * the column name for the expired field
     */
    const COL_EXPIRED = 'user_app_archive.expired';

    /**
     * the column name for the expires_at field
     */
    const COL_EXPIRES_AT = 'user_app_archive.expires_at';

    /**
     * the column name for the confirmation_token field
     */
    const COL_CONFIRMATION_TOKEN = 'user_app_archive.confirmation_token';

    /**
     * the column name for the password_requested_at field
     */
    const COL_PASSWORD_REQUESTED_AT = 'user_app_archive.password_requested_at';

    /**
     * the column name for the roles field
     */
    const COL_ROLES = 'user_app_archive.roles';

    /**
     * the column name for the credentials_expired field
     */
    const COL_CREDENTIALS_EXPIRED = 'user_app_archive.credentials_expired';

    /**
     * the column name for the credentials_expire_at field
     */
    const COL_CREDENTIALS_EXPIRE_AT = 'user_app_archive.credentials_expire_at';

    /**
     * the column name for the firstname field
     */
    const COL_FIRSTNAME = 'user_app_archive.firstname';

    /**
     * the column name for the lastname field
     */
    const COL_LASTNAME = 'user_app_archive.lastname';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'user_app_archive.phone';

    /**
     * the column name for the imagePath field
     */
    const COL_IMAGEPATH = 'user_app_archive.imagePath';

    /**
     * the column name for the address_id field
     */
    const COL_ADDRESS_ID = 'user_app_archive.address_id';

    /**
     * the column name for the tags field
     */
    const COL_TAGS = 'user_app_archive.tags';

    /**
     * the column name for the section_id field
     */
    const COL_SECTION_ID = 'user_app_archive.section_id';

    /**
     * the column name for the deleted_at field
     */
    const COL_DELETED_AT = 'user_app_archive.deleted_at';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'user_app_archive.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'user_app_archive.updated_at';

    /**
     * the column name for the archived_at field
     */
    const COL_ARCHIVED_AT = 'user_app_archive.archived_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Username', 'UsernameCanonical', 'Email', 'EmailCanonical', 'Enabled', 'Privacy', 'Terms', 'Salt', 'Password', 'LastLogin', 'Locked', 'Expired', 'ExpiresAt', 'ConfirmationToken', 'PasswordRequestedAt', 'Roles', 'CredentialsExpired', 'CredentialsExpireAt', 'Firstname', 'Lastname', 'Phone', 'Imagepath', 'AddressId', 'Tags', 'SectionId', 'DeletedAt', 'CreatedAt', 'UpdatedAt', 'ArchivedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'username', 'usernameCanonical', 'email', 'emailCanonical', 'enabled', 'privacy', 'terms', 'salt', 'password', 'lastLogin', 'locked', 'expired', 'expiresAt', 'confirmationToken', 'passwordRequestedAt', 'roles', 'credentialsExpired', 'credentialsExpireAt', 'firstname', 'lastname', 'phone', 'imagepath', 'addressId', 'tags', 'sectionId', 'deletedAt', 'createdAt', 'updatedAt', 'archivedAt', ),
        self::TYPE_COLNAME       => array(UserAppArchiveTableMap::COL_ID, UserAppArchiveTableMap::COL_USERNAME, UserAppArchiveTableMap::COL_USERNAME_CANONICAL, UserAppArchiveTableMap::COL_EMAIL, UserAppArchiveTableMap::COL_EMAIL_CANONICAL, UserAppArchiveTableMap::COL_ENABLED, UserAppArchiveTableMap::COL_PRIVACY, UserAppArchiveTableMap::COL_TERMS, UserAppArchiveTableMap::COL_SALT, UserAppArchiveTableMap::COL_PASSWORD, UserAppArchiveTableMap::COL_LAST_LOGIN, UserAppArchiveTableMap::COL_LOCKED, UserAppArchiveTableMap::COL_EXPIRED, UserAppArchiveTableMap::COL_EXPIRES_AT, UserAppArchiveTableMap::COL_CONFIRMATION_TOKEN, UserAppArchiveTableMap::COL_PASSWORD_REQUESTED_AT, UserAppArchiveTableMap::COL_ROLES, UserAppArchiveTableMap::COL_CREDENTIALS_EXPIRED, UserAppArchiveTableMap::COL_CREDENTIALS_EXPIRE_AT, UserAppArchiveTableMap::COL_FIRSTNAME, UserAppArchiveTableMap::COL_LASTNAME, UserAppArchiveTableMap::COL_PHONE, UserAppArchiveTableMap::COL_IMAGEPATH, UserAppArchiveTableMap::COL_ADDRESS_ID, UserAppArchiveTableMap::COL_TAGS, UserAppArchiveTableMap::COL_SECTION_ID, UserAppArchiveTableMap::COL_DELETED_AT, UserAppArchiveTableMap::COL_CREATED_AT, UserAppArchiveTableMap::COL_UPDATED_AT, UserAppArchiveTableMap::COL_ARCHIVED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'username', 'username_canonical', 'email', 'email_canonical', 'enabled', 'privacy', 'terms', 'salt', 'password', 'last_login', 'locked', 'expired', 'expires_at', 'confirmation_token', 'password_requested_at', 'roles', 'credentials_expired', 'credentials_expire_at', 'firstname', 'lastname', 'phone', 'imagePath', 'address_id', 'tags', 'section_id', 'deleted_at', 'created_at', 'updated_at', 'archived_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Username' => 1, 'UsernameCanonical' => 2, 'Email' => 3, 'EmailCanonical' => 4, 'Enabled' => 5, 'Privacy' => 6, 'Terms' => 7, 'Salt' => 8, 'Password' => 9, 'LastLogin' => 10, 'Locked' => 11, 'Expired' => 12, 'ExpiresAt' => 13, 'ConfirmationToken' => 14, 'PasswordRequestedAt' => 15, 'Roles' => 16, 'CredentialsExpired' => 17, 'CredentialsExpireAt' => 18, 'Firstname' => 19, 'Lastname' => 20, 'Phone' => 21, 'Imagepath' => 22, 'AddressId' => 23, 'Tags' => 24, 'SectionId' => 25, 'DeletedAt' => 26, 'CreatedAt' => 27, 'UpdatedAt' => 28, 'ArchivedAt' => 29, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'username' => 1, 'usernameCanonical' => 2, 'email' => 3, 'emailCanonical' => 4, 'enabled' => 5, 'privacy' => 6, 'terms' => 7, 'salt' => 8, 'password' => 9, 'lastLogin' => 10, 'locked' => 11, 'expired' => 12, 'expiresAt' => 13, 'confirmationToken' => 14, 'passwordRequestedAt' => 15, 'roles' => 16, 'credentialsExpired' => 17, 'credentialsExpireAt' => 18, 'firstname' => 19, 'lastname' => 20, 'phone' => 21, 'imagepath' => 22, 'addressId' => 23, 'tags' => 24, 'sectionId' => 25, 'deletedAt' => 26, 'createdAt' => 27, 'updatedAt' => 28, 'archivedAt' => 29, ),
        self::TYPE_COLNAME       => array(UserAppArchiveTableMap::COL_ID => 0, UserAppArchiveTableMap::COL_USERNAME => 1, UserAppArchiveTableMap::COL_USERNAME_CANONICAL => 2, UserAppArchiveTableMap::COL_EMAIL => 3, UserAppArchiveTableMap::COL_EMAIL_CANONICAL => 4, UserAppArchiveTableMap::COL_ENABLED => 5, UserAppArchiveTableMap::COL_PRIVACY => 6, UserAppArchiveTableMap::COL_TERMS => 7, UserAppArchiveTableMap::COL_SALT => 8, UserAppArchiveTableMap::COL_PASSWORD => 9, UserAppArchiveTableMap::COL_LAST_LOGIN => 10, UserAppArchiveTableMap::COL_LOCKED => 11, UserAppArchiveTableMap::COL_EXPIRED => 12, UserAppArchiveTableMap::COL_EXPIRES_AT => 13, UserAppArchiveTableMap::COL_CONFIRMATION_TOKEN => 14, UserAppArchiveTableMap::COL_PASSWORD_REQUESTED_AT => 15, UserAppArchiveTableMap::COL_ROLES => 16, UserAppArchiveTableMap::COL_CREDENTIALS_EXPIRED => 17, UserAppArchiveTableMap::COL_CREDENTIALS_EXPIRE_AT => 18, UserAppArchiveTableMap::COL_FIRSTNAME => 19, UserAppArchiveTableMap::COL_LASTNAME => 20, UserAppArchiveTableMap::COL_PHONE => 21, UserAppArchiveTableMap::COL_IMAGEPATH => 22, UserAppArchiveTableMap::COL_ADDRESS_ID => 23, UserAppArchiveTableMap::COL_TAGS => 24, UserAppArchiveTableMap::COL_SECTION_ID => 25, UserAppArchiveTableMap::COL_DELETED_AT => 26, UserAppArchiveTableMap::COL_CREATED_AT => 27, UserAppArchiveTableMap::COL_UPDATED_AT => 28, UserAppArchiveTableMap::COL_ARCHIVED_AT => 29, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'username' => 1, 'username_canonical' => 2, 'email' => 3, 'email_canonical' => 4, 'enabled' => 5, 'privacy' => 6, 'terms' => 7, 'salt' => 8, 'password' => 9, 'last_login' => 10, 'locked' => 11, 'expired' => 12, 'expires_at' => 13, 'confirmation_token' => 14, 'password_requested_at' => 15, 'roles' => 16, 'credentials_expired' => 17, 'credentials_expire_at' => 18, 'firstname' => 19, 'lastname' => 20, 'phone' => 21, 'imagePath' => 22, 'address_id' => 23, 'tags' => 24, 'section_id' => 25, 'deleted_at' => 26, 'created_at' => 27, 'updated_at' => 28, 'archived_at' => 29, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user_app_archive');
        $this->setPhpName('UserAppArchive');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\UserAppArchive');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 255, null);
        $this->addColumn('username_canonical', 'UsernameCanonical', 'VARCHAR', true, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 255, null);
        $this->addColumn('email_canonical', 'EmailCanonical', 'VARCHAR', true, 255, null);
        $this->addColumn('enabled', 'Enabled', 'BOOLEAN', true, 1, null);
        $this->addColumn('privacy', 'Privacy', 'BOOLEAN', false, 1, null);
        $this->addColumn('terms', 'Terms', 'BOOLEAN', false, 1, null);
        $this->addColumn('salt', 'Salt', 'VARCHAR', true, 255, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 255, null);
        $this->addColumn('last_login', 'LastLogin', 'TIMESTAMP', false, null, null);
        $this->addColumn('locked', 'Locked', 'BOOLEAN', true, 1, null);
        $this->addColumn('expired', 'Expired', 'BOOLEAN', true, 1, null);
        $this->addColumn('expires_at', 'ExpiresAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('confirmation_token', 'ConfirmationToken', 'VARCHAR', false, 255, null);
        $this->addColumn('password_requested_at', 'PasswordRequestedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('roles', 'Roles', 'CLOB', true, null, null);
        $this->addColumn('credentials_expired', 'CredentialsExpired', 'BOOLEAN', true, 1, null);
        $this->addColumn('credentials_expire_at', 'CredentialsExpireAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('firstname', 'Firstname', 'VARCHAR', false, 255, null);
        $this->addColumn('lastname', 'Lastname', 'VARCHAR', false, 255, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', false, 255, null);
        $this->addColumn('imagePath', 'Imagepath', 'VARCHAR', false, 255, null);
        $this->addColumn('address_id', 'AddressId', 'INTEGER', false, null, null);
        $this->addColumn('tags', 'Tags', 'CLOB', false, null, null);
        $this->addColumn('section_id', 'SectionId', 'INTEGER', false, null, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('archived_at', 'ArchivedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UserAppArchiveTableMap::CLASS_DEFAULT : UserAppArchiveTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UserAppArchive object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UserAppArchiveTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UserAppArchiveTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UserAppArchiveTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UserAppArchiveTableMap::OM_CLASS;
            /** @var UserAppArchive $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UserAppArchiveTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UserAppArchiveTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UserAppArchiveTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UserAppArchive $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UserAppArchiveTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_ID);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_USERNAME);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_USERNAME_CANONICAL);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_EMAIL);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_EMAIL_CANONICAL);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_ENABLED);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_PRIVACY);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_TERMS);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_SALT);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_PASSWORD);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_LAST_LOGIN);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_LOCKED);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_EXPIRED);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_EXPIRES_AT);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_CONFIRMATION_TOKEN);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_PASSWORD_REQUESTED_AT);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_ROLES);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_CREDENTIALS_EXPIRED);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_CREDENTIALS_EXPIRE_AT);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_FIRSTNAME);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_LASTNAME);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_PHONE);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_IMAGEPATH);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_ADDRESS_ID);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_TAGS);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_SECTION_ID);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_DELETED_AT);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_UPDATED_AT);
            $criteria->addSelectColumn(UserAppArchiveTableMap::COL_ARCHIVED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.username');
            $criteria->addSelectColumn($alias . '.username_canonical');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.email_canonical');
            $criteria->addSelectColumn($alias . '.enabled');
            $criteria->addSelectColumn($alias . '.privacy');
            $criteria->addSelectColumn($alias . '.terms');
            $criteria->addSelectColumn($alias . '.salt');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.last_login');
            $criteria->addSelectColumn($alias . '.locked');
            $criteria->addSelectColumn($alias . '.expired');
            $criteria->addSelectColumn($alias . '.expires_at');
            $criteria->addSelectColumn($alias . '.confirmation_token');
            $criteria->addSelectColumn($alias . '.password_requested_at');
            $criteria->addSelectColumn($alias . '.roles');
            $criteria->addSelectColumn($alias . '.credentials_expired');
            $criteria->addSelectColumn($alias . '.credentials_expire_at');
            $criteria->addSelectColumn($alias . '.firstname');
            $criteria->addSelectColumn($alias . '.lastname');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.imagePath');
            $criteria->addSelectColumn($alias . '.address_id');
            $criteria->addSelectColumn($alias . '.tags');
            $criteria->addSelectColumn($alias . '.section_id');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
            $criteria->addSelectColumn($alias . '.archived_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UserAppArchiveTableMap::DATABASE_NAME)->getTable(UserAppArchiveTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UserAppArchiveTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UserAppArchiveTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UserAppArchiveTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UserAppArchive or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UserAppArchive object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAppArchiveTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\UserAppArchive) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UserAppArchiveTableMap::DATABASE_NAME);
            $criteria->add(UserAppArchiveTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = UserAppArchiveQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UserAppArchiveTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UserAppArchiveTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the user_app_archive table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UserAppArchiveQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UserAppArchive or Criteria object.
     *
     * @param mixed               $criteria Criteria or UserAppArchive object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserAppArchiveTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UserAppArchive object
        }


        // Set the correct dbName
        $query = UserAppArchiveQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UserAppArchiveTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UserAppArchiveTableMap::buildTableMap();
