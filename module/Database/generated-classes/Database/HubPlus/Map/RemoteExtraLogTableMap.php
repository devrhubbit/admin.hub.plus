<?php

namespace Database\HubPlus\Map;

use Database\HubPlus\RemoteExtraLog;
use Database\HubPlus\RemoteExtraLogQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'remote_extra_log' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class RemoteExtraLogTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Database.HubPlus.Map.RemoteExtraLogTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hp_core';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'remote_extra_log';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Database\\HubPlus\\RemoteExtraLog';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Database.HubPlus.RemoteExtraLog';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the id field
     */
    const COL_ID = 'remote_extra_log.id';

    /**
     * the column name for the connector_id field
     */
    const COL_CONNECTOR_ID = 'remote_extra_log.connector_id';

    /**
     * the column name for the remote_id field
     */
    const COL_REMOTE_ID = 'remote_extra_log.remote_id';

    /**
     * the column name for the device_id field
     */
    const COL_DEVICE_ID = 'remote_extra_log.device_id';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'remote_extra_log.type';

    /**
     * the column name for the content_type field
     */
    const COL_CONTENT_TYPE = 'remote_extra_log.content_type';

    /**
     * the column name for the timestamp field
     */
    const COL_TIMESTAMP = 'remote_extra_log.timestamp';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ConnectorId', 'RemoteId', 'DeviceId', 'Type', 'ContentType', 'Timestamp', ),
        self::TYPE_CAMELNAME     => array('id', 'connectorId', 'remoteId', 'deviceId', 'type', 'contentType', 'timestamp', ),
        self::TYPE_COLNAME       => array(RemoteExtraLogTableMap::COL_ID, RemoteExtraLogTableMap::COL_CONNECTOR_ID, RemoteExtraLogTableMap::COL_REMOTE_ID, RemoteExtraLogTableMap::COL_DEVICE_ID, RemoteExtraLogTableMap::COL_TYPE, RemoteExtraLogTableMap::COL_CONTENT_TYPE, RemoteExtraLogTableMap::COL_TIMESTAMP, ),
        self::TYPE_FIELDNAME     => array('id', 'connector_id', 'remote_id', 'device_id', 'type', 'content_type', 'timestamp', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ConnectorId' => 1, 'RemoteId' => 2, 'DeviceId' => 3, 'Type' => 4, 'ContentType' => 5, 'Timestamp' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'connectorId' => 1, 'remoteId' => 2, 'deviceId' => 3, 'type' => 4, 'contentType' => 5, 'timestamp' => 6, ),
        self::TYPE_COLNAME       => array(RemoteExtraLogTableMap::COL_ID => 0, RemoteExtraLogTableMap::COL_CONNECTOR_ID => 1, RemoteExtraLogTableMap::COL_REMOTE_ID => 2, RemoteExtraLogTableMap::COL_DEVICE_ID => 3, RemoteExtraLogTableMap::COL_TYPE => 4, RemoteExtraLogTableMap::COL_CONTENT_TYPE => 5, RemoteExtraLogTableMap::COL_TIMESTAMP => 6, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'connector_id' => 1, 'remote_id' => 2, 'device_id' => 3, 'type' => 4, 'content_type' => 5, 'timestamp' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('remote_extra_log');
        $this->setPhpName('RemoteExtraLog');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Database\\HubPlus\\RemoteExtraLog');
        $this->setPackage('Database.HubPlus');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('connector_id', 'ConnectorId', 'INTEGER', 'section_connector', 'id', false, null, null);
        $this->addColumn('remote_id', 'RemoteId', 'VARCHAR', false, 255, null);
        $this->addForeignKey('device_id', 'DeviceId', 'INTEGER', 'device', 'id', false, null, null);
        $this->addColumn('type', 'Type', 'VARCHAR', false, 10, null);
        $this->addColumn('content_type', 'ContentType', 'VARCHAR', false, 10, null);
        $this->addColumn('timestamp', 'Timestamp', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Device', '\\Database\\HubPlus\\Device', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':device_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('SectionConnector', '\\Database\\HubPlus\\SectionConnector', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':connector_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? RemoteExtraLogTableMap::CLASS_DEFAULT : RemoteExtraLogTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (RemoteExtraLog object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = RemoteExtraLogTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = RemoteExtraLogTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + RemoteExtraLogTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = RemoteExtraLogTableMap::OM_CLASS;
            /** @var RemoteExtraLog $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            RemoteExtraLogTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = RemoteExtraLogTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = RemoteExtraLogTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var RemoteExtraLog $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                RemoteExtraLogTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(RemoteExtraLogTableMap::COL_ID);
            $criteria->addSelectColumn(RemoteExtraLogTableMap::COL_CONNECTOR_ID);
            $criteria->addSelectColumn(RemoteExtraLogTableMap::COL_REMOTE_ID);
            $criteria->addSelectColumn(RemoteExtraLogTableMap::COL_DEVICE_ID);
            $criteria->addSelectColumn(RemoteExtraLogTableMap::COL_TYPE);
            $criteria->addSelectColumn(RemoteExtraLogTableMap::COL_CONTENT_TYPE);
            $criteria->addSelectColumn(RemoteExtraLogTableMap::COL_TIMESTAMP);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.connector_id');
            $criteria->addSelectColumn($alias . '.remote_id');
            $criteria->addSelectColumn($alias . '.device_id');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.content_type');
            $criteria->addSelectColumn($alias . '.timestamp');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(RemoteExtraLogTableMap::DATABASE_NAME)->getTable(RemoteExtraLogTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(RemoteExtraLogTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(RemoteExtraLogTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new RemoteExtraLogTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a RemoteExtraLog or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or RemoteExtraLog object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteExtraLogTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Database\HubPlus\RemoteExtraLog) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(RemoteExtraLogTableMap::DATABASE_NAME);
            $criteria->add(RemoteExtraLogTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = RemoteExtraLogQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            RemoteExtraLogTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                RemoteExtraLogTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the remote_extra_log table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return RemoteExtraLogQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a RemoteExtraLog or Criteria object.
     *
     * @param mixed               $criteria Criteria or RemoteExtraLog object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RemoteExtraLogTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from RemoteExtraLog object
        }

        if ($criteria->containsKey(RemoteExtraLogTableMap::COL_ID) && $criteria->keyContainsValue(RemoteExtraLogTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.RemoteExtraLogTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = RemoteExtraLogQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // RemoteExtraLogTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
RemoteExtraLogTableMap::buildTableMap();
