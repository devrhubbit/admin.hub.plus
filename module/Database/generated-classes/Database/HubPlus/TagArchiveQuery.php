<?php

namespace Database\HubPlus;

use Database\HubPlus\Base\TagArchiveQuery as BaseTagArchiveQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'tag_archive' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TagArchiveQuery extends BaseTagArchiveQuery
{

}
