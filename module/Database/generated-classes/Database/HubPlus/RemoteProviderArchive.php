<?php

namespace Database\HubPlus;

use Database\HubPlus\Base\RemoteProviderArchive as BaseRemoteProviderArchive;

/**
 * Skeleton subclass for representing a row from the 'remote_provider_archive' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RemoteProviderArchive extends BaseRemoteProviderArchive
{

}
