<?php

if (file_exists("propel.parameters.^#DB_NAME#^.php")) {
    include_once "propel.parameters.^#DB_NAME#^.php";
} else {
    include_once "propel.parameters.local.php";
}

return [
    'propel' => [
        'database' => [
            'connections' => [
                '%env.propel_name%' => [
                    'adapter' => 'mysql',
                    'classname' => 'Propel\Runtime\Connection\ConnectionWrapper',
                    'dsn' => 'mysql:host=%env.propel_host%;dbname=%env.propel_name%',
                    'user' => '%env.propel_user%',
                    'password' => '%env.propel_password%',
                    'attributes' => [],
                    'settings' => [
                        'charset' => 'utf8'
                    ]
                ],
            ]
        ],
        'exclude_tables' => [
            'queue_*',
            'payments_*',
            'custom_form_*'
        ],
        'runtime' => [
            'defaultConnection' => '%env.propel_name%',
            'connections' => ['%env.propel_name%']
        ],
        'generator' => [
            'defaultConnection' => '%env.propel_name%',
            'connections' => ['%env.propel_name%']
        ]
    ]
];