
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- address
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `address`;

CREATE TABLE `address`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `address` VARCHAR(255),
    `city` VARCHAR(255),
    `state` VARCHAR(255),
    `country` VARCHAR(255),
    `zipcode` VARCHAR(255),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- application
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `application`;

CREATE TABLE `application`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(255),
    `slug` VARCHAR(255),
    `bundle` VARCHAR(255),
    `description` TEXT,
    `app_key` VARCHAR(255),
    `dsn` VARCHAR(255),
    `db_host` VARCHAR(255),
    `db_name` VARCHAR(255),
    `db_user` VARCHAR(255),
    `db_pwd` VARCHAR(255),
    `api_version` VARCHAR(10),
    `pack_id` int(11) unsigned,
    `category_id` int(11) unsigned,
    `template_id` int(11) unsigned,
    `push_notification` TEXT,
    `max_notification` INTEGER DEFAULT 0,
    `max_advice_hour` INTEGER DEFAULT 0,
    `advice_hour_done` INTEGER DEFAULT 0,
    `max_content_insert` INTEGER DEFAULT 0,
    `icon` VARCHAR(255),
    `splash_screen` VARCHAR(255),
    `background_color` VARCHAR(7),
    `app_logo` VARCHAR(255),
    `layout` TEXT,
    `white_label` TINYINT(1),
    `demo` TINYINT(1),
    `facebook_token` VARCHAR(255),
    `apple_id` VARCHAR(255),
    `apple_id_password` VARCHAR(255),
    `apple_store_app_link` VARCHAR(255),
    `play_store_id` VARCHAR(255),
    `play_store_id_password` VARCHAR(255),
    `play_store_app_link` VARCHAR(255),
    `controller_uri` TEXT,
    `google_analytics_ua` TEXT,
    `published` TINYINT(1) DEFAULT 0 NOT NULL,
    `expired_at` DATETIME,
    `main_contents_language` VARCHAR(2),
    `other_contents_languages` VARCHAR(255),
    `deleted_at` DATETIME,
    `system_icons` TEXT NOT NULL,
    `toolbar` enum('SLIDESHOW','TITLE','ICON') DEFAULT 'SLIDESHOW' NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `bundle` (`bundle`),
    INDEX `template_index` (`template_id`),
    INDEX `category_id` (`category_id`),
    INDEX `pack_id` (`pack_id`),
    CONSTRAINT `application_ibfk_1`
        FOREIGN KEY (`category_id`)
        REFERENCES `category` (`id`),
    CONSTRAINT `application_ibfk_2`
        FOREIGN KEY (`template_id`)
        REFERENCES `template` (`id`),
    CONSTRAINT `application_ibfk_3`
        FOREIGN KEY (`pack_id`)
        REFERENCES `pack` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- category
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255),
    `weight` INTEGER,
    `visible` TINYINT(1) DEFAULT 1,
    `setting` TEXT,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cf_extra_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cf_extra_log`;

CREATE TABLE `cf_extra_log`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `form_id` int(11) unsigned,
    `row_id` int(11) unsigned,
    `device_id` int(11) unsigned,
    `type` VARCHAR(10),
    `timestamp` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `form_id` (`form_id`),
    INDEX `device_id` (`device_id`),
    CONSTRAINT `cf_extra_log_ibfk_1`
        FOREIGN KEY (`form_id`)
        REFERENCES `section` (`id`),
    CONSTRAINT `cf_extra_log_ibfk_2`
        FOREIGN KEY (`device_id`)
        REFERENCES `device` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cf_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cf_log`;

CREATE TABLE `cf_log`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `form_id` int(11) unsigned,
    `row_id` int(11) unsigned,
    `user_id` int(11) unsigned,
    `liked_at` DATETIME,
    `shared_at` DATETIME,
    `like_count` INTEGER DEFAULT 0,
    `share_count` INTEGER DEFAULT 0,
    `view_count` INTEGER DEFAULT 0,
    PRIMARY KEY (`id`),
    INDEX `form_id` (`form_id`),
    INDEX `user_id` (`user_id`),
    CONSTRAINT `cf_log_ibfk_1`
        FOREIGN KEY (`form_id`)
        REFERENCES `section` (`id`),
    CONSTRAINT `cf_log_ibfk_2`
        FOREIGN KEY (`user_id`)
        REFERENCES `user_app` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- device
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `device`;

CREATE TABLE `device`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) unsigned,
    `status` VARCHAR(20) NOT NULL,
    `type` VARCHAR(20) NOT NULL,
    `os` LONGTEXT NOT NULL,
    `os_version` LONGTEXT NOT NULL,
    `enable_notification` TINYINT(1),
    `api_version` VARCHAR(50),
    `language` VARCHAR(10),
    `demo` TINYINT(1),
    `token` LONGTEXT NOT NULL,
    `api_key` VARCHAR(255),
    `ip_address` VARCHAR(50),
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `IDX_E83B3B8A76ED395` (`user_id`),
    CONSTRAINT `device_ibfk_1`
        FOREIGN KEY (`user_id`)
        REFERENCES `user_app` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- gallery
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery`
(
    `media_id` int(11) unsigned NOT NULL,
    `post_id` int(11) unsigned NOT NULL,
    `weight` INTEGER,
    PRIMARY KEY (`media_id`,`post_id`),
    UNIQUE INDEX `gallery_u_546ef0` (`media_id`, `post_id`),
    INDEX `media_id` (`media_id`),
    INDEX `post_id` (`post_id`),
    CONSTRAINT `gallery_ibfk_1`
        FOREIGN KEY (`media_id`)
        REFERENCES `media` (`id`),
    CONSTRAINT `gallery_ibfk_2`
        FOREIGN KEY (`post_id`)
        REFERENCES `post` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- gallery_form
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `gallery_form`;

CREATE TABLE `gallery_form`
(
    `media_id` int(11) unsigned,
    `form_id` int(11) unsigned,
    `row_id` int(11) unsigned,
    `field_name` VARCHAR(1024),
    `weight` INTEGER,
    INDEX `media_id` (`media_id`),
    INDEX `form_id` (`form_id`),
    CONSTRAINT `gallery_form_ibfk_1`
        FOREIGN KEY (`media_id`)
        REFERENCES `media` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- group
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `group`;

CREATE TABLE `group`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `label` VARCHAR(255) NOT NULL,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- media
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `device_id` int(11) unsigned,
    `is_public` TINYINT(1) DEFAULT 1 NOT NULL,
    `type` VARCHAR(20) NOT NULL,
    `uri` VARCHAR(255),
    `uri_thumb` VARCHAR(255),
    `extra_params` TEXT,
    `weight` INTEGER NOT NULL,
    `title` VARCHAR(255),
    `description` TEXT,
    `extension` VARCHAR(20),
    `mime_type` VARCHAR(20),
    `size` INTEGER,
    `count_like` INTEGER DEFAULT 0,
    `count_share` INTEGER DEFAULT 0,
    `format` TEXT,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `device_id` (`device_id`),
    CONSTRAINT `media_ibfk_1`
        FOREIGN KEY (`device_id`)
        REFERENCES `device` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- media_extra_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `media_extra_log`;

CREATE TABLE `media_extra_log`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `media_id` int(11) unsigned,
    `device_id` int(11) unsigned,
    `type` VARCHAR(10),
    `timestamp` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `media_id` (`media_id`),
    INDEX `device_id` (`device_id`),
    CONSTRAINT `media_extra_log_ibfk_1`
        FOREIGN KEY (`media_id`)
        REFERENCES `media` (`id`),
    CONSTRAINT `media_extra_log_ibfk_2`
        FOREIGN KEY (`device_id`)
        REFERENCES `device` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- media_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `media_log`;

CREATE TABLE `media_log`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `media_id` int(11) unsigned,
    `user_id` int(11) unsigned,
    `liked_at` DATETIME,
    `shared_at` DATETIME,
    `share_count` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `media_id` (`media_id`),
    INDEX `user_id` (`user_id`),
    CONSTRAINT `media_log_ibfk_1`
        FOREIGN KEY (`media_id`)
        REFERENCES `media` (`id`),
    CONSTRAINT `media_log_ibfk_2`
        FOREIGN KEY (`user_id`)
        REFERENCES `user_app` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- pack
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `pack`;

CREATE TABLE `pack`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255),
    `bundle` VARCHAR(255),
    `json_constraints` TEXT,
    `expired` TINYINT(1),
    `expired_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post`;

CREATE TABLE `post`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(20),
    `status` TINYINT(3),
    `author_id` int(11) unsigned,
    `cover_id` int(11) unsigned,
    `slug` VARCHAR(255),
    `title` TEXT,
    `subtitle` TEXT,
    `description` TEXT,
    `body` TEXT,
    `search` TEXT,
    `template_id` int(11) unsigned NOT NULL,
    `layout` TEXT,
    `tags` TEXT,
    `tags_user` TEXT,
    `row_data` TEXT,
    `count_like` INTEGER DEFAULT 0,
    `count_share` INTEGER DEFAULT 0,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `template_id` (`template_id`),
    INDEX `cover_id` (`cover_id`),
    FULLTEXT INDEX `post_i_a5db2a` (`tags`),
    FULLTEXT INDEX `post_i_e26ddd` (`tags_user`),
    INDEX `post_ibfi_3` (`author_id`),
    CONSTRAINT `post_ibfk_1`
        FOREIGN KEY (`template_id`)
        REFERENCES `template` (`id`),
    CONSTRAINT `post_ibfk_2`
        FOREIGN KEY (`cover_id`)
        REFERENCES `media` (`id`),
    CONSTRAINT `post_ibfk_3`
        FOREIGN KEY (`author_id`)
        REFERENCES `user_backend` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_action
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_action`;

CREATE TABLE `post_action`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `post_id` int(11) unsigned NOT NULL,
    `area` VARCHAR(50),
    `type` VARCHAR(10) NOT NULL,
    `label` VARCHAR(255) NOT NULL,
    `cover_id` int(11) unsigned,
    `params` LONGTEXT,
    `weight` INTEGER,
    `section_id` int(11) unsigned,
    `content_id` int(11) unsigned,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `post_id` (`post_id`),
    INDEX `section_id` (`section_id`),
    INDEX `content_id` (`content_id`),
    INDEX `post_action_ibfi_4` (`cover_id`),
    CONSTRAINT `post_action_ibfk_1`
        FOREIGN KEY (`post_id`)
        REFERENCES `post` (`id`),
    CONSTRAINT `post_action_ibfk_2`
        FOREIGN KEY (`section_id`)
        REFERENCES `section` (`id`),
    CONSTRAINT `post_action_ibfk_3`
        FOREIGN KEY (`content_id`)
        REFERENCES `post` (`id`),
    CONSTRAINT `post_action_ibfk_4`
        FOREIGN KEY (`cover_id`)
        REFERENCES `media` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_connector
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_connector`;

CREATE TABLE `post_connector`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `post_id` int(11) unsigned NOT NULL,
    `provider_id` int(11) unsigned NOT NULL,
    `type` VARCHAR(20) DEFAULT '' NOT NULL,
    `baseurl` VARCHAR(255) DEFAULT '' NOT NULL,
    `remote_post_id` int(11) unsigned,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `post_key` (`post_id`),
    INDEX `provider_key` (`provider_id`),
    CONSTRAINT `post_connector_ibfk_1`
        FOREIGN KEY (`provider_id`)
        REFERENCES `remote_provider` (`id`)
        ON UPDATE CASCADE,
    CONSTRAINT `post_key`
        FOREIGN KEY (`post_id`)
        REFERENCES `post` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_event
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_event`;

CREATE TABLE `post_event`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `post_id` int(11) unsigned,
    `start_at` DATETIME,
    `end_at` DATETIME,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `post_id` (`post_id`),
    CONSTRAINT `post_event_ibfk_1`
        FOREIGN KEY (`post_id`)
        REFERENCES `post` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_extra_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_extra_log`;

CREATE TABLE `post_extra_log`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `post_id` int(11) unsigned,
    `device_id` int(11) unsigned,
    `type` VARCHAR(10),
    `timestamp` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `post_id` (`post_id`),
    INDEX `device_id` (`device_id`),
    CONSTRAINT `post_extra_log_ibfk_1`
        FOREIGN KEY (`post_id`)
        REFERENCES `post` (`id`),
    CONSTRAINT `post_extra_log_ibfk_2`
        FOREIGN KEY (`device_id`)
        REFERENCES `device` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_log`;

CREATE TABLE `post_log`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `post_id` int(11) unsigned,
    `user_id` int(11) unsigned,
    `liked_at` DATETIME,
    `shared_at` DATETIME,
    `share_count` INTEGER,
    `view_count` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `post_id` (`post_id`),
    INDEX `user_id` (`user_id`),
    CONSTRAINT `post_log_ibfk_1`
        FOREIGN KEY (`post_id`)
        REFERENCES `post` (`id`),
    CONSTRAINT `post_log_ibfk_2`
        FOREIGN KEY (`user_id`)
        REFERENCES `user_app` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_person
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_person`;

CREATE TABLE `post_person`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `post_id` int(11) unsigned,
    `firstname` VARCHAR(127),
    `lastname` VARCHAR(127),
    `birthday` DATETIME,
    `death` DATETIME,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `post_id` (`post_id`),
    CONSTRAINT `post_person_ibfk_1`
        FOREIGN KEY (`post_id`)
        REFERENCES `post` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_poi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_poi`;

CREATE TABLE `post_poi`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `post_id` int(11) unsigned,
    `identifier` VARCHAR(20) NOT NULL,
    `type` VARCHAR(20),
    `lat` FLOAT(10,6),
    `lng` FLOAT(10,6),
    `radius` INTEGER,
    `uuid` VARCHAR(255),
    `major` smallint(1) unsigned,
    `minor` smallint(1) unsigned,
    `distance` VARCHAR(10),
    `pin` VARCHAR(255),
    `poi_notification` TINYINT(1) DEFAULT 0,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `unique_identifier` (`identifier`),
    INDEX `post_id` (`post_id`),
    CONSTRAINT `post_poi_ibfk_1`
        FOREIGN KEY (`post_id`)
        REFERENCES `post` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- push_notification
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `push_notification`;

CREATE TABLE `push_notification`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `lang` VARCHAR(2),
    `title` VARCHAR(255) DEFAULT '',
    `body` TEXT NOT NULL,
    `section_id` int(11) unsigned,
    `post_id` int(11) unsigned,
    `send_date` DATETIME NOT NULL,
    `receiver_filter` TEXT,
    `device_count` int(11) unsigned,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `section_id` (`section_id`),
    INDEX `post_id` (`post_id`),
    FULLTEXT INDEX `push_notification_i_03d6e2` (`receiver_filter`),
    CONSTRAINT `push_notification_ibfk_1`
        FOREIGN KEY (`section_id`)
        REFERENCES `section` (`id`),
    CONSTRAINT `push_notification_ibfk_2`
        FOREIGN KEY (`post_id`)
        REFERENCES `post` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- push_notification_device
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `push_notification_device`;

CREATE TABLE `push_notification_device`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `notification_id` int(11) unsigned NOT NULL,
    `device_id` int(11) unsigned NOT NULL,
    `push_notification_hash_id` VARCHAR(32) DEFAULT '' NOT NULL,
    `read_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `notification_id` (`notification_id`),
    INDEX `device_id` (`device_id`),
    CONSTRAINT `push_notification_device_ibfk_1`
        FOREIGN KEY (`notification_id`)
        REFERENCES `push_notification` (`id`),
    CONSTRAINT `push_notification_device_ibfk_2`
        FOREIGN KEY (`device_id`)
        REFERENCES `device` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- remote_extra_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `remote_extra_log`;

CREATE TABLE `remote_extra_log`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `connector_id` int(11) unsigned,
    `remote_id` VARCHAR(255),
    `device_id` int(11) unsigned,
    `type` VARCHAR(10),
    `content_type` VARCHAR(10),
    `timestamp` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `content_id` (`connector_id`, `remote_id`),
    INDEX `device_id` (`device_id`),
    CONSTRAINT `remote_extra_log_ibfk_1`
        FOREIGN KEY (`device_id`)
        REFERENCES `device` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `remote_extra_log_ibfk_2`
        FOREIGN KEY (`connector_id`)
        REFERENCES `section_connector` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- remote_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `remote_log`;

CREATE TABLE `remote_log`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `connector_id` int(11) unsigned,
    `remote_id` VARCHAR(255),
    `user_id` int(11) unsigned,
    `content_type` VARCHAR(10),
    `liked_at` DATETIME,
    `shared_at` DATETIME,
    `share_count` INTEGER DEFAULT 0,
    `view_count` INTEGER DEFAULT 0,
    `like_count` INTEGER DEFAULT 0,
    PRIMARY KEY (`id`),
    INDEX `content_id` (`connector_id`, `remote_id`, `user_id`),
    INDEX `user_id` (`user_id`),
    CONSTRAINT `remote_log_ibfk_1`
        FOREIGN KEY (`connector_id`)
        REFERENCES `section_connector` (`id`),
    CONSTRAINT `remote_log_ibfk_2`
        FOREIGN KEY (`user_id`)
        REFERENCES `user_app` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- remote_provider
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `remote_provider`;

CREATE TABLE `remote_provider`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `uuid` VARCHAR(255) DEFAULT '' NOT NULL,
    `email` VARCHAR(255) DEFAULT '' NOT NULL,
    `email_canonical` VARCHAR(255) DEFAULT '' NOT NULL,
    `name` VARCHAR(255) DEFAULT '' NOT NULL,
    `slug` VARCHAR(255) DEFAULT '' NOT NULL,
    `icon` VARCHAR(255),
    `site` VARCHAR(255),
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- role
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255),
    `description` VARCHAR(255),
    `icon` VARCHAR(255),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- section
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `section`;

CREATE TABLE `section`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `area` VARCHAR(50),
    `type` VARCHAR(50),
    `author_id` int(11) unsigned,
    `title` TEXT,
    `description` TEXT,
    `slug` VARCHAR(255),
    `icon_id` int(11) unsigned,
    `icon_selected_id` int(11) unsigned,
    `resource` VARCHAR(2048),
    `params` TEXT,
    `template_id` int(11) unsigned NOT NULL,
    `layout` TEXT,
    `tags` TEXT,
    `tags_user` TEXT,
    `parsed_tags` LONGTEXT,
    `status` TINYINT(1),
    `weight` INTEGER,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `template_id` (`template_id`),
    FULLTEXT INDEX `section_i_56655f` (`parsed_tags`),
    FULLTEXT INDEX `section_i_e26ddd` (`tags_user`),
    FULLTEXT INDEX `section_i_87945f` (`title`, `description`),
    INDEX `section_ibfi_2` (`icon_id`),
    INDEX `section_ibfi_3` (`icon_selected_id`),
    INDEX `section_ibfi_4` (`author_id`),
    CONSTRAINT `section_ibfk_1`
        FOREIGN KEY (`template_id`)
        REFERENCES `template` (`id`),
    CONSTRAINT `section_ibfk_2`
        FOREIGN KEY (`icon_id`)
        REFERENCES `media` (`id`),
    CONSTRAINT `section_ibfk_3`
        FOREIGN KEY (`icon_selected_id`)
        REFERENCES `media` (`id`),
    CONSTRAINT `section_ibfk_4`
        FOREIGN KEY (`author_id`)
        REFERENCES `user_backend` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- section_connector
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `section_connector`;

CREATE TABLE `section_connector`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `section_id` int(11) unsigned NOT NULL,
    `provider_id` int(11) unsigned NOT NULL,
    `type` VARCHAR(20) DEFAULT '' NOT NULL,
    `baseurl` VARCHAR(255) DEFAULT '' NOT NULL,
    `remote_section_id` int(11) unsigned,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `section_key` (`section_id`),
    INDEX `provider_key` (`provider_id`),
    INDEX `section_connector_ibfi_1` (`remote_section_id`),
    CONSTRAINT `provider_key`
        FOREIGN KEY (`provider_id`)
        REFERENCES `remote_provider` (`id`)
        ON UPDATE CASCADE,
    CONSTRAINT `section_key`
        FOREIGN KEY (`section_id`)
        REFERENCES `section` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `section_connector_ibfk_1`
        FOREIGN KEY (`remote_section_id`)
        REFERENCES `section` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- section_related_to
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `section_related_to`;

CREATE TABLE `section_related_to`
(
    `from_section_id` int(11) unsigned NOT NULL,
    `to_section_id` int(11) unsigned NOT NULL,
    PRIMARY KEY (`from_section_id`,`to_section_id`),
    UNIQUE INDEX `section_related_to_u_e0dd66` (`from_section_id`, `to_section_id`),
    INDEX `from_section_id` (`from_section_id`),
    INDEX `to_section_id` (`to_section_id`),
    CONSTRAINT `section_related_to_ibfk_1`
        FOREIGN KEY (`from_section_id`)
        REFERENCES `section` (`id`)
        ON DELETE CASCADE,
    CONSTRAINT `section_related_to_ibfk_2`
        FOREIGN KEY (`to_section_id`)
        REFERENCES `section` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tag
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tag`;

CREATE TABLE `tag`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `related_tag` int(11) unsigned,
    `label` VARCHAR(255),
    `type` VARCHAR(255),
    `group_label` VARCHAR(255),
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `tag_fi_e506e8` (`related_tag`),
    CONSTRAINT `tag_fk_e506e8`
        FOREIGN KEY (`related_tag`)
        REFERENCES `tag` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- template
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `template`;

CREATE TABLE `template`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(255),
    `subtitle` VARCHAR(255),
    `description` TEXT,
    `icon` VARCHAR(255),
    `cover` VARCHAR(255),
    `json_views` TEXT,
    `layout` TEXT,
    `enabled` TINYINT(1),
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- user_app
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user_app`;

CREATE TABLE `user_app`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(255) NOT NULL,
    `username_canonical` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `email_canonical` VARCHAR(255) NOT NULL,
    `enabled` TINYINT(1) NOT NULL,
    `privacy` tinyint(1) unsigned,
    `terms` tinyint(1) unsigned,
    `salt` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `last_login` DATETIME,
    `locked` TINYINT(1) NOT NULL,
    `expired` TINYINT(1) NOT NULL,
    `expires_at` DATETIME,
    `confirmation_token` VARCHAR(255),
    `password_requested_at` DATETIME,
    `roles` LONGTEXT NOT NULL,
    `credentials_expired` TINYINT(1) NOT NULL,
    `credentials_expire_at` DATETIME,
    `firstname` VARCHAR(255),
    `lastname` VARCHAR(255),
    `phone` VARCHAR(255),
    `imagePath` VARCHAR(255),
    `address_id` int(11) unsigned,
    `tags` LONGTEXT,
    `section_id` int(11) unsigned,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `UNIQ_FCB96C9E92FC23A8` (`username_canonical`),
    UNIQUE INDEX `UNIQ_FCB96C9EA0D96FBF` (`email_canonical`),
    UNIQUE INDEX `UNIQ_FCB96C9EF5B7AF75` (`address_id`),
    INDEX `section_id` (`section_id`),
    CONSTRAINT `user_app_ibfk_1`
        FOREIGN KEY (`address_id`)
        REFERENCES `address` (`id`),
    CONSTRAINT `user_app_ibfk_2`
        FOREIGN KEY (`section_id`)
        REFERENCES `section` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- user_application
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user_application`;

CREATE TABLE `user_application`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) unsigned,
    `app_id` int(11) unsigned,
    `connected_at` DATETIME,
    `delated_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `app_id` (`app_id`),
    INDEX `user_id` (`user_id`),
    CONSTRAINT `user_application_ibfk_1`
        FOREIGN KEY (`app_id`)
        REFERENCES `application` (`id`)
        ON DELETE CASCADE,
    CONSTRAINT `user_application_ibfk_2`
        FOREIGN KEY (`user_id`)
        REFERENCES `user_backend` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- user_backend
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user_backend`;

CREATE TABLE `user_backend`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `imagePath` VARCHAR(255),
    `firstname` VARCHAR(255),
    `lastname` VARCHAR(255),
    `email` VARCHAR(255),
    `password` VARCHAR(255),
    `password_requested_at` INTEGER,
    `confirmation_token` VARCHAR(255),
    `last_login` DATETIME,
    `locked` TINYINT(1) DEFAULT 0,
    `locked_at` DATETIME,
    `expired` TINYINT(1) DEFAULT 0,
    `expired_at` DATETIME,
    `role_id` INTEGER,
    `privacy` TINYINT(1),
    `privacy_at` DATETIME,
    `terms` TINYINT(1),
    `terms_at` DATETIME,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- webhook
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `webhook`;

CREATE TABLE `webhook`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `bind_to` TEXT NOT NULL,
    `hash_id` VARCHAR(255) NOT NULL,
    `url` VARCHAR(2024) NOT NULL,
    `provider_id` int(11) unsigned NOT NULL,
    `section_id` int(11) unsigned,
    `post_id` int(11) unsigned,
    `seconds_delay` int(11) unsigned DEFAULT 0 NOT NULL,
    `max_attempts` TINYINT(1) DEFAULT 1 NOT NULL,
    `enabled` TINYINT(1) DEFAULT 1 NOT NULL,
    `last_trigger_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `webhook_ibfi_1` (`provider_id`),
    INDEX `webhook_ibfi_2` (`section_id`),
    INDEX `webhook_ibfi_3` (`post_id`),
    CONSTRAINT `webhook_ibfk_1`
        FOREIGN KEY (`provider_id`)
        REFERENCES `remote_provider` (`id`),
    CONSTRAINT `webhook_ibfk_2`
        FOREIGN KEY (`section_id`)
        REFERENCES `section` (`id`),
    CONSTRAINT `webhook_ibfk_3`
        FOREIGN KEY (`post_id`)
        REFERENCES `post` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- webhook_queue
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `webhook_queue`;

CREATE TABLE `webhook_queue`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `webhook_id` int(11) unsigned NOT NULL,
    `status` TINYINT(1) DEFAULT 0 NOT NULL,
    `failed_attempts` TINYINT(1) DEFAULT 0 NOT NULL,
    `max_attempts` TINYINT(1) DEFAULT 1 NOT NULL,
    `response_log` TEXT,
    `url` VARCHAR(2024) NOT NULL,
    `payload` TEXT NOT NULL,
    `to_sync_at` DATETIME,
    `synced_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `webhook_queue_ibfi_1` (`webhook_id`),
    CONSTRAINT `webhook_queue_ibfk_1`
        FOREIGN KEY (`webhook_id`)
        REFERENCES `webhook` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- application_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `application_archive`;

CREATE TABLE `application_archive`
(
    `id` int(11) unsigned NOT NULL,
    `title` VARCHAR(255),
    `slug` VARCHAR(255),
    `bundle` VARCHAR(255),
    `description` TEXT,
    `app_key` VARCHAR(255),
    `dsn` VARCHAR(255),
    `db_host` VARCHAR(255),
    `db_name` VARCHAR(255),
    `db_user` VARCHAR(255),
    `db_pwd` VARCHAR(255),
    `api_version` VARCHAR(10),
    `pack_id` int(11) unsigned,
    `category_id` int(11) unsigned,
    `template_id` int(11) unsigned,
    `push_notification` TEXT,
    `max_notification` INTEGER DEFAULT 0,
    `max_advice_hour` INTEGER DEFAULT 0,
    `advice_hour_done` INTEGER DEFAULT 0,
    `max_content_insert` INTEGER DEFAULT 0,
    `icon` VARCHAR(255),
    `splash_screen` VARCHAR(255),
    `background_color` VARCHAR(7),
    `app_logo` VARCHAR(255),
    `layout` TEXT,
    `white_label` TINYINT(1),
    `demo` TINYINT(1),
    `facebook_token` VARCHAR(255),
    `apple_id` VARCHAR(255),
    `apple_id_password` VARCHAR(255),
    `apple_store_app_link` VARCHAR(255),
    `play_store_id` VARCHAR(255),
    `play_store_id_password` VARCHAR(255),
    `play_store_app_link` VARCHAR(255),
    `controller_uri` TEXT,
    `google_analytics_ua` TEXT,
    `published` TINYINT(1) DEFAULT 0 NOT NULL,
    `expired_at` DATETIME,
    `main_contents_language` VARCHAR(2),
    `other_contents_languages` VARCHAR(255),
    `deleted_at` DATETIME,
    `system_icons` TEXT NOT NULL,
    `toolbar` enum('SLIDESHOW','TITLE','ICON') DEFAULT 'SLIDESHOW' NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `template_index` (`template_id`),
    INDEX `category_id` (`category_id`),
    INDEX `pack_id` (`pack_id`),
    INDEX `application_archive_i_6f8d34` (`bundle`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- device_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `device_archive`;

CREATE TABLE `device_archive`
(
    `id` int(11) unsigned NOT NULL,
    `user_id` int(11) unsigned,
    `status` VARCHAR(20) NOT NULL,
    `type` VARCHAR(20) NOT NULL,
    `os` LONGTEXT NOT NULL,
    `os_version` LONGTEXT NOT NULL,
    `enable_notification` TINYINT(1),
    `api_version` VARCHAR(50),
    `language` VARCHAR(10),
    `demo` TINYINT(1),
    `token` LONGTEXT NOT NULL,
    `api_key` VARCHAR(255),
    `ip_address` VARCHAR(50),
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `IDX_E83B3B8A76ED395` (`user_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- group_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `group_archive`;

CREATE TABLE `group_archive`
(
    `id` int(11) unsigned NOT NULL,
    `label` VARCHAR(255) NOT NULL,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- media_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `media_archive`;

CREATE TABLE `media_archive`
(
    `id` int(11) unsigned NOT NULL,
    `device_id` int(11) unsigned,
    `is_public` TINYINT(1) DEFAULT 1 NOT NULL,
    `type` VARCHAR(20) NOT NULL,
    `uri` VARCHAR(255),
    `uri_thumb` VARCHAR(255),
    `extra_params` TEXT,
    `weight` INTEGER NOT NULL,
    `title` VARCHAR(255),
    `description` TEXT,
    `extension` VARCHAR(20),
    `mime_type` VARCHAR(20),
    `size` INTEGER,
    `count_like` INTEGER DEFAULT 0,
    `count_share` INTEGER DEFAULT 0,
    `format` TEXT,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `device_id` (`device_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_archive`;

CREATE TABLE `post_archive`
(
    `id` int(11) unsigned NOT NULL,
    `type` VARCHAR(20),
    `status` TINYINT(3),
    `author_id` int(11) unsigned,
    `cover_id` int(11) unsigned,
    `slug` VARCHAR(255),
    `title` TEXT,
    `subtitle` TEXT,
    `description` TEXT,
    `body` TEXT,
    `search` TEXT,
    `template_id` int(11) unsigned NOT NULL,
    `layout` TEXT,
    `tags` TEXT,
    `tags_user` TEXT,
    `row_data` TEXT,
    `count_like` INTEGER DEFAULT 0,
    `count_share` INTEGER DEFAULT 0,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `template_id` (`template_id`),
    INDEX `cover_id` (`cover_id`),
    FULLTEXT INDEX `post_archive_i_a5db2a` (`tags`),
    FULLTEXT INDEX `post_archive_i_e26ddd` (`tags_user`),
    INDEX `post_ibfi_3` (`author_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_action_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_action_archive`;

CREATE TABLE `post_action_archive`
(
    `id` int(11) unsigned NOT NULL,
    `post_id` int(11) unsigned NOT NULL,
    `area` VARCHAR(50),
    `type` VARCHAR(10) NOT NULL,
    `label` VARCHAR(255) NOT NULL,
    `cover_id` int(11) unsigned,
    `params` LONGTEXT,
    `weight` INTEGER,
    `section_id` int(11) unsigned,
    `content_id` int(11) unsigned,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `post_id` (`post_id`),
    INDEX `section_id` (`section_id`),
    INDEX `content_id` (`content_id`),
    INDEX `post_action_ibfi_4` (`cover_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_connector_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_connector_archive`;

CREATE TABLE `post_connector_archive`
(
    `id` int(11) unsigned NOT NULL,
    `post_id` int(11) unsigned NOT NULL,
    `provider_id` int(11) unsigned NOT NULL,
    `type` VARCHAR(20) DEFAULT '' NOT NULL,
    `baseurl` VARCHAR(255) DEFAULT '' NOT NULL,
    `remote_post_id` int(11) unsigned,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `post_key` (`post_id`),
    INDEX `provider_key` (`provider_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_event_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_event_archive`;

CREATE TABLE `post_event_archive`
(
    `id` int(11) unsigned NOT NULL,
    `post_id` int(11) unsigned,
    `start_at` DATETIME,
    `end_at` DATETIME,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `post_id` (`post_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_person_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_person_archive`;

CREATE TABLE `post_person_archive`
(
    `id` int(11) unsigned NOT NULL,
    `post_id` int(11) unsigned,
    `firstname` VARCHAR(127),
    `lastname` VARCHAR(127),
    `birthday` DATETIME,
    `death` DATETIME,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `post_id` (`post_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- post_poi_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `post_poi_archive`;

CREATE TABLE `post_poi_archive`
(
    `id` int(11) unsigned NOT NULL,
    `post_id` int(11) unsigned,
    `identifier` VARCHAR(20) NOT NULL,
    `type` VARCHAR(20),
    `lat` FLOAT(10,6),
    `lng` FLOAT(10,6),
    `radius` INTEGER,
    `uuid` VARCHAR(255),
    `major` smallint(1) unsigned,
    `minor` smallint(1) unsigned,
    `distance` VARCHAR(10),
    `pin` VARCHAR(255),
    `poi_notification` TINYINT(1) DEFAULT 0,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `post_id` (`post_id`),
    INDEX `post_poi_archive_i_c6571e` (`identifier`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- push_notification_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `push_notification_archive`;

CREATE TABLE `push_notification_archive`
(
    `id` int(11) unsigned NOT NULL,
    `lang` VARCHAR(2),
    `title` VARCHAR(255) DEFAULT '',
    `body` TEXT NOT NULL,
    `section_id` int(11) unsigned,
    `post_id` int(11) unsigned,
    `send_date` DATETIME NOT NULL,
    `receiver_filter` TEXT,
    `device_count` int(11) unsigned,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `section_id` (`section_id`),
    INDEX `post_id` (`post_id`),
    FULLTEXT INDEX `push_notification_archive_i_03d6e2` (`receiver_filter`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- remote_provider_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `remote_provider_archive`;

CREATE TABLE `remote_provider_archive`
(
    `id` int(11) unsigned NOT NULL,
    `uuid` VARCHAR(255) DEFAULT '' NOT NULL,
    `email` VARCHAR(255) DEFAULT '' NOT NULL,
    `email_canonical` VARCHAR(255) DEFAULT '' NOT NULL,
    `name` VARCHAR(255) DEFAULT '' NOT NULL,
    `slug` VARCHAR(255) DEFAULT '' NOT NULL,
    `icon` VARCHAR(255),
    `site` VARCHAR(255),
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- section_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `section_archive`;

CREATE TABLE `section_archive`
(
    `id` int(11) unsigned NOT NULL,
    `area` VARCHAR(50),
    `type` VARCHAR(50),
    `author_id` int(11) unsigned,
    `title` TEXT,
    `description` TEXT,
    `slug` VARCHAR(255),
    `icon_id` int(11) unsigned,
    `icon_selected_id` int(11) unsigned,
    `resource` VARCHAR(2048),
    `params` TEXT,
    `template_id` int(11) unsigned NOT NULL,
    `layout` TEXT,
    `tags` TEXT,
    `tags_user` TEXT,
    `parsed_tags` LONGTEXT,
    `status` TINYINT(1),
    `weight` INTEGER,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `template_id` (`template_id`),
    FULLTEXT INDEX `section_archive_i_56655f` (`parsed_tags`),
    FULLTEXT INDEX `section_archive_i_e26ddd` (`tags_user`),
    FULLTEXT INDEX `section_archive_i_87945f` (`title`, `description`),
    INDEX `section_ibfi_2` (`icon_id`),
    INDEX `section_ibfi_3` (`icon_selected_id`),
    INDEX `section_ibfi_4` (`author_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- section_connector_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `section_connector_archive`;

CREATE TABLE `section_connector_archive`
(
    `id` int(11) unsigned NOT NULL,
    `section_id` int(11) unsigned NOT NULL,
    `provider_id` int(11) unsigned NOT NULL,
    `type` VARCHAR(20) DEFAULT '' NOT NULL,
    `baseurl` VARCHAR(255) DEFAULT '' NOT NULL,
    `remote_section_id` int(11) unsigned,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `section_key` (`section_id`),
    INDEX `provider_key` (`provider_id`),
    INDEX `section_connector_ibfi_1` (`remote_section_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tag_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tag_archive`;

CREATE TABLE `tag_archive`
(
    `id` int(11) unsigned NOT NULL,
    `related_tag` int(11) unsigned,
    `label` VARCHAR(255),
    `type` VARCHAR(255),
    `group_label` VARCHAR(255),
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `tag_fi_e506e8` (`related_tag`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- template_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `template_archive`;

CREATE TABLE `template_archive`
(
    `id` int(11) unsigned NOT NULL,
    `title` VARCHAR(255),
    `subtitle` VARCHAR(255),
    `description` TEXT,
    `icon` VARCHAR(255),
    `cover` VARCHAR(255),
    `json_views` TEXT,
    `layout` TEXT,
    `enabled` TINYINT(1),
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- user_app_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user_app_archive`;

CREATE TABLE `user_app_archive`
(
    `id` int(11) unsigned NOT NULL,
    `username` VARCHAR(255) NOT NULL,
    `username_canonical` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `email_canonical` VARCHAR(255) NOT NULL,
    `enabled` TINYINT(1) NOT NULL,
    `privacy` tinyint(1) unsigned,
    `terms` tinyint(1) unsigned,
    `salt` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `last_login` DATETIME,
    `locked` TINYINT(1) NOT NULL,
    `expired` TINYINT(1) NOT NULL,
    `expires_at` DATETIME,
    `confirmation_token` VARCHAR(255),
    `password_requested_at` DATETIME,
    `roles` LONGTEXT NOT NULL,
    `credentials_expired` TINYINT(1) NOT NULL,
    `credentials_expire_at` DATETIME,
    `firstname` VARCHAR(255),
    `lastname` VARCHAR(255),
    `phone` VARCHAR(255),
    `imagePath` VARCHAR(255),
    `address_id` int(11) unsigned,
    `tags` LONGTEXT,
    `section_id` int(11) unsigned,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `section_id` (`section_id`),
    INDEX `user_app_archive_i_452367` (`username_canonical`),
    INDEX `user_app_archive_i_a34432` (`email_canonical`),
    INDEX `user_app_archive_i_39c0fe` (`address_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- user_application_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user_application_archive`;

CREATE TABLE `user_application_archive`
(
    `id` int(11) unsigned NOT NULL,
    `user_id` int(11) unsigned,
    `app_id` int(11) unsigned,
    `connected_at` DATETIME,
    `delated_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `app_id` (`app_id`),
    INDEX `user_id` (`user_id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- user_backend_archive
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user_backend_archive`;

CREATE TABLE `user_backend_archive`
(
    `id` int(11) unsigned NOT NULL,
    `imagePath` VARCHAR(255),
    `firstname` VARCHAR(255),
    `lastname` VARCHAR(255),
    `email` VARCHAR(255),
    `password` VARCHAR(255),
    `password_requested_at` INTEGER,
    `confirmation_token` VARCHAR(255),
    `last_login` DATETIME,
    `locked` TINYINT(1) DEFAULT 0,
    `locked_at` DATETIME,
    `expired` TINYINT(1) DEFAULT 0,
    `expired_at` DATETIME,
    `role_id` INTEGER,
    `privacy` TINYINT(1),
    `privacy_at` DATETIME,
    `terms` TINYINT(1),
    `terms_at` DATETIME,
    `deleted_at` DATETIME,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    `archived_at` DATETIME,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
