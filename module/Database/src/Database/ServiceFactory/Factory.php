<?php
namespace Database\ServiceFactory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * THIS FACTORY HELPS CONFIGURE AND PREPARE THE ORM
 */
class Factory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        $config = $serviceLocator->get('Config');

        switch ($config['dbconnector']['type']) {
            case 'propel':
                if (!isset($config['database'])) {
                    die("You did not set the correct configuration for Propel");
                }

                $databaseKey = $config['database']['core']['db_name'];
                $databasesArray = array();
                $databasesArray[$databaseKey] = array(
                    "debug" => false,
                    "dsn" => $config['database']['core']['dsn'],
                    "database" => $config['database']['core']['db_name'],
                    "user" => $config['database']['core']['usr'],
                    "password" => $config['database']['core']['pwd']
                );

                $propelConfig = array(
                    "ormpath" => $config['propelconfiguration']['ormpath'],
                    "logfilepath" => $config['database']['logfilepath'],
                    "databases" => $databasesArray,
                );

                return new PropelORM($propelConfig);
                break;

            default:
                die("Your ORM type is unknown/unsupported!");
                break;
        }
    }
}