<?php
namespace Database\ServiceFactory;

/**
 * INTERFACE FOR THE ORM
 */
interface IORM
{
    public function __construct($configuration);

    public function configure();
}