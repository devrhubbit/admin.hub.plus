<?php
namespace Database\ServiceFactory;

use Propel\Runtime\Propel,
    Propel\Runtime\Exception\PropelException,
    Propel\Runtime\Connection\ConnectionManagerSingle;

class PropelORM implements IORM
{
    protected $configuration = null;

    public function __construct($configuration)
    {
        $this->configuration = $configuration;
    }

    public function configure()
    {
        // SET INCLUDE PATH IS NOT NEEDED ANYMORE AS DATABASE IS IN A PACKAGE COMPOSER STYLE
        set_include_path(realpath(APPLICATION_ROOT . '/module/Database/generated-classes') . PATH_SEPARATOR . get_include_path());

        $configuration = include($this->configuration['ormpath']);
        if ($configuration === false) {
            throw new PropelException(sprintf('Unable to open configuration file: "%s"', $configuration));
        }

        $ConnectionManagers = Propel::getServiceContainer()->getConnectionManagers();

        foreach ($ConnectionManagers as $ConnectionManager) {
            /** @var ConnectionManagerSingle $ConnectionManager */
            $propelConfig = $ConnectionManager->getConfiguration();
            $propelConfig["dsn"] = $this->configuration['databases'][$ConnectionManager->getName()]['dsn'];
            $propelConfig["user"] = $this->configuration['databases'][$ConnectionManager->getName()]['user'];
            $propelConfig["password"] = $this->configuration['databases'][$ConnectionManager->getName()]['password'];

            $ConnectionManager->setConfiguration($propelConfig);
        }
    }
}