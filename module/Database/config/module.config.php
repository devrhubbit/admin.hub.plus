<?php
/**
 * Database module config
 *
 * @author Sergio Pellicani <serpe@fastwebnet.it>
 */

return array(
    'service_manager' => array(
        'factories' => array(
            'dbconnector' => 'Database\ServiceFactory\Factory',
        ),
    ),

    'dbconnector' => array(
        'type' => 'propel',
    ),

    'propelconfiguration' => array(
        'ormpath' => APPLICATION_ROOT . '/module/Database/generated-conf/config.php',
        'logfilepath' => 'OVERWRITE WITH THE PATH OF THE LOG FILE',
        'databases' => array(
            'hp_core' => array(
                'debug' => true,
                'dsn' => 'HERE IS WHERE I PUT MY DSN',
                'database' => 'HERE IS WHERE I PUT MY DATABASE',
                'user' => 'HERE IS WHERE I PUT MY USRERNAME',
                'password' => 'HERE IS WHERE I PUT MY PASSWORD',
            ),
        ),

    ),
);
