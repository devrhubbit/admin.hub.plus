<?php
namespace Form\Profile;

// Add these import statements
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\StringLength;

//use Validator\HpStringLength;

class ProfileFilter implements InputFilterAwareInterface
{
    protected $inputFilter;                       // <-- Add this variable

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

	public function getInputFilter()
    {
    	if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
            	'name'     => 'email',
            	'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'   => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                            ),
                        ),
                        'break_chain_on_failure' => true,
                    ),
                    array(
                        'name'   => 'EmailAddress',
                        'options' => array(
                            'message' => 'Invalid email address'
                        ),
                    ),
                ),
            )));

            $password = new Input('password');
            $password->setRequired(false)
                     ->getFilterChain()
                     ->attach( new StripTags())
                     ->attach( new StringTrim());
            $password->getValidatorChain()
                     ->attach( new StringLength( array( 'min' => 8 ) ));

            $inputFilter->add($password);


            $confirm = new Input('confirm-password');
            $confirm->setRequired(false)
                    ->getFilterChain()
                    ->attach( new StripTags())
                    ->attach( new StringTrim());
            $confirm->getValidatorChain()
                    ->attach( new StringLength( array( 'min' => 8 ) ));

            $inputFilter->add($confirm);


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}