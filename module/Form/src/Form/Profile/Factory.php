<?php 
namespace Form\Profile;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Factory implements FactoryInterface
{
	
	public function createService(ServiceLocatorInterface $serviceLocator) {
		
		$form = new ProfileForm($serviceLocator);
		$Translator = $serviceLocator->get('translator');		
		\Zend\Validator\AbstractValidator::setDefaultTranslator($Translator);
		
		$filter = new ProfileFilter();
		$form->setInputFilter($filter->getInputFilter());
		
		return $form;
		
	}
}