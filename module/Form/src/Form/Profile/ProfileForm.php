<?php
namespace Form\Profile;

use Form\HpForm;

class ProfileForm extends HpForm
{
	protected $serviceLocator;

	public function __construct($serviceLocator)
	{
		parent::__construct('profile');

		$this->serviceLocator = $serviceLocator;

		$this->setAttribute('method', 'POST');
		$this->setAttribute('enctype','multipart/form-data');

		$this->add(array(
			'name' => 'upload-avatar',
			'attributes' => array(
				'type' 	=> 'File',
				'id' 	=> 'upload-avatar',
				'class'	=> 'file-loading',
			),
		));

		$this->add(array(
			'name' => 'avatar',
			'attributes' => array(
				'type' 	=> 'Hidden',
				'id' 	=> 'avatar',
			),
		));

		$this->add(array(
			'name' => 'first-name',
			'attributes' => array(
				'type' 	=> 'Text',
				'id' 	=> 'first-name',
				'class' => 'form-control',
				'placeholder'=> 'First name',
			),
			'options' => array(
				'label' => 'First name',
			),
		));

		$this->add(array(
			'name' => 'last-name',
			'attributes' => array(
				'type' 	=> 'Text',
				'id' 	=> 'last-name',
				'class' => 'form-control',
				'placeholder'=> 'Last name',
			),
			'options' => array(
				'label' => 'Last name',
			),
		));

		$this->add(array(
			'name' => 'email',
			'attributes' => array(
				'type' 	=> 'Text',
				'id' 	=> 'email',
				'autocomplete' => 'off',
				'autocorrect' => 'off',
				'autocapitalize' => 'off',
				'class' => 'form-control',
				'placeholder'=> 'Email*',
			),
			'options' => array(
				'label' => 'Email',
			),
		));

		$this->add(array(
			'name' => 'password',
			'attributes' => array(
				'type' 	=> 'Password',
				'id' 	=> 'password',
				'autocomplete' => 'nope',
				'autocorrect' => 'off',
				'autocapitalize' => 'off',
				'class' => 'form-control',
				'placeholder'=> 'New password',
			),
			'options' => array(
				'label' => 'Password:',
			),
		));

		$this->add(array(
			'name' => 'confirm-password',
			'attributes' => array(
				'type' 	=> 'Password',
				'id' 	=> 'confirm-password',
				'autocomplete' => 'nope',
				'autocorrect' => 'off',
				'autocapitalize' => 'off',
				'class' => 'form-control',
				'placeholder'=> 'Confirm password',
			),
			'options' => array(
				'label' => 'Confirm password:',
			),
		));

		$this->add(array(
			'name' => 'update',
			'type' => 'Button',
			'attributes' => array(
				'id' 	=> 'update',
				'value' => 'UPDATE PROFILE',
				'class'	=> 'btn btn-primary btn-block btn-flat',
			),
            'options' => array(
                'label' => 'UPDATE PROFILE',
            ),
		));
	}

	public function getView() {
		return 'form/profile';
	}

}


