<?php

namespace Form;

use Application\Model\ProviderModel;

abstract class LayoutForm extends HpForm
{
    protected $serviceLocator;

    const APP = "APP";
    const RSS = "RSS";
    const PODCAST = "PODCAST";
    const TWITTER = "TWITTER";
    const REGISTER = "REGISTER";
    const FAVOURITE = "FAVOURITE";
    const INFO = "INFO";
    const EVENT = "EVENT";
    const PERSON = "PERSON";
    const POI = "POI";
    const GENERIC = "GENERIC";
    const WEB = "WEB";
    const SECTION = "SECTION";
    const DEV = "DEV";
    const QR_CODE = "QR_CODE";
    const USER = "USER";

    const NO_LAYOUT_NEEDED = "NO_LAYOUT";

    const CUSTOM_FORM = "CUSTOM_FORM";
    const CUSTOM_FIELD = "CUSTOM_FIELD";

    const AUTH_MODE_MANDATORY = "MANDATORY";
    const AUTH_MODE_OPTIONAL = "OPTIONAL";
    const AUTH_MODE_POPUP = "POPUP";

    const SEARCH_DISABLED_KEY = "DISABLED";
    const SEARCH_DISABLED_VALUE = "DISABLED";
    const SEARCH_TOP_KEY = "TOP";
    const SEARCH_TOP_VALUE = "TOP";
    const SEARCH_BOTTOM_KEY = "BOTTOM";
    const SEARCH_BOTTOM_VALUE = "BOTTOM";

    const CONNECTOR_TYPE_HUB = "HUB";
    const CONNECTOR_TYPE_REMOTE = "REMOTE";

    private $disabledTabFields = array();

    public function getDisabledTabFields()
    {
        return $this->disabledTabFields;
    }

    public function addLayoutBlock($disabledFields = array(), $type = null)
    {
        $this->disabledTabFields = $disabledFields;

        $config = $this->serviceLocator->get('config');

        $opts = array();
        $opts [] = array('value' => null, 'label' => '-');

        $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');

        $this->addHiddenField("layout");
        $this->addHiddenField("template_id");


        if (!in_array('page_size', $disabledFields)) {
            $this->add(array(
                'name' => 'page_size',
                'attributes' => array(
                    'type' => 'Text',
                    'id' => 'page_size',
                    'class' => 'form-control',
                    'value' => '10',
                ),
                'options' => array(
                    'label' => 'Page size',
                ),
            ));
        }


        /* LAYOUT */

        $selected = "";
        if (!in_array('cell-type', $disabledFields)) {
            switch ($this->getName()) {
                case "app":
                    $key = 'list_template_types';
                    $selected = "TAB_PAGE";
                    break;
                case "content":
                    $key = 'list_page_types';
                    break;
                case "section";
                    $key = 'list_cell_types';
                    break;
                default:
                    $key = '';
            }

            foreach ($config[$key] as $t) {
                if (APPLICATION_ENV === "production" && $t["only_dev"] === true) {
                    continue;
                }
                if (in_array($type, $t["types"])) {
                    $opts [] = array(
                        'value' => $t["value"],
                        'label' => $t["label"],
                        'attributes' => array(
                            'data-content' => $renderer->basePath($t["icon"]),
                        )
                    );
                }
            }

            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'cell-type',
                'attributes' => array(
                    'id' => 'cell-type',
                    'class' => 'select-grid',
                    'data-toggle' => false,
                    'options' => $opts,
                    'value' => $selected,
                ),
            ));
        }


        /* LOOK AND FEEL */

        if (!in_array('look-tab', $disabledFields)) {
            $this->add(array(
                'name' => 'global_bg',
                'attributes' => array(
                    'type' => 'Text',
                    'id' => 'global_bg',
                    'class' => 'form-control advanced-layout hidden',
                ),
                'options' => array(
                    'label' => 'Main color',
                ),
            ));

            $this->add(array(
                'name' => 'accent_bg',
                'attributes' => array(
                    'type' => 'Text',
                    'id' => 'accent_bg',
                    'class' => 'form-control advanced-layout hidden',
                ),
                'options' => array(
                    'label' => 'Accent color',
                ),
            ));

            $this->add(array(
                'name' => 'primary_bg',
                'attributes' => array(
                    'type' => 'Text',
                    'id' => 'primary_bg',
                    'class' => 'form-control advanced-layout hidden',
                ),
                'options' => array(
                    'label' => 'Primary color',
                ),
            ));

            $this->add(array(
                'name' => 'secondary_bg',
                'attributes' => array(
                    'type' => 'Text',
                    'id' => 'secondary_bg',
                    'class' => 'form-control advanced-layout hidden',
                ),
                'options' => array(
                    'label' => 'Secondary color',
                ),
            ));

            $this->add(array(
                'name' => 'light_txt',
                'attributes' => array(
                    'type' => 'Text',
                    'id' => 'light_txt',
                    'class' => 'form-control advanced-layout hidden',
                ),
                'options' => array(
                    'label' => 'Light text color',
                ),
            ));

            $this->add(array(
                'name' => 'dark_txt',
                'attributes' => array(
                    'type' => 'Text',
                    'id' => 'dark_txt',
                    'class' => 'form-control advanced-layout hidden',
                ),
                'options' => array(
                    'label' => 'Dark text color',
                ),
            ));

            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'primary_font',
                'attributes' => array(
                    'id' => 'primary_font',
                    'class' => 'selectpicker',
                    'options' => $config['font_list'],
                    'data-width' => '100%',
                ),
                'options' => array(
                    'label' => 'Primary font',
                    'label_options' => array(
                        'disable_html_escape' => true,
                    )
                ),
            ));

            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'primary_font_size',
                'attributes' => array(
                    'id' => 'primary_font_size',
                    'class' => 'selectpicker',
                    'options' => array(
                        10 => 10,
                        12 => 12,
                        14 => 14,
                        16 => 16,
                        18 => 18,
                        20 => 20,
                    ),
                    'data-width' => '100%',
                ),
                'options' => array(
                    'label' => 'Primary font size',
                    'label_options' => array(
                        'disable_html_escape' => true,
                    )
                ),
            ));

            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'secondary_font',
                'attributes' => array(
                    'id' => 'secondary_font',
                    'class' => 'selectpicker',
                    'options' => $config['font_list'],
                    'data-width' => '100%',
                ),
                'options' => array(
                    'label' => 'Secondary font',
                    'label_options' => array(
                        'disable_html_escape' => true,
                    )
                ),
            ));

            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'secondary_font_size',
                'attributes' => array(
                    'id' => 'secondary_font_size',
                    'class' => 'selectpicker',
                    'options' => array(
                        10 => 10,
                        12 => 12,
                        14 => 14,
                        16 => 16,
                        18 => 18,
                        20 => 20,
                    ),
                    'data-width' => '100%',
                ),
                'options' => array(
                    'label' => 'Secondary font size',
                    'label_options' => array(
                        'disable_html_escape' => true,
                    )
                ),
            ));
        }


        /* OPTIONS */
        if (!in_array('auth', $disabledFields)) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'auth',
                'attributes' => array(
                    'id' => 'auth',
                    'title' => 'Select your login mode*',
                    'class' => 'selectpicker',
                    'data-style' => 'btn-default',
                    'data-width' => '100%',
                    'options' => [
                        LayoutForm::AUTH_MODE_OPTIONAL => LayoutForm::AUTH_MODE_OPTIONAL,
                        LayoutForm::AUTH_MODE_POPUP => LayoutForm::AUTH_MODE_POPUP,
                        LayoutForm::AUTH_MODE_MANDATORY => LayoutForm::AUTH_MODE_MANDATORY,
                    ],
                    'value' => LayoutForm::AUTH_MODE_OPTIONAL,
                ),
                'options' => array(
                    'label' => 'LOGIN MODE*',
                    'disable_inarray_validator' => true,
                ),
            ));
        }

        if (!in_array('filter', $disabledFields)) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'filter',
                'attributes' => array(
                    'id' => 'filter',
                    'class' => 'selectpicker',
                    'options' => [
                        LayoutForm::SEARCH_DISABLED_KEY => LayoutForm::SEARCH_DISABLED_VALUE,
                        LayoutForm::SEARCH_TOP_KEY => LayoutForm::SEARCH_TOP_VALUE,
                        LayoutForm::SEARCH_BOTTOM_KEY => LayoutForm::SEARCH_BOTTOM_VALUE,
                    ],
                    'data-width' => '100%',
                ),
                'options' => array(
                    'label' => 'SEARCH box',
                    'label_options' => array(
                        'disable_html_escape' => true,
                    )
                ),
            ));
        }

        if (!in_array('share', $disabledFields)) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => 'share',
                'options' => array(
                    'label' => 'SHARE button',
                ),
                'attributes' => array(
                    'id' => 'share',
                    'class' => 'switch-btn',
                )
            ));
        }

        if (!in_array('like', $disabledFields)) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => 'like',
                'options' => array(
                    'label' => 'LIKE button',
                ),
                'attributes' => array(
                    'id' => 'like',
                    'class' => 'switch-btn',
                )
            ));
        }

        if (!in_array('paged', $disabledFields)) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => 'paged',
                'options' => array(
                    'label' => 'Paged',
                ),
                'attributes' => array(
                    'id' => 'paged',
                    'class' => 'switch-btn',
                )
            ));
        }

        if (!in_array('open_in_app', $disabledFields)) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => 'open_in_app',
                'options' => array(
                    'label' => 'Open in App',
                ),
                'attributes' => array(
                    'id' => 'open_in_app',
                    'class' => 'switch-btn',
                    'value' => true,
                )
            ));
        }

        if (!in_array('autoplay', $disabledFields)) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => 'autoplay',
                'options' => array(
                    'label' => 'AutoPlay',
                ),
                'attributes' => array(
                    'id' => 'autoplay',
                    'class' => 'switch-btn',
                )
            ));
        }

        if (!in_array('streaming', $disabledFields)) {
            $this->add(array(
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => 'streaming',
                'options' => array(
                    'label' => 'Streaming',
                ),
                'attributes' => array(
                    'id' => 'streaming',
                    'class' => 'switch-btn',
                )
            ));
        }
        /* .../OPTIONS */

        if (!in_array('connector', $disabledFields)) {

            $providerModel = new ProviderModel($this->serviceLocator);

            $this->add(array(
                'type' => 'Zend\Form\Element\Radio',
                'name' => 'connector',
                'attributes' => array(
                    'value' => self::CONNECTOR_TYPE_HUB,
                ),
                'options' => array(
                    'label' => 'Connector',
                    'label_attributes' => array(
                        'class' => 'btn btn-default'
                    ),
                    'value_options' => array(
                        self::CONNECTOR_TYPE_HUB => 'HUB +',
                        self::CONNECTOR_TYPE_REMOTE => 'Remote',
                    ),
                    'disable_inarray_validator' => true,
                ),
            ));

            $this->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => 'provider',
                'required' => false,
                'attributes' => array(
                    'id' => 'provider',
                    'title' => 'Select your provider',
                    'class' => 'selectpicker',
                    'data-style' => 'btn-default',
                    'data-width' => '100%',
                    'options' => $providerModel->getProviders(),
                ),
                'options' => array(
                    'label' => 'PROVIDER',
                    'disable_inarray_validator' => false,
                ),
            ));

            $this->add(array(
                'type' => 'Zend\Form\Element\Text',
                'name' => 'connector_base_url',
                'required' => false,
                'attributes' => array(
                    'type' => 'Text',
                    'id' => 'connector_base_url',
                    'class' => 'form-control',
                    'placeholder' => "Connector's base-url",
                    'data-width' => '100%',
                ),
                'options' => array(
                    'label' => "Connector's base-url",
                ),
            ));

            $this->add(array(
                'type' => 'Zend\Form\Element\Hidden',
                'name' => 'connector_validator',
                'attributes' => array(
                    'id' => 'connector_validator',
                ),
            ));

        }

    }

}


