<?php

namespace Form\Media;

use Zend\Validator\AbstractValidator;
use Zend\Validator\Hostname as HostnameValidator;
use Zend\Validator\Regex as RegexValidator;
use Zend\Uri\Http as ZendUriHttp;
use Zend\Uri\Exception\InvalidUriException as ZendUriException;
use Zend\Uri\Exception\InvalidUriPartException as ZendUriPartException;

class RemoteMediaValidator extends AbstractValidator
{
    const INVALID_REMOTE_MEDIA = 'INVALID_MEDIA';
    const INVALID_REMOTE_MEDIA_EMBEDDED = 'INVALID_EMBEDDED';
    const INVALID_REMOTE_MEDIA_URL = 'INVALID_URL';
    const INVALID_REMOTE_MEDIA_URL_TYPE = 'INVALID_URL_TYPE';

    protected $messageTemplates = array(
        self::INVALID_REMOTE_MEDIA => 'Invalid remote media',
        self::INVALID_REMOTE_MEDIA_EMBEDDED => 'Invalid remote media embedded code',
        self::INVALID_REMOTE_MEDIA_URL => "'%value%' is not a valid URL. It must start with http(s):// and be valid.",
        self::INVALID_REMOTE_MEDIA_URL_TYPE => "'%value%' is not a supported URL.",
    );

    private static $validLink = null;

    public function __construct(array $options = array())
    {
        parent::__construct($options);
    }

    public function isValid($value)
    {
        $this->setValue($value);
        //get a Zend\Uri\Http object for our URL, this will only accept http(s) schemes
        try {
            $uriHttp = new ZendUriHttp($value);

            $hostnameValidator = new HostnameValidator(
                array(
                    'allow' => HostnameValidator::ALLOW_DNS,
                    'useIdnCheck' => true,
                    'useTldCheck' => false
                ));

            if ($hostnameValidator->isValid($uriHttp->getHost())) {
                // $value is a valid link
                $result = $this->isValidLink($value);
            } else {
                // hostname is invalid; check if enbedded code
                $regexValidator = new RegexValidator('/<("[^"]*"|\'[^\']*\'|[^\'">])*>/');
                if ($regexValidator->isValid($value)) {
                    return true;
                } else {
                    $this->error(self::INVALID_REMOTE_MEDIA_URL);
                    $result = false;
                }
            }
        } catch (ZendUriException $e) {
            $this->error(self::INVALID_REMOTE_MEDIA);
            return false;
        } catch (ZendUriPartException $e) {
            $this->error(self::INVALID_REMOTE_MEDIA);
            return false;
        }

        return $result;
    }

    static public function getValidLink()
    {
        // WARNING: don't change elements order!
        self::$validLink = array(
            "https://www.youtube.com/watch?v=",
            "https://youtu.be/",
            "https://www.youtube.com/embed/",
            "https://vimeo.com/",
            "https://player.vimeo.com/video/",
            "https://my.matterport.com/",
            "//www.dailymotion.com/",
            "//dai.ly/",
            "https://issuu.com/",
            "https://www.facebook.com/",
            "www.slideshare.net",
            "tour3d.dimensione3.com",
            "go.dimensionetour.com",
        );

        return self::$validLink;
    }

    private function isValidLink($url)
    {
        $res = false;
        foreach (self::getValidLink() as $link) {
            if (strpos($url, $link) !== false) {
                $res = true;
            }
        }

        if ($res === false) {
            $this->error(self::INVALID_REMOTE_MEDIA_URL_TYPE);
        }
        return $res;
    }
}