<?php

namespace Form\Media;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

class Factory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $form = new AddRemoteMediaForm($serviceLocator);
        $Translator = $serviceLocator->get('translator');
        AbstractValidator::setDefaultTranslator($Translator);

        $filter = new AddRemoteMediaFilter();
        $form->setInputFilter($filter->getInputFilter());

        return $form;
    }
}