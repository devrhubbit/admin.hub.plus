<?php

namespace Form\Media;

use Form\HpForm;

class AddRemoteMediaForm extends HpForm
{
    protected $serviceLocator;

    public function __construct($serviceLocator)
    {
        parent::__construct('add_remote_media');

        $this->serviceLocator = $serviceLocator;

        $this->add(array(
            'name' => 'remote_media_link',
            'type' => 'Textarea',
            'attributes' => array(
                'id' => 'remote_media_link',
                'class' => 'form-control',
                'placeholder' => 'Insert link or embedded code copied from supported sites shown below',
                'rows' => 8,
            ),
            'options' => array(
                'label' => 'Remote',
            ),
        ));
    }

    public function getView()
    {
        return 'form/media';
    }

}


