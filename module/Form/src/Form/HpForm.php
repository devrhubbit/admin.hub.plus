<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 04/05/16
 * Time: 17:03
 */

namespace Form;

use Zend\Form\Form;

/**
 * Class HpForm
 * @package Form
*/

abstract class HpForm extends Form {

    private $name = null;
    private $hidden_fields = array();

    public function __construct($name)
    {
        parent::__construct($name);
        $this->name = $name;
    }

    public function isValidPostRequest($request) {
        if ($request->isPost()) {
            $this->setData($request->getPost());
            if ($this->isValid()) {
                return true;
            }
        }
        return false;
    }

    public function addHiddenField($name, $value = "") {
        $this->hidden_fields []= $name;

        $this->add(array(
            'name' => $name,
            'attributes' => array(
                'type' 	=> 'Hidden',
                'id' 	=> $name,
                'value' => $value
            ),
        ));
    }

    public function getFormHiddenElement() {
        return $this->hidden_fields;
    }

    public function getView() {}
}