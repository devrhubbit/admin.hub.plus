<?php

namespace Form\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class UrlValidator extends AbstractValidator
{
    const INVALID_URL = 'invalidUrl';

    protected $messageTemplates = array(
        self::INVALID_URL => 'Invalid Url value',
    );

    public function __construct(array $options = array())
    {
        parent::__construct($options);
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        $result = true;

        if (filter_var($value, FILTER_VALIDATE_URL) === false) {
            $result = false;
            $this->error(self::INVALID_URL);
        }

        return $result;
    }

}