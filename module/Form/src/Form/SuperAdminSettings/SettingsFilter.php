<?php

namespace Form\SuperAdminSettings;

// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\GreaterThan;

class SettingsFilter implements InputFilterAwareInterface
{
    protected $inputFilter;

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name' => 'max-push-notification',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'Int'),
                ),
                'validators' => array(
                    array(
                        'name' => 'GreaterThan',
                        'options' => array(
                            'min' => 0,
                            'inclusive' => true,
                            'messages' => array(
                                GreaterThan::NOT_GREATER_INCLUSIVE => "Value must greater or equal to 0"
                            ),
                        ),
                        'break_chain_on_failure' => true,
                    ),
                ),

            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}