<?php

namespace Form\SuperAdminSettings;

use Zend\Form\Element\Textarea;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\Model\AppModel;
use Application\Model\TemplateModel;
use Form\LayoutForm;

class SettingsForm extends LayoutForm
{
    private $appModel = null;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        parent::__construct('super-admin-settings');

        $this->serviceLocator = $serviceLocator;
        $this->appModel = new AppModel($serviceLocator);

        $this->setAttribute('method', 'POST');
        $this->setAttribute('enctype', 'multipart/form-data');


        $this->add(array(
            'name' => 'max-push-notification',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'max-push-notification',
                'class' => 'form-control',
                'placeholder' => 'Max push notification / month',
                'style' => 'margin-bottom: 10px;',
            ),
            'options' => array(
                'label' => 'Max push notification / month',
            ),
        ));

        $this->add(array(
            'name' => 'sent-push-notification',
            'attributes' => array(
                'type' => 'Text',
                'readonly' => true,
                'id' => 'sent-push-notification',
                'class' => 'form-control',
                'placeholder' => 'Sent push notification / month',
                'style' => 'margin-bottom: 10px;',
            ),
            'options' => array(
                'label' => 'Sent push notification / month',
            ),
        ));

        $this->add(array(
            'name' => 'max-advice-hours',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'max-advice-hours',
                'class' => 'form-control',
                'placeholder' => 'Max hours advice / month',
                'style' => 'margin-bottom: 10px;',
            ),
            'options' => array(
                'label' => 'Max hours advice / month',
            ),
        ));

        $this->add(array(
            'name' => 'advice-hours-done',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'advice-hours-done',
                'class' => 'form-control',
                'placeholder' => 'Hours advice done',
                'style' => 'margin-bottom: 10px;',
            ),
            'options' => array(
                'label' => 'Hours advice done',
            ),
        ));

        $this->add(array(
            'name' => 'max-insert-contents',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'max-insert-contents',
                'class' => 'form-control',
                'placeholder' => 'Max insert contents',
                'style' => 'margin-bottom: 10px;',
            ),
            'options' => array(
                'label' => 'Max insert contents',
            ),
        ));

        $this->add(array(
            'name' => 'save',
            'type' => 'Button',
            'options' => array(
                'label' => 'SAVE <span class="glyphicon glyphicon-ok" style="float: right;"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
            'attributes' => array(
                'id' => 'save',
                'class' => 'btn btn-success btn-block btn-flat',
            ),
        ));


//        $this->addLayoutBlock(array('page_size', 'paged', 'open_in_app', 'autoplay', 'streaming', 'connector'), LayoutForm::APP);
    }


    public function getView()
    {
        return 'form/super-admin-settings';
    }

}