<?php

namespace Form\App;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

class Factory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $form = new AppForm($serviceLocator);
        $Translator = $serviceLocator->get('translator');
        AbstractValidator::setDefaultTranslator($Translator);

        $filter = new AppFilter($serviceLocator);
        $form->setInputFilter($filter->getInputFilter());

        return $form;

    }
}