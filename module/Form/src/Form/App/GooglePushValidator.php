<?php

namespace Form\App;

use Application\Model\AppModel;
use Common\Helper\AclHelper;
use Zend\Validator\AbstractValidator;

class GooglePushValidator extends AbstractValidator
{
    const INVALID_FILE_TYPE = 'INVALID_FILE_TYPE';
    const INVALID_BUNDLE = 'INVALID_BUNDLE';
    const UNKNOWN_ERROR = 'UNKNOWN_ERROR';
    const UPLOAD_ERR_INI_SIZE = 'UPLOAD_ERR_INI_SIZE';
    const UPLOAD_ERR_FORM_SIZE = 'UPLOAD_ERR_FORM_SIZE';
    const UPLOAD_ERR_PARTIAL = 'UPLOAD_ERR_PARTIAL';
    const UPLOAD_ERR_NO_TMP_DIR = 'UPLOAD_ERR_NO_TMP_DIR';
    const UPLOAD_ERR_CANT_WRITE = 'UPLOAD_ERR_CANT_WRITE';
    const UPLOAD_ERR_EXTENSION = 'UPLOAD_ERR_EXTENSION';

    protected $messageTemplates = array(
        self::INVALID_FILE_TYPE => 'Invalid file type. Must be JSON',
        self::INVALID_BUNDLE => 'Invalid bundle. Json file must contain \'%value%\' bundle',
        self::UNKNOWN_ERROR => 'Unknown error',
        self::UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
        self::UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
        self::UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded.',
        self::UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder.',
        self::UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
        self::UPLOAD_ERR_EXTENSION => 'A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help.',
    );

    public function __construct(array $options = array())
    {
        parent::__construct($options);
    }

    public function isValid($value)
    {
//        die("isValid???");
        $fileType = $value['type'];
        $fileTempName = $value['tmp_name'];
        $fileSize = $value['size'];
        $fileName = $value['name'];
        $fileError = $value['error'];

        $fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);

        if ($fileError === 4) {
            $result = true;
        } else {
            if ($fileExtension === AppModel::GOOGLE_JSON_TYPE && $fileType === "application/json") {
                switch ($fileError) {
                    case 0:
                        $serviceLocator = $this->getOption('serviceLocator');
                        $config = $serviceLocator->get('Config');
                        $aclHelper = new AclHelper($serviceLocator);

                        $bundle = isset($_POST['bundle']) ? $config['bundle_root'] . $_POST['bundle'] : $aclHelper->getApp()->bundle;
                        $content = file_get_contents($fileTempName);

                        $pos = strpos($content, '"' . $bundle . '"');
                        if ($pos === false) {
                            $result = false;
                            $this->setValue($bundle);
                            $this->error(self::INVALID_BUNDLE);
                        } else {
                            $result = true;
                        }
                        break;
                    case 1:
                        $this->error(self::UPLOAD_ERR_INI_SIZE);
                        $result = false;
                        break;
                    case 2:
                        $this->error(self::UPLOAD_ERR_FORM_SIZE);
                        $result = false;
                        break;
                    case 3:
                        $this->error(self::UPLOAD_ERR_PARTIAL);
                        $result = false;
                        break;
                    case 6:
                        $this->error(self::UPLOAD_ERR_NO_TMP_DIR);
                        $result = false;
                        break;
                    case 7:
                        $this->error(self::UPLOAD_ERR_CANT_WRITE);
                        $result = false;
                        break;
                    case 8:
                        $this->error(self::UPLOAD_ERR_EXTENSION);
                        $result = false;
                        break;
                    default:
                        $this->error(self::UNKNOWN_ERROR);
                        $result = false;
                        break;
                }

            } else {
                $this->error(self::INVALID_FILE_TYPE);
                $result = false;
            }
        }

        return $result;
    }
}