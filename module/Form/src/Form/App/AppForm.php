<?php

namespace Form\App;

use Zend\ServiceManager\ServiceLocatorInterface;
use Application\Model\AppModel;
use Application\Model\TemplateModel;
use Form\LayoutForm;

class AppForm extends LayoutForm
{
    private $appModel = null;
    private $editMode = false;
    private $action = AppForm::CREATE_APP;

    const CREATE_APP = "create";
    const UPDATE_APP = "update";

    const TOOLBAR_SLIDESHOW = "SLIDESHOW";
    const TOOLBAR_TITLE = "TITLE";
    const TOOLBAR_ICON = "ICON";

    const TOOLBAR_SLIDESHOW_LABEL = "SLIDESHOW";
    const TOOLBAR_TITLE_LABEL = "NAVIGATION BAR";
    const TOOLBAR_ICON_LABEL = "APP LOGO";


    private $disabledTabFields = array();

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        parent::__construct('app');

        $this->serviceLocator = $serviceLocator;
        $this->appModel = new AppModel($serviceLocator);
        $this->templateModel = new TemplateModel($serviceLocator);

        $this->setAttribute('method', 'POST');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->addHiddenField("icon");
        $this->addHiddenField("splash");
        $this->addHiddenField("app-logo");
        $this->addHiddenField("old_layout");
        $this->addHiddenField("new_layout");

        /* STEP 1 - MAIN DATA */
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'title',
                'class' => 'form-control',
                'placeholder' => 'Title*',
                'style' => 'margin-bottom: 10px;',
            ),
            'options' => array(
                'label' => 'Title',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'category',
            'attributes' => array(
                'id' => 'category',
                'title' => 'Select category*',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => $this->appModel->getCategories(),
            ),
            'options' => array(
                'label' => 'Category',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'name' => 'bundle',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'bundle',
                'class' => 'form-control',
                'placeholder' => 'Bundle* (unique app name)',
            ),
            'options' => array(
                'label' => 'Bundle',
            ),
        ));

        $this->add(array(
            'name' => 'description',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'description',
                'class' => 'form-control',
                'placeholder' => "A brief description of your app...",
                'style' => 'height: 152px;',
                'width' => '100%',
            ),
            'options' => array(
                'label' => "Description",
            ),
        ));

        /* STEP 4 - MEDIA */
        $this->add(array(
            'name' => 'upload-icon',
            'attributes' => array(
                'type' => 'File',
                'id' => 'upload-icon',
                'class' => 'file-loading',
            ),
            'options' => array(
                'label' => 'Icon',
            )
        ));

        $this->add(array(
            'name' => 'upload-app-logo',
            'attributes' => array(
                'type' => 'File',
                'id' => 'upload-app-logo',
                'class' => 'file-loading',
            ),
            'options' => array(
                'label' => 'App logo',
            )
        ));

        $this->add(array(
            'name' => 'splash-bg',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'splash-bg',
                'class' => 'form-control advanced-layout',
                'value' => '#ffffff'
            ),
            'options' => array(
                'label' => 'Background color',
            ),
        ));

        $this->add(array(
            'name' => 'upload-splash',
            'attributes' => array(
                'type' => 'File',
                'id' => 'upload-splash',
                'class' => 'file-loading',
            ),
            'options' => array(
                'label' => 'Splash screen',
            )
        ));

        $this->add(array(
            'name' => 'get-splash-screen-image',
            'type' => 'Button',
            'attributes' => array(
                'id' => 'get-splash-screen-image',
                'value' => 'Get Splash Screen Image',
                'class' => 'btn btn-primary btn-block btn-flat',
            ),
            'options' => array(
                'label' => 'Get Splash Screen Image',
            ),
        ));

        /* STEP 5 - SETTINGS */
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'toolbar',
            'attributes' => array(
                'id' => 'toolbar',
                'class' => 'selectpicker',
                'options' => [
                    self::TOOLBAR_SLIDESHOW => self::TOOLBAR_SLIDESHOW_LABEL,
                    self::TOOLBAR_TITLE => self::TOOLBAR_TITLE_LABEL,
                    self::TOOLBAR_ICON => self::TOOLBAR_ICON_LABEL,
                ],
                'data-width' => '100%',
            ),
            'options' => array(
                'label' => 'Header',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'name' => 'upload-apple-certificate',
            'attributes' => array(
                'type' => 'File',
                'id' => 'upload-apple-certificate',
                'class' => 'file',
                'data-show-preview' => false,
                'data-show-upload' => false,
                'data-show-caption' => false,
                'data-allowed-file-extensions' => '["pem"]'
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'control-label'
                ),
                'label' => 'Apple .pem certificate',
            )
        ));

        $this->add(array(
            'name' => 'pass-phrase-apple-certificate',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'pass-phrase-apple-certificate',
                'class' => 'form-control',
                'placeholder' => 'Insert pass phrase if required',
            ),
            'options' => array(
                'label' => 'Certificate pass phrase',
            ),
        ));

        $this->add(array(
            'name' => 'upload-google-notification-json',
            'attributes' => array(
                'type' => 'File',
                'id' => 'upload-google-notification-json',
                'class' => 'file',
                'data-show-preview' => false,
                'data-show-upload' => false,
                'data-show-caption' => false,
            ),
            'options' => array(
                'label_attributes' => array(
                    'class' => 'control-label'
                ),
                'label' => 'Google Notification .json',
            )
        ));

        $this->add(array(
            'name' => 'download-google-notification-json',
            'attributes' => array(
                'type' => 'Button',
                'id' => 'download-google-notification-json',
                'class' => 'btn btn-md btn-success white-space',
            ),
            'options' => array(
                'label' => '<i class="glyphicon glyphicon-download"></i> Download Google Notification .json',
                'label_attributes' => array(
                    'class' => 'control-label',
                ),
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            )
        ));

        $this->add(array(
            'name' => 'google-api-key',
            'attributes' => array(
                'type' => 'Textarea',
                'id' => 'google-api-key',
                'class' => 'form-control text-area-no-resize',
                'rows' => 5,
                'placeholder' => 'Insert Google Api Key',
            ),
            'options' => array(
                'label' => 'Google Api Key',
            ),
        ));

        $this->add(array(
            'name' => 'facebook-token',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'facebook-token',
                'class' => 'form-control',
                'placeholder' => 'Insert Facebook App ID',
            ),
            'options' => array(
                'label' => 'Facebook App ID',
            ),
        ));

        $this->add(array(
            'name' => 'google-analytics-ua',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'google-analytics-ua',
                'class' => 'form-control',
                'placeholder' => 'Insert Google Analytics UA',
            ),
            'options' => array(
                'label' => 'Google Analytics UA',
            ),
        ));

        $this->add(array(
            'name' => 'store-apple-username',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'store-apple-username',
                'class' => 'form-control',
                'placeholder' => 'Apple ID',
            ),
            'options' => array(
                'label' => 'Apple ID',
            ),
        ));

        $this->add(array(
            'name' => 'store-apple-password',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'store-apple-password',
                'class' => 'form-control',
                'placeholder' => 'Apple Store Password',
            ),
            'options' => array(
                'label' => 'Password',
            ),
        ));

        $this->add(array(
            'name' => 'store-apple-app-url',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'store-apple-app-url',
                'class' => 'form-control',
                'placeholder' => 'Your app link to App Store',
            ),
            'options' => array(
                'label' => 'App Store Link',
            ),
        ));

        $this->add(array(
            'name' => 'store-google-username',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'store-google-username',
                'class' => 'form-control',
                'placeholder' => 'Play Store Username',
            ),
            'options' => array(
                'label' => 'Username',
            ),
        ));

        $this->add(array(
            'name' => 'store-google-password',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'store-google-password',
                'class' => 'form-control',
                'placeholder' => 'Play Store Password',
            ),
            'options' => array(
                'label' => 'Password',
            ),
        ));

        $this->add(array(
            'name' => 'store-google-app-url',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'store-google-app-url',
                'class' => 'form-control',
                'placeholder' => 'Your app link to Play Store',
            ),
            'options' => array(
                'label' => 'Play Store Link',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'app-published',
            'attributes' => array(
                'id' => 'app-published',
                'class' => 'switch-btn',
            ),
            'options' => array(
                'label' => 'APP Published in Store',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'main-language',
            'attributes' => array(
                'id' => 'main-language',
                'title' => 'Select main contents language*',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => $this->appModel->getLanguages(),
            ),
            'options' => array(
                'label' => 'Main Contents Language*',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'other-language',
            'attributes' => array(
                'id' => 'other-language',
                'title' => 'Select other languages',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'multiple' => true,
                'options' => $this->appModel->getLanguages(),
            ),
            'options' => array(
                'label' => 'Other Contents Languages',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->addLayoutBlock(array('page_size', 'paged', 'open_in_app', 'autoplay', 'streaming', 'connector'), LayoutForm::APP);
    }

    public function setExtraAppIcons($extraAppIcons)
    {
        foreach ($extraAppIcons as $icon) {
            $this->addHiddenField($icon['value']);

            $this->add(array(
                'name' => 'upload-' . $icon['value'],
                'attributes' => array(
                    'type' => 'File',
                    'id' => 'upload-' . $icon['value'],
                    'class' => 'file-loading',
                    'data-type' => $icon['type'],
                ),
                'options' => array(
                    'label' => $icon['label'],
                )
            ));
        }
    }

    public function setControllerUriFields($controllerUriFields = null)
    {
        // Add iOS/Android form fields
        if (!is_null($controllerUriFields)) {
            foreach ($controllerUriFields as $os) {
                foreach ($os as $fieldId => $field) {
                    $this->add(
                        array(
                            'name' => $fieldId,
                            'attributes' => array(
                                'type' => 'Text',
                                'id' => $fieldId,
                                'class' => 'form-control',
                                'placeholder' => $field['placeholder'],
                            ),
                            'options' => array(
                                'label' => $field['label'],
                            ),
                        )
                    );
                }
            }
        }
    }

    public function setAction($action)
    {
        $this->action = $action;

        switch ($this->action) {
            case AppForm::CREATE_APP:
                $this->add(array(
                    'name' => 'back',
                    'type' => 'Button',
                    'options' => array(
                        'label' => '<span class="glyphicon glyphicon-menu-left" style="float: left;"></span> BACK',
                        'label_options' => array(
                            'disable_html_escape' => true,
                        )
                    ),
                    'attributes' => array(
                        'id' => 'back',
                        'class' => 'btn btn-default btn-block btn-flat hidden',
                    ),
                ));

                $this->add(array(
                    'name' => 'next',
                    'type' => 'Button',
                    'options' => array(
                        'label' => 'NEXT <span class="glyphicon glyphicon-menu-right" style="float: right;"></span>',
                        'label_options' => array(
                            'disable_html_escape' => true,
                        )
                    ),
                    'attributes' => array(
                        'id' => 'next',
                        'class' => 'btn btn-primary btn-block btn-flat',
                    ),
                ));

                $this->add(array(
                    'name' => 'save',
                    'type' => 'Button',
                    'options' => array(
                        'label' => 'SAVE <span class="glyphicon glyphicon-ok" style="float: right;"></span>',
                        'label_options' => array(
                            'disable_html_escape' => true,
                        )
                    ),
                    'attributes' => array(
                        'id' => 'save',
                        'class' => 'btn btn-success btn-block btn-flat hidden',
                    ),
                ));
                break;
            case AppForm::UPDATE_APP:
                $this->add(array(
                    'name' => 'save',
                    'type' => 'Button',
                    'options' => array(
                        'label' => 'SAVE <span class="glyphicon glyphicon-ok" style="float: right;"></span>',
                        'label_options' => array(
                            'disable_html_escape' => true,
                        )
                    ),
                    'attributes' => array(
                        'id' => 'save',
                        'class' => 'btn btn-success btn-block btn-flat',
                    ),
                ));
                break;
        }
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setEditMode($enable)
    {
        $this->editMode = $enable;

        $this->add(array(
            'name' => 'bundle',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'bundle',
                'class' => 'form-control',
                'placeholder' => 'Bundle* (unique app name identifier)',
                'disabled' => 'disabled',
            ),
            'options' => array(
                'label' => 'Bundle',
            ),
        ));

        $this->getInputFilter()->remove('bundle');
    }

    public function isEditMode()
    {
        return $this->editMode;
    }

    public function getDisabledTabFields()
    {
        return $this->disabledTabFields;
    }

    public function getView()
    {
        return 'form/app';
    }

}