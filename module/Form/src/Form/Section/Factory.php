<?php

namespace Form\Section;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use \Application\Model\SectionModel;

class Factory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $sectionType = SectionModel::getSelectedType();
        $form = null;
        $filter = null;

        switch ($sectionType) {
            case SectionForm::RSS:
                $filter = new Rss\RssSectionFilter();
                $form = new Rss\RssSectionForm($serviceLocator);
                break;
            case SectionForm::PODCAST:
                $filter = new Podcast\PodcastSectionFilter();
                $form = new Podcast\PodcastSectionForm($serviceLocator);
                break;
            case SectionForm::TWITTER:
                $filter = new Twitter\TwitterSectionFilter();
                $form = new Twitter\TwitterSectionForm($serviceLocator);
                break;
            case SectionForm::REGISTER:
                $filter = new Register\RegisterSectionFilter();
                $form = new Register\RegisterSectionForm($serviceLocator);
                break;
            case SectionForm::FAVOURITE:
                $filter = new Favourite\FavouriteSectionFilter();
                $form = new Favourite\FavouriteSectionForm($serviceLocator);
                break;
            case SectionForm::INFO:
                $filter = new Info\InfoSectionFilter();
                $form = new Info\InfoSectionForm($serviceLocator);
                break;
            case SectionForm::EVENT:
                $filter = new Event\EventSectionFilter();
                $form = new Event\EventSectionForm($serviceLocator);
                break;
            case SectionForm::PERSON:
                $filter = new Person\PersonSectionFilter();
                $form = new Person\PersonSectionForm($serviceLocator);
                break;
            case SectionForm::POI:
                $filter = new Poi\PoiSectionFilter();
                $form = new Poi\PoiSectionForm($serviceLocator);
                break;
            case SectionForm::GENERIC:
                $filter = new Generic\GenericSectionFilter();
                $form = new Generic\GenericSectionForm($serviceLocator);
                break;
            case SectionForm::CUSTOM_FORM:
                $filter = new CustomForm\CustomFormSectionFilter();
                $form = new CustomForm\CustomFormSectionForm($serviceLocator);
                break;
            case SectionForm::WEB:
                $filter = new Web\WebSectionFilter();
                $form = new Web\WebSectionForm($serviceLocator);
                break;
            case SectionForm::SECTION:
                $filter = new Section\SectionSectionFilter();
                $form = new Section\SectionSectionForm($serviceLocator);
                break;
            case SectionForm::DEV:
                $filter = new Dev\DevSectionFilter();
                $form = new Dev\DevSectionForm($serviceLocator);
                break;
            case SectionForm::QR_CODE:
                $filter = new QrCode\QrCodeSectionFilter();
                $form = new QrCode\QrCodeSectionForm($serviceLocator);
                break;
            case SectionForm::USER:
                $filter = new User\UserSectionFilter($serviceLocator);
                $form = new User\UserSectionForm($serviceLocator);
                break;
        }

        $form->setInputFilter($filter->getInputFilter());

        $Translator = $serviceLocator->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($Translator);

        return $form;
    }
}