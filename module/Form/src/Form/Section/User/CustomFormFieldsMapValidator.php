<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 16/10/17
 * Time: 12:38
 */

namespace Form\Section\User;

use Application\Model\HpModel;
use Application\Model\SectionModel;
use Database\HubPlus\SectionQuery;
use Rest\Model\Json\ContentUserSerializer;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class CustomFormFieldsMapValidator extends AbstractValidator
{
    const CUSTOM_FORM_ERROR = 'CUSTOM_FORM_ERROR';
    const MISSING_MAPPING = 'MISSING_MAPPING';
    const UNIQUE_FIELD = 'UNIQUE_FIELD';
    const REQUIRE_FIELD = 'REQUIRE_FIELD';
    const REQUIRE_CUSTOM_FORM_FIELD = 'REQUIRE_CUSTOM_FORM_FIELD';

    protected $messageTemplates = array(
        self::CUSTOM_FORM_ERROR => 'An error occurred to get form params (form id: %s).',
        self::MISSING_MAPPING => 'Missing fields mapping: %s',
        self::UNIQUE_FIELD => '%s must be unique.',
        self::REQUIRE_FIELD => '%s is required.',
        self::REQUIRE_CUSTOM_FORM_FIELD => 'In form "%s" at least one mandatory field is required between these: %s',
    );

    /** @var string[] */
    private $messages = [];

    public function __construct(array $options = array())
    {
        parent::__construct($options);
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param mixed $value
     * @param $context
     * @return bool
     */
    public function isValid($value, $context = null)
    {
        $this->messages = [];
        $result = true;
        $value = json_decode($value);
        $customFormParams = null;

        $customFormId = $context['custom_form_list'];
        $customForm = SectionQuery::create()->findPk($customFormId);
        if ($customForm) {
            $customFormParams = json_decode($customForm->getParams());
        }

        if (!is_null($customFormParams)) {
            $serviceLocator = $this->getOption('serviceLocator');
            $customFormFieldsMapConfig = $serviceLocator->get('Config')['custom_form_fields_map'];
            $HpModel = new HpModel($serviceLocator);
            $lang = $HpModel->aclHelper->session->currentLang;

            $mappedKeys = array();

            foreach ($value as $field) {
                foreach ($field as $fieldName => $map) {
                    $mappedKeys[$fieldName] = $map->map_type;
                }
            }

            // counting form field
            $formFieldsNumber = array();
            foreach ($customFormParams->fieldsets as $fieldSet) {
                foreach ($fieldSet->fields as $field) {
                    if ($field->type !== SectionModel::HPFORM_INPUT_DESCR) {
                        if (isset($field->name)) {
                            $formFieldsNumber[$field->name] = array(
                                'label' => $HpModel->getLocaleText($lang, json_encode($field->label)),
                            );
                        } else {
                            $formFieldsNumber[$field->label] = array(
                                'label' => $field->label,
                            );
                        }
                    }
                }
            }

            if (count($mappedKeys) < count($formFieldsNumber)) {
                $missingFieldsMap = array_diff_key($formFieldsNumber, $mappedKeys);
                $missingFieldsMapString = implode(', ', array_map(
                    function ($v) {
                        if (is_array($v)) {
                            return implode(', ', $v);
                        } else {
                            return $v;
                        }
                    },
                    $missingFieldsMap
                ));

                $result = false;
                $this->messages[self::MISSING_MAPPING] = sprintf($this->messageTemplates[self::MISSING_MAPPING], $missingFieldsMapString);
            } else {
                $mappedKeysCount = array_count_values($mappedKeys);

                foreach ($customFormFieldsMapConfig as $key => $fieldMap) {
                    // Check Unique fields
                    if ($fieldMap['unique']) {
                        if (isset($mappedKeysCount[$key])) {
                            if ($mappedKeysCount[$key] > 1) {
                                $result = false;
                                $this->messages[self::UNIQUE_FIELD] = sprintf($this->messageTemplates[self::UNIQUE_FIELD], $fieldMap['value']);
                            }
                        }

                    }

                    // Check Required fields
                    if ($fieldMap['required']) {
                        if (!isset($mappedKeysCount[$key])) {
                            $result = false;
                            $this->messages[self::REQUIRE_FIELD] = sprintf($this->messageTemplates[self::REQUIRE_FIELD], $fieldMap['value']);
                        } else {


                            $fieldRequired = false;
                            $fieldRequiredLabel = array();
                            // check if form's mapped field is mandatory
                            foreach ($customFormParams->fieldsets as $fieldSet) {
                                foreach ($fieldSet->fields as $field) {
                                    $name = "";
                                    $label = "";
                                    if (isset($field->name) && is_object($field->label) && isset($mappedKeys[$field->name])) {
                                        $name = $field->name;
                                        $label = $HpModel->getLocaleText($lang, json_encode($field->label));
                                    } elseif (isset($field->label) && is_string($field->label) && isset($mappedKeys[$field->label])) {
                                        $name = $field->label;
                                        $label = $name;
                                    }

                                    $type = null;
                                    if (isset($mappedKeys[$name])) {
                                        $type = $mappedKeys[$name];
                                    }

                                    if ($type === $key) {
                                        $fieldRequiredLabel[$type][] = $label;
                                        if ($field->required === true) {
                                            $fieldRequired = $field->required;
                                        }
                                    }
                                }
                            }
                            if ($fieldRequired === false) {
                                $result = false;
                                /** @var \Zend\View\Helper\Url $urlHelper */
                                $urlHelper = $serviceLocator->get('ViewHelperManager')->get('url');
                                $customFormUrl = $HpModel->getBaseurl() . $urlHelper->__invoke("section-update",
                                        array(
                                            "type" => "custom_form",
                                            "id" => $customFormId
                                        )
                                    );
                                $customFormTitle = "<a href='$customFormUrl'>" . $HpModel->getLocaleText($lang, $customForm->getTitle()) . "</a>";

                                $fieldErrorText = "";
                                $idx = 0;
                                foreach ($fieldRequiredLabel as $type => $fields) {
                                    if ($idx > 0) {
                                        $fieldErrorText .= ", ";
                                    }
                                    $fieldErrorText .= implode(" or ", $fields) . " for " . $type;
                                    $idx++;
                                }

                                $this->messages[self::REQUIRE_CUSTOM_FORM_FIELD] = sprintf($this->messageTemplates[self::REQUIRE_CUSTOM_FORM_FIELD], $customFormTitle, $fieldErrorText);
                            }
                        }
                    }
                }

                // Body check
                if (!isset($mappedKeysCount[ContentUserSerializer::MAP_TYPE_BODY_TEXT]) && !isset($mappedKeysCount[ContentUserSerializer::MAP_TYPE_BODY_TABLE])) {
                    $result = false;
                    $this->messages[self::REQUIRE_FIELD] = sprintf($this->messageTemplates[self::REQUIRE_FIELD], 'A body (text or table)');
                }
            }
        } else {
            $result = false;
            $this->messages[self::CUSTOM_FORM_ERROR] = sprintf($this->messageTemplates[self::CUSTOM_FORM_ERROR], $customFormId);
        }

        return $result;
    }

    public function getMessages()
    {
        return $this->messages;
    }
}