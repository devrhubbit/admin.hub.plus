<?php

namespace Form\Section\User;

use Application\Model\SectionModel;
use Database\HubPlus\Section;
use Form\Section\CustomForm\CustomFormSectionForm;
use Form\Section\SectionForm;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserSectionForm extends SectionForm
{
    private $sectionModel = null;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::USER);

        $this->sectionModel = new SectionModel($serviceLocator);

        $config = $serviceLocator->get('config');

        $this->addHiddenField("user_section_add_icon");
        $this->addHiddenField("user_section_add_icon_id");
        $this->addHiddenField("user_section_edit_icon");
        $this->addHiddenField("user_section_edit_icon_id");
        $this->addHiddenField("user_section_custom_form_field_map_json", "{}");

        $iconsPositionOptions = array();
        foreach ($config['user_section_icons_position'] as $opt) {
            $iconsPositionOptions[] = array(
                'value' => $opt["value"],
                'label' => $opt["label"],
            );
        }

        $detailLayoutOptions = array();
        foreach ($config['list_page_types'] as $opt) {
            if (in_array(strtoupper(SectionForm::USER), $opt['types'])) {
                $detailLayoutOptions[] = array(
                    'value' => $opt["value"],
                    'label' => $opt["label"],
                );
            }
        }

        $contentFilterOptions = array();
        foreach ($config['user_section_content_filter'] as $opt) {
            $contentFilterOptions[] = array(
                'value' => $opt["value"],
                'label' => $opt["label"],
            );
        }

        $this->add(array(
            'name' => 'tags',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'tags',
                'class' => 'form-control',
                'placeholder' => 'type something and hit enter',
            ),
            'options' => array(
                'label' => 'TAGS <span class="tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'custom_form_list',
            'attributes' => array(
                'id' => 'custom_form_list',
//                'title' => 'Select Custom Form',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => $this->sectionModel->getCustomFormSelectList(CustomFormSectionForm::USER_FORM),
            ),
            'options' => array(
                'label' => 'Select Custom Form*',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'name' => 'upload_user_section_add_icon',
            'attributes' => array(
                'type' => 'File',
                'id' => 'upload_user_section_add_icon',
                'class' => 'file-loading',
            ),
            'options' => array(
                'label' => 'Add Icon',
            ),
        ));

        $this->add(array(
            'name' => 'upload_user_section_add_icon_label',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'upload_user_section_add_icon_label',
                'class' => 'form-control',
                'placeholder' => 'Add icon label',
            ),
            'options' => array(
                'label' => 'Label',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'upload_user_section_add_icon_position',
            'attributes' => array(
                'id' => 'upload_user_section_add_icon_position',
                'title' => 'Position',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => $iconsPositionOptions,
            ),
            'options' => array(
                'label' => 'Add icon position',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'user_section_detail_layout',
            'attributes' => array(
                'id' => 'user_section_detail_layout',
                'title' => 'Detail layout',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => $detailLayoutOptions,
            ),
            'options' => array(
                'label' => 'Detail layout*',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'user_section_content_filter',
            'attributes' => array(
                'id' => 'user_section_content_filter',
                'title' => 'Content filter',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => $contentFilterOptions,
            ),
            'options' => array(
                'label' => 'Content filter*',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->addLayoutBlock(array('paged', 'open_in_app', 'autoplay', 'streaming'), SectionForm::USER);
    }

    public function getView()
    {
        return 'form/section/user';
    }

}