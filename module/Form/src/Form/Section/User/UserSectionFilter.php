<?php

namespace Form\Section\User;

use Form\Section\SectionFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserSectionFilter extends SectionFilter {

    protected $serviceLocator;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getInputFilter() {

        parent::getInputFilter();

        if ($this->inputFilter) {
            /** @var UserSectionFilter $inputFilter */
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'connector_validator',
                'validators' => array(
                    array(
                        'name' => '\Form\Section\ConnectorValidator',
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'provider',
                'required' => false,
                'allow_empty' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'upload_user_section_add_icon_position',
                'required' => false,
                'allow_empty' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'user_section_custom_form_field_map_json',
                'validators' => array(
                    array(
                        'name' => '\Form\Section\User\CustomFormFieldsMapValidator',
                        'options' => array(
                            'serviceLocator' => $this->serviceLocator,
                        ),
                    ),
                ),
            )));

        }

        return $this->inputFilter;
    }

}