<?php

namespace Form\Section\Web;

use Form\LayoutForm;
use Form\Section\SectionForm;

class WebSectionForm extends SectionForm {

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::WEB);

        $this->add(array(
            'name' => 'tags',
            'attributes' => array(
                'type' 	      => 'Text',
                'id' 	      => 'tags',
                'class'       => 'form-control',
                'placeholder' => 'type something and hit enter',
            ),
            'options' => array(
                'label' => 'TAGS <span class="tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'order',
            'attributes' => array(
                'id' => 'order',
                'title' => 'Select how to order content',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => array('ASC'=>'Ascendent', 'DESC'=>'Descendent'),
            ),
            'options' => array(
                'label' => 'Order*',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->addLayoutBlock(array('paged', 'open_in_app', 'autoplay', 'streaming'), LayoutForm::WEB);
    }

    public function getView() {
        return 'form/section/web';
    }
}