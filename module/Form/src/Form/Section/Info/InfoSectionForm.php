<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 17:57
 */

namespace Form\Section\Info;


use Form\LayoutForm;
use Form\Section\SectionForm;

class InfoSectionForm extends SectionForm
{

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::INFO);

        $this->addHiddenField("latitude");
        $this->addHiddenField("longitude");

        $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'address',
                'class' => 'form-control',
                'placeholder' => 'Insert your address here',
            ),
            'options' => array(
                'label' => 'Address',
            ),
        ));

        $this->add(array(
            'name' => 'street',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'street',
                'disabled' 	=> 'disabled',
                'class' => 'address-field form-control',
                'placeholder' => 'Street',
            ),
            'options' => array(
                'label' => 'Street',
            ),
        ));

        $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'city',
                'disabled' 	=> 'disabled',
                'class' => 'address-field form-control',
                'placeholder' => 'City',
            ),
            'options' => array(
                'label' => 'City',
            ),
        ));

        $this->add(array(
            'name' => 'state',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'state',
                'disabled' 	=> 'disabled',
                'class' => 'address-field form-control',
                'placeholder' => 'State',
            ),
            'options' => array(
                'label' => 'State or Province',
            ),
        ));

        $this->add(array(
            'name' => 'zip',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'zip',
                'disabled' 	=> 'disabled',
                'class' => 'address-field form-control',
                'placeholder' => 'Postal code',
            ),
            'options' => array(
                'label' => 'Postal code',
            ),
        ));

        $this->add(array(
            'name' => 'country',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'country',
                'disabled' 	=> 'disabled',
                'class' => 'address-field form-control',
                'placeholder' => 'Country',
            ),
            'options' => array(
                'label' => 'Country',
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'email',
                'class' => 'form-control',
                'placeholder' => 'Email address',
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));

        $this->add(array(
            'name' => 'site',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'site',
                'class' => 'form-control',
                'placeholder' => 'Web site url',
            ),
            'options' => array(
                'label' => 'Website',
            ),
        ));

        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'phone',
                'class' => 'form-control',
                'placeholder' => 'Phone number',
            ),
            'options' => array(
                'label' => 'Phone number',
            ),
        ));

        $this->add(array(
            'name' => 'info-description',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' 	=> 'info-description',
                'class' => 'form-control',
                'placeholder'=> 'My company...',
            ),
            'options' => array(
                'label' => 'DESCRIPTION',
            ),
        ));

        $this->add(array(
            'name' => 'tags',
            'attributes' => array(
                'type' 	      => 'Text',
                'id' 	      => 'tags',
                'class'       => 'form-control',
                'placeholder' => 'type something and hit enter',
            ),
            'options' => array(
                'label' => 'TAGS <span class="tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->addLayoutBlock(array('auth', 'filter', 'share', 'like', 'paged', 'open_in_app', 'autoplay', 'streaming', 'connector'), LayoutForm::INFO);

    }

    public function getView() {
        return 'form/section/info';
    }
}