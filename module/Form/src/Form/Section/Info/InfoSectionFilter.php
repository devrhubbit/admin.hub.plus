<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 18:00
 */

namespace Form\Section\Info;

use Form\Section\SectionFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\Validator\Hostname;

class InfoSectionFilter extends SectionFilter
{
    public function getInputFilter()
    {
        parent::getInputFilter();

        if ($this->inputFilter) {
            $inputFilter = $this->inputFilter;
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name' => 'email',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'EmailAddress',
                        'options' => array(
                            'message' => 'Invalid email address'
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'phone',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Regex',
                        'options' => array(
                            'pattern' => '/^\s*(?:\+?(\d{1,4}))?([-. (]*(\d{1,3})[-. )]*)?((\d{2,4})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/',
                            'message' => "Invalid phone number",
                        )
                    )
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'site',
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => '\Form\Validator\UrlValidator',
                        'options' => array(
                            'allow' => Hostname::ALLOW_DNS,
                            'idn' => true,
                            'tld' => false
                        )
                    )
                ),
            )));
        }

        return $this->inputFilter;
    }
}