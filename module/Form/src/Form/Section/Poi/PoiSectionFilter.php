<?php

namespace Form\Section\Poi;

use Form\Section\SectionFilter;
use Zend\InputFilter\Factory as InputFactory;

class PoiSectionFilter extends SectionFilter {

    public function getInputFilter() {

        parent::getInputFilter();

        if ($this->inputFilter) {
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'region',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'   => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                            ),
                        ),
                        'break_chain_on_failure' => true,
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'beacon_uuid_validator',
                'validators' => array(
                    array(
                        'name' => '\Form\Content\Poi\PoiGeodataValidator',
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'connector_validator',
                'validators' => array(
                    array(
                        'name' => '\Form\Section\ConnectorValidator',
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'provider',
                'required' => false,
                'allow_empty' => true,
            )));

        }

        return $this->inputFilter;
    }

}