<?php

namespace Form\Section\Poi;

use Form\LayoutForm;
use Form\Section\SectionForm;

class PoiSectionForm extends SectionForm {

    const ZOOM_MAP_DEFAULT = 16;

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::POI);

        $this->addHiddenField("mode");
        $this->addHiddenField("region"); //CIRCULAR or BEACON
        $this->addHiddenField("beacon_uuid_validator");

        $this->addHiddenField("latitude");
        $this->addHiddenField("longitude");
        $this->addHiddenField("zoom_map", self::ZOOM_MAP_DEFAULT); // value between 0 - 22

        $this->add(array(
            'name' => 'tags',
            'attributes' => array(
                'type' 	      => 'Text',
                'id' 	      => 'tags',
                'class'       => 'form-control',
                'placeholder' => 'type something and hit enter',
            ),
            'options' => array(
                'label' => 'TAGS <span class="tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'scan',
            'options' => array(
                'label' => 'SCAN mode',
            ),
            'attributes' => array(
                'id' => 'filter',
                'class' => 'switch-btn',
            )
        ));

        $this->add(array(
            'name' => 'subtitle',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'subtitle',
                'class' => 'form-control',
                'placeholder'=> "iBeacon's identification name",
            ),
            'options' => array(
                'label' => 'Name',
            ),
        ));

        $this->add(array(
            'name' => 'uuid',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'uuid',
                'class' => 'form-control',
                'placeholder'=> "iBeacon's UUID*",
            ),
            'options' => array(
                'label' => 'UUID',
            ),
        ));

        $this->add(array(
            'name' => 'major',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'major',
                'class' => 'form-control',
                'placeholder'=> "iBeacon's major",
            ),
            'options' => array(
                'label' => 'Major',
            ),
        ));

        $this->add(array(
            'name' => 'minor',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'minor',
                'class' => 'form-control',
                'placeholder'=> "iBeacon's minor",
            ),
            'options' => array(
                'label' => 'Minor',
            ),
        ));

        $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'address',
                'class' => 'form-control',
                'placeholder' => 'Insert your address here',
                'value' => 'Via Giuseppe Orlandi, 54, 70010 Turi BA, Italia',
            ),
            'options' => array(
                'label' => 'Address',
            ),
        ));

        $this->addLayoutBlock(array('paged', 'open_in_app', 'autoplay', 'streaming'), LayoutForm::POI);
    }

    public function getView() {
        return 'form/section/poi';
    }

}