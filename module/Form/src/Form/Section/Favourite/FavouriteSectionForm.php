<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 17:57
 */

namespace Form\Section\Favourite;

use Common\Helper\AclHelper;
use Form\LayoutForm;
use Form\Section\SectionForm;

class FavouriteSectionForm extends SectionForm
{
    private $content_types = array();

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::FAVOURITE);

        $config = $this->serviceLocator->get('config');
        $dev = $config['dev'];

        $this->content_types = []; //(new AclHelper($this->serviceLocator))->getCategorySetting()->content_types;
        $content_types = $config['content_types'];

        foreach($content_types as $type => $options) {

            if( (!$options['only_dev']) || ($options['only_dev'] && $dev) ) {
                $this->add(array(
                    'type' => 'Zend\Form\Element\Checkbox',
                    'name' => strtolower($type),
                    'options' => array(
                        'label' => "Include $type",
                    ),
                    'attributes' => array(
                        'id' 	  => strtolower($type),
                        'class'   => 'switch-btn',
                    )
                ));
                $this->content_types []= $type;
            }
        }

        $this->add(array(
            'name' => 'tags',
            'attributes' => array(
                'type' 	      => 'Text',
                'id' 	      => 'tags',
                'class'       => 'form-control',
                'placeholder' => 'type something and hit enter',
            ),
            'options' => array(
                'label' => 'TAGS <span class="tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->addLayoutBlock(array('paged', 'open_in_app', 'autoplay', 'streaming'), LayoutForm::FAVOURITE);
    }

    public function getContentTypes() {
        return $this->content_types;
    }

    public function getView() {
        return 'form/section/favourite';
    }
}