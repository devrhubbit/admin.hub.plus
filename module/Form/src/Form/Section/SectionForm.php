<?php

namespace Form\Section;

use Application\Model\UserModel;
use Form\LayoutForm;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;

abstract class SectionForm extends LayoutForm
{
    /* @var $serviceLocator \Zend\ServiceManager\ServiceLocatorInterface */
    protected $serviceLocator;
    private $type;
    private $userModel = null;

    const AREA_HEADER = "HEADER";
    const AREA_HEADER_LABEL = "HEADER";
    const AREA_BODY = "BODY";
    const AREA_BODY_LABEL = "BODY";
    const AREA_MENU = "MENU";
    const AREA_MENU_LABEL = "MENU";
    const AREA_FOOTER = "FOOTER";
    const AREA_FOOTER_LABEL = "FOOTER";
    const AREA_HIDDEN = "HIDDEN";
    const AREA_HIDDEN_LABEL = "HIDDEN";

    public function __construct(ServiceLocatorInterface $serviceLocator, $type)
    {
        parent::__construct('section');

        $this->serviceLocator = $serviceLocator;
        $session = new Container('acl');

        $this->userModel = new UserModel($serviceLocator);

        $this->addHiddenField("section_icon_id");
        $this->addHiddenField("section_icon_name");
        $this->addHiddenField("type", $type);
        $this->addHiddenField("content_lang", $session->currentLang);
        $this->addHiddenField("reload_content", 0);
        $this->type = $type;

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'title',
                'class' => 'form-control',
                'placeholder' => 'Section title*',
            ),
            'options' => array(
                'label' => 'Section title',
            ),
        ));

        $areaOptions = array();
        if ($type === SectionForm::EVENT ||
            $type === SectionForm::PERSON ||
            $type === SectionForm::POI ||
            $type === SectionForm::WEB ||
            $type === SectionForm::GENERIC ||
            $type === SectionForm::SECTION ||
            $type === SectionForm::USER) {
            $areaOptions[SectionForm::AREA_HEADER] = SectionForm::AREA_HEADER_LABEL;
        }
        if ($type !== SectionForm::DEV)  {
            $areaOptions[SectionForm::AREA_BODY] = SectionForm::AREA_BODY_LABEL;
        }
        $areaOptions[SectionForm::AREA_MENU] = SectionForm::AREA_MENU_LABEL;
        $areaOptions[SectionForm::AREA_FOOTER] = SectionForm::AREA_FOOTER_LABEL;
        $areaOptions[SectionForm::AREA_HIDDEN] = SectionForm::AREA_HIDDEN_LABEL;

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'area',
            'attributes' => array(
                'id' => 'area',
                'title' => 'Select area*',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => $areaOptions,
            ),
            'options' => array(
                'label' => 'Area',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'description',
                'class' => 'form-control',
                'placeholder' => 'Description',
            ),
            'options' => array(
                'label' => 'Section description',
            ),
        ));


        $this->add(array(
            'name' => 'upload-icon',
            'attributes' => array(
                'type' => 'File',
                'id' => 'upload-icon',
                'class' => 'file-loading',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'user_tags',
            'attributes' => array(
                'id' => 'user_tags',
                'title' => 'User Tags',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'multiple' => true,
                'options' => $this->userModel->getUserTags(),
            ),
            'options' => array(
                'label' => 'User TAGS <span class="user_tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                ),
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'name' => 'save',
            'type' => 'Button',
            'options' => array(
                'label' => 'SAVE SECTION<span id="save_button_icon" class="glyphicon glyphicon-ok" style="float: right;"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
            'attributes' => array(
                'id' => 'save',
                'value' => 'SAVE SECTION',
                'class' => 'btn btn-success btn-block btn-flat',
            ),
        ));
    }

    public function getType()
    {
        return $this->type;
    }

}


