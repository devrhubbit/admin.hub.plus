<?php

namespace Form\Section\QrCode;

use Form\Section\SectionFilter;
use Zend\InputFilter\Factory as InputFactory;

class QrCodeSectionFilter extends SectionFilter {

    public function getInputFilter() {

        parent::getInputFilter();

        if ($this->inputFilter) {
            /** @var QrCodeSectionFilter $inputFilter */
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

        }

        return $this->inputFilter;
    }

}