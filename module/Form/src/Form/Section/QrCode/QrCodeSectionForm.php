<?php

namespace Form\Section\QrCode;

use Form\Section\SectionForm;

class QrCodeSectionForm extends SectionForm
{
    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::QR_CODE);

        $this->addLayoutBlock(array('paged', 'open_in_app', 'autoplay', 'streaming', 'share', 'like', 'connector', 'filter', 'cell-type', 'page_size'), SectionForm::QR_CODE);
    }

    public function getView()
    {
        return 'form/section/qr-code';
    }
}