<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 17:57
 */

namespace Form\Section\Twitter;

use Form\LayoutForm;
use Form\Section\SectionForm;

class TwitterSectionForm extends SectionForm
{

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::TWITTER);

        $this->add(array(
            'name' => 'tw_filter',
            'attributes' => array(
                'type' 	      => 'Text',
                'id' 	      => 'tw_filter',
                'class'       => 'form-control, tagfield',
                'placeholder' => 'type #something and hit enter',
            ),
            'options' => array(
                'label' => 'Filter with hashtag*',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'enable_retweet',
            'options' => array(
                'label' => 'Enable retweet',
            ),
            'attributes' => array(
                'id' => 'enable_retweet',
                'class' => 'switch-btn',
                'value' => false,
            )
        ));

        $this->add(array(
            'name' => 'retweet',
            'attributes' => array(
                'type' 	      => 'Text',
                'id' 	      => 'retweet',
                'class'       => 'form-control, tagfield',
                'placeholder' => 'type #something and hit enter',
            ),
            'options' => array(
                'label' => 'Retweet with hashtag',
            ),
        ));

        $this->add(array(
            'name' => 'tags',
            'attributes' => array(
                'type' 	      => 'Text',
                'id' 	      => 'tags',
                'class'       => 'form-control',
                'placeholder' => 'type something and hit enter',
            ),
            'options' => array(
                'label' => 'TAGS <span class="tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->addLayoutBlock(array('auth', 'filter', 'share', 'like', 'paged', 'open_in_app', 'autoplay', 'streaming', 'connector'), LayoutForm::TWITTER);

    }

    public function getView() {
        return 'form/section/twitter';
    }
}