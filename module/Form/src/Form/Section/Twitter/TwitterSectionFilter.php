<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 18:00
 */

namespace Form\Section\Twitter;

use Form\Section\SectionFilter;
use Zend\InputFilter\Factory as InputFactory;

class TwitterSectionFilter extends SectionFilter
{
    public function getInputFilter()
    {
        parent::getInputFilter();

        if ($this->inputFilter) {
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'tw_filter',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'   => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                            ),
                        ),
                        'break_chain_on_failure' => true,
                    ),
                ),
            )));

//            $inputFilter->add($factory->createInput(array(
//                'name'     => 'retweet',
//                'required' => true,
//                'filters'  => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                ),
//                'validators' => array(
//                    array(
//                        'name'   => 'NotEmpty',
//                        'options' => array(
//                            'messages' => array(
//                                \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
//                            ),
//                        ),
//                        'break_chain_on_failure' => true,
//                    ),
//                ),
//            )));
        }

        return $this->inputFilter;
    }
}