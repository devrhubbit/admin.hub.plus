<?php
namespace Form\Section\Podcast;

use Form\LayoutForm;
use Form\Section\SectionForm;

class PodcastSectionForm extends SectionForm
{
    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::PODCAST);

        $this->add(array(
            'name' => 'resource',
            'attributes' => array(
                'type' 	=> 'Zend\Form\Element\Url',
                'id' 	=> 'resource',
                'class' => 'form-control',
                'placeholder'=> 'http://www.your-domain.com/feed',
            ),
            'options' => array(
                'label' => 'Resource*',
                'addon' => 'feed url',
            ),
        ));

        $this->addLayoutBlock(array('filter', 'like', 'open_in_app', 'connector'), LayoutForm::PODCAST);
    }

    public function getView() {
        return 'form/section/podcast';
    }
}