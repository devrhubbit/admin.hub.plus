<?php

namespace Form\Section\CustomForm;

use Form\LayoutForm;
use Form\Section\SectionForm;

class CustomFormSectionForm extends SectionForm {

    const USER_FORM         = "USER_FORM";
    const REGISTRATION_FORM = "REGISTRATION_FORM";
    const SURVEY_FORM       = "SURVEY_FORM";
    const CONTACT_FORM      = "CONTACT_FORM";

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::CUSTOM_FORM);

        $this->addHiddenField("custom_forms_details");
        $this->addHiddenField("custom_forms_deleted");

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'form_type',
            'attributes' =>  array(
                'id' => 'form_type',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => [
                    self::USER_FORM         => 'User',
                    self::REGISTRATION_FORM => 'Registration',
                    self::SURVEY_FORM       => 'Survey',
                    self::CONTACT_FORM      => 'Contact',
                ],
            ),
            'options' => array(
                'label' => 'Type',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'name' => 'privacy_link',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'privacy_link',
                'class' => 'form-control',
                'placeholder' => "Link to Privacy page",
                'data-width' => '100%',
            ),
            'options' => array(
                'label' => 'Privacy *',
            ),
        ));

        $this->add(array(
            'name' => 'terms_link',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'terms_link',
                'class' => 'form-control',
                'placeholder' => "Link to Terms & Conditions page",
                'data-width' => '100%',
            ),
            'options' => array(
                'label' => 'Terms & Conditions *',
            ),
        ));

        $this->add(array(
            'name' => 'forward_to',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'forward_to',
                'class' => 'form-control',
                'placeholder' => "Email address of your contact manager",
                'data-width' => '100%',
            ),
            'options' => array(
                'label' => 'Forward to *',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'links_validator',
            'attributes' => array(
                'id' 	=> 'links_validator',
            ),
        ));

        $this->add(array(
            'name' => 'success_message',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'success_message',
                'class' => 'form-control',
                'placeholder' => "Message for users",
                'data-width' => '100%',
            ),
            'options' => array(
                'label' => 'Success message*',
            ),
        ));

        $this->add(array(
            'name' => 'tags',
            'attributes' => array(
                'type' 	      => 'Text',
                'id' 	      => 'tags',
                'class'       => 'form-control',
                'placeholder' => 'type something and hit enter',
            ),
            'options' => array(
                'label' => 'TAGS <span class="tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->addLayoutBlock(array('filter', 'paged', 'open_in_app', 'autoplay', 'streaming', 'share', 'like'), LayoutForm::CUSTOM_FORM);
    }

    public function getView() {
        return 'form/section/custom_form';
    }
}