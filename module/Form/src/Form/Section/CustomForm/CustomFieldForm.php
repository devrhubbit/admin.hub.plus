<?php

namespace Form\Section\CustomForm;

use Form\HpForm;
use Form\Section\SectionForm;

class CustomFieldForm extends HpForm {

    const HP_FORM_SELECT_TYPE_LOCALE = "locale";
    const HP_FORM_SELECT_TYPE_LOCALE_LABEL = "HUB+";
    const HP_FORM_SELECT_TYPE_REMOTE = "remote";
    const HP_FORM_SELECT_TYPE_REMOTE_LABEL = "Remote";
    const HP_FORM_SELECT_RELATED_DROP_DOWN_NULL_LABEL = "Related value";
    const HP_FORM_SELECT_RELATED_TO_NULL_LABEL = "Select related field";
    const HP_FORM_SELECT_DEFAULT_EMPTY_OPTION = "-";

    public function __construct($serviceLocator)
    {

        parent::__construct(SectionForm::CUSTOM_FIELD);

        $config = $serviceLocator->get('config');

        $this->addHiddenField("custom_form_field_icon");

        $this->add(array(
            'name' => 'custom_form_field_label',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'custom_form_field_label',
                'class' => 'form-control',
                'placeholder'=> 'Field label *',
//                'style' => 'width: 210px;',
                'maxlength' => '20',
            ),
            'options' => array(
                'label' => 'Field label *',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'custom_form_old_field_label',
            'attributes' => array(
                'id' 	=> 'custom_form_old_field_label',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'custom_form_field_label_modified',
            'attributes' => array(
                'id' 	=> 'custom_form_field_label_modified',
            ),
            'options' => array(
                'value' => false,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'custom_form_field_required',
            'options' => array(
                'label' => 'Required',
            ),
            'attributes' => array(
                'id' => 'custom_form_field_required',
                'class' => 'switch-btn',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'custom_form_field_hidden',
            'options' => array(
                'label' => 'Hidden',
            ),
            'attributes' => array(
                'id' => 'custom_form_field_hidden',
                'class' => 'switch-btn',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'custom_form_field_is_avatar',
            'options' => array(
                'label' => 'Set as avatar',
            ),
            'attributes' => array(
                'id' => 'custom_form_field_is_avatar',
                'class' => 'switch-btn',
            )
        ));

        $cftopts = array();
        $selectedType = "";
        foreach ($config['custom_form_types'] as $cft) {
            if ($cft['selected']) $selectedType = $cft["value"];
            $cftopts [] = array(
                'value' => $cft["value"],
                'label' => $cft["label"],
            );
        }
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'custom_form_field_type',
            'attributes' =>  array(
                'id' => 'custom_form_field_type',
//                'title' => 'Select area*',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'options' => $cftopts,
//                'data-width' => '210px',
            ),
            'options' => array(
                'label' => 'Field type',
                'disable_inarray_validator' => true,
            ),
        ));

        $cftvopts = array();
        $selectedTypeOption = "";
        foreach ($config['custom_form_validators'] as $cftv) {
            if (in_array($selectedType, $cftv["types"])) {
                if ($cftv['selected']) $selectedTypeOption = $cftv["value"];
                $cftvopts [] = array(
                    'value' => $cftv["value"],
                    'label' => $cftv["label"],
                );
            }
        }
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'custom_form_field_type_validator',
            'attributes' =>  array(
                'id' => 'custom_form_field_type_validator',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'options' => $cftvopts,
                'value' => $selectedTypeOption,
            ),
            'options' => array(
                'label' => 'Field type validator',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'custom_form_field_facebook_type',
            'attributes' =>  array(
                'id' => 'custom_form_field_facebook_type',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'options' => $config['facebook_fields'],
            ),
            'options' => array(
                'label' => 'Facebook related data',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'name' => 'custom_form_field_default_value',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'custom_form_field_default_value',
                'class' => 'form-control',
                'placeholder'=> 'Field default value',
            ),
            'options' => array(
                'label' => 'Field default value',
            ),
        ));

        $this->add(array(
            'name' => 'custom_form_field_description',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'custom_form_field_description',
                'class' => 'form-control',
                'placeholder'=> 'Field description',
            ),
            'options' => array(
                'label' => 'Field description *',
            ),
        ));

        $this->add(array(
            'name' => 'custom_form_field_tag_values',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'custom_form_field_tag_values',
                'class' => 'form-control',
                'placeholder'=> 'Type #something and hit enter - the tags must be of at least 4 letters!',
            ),
            'options' => array(
                'label' => 'Field tag values *',
            ),
        ));

        $this->add(array(
            'name' => 'custom_form_field_boxes_value',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'custom_form_field_boxes_value',
                'class' => 'form-control',
                'placeholder'=> 'Option value...',
            ),
            'options' => array(
                'label' => 'Option values *',
            ),
        ));

        $this->add(array(
            'name' => 'custom_form_field_upload-icon',
            'attributes' => array(
                'type' 	=> 'File',
                'id' 	=> 'custom_form_field_upload-icon',
                'class'	=> 'file-loading',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'custom_form_field_boxes_values_json',
            'attributes' => array(
                'id' 	=> 'custom_form_field_boxes_values_json',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'custom_form_field_select_type',
            'attributes' =>  array(
                'id' => 'custom_form_field_select_type',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'options' => array(
                    self::HP_FORM_SELECT_TYPE_LOCALE => self::HP_FORM_SELECT_TYPE_LOCALE_LABEL,
                    self::HP_FORM_SELECT_TYPE_REMOTE => self::HP_FORM_SELECT_TYPE_REMOTE_LABEL,
                ),
            ),
            'options' => array(
                'label' => 'Select origin data',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'custom_form_field_select_related',
            'attributes' =>  array(
                'id' => 'custom_form_field_select_related',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'options' => array(
                ),
            ),
            'options' => array(
                'empty_option' => self::HP_FORM_SELECT_RELATED_TO_NULL_LABEL,
                'label' => 'Select related to',
                'disable_inarray_validator' => true,
            ),
        ));
    }

    public function getView() {
        return 'form/section/custom_field';
    }

}