<?php

namespace Form\Section\CustomForm;

use Form\Section\SectionFilter;
use Zend\InputFilter\Factory as InputFactory;

class CustomFieldFilter extends SectionFilter {

    public function getInputFilter() {

        parent::getInputFilter();

        if ($this->inputFilter) {
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'custom_form_field_type_validator',
                'required' => false,
                'allow_empty' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'custom_form_field_type',
                'required' => false,
                'allow_empty' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'custom_form_field_required',
                'required' => false,
                'allow_empty' => true,
            )));


        }

        return $this->inputFilter;
    }

}