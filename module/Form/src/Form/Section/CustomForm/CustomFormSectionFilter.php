<?php

namespace Form\Section\CustomForm;

use Form\Section\SectionFilter;
use Zend\InputFilter\Factory as InputFactory;

class CustomFormSectionFilter extends SectionFilter
{

    public function getInputFilter()
    {

        parent::getInputFilter();

        if ($this->inputFilter) {
            $inputFilter = $this->inputFilter;
            $factory = new InputFactory();

            /* TYPE */
            $inputFilter->add($factory->createInput(array(
                'name' => 'links_validator',
//                'required' => false,
                'validators' => array(
                    array(
                        'name' => '\Form\Section\CustomForm\LinksValidator',
                    ),
                ),
            )));


            /* CONNECTOR */
            $inputFilter->add($factory->createInput(array(
                'name' => 'connector_validator',
                'validators' => array(
                    array(
                        'name' => '\Form\Section\ConnectorValidator',
                    ),
                ),
            )));

            /* JSTREE */
            $inputFilter->add($factory->createInput(array(
                'name' => 'custom_forms_details',
                'validators' => array(
                    array(
                        'name' => '\Form\Section\CustomForm\JsTreeValidator',
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'provider',
                'required' => false,
                'allow_empty' => true,
            )));
        }

        return $this->inputFilter;
    }

}