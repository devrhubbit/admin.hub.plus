<?php

namespace Form\Section\CustomForm;

use Zend\Validator\AbstractValidator;

class LinksValidator extends AbstractValidator {

    const IS_EMPTY_BOTH_URL    = 'ISEMPTYBOTHURL';
    const IS_EMPTY_PRIVACY_URL = 'ISEMPTYPRIVACYURL';
    const IS_EMPTY_TERMS_URL   = 'ISEMPTYTERMSURL';
    const IS_EMPTY_FORWARD_TO  = 'ISEMPTYFORWARDTO';
    const IS_EMPTY_FIELDS      = 'ISEMPTYFIELDS';
    const INVALID_BOTH_URL     = 'INVALIDBOTHURL';
    const INVALID_PRIVACY_URL  = 'INVALIDPRIVACYURL';
    const INVALID_TERMS_URL    = 'INVALIDTERMSURL';
    const INVALID_FORWARD_TO   = 'INVALIDFORWARDTO';

    protected $messageTemplates = array(
        self::IS_EMPTY_BOTH_URL    => 'Required Url value of privacy and terms links',
        self::IS_EMPTY_PRIVACY_URL => 'Required privacy Url value',
        self::IS_EMPTY_TERMS_URL   => 'Required terms & conditions Url value',
        self::IS_EMPTY_FORWARD_TO  => 'Required contact email',
        self::IS_EMPTY_FIELDS      => 'Required at least 1 field',

        self::INVALID_BOTH_URL     => 'Invalid Url value of both links',
        self::INVALID_PRIVACY_URL  => 'Invalid privacy Url value',
        self::INVALID_TERMS_URL    => 'Invalid terms & conditions Url value',
        self::INVALID_FORWARD_TO   => 'Invalid email',
    );

    public function __construct(array $options = array()) {
        parent::__construct($options);
    }

    public function isValid($value) {
        $result = false;

        $value = json_decode($value);
        switch ($value->choice) {
            case CustomFormSectionForm::REGISTRATION_FORM:
                if(self::urlsNotEmptyValidation($value)) {
                    if (self::valuesBaseUrlValidation($value)) {
                        if ($value->fields) {
                            $result = true;
                        } else {
                            $this->error(self::IS_EMPTY_FIELDS);
                        }
                    }
                }
                break;

            case CustomFormSectionForm::CONTACT_FORM:
                if(!self::notEmptyValidator($value->forward_to)) {
                    if (!self::valueEmailValidation($value->forward_to)) {
                        if ($value->fields) {
                            $result = true;
                        } else {
                            $this->error(self::IS_EMPTY_FIELDS);
                        }
                    } else {
                        $this->error(self::INVALID_FORWARD_TO);
                    }
                } else {
                    $this->error(self::IS_EMPTY_FORWARD_TO);
                }
                break;

            case CustomFormSectionForm::SURVEY_FORM:
            case CustomFormSectionForm::USER_FORM:
                if ($value->fields) {
                    $result = true;
                } else {
                    $this->error(self::IS_EMPTY_FIELDS);
                }
                break;
        }

        return $result;
    }

    private function urlsNotEmptyValidation($value) {
        $result = false;
        $privacyEmptyResult = self::notEmptyValidator($value->privacy);
        $termsEmptyResult   = self::notEmptyValidator($value->terms);
        $bothEmptyResult    = ($privacyEmptyResult && $termsEmptyResult);

        if ($bothEmptyResult) {
            $this->error(self::IS_EMPTY_BOTH_URL);
        } elseif ($privacyEmptyResult) {
            $this->error(self::IS_EMPTY_PRIVACY_URL);
        } elseif ($termsEmptyResult) {
            $this->error(self::IS_EMPTY_TERMS_URL);
        } else {
            $result = true;
        }

        return $result;
    }

    private function notEmptyValidator($value) {
        if ($value == "") {
            return true;
        } else {
            return false;
        }
    }

    private function valuesBaseUrlValidation($value) {
        $result = false;
        $privacyUrlResult = self::baseUrlValidator($value->privacy);
        $termsUrlResult   = self::baseUrlValidator($value->terms);
        $bothUrlResult    = ($privacyUrlResult && $termsUrlResult);

        if ($bothUrlResult) {
            $this->error(self::INVALID_BOTH_URL);
        } elseif ($privacyUrlResult) {
            $this->error(self::INVALID_PRIVACY_URL);
        } elseif ($termsUrlResult) {
            $this->error(self::INVALID_TERMS_URL);
        } else {
            $result = true;
        }

        return $result;
    }

    private function baseUrlValidator($value) {
        $baseurlValidated = preg_match('%^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@|\d{1,3}(?:\.\d{1,3}){3}|(?:(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)(?:\.(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)*(?:\.[a-z\x{00a1}-\x{ffff}]{2,6}))(?::\d+)?(?:[^\s]*)?$%iu', $value);
        if (!$baseurlValidated) {
            return true;
        } else {
            return false;
        }
    }

    private function valueEmailValidation($email) {
        $emailValidated = preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/', $email);
        if (!$emailValidated) {
            return true;
        } else {
            return false;
        }
    }
}