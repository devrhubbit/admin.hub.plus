<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 20/11/17
 * Time: 11:26
 */

namespace Form\Section\CustomForm;

use Application\Model\SectionModel;
use Form\LayoutForm;
use Zend\Session\Container;
use Zend\Validator\AbstractValidator;

class JsTreeValidator extends AbstractValidator
{
    const NO_SECTION_REMOTE_URL = 'NO_SECTION_REMOTE_URL';

    protected $messageTemplates = array(
        self::NO_SECTION_REMOTE_URL => '%s fields have the "origin data" option set to remote, then connector setting must be REMOTE',
    );

    /** @var string[] */
    private $messages = [];

    public function __construct(array $options = array())
    {
        parent::__construct($options);
    }

    public function isValid($value, $context = null)
    {
        $session = new Container('acl');
        $this->messages = [];
        $value = json_decode($value);
        $remoteFields = array();

        foreach ($value->fieldsets as $fieldSet) {
            foreach ($fieldSet->fields as $field) {
                if ($field->type === SectionModel::HPFORM_SELECT) {
                    $originData = $field->value->origin_data;

                    if ($originData === CustomFieldForm::HP_FORM_SELECT_TYPE_REMOTE && $context['connector'] !== LayoutForm::CONNECTOR_TYPE_REMOTE) {
                        $remoteFields[] = "'" . $field->label->{$session->currentLang} . "'";
                    }
                }
            }
        }

        if (count($remoteFields) > 0) {
            $result = false;
            $this->messages[self::NO_SECTION_REMOTE_URL] = sprintf($this->messageTemplates[self::NO_SECTION_REMOTE_URL], implode(", ", $remoteFields));
        } else {
            $result = true;
        }

        return $result;
    }

    public function getMessages()
    {
        return $this->messages;
    }
}