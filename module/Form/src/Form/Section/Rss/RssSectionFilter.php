<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 18:00
 */

namespace Form\Section\Rss;


use Form\Section\SectionFilter;
use Zend\InputFilter\Factory as InputFactory;

class RssSectionFilter extends SectionFilter
{
    public function getInputFilter()
    {
        parent::getInputFilter();

        if ($this->inputFilter) {
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'resource',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'   => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                            ),
                        ),
                        'break_chain_on_failure' => true,
                    ),
                    array(
                        'name'    => 'Regex',
                        'options' => array(
                            'pattern' => '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                            'message' => "Invalid URL value",
                        )
                    )
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'cell-type',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'   => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Choose your cell layout"
                            ),
                        ),
                        'break_chain_on_failure' => true,
                    ),
                ),
            )));
        }

        return $this->inputFilter;
    }
}