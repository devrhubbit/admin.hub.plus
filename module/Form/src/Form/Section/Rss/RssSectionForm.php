<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 17:57
 */

namespace Form\Section\Rss;

use Form\LayoutForm;
use Form\Section\SectionForm;

class RssSectionForm extends SectionForm
{
    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::RSS);

        $this->add(array(
            'name' => 'resource',
            'attributes' => array(
                'type' 	=> 'Zend\Form\Element\Url',
                'id' 	=> 'resource',
                'class' => 'form-control',
                'placeholder'=> 'http://www.your-domain.com/feed',
            ),
            'options' => array(
                'label' => 'Resource*',
                'addon' => 'feed url',
            ),
        ));

        $this->add(array(
            'name' => 'custom_css',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' 	=> 'custom_css',
                'class' => 'form-control hidden',
            ),
            'options' => array(
                'label' => 'Custom CSS',
            ),
        ));

        $this->add(array(
            'name' => 'custom_js',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' 	=> 'custom_js',
                'class' => 'form-control hidden',
            ),
            'options' => array(
                'label' => 'Custom JavaScript',
            ),
        ));

        $this->add(array(
            'name' => 'tags',
            'attributes' => array(
                'type' 	      => 'Text',
                'id' 	      => 'tags',
                'class'       => 'form-control',
                'placeholder' => 'type something and hit enter',
            ),
            'options' => array(
                'label' => 'TAGS <span class="tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->addLayoutBlock(array('filter', 'like', 'autoplay', 'streaming', 'connector'), LayoutForm::RSS);
    }

    public function getView() {
        return 'form/section/rss';
    }
}