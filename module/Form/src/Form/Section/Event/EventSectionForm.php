<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 17:57
 */

namespace Form\Section\Event;

use Form\LayoutForm;
use Form\Section\SectionForm;

class EventSectionForm extends SectionForm
{

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::EVENT);

        $this->addHiddenField("date-validator", "check");

        $this->add(array(
            'name' => 'start-date',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'start-date',
                'class' => 'form-control',
                'placeholder' => 'Insert start filter date',
            ),
            'options' => array(
                'label' => 'From date',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'start-current-date',
            'options' => array(
                'label' => 'From app date',
            ),
            'attributes' => array(
                'id' => 'start-current-date',
                'class' => 'switch-btn',
            )
        ));

        $this->add(array(
            'name' => 'end-date',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'end-date',
                'class' => 'form-control',
                'placeholder' => 'Insert end filter date',
            ),
            'options' => array(
                'label' => 'To date',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'end-current-date',
            'options' => array(
                'label' => 'To app date',
            ),
            'attributes' => array(
                'id' => 'end-current-date',
                'class' => 'switch-btn',
            )
        ));

        $this->add(array(
            'name' => 'tags',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'tags',
                'class' => 'form-control',
                'placeholder' => 'type #something and hit enter',
            ),
            'options' => array(
                'label' => 'TAGS <span class="tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'order',
            'attributes' => array(
                'id' => 'order',
                'title' => 'Select how to order content',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => array('ASC'=>'Ascendent', 'DESC'=>'Descendent'),
            ),
            'options' => array(
                'label' => 'Order*',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->addLayoutBlock(array('paged', 'open_in_app', 'autoplay', 'streaming'), LayoutForm::EVENT);
    }

    public function getView()
    {
        return 'form/section/event';
    }
}