<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 14/12/17
 * Time: 10:45
 */

namespace Form\Section\Event;

use Zend\Validator\AbstractValidator;

class DateRangeValidator extends AbstractValidator
{
    const INVALID_RANGE_DATE = 'INVALID_RANGE_DATE';

    protected $messageTemplates = array(
        self::INVALID_RANGE_DATE => '"From date" must be earlier than "To date".',
    );

    /** @var string[] */
    private $messages = [];

    public function __construct(array $options = array())
    {
        parent::__construct($options);
    }

    public function isValid($value, $context = null)
    {
        $this->messages = [];

        $from = 0;
//        if (isset($context['start-current-date']) && $context['start-current-date'] === "1") {
//            $from = gmmktime('0', '0', '0', date("n"), date("j"), date("Y"));
//        } else
        if (isset($context['start-date'])) {
            $context['start-date'] = $context['start-date'] . ":00";
            $from = (int)strtotime($context['start-date']);
        }

        $to = strtotime('01-01-2037');
//        if (isset($context['end-current-date']) && $context['end-current-date'] === "1") {
//            $to = gmmktime('23', '59', '59', date("n"), date("j"), date("Y"));
//        } else
        if (isset($context['end-date'])) {
            $context['end-date'] = $context['end-date'] . ":00";
            $to = (int)strtotime($context['end-date']);
        }

        if ($from > $to) {
            $this->messages[self::INVALID_RANGE_DATE] = $this->messageTemplates[self::INVALID_RANGE_DATE];
            $result = false;
        } else {
            $result = true;
        }

        return $result;
    }

    public function getMessages()
    {
        return $this->messages;
    }
}