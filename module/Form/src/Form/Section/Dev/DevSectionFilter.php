<?php

namespace Form\Section\Dev;

use Form\Section\SectionFilter;
use Zend\InputFilter\Factory as InputFactory;

class DevSectionFilter extends SectionFilter {

    public function getInputFilter() {

        parent::getInputFilter();

        if ($this->inputFilter) {
            /** @var DevSectionFilter $inputFilter */
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'dev_section_uri',
                'required' => true,
                'allow_empty' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'dev_section_ios',
                'required' => true,
                'allow_empty' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'dev_section_android',
                'required' => true,
                'allow_empty' => false,
            )));

        }

        return $this->inputFilter;
    }

}