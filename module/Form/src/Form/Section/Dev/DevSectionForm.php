<?php

namespace Form\Section\Dev;

use Form\Section\SectionForm;

class DevSectionForm extends SectionForm
{
    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::DEV);

        $this->addHiddenField("section_dev_extra_params");

        $this->add(array(
            'name' => 'dev_section_uri',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'dev_section_uri',
                'class' => 'form-control',
                'placeholder' => 'Type your section URI',
            ),
            'options' => array(
                'label' => 'URI',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'name' => 'dev_section_ios',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'dev_section_ios',
                'class' => 'form-control',
                'placeholder' => 'Type your iOS controller name',
            ),
            'options' => array(
                'label' => 'iOS',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'name' => 'dev_section_android',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'dev_section_android',
                'class' => 'form-control',
                'placeholder' => 'Type your Android activity name',
            ),
            'options' => array(
                'label' => 'Android',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'name' => 'dev_section_param_key',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'dev_section_param_key',
                'class' => 'form-control',
                'placeholder' => 'Key...',
            ),
            'options' => array(
                'label' => 'Extra params',
            ),
        ));

        $this->add(array(
            'name' => 'dev_section_param_value',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'dev_section_param_value',
                'class' => 'form-control',
                'placeholder' => 'Value...',
            ),
            'options' => array(
                'label' => 'Value',
            ),
        ));

        $this->add(array(
            'name' => 'dev_section_param_add_param',
            'type' => 'Button',
            'options' => array(
                'label' => '<i class="fa fa-plus" aria-hidden="true"></i>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
            'attributes' => array(
                'id' => 'dev_section_param_add_param',
                'class' => 'btn btn-primary btn-block btn-flat',
            ),
        ));

        $this->addLayoutBlock(array('paged', 'open_in_app', 'autoplay', 'streaming', 'share', 'like', 'connector', 'filter', 'cell-type', 'page_size'), SectionForm::DEV);
    }

    public function getView()
    {
        return 'form/section/dev';
    }
}