<?php

namespace Form\Section;

use Zend\Validator\AbstractValidator;

class ConnectorValidator extends AbstractValidator {

    const IS_EMPTY_PROVIDER = 'ISEMPTYPROVIDER';
    const IS_EMPTY_URL      = 'ISEMPTYURL';
    const INVALID_URL       = 'INVALIDURL';

    protected $messageTemplates = array(
        self::IS_EMPTY_PROVIDER => 'Required Provider',
        self::IS_EMPTY_URL      => 'Required Url value',
        self::INVALID_URL       => 'Invalid Url value',
    );

    public function __construct(array $options = array()) {
        parent::__construct($options);
    }

    public function isValid($value) {

        $value = json_decode($value);
        switch ($value->choice) {
            case \Form\LayoutForm::CONNECTOR_TYPE_HUB:
                $result = true;
                break;
            case \Form\LayoutForm::CONNECTOR_TYPE_REMOTE:
                $providerResult = self::providerValidator($value);
                if ($providerResult) {
                    $urlResult = self::baseUrlValidator($value);
                } else {
                    $urlResult = false;
                }
                $result = ($providerResult && $urlResult);
                break;
            default:
                $result = false;
        }

        return $result;
    }

    private function providerValidator($value) {
        if ($value->provider == "") {
            $this->error(self::IS_EMPTY_PROVIDER);
            return false;
        } else {
            return true;
        }
    }

    private function baseUrlValidator($value) {
        if ($value->baseurl == "") {
            $this->error(self::IS_EMPTY_URL);
            return false;
        } else {
            if (filter_var($value->baseurl, FILTER_VALIDATE_URL)) {
                return true;
            } else {
                $this->error(self::INVALID_URL);
                return false;
            }
        }
    }

}