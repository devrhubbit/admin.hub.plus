<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 17:57
 */

namespace Form\Section\Person;

use Form\LayoutForm;
use Form\Section\SectionForm;

class PersonSectionForm extends SectionForm
{

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::PERSON);

        $this->add(array(
            'name' => 'tags',
            'attributes' => array(
                'type' 	      => 'Text',
                'id' 	      => 'tags',
                'class'       => 'form-control',
                'placeholder' => 'type something and hit enter',
            ),
            'options' => array(
                'label' => 'TAGS <span class="tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'order',
            'attributes' => array(
                'id' => 'order',
                'title' => 'Select how to order content',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => array('ASC'=>'Ascendent', 'DESC'=>'Descendent'),
            ),
            'options' => array(
                'label' => 'Order*',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->addLayoutBlock(array('paged', 'open_in_app', 'autoplay', 'streaming'), LayoutForm::PERSON);
    }

    public function getView() {
        return 'form/section/person';
    }
}