<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 27/06/16
 * Time: 16:50
 */

namespace Form\Section\Register;

use Zend\Validator\AbstractValidator;

class RegisterTagValidator extends AbstractValidator {

    const TAGS  = 'tags';
    const LABEL = 'label';

    protected $messageTemplates = array(
        self::LABEL => "Tags groups labals required",
        self::TAGS  => "Use almost two tag per group",
    );

    public function isValid($value) {

        $this->setValue($value);

        $jsonList = json_decode($value);

        foreach($jsonList as $jsonObj) {
            if($jsonObj['key'] == "") {
                $this->error(self::LABEL);
                return false;
            }
            if(count($jsonObj["values"]) < 2) {
                $this->error(self::TAGS);
                return false;
            }
        }

        if (!is_float($value)) {
            $this->error(self::FLOAT);
            return false;
        }

        return true;
    }
}