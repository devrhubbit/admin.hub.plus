<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 17:57
 */

namespace Form\Section\Register;

use Form\LayoutForm;
use Form\Section\SectionForm;

class RegisterSectionForm extends SectionForm
{

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, SectionForm::REGISTER);

        $this->addHiddenField("tags");


        $this->add(array(
            'name' => 'privacy_link',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'privacy_link',
                'class' => 'form-control',
                'placeholder' => "Link to your Privacy page",
                'data-width' => '100%',
            ),
            'options' => array(
                'label' => 'Privacy link *',
            ),
        ));

        $this->add(array(
            'name' => 'terms_link',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'terms_link',
                'class' => 'form-control',
                'placeholder' => "Link to your Terms & Conditions page",
                'data-width' => '100%',
            ),
            'options' => array(
                'label' => 'Terms & Conditions link *',
            ),
        ));

        $this->addLayoutBlock(array('auth', 'filter', 'share', 'like', 'paged', 'open_in_app', 'autoplay', 'streaming', 'connector'), LayoutForm::REGISTER);

    }

    public function getView() {
        return 'form/section/register';
    }
}