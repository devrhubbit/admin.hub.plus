<?php

namespace Form\Section\Generic;

use Form\Section\SectionFilter;
use Zend\InputFilter\Factory as InputFactory;

class GenericSectionFilter extends SectionFilter {

    public function getInputFilter() {

        parent::getInputFilter();

        if ($this->inputFilter) {
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'order',
                'required' => true,
                'allow_empty' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'connector_validator',
                'validators' => array(
                    array(
                        'name' => '\Form\Section\ConnectorValidator',
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'provider',
                'required' => false,
                'allow_empty' => true,
            )));

        }

        return $this->inputFilter;
    }

}