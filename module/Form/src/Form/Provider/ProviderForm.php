<?php
namespace Form\Provider;

use Form\HpForm;

class ProviderForm extends HpForm
{
    protected $serviceLocator;

    public function __construct($serviceLocator)
    {
        parent::__construct('provider');

        $this->serviceLocator = $serviceLocator;

        $this->setAttribute('method', 'POST');
        $this->setAttribute('enctype','multipart/form-data');

        $this->addHiddenField("provider_id");

        $this->add(array(
            'name' => 'icon',
            'attributes' => array(
                'type' 	=> 'Hidden',
                'id' 	=> 'icon',
            ),
        ));

        $this->add(array(
            'name' => 'upload-icon',
            'attributes' => array(
                'type' 	=> 'File',
                'id' 	=> 'upload-icon',
                'class'	=> 'file-loading',
            ),
        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'Text',
                'id'   => 'name',
                'class' => 'form-control',
                'placeholder'=> "Name",
            ),
            'options' => array(
                'label' => 'Name',
            ),
        ));

        $this->add(array(
            'name' => 'site',
            'attributes' => array(
                'type' => 'Text',
                'id'   => 'site',
                'class' => 'form-control',
                'placeholder'=> 'Site',
            ),
            'options' => array(
                'label' => 'Site',
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'email',
                'class' => 'form-control',
                'placeholder'=> 'Email',
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));

        $this->add(array(
            'name' => 'save',
            'type' => 'Button',
            'attributes' => array(
                'id' 	=> 'save',
                'value' => 'ADD PROVIDER',
                'class'	=> 'btn btn-primary btn-block btn-flat',
                'disabled' => true,
            ),
            'options' => array(
                'label' => 'ADD PROVIDER',
            ),
        ));

        $this->add(array(
            'name' => 'update',
            'type' => 'Button',
            'attributes' => array(
                'id' 	=> 'update',
                'value' => 'UPDATE PROVIDER',
                'class'	=> 'btn btn-primary btn-block btn-flat hidden',
                'disabled' => true,
            ),
            'options' => array(
                'label' => 'UPDATE PROVIDER',
            ),
        ));

        $this->add(array(
            'name' => 'reset',
            'type' => 'Button',
            'attributes' => array(
                'id' 	=> 'reset',
                'value' => 'RESET',
                'class'	=> 'btn btn-default btn-block btn-flat hidden',
                'style' => 'min-width: 65px;'
            ),
            'options' => array(
                'label' => 'RESET',
            ),
        ));

    }

    public function getView() {
        return 'form/provider';
    }

}