<?php
namespace Form\Provider;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Factory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {

        $form = new ProviderForm($serviceLocator);
        $Translator = $serviceLocator->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($Translator);

        $filter = new ProviderFilter();
        $form->setInputFilter($filter->getInputFilter());

        return $form;

    }
}