<?php
namespace Form\Registration;

// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class RegistrationFilter implements InputFilterAwareInterface
{
    protected $serviceLocator;
    protected $inputFilter;                       // <-- Add this variable
    protected $controllerName;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $routeMatch = $this->serviceLocator->get('Application')->getMvcEvent()->getRouteMatch();
        $this->controllerName = $routeMatch->getParam('controller');
    }

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

	public function getInputFilter()
    {
    	if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
            	'name'     => 'email',
            	'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'   => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                            ),
                        ),
                        'break_chain_on_failure' => true,
                    ),
                    array(
                        'name'   => 'EmailAddress',
                        'options' => array(
                            'message' => 'Invalid email address'
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
            	'name'     => 'password',
            	'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'   => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                            ),
                        ),
                        'break_chain_on_failure' => true
                    ),
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'min' => 8,
                        ),
                    ),
                ),
            )));

            if ($this->controllerName !== 'Application\Controller\Superadmin') {
                $inputFilter->add($factory->createInput(array(
                    'name'     => 'privacy',
                    'required' => true,
                    'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'Identical',
                            'options' => array(
                                'token' => 'yes',
                                'messages' => array(
                                    \Zend\Validator\Identical::NOT_SAME => 'Agree to proceed',
                                ),
                            ),
                        ),
                    ),
                )));
            }

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}