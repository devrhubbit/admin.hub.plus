<?php

namespace Form\Registration;

use Form\HpForm;
use Zend\ServiceManager\ServiceLocatorInterface;

class RegistrationForm extends HpForm
{
    protected $serviceLocator;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        parent::__construct('registration');

        $this->serviceLocator = $serviceLocator;

        $routeMatch = $this->serviceLocator->get('Application')->getMvcEvent()->getRouteMatch();
        $controllerName = $routeMatch->getParam('controller');

        $this->add(array(
            'name' => 'first-name',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'first-name',
                'class' => 'form-control',
                'placeholder' => 'First name',
            ),
            'options' => array(
                'label' => 'First name',
            ),
        ));

        $this->add(array(
            'name' => 'last-name',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'last-name',
                'class' => 'form-control',
                'placeholder' => 'Last name',
            ),
            'options' => array(
                'label' => 'Last name',
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'email',
                'autocomplete' => 'off',
                'autocorrect' => 'off',
                'autocapitalize' => 'off',
                'class' => 'form-control',
                'placeholder' => 'Email*',
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));

        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'password',
                'autocomplete' => 'nope',
                'autocorrect' => 'off',
                'autocapitalize' => 'off',
                'class' => 'form-control',
                'placeholder' => 'Password*',
            ),
            'options' => array(
                'label' => 'Password:',
            ),
        ));
//
//		$this->add(array(
//			'name' => 'privacy',
//			'type' => 'Checkbox',
//			'attributes' => array(
//				'type' 	=> 'Checkbox',
//				'id' 	=> 'privacy',
//			),
//			'options' => array(
//				'label' => 'I agree and submit the HUB+ <a href="/privacy" target="_blank">privacy</a> and <a href="/terms-condition" target="_blank">terms & condition</a> policy',
//				'use_hidden_element' => false,
//			),
//		));

        if ($controllerName !== 'Application\Controller\Superadmin') {
            $this->add(array(
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => 'privacy',
                'options' => array(
                    'label' => 'I agree the <a href="/privacy" target="_blank">privacy</a> and <a href="/terms-condition" target="_blank">terms & condition</a> policy',
                    'use_hidden_element' => true,
                    'checked_value' => 'yes',
                    'unchecked_value' => 'no'
                ),
                'attributes' => array(
                    'value' => 'no',
                    'id' => 'privacy',
                )
            ));
        }

        $this->add(array(
            'name' => 'register',
            'type' => 'Submit',
            'attributes' => array(
                'id' => 'register',
                'value' => 'REGISTER',
                'class' => 'btn btn-primary btn-block btn-flat',
            ),
        ));
    }

    public function getView()
    {
        return 'form/registration';
    }

}


