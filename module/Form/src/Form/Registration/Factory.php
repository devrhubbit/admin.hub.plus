<?php

namespace Form\Registration;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

class Factory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $form = new RegistrationForm($serviceLocator);
        $Translator = $serviceLocator->get('translator');
        AbstractValidator::setDefaultTranslator($Translator);

        $filter = new RegistrationFilter($serviceLocator);
        $form->setInputFilter($filter->getInputFilter());

        return $form;

    }
}