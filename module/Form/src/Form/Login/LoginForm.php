<?php
namespace Form\Login;

use Form\HpForm;


class LoginForm extends HpForm
{
	protected $serviceLocator;

	public function __construct($serviceLocator)
	{
		parent::__construct('login');

		$this->serviceLocator = $serviceLocator;
		
		
		$this->add(array(
			'name' => 'email',
			'attributes' => array(
				'type' 	=> 'Text',
				'id' 	=> 'email',
				'autofocus' => true,
				'class' => 'form-control', 
				'placeholder'=> 'Email',
			),
			'options' => array(
				'label' => 'Email',
			),
		));
		
		$this->add(array(
			'name' => 'password',
			'attributes' => array(
				'type' 	=> 'Password',
				'id' 	=> 'password',
				'class' => 'form-control',
				'placeholder'=> 'Password',
			),
			'options' => array(
				'label' => 'Password:',
			),
		));

		$this->add(array(
			'name' => 'remember-me',
			'attributes' => array(
				'type' 	=> 'Checkbox',
				'id' 	=> 'remember-me',
			),
			'options' => array(
				'label' => 'Remember Me',
			),
		));
		
		$this->add(array(
			'name' => 'sign-in',
			'type' => 'Submit',
			'attributes' => array(
				'id' 	=> 'sign-in',
				'value' => 'Sign In',
				'class'	=> 'btn btn-primary btn-block btn-flat',
			),
		));
		
	}

    public function getView()
    {
        return 'form/login';
    }

}


