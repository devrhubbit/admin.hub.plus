<?php 
namespace Form\Login;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Factory implements FactoryInterface
{
	
	public function createService(ServiceLocatorInterface $serviceLocator) {
		
		$form = new LoginForm($serviceLocator);
		$Translator = $serviceLocator->get('translator');		
		\Zend\Validator\AbstractValidator::setDefaultTranslator($Translator);
		
		$filter = new LoginFilter();
		$form->setInputFilter($filter->getInputFilter());
		
		return $form;
		
	}
}