<?php

namespace Form\CustomForm;

use Application\Model\SectionModel;
use Form\HpForm;
use Form\Section\CustomForm\CustomFieldForm;
use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\InputFilter\Factory as InputFactory;

class CustomForm extends HpForm
{
    const SINGLE_IMAGE_VALIDATOR = "SINGLE";
    const GALLERY_IMAGE_VALIDATOR = "GALLERY";
    const NO_TEXT_VALIDATOT = "NONE";
    const EMAIL_TEXT_VALIDATOR = "VALID_EMAIL";
    const URL_TEXT_VALIDATOR = "VALID_URL";
    const PHONE_TEXT_VALIDATOR = "VALID_PHONE";
    const NUMERIC_TEXT_VALIDATOR = "NUMERIC";
    const SINGLE_TAG_VALIDATOR = "TAG_SINGLE";
    const MULTIPLE_TAG_VALIDATOR = "TAG_MULTIPLE";

    protected $fieldsType = array();

    protected $custom_fieldsets;
    protected $serviceLocator;
    protected $inputFilter;
    protected $inputFactory;

    public function __construct(ServiceLocatorInterface $serviceLocator, $fieldsets)
    {
        parent::__construct('custom_form');

        $this->serviceLocator = $serviceLocator;
        $this->custom_fieldsets = $fieldsets;

        $this->inputFilter = new InputFilter();
        $this->inputFactory = new InputFactory();

        foreach ($fieldsets as $fieldset) {
            foreach ($fieldset->fields as $field) {
                switch ($field->type) {
                    //case SectionModel::HPFORM_INPUT_DESCR://Nothing to do with description block
                    case SectionModel::HPFORM_INPUT_TEXT:   $this->appendInputText($field); break;
                    case SectionModel::HPFORM_TEXTAREA:     $this->appendTextarea($field); break;
                    //case SectionModel::HPFORM_IMAGE:      //TODO: implemnts that fields in edit form
                    case SectionModel::HPFORM_CHECKBOX:     $this->appendCheckbox($field); break;
                    case SectionModel::HPFORM_RADIOBOX:     $this->appendRadiobox($field); break;
                    case SectionModel::HPFORM_SELECT:       $this->appendSelect($field); break;
                    //case SectionModel::HPFORM_ADDRESS:    //TODO: implemnts that fields in edit form
                    case SectionModel::HPFORM_TAG_GROUP:    $this->appendTagGroup($field); break;
                }
                $this->fieldsType[base64_encode($field->name)] = $field->type;
            }
        }

        $this->add(array(
            'name' => 'save',
            'type' => 'Button',
            'options' => array(
                'label' => 'SAVE DATA',
            ),
            'attributes' => array(
                'id' => 'save',
                'value' => 'SAVE DATA',
                'type' => 'submit',
                'class' => 'btn btn-success btn-block btn-flat',
            ),
        ));

        $this->setInputFilter($this->inputFilter);
    }

    public function getCustomFieldsets()
    {
        return $this->custom_fieldsets;
    }

    public function setData($data) {
        $parsedData = array();

        foreach ($data as $key => $value) {
            $type = isset($this->fieldsType[$key]) ? $this->fieldsType[$key] : "";

            switch ($type) {
                case SectionModel::HPFORM_CHECKBOX:
                    $parsedData[$key] = is_array($value) ? $value : explode(",", $value);
                    break;
                default:
                    $parsedData[$key] = $value;
            }
        }

        parent::setData($parsedData);
    }

    private function appendInputText(\stdClass $field) {
        $this->add(array(
            'name' => base64_encode($field->name),
            'type' => 'Zend\Form\Element\Text',
            'attributes' => array(
                'type' => 'Text',
                'id' => base64_encode($field->name),
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => $field->name . ($field->required ? "*" : ""),
            ),
        ));

        $validators = array();
        if($field->required) {
            $validators []= array(
                'name'   => 'NotEmpty',
                'options' => array(
                    'messages' => array(
                        \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                    ),
                ),
                'break_chain_on_failure' => true,
            );
        }

        switch ($field->validator) {
            case self::EMAIL_TEXT_VALIDATOR:
                $validators []= array(
                    'name'   => 'EmailAddress',
                    'options' => array(
                        'message' => 'Invalid email address'
                    ),
                );
                break;
            case self::URL_TEXT_VALIDATOR:
                $validators []= array(
                    'name'    => 'Regex',
                    'options' => array(
                        'pattern' => '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                        'message' => "Invalid URL value",
                    )
                );
                break;
            case self::PHONE_TEXT_VALIDATOR:
                $validators []= array(
                    'name'    => 'Regex',
                    'options' => array(
                        'pattern' => '/^\s*(?:\+?(\d{1,4}))?([-. (]*(\d{1,3})[-. )]*)?((\d{2,4})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/',
                        'message' => "Invalid phone number",
                    )
                );
                break;
            case self::NUMERIC_TEXT_VALIDATOR:
                $validators []= array(
                    'name'    => 'Digits',
                    'options' => array(
                        'message' => 'Invalid number value'
                    ),
                );
                break;
        }

        $this->inputFilter->add($this->inputFactory->createInput(array(
            'name'     => base64_encode($field->name),
            'required' => $field->required,
            'validators' => $validators,
        )));
    }

    private function appendTextarea(\stdClass $field) {
        $this->add(array(
            'name' => base64_encode($field->name),
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => base64_encode($field->name),
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => $field->name . ($field->required ? "*" : ""),
            ),
        ));

        $validators = array();
        if($field->required) {
            $validators []= array(
                'name'   => 'NotEmpty',
                'options' => array(
                    'messages' => array(
                        \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                    ),
                ),
                'break_chain_on_failure' => true,
            );
        }

        $this->inputFilter->add($this->inputFactory->createInput(array(
            'name'     => base64_encode($field->name),
            'required' => $field->required,
            'validators' => $validators,
        )));
    }

    private function appendCheckbox(\stdClass $field) {
        $options = array();
        $values = (array) $field->value;
        foreach ($values as $key => $value) {
            $options[$key] = $key;
        }

        $this->add(array(
            'name' => base64_encode($field->name),
            'type' => 'Zend\Form\Element\MultiCheckbox',
            'attributes' => array(
                'id' => base64_encode($field->name),
            ),
            'options' => array(
                'label' => $field->name . ($field->required ? "*" : ""),
                'value_options' => $options,
                'disable_inarray_validator' => true,
            ),
        ));

        $validators = array();
        if($field->required) {
            $validators []= array(
                'name'   => 'NotEmpty',
                'options' => array(
                    'messages' => array(
                        \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                    ),
                ),
                'break_chain_on_failure' => true,
            );
        }

        $this->inputFilter->add($this->inputFactory->createInput(array(
            'name'     => base64_encode($field->name),
            'required' => $field->required,
            'validators' => $validators,
        )));
    }

    private function appendRadiobox(\stdClass $field) {
        $options = array();
        $values = (array) $field->value;
        foreach ($values as $key => $value) {
            $options[$key] = $key;
        }

        $this->add(array(
            'name' => base64_encode($field->name),
            'type' => 'Zend\Form\Element\Radio',
            'attributes' => array(
                'id' => base64_encode($field->name),
            ),
            'options' => array(
                'label' => $field->name . ($field->required ? "*" : ""),
                'value_options' => $options,
                'disable_inarray_validator' => true,
            ),
        ));

        $validators = array();
        if($field->required) {
            $validators []= array(
                'name'   => 'NotEmpty',
                'options' => array(
                    'messages' => array(
                        \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                    ),
                ),
                'break_chain_on_failure' => true,
            );
        }

        $this->inputFilter->add($this->inputFactory->createInput(array(
            'name'     => base64_encode($field->name),
            'required' => $field->required,
            'validators' => $validators,
        )));

    }

    private function appendSelect(\stdClass $field) {
        $options = array();
        $values = (array) $field->value->option_values;
        foreach ($values as $key => $value) {
            $options[$key] = $key;
        }

        $this->add(array(
            'name' => base64_encode($field->name),
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => base64_encode($field->name),
                'title' => 'Select something ...',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => $options,
            ),
            'options' => array(
                'label' => $field->name . ($field->required ? "*" : ""),
                //'value_options' => $options,
                'disable_inarray_validator' => true,
            ),
        ));

        $validators = array();
        if($field->required) {
            $validators []= array(
                'name'   => 'NotEmpty',
                'options' => array(
                    'messages' => array(
                        \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                    ),
                ),
                'break_chain_on_failure' => true,
            );
        }

        $this->inputFilter->add($this->inputFactory->createInput(array(
            'name'     => base64_encode($field->name),
            'required' => $field->required,
            'validators' => $validators,
        )));
    }

    private function appendTagGroup(\stdClass $field) {
        $source = array();
        $values = (array) $field->value;
        foreach ($values as $key => $value) {
            $source []= $key;
        }

        $this->add(array(
            'name' => base64_encode($field->name),
            'attributes' => array(
                'type' => 'Text',
                'id' => base64_encode($field->name),
                'class' => 'form-control',
                'placeholder' => 'type something and hit enter',
                'source' => $source,
                'single' => ($field->validator === self::SINGLE_TAG_VALIDATOR ? "true" : "false"),
            ),
            'options' => array(
                'label' => $field->name . ($field->required ? "*" : "") . " <span class=\"tag_error text-danger\"></span>",
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $validators = array();
        if($field->required) {
            $validators []= array(
                'name'   => 'NotEmpty',
                'options' => array(
                    'messages' => array(
                        \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                    ),
                ),
                'break_chain_on_failure' => true,
            );
        }

        $this->inputFilter->add($this->inputFactory->createInput(array(
            'name'     => base64_encode($field->name),
            'required' => $field->required,
            'validators' => $validators,
        )));
    }

    public function getView()
    {
        return 'form/custom';
    }

}


