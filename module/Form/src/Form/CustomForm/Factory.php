<?php

namespace Form\CustomForm;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\MutableCreationOptionsInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

class Factory implements FactoryInterface, MutableCreationOptionsInterface
{
    /** @var  ServiceLocatorInterface */
    protected $serviceLocator;
    protected $options;

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return $this|mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    /**
     * Set creation options
     *
     * @param  array $options
     * @return void
     */
    public function setCreationOptions(array $options)
    {
        $this->options = $options;
    }

    public function getForm() {
        $form = new CustomForm($this->serviceLocator, $this->options);

        $Translator = $this->serviceLocator->get('translator');
        AbstractValidator::setDefaultTranslator($Translator);

        return $form;
    }
}