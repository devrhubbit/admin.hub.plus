<?php
namespace Form\Template;

use Form\HpForm;
use Form\LayoutForm;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\Model\TemplateModel;

class TemplateForm extends HpForm{

    protected $serviceLocator;

    public function __construct($serviceLocator) {
        parent::__construct('template');

        $this->serviceLocator = $serviceLocator;
        $config = $this->serviceLocator->get('config');

        $this->setAttribute('method', 'POST');
        $this->setAttribute('enctype','multipart/form-data');

        $this->addHiddenField("template-id");
        $this->addHiddenField("icon");


        /* GENERAL */
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'title',
                'class' => 'form-control',
                'placeholder'=> 'Template title*',
                'style' => 'width: 200px;',
            ),
            'options' => array(
                'label' => 'Template title',
            ),
        ));

        $this->add(array(
            'name' => 'upload-icon',
            'attributes' => array(
                'type' 	=> 'File',
                'id' 	=> 'upload-icon',
                'class'	=> 'file-loading',
            ),
        ));
        /* .../GENERAL */


        /* EXTRA */
        $this->add(array(
            'name' => 'page_size',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'page_size',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Page size',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mode',
            'attributes' => array(
                'id' => 'mode',
                'title' => 'Select your login mode*',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '84%',
                'options' => [
                    LayoutForm::AUTH_MODE_OPTIONAL  => LayoutForm::AUTH_MODE_OPTIONAL,
                    LayoutForm::AUTH_MODE_POPUP     => LayoutForm::AUTH_MODE_POPUP,
                    LayoutForm::AUTH_MODE_MANDATORY => LayoutForm::AUTH_MODE_MANDATORY,
                ],
                'value' => LayoutForm::AUTH_MODE_POPUP,
            ),
            'options' => array(
                'label' => 'LOGIN MODE*',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array( //default value for now
            'name' => 'login',
            'attributes' => array(
                'type' 	=> 'Hidden',
                'id' 	=> 'login',
                'value' => 'SIMPLE',
            ),
        ));

        $this->add(array( //default value for now
            'name' => 'sort',
            'attributes' => array(
                'type' 	=> 'Hidden',
                'id' 	=> 'sort',
                'value' => 0,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'filter',
            'options' => array(
                'label' => 'SEARCH box',
            ),
            'attributes' => array(
                'id' => 'filter',
                'class' => 'switch-btn',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'share',
            'options' => array(
                'label' => 'SHARE button',
            ),
            'attributes' => array(
                'id' => 'share',
                'class' => 'switch-btn',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'like',
            'options' => array(
                'label' => 'LIKE button',
            ),
            'attributes' => array(
                'id' => 'like',
                'class' => 'switch-btn',
            )
        ));
        /* .../EXTRA */


        /* FONTS */
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'primary_font',
            'attributes' => array(
                'id' => 'primary_font',
                'class' => 'selectpicker',
                'options' => $config['font_list'],
                'data-width' => '100%',
            ),
            'options' => array(
                'label' => 'Primary font',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'secondary_font',
            'attributes' => array(
                'id' => 'secondary_font',
                'class' => 'selectpicker',
                'options' => $config['font_list'],
                'data-width' => '100%',
            ),
            'options' => array(
                'label' => 'Secondary font',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));
        /* .../FONTS */


        /* COLORS */
        $this->add(array(
            'name' => 'global_bg',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'global_bg',
                'class' => 'form-control',
                'value' => '#ffffff'
            ),
            'options' => array(
                'label' => 'Main color',
            ),
        ));

        $this->add(array(
            'name' => 'accent_bg',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'accent_bg',
                'class' => 'form-control',
                'value' => '#ffffff'
            ),
            'options' => array(
                'label' => 'Accent color',
            ),
        ));

        $this->add(array(
            'name' => 'primary_bg',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'primary_bg',
                'class' => 'form-control',
                'value' => '#ffffff'
            ),
            'options' => array(
                'label' => 'Primary color',
            ),
        ));

        $this->add(array(
            'name' => 'secondary_bg',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'secondary_bg',
                'class' => 'form-control',
                'value' => '#ffffff'
            ),
            'options' => array(
                'label' => 'Secondary color',
            ),
        ));

        $this->add(array(
            'name' => 'light_txt',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'light_txt',
                'class' => 'form-control',
                'value' => '#ffffff'
            ),
            'options' => array(
                'label' => 'Light text color',
            ),
        ));

        $this->add(array(
            'name' => 'dark_txt',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'dark_txt',
                'class' => 'form-control',
                'value' => '#000000'
            ),
            'options' => array(
                'label' => 'Dark text color',
            ),
        ));
        /* .../COLORS */


        $this->add(array(
            'name' => 'save',
//            'type' => 'Submit',
            'type' => 'Button',
            'options' => array(
                'label' => '<span class="glyphicon" style="float: left;"></span>SAVE TEMPLATE<span class="glyphicon glyphicon-ok" style="float: right;"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
            'attributes' => array(
                'id' 	=> 'save',
                'value' => 'SAVE TEMPLATE',
                'class'	=> 'btn btn-success btn-block btn-flat',
                'style' => 'margin-bottom: 0;'
            ),
        ));

    }

    public function getView() {
        return 'form/template';
    }
}