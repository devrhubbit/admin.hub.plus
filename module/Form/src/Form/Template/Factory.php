<?php
namespace Form\Template;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Factory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {

        $form = new TemplateForm($serviceLocator);
        $Translator = $serviceLocator->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($Translator);

        $filter = new TemplateFilter();
        $form->setInputFilter($filter->getInputFilter());

        return $form;
    }
}