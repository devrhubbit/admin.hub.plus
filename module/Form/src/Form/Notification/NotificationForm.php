<?php

namespace Form\Notification;


use Application\Model\ContentModel;
use Application\Model\SectionModel;
use Common\Helper\AclHelper;
use Form\HpForm;
use Zend\ServiceManager\ServiceManager;

class NotificationForm extends HpForm
{

    const OPEN_MAIN = 'MAIN';
    const OPEN_SECTION = 'MASTER';
    const OPEN_CONTENT = 'DETAIL';

    public function __construct(ServiceManager $serviceLocator)
    {
        parent::__construct('notification');

        $aclHelper = new AclHelper($serviceLocator);

        $this->addHiddenField('scheduled_at', date('m/d/Y'));
        $this->addHiddenField('notification_lang', $aclHelper->getApp()->main_contents_language);

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'title',
                'class' => 'form-control',
                'placeholder' => 'Insert notification title or nothing to automatically set app name as title',
            ),
            'options' => array(
                'label' => 'Message title',
            ),
        ));

        $this->add(array(
            'name' => 'message',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'message',
                'class' => 'form-control',
                'placeholder' => 'Insert your notification message*',
            ),
            'options' => array(
                'label' => 'Message body*',
            ),
        ));

        $this->add(array(
            'name' => 'send_date',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'send_date',
                'class' => 'form-control',
                'placeholder' => 'Send notification date',
            ),
            'options' => array(
                'label' => 'Send date',
            ),
        ));

        $this->add(array(
            'name' => 'send_time',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'send_time',
                'class' => 'form-control',
                'placeholder' => 'Send notification time',
                //'value'         => date('G:i'),
            ),
            'options' => array(
                'label' => 'Send time',
            ),
        ));

        $this->add(array(
            'name' => 'to',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'to',
                'class' => 'form-control',
                'placeholder' => 'your filter...',
            ),
            'options' => array(
                'label' => 'Receiver filter
                            <small style="font-weight: normal; color: #AAAAAA;">
                                type #hashtag or your users name/email and hit enter
                            </small>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $contentModel = new ContentModel($serviceLocator);
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'content',
            'attributes' => array(
                'id' => 'content',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                "data-live-search" => true,
                "data-size" => 5,
                'options' => $contentModel->getContentSimpleList(),
            ),
            'options' => array(
                'label' => 'Select content',
                'disable_inarray_validator' => true,
            ),
        ));

        $sectionModel = new SectionModel($serviceLocator);
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'section',
            'attributes' => array(
                'id' => 'section',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                "data-live-search" => true,
                "data-size" => 5,
                'options' => $sectionModel->getSectionSimpleList(),
            ),
            'options' => array(
                'label' => 'Select section',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'action',
            'attributes' => array(
                'value' => self::OPEN_MAIN,
            ),
            'options' => array(
                'label' => 'Page to open',
                'label_attributes' => array(
                    'class' => 'btn btn-default'
                ),
                'value_options' => array(
                    self::OPEN_MAIN => 'MAIN',
                    self::OPEN_SECTION => 'SECTION',
                    self::OPEN_CONTENT => 'CONTENT',
                ),
                'disable_inarray_validator' => true,
            ),
        ));
    }

    public function getView()
    {
        return 'form/notification';
    }
}