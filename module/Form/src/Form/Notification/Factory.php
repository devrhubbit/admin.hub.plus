<?php

namespace Form\Notification;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;

class Factory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $form = new NotificationForm($serviceLocator);
        $Translator = $serviceLocator->get('translator');
        AbstractValidator::setDefaultTranslator($Translator);

        $filter = new NotificationFilter();
        $form->setInputFilter($filter->getInputFilter());

        return $form;
    }
}