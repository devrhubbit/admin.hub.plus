<?php
namespace Form\Action\Calendar;

use Form\Action\ActionForm;

class CalendarActionForm extends ActionForm {

    protected $serviceLocator;

    public function __construct($serviceLocator) {

        parent::__construct($serviceLocator, ActionForm::CALENDAR_ACTION);

        $this->addHiddenField("action_start_date");
        $this->addHiddenField("action_end_date");

        $this->add(array(
            'name' => 'action_date_picker',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'action_date_picker',
                'class' => 'form-control',
                'placeholder'=> 'From date - to date',
            ),
            'options' => array(
                'label' => 'Action date *',
            ),
        ));
    }

    public function getView() {
        return 'form/action/calendar';
    }

    public function getType()
    {
        return ActionForm::CALENDAR_ACTION;
    }
}