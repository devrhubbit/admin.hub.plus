<?php
namespace Form\Action\Attachment;

use Form\Action\ActionFilter;
use Zend\InputFilter\Factory as InputFactory;

class AttachmentActionFilter extends ActionFilter {

    public function getInputFilter()
    {
        parent::getInputFilter();

        if ($this->inputFilter) {
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'action_attachment_media_id',
                'required' => true,
                'validators' => array(
                    array(
                        'name'   => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                            ),
                        ),
                        'break_chain_on_failure' => true,
                    ),
                ),
            )));

        }

        return $this->inputFilter;
    }

}