<?php
namespace Form\Action\Attachment;

use Form\Action\ActionForm;

class AttachmentActionForm extends ActionForm {

    protected $serviceLocator;

    public function __construct($serviceLocator) {

        parent::__construct($serviceLocator, ActionForm::ATTACHMENT_ACTION);

        $this->addHiddenField("action_attachment_media_id");
        $this->addHiddenField("action_attachment_media_url");
        $this->addHiddenField("action_attachment_media_mime_type");

        $this->add(array(
            'name' => 'action_upload_file',
            'attributes' => array(
                'type' => 'File',
                'id' => 'action_upload_file',
                'class' => 'file-loading',
            ),
            'options' => array(
                'label' => 'Add Document *',
            ),
        ));

    }

    public function getView() {
        return 'form/action/attachment';
    }

    public function getType()
    {
        return ActionForm::ATTACHMENT_ACTION;
    }
}