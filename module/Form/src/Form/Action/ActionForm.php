<?php

namespace Form\Action;

use Form\HpForm;

abstract class ActionForm extends HpForm
{

    protected $serviceLocator;

    const CALENDAR_ACTION = "CALENDAR";
    const CONTENT_ACTION = "CONTENT";
    const EMAIL_ACTION = "EMAIL";
    const LINK_ACTION = "LINK";
    const MAP_ACTION = "MAP";
    const PHONE_ACTION = "PHONE";
    const SECTION_ACTION = "SECTION";
    const ATTACHMENT_ACTION = "ATTACHMENT";

    const MENU_ACTION_AREA = "MENU";
    const HEADER_ACTION_AREA = "HEADER";
    const FOOTER_ACTION_AREA = "FOOTER";
    const FLOATING_ACTION_AREA = "FLOATING";

    private static $actions = null;

    public function __construct($serviceLocator, $type)
    {
        parent::__construct('action');

        $this->addHiddenField("action_icon");
        $this->addHiddenField("action_icon_id");

        $this->add(array(
            'name' => 'action_label',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'action_label',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Action label *',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'action_area',
            'attributes' => array(
                'id' => 'action_area',
                'title' => 'Select Area *',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => [
//                    self::MENU_ACTION_AREA => self::MENU_ACTION_AREA,
                    self::HEADER_ACTION_AREA => self::HEADER_ACTION_AREA,
                    self::FOOTER_ACTION_AREA => self::FOOTER_ACTION_AREA,
//                    self::FLOATING_ACTION_AREA => self::FLOATING_ACTION_AREA,
                ],
                'value' => self::FOOTER_ACTION_AREA,
            ),
            'options' => array(
                'label' => 'Select Area *',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'name' => 'action_upload_icon',
            'attributes' => array(
                'type' => 'File',
                'id' => 'action_upload_icon',
                'class' => 'file-loading',
            ),
            'options' => array(
                'label' => 'Icon',
            )
        ));

    }

    public static function getActions()
    {
        if (self::$actions === null) {
            self::$actions = array(
                self::ATTACHMENT_ACTION => array('label' => ucfirst(strtolower(self::ATTACHMENT_ACTION)), 'icon_class' => 'fa fa-paperclip'),
                self::CALENDAR_ACTION => array('label' => ucfirst(strtolower(self::CALENDAR_ACTION)), 'icon_class' => 'fa fa-calendar'),
                self::CONTENT_ACTION => array('label' => ucfirst(strtolower(self::CONTENT_ACTION)), 'icon_class' => 'fa fa-files-o'),
                self::EMAIL_ACTION => array('label' => ucfirst(strtolower(self::EMAIL_ACTION)), 'icon_class' => 'fa fa-envelope-o'),
                self::LINK_ACTION => array('label' => ucfirst(strtolower(self::LINK_ACTION)), 'icon_class' => 'fa fa-link'),
                self::MAP_ACTION => array('label' => ucfirst(strtolower(self::MAP_ACTION)), 'icon_class' => 'fa fa-map-marker'),
                self::PHONE_ACTION => array('label' => ucfirst(strtolower(self::PHONE_ACTION)), 'icon_class' => 'fa fa-phone'),
                self::SECTION_ACTION => array('label' => ucfirst(strtolower(self::SECTION_ACTION)), 'icon_class' => 'fa fa-puzzle-piece'),
            );
        }
        return self::$actions;
    }

}