<?php
namespace Form\Action\Phone;

use Form\Action\ActionFilter;
use Zend\InputFilter\Factory as InputFactory;

class PhoneActionFilter extends ActionFilter {

    public function getInputFilter()
    {
        parent::getInputFilter();

        if ($this->inputFilter) {
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'action_number',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'Regex',
                        'options' => array(
                            'pattern' => '/^\s*(?:\+?(\d{1,4}))?([-. (]*(\d{1,3})[-. )]*)?((\d{2,4})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/',
                            'message' => "Invalid phone number",
                        )
                    )
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'action_prefix',
                'required' => false,
                'allow_empty' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'action_operation',
                'required' => false,
                'allow_empty' => true,
            )));

        }

        return $this->inputFilter;
    }




}