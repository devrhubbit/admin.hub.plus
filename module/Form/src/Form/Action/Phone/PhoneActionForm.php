<?php
namespace Form\Action\Phone;

use Form\Action\ActionForm;
use MessagesService\Model\MessagesModel;
use MessagesService\Service\Factory as MessagesFactory;

class PhoneActionForm extends ActionForm {

    protected $serviceLocator;

    const PHONE_MODE_CALL = "CALL";
    const PHONE_MODE_CALL_LABEL = "Call";
    const PHONE_MODE_SMS = "SMS";
    const PHONE_MODE_SMS_LABEL = "SMS";

    public function __construct($serviceLocator) {

        parent::__construct($serviceLocator, ActionForm::PHONE_ACTION);

        $smsModel = new MessagesModel($serviceLocator, MessagesFactory::MODE_SMS);
        $phoneArray = array();
        foreach ($smsModel->getPhoneCountries() as $phones) {
            $phoneArray[$phones] = $phones;
        }

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'action_prefix',
            'attributes' => array(
                'id' => 'action_prefix',
                'class' => 'selectpicker',
                'options' => $phoneArray,
                'value' => $smsModel->getDefaultPhoneCode(),
                'data-width' => '100%',
                'data-size'  => '5',
                'data-live-search' => "true",
            ),
            'options' => array(
                'label' => 'Prefix',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'name' => 'action_number',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'action_number',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Phone number',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'action_operation',
            'attributes' => array(
                'id' => 'action_operation',
                'class' => 'selectpicker',
                'options' => [
                    self::PHONE_MODE_CALL => self::PHONE_MODE_CALL_LABEL,
                    self::PHONE_MODE_SMS  => self::PHONE_MODE_SMS_LABEL,
                ],
                'data-width' => '100%',
            ),
            'options' => array(
                'label' => 'Operation',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'name' => 'action_phone_placeholder',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'action_phone_placeholder',
                'class' => 'form-control',
                'style' => 'height: 100px;',
            ),
            'options' => array(
                'label' => 'Placeholder',
            ),
        ));
    }

    public function getView() {
        return 'form/action/phone';
    }

    public function getType()
    {
        return ActionForm::PHONE_ACTION;
    }
}