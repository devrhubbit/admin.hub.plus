<?php
namespace Form\Action\Section;

use Form\Action\ActionForm;

class SectionActionForm extends ActionForm {

    protected $serviceLocator;

    private $sectionList = array();

    public function __construct($serviceLocator) {

        parent::__construct($serviceLocator, ActionForm::SECTION_ACTION);

    }

    public function setSectionList($sectionList) {
        $this->sectionList = $sectionList;

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'action_section_list',
            'attributes' => array(
                'id' => 'action_section_list',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => $this->sectionList,
                'data-size'  => '5',
                'data-live-search' => "true",
            ),
            'options' => array(
                'label' => 'Section list',
                'disable_inarray_validator' => true,
            ),
        ));
    }

    public function getView() {
        return 'form/action/section';
    }

    public function getType()
    {
        return ActionForm::SECTION_ACTION;
    }
}