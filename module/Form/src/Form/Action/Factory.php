<?php
namespace Form\Action;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use \Application\Model\ActionModel;

class Factory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $actionType = ActionModel::getSelectedType();
        $form = null;
        $filter = null;

        switch($actionType) {
            case ActionForm::ATTACHMENT_ACTION:
                $filter = new Attachment\AttachmentActionFilter();
                $form   = new Attachment\AttachmentActionForm($serviceLocator);
                break;
            case ActionForm::CALENDAR_ACTION:
                $filter = new Calendar\CalendarActionFilter();
                $form   = new Calendar\CalendarActionForm($serviceLocator);
                break;
            case ActionForm::CONTENT_ACTION:
                $filter = new Content\ContentActionFilter();
                $form   = new Content\ContentActionForm($serviceLocator);
                break;
            case ActionForm::EMAIL_ACTION:
                $filter = new Email\EmailActionFilter();
                $form   = new Email\EmailActionForm($serviceLocator);
                break;
            case ActionForm::LINK_ACTION:
                $filter = new Link\LinkActionFilter();
                $form   = new Link\LinkActionForm($serviceLocator);
                break;
            case ActionForm::MAP_ACTION:
                $filter = new Map\MapActionFilter();
                $form   = new Map\MapActionForm($serviceLocator);
                break;
            case ActionForm::PHONE_ACTION:
                $filter = new Phone\PhoneActionFilter();
                $form   = new Phone\PhoneActionForm($serviceLocator);
                break;
            case ActionForm::SECTION_ACTION:
                $filter = new Section\SectionActionFilter();
                $form   = new Section\SectionActionForm($serviceLocator);
                break;
        }

        $form->setInputFilter($filter->getInputFilter());

        $Translator = $serviceLocator->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($Translator);

        return $form;
    }
}