<?php
namespace Form\Action\Map;

use Form\Action\ActionForm;

class MapActionForm extends ActionForm {

    protected $serviceLocator;

    public function __construct($serviceLocator) {

        parent::__construct($serviceLocator, ActionForm::MAP_ACTION);

        $this->addHiddenField("action_latitude");
        $this->addHiddenField("action_longitude");

        $this->add(array(
            'name' => 'action_address',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'action_address',
                'class' => 'form-control',
                'placeholder' => 'Insert your address here',
                'value' => 'Via Giuseppe Orlandi, 54, 70010 Turi BA, Italia',
            ),
            'options' => array(
                'label' => 'Address *',
            ),
        ));

        $this->add(array(
            'name' => 'action_street',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'action_street',
                'disabled' 	=> 'disabled',
                'class' => 'address-field form-control',
                'placeholder' => 'Street',
            ),
            'options' => array(
                'label' => 'Street',
            ),
        ));

        $this->add(array(
            'name' => 'action_city',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'action_city',
                'disabled' 	=> 'disabled',
                'class' => 'address-field form-control',
                'placeholder' => 'City',
            ),
            'options' => array(
                'label' => 'City',
            ),
        ));

        $this->add(array(
            'name' => 'action_state',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'action_state',
                'disabled' 	=> 'disabled',
                'class' => 'address-field form-control',
                'placeholder' => 'State',
            ),
            'options' => array(
                'label' => 'State or Province',
            ),
        ));

        $this->add(array(
            'name' => 'action_zip',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'action_zip',
                'disabled' 	=> 'disabled',
                'class' => 'address-field form-control',
                'placeholder' => 'Postal code',
            ),
            'options' => array(
                'label' => 'Postal code',
            ),
        ));

        $this->add(array(
            'name' => 'action_country',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'action_country',
                'disabled' 	=> 'disabled',
                'class' => 'address-field form-control',
                'placeholder' => 'Country',
            ),
            'options' => array(
                'label' => 'Country',
            ),
        ));

    }

    public function getView() {
        return 'form/action/map';
    }

    public function getType()
    {
        return ActionForm::MAP_ACTION;
    }
}