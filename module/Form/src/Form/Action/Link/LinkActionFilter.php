<?php
namespace Form\Action\Link;

use Form\Action\ActionFilter;
use Form\Validator\UrlValidator;
use Zend\InputFilter\Factory as InputFactory;

class LinkActionFilter extends ActionFilter {

    public function getInputFilter()
    {
        parent::getInputFilter();

        if ($this->inputFilter) {
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'action_url',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'   => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                            ),
                        ),
                        'break_chain_on_failure' => true,
                    ),
                    array(
                        'name' => '\Form\Validator\UrlValidator',
                        'options' => array(
                            'messages' => array(
                                UrlValidator::INVALID_URL => "Invalid URL value"
                            ),
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'action_open_in_app',
                'required' => false,
                'allow_empty' => true,
            )));

        }

        return $this->inputFilter;
    }




}