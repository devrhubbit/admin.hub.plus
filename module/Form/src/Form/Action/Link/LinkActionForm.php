<?php
namespace Form\Action\Link;

use Form\Action\ActionForm;

class LinkActionForm extends ActionForm {

    protected $serviceLocator;

    public function __construct($serviceLocator) {

        parent::__construct($serviceLocator, ActionForm::LINK_ACTION);

        $this->add(array(
            'name' => 'action_url',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'action_url',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Action Url *',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'action_open_in_app',
            'options' => array(
                'label' => 'Open in App',
            ),
            'attributes' => array(
                'id' => 'action_open_in_app',
                'class' => 'switch-btn',
                'value' => true,
            )
        ));

        $this->add(array(
            'name' => 'action_custom_css',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' 	=> 'action_custom_css',
                'class' => 'form-control hidden',
            ),
            'options' => array(
                'label' => 'Custom CSS',
            ),
        ));

        $this->add(array(
            'name' => 'action_custom_js',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' 	=> 'action_custom_js',
                'class' => 'form-control hidden',
            ),
            'options' => array(
                'label' => 'Custom JavaScript',
            ),
        ));

    }

    public function getView() {
        return 'form/action/link';
    }

    public function getType()
    {
        return ActionForm::LINK_ACTION;
    }
}