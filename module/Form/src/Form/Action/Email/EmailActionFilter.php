<?php
namespace Form\Action\Email;

use Form\Action\ActionFilter;
use Zend\InputFilter\Factory as InputFactory;

class EmailActionFilter extends ActionFilter {

    public function getInputFilter()
    {
        parent::getInputFilter();

        if ($this->inputFilter) {
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'action_email',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'   => 'EmailAddress',
                        'options' => array(
                            'message' => 'Invalid email address'
                        ),
                    ),
                ),
            )));

        }

        return $this->inputFilter;
    }




}