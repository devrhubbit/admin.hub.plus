<?php
namespace Form\Action\Email;

use Form\Action\ActionForm;

class EmailActionForm extends ActionForm {

    protected $serviceLocator;

    public function __construct($serviceLocator) {

        parent::__construct($serviceLocator, ActionForm::EMAIL_ACTION);

        $this->add(array(
            'name' => 'action_email',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'action_email',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Email *',
            ),
        ));

        $this->add(array(
            'name' => 'action_email_subject',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'action_email_subject',
                'class' => 'form-control',
            ),
            'options' => array(
                'label' => 'Subject',
            ),
        ));

        $this->add(array(
            'name' => 'action_email_placeholder',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'action_email_placeholder',
                'class' => 'form-control',
                'style' => 'height: 100px;',
            ),
            'options' => array(
                'label' => 'Message body',
            ),
        ));

    }

    public function getView() {
        return 'form/action/email';
    }

    public function getType()
    {
        return ActionForm::EMAIL_ACTION;
    }
}