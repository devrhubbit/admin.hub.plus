<?php
namespace Form\Action\Content;

use Form\Action\ActionForm;

class ContentActionForm extends ActionForm {

    protected $serviceLocator;

    private $contentList = array();

    public function __construct($serviceLocator) {

        parent::__construct($serviceLocator, ActionForm::CONTENT_ACTION);

    }

    public function setContentList($contentList) {
        $this->contentList = $contentList;

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'action_content_list',
            'attributes' => array(
                'id' => 'action_content_list',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => $this->contentList,
                'data-size'  => '5',
                'data-live-search' => "true",
            ),
            'options' => array(
                'label' => 'Content list',
                'disable_inarray_validator' => true,
            ),
        ));
    }

    public function getView() {
        return 'form/action/content';
    }

    public function getType()
    {
        return ActionForm::CONTENT_ACTION;
    }
}