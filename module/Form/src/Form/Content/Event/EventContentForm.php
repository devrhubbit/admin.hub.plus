<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 17:57
 */

namespace Form\Content\Event;

use Form\Content\ContentForm;
use Form\LayoutForm;

class EventContentForm extends ContentForm
{

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, ContentForm::EVENT);

        $this->addHiddenField("start-date");
        $this->addHiddenField("end-date");
        $this->addHiddenField("latitude");
        $this->addHiddenField("longitude");
        $this->addHiddenField("content_push_notification_id");
        $this->addHiddenField("content_push_notification_enable_hidden");

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'title',
                'class' => 'form-control',
                'placeholder' => 'Your title...',
            ),
            'options' => array(
                'label' => 'Event title *',
            ),
        ));

        $this->add(array(
            'name' => 'event_date_picker',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'event_date_picker',
                'class' => 'form-control',
                'placeholder' => 'from date - to date',
            ),
            'options' => array(
                'label' => 'Event date *',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'content_push_notification_enable',
            'options' => array(
                'label' => 'Auto notification',
            ),
            'attributes' => array(
                'id' => 'content_push_notification_enable',
                'class' => 'switch-btn',
            )
        ));

        $this->add(array(
            'name' => 'content_push_notification_send_date',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'content_push_notification_send_date',
                'class' => 'form-control',
                'placeholder'=> 'Send notification date',
            ),
            'options' => array(
                'label' => 'Notification send date',
            ),
        ));

        $this->add(array(
            'name' => 'content_push_notification_send_time',
            'attributes' => array(
                'type' 	        => 'Text',
                'id' 	        => 'content_push_notification_send_time',
                'class'         => 'form-control',
                'placeholder'   => 'Send notification time',
            ),
            'options' => array(
                'label' => 'Notification send time',
            ),
        ));


        $this->addLayoutBlock(array('filter', 'paged', 'open_in_app', 'page_size', 'autoplay', 'streaming'), LayoutForm::EVENT);
    }

    public function getView()
    {
        return 'form/content/event';
    }
}