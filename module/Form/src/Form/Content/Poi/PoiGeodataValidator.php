<?php

namespace Form\Content\Poi;

use Zend\Validator\AbstractValidator;

class PoiGeodataValidator extends AbstractValidator {

    const IS_EMPTY     = 'ISEMPTY';
    const INVALID_UUID = 'INVALIDUUID';

    protected $messageTemplates = array(
        self::IS_EMPTY     => 'Required value',
        self::INVALID_UUID => 'Invalid Uuid value',
    );

    public function __construct(array $options = array()) {
        parent::__construct($options);
    }

    public function isValid($value) {

        $value = json_decode($value);
        switch ($value->region) {
            case "":
                $result = true;
                break;
            case "circular_region":
                $result = true;
                break;
            case "beacon_region":
                $result = self::beaconRegionUuidValidator($value);
                break;
            default:
                $result = false;
        }

        return $result;
    }

    private function beaconRegionUuidValidator($value) {
        if ($value->uuid == "") {
            $this->error(self::IS_EMPTY);
            return false;
        } else {
            return self::uuidPregMatchValidator($value->uuid);
        }
    }

    private function uuidPregMatchValidator($uuid) {
        $uuidValidated = preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i', $uuid);
        if (!$uuidValidated) {
            $this->error(self::INVALID_UUID);
            return false;
        } else {
            return true;
        }
    }
}