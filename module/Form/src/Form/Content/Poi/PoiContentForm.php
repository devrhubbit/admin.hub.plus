<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 17:57
 */

namespace Form\Content\Poi;

use Form\Content\ContentForm;
use Form\LayoutForm;

class PoiContentForm extends ContentForm
{

    public function __construct($serviceLocator)
    {

        parent::__construct($serviceLocator, ContentForm::POI);

        $this->addHiddenField("region"); //CIRCULAR or BEACON
        $this->addHiddenField("beacon_uuid_validator");

        $this->addHiddenField("latitude");
        $this->addHiddenField("longitude");

        $this->addHiddenField("pin_map");
        $this->addHiddenField("pin_map_id");

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'title',
                'class' => 'form-control',
                'placeholder' => 'Your title...',
            ),
            'options' => array(
                'label' => 'POI title*',
            ),
        ));

        $this->add(array(
            'name' => 'subtitle',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'subtitle',
                'class' => 'form-control',
                'placeholder' => "iBeacon's identification name",
            ),
            'options' => array(
                'label' => 'Name',
            ),
        ));

        $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'address',
                'class' => 'form-control',
                'placeholder' => 'Insert your address here',
                'value' => 'Via Giuseppe Orlandi, 54, 70010 Turi BA, Italia',
            ),
            'options' => array(
                'label' => 'Address',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'poi_notification',
            'options' => array(
                'label' => 'Enable notification',
            ),
            'attributes' => array(
                'id' => 'poi_notification',
                'class' => 'switch-btn',
            )
        ));

        $this->add(array(
            'name' => 'upload_pin_map',
            'attributes' => array(
                'type' => 'File',
                'id' => 'upload_pin_map',
                'class' => 'file-loading',
            ),
            'options' => array(
                'label' => 'Pin Map Image',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'radius',
            'attributes' => array(
                'id' => 'radius',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'options' => [
                    "0" => "0 m",
                    "25" => "25 m",
                    "50" => "50 m",
                    "100" => "100 m",
                    "250" => "250 m",
                    "500" => "500 m",
                    "1000" => "1 km",
                    "2000" => "2 km",
                    "5000" => "5 km",
                    "10000" => "10 km",
                ],
//                'value' => "25",
            ),
            'options' => array(
                'label' => 'Radius',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'name' => 'uuid',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'uuid',
                'class' => 'form-control',
                'placeholder' => "iBeacon's UUID*",
            ),
            'options' => array(
                'label' => 'UUID',
            ),
        ));

        $this->add(array(
            'name' => 'major',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'major',
                'class' => 'form-control',
                'placeholder' => "iBeacon's major",
            ),
            'options' => array(
                'label' => 'Major',
            ),
        ));

        $this->add(array(
            'name' => 'minor',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'minor',
                'class' => 'form-control',
                'placeholder' => "iBeacon's minor",
            ),
            'options' => array(
                'label' => 'Minor',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'beacon_range',
            'attributes' => array(
                'value' => 'IMMEDIATE',
            ),
            'options' => array(
                'label' => 'Range',
                'label_attributes' => array(
                    'class' => 'btn btn-default'
                ),
                'value_options' => array(
                    'IMMEDIATE' => '< 1m',
                    'NEAR' => '< 3m',
                    'FAR' => '> 3m',
                ),
                'disable_inarray_validator' => true,
            ),
        ));

        $this->addLayoutBlock(array('filter', 'paged', 'open_in_app', 'page_size', 'autoplay', 'streaming'), LayoutForm::POI);
    }

    public function getView()
    {
        return 'form/content/poi';
    }
}