<?php

namespace Form\Content;

use Zend\Validator\AbstractValidator;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use \Application\Model\ContentModel;

class Factory implements FactoryInterface
{

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $contentType = ContentModel::getSelectedType();
        $form = null;
        $filter = null;

        switch ($contentType) {
            case ContentForm::EVENT:
                $filter = new Event\EventContentFilter();
                $form = new Event\EventContentForm($serviceLocator);
                break;
            case ContentForm::PERSON:
                $filter = new Person\PersonContentFilter();
                $form = new Person\PersonContentForm($serviceLocator);
                break;
            case ContentForm::POI:
                $filter = new Poi\PoiContentFilter();
                $form = new Poi\PoiContentForm($serviceLocator);
                break;
            case ContentForm::GENERIC:
                $filter = new Generic\GenericContentFilter();
                $form = new Generic\GenericContentForm($serviceLocator);
                break;
            case ContentForm::WEB:
                $filter = new Web\WebContentFilter();
                $form = new Web\WebContentForm($serviceLocator);
                break;
        }

        $form->setInputFilter($filter->getInputFilter());

        $Translator = $serviceLocator->get('translator');
        AbstractValidator::setDefaultTranslator($Translator);

        return $form;
    }
}