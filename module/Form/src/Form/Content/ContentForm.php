<?php

namespace Form\Content;

use Application\Model\UserModel;
use Form\LayoutForm;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;

abstract class ContentForm extends LayoutForm
{
    protected $serviceLocator;

    private $userModel = null;

    public function __construct(ServiceLocatorInterface $serviceLocator, $type)
    {
        parent::__construct('content');

        $this->serviceLocator = $serviceLocator;
        $session = new Container('acl');

        $this->userModel = new UserModel($serviceLocator);

        $this->addHiddenField("content_status");
        $this->addHiddenField("type", $type);
        $this->addHiddenField("gallery_ids");
        $this->addHiddenField("cover_id");
        $this->addHiddenField("extra-params");
        $this->addHiddenField("actions");
        $this->addHiddenField("deleted_actions");
        $this->addHiddenField("content_lang", $session->currentLang);
        $this->addHiddenField("reload_content", 0);

        $this->add(array(
            'name' => 'description',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'description',
                'class' => 'form-control',
                'placeholder' => 'Short description',
            ),
            'options' => array(
                'label' => 'Short description',
            ),
        ));

        $this->add(array(
            'name' => 'tags',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'tags',
                'class' => 'form-control',
                'placeholder' => 'type something and hit enter',
            ),
            'options' => array(
                'label' => 'Content TAGS <span class="tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'user_tags',
            'attributes' => array(
                'id' => 'user_tags',
                'title' => 'User Tags',
                'class' => 'selectpicker',
                'data-style' => 'btn-default',
                'data-width' => '100%',
                'multiple' => true,
                'options' => $this->userModel->getUserTags(),
            ),
            'options' => array(
                'label' => 'User TAGS <span class="user_tag_error text-danger"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                ),
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'name' => 'body',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' => 'body',
                'class' => 'form-control',
                'placeholder' => 'My content...',
            ),
            'options' => array(
                'label' => 'Post body',
            ),
        ));

        $this->add(array(
            'name' => 'save',
            'type' => 'Button',
            'options' => array(
                'label' => 'SAVE CONTENT<span id="save_button_icon" class="glyphicon glyphicon-ok" style="float: right;"></span>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
            'attributes' => array(
                'id' => 'save',
                'value' => 'SAVE POST',
                'class' => 'btn btn-success btn-block btn-flat',
            ),
        ));

        $this->add(array(
            'name' => 'param-key',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'param-key',
                'class' => 'form-control',
                'placeholder' => 'Key...',
            ),
            'options' => array(
                'label' => 'Key',
            ),
        ));

        $this->add(array(
            'name' => 'param-value',
            'attributes' => array(
                'type' => 'Text',
                'id' => 'param-value',
                'class' => 'form-control',
                'placeholder' => 'Value...',
            ),
            'options' => array(
                'label' => 'Value',
            ),
        ));

        $this->add(array(
            'name' => 'add-param',
            'type' => 'Button',
            'options' => array(
                'label' => '<i class="fa fa-plus" aria-hidden="true"></i>',
                'label_options' => array(
                    'disable_html_escape' => true,
                )
            ),
            'attributes' => array(
                'id' => 'add-param',
                'class' => 'btn btn-primary btn-block btn-flat',
            ),
        ));
    }
}


