<?php
namespace Form\Content;

// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ContentFilter implements InputFilterAwareInterface {

    protected $inputFilter;                       // <-- Add this variable

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

	public function getInputFilter() {

    	if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'cell-type',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'   => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Choose your post layout"
                            ),
                        ),
                        'break_chain_on_failure' => true,
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'user_tags',
                'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'connector_validator',
                'validators' => array(
                    array(
                        'name' => '\Form\Section\ConnectorValidator',
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'provider',
                'required' => false,
                'allow_empty' => true,
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}