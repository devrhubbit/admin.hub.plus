<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 18:00
 */

namespace Form\Content\Person;

use Form\Content\ContentFilter;
use Zend\InputFilter\Factory as InputFactory;

class PersonContentFilter extends ContentFilter
{
    public function getInputFilter()
    {
        parent::getInputFilter();

        if ($this->inputFilter) {
            $inputFilter = $this->inputFilter;
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'firstname',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'   => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
                            ),
                        ),
                        'break_chain_on_failure' => true,
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'lastname',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
//                'validators' => array(
//                    array(
//                        'name'   => 'NotEmpty',
//                        'options' => array(
//                            'messages' => array(
//                                \Zend\Validator\NotEmpty::IS_EMPTY => "Required value"
//                            ),
//                        ),
//                        'break_chain_on_failure' => true,
//                    ),
//                ),
            )));
        }

        return $this->inputFilter;
    }
}