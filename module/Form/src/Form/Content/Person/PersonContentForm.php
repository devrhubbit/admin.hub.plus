<?php
/**
 * Created by PhpStorm.
 * User: mirco
 * Date: 15/06/16
 * Time: 17:57
 */

namespace Form\Content\Person;

use Form\Content\ContentForm;
use Form\LayoutForm;

class PersonContentForm extends ContentForm
{

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, ContentForm::PERSON);

        $this->remove('title');
        $this->addHiddenField("title");

        $this->add(array(
            'name' => 'firstname',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'firstname',
                'class' => 'form-control',
                'placeholder'=> 'Firstname*',
            ),
            'options' => array(
                'label' => 'Firstname*',
            ),
        ));

        $this->add(array(
            'name' => 'lastname',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'lastname',
                'class' => 'form-control',
                'placeholder'=> 'Lastname',
            ),
            'options' => array(
                'label' => 'Lastname',
            ),
        ));

        $this->add(array(
            'name' => 'birthday',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'birthday',
                'class' => 'form-control',
                'placeholder'=> 'Insert birthday',
            ),
            'options' => array(
                'label' => 'Birthday',
            ),
        ));

        $this->add(array(
            'name' => 'death',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'death',
                'class' => 'form-control',
                'placeholder'=> 'Insert death day',
            ),
            'options' => array(
                'label' => 'Death day',
            ),
        ));

        $this->addLayoutBlock(array('filter', 'paged', 'open_in_app', 'page_size', 'autoplay', 'streaming'), LayoutForm::PERSON);
    }

    public function getView() {
        return 'form/content/person';
    }
}