<?php

namespace Form\Content\Web;

use Form\Content\ContentForm;
use Form\LayoutForm;

class WebContentForm extends ContentForm
{

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, ContentForm::WEB);

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'title',
                'class' => 'form-control',
                'placeholder' => 'Your title...',
            ),
            'options' => array(
                'label' => 'Title *',
            ),
        ));

        $this->add(array(
            'name' => 'subtitle',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'subtitle',
                'class' => 'form-control',
                'placeholder'=> "This post's subtitle...",
            ),
            'options' => array(
                'label' => 'Subtitle',
            ),
        ));

        $this->add(array(
            'name' => 'body',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'body',
                'class' => 'form-control',
                'placeholder'=> "http://external-link.com/",
            ),
            'options' => array(
                'label' => 'External link *',
            ),
        ));

        $this->add(array(
            'name' => 'custom_css',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' 	=> 'custom_css',
                'class' => 'form-control hidden',
            ),
            'options' => array(
                'label' => 'Custom CSS',
            ),
        ));

        $this->add(array(
            'name' => 'custom_js',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id' 	=> 'custom_js',
                'class' => 'form-control hidden',
            ),
            'options' => array(
                'label' => 'Custom JavaScript',
            ),
        ));

        $this->addLayoutBlock(array('filter', 'paged', 'page_size', 'autoplay', 'streaming'), LayoutForm::WEB);
    }

    public function getView() {
        return 'form/content/web';
    }
}