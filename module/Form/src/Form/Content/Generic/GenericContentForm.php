<?php

namespace Form\Content\Generic;

use Form\Content\ContentForm;
use Form\LayoutForm;

class GenericContentForm extends ContentForm
{

    public function __construct($serviceLocator)
    {
        parent::__construct($serviceLocator, ContentForm::GENERIC);

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'title',
                'class' => 'form-control',
                'placeholder'=> 'Your title...',
            ),
            'options' => array(
                'label' => 'Title*',
            ),
        ));

        $this->add(array(
            'name' => 'subtitle',
            'attributes' => array(
                'type' 	=> 'Text',
                'id' 	=> 'subtitle',
                'class' => 'form-control',
                'placeholder'=> "This post's subtitle...",
            ),
            'options' => array(
                'label' => 'Subtitle',
            ),
        ));

        $this->addLayoutBlock(array('filter', 'paged', 'open_in_app', 'page_size', 'autoplay', 'streaming'), LayoutForm::GENERIC);
    }

    public function getView() {
        return 'form/content/generic';
    }
}