<?php
namespace Form\PasswordRecovery;

use Form\HpForm;

class PasswordRecoveryForm extends HpForm
{
	protected $serviceLocator;

	public function __construct($serviceLocator)
	{
		parent::__construct('password-recovery');

		$this->serviceLocator = $serviceLocator;
		
		
		$this->add(array(
			'name' => 'email',
			'attributes' => array(
				'type' 	=> 'Text',
				'id' 	=> 'email',
				'class' => 'form-control', 
				'placeholder'=> 'Email',
			),
			'options' => array(
				'label' => 'Email*',
			),
		));
		
		$this->add(array(
			'name' => 'continue',
			'type' => 'Submit',
			'attributes' => array(
				'id' 	=> 'continue',
				'value' => 'Continue',
				'class'	=> 'btn btn-primary btn-block btn-flat',
			),
		));
		
	}
	
	public function isValidPostRequest($request) {
		if ($request->isPost()) {
						
			$this->setData($request->getPost());
			
			if ($this->isValid()) {
				return true;
			}
		}
		return false;
	}

	public function getView()
	{
		return 'form/password-recovery';
	}
	
	
}


