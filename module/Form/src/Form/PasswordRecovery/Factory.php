<?php 
namespace Form\PasswordRecovery;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Factory implements FactoryInterface
{
	
	public function createService(ServiceLocatorInterface $serviceLocator) {
		
		$form = new PasswordRecoveryForm($serviceLocator);
		$Translator = $serviceLocator->get('translator');		
		\Zend\Validator\AbstractValidator::setDefaultTranslator($Translator);
		
		$filter = new PasswordRecoveryFilter();
		$form->setInputFilter($filter->getInputFilter());
		
		return $form;
		
	}
}