<?php
namespace Form;

use Zend\Mvc\MvcEvent;

class Module
{
	protected $serviceLocator;

	public function onBootstrap(MvcEvent $e)
	{	
		$this->serviceManager 		= $e->getApplication()->getServiceManager();

		//Per essere sicuri che setLayout avvenga dopo il dispatch del controller
		$e->getApplication()
			->getEventManager()
			->getSharedManager()
			->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array($this, 'setLayout'), 10);
	
	}
	
	public function setLayout( \Zend\Mvc\MvcEvent $e ){
		$ServiceManagerTemplateMap = $this->serviceManager->get('Zend\View\Resolver\TemplateMapResolver');
		
//		$ServiceManagerTemplateMap->add(
//			array(
//				'form/login' 			 => __DIR__ . '/view/login-form.phtml',
//				'form/registration'		 => __DIR__ . '/view/registration-form.phtml',
////				'form/app'				 => __DIR__ . '/view/app-form.phtml',
//				'form/password-recovery' => __DIR__ . '/view/password-recovery-form.phtml',
//			)
//		);

	}
	
	public function getAutoloaderConfig()
    {
    	return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
    	return include __DIR__ . '/config/module.config.php';
    }
    
}
