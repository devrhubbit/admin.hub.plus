UPDATE `post` SET `type` = 'PERSON' WHERE `type` = 'ARTIST';
RENAME TABLE `post_artist` TO `post_person`;
UPDATE `section` SET `type` = 'PERSON' WHERE `type` = 'ARTIST';
UPDATE `section` SET `params` = '{"types":["EVENT","PERSON"]}' WHERE `params` = '{"types":["EVENT","ARTIST"]}';
UPDATE `section` SET `params` = '{"types":["PERSON"]}' WHERE `params` = '{"types":["ARTIST"]}';

CREATE TABLE `post_poi` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) unsigned DEFAULT NULL,
  `identifier` varchar(20) NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `lat` float(10,6) DEFAULT NULL,
  `lng` float(10,6) DEFAULT NULL,
  `radius` int(11) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `major` tinyint(3) unsigned DEFAULT NULL,
  `minor` tinyint(3) unsigned DEFAULT NULL,
  `distance` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_identifier` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE post ADD template_id INT NOT NULL AFTER search;
ALTER TABLE section ADD template_id INT NOT NULL AFTER params;

ALTER TABLE `post_poi` CHANGE `major` `major` SMALLINT(1)  UNSIGNED  NULL  DEFAULT NULL;
ALTER TABLE `post_poi` CHANGE `minor` `minor` SMALLINT(1)  UNSIGNED  NULL  DEFAULT NULL;


CREATE TABLE `remote_provider` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `email_canonical` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `slug` varchar(255) NOT NULL DEFAULT '',
  `icon` varchar(255) DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `section_connector` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `section_id` int(11) unsigned NOT NULL,
  `provider_id` int(11) unsigned NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT '',
  `baseurl` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `section_key` (`section_id`),
  KEY `provider_key` (`provider_id`),
  CONSTRAINT `provider_key` FOREIGN KEY (`provider_id`) REFERENCES `remote_provider` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `section_key` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `remote_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `connector_id` int(11) DEFAULT NULL,
  `remote_id` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `content_type` varchar(10) DEFAULT NULL,
  `liked_at` datetime DEFAULT NULL,
  `shared_at` datetime DEFAULT NULL,
  `share_count` int(11) DEFAULT '0',
  `view_count` int(11) DEFAULT '0',
  `like_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_id` (`connector_id`,`remote_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `remote_extra_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `connector_id` int(11) DEFAULT NULL,
  `remote_id` varchar(255) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL COMMENT 'Valid type string: VIEW, LIKE, UNLIKE, SHARE',
  `content_type` varchar(10) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `content_id` (`connector_id`,`remote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `group`
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  label VARCHAR(255) NOT NULL,
  created_at DATETIME NOT NULL,
  updated_at DATETIME NOT NULL,
  deleted_at DATETIME DEFAULT NULL
);

CREATE TABLE `notification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `to` text,
  `title` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `total_receiver` int(11) unsigned DEFAULT NULL,
  `count_click` int(11) unsigned DEFAULT NULL,
  `action` varchar(20) NOT NULL,
  `related_action_id` int(11) unsigned DEFAULT NULL,
  `scheduled_at` datetime NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `device` DROP FOREIGN KEY `FK_E83B3B8A76ED395`;
ALTER TABLE `user` DROP FOREIGN KEY `FK_FCB96C9EF5B7AF75`;
ALTER TABLE `user` ENGINE = MyISAM;

ALTER TABLE `user` ADD `terms` TINYINT(1)  UNSIGNED  NULL  DEFAULT NULL  AFTER `privacy`;
UPDATE `user` SET `terms` = '1';
