ALTER TABLE application ADD description TEXT NULL AFTER bundle;

/* RUN ON HP_CORE DATABASE */
DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'category unique identifier',
  `name` varchar(255) DEFAULT NULL COMMENT 'category name',
  `weight` int(11) DEFAULT NULL COMMENT 'ordering priority weight',
  `visible` tinyint(1) DEFAULT '1' COMMENT 'TRUE if category is visible in category list',
  `setting` text COMMENT 'json with enabled section per category',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;

INSERT INTO `category` (`id`, `name`, `weight`, `visible`, `setting`)
VALUES
  (1,'Event and Exhibitions',1,1,'{ \"section_types\": [\"EVENT\", \"PERSON\", \"POI\"] , \"content_types\": [\"EVENT\", \"PERSON\", \"POI\"]}'),
  (2,'Store and Retail',2,1,'{ \"section_types\": [\"EVENT\", \"PERSON\", \"POI\"] , \"content_types\": [\"EVENT\", \"PERSON\", \"POI\"]}'),
  (3,'Musem and Art Gallery',3,1,'{ \"section_types\": [\"EVENT\", \"PERSON\", \"POI\"] , \"content_types\": [\"EVENT\", \"PERSON\", \"POI\"]}'),
  (4,'Sport & Welness',4,1,'{ \"section_types\": [\"EVENT\", \"PERSON\", \"POI\"] , \"content_types\": [\"EVENT\", \"PERSON\", \"POI\"]}'),
  (5,'Tourism',5,1,'{ \"section_types\": [\"EVENT\", \"PERSON\", \"POI\"] , \"content_types\": [\"EVENT\", \"PERSON\", \"POI\"]}');

/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;


DROP TABLE IF EXISTS `template`;

CREATE TABLE `template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description` text,
  `icon` varchar(255) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `json_views` text COMMENT 'JSON defining views list of the template',
  `layout` text COMMENT 'JSON defining the standard layout relate to this template',
  `enabled` tinyint(1) DEFAULT NULL COMMENT 'TRUE if template is active, FALSE if exists only in old app release',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `template` WRITE;
/*!40000 ALTER TABLE `template` DISABLE KEYS */;

INSERT INTO `template` (`id`, `title`, `subtitle`, `description`, `icon`, `cover`, `json_views`, `layout`, `enabled`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'custom','','','/custom.jpg','','[]','{ \"name\": \"CUSTOM_LAYOUT\", \"like\": false, \"share\": false,	\"filter\": false, \"sort\": false, \"page_size\": 10, \"auth\": { \"mode\": \"POPUP\", \"login\": \"SIMPLE\" }, \"theme\": { \"color\": { \"global_bg\": \"#ffffff\", \"primary_bg\": \"#1a7197\", \"secondary_bg\": \"#e6e6e6\", \"accent_bg\": \"#e83185\", \"light_txt\": \"#e6e6e6\", \"dark_txt\": \"#000000\" }, \"font\": { \"primary\": \"System font\", \"secondary\": \"System font\" } } }',1,'2016-10-28 17:59:34','2016-10-28 17:59:34',NULL),
	(2,'anakin','','','/anakin.jpg','','[]','{ \"name\": \"ANAKIN_LAYOUT\", \"like\": false, \"share\": false,	\"filter\": false, \"sort\": false, \"page_size\": 10, \"auth\": { \"mode\": \"POPUP\", \"login\": \"SIMPLE\" }, \"theme\": { \"color\": { \"global_bg\": \"#ffffff\", \"primary_bg\": \"#3b3a36\", \"secondary_bg\": \"#b3c2bf\", \"accent_bg\": \"#e9ece5\", \"light_txt\": \"#ffffff\", \"dark_txt\": \"#3b3a36\" }, \"font\": { \"primary\": \"System font\", \"secondary\": \"System font\" } } }',1,'2016-10-28 18:00:41','2016-10-28 18:00:41',NULL),
	(3,'avenir','','','/avenir.jpg','','[]','{ \"name\": \"AVENIR_LAYOUT\", \"like\": false, \"share\": false,	\"filter\": false, \"sort\": false, \"page_size\": 10, \"auth\": { \"mode\": \"POPUP\", \"login\": \"SIMPLE\" }, \"theme\": { \"color\": { \"global_bg\": \"#e1e8f0\", \"primary_bg\": \"#6ed3cf\", \"secondary_bg\": \"#9068be\", \"accent_bg\": \"#e62739\", \"light_txt\": \"#e1e8f0\", \"dark_txt\": \"#9068be\" }, \"font\": { \"primary\": \"System font\", \"secondary\": \"System font\" } } }',1,'2016-10-28 18:00:42','2016-10-28 18:00:42',NULL),
	(4,'bbq','','','/bbq.jpg','','[]','{ \"name\": \"BBQ_LAYOUT\", \"like\": false, \"share\": false,	\"filter\": false, \"sort\": false, \"page_size\": 10, \"auth\": { \"mode\": \"POPUP\", \"login\": \"SIMPLE\" }, \"theme\": { \"color\": { \"global_bg\": \"#bcd5d1\", \"primary_bg\": \"#1d2120\", \"secondary_bg\": \"#5a5c51\", \"accent_bg\": \"#ba9077\", \"light_txt\": \"#bcd5d1\", \"dark_txt\": \"#1d2120\" }, \"font\": { \"primary\": \"System font\", \"secondary\": \"System font\" } } }',1,'2016-10-28 18:00:43','2016-10-28 18:00:43',NULL),
	(5,'ceramic','','','/ceramic.jpg','','[]','{ \"name\": \"CERAMIC_LAYOUT\", \"like\": false, \"share\": false,	\"filter\": false, \"sort\": false, \"page_size\": 10, \"auth\": { \"mode\": \"POPUP\", \"login\": \"SIMPLE\" }, \"theme\": { \"color\": { \"global_bg\": \"#f2efe8\", \"primary_bg\": \"#b0aac2\", \"secondary_bg\": \"#c2d4d8\", \"accent_bg\": \"#a6ada4\", \"light_txt\": \"#f2efe8\", \"dark_txt\": \"#777382\" }, \"font\": { \"primary\": \"System font\", \"secondary\": \"System font\" } } }',1,'2016-10-28 18:00:45','2016-10-28 18:00:45',NULL),
	(6,'denkwerk','','','/denkwerk.jpg','','[]','{ \"name\": \"DENKWERK_LAYOUT\", \"like\": false, \"share\": false,	\"filter\": false, \"sort\": false, \"page_size\": 10, \"auth\": { \"mode\": \"POPUP\", \"login\": \"SIMPLE\" }, \"theme\": { \"color\": { \"global_bg\": \"#feffff\", \"primary_bg\": \"#312c32\", \"secondary_bg\": \"#daad86\", \"accent_bg\": \"#98dafc\", \"light_txt\": \"#feffff\", \"dark_txt\": \"#312c32\" }, \"font\": { \"primary\": \"System font\", \"secondary\": \"System font\" } } }',1,'2016-10-28 18:00:46','2016-10-28 18:00:46',NULL),
	(7,'dream-team','','','/dream-team.jpg','','[]','{ \"name\": \"DREAMTEAM_LAYOUT\", \"like\": false, \"share\": false,	\"filter\": false, \"sort\": false, \"page_size\": 10, \"auth\": { \"mode\": \"POPUP\", \"login\": \"SIMPLE\" }, \"theme\": { \"color\": { \"global_bg\": \"#e3e3e3\", \"primary_bg\": \"#c9c9c9\", \"secondary_bg\": \"#9ad3de\", \"accent_bg\": \"#89bdd3\", \"light_txt\": \"#e3e3e3\", \"dark_txt\": \"#787878\" }, \"font\": { \"primary\": \"System font\", \"secondary\": \"System font\" } } }',1,'2016-10-28 18:00:48','2016-10-28 18:00:48',NULL),
	(8,'landscape','','','/landscape.jpg','','[]','{ \"name\": \"LANDSCAPE_LAYOUT\", \"like\": false, \"share\": false,	\"filter\": false, \"sort\": false, \"page_size\": 10, \"auth\": { \"mode\": \"POPUP\", \"login\": \"SIMPLE\" }, \"theme\": { \"color\": { \"global_bg\": \"#dddfd4\", \"primary_bg\": \"#173e43\", \"secondary_bg\": \"#3fb0ac\", \"accent_bg\": \"#173e43\", \"light_txt\": \"#dddfd4\", \"dark_txt\": \"#3fb0ac\" }, \"font\": { \"primary\": \"System font\", \"secondary\": \"System font\" } } }',1,'2016-10-28 18:00:50','2016-10-28 18:00:50',NULL),
	(9,'lookbook','','','/lookbook.jpg','','[]','{ \"name\": \"LOOKBOOK_LAYOUT\", \"like\": false, \"share\": false,	\"filter\": false, \"sort\": false, \"page_size\": 10, \"auth\": { \"mode\": \"POPUP\", \"login\": \"SIMPLE\" }, \"theme\": { \"color\": { \"global_bg\": \"#ffffff\", \"primary_bg\": \"#9fa8a3\", \"secondary_bg\": \"#c5d5cb\", \"accent_bg\": \"#9fa8a3\", \"light_txt\": \"#ffffff\", \"dark_txt\": \"#595959\" }, \"font\": { \"primary\": \"System font\", \"secondary\": \"System font\" } } }',1,'2016-10-28 18:00:53','2016-10-28 18:00:53',NULL),
	(10,'quary','','','/quary.jpg','','[]','{ \"name\": \"QUARY_LAYOUT\", \"like\": false, \"share\": false,	\"filter\": false, \"sort\": false, \"page_size\": 10, \"auth\": { \"mode\": \"POPUP\", \"login\": \"SIMPLE\" }, \"theme\": { \"color\": { \"global_bg\": \"#e8edf3\", \"primary_bg\": \"#22264b\", \"secondary_bg\": \"#b56969\", \"accent_bg\": \"#e6cf8b\", \"light_txt\": \"#e8edf3\", \"dark_txt\": \"#22264b\" }, \"font\": { \"primary\": \"System font\", \"secondary\": \"System font\" } } }',1,'2016-10-28 18:00:54','2016-10-28 18:00:54',NULL),
	(11,'relax','','','/relax.jpg','','[]','{ \"name\": \"RELAX_LAYOUT\", \"like\": false, \"share\": false,	\"filter\": false, \"sort\": false, \"page_size\": 10, \"auth\": { \"mode\": \"POPUP\", \"login\": \"SIMPLE\" }, \"theme\": { \"color\": { \"global_bg\": \"#ffffff\", \"primary_bg\": \"#7d4627\", \"secondary_bg\": \"#a8b6bf\", \"accent_bg\": \"#edd9c0\", \"light_txt\": \"#ffffff\",  \"dark_txt\": \"#7d4627\" }, \"font\": { \"primary\": \"System font\", \"secondary\": \"System font\" } } }',1,'2016-10-28 18:00:56','2016-10-28 18:00:56',NULL),
	(12,'sunglass','','','/sunglass.jpg','','[]','{ \"name\": \"SUNGLASSES_LAYOUT\", \"like\": false, \"share\": false,	\"filter\": false, \"sort\": false, \"page_size\": 10, \"auth\": { \"mode\": \"POPUP\", \"login\": \"SIMPLE\" }, \"theme\": { \"color\": { \"global_bg\": \"#f0eceb\", \"primary_bg\": \"#283018\", \"secondary_bg\": \"#aa863a\", \"accent_bg\": \"#729f98\", \"light_txt\": \"#f0eceb\", \"dark_txt\": \"#283018\" }, \"font\": { \"primary\": \"System font\", \"secondary\": \"System font\" } } }',1,'2016-10-28 18:00:57','2016-10-28 18:00:57',NULL);

/*!40000 ALTER TABLE `template` ENABLE KEYS */;
UNLOCK TABLES;