UPDATE `post` SET `type` = 'PERSON' WHERE `type` = 'ARTIST';
RENAME TABLE `post_artist` TO `post_person`;
UPDATE `section` SET `type` = 'PERSON' WHERE `type` = 'ARTIST';
UPDATE `section` SET `params` = '{"types":["EVENT","PERSON"]}' WHERE `params` = '{"types":["EVENT","ARTIST"]}';
UPDATE `section` SET `params` = '{"types":["PERSON"]}' WHERE `params` = '{"types":["ARTIST"]}';

CREATE TABLE `post_poi` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) unsigned DEFAULT NULL,
  `identifier` varchar(20) NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `lat` float(10,6) DEFAULT NULL,
  `lng` float(10,6) DEFAULT NULL,
  `radius` int(11) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `major` tinyint(3) unsigned DEFAULT NULL,
  `minor` tinyint(3) unsigned DEFAULT NULL,
  `distance` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_identifier` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE post ADD template_id INT NOT NULL AFTER search;
ALTER TABLE section ADD template_id INT NOT NULL AFTER params;

