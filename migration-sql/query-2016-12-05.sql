
/* RUN ON EVERY HP APP DATABASE */
CREATE TABLE `gallery_form` (
  `media_id` int(11) DEFAULT NULL,
  `form_id` int(11) unsigned DEFAULT NULL,
  `row_id` int(11) DEFAULT NULL,
  `field_name` varchar(1024) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  KEY `media_id` (`media_id`),
  KEY `form_id` (`form_id`),
  CONSTRAINT `gallery_form_ibfk_1` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `gallery_form_ibfk_2` FOREIGN KEY (`form_id`) REFERENCES `section` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;