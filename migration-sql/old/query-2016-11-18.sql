/* RUN ON HP_CORE DATABASE */
ALTER TABLE `application` ADD `max_notification`  INT(11)  NULL  DEFAULT '0'  AFTER `template_id`;
ALTER TABLE `application` ADD `sent_notification` INT(11)  NULL  DEFAULT '0'  AFTER `max_notification`;


/* RUN ON EVERY HP APP DATABASE */
CREATE TABLE `notification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `to` text,
  `title` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `total_receiver` int(11) unsigned DEFAULT NULL,
  `count_click` int(11) unsigned DEFAULT NULL,
  `action` varchar(20) NOT NULL,
  `related_action_id` int(11) unsigned DEFAULT NULL,
  `scheduled_at` datetime NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `device` DROP FOREIGN KEY `FK_E83B3B8A76ED395`;
ALTER TABLE `user` DROP FOREIGN KEY `FK_FCB96C9EF5B7AF75`;
ALTER TABLE `user` ENGINE = MyISAM;

/******************* IMPORTANT *******************/
/* remember to call -> /admin/user/tags/restore/
/* to restore users tags (originally store
/* as comma separeted ids list and now required
/* as strings list
/******************* IMPORTANT *******************/
