/* RUN ON EVERY HP APP DATABASE */

ALTER TABLE `post_poi` ADD `created_at` DATETIME  NULL;
ALTER TABLE `post_poi` ADD `updated_at` DATETIME  NULL;
ALTER TABLE `post_poi` ADD `deleted_at` DATETIME  NULL;

ALTER TABLE `post_poi` CHANGE `radius` `radius` INT(11)  NULL  DEFAULT NULL;
ALTER TABLE `post_poi` CHANGE `major` `major` TINYINT(3)  UNSIGNED  NULL  DEFAULT NULL;
ALTER TABLE `post_poi` CHANGE `minor` `minor` TINYINT(3)  UNSIGNED  NULL  DEFAULT NULL;

