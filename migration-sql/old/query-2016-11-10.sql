CREATE TABLE `remote_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `connector_id` int(11) DEFAULT NULL,
  `remote_id` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `content_type` varchar(10) DEFAULT NULL,
  `liked_at` datetime DEFAULT NULL,
  `shared_at` datetime DEFAULT NULL,
  `share_count` int(11) DEFAULT '0',
  `view_count` int(11) DEFAULT '0',
  `like_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_id` (`connector_id`,`remote_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `remote_extra_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `connector_id` int(11) DEFAULT NULL,
  `remote_id` varchar(255) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL COMMENT 'Valid type string: VIEW, LIKE, UNLIKE, SHARE',
  `content_type` varchar(10) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `content_id` (`connector_id`,`remote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;