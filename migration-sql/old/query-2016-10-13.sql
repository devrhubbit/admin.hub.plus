

/* RUN ON EVERY HP APP DATABASE */
ALTER TABLE post_poi ADD identifier VARCHAR(20) NOT NULL AFTER post_id;
ALTER TABLE post_poi ADD CONSTRAINT unique_identifier UNIQUE (identifier);