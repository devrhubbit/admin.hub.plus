/* RUN ON HP_CORE DATABASE */
DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'category unique identifier',
  `name` varchar(255) DEFAULT NULL COMMENT 'category name',
  `weight` int(11) DEFAULT NULL COMMENT 'ordering priority weight',
  `visible` tinyint(1) DEFAULT '1' COMMENT 'TRUE if category is visible in category list',
  `setting` text COMMENT 'json with enabled section per category',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;

INSERT INTO `category` (`id`, `name`, `weight`, `visible`, `setting`)
VALUES
  (1,'Event and Exhibitions',1,1,'{ \"section_types\": [\"EVENT\", \"PERSON\", \"POI\"] , \"content_types\": [\"EVENT\", \"PERSON\", \"POI\"]}'),
  (2,'Store and Retail',2,1,'{ \"section_types\": [\"EVENT\", \"PERSON\", \"POI\"] , \"content_types\": [\"EVENT\", \"PERSON\", \"POI\"]}'),
  (3,'Musem and Art Gallery',3,1,'{ \"section_types\": [\"EVENT\", \"PERSON\", \"POI\"] , \"content_types\": [\"EVENT\", \"PERSON\", \"POI\"]}'),
  (4,'Sport & Welness',4,1,'{ \"section_types\": [\"EVENT\", \"PERSON\", \"POI\"] , \"content_types\": [\"EVENT\", \"PERSON\", \"POI\"]}'),
  (5,'Tourism',5,1,'{ \"section_types\": [\"EVENT\", \"POI\"] , \"content_types\": [\"EVENT\", \"POI\"]}');

/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;







/* RUN ON EVERY HP APP DATABASE */
UPDATE `post` SET `type` = 'PERSON' WHERE `type` = 'ARTIST';
RENAME TABLE `post_artist` TO `post_person`;
UPDATE `section` SET `type` = 'PERSON' WHERE `type` = 'ARTIST';
UPDATE `section` SET `params` = '{"types":["EVENT","PERSON"]}' WHERE `params` = '{"types":["EVENT","ARTIST"]}';
UPDATE `section` SET `params` = '{"types":["PERSON"]}' WHERE `params` = '{"types":["ARTIST"]}';
