/* RUN ON EVERY HP APP DATABASE */
CREATE TABLE `post_poi` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) unsigned DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `lat` float(10,6) DEFAULT NULL,
  `lng` float(10,6) DEFAULT NULL,
  `radius` tinyint(3) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `major` tinyint(3) DEFAULT NULL,
  `minor` tinyint(3) DEFAULT NULL,
  `distance` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;