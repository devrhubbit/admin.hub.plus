<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 12/06/17
 * Time: 18:08
 */

return array(
//    'debug' => true,
//    'dev'   => false,
    'controller-with-no-auth' => array(
        'Application\Controller\Index',
        'Application\Controller\Ajax',
        'Application\Controller\SuperadminData',
        'Application\Controller\Remote',
        'Rest\Controller\Error',
        'Rest\Controller\Identity',
        'Rest\Controller\Page',
        'Rest\Controller\Media',
        'Rest\Controller\Tracker',
    ),
    'status_code' => array(
        '100' => 'Continue',
        '101' => 'Switching Protocols',
        '200' => 'OK',                                // Risposta standard per le richieste HTTP andate a buon fine.
        '201' => 'Created',
        '202' => 'Accepted',
        '203' => 'Non-Authoritative Information',
        '204' => 'No Content',
        '205' => 'Reset Content',
        '206' => 'Partial Content',
        '207' => 'Multi-Status',                    // In caso di risposte XML, quando più azioni possono essere richieste, i dettagli dei singoli stati sono dati nel corpo della risposta.
        // Vedi WebDAV (RFC 4918) per le specifiche associate.
        '300' => 'Multiple Choices',
        '301' => 'Moved Permanently',                // Questa e tutte le future richieste andranno dirette ad un altro URI (specificato nell'header Location).
        '302' => 'Found',                            // Questo è il codice più usato ma anche un classico esempio di non aderenza agli standard nella pratica quotidiana.
        // Infatti, le specifiche di HTTP/1.0 (RFC 1945) richiederebbero che il client esegua redirezioni temporanee
        // (la descrizione originale era "Moved Temporarily"), ma i più diffusi browser l'hanno implementata come 303
        // descritta di seguito. Perciò, HTTP/1.1 ha aggiunto i codici di stato 303 e 307 per distinguere tra i due comportamenti.
        // Comunque, la maggior parte delle applicazioni e dei framework web ancora usano il codice di stato 302 come se fosse il 303.
        '303' => 'See Other (da HTTP/1.1)',            // La risposta alla richiesta può essere trovata sotto un'altra URI usando il metodo GET.
        '304' => 'Not Modified',
        '305' => 'Use Proxy (da HTTP/1.1)',        // Molti client HTTP (come Mozilla ed Internet Explorer) non gestiscono correttamente le risposte con questo codice di stato.
        '306' => 'Switch Proxy',                    // Non più usato.
        '307' => 'Temporary Redirect (da HTTP/1.1)',// In quest'occasione, la richiesta dovrebbe essere ripetuta con un'altra URI, ma successive richieste possono essere ancora
        // dirette a quella originale. In contrasto con 303, la richiesta di POST originale deve essere reiterata con un'altra richiesta di tipo POST.
        '308' => 'Permanent Redirect',                // Questa richiesta e le future dovrebbero essere fatte verso un altro URI. Le risposte 307 e 308 (come proposta) dovrebbero
        // comportarsi similmente alla 302 e la 301, ma non prevedono un cambiamento di metodo.
        '400' => 'Bad Request',                    // La richiesta non può essere soddisfatta a causa di errori di sintassi.
        '401' => 'Unauthorized',                    // Simile a 403/Forbidden, ma pensato per essere usato quando l'autenticazione è possibile ma è fallita o non può essere fornita.
        // Vedi anche basic access authentication e digest access authentication.
        '402' => 'Payment Required',                // L'intendimento originale prevedeva un suo utilizzo per realizzare meccanismi di digital cash/micropagamento, ma questo non si
        // è mai verificato ed il codice non è mai stato utilizzato.
        '403' => 'Forbidden',                        // La richiesta è legittima ma il server si rifiuta di soddisfarla. Contrariamente al codice 401 Unauthorized, l'autenticazione non ha effetto.
        '404' => 'Not Found',                        // La risorsa richiesta non è stata trovata ma in futuro potrebbe essere disponibile.
        '405' => 'Method Not Allowed',                // La richiesta è stata eseguita usando un metodo non permesso. Ad esempio questo accade quando di usa il metodo GET per
        // inviare dati da presentare con un metodo POST.
        '406' => 'Not Acceptable',                    // La risorsa richiesta è solo in grado di generare contenuti non accettabili secondo la header Accept inviato nella richiesta.[2]
        '407' => 'Proxy Authentication Required',
        '408' => 'Request Timeout',                    // Il tempo per inviare la richiesta è scaduto e il server ha terminato la connessione.
        '409' => 'Conflict',
        '410' => 'Gone',                            // Indica che la risorsa richiesta non è più disponibile e non lo sarà più in futuro.
        '411' => 'Length Required',                    // La richiesta non specifica la propria dimensione come richiesto dalla risorsa richiesta.
        '412' => 'Precondition Failed',
        '413' => 'Request Entity Too Large',        // La richiesta è più grande di quanto il server possa gestire.
        '414' => 'Request-URI Too Long',            // L'URI richiesto è troppo grande per essere processato dal server.
        '415' => 'Unsupported Media Type',            // L'entità della richiesta è di un tipo non accettato dal server o dalla risorsa richiesta.
        '416' => 'Requested Range Not Satisfiable',
        '417' => 'Expectation Failed',
        '418' => 'I\'m a teapot',                    // Questo è un tipico pesce d'aprile dell'IETF (RFC 2324). Non si aspettano implementazioni in alcun server HTTP.
        '426' => 'Upgrade Required (RFC 2817)',        // Il client dovrebbe cambiare il protocollo ed usare ad esempio il TLS/1.0.
        '451' => 'Unavailable For Legal Reasons',    // Stato non obbligatorio utilizzato quando l'accesso alla risorsa è limitato per ragioni legali come censura o mandati governativi.
        '500' => 'Internal Server Error',            // Messaggio di errore generico senza alcun dettaglio.
        '501' => 'Not Implemented',                    // Il server non è in grado di soddisfare il metodo della richiesta.
        '502' => 'Bad Gateway',
        '503' => 'Trigger Unavailable',                // Il server non è al momento disponibile. Generalmente è una condizione temporanea.
        '504' => 'Gateway Timeout',
        '505' => 'HTTP Version Not Supported',        // Il server non supporta la versione HTTP della richiesta.
        '509' => 'Bandwidth Limit Exceeded',        // Questo codice di stato, benchè usato da molti server, non è un codice di stato ufficiale in quanto non è specificato in alcuna RFC.'
    ),

    'section_types' => array(
        "EVENT" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/event", "class" => "btn-event-section", "label" => "EVENTS LIST", "icon" => "fa-calendar-o", "api_version" => "1.0.0"],
        "PERSON" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/person", "class" => "btn-person-section", "label" => "PEOPLE LIST", "icon" => "fa-users", "api_version" => "1.0.0"],
        "POI" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/poi", "class" => "btn-poi-section", "label" => "POI LIST", "icon" => "fa-map-marker", "api_version" => "1.0.0"],
        "GENERIC" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/generic", "class" => "btn-generic-section", "label" => "GENERIC LIST", "icon" => "fa-file", "api_version" => "1.0.0"],
        "WEB" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/web", "class" => "btn-web-section", "label" => "WEB LIST", "icon" => "fa-globe", "api_version" => "1.2.0"],
        "SECTION" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/section", "class" => "btn-section-section", "label" => "SECTION LIST", "icon" => "fa-puzzle-piece", "api_version" => "1.1.0"],
        "DEV" => ["only_dev" => true, "enabled" => true, "active" => false, "url" => "/admin/section/dev", "class" => "btn-section-dev", "label" => "DEV SECTION", "icon" => "fa-cog", "api_version" => "1.3.0"],
        "QR_CODE" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/qr_code", "class" => "btn-section-qr-code", "label" => "QR CODE SCANNER", "icon" => "fa-qrcode", "api_version" => "1.2.0"],
        "USER" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/user", "class" => "btn-section-user", "label" => "USER LIST", "icon" => "fa-external-link", "api_version" => "1.2.0"],
        "FAVOURITE" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/favourite", "class" => "btn-favourite-section", "label" => "FAVOURITES", "icon" => "fa-star", "api_version" => "1.0.0"],
        "RSS" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/rss", "class" => "btn-rss-section", "label" => "RSS FEED", "icon" => "fa-rss", "api_version" => "1.0.0"],
        "PODCAST" => ["only_dev" => true, "enabled" => true, "active" => false, "url" => "/admin/section/podcast", "class" => "btn-podcast-section", "label" => "PODCAST", "icon" => "fa-podcast", "api_version" => "1.0.0"],
        "TWITTER" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/twitter", "class" => "btn-twitter-section", "label" => "TWITTER FEED", "icon" => "fa-twitter", "api_version" => "1.0.0"],
        "INFO" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/info", "class" => "btn-info-section", "label" => "INFO PAGE", "icon" => "fa-info", "api_version" => "1.0.0"],
        "CUSTOM_FORM" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/section/custom_form", "class" => "btn-custom_form-section", "label" => "FORM", "icon" => "fa-edit", "api_version" => "1.1.0"],
    ),
    'list_cell_types' => array(
        "LEFT_LAYOUT" => ["only_dev" => false, "default" => true, "value" => "LEFT_LAYOUT", "label" => "LEFT LAYOUT", "icon" => "/img/icon/section/section-left-layout.png", "cover_format" => "512x512",
            "types" => array("RSS", "PODCAST", "TWITTER", "FAVOURITE", "EVENT", "PERSON", "POI", "GENERIC", "WEB", "SECTION", "USER")],

        "RIGHT_LAYOUT" => ["only_dev" => false, "default" => false, "value" => "RIGHT_LAYOUT", "label" => "RIGHT LAYOUT", "icon" => "/img/icon/section/section-right-layout.png", "cover_format" => "512x512",
            "types" => array("RSS", "PODCAST", "FAVOURITE", "EVENT", "PERSON", "POI", "GENERIC", "WEB", "SECTION", "USER")],

        "CARD_LAYOUT" => ["only_dev" => false, "default" => false, "value" => "CARD_LAYOUT", "label" => "CARD LAYOUT", "icon" => "/img/icon/section/section-card-layout.png", "cover_format" => "1280x720",
            "types" => array("RSS", "PODCAST", "FAVOURITE", "EVENT", "PERSON", "POI", "GENERIC", "WEB", "SECTION", "USER")],

        "GRID_LAYOUT" => ["only_dev" => false, "default" => false, "value" => "GRID_LAYOUT", "label" => "GRID LAYOUT", "icon" => "/img/icon/section/section-grid-layout.png", "cover_format" => "512x512",
            "types" => array("RSS", "PODCAST", "FAVOURITE", "EVENT", "PERSON", "POI", "GENERIC", "WEB", "SECTION", "USER")],

        "FORM_LEFT_LAYOUT" => ["only_dev" => false, "default" => false, "value" => "FORM_LEFT_LAYOUT", "label" => "LEFT LAYOUT", "icon" => "/img/icon/section/section-form-left.png", "cover_format" => "512x512",
            "types" => array("CUSTOM_FORM")],

//        "FORM_RIGHT_LAYOUT" => ["only_dev" => false, "default" => false, "value" => "FORM_RIGHT_LAYOUT", "label" => "RIGHT LAYOUT", "icon" => "/img/icon/section/section-form-right.png", "cover_format" => "512x512",
//            "types" => array("CUSTOM_FORM")],

        "FORM_PAGE_LAYOUT" => ["only_dev" => false, "default" => false, "value" => "FORM_PAGE_LAYOUT", "label" => "WIZARD LAYOUT", "icon" => "/img/icon/section/section-form-step.png", "cover_format" => "512x512",
            "types" => array("CUSTOM_FORM")],

        "INFO_LAYOUT" => ["only_dev" => false, "default" => false, "value" => "INFO_LAYOUT", "label" => "INFO LAYOUT", "icon" => "/img/icon/section/section-info-layout.png", "cover_format" => "1280x720",
            "types" => array("INFO")],

        "MAP_LAYOUT" => ["only_dev" => false, "default" => false, "value" => "MAP_LAYOUT", "label" => "MAP LAYOUT", "icon" => "/img/icon/section/section-map-layout.png", "cover_format" => null,
            "types" => array("POI")],
    ),

    'content_types' => array(
        "EVENT" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/content/event", "class" => "btn-event-section", "label" => "EVENT", "icon" => "fa-calendar-o", "api_version" => "1.0.0"],
        "PERSON" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/content/person", "class" => "btn-person-section", "label" => "PERSON", "icon" => "fa-user", "api_version" => "1.0.0"],
        "POI" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/content/poi", "class" => "btn-poi-section", "label" => "POI", "icon" => "fa-map-marker", "api_version" => "1.0.0"],
        "GENERIC" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/content/generic", "class" => "btn-generic-section", "label" => "GENERIC", "icon" => "fa-file", "api_version" => "1.0.0"],
        "WEB" => ["only_dev" => false, "enabled" => true, "active" => false, "url" => "/admin/content/web", "class" => "btn-web-section", "label" => "WEB", "icon" => "fa-globe", "api_version" => "1.2.0"],
    ),
    'list_page_types' => array(
        "SLIM_LAYOUT" => ["only_dev" => false, "default" => true, "value" => "SLIM_LAYOUT", "label" => "SLIM HEADER", "icon" => "/img/icon/content/content-slim-layout.png", "cover_format" => "512x512",
            "types" => array("EVENT", "PERSON", "POI", "GENERIC", "WEB", "USER")],

        "SLIM_LAYOUT_GALLERY" => ["only_dev" => false, "default" => false, "value" => "SLIM_LAYOUT_GALLERY", "label" => "SLIM GALLERY HEADER", "icon" => "/img/icon/content/content-slim-layout-gallery.png", "cover_format" => "512x512",
            "types" => array("EVENT", "PERSON", "POI", "GENERIC", "WEB", "USER")],

        "MEDIUM_LAYOUT" => ["only_dev" => false, "default" => false, "value" => "MEDIUM_LAYOUT", "label" => "HEADER (1:1)", "icon" => "/img/icon/content/content-medium-layout.png", "cover_format" => "1280x1280",
            "types" => array("EVENT", "PERSON", "POI", "GENERIC", "WEB", "USER")],

        "MEDIUM_16_9_LAYOUT" => ["only_dev" => false, "default" => false, "value" => "MEDIUM_16_9_LAYOUT", "label" => "HEADER (16:9)", "icon" => "/img/icon/content/content-medium-169-layout.png", "cover_format" => "1280x720",
            "types" => array("EVENT", "PERSON", "POI", "GENERIC", "WEB", "USER")],

        "LARGE_LAYOUT" => ["only_dev" => false, "default" => false, "value" => "LARGE_LAYOUT", "label" => "HEADER (3:4)", "icon" => "/img/icon/content/content-large-layout.png", "cover_format" => "1280x1707",
            "types" => array("EVENT", "PERSON", "POI", "GENERIC", "WEB", "USER")],

        "LARGE_LAYOUT_GALLERY" => ["only_dev" => false, "default" => false, "value" => "LARGE_LAYOUT_GALLERY", "label" => "LARGE GALLERY (auto)", "icon" => "/img/icon/content/content-large-layout-gallery.png", "cover_format" => "1280x1707",
            "types" => array("EVENT", "PERSON", "POI", "GENERIC", "WEB", "USER")],
    ),

    'font_list' => array(
        'System font' => 'System font',
        'Arial' => 'Arial',
        'Verdana' => 'Verdana',
        'Test font' => 'Test font'
    ),
    'list_template_types' => array(
        "TAB_PAGE" => ["only_dev" => false, "default" => true, "value" => "TAB_PAGE", "label" => "TAB PAGE", "icon" => "/img/icon/main/main-tab-page-layout.png",
            "types" => array("APP")],

        "GRID_PAGE" => ["only_dev" => false, "default" => false, "value" => "GRID_PAGE", "label" => "GRID PAGE", "icon" => "/img/icon/main/main-grid-page-layout.png",
            "types" => array("APP")],
    ),

    'remote_sections' => array(
        "EVENT",
        "PERSON",
        "POI",
        "GENERIC",
        "SECTION",
        "CUSTOM_FORM",
    ),

    'custom_form_types' => array(
        "INPUT_TEXT" => ["selected" => true, "value" => "INPUT_TEXT", "label" => "Short Text"],
        "TEXTAREA" => ["selected" => false, "value" => "TEXTAREA", "label" => "Long Text"],
        "DESCRIPTION" => ["selected" => false, "value" => "DESCRIPTION", "label" => "Description"],
        "IMAGE" => ["selected" => false, "value" => "IMAGE", "label" => "Image"],
        "CHECKBOX" => ["selected" => false, "value" => "CHECKBOX", "label" => "Checkbox"],
        "RADIOBOX" => ["selected" => false, "value" => "RADIOBOX", "label" => "Radio Box"],
        "SELECT" => ["selected" => false, "value" => "SELECT", "label" => "Select"],
        "ADDRESS" => ["selected" => false, "value" => "ADDRESS", "label" => "Address"],
        "TAG_GROUP" => ["selected" => false, "value" => "TAG_GROUP", "label" => "Tag Group"],
    ),
    'custom_form_validators' => array(
        "SINGLE" => ["selected" => true, "value" => "SINGLE", "label" => "Single Image", "types" => array("IMAGE")],
        "GALLERY" => ["selected" => false, "value" => "GALLERY", "label" => "Gallery of Images", "types" => array("IMAGE")],
        "NONE" => ["selected" => true, "value" => "NONE", "label" => "No Validators", "types" => array("INPUT_TEXT")],
        "VALID_EMAIL" => ["selected" => false, "value" => "VALID_EMAIL", "label" => "Valid Email Address", "types" => array("INPUT_TEXT")],
        "VALID_URL" => ["selected" => false, "value" => "VALID_URL", "label" => "Valid Url", "types" => array("INPUT_TEXT")],
        "VALID_PHONE" => ["selected" => false, "value" => "VALID_PHONE", "label" => "Valid Phone Number", "types" => array("INPUT_TEXT")],
        "NUMERIC" => ["selected" => false, "value" => "NUMERIC", "label" => "Numeric", "types" => array("INPUT_TEXT")],
        "TAG_SINGLE" => ["selected" => true, "value" => "TAG_SINGLE", "label" => "Single Tag", "types" => array("TAG_GROUP")],
        "TAG_MULTIPLE" => ["selected" => false, "value" => "TAG_MULTIPLE", "label" => "Multiple Tags", "types" => array("TAG_GROUP")],
    ),
    'facebook_fields' => array(
        ["value" => "-", "label" => "-"],
//		["value" => "id", 			"label" => "User identifier"],
        ["value" => "email", "label" => "User email"],
        ["value" => "cover", "label" => "User cover"],
        ["value" => "name", "label" => "User nickname"],
        ["value" => "first_name", "label" => "User first name"],
        ["value" => "last_name", "label" => "User last name"],
        ["value" => "age_range", "label" => "User age range"],
        ["value" => "link", "label" => "User site link"],
        ["value" => "gender", "label" => "User gender"],
        ["value" => "locale", "label" => "User language"],
        ["value" => "picture", "label" => "User profile image"],
        ["value" => "timezone", "label" => "User timezione"]
    ),

    'extra_app_icons' => array(
        'BACK' => array('value' => 'app_back_icon', 'label' => 'Back Icon', 'type' => 'GENERAL'),
        'GALLERY' => array('value' => 'app_gallery_icon', 'label' => 'Gallery Icon', 'type' => 'GENERAL'),
        'SHARE' => array('value' => 'app_share_icon', 'label' => 'Share Icon', 'type' => 'GENERAL'),
        'LIKE' => array('value' => 'app_like_icon', 'label' => 'Like Icon', 'type' => 'GENERAL'),
        'SEARCH' => array('value' => 'app_search_icon', 'label' => 'Search Icon', 'type' => 'GENERAL'),
        'CLOSE_ICON' => array('value' => 'close_icon', 'label' => 'Close Icon (X)', 'type' => 'GENERAL'),
        'MORE' => array('value' => 'app_more_icon', 'label' => 'More Icon', 'type' => 'GENERAL'),
        'MENU' => array('value' => 'app_menu_icon', 'label' => 'Menu Icon', 'type' => 'GENERAL'),
        'ANDROID_NOTIFICATION' => array('value' => 'app_android_notification_icon', 'label' => 'Android Notifications', 'type' => 'GENERAL'),
        'IN_APP_NOTIFICATION' => array('value' => 'app_in_app_notification_icon', 'label' => 'In App Notification Icon', 'type' => 'GENERAL'),
        'YOUTUBE' => array('value' => 'youtube_icon', 'label' => 'Youtube Icon', 'type' => 'REMOTE'),
        'VIMEO' => array('value' => 'vimeo_icon', 'label' => 'Vimeo Icon', 'type' => 'REMOTE'),
        'DAILYMOTION' => array('value' => 'dailymotion_icon', 'label' => 'Dailymotion Icon', 'type' => 'REMOTE'),
        'FACEBOOK_VIDEO' => array('value' => 'facebook_video_icon', 'label' => 'Facebook Video Icon', 'type' => 'REMOTE'),
        'MATTERPORT' => array('value' => 'matterport_icon', 'label' => 'Matterport Icon', 'type' => 'REMOTE'),
        'ISSUU' => array('value' => 'issuu_icon', 'label' => 'Issuu Icon', 'type' => 'REMOTE'),
        'SLIDE_SHARE' => array('value' => 'slide_share_icon', 'label' => 'Slide Share Icon', 'type' => 'REMOTE'),
    ),

    'user_section_icons_position' => array(
        'FOOTER' => array('value' => 'FOOTER', 'label' => 'Footer'),
        'HEADER' => array('value' => 'HEADER', 'label' => 'Header'),
        'FLOAT' => array('value' => 'FLOAT', 'label' => 'Float'),
    ),

    'custom_form_fields_map' => array(
        'title' => array('value' => 'Title', 'unique' => false, 'required' => true),
        'subtitle' => array('value' => 'Subtitle', 'unique' => true, 'required' => false),
        'body_text' => array('value' => 'Body-Text', 'unique' => false, 'required' => false),
        'body_table' => array('value' => 'Body-Table', 'unique' => false, 'required' => false),
        'cover' => array('value' => 'Cover', 'unique' => true, 'required' => false),
        'gallery' => array('value' => 'Gallery', 'unique' => false, 'required' => false),
    ),

    'user_section_content_filter' => array(
        'OTHER_USER' => array('value' => 'OTHER_USER', 'label' => 'Other user'),
        'LOGGED_USER' => array('value' => 'LOGGED_USER', 'label' => 'Logged user'),
        'ALL_USER' => array('value' => 'ALL_USER', 'label' => 'All user'),
    ),

    'controller_uri' => array(
        'ios' => array(
            "apple-uri-detail-large" => ["controller_name" => "hub.plus/detail/large", "label" => "Large detail", "placeholder" => "Override detail large controller"],
            "apple-uri-detail-medium" => ["controller_name" => "hub.plus/detail/medium", "label" => "Medium detail", "placeholder" => "Override detail medium controller"],
            "apple-uri-detail-slim" => ["controller_name" => "hub.plus/detail/slim", "label" => "Slim detail", "placeholder" => "Override detail slim controller"],
            "apple-uri-form" => ["controller_name" => "hub.plus/form", "label" => "Form", "placeholder" => "Override form controller"],
            "apple-uri-info" => ["controller_name" => "hub.plus/info", "label" => "Info", "placeholder" => "Override info controller"],
            "apple-uri-list-person" => ["controller_name" => "hub.plus/list/person", "label" => "Person list", "placeholder" => "Override person list controller"],
            "apple-uri-list-poi" => ["controller_name" => "hub.plus/list/poi", "label" => "POI list", "placeholder" => "Override POI list controller"],
            "apple-uri-list-event" => ["controller_name" => "hub.plus/list/event", "label" => "Event list", "placeholder" => "Override event list controller"],
            "apple-uri-list-generic" => ["controller_name" => "hub.plus/list/generic", "label" => "Generic list", "placeholder" => "Override generic list controller"],
            "apple-uri-login" => ["controller_name" => "hub.plus/login", "label" => "Login", "placeholder" => "Override login controller"],
            "apple-uri-rss" => ["controller_name" => "hub.plus/rss", "label" => "RSS", "placeholder" => "Override rss controller"],
            "apple-uri-storyboard" => ["controller_name" => "storyboard", "label" => "Storyboard", "placeholder" => "Override storyboard controller"],
        ),
        'android' => array(
            "google-uri-detail-large" => ["controller_name" => "hub.plus/detail/large", "label" => "Large detail", "placeholder" => "Override detail large controller"],
            "google-uri-detail-medium" => ["controller_name" => "hub.plus/detail/medium", "label" => "Medium detail", "placeholder" => "Override detail medium controller"],
            "google-uri-detail-slim" => ["controller_name" => "hub.plus/detail/slim", "label" => "Slim detail", "placeholder" => "Override detail slim controller"],
            "google-uri-form" => ["controller_name" => "hub.plus/form", "label" => "Form", "placeholder" => "Override form controller"],
            "google-uri-info" => ["controller_name" => "hub.plus/info", "label" => "Info", "placeholder" => "Override info controller"],
            "google-uri-list-person" => ["controller_name" => "hub.plus/list/person", "label" => "Person list", "placeholder" => "Override person list controller"],
            "google-uri-list-poi" => ["controller_name" => "hub.plus/list/poi", "label" => "POI list", "placeholder" => "Override POI list controller"],
            "google-uri-list-event" => ["controller_name" => "hub.plus/list/event", "label" => "Event list", "placeholder" => "Override event list controller"],
            "google-uri-list-generic" => ["controller_name" => "hub.plus/list/generic", "label" => "Generic list", "placeholder" => "Override generic list controller"],
            "google-uri-login" => ["controller_name" => "hub.plus/login", "label" => "Login", "placeholder" => "Override login controller"],
            "google-uri-rss" => ["controller_name" => "hub.plus/rss", "label" => "RSS", "placeholder" => "Override rss controller"],
        ),
    ),

    'resthook_valid_type' => array(
        \Rest\Model\HpRestModel::SCT_HASH_PREFIX => array('user', 'form', 'section'),
        \Rest\Model\HpRestModel::CNT_HASH_PREFIX => array('content'),
    ),

    'system_languages' => array(
        'en' => ['label' => 'English', 'icon' => 'flag-icon flag-icon-gb'],
        'fr' => ['label' => 'French', 'icon' => 'flag-icon flag-icon-fr'],
        'de' => ['label' => 'German', 'icon' => 'flag-icon flag-icon-de'],
        'it' => ['label' => 'Italian', 'icon' => 'flag-icon flag-icon-it'],
        'pt' => ['label' => 'Portuguese', 'icon' => 'flag-icon flag-icon-pt'],
        'es' => ['label' => 'Spanish', 'icon' => 'flag-icon flag-icon-es'],
    ),
);