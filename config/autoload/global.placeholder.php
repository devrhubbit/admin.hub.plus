<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'debug' => false,
    'dev' => false,
    'version' => '^#VERSION#^',
    'bundle_root' => '^#BUNDLE_ROOT#^',
    'backend_name' => '^#BACKEND_NAME#^',
    'log_files' => array(
        'mail' => '^#MAIL_LOG_PATH#^'
    ),
    'cdn' => array(
        'baseurl' => '^#CDN_BASE_URL#^',
        'basepath' => '^#CDN_BASE_PATH#^',
    ),
    'google-maps' => array(
        'key' => '^#GMAP_KEY#^',
    ),
    'google-analytics' => array(
        'ua' => 'UA-79615533-2',
        'ua-app' => '^#UA_APP#^'
    ),
    'database' => array(
        'core' => array(
            'dsn' => 'mysql:host=^#DB_HOST#^;dbname=^#DB_NAME#^;charset=utf8',
            'usr' => '^#DB_USER#^',
            'pwd' => '^#DB_PASSWORD#^',
            'db_name' => '^#DB_NAME#^',
        ),
        'logfilepath' => '/var/www/logdev.hub.plus/^#DB_NAME#^-database.log',
        'maxPerPage' => 10,
    ),
    'view_manager' => array(
        'base_path' => '^#BASE_PATH#^',
    ),
    'push-notification-file-path' => '/var/www/push-notification-certs',
    'hub_plus_store_links' => array(
        'app_store' => 'https://itunes.apple.com/us/app/hub-plus/id1187270256?l=it&ls=1&mt=8',
        'play_store' => 'https://play.google.com/store/apps/details?id=plus.hub.app.tester'
    ),
);