<?php
/**
 * Created by PhpStorm.
 * User: serpe
 * Date: 19/05/17
 * Time: 14:16
 */

return array(
    'acmailer_options' => [
        // Default mail service
        'default' => [
            'mail_adapter' => 'Zend\Mail\Transport\Smtp',
            'transport' => 'Zend\Mail\Transport\Smtp',

            'message_options' => [],

            'smtp_options' => [
                'host' => 'smtps.aruba.it',
                'port' => 465,
                'connection_class' => 'login', // The value can be one of 'smtp', 'plain', 'login' or 'crammd5'
                'connection_config' => [
                    'username' => 'info@hub.plus',
                    'password' => 'giovannimirco',
                    'ssl' => 'ssl', // The value can be one of 'ssl', 'tls' or null
                ],
            ],

            'file_options' => [],
        ],
    ],
    'sms_message' => array(
        'sender.sms' => 'Info',
        'limit.max.tries' => 5,
        'limit.push.default' => 10,
        'log_file' => '/var/log/cli/%s/%s/%s/sms.log',
        'smsapi.user' => 'username',
        'smsapi.password' => 'password',
    ),
    'email_message' => array(
        'sender.email' => 'info@hub.plus',
        'sender.alias' => 'HUB+',
        'sender.label' => 'HUB+',
        'limit.max.tries' => 5,
        'limit.push.default' => 10,
        'log_file' => '/var/log/cli/%s/%s/%s/mail.log',
        'defaultTemplate' => 'email/only-text',
    ),
    'push_message' => array(
        'limit.max.tries' => 5,
        'limit.push.default' => 10,
        'ios.certificate_path' => '',
        'ios.certificate_pass_phrase' => '',
        'android.api_key' => '',
        'log_file' => '/var/log/cli/%s/%s/%s/push.log',
    ),
);