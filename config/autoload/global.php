<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'debug' => false,
    'dev' => false,
    'version' => '1.0.0',
    'bundle_root' => 'plus.hub.',
    'backend_name' => 'HUB.plus',
    'log_files' => array(
        'mail' => '/var/www/logdev.hub.plus/mail.log'
    ),
    'cdn' => array(
        'baseurl' => 'https://cdndev.hub.plus',
        'basepath' => '/var/www/cdndev.hub.plus',
    ),
    'google-maps' => array(
        'key' => 'AIzaSyAYhvQcSE5MbdN3DGgmWgoxNNnO7kb_f94',
    ),
    'google-analytics' => array(
        'ua' => 'UA-79615533-2',
        'ua-app' => ''
    ),
    'database' => array(
        'core' => array(
            'dsn' => 'mysql:host=127.0.0.1;dbname=hpdev_core;charset=utf8',
            'usr' => 'root',
            'pwd' => '54328f6b27a45c51986ed436f3f609bf',
            'db_name' => 'hpdev_core',
        ),
        'logfilepath' => '/var/www/logdev.hub.plus/hpdev_core-database.log',
        'maxPerPage' => 10,
    ),
    'view_manager' => array(
        'base_path' => 'https://dev.hub.plus',
    ),
    'push-notification-file-path' => '/var/www/push-notification-certs',
    'hub_plus_store_links' => array(
        'app_store' => 'https://itunes.apple.com/us/app/hub-plus/id1187270256?l=it&ls=1&mt=8',
        'play_store' => 'https://play.google.com/store/apps/details?id=plus.hub.app.tester'
    ),
);