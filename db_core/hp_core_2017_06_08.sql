TRUNCATE TABLE `role`;

INSERT INTO `role` (`id`, `name`, `description`, `icon`)
VALUES
  (1, 'SUPERADMIN', 'Super Administrator', '/icon/admin.jpg'),
  (2, 'ADMIN', 'Administrator', '/icon/admin.jpg'),
  (3, 'USER', 'User', NULL);


ALTER TABLE `section` ADD `description` TEXT  NULL  AFTER `title`;
