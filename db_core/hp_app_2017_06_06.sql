UPDATE `section` SET `area` = 'FOOTER' WHERE `area` = 'MENU';

UPDATE `section` SET `area` = 'MENU' WHERE `area` = 'DRAWER';

ALTER TABLE `media` ADD `device_id` INT(11)  UNSIGNED  NULL  DEFAULT NULL  AFTER `id`;
ALTER TABLE `media` ADD FOREIGN KEY (`device_id`) REFERENCES `device` (`id`);
