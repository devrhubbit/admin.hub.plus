ALTER TABLE application
  ADD `push_notification` TEXT NULL AFTER template_id,
  ADD `facebook_token` VARCHAR(255)  NULL  DEFAULT NULL  AFTER `demo`,
  ADD `google_analytics_ua` TEXT  NULL  AFTER `facebook_token`,
  ADD `background_color` VARCHAR(7)  NULL  DEFAULT NULL  AFTER `splash_screen`,
  ADD `app_logo` VARCHAR(255)  NULL  DEFAULT NULL  AFTER `background_color`;
