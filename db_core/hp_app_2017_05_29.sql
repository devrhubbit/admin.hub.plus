ALTER TABLE `device` CHANGE `id` `id` INT(11)  UNSIGNED  NOT NULL  AUTO_INCREMENT;


CREATE TABLE `push_notification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `body` text NOT NULL,
  `section_id` int(11) unsigned DEFAULT NULL,
  `post_id` int(11) unsigned DEFAULT NULL,
  `send_date` datetime NOT NULL,
  `receiver_filter` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `section_id` (`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `push_notification_device` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notification_id` int(11) unsigned NOT NULL,
  `device_id` int(11) unsigned NOT NULL,
  `push_notification_hash_id` varchar(32) NOT NULL DEFAULT '',
  `read_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notification_id` (`notification_id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `push_notification_device_ibfk_1` FOREIGN KEY (`notification_id`) REFERENCES `push_notification` (`id`),
  CONSTRAINT `push_notification_device_ibfk_2` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `device` ADD `ip_address` VARCHAR(50)  NULL  DEFAULT NULL  AFTER `api_key`;
