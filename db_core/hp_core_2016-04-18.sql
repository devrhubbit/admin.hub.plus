# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.47-0ubuntu0.14.04.1)
# Database: hp_core
# Generation Time: 2016-04-18 17:08:03 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table application
# ------------------------------------------------------------

DROP TABLE IF EXISTS `application`;

CREATE TABLE `application` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `bundle` varchar(255) DEFAULT NULL COMMENT 'unique valid app bundle string',
  `token` varchar(255) DEFAULT NULL COMMENT 'unique token valid to authenticate app on H+ server',
  `dsn` varchar(255) DEFAULT NULL COMMENT 'database DSN dinamically generate during registration',
  `server_name` varchar(255) DEFAULT NULL COMMENT 'sarver name to identify where database is (if in the future we will have multiple server)',
  `api_version` tinyint(1) DEFAULT NULL COMMENT 'api version used by app',
  `template_id` int(11) DEFAULT NULL COMMENT 'related app template',
  `logo` varchar(255) DEFAULT NULL COMMENT 'app logo',
  `splash_screen` varchar(255) DEFAULT NULL COMMENT 'app splashscreen image',
  `white_label` tinyint(1) DEFAULT NULL COMMENT 'TRUE if app is unbranded by H+',
  `demo` tinyint(1) DEFAULT NULL COMMENT 'TRUE if app is in demo mode',
  `expired_at` datetime DEFAULT NULL COMMENT 'when account expires (if the monthly fee is not paid or the demo ends)',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `related_template` FOREIGN KEY (`id`) REFERENCES `template` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;

INSERT INTO `role` (`id`, `name`, `description`, `icon`)
VALUES
	(1,'ADMIN','Administrator','/icon/admin.jpg');

/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table template
# ------------------------------------------------------------

DROP TABLE IF EXISTS `template`;

CREATE TABLE `template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description` text,
  `icon` varchar(255) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `json_views` text COMMENT 'JSON defining views list of the template',
  `enabled` tinyint(1) DEFAULT NULL COMMENT 'TRUE if template is active, FALSE if exists only in old app release',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `template` WRITE;
/*!40000 ALTER TABLE `template` DISABLE KEYS */;

INSERT INTO `template` (`id`, `title`, `subtitle`, `description`, `icon`, `cover`, `json_views`, `enabled`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'modern','new modern layout','modern layout description will be here','/modern/icon.jpg','/modern/cover.jpg','[\"/modern/preview/view001.jpg\", \"/modern/preview/view002.jpg\", \"/modern/preview/view003.jpg\", \"/modern/preview/view001.jpg\"]',1,'2016-04-18 15:21:45','2016-04-18 15:21:45',NULL),
	(2,'classic','new classic layout','classic layout description will be here','/classic/icon.jpg','/classic/cover.jpg','[\"/classic/preview/view001.jpg\", \"/classic/preview/view002.jpg\", \"/classic/preview/view003.jpg\", \"/classic/preview/view001.jpg\"]',1,'2016-04-18 15:26:00','2016-04-18 15:26:00',NULL);

/*!40000 ALTER TABLE `template` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `imagePath` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `password_requested_at` int(11) DEFAULT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) DEFAULT '0',
  `locked_at` datetime DEFAULT NULL,
  `expired` tinyint(1) DEFAULT '0',
  `expired_at` datetime DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `related_role` FOREIGN KEY (`id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_application
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_application`;

CREATE TABLE `user_application` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `related_app` FOREIGN KEY (`id`) REFERENCES `application` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `related_user` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
