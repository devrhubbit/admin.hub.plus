

-- RUN ON EVERY APP DB
CREATE TABLE hp_app_aaaTest28102106part2.post_action
(
  id INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  post_id INT(11) UNSIGNED NOT NULL,
  type VARCHAR(10) NOT NULL,
  label VARCHAR(255) NOT NULL,
  icon VARCHAR(255),
  params LONGTEXT,
  weight INT,
  section_id INT(11) UNSIGNED,
  content_id INT(11) UNSIGNED,
  created_at DATETIME NOT NULL,
  updated_at DATETIME NOT NULL,
  deleted_at DATETIME
);


-- RUN ON EVERY APP DB
ALTER TABLE hp_app_aaaTest28102106part2.section ADD parsed_tags LONGTEXT NULL AFTER tags;
ALTER TABLE hp_app_aaaTest28102106part2.section ADD FULLTEXT KEY `TagsIndex` (`parsed_tags`);


-- 15/02/2017
-- RUN ON EVERY APP DB (query non pi� necessaria) FARE CHECK SE GIA' ESEGUITA LA CREAZIONE
-- ALTER TABLE post_event DROP booking;
-- ALTER TABLE post_event DROP location;