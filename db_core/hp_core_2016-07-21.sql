# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.49-0ubuntu0.14.04.1)
# Database: hp_core
# Generation Time: 2016-07-21 10:07:08 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table application
# ------------------------------------------------------------

CREATE TABLE `application` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `bundle` varchar(255) DEFAULT NULL COMMENT 'unique valid app bundle string',
  `app_key` varchar(255) DEFAULT NULL COMMENT 'unique token valid to authenticate app on H+ server',
  `dsn` varchar(255) DEFAULT NULL COMMENT 'database DSN dinamically generate during registration',
  `db_host` varchar(255) DEFAULT NULL COMMENT 'sarver name to identify where database is (if in the future we will have multiple server)',
  `db_name` varchar(255) DEFAULT NULL,
  `db_user` varchar(255) DEFAULT NULL,
  `db_pwd` varchar(255) DEFAULT NULL,
  `api_version` varchar(10) DEFAULT NULL COMMENT 'api version used by app',
  `pack_id` int(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL COMMENT 'related app template',
  `icon` varchar(255) DEFAULT NULL COMMENT 'app logo',
  `splash_screen` varchar(255) DEFAULT NULL COMMENT 'app splashscreen image',
  `layout` text COMMENT 'json with global layout app setting',
  `white_label` tinyint(1) DEFAULT NULL COMMENT 'TRUE if app is unbranded by H+',
  `demo` tinyint(1) DEFAULT NULL COMMENT 'TRUE if app is in demo mode',
  `expired_at` datetime DEFAULT NULL COMMENT 'when account expires (if the monthly fee is not paid or the demo ends)',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bundle` (`bundle`),
  KEY `template_index` (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table category
# ------------------------------------------------------------

CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'category unique identifier',
  `name` varchar(255) DEFAULT NULL COMMENT 'category name',
  `weight` int(11) DEFAULT NULL COMMENT 'ordering priority weight',
  `visible` tinyint(1) DEFAULT '1' COMMENT 'TRUE if category is visible in category list',
  `setting` text COMMENT 'json with enabled section per category',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pack
# ------------------------------------------------------------

CREATE TABLE `pack` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'pack name',
  `bundle` varchar(255) DEFAULT NULL COMMENT 'unique identifier string',
  `json_constraints` text COMMENT 'json width pack limit',
  `expired` tinyint(1) DEFAULT NULL COMMENT 'TRUE if expired',
  `expired_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table role
# ------------------------------------------------------------

CREATE TABLE `role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table template
# ------------------------------------------------------------

CREATE TABLE `template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description` text,
  `icon` varchar(255) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `json_views` text COMMENT 'JSON defining views list of the template',
  `layout` text COMMENT 'JSON defining the standard layout relate to this template',
  `enabled` tinyint(1) DEFAULT NULL COMMENT 'TRUE if template is active, FALSE if exists only in old app release',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user
# ------------------------------------------------------------

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `imagePath` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `password_requested_at` int(11) DEFAULT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) DEFAULT '0',
  `locked_at` datetime DEFAULT NULL,
  `expired` tinyint(1) DEFAULT '0',
  `expired_at` datetime DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `privacy` tinyint(1) DEFAULT NULL,
  `privacy_at` datetime DEFAULT NULL,
  `terms` tinyint(1) DEFAULT NULL,
  `terms_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_application
# ------------------------------------------------------------

CREATE TABLE `user_application` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  `connected_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `delated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
