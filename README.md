HUB.PLUS
=======================

This is a simple tutorial writen to defined how to install and run HUB.plus in your local LAMP server.  

Installation
------------

### Download Composer

The recommended way to get a working copy of this project is to clone the repository
and use `composer` to install dependencies using the `create-project` command:
```bash
curl -s https://getcomposer.org/installer | php --
```

### Clone repository
Now, clone this repository and  invoke `composer` using the shipped
`composer.phar`:
```bash
cd my/project/dir
git clone https://bitbucket.org/devrhubbit/admin.hub.plus.git
cd project_folder
php composer.phar self-update
php composer.phar install
```

(The `self-update` directive is to ensure you have an up-to-date `composer.phar`
available.)

You would then invoke `composer` to install dependencies.


### Configure database connection
To ensure valid database connection create you need to:
nell’ordine:

1 - create DB called `hp_core` in your local mysql instance

2 - mouving in `Database` module folder and create a config file replacing placeholder with your database connection params
```bash
cd ./module/Database
cp propel.parameters.local.template propel.parameters.local.php
```

3 - execute the following propel commmand
```bash
../../vendor/propel/propel/bin/propel config:convert
../../vendor/propel/propel/bin/propel sql:build --overwrite
../../vendor/propel/propel/bin/propel model:build
../../vendor/propel/propel/bin/propel sql:insert
```

4 - Copy `./config/global.php` in `./config/local.php` replacing placeholder params.

### Configure application
To complete your installation you need move in your application root and execute the following command to configure message module and populate database with initial data:
```bash
cd my/project/dir
php ./public/index.php zf2-messenger install
php ./public/index.php hp install $param1 $param2
```

Where `$param1` is your domain name and `$param2` is your local tld 
(eg if you would run your application with `http://hubplus.dev` then you should have `$param1=hubplus` and `$param2=dev`). 


### Configure VirtualHost
To run apache in your apache web server you need a really standard `zend framework 2` virtual host like this:

    <VirtualHost *:80>
        ServerName hubplus.dev
        ServerAlias hubplus.dev
        DocumentRoot /my/project/dir/public

        <Directory /my/project/dir/public >
            Options -Indexes
            DirectoryIndex index.php index.html
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>


RUN
===
Now you are ready to run you application. 
The HUB.plus installer create a super admin user but you don't use this.

Follow the next three steps:

1. register new user
2. create your hub.plus application following the starting application wizard 
3. navigate and use hub.plus

